CREATE OR REPLACE FUNCTION f_check_spell_name_russia (pi_val VARCHAR2) RETURN VARCHAR2 IS
    p_val VARCHAR2(2000);
		p_s VARCHAR2(10);
	BEGIN
		IF pi_val IS NULL THEN
			RETURN 'N';
		END IF;

	  p_val := upper(TRIM(pi_val));

		FOR w IN 1..length (p_val)
		LOOP

			p_s := substr (p_val,w,1);
			IF p_s NOT IN ('�','�','�','�','�',
			              '�','�','�','�','�','�',
										'�','�','�','�','�',
										'�','�','�','�','�',
										'�','�','�','�','�',
										'�','�','�','�','�',
										'�','�',' ','-')
			THEN
				RETURN 'N';

      end IF;

    END LOOP;
    RETURN 'Y';   
    EXCEPTION
      WHEN OTHERS THEN
        RETURN 'Y';
        
  END;
/
