i�?CREATE OR REPLACE FUNCTION LAWMAIN."F$_USR_ID" RETURN VARCHAR2
	IS
	L_ID NUMBER;
BEGIN
	FOR i IN (
		SELECT LST.ID
		FROM T_USER_LIST LST
		WHERE LST.USERNAME = NVL(TRIM(UPPER(V('APP_USER'))),'ADMIN')
						)
	LOOP
		RETURN i.id;
	END LOOP;
	RETURN L_ID;
END;
/

