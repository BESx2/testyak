i�?CREATE OR REPLACE PACKAGE LAWMAIN.PKG_DOC_014 IS

	-- Author  : BES
	-- Created : 14.02.2019
	-- Purpose : УТОЧНЕНИЕ ИСКОВОГО
	-- CODE    : RESTRUCT_AGREE

	FUNCTION RUN_REPORT(P_PAR_01 VARCHAR2 DEFAULT NULL
										 ,P_PAR_02 VARCHAR2 DEFAULT NULL
										 ,P_PAR_03 VARCHAR2 DEFAULT NULL
										 ,P_PAR_04 VARCHAR2 DEFAULT NULL
										 ,P_PAR_05 VARCHAR2 DEFAULT NULL
										 ,P_PAR_06 VARCHAR2 DEFAULT NULL
										 ,P_PAR_07 VARCHAR2 DEFAULT NULL
										 ,P_PAR_08 VARCHAR2 DEFAULT NULL
										 ,P_PAR_09 VARCHAR2 DEFAULT NULL
										 ,P_PAR_10 VARCHAR2 DEFAULT NULL)
		RETURN LAWSUP.PKG_FILES.REC_DOC;

END PKG_DOC_014;
/

CREATE OR REPLACE PACKAGE BODY LAWMAIN.PKG_DOC_014 IS

	TYPE REC_DOC IS RECORD(
		 P_BLOB BLOB DEFAULT NULL);

	RTF_DOC REC_DOC := NULL;
  
	
	
	FUNCTION F_MAKE_TABLE(P_ID_REST NUMBER) RETURN CLOB
		IS                                  
		L_HEADER CLOB;
		L_ROW    CLOB;   
		L_RET    CLOB;        
		L_FOOTER CLOB;
		L_DOC_MRG BOOLEAN := FALSE;     
		L_CASE_MRG BOOLEAN := FALSE;
		L_PAY_MRG BOOLEAN := FALSE;     
		L_NLS     VARCHAR2(50) := 'NLS_NUMERIC_CHARACTERS='', ''';
		L_FMT     VARCHAR2(50) := 'FM999G999G999G999G990D00'; 
		L_CNT     NUMBER := 0;
    L_FIRST_ROW_CASE BOOLEAN DEFAULT TRUE;     
		L_SUM_TOLL NUMBER; 
		L_SUM_PENY NUMBER;  
		L_SUM_PAY  NUMBER;
		
		L_TOT_DEBT NUMBER := 0;
		L_TOT_PENY NUMBER := 0;
		L_TOT_TOLL NUMBER := 0;
		L_TOT_PENY_COURT NUMBER := 0;
		L_TOT_PAY  NUMBER := 0;
	BEGIN           
		L_HEADER := '\trowd \irow0\irowband0\ltrrow\ts11\trgaph108\trleft-57\trbrdrt\brdrs\brdrw10\brdrcf1 \trbrdrl\brdrs\brdrw10\brdrcf1 \trbrdrb\brdrs\brdrw10\brdrcf1 \trbrdrr\brdrs\brdrw10\brdrcf1 \trbrdrh\brdrs\brdrw10\brdrcf1 \trbrdrv\brdrs\brdrw10\brdrcf1 
\trftsWidth2\trwWidth5000\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddft3\trpaddfb3\trpaddfr3\tblrsid3293709\tbllkhdrrows\tbllkhdrcols\tbllknocolband\tblind0\tblindtype3 \clvertalc\clbrdrt\brdrs\brdrw10\brdrcf1 \clbrdrl\brdrs\brdrw10\brdrcf1 
\clbrdrb\brdrs\brdrw10\brdrcf1 \clbrdrr\brdrs\brdrw10\brdrcf1 \cltxlrtb\clftsWidth3\clwWidth562\clpadt57\clpadr57\clpadft3\clpadfr3 \cellx552\clvertalc\clbrdrt\brdrs\brdrw10\brdrcf1 \clbrdrl\brdrs\brdrw10\brdrcf1 \clbrdrb\brdrs\brdrw10\brdrcf1 \clbrdrr
\brdrs\brdrw10\brdrcf1 \cltxlrtb\clftsWidth3\clwWidth1134\clshdrawnil \cellx1779\clvertalc\clbrdrt\brdrs\brdrw10\brdrcf1 \clbrdrl\brdrs\brdrw10\brdrcf1 \clbrdrb\brdrs\brdrw10\brdrcf1 \clbrdrr\brdrs\brdrw10\brdrcf1 
\cltxlrtb\clftsWidth3\clwWidth1134\clpadt57\clpadr57\clpadft3\clpadfr3 \cellx3006\clvertalc\clbrdrt\brdrs\brdrw10\brdrcf1 \clbrdrl\brdrs\brdrw10\brdrcf1 \clbrdrb\brdrs\brdrw10\brdrcf1 \clbrdrr\brdrs\brdrw10\brdrcf1 
\cltxlrtb\clftsWidth3\clwWidth1276\clpadt57\clpadr57\clpadft3\clpadfr3 \cellx4387\clvertalc\clbrdrt\brdrs\brdrw10\brdrcf1 \clbrdrl\brdrs\brdrw10\brdrcf1 \clbrdrb\brdrs\brdrw10\brdrcf1 \clbrdrr\brdrs\brdrw10\brdrcf1 
\cltxlrtb\clftsWidth3\clwWidth1134\clpadt57\clpadr57\clpadft3\clpadfr3 \cellx5614\clvertalc\clbrdrt\brdrs\brdrw10\brdrcf1 \clbrdrl\brdrs\brdrw10\brdrcf1 \clbrdrb\brdrs\brdrw10\brdrcf1 \clbrdrr\brdrs\brdrw10\brdrcf1 
\cltxlrtb\clftsWidth3\clwWidth993\clpadt57\clpadr57\clpadft3\clpadfr3 \cellx6689\clvertalc\clbrdrt\brdrs\brdrw10\brdrcf1 \clbrdrl\brdrs\brdrw10\brdrcf1 \clbrdrb\brdrs\brdrw10\brdrcf1 \clbrdrr\brdrs\brdrw10\brdrcf1 
\cltxlrtb\clftsWidth3\clwWidth1134\clshdrawnil \cellx7916\clvertalc\clbrdrt\brdrs\brdrw10\brdrcf1 \clbrdrl\brdrs\brdrw10\brdrcf1 \clbrdrb\brdrs\brdrw10\brdrcf1 \clbrdrr\brdrs\brdrw10\brdrcf1 
\cltxlrtb\clftsWidth3\clwWidth1275\clpadt57\clpadr57\clpadft3\clpadfr3 \cellx9296\clvertalc\clbrdrt\brdrs\brdrw10\brdrcf1 \clbrdrl\brdrs\brdrw10\brdrcf1 \clbrdrb\brdrs\brdrw10\brdrcf1 \clbrdrr\brdrs\brdrw10\brdrcf1 
\cltxlrtb\clftsWidth3\clwWidth1134\clpadt57\clpadr57\clpadft3\clpadfr3 \cellx10523\pard\plain \ltrpar\qc \li0\ri0\sl276\slmult1\widctlpar\intbl\wrapdefault\aspalpha\aspnum\faauto\adjustright\rin0\lin0\pararsid8337278 \rtlch\fcs1 \af0\afs24\alang1025 
\ltrch\fcs0 \fs24\lang1049\langfe1033\loch\af39\hich\af39\dbch\af39\cgrid\langnp1049\langfenp1033 {\rtlch\fcs1 \af1\afs22 \ltrch\fcs0 \f1\fs22\insrsid14366354\charrsid12926852 \loch\af1\dbch\af39\hich\f1 \''b9\loch\f1 \hich\f1  \''ef\loch\f1 \hich\f1 /\''ef
\cell }\pard \ltrpar\qc \li0\ri0\sl276\slmult1\widctlpar\intbl\wrapdefault\aspalpha\aspnum\faauto\adjustright\rin0\lin0\pararsid1538448 {\rtlch\fcs1 \af1\afs22 \ltrch\fcs0 \f1\fs22\insrsid14366354\charrsid12926852 \loch\af1\dbch\af39\hich\f1 \''b9\loch\f1 
\hich\f1  \''e4\''e5\''eb\''e0
\par \loch\af1\dbch\af39\hich\f1 \''e2\loch\f1 \hich\f1  \''c0\''f0\''e1\''e8\''f2\''f0\''e0\''e6\''ed\''ee\''ec\loch\f1 \hich\f1  \''f1\''f3\''e4\''e5\cell }\pard \ltrpar\qc \li0\ri0\sl276\slmult1
\widctlpar\intbl\wrapdefault\aspalpha\aspnum\faauto\adjustright\rin0\lin0\pararsid8337278 {\rtlch\fcs1 \af1\afs22 \ltrch\fcs0 \f1\fs22\insrsid14366354\charrsid12926852 \loch\af1\dbch\af39\hich\f1 \''cf\''e5\''f0\''e8\''ee\''e4
\par }{\rtlch\fcs1 \af1\afs22 \ltrch\fcs0 \f1\fs22\insrsid11677508 \loch\af1\dbch\af39\hich\f1 \''e7}{\rtlch\fcs1 \af1\afs22 \ltrch\fcs0 \f1\fs22\insrsid14366354\charrsid12926852 \loch\af1\dbch\af39\hich\f1 \''e0\''e4\''ee\''eb\''e6\''e5\''ed\''ed\''ee\''f1\''f2\''e8\cell 
\loch\af1\dbch\af39\hich\f1 \''ce\''f1\''ed\''ee\''e2\''ed\''ee\''e9\loch\f1 \hich\f1  \''e4\''ee\''eb\''e3,
\par \loch\af1\dbch\af39\hich\f1 \''f0\''f3\''e1\loch\f1 \hich\f1 . \''f1\loch\f1 \hich\f1  \''cd\''c4\''d1\cell \loch\af1\dbch\af39\hich\f1 \''cf\''f0\''ee\''f6\''e5\''ed\''f2\''fb\loch\f1 \hich\f1 , \''f0\''f3\''e1.\cell \loch\af1\dbch\af39\hich\f1 \''c3\''ee\''f1\''ef\''ee\''f8
\''eb\''e8\''ed\''e0,
\par \loch\af1\dbch\af39\hich\f1 \''f0\''f3\''e1.\cell \loch\af1\dbch\af39\hich\f1 \''cd\''e5\''f3\''f1\''f2\''ee\''e9\''ea\''e0\loch\f1 \hich\f1  \''ef\''ee\loch\f1 \hich\f1  \''f0\''e5\''f8\''e5\''ed\''e8\''fe\loch\f1 \hich\f1  \''f1\''f3\''e4\''e0\cell \loch\af1\dbch\af39\hich\f1 
\''ce\''e1\''f9\''e0\''ff\loch\f1 \hich\f1  \''f1\''f3\''ec\''ec\''e0\loch\f1 \hich\f1  \''ef\''ee\''e3\''e0\''f8\''e5\''ed\''e8\''ff\loch\f1 , \loch\af1\dbch\af39\hich\f1 \''f0\''f3\''e1.\cell \loch\af1\dbch\af39\hich\f1 \''d1\''f0\''ee\''ea\loch\f1 \hich\f1  \''ef\''ee\''e3\''e0\''f8
\''e5\''ed\''e8\''ff,
\par \loch\af1\dbch\af39\hich\f1 \''ed\''e5\loch\f1 \hich\f1  \''ef\''ee\''e7\''e4\''ed\''e5\''e5\cell }\pard\plain \ltrpar\ql \li0\ri0\sa160\sl259\slmult1\widctlpar\intbl\wrapdefault\aspalpha\aspnum\faauto\adjustright\rin0\lin0 \rtlch\fcs1 \af0\afs24\alang1025 
\ltrch\fcs0 \fs24\lang1049\langfe1033\loch\af39\hich\af39\dbch\af39\cgrid\langnp1049\langfenp1033 {\rtlch\fcs1 \af0 \ltrch\fcs0 \insrsid14366354\charrsid12926852 \trowd \irow0\irowband0\ltrrow\ts11\trgaph108\trleft-57\trbrdrt\brdrs\brdrw10\brdrcf1 
\trbrdrl\brdrs\brdrw10\brdrcf1 \trbrdrb\brdrs\brdrw10\brdrcf1 \trbrdrr\brdrs\brdrw10\brdrcf1 \trbrdrh\brdrs\brdrw10\brdrcf1 \trbrdrv\brdrs\brdrw10\brdrcf1 
\trftsWidth2\trwWidth5000\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddft3\trpaddfb3\trpaddfr3\tblrsid3293709\tbllkhdrrows\tbllkhdrcols\tbllknocolband\tblind0\tblindtype3 \clvertalc\clbrdrt\brdrs\brdrw10\brdrcf1 \clbrdrl\brdrs\brdrw10\brdrcf1 
\clbrdrb\brdrs\brdrw10\brdrcf1 \clbrdrr\brdrs\brdrw10\brdrcf1 \cltxlrtb\clftsWidth3\clwWidth562\clpadt57\clpadr57\clpadft3\clpadfr3 \cellx552\clvertalc\clbrdrt\brdrs\brdrw10\brdrcf1 \clbrdrl\brdrs\brdrw10\brdrcf1 \clbrdrb\brdrs\brdrw10\brdrcf1 \clbrdrr
\brdrs\brdrw10\brdrcf1 \cltxlrtb\clftsWidth3\clwWidth1134\clshdrawnil \cellx1779\clvertalc\clbrdrt\brdrs\brdrw10\brdrcf1 \clbrdrl\brdrs\brdrw10\brdrcf1 \clbrdrb\brdrs\brdrw10\brdrcf1 \clbrdrr\brdrs\brdrw10\brdrcf1 
\cltxlrtb\clftsWidth3\clwWidth1134\clpadt57\clpadr57\clpadft3\clpadfr3 \cellx3006\clvertalc\clbrdrt\brdrs\brdrw10\brdrcf1 \clbrdrl\brdrs\brdrw10\brdrcf1 \clbrdrb\brdrs\brdrw10\brdrcf1 \clbrdrr\brdrs\brdrw10\brdrcf1 
\cltxlrtb\clftsWidth3\clwWidth1276\clpadt57\clpadr57\clpadft3\clpadfr3 \cellx4387\clvertalc\clbrdrt\brdrs\brdrw10\brdrcf1 \clbrdrl\brdrs\brdrw10\brdrcf1 \clbrdrb\brdrs\brdrw10\brdrcf1 \clbrdrr\brdrs\brdrw10\brdrcf1 
\cltxlrtb\clftsWidth3\clwWidth1134\clpadt57\clpadr57\clpadft3\clpadfr3 \cellx5614\clvertalc\clbrdrt\brdrs\brdrw10\brdrcf1 \clbrdrl\brdrs\brdrw10\brdrcf1 \clbrdrb\brdrs\brdrw10\brdrcf1 \clbrdrr\brdrs\brdrw10\brdrcf1 
\cltxlrtb\clftsWidth3\clwWidth993\clpadt57\clpadr57\clpadft3\clpadfr3 \cellx6689\clvertalc\clbrdrt\brdrs\brdrw10\brdrcf1 \clbrdrl\brdrs\brdrw10\brdrcf1 \clbrdrb\brdrs\brdrw10\brdrcf1 \clbrdrr\brdrs\brdrw10\brdrcf1 
\cltxlrtb\clftsWidth3\clwWidth1134\clshdrawnil \cellx7916\clvertalc\clbrdrt\brdrs\brdrw10\brdrcf1 \clbrdrl\brdrs\brdrw10\brdrcf1 \clbrdrb\brdrs\brdrw10\brdrcf1 \clbrdrr\brdrs\brdrw10\brdrcf1 
\cltxlrtb\clftsWidth3\clwWidth1275\clpadt57\clpadr57\clpadft3\clpadfr3 \cellx9296\clvertalc\clbrdrt\brdrs\brdrw10\brdrcf1 \clbrdrl\brdrs\brdrw10\brdrcf1 \clbrdrb\brdrs\brdrw10\brdrcf1 \clbrdrr\brdrs\brdrw10\brdrcf1 
\cltxlrtb\clftsWidth3\clwWidth1134\clpadt57\clpadr57\clpadft3\clpadfr3 \cellx10523\row \ltrrow}';

  L_ROW := '\trowd \irow1\irowband1\ltrrow\ts11\trgaph108\trrh592\trleft-57\trbrdrt\brdrs\brdrw10\brdrcf1 \trbrdrl\brdrs\brdrw10\brdrcf1 \trbrdrb\brdrs\brdrw10\brdrcf1 
\trbrdrr\brdrs\brdrw10\brdrcf1 \trbrdrh\brdrs\brdrw10\brdrcf1 \trbrdrv\brdrs\brdrw10\brdrcf1 
\trftsWidth2\trwWidth5000\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddft3\trpaddfb3\trpaddfr3\tblrsid3293709\tbllkhdrrows\tbllkhdrcols\tbllknocolband\tblind0\tblindtype3 \clvertalc\clbrdrt\brdrs\brdrw10\brdrcf1 \clbrdrl\brdrs\brdrw10\brdrcf1 
\clbrdrb\brdrs\brdrw10\brdrcf1 \clbrdrr\brdrs\brdrw10\brdrcf1 \cltxlrtb\clftsWidth3\clwWidth562\clpadt57\clpadr57\clpadft3\clpadfr3 MERGEPAY \cellx552\clvertalc\clbrdrt\brdrs\brdrw10\brdrcf1 \clbrdrl\brdrs\brdrw10\brdrcf1 \clbrdrb\brdrs\brdrw10\brdrcf1 \clbrdrr
\brdrs\brdrw10\brdrcf1 \cltxlrtb\clftsWidth3\clwWidth1134\clshdrawnil MERGECASE \cellx1779\clvertalc\clbrdrt\brdrs\brdrw10\brdrcf1 \clbrdrl\brdrs\brdrw10\brdrcf1 \clbrdrb\brdrs\brdrw10\brdrcf1 \clbrdrr\brdrs\brdrw10\brdrcf1 
\cltxlrtb\clftsWidth3\clwWidth1134\clpadt57\clpadr57\clpadft3\clpadfr3 MERGEDOC \cellx3006\clvertalc\clbrdrt\brdrs\brdrw10\brdrcf1 \clbrdrl\brdrs\brdrw10\brdrcf1 \clbrdrb\brdrs\brdrw10\brdrcf1 \clbrdrr\brdrs\brdrw10\brdrcf1 
\cltxlrtb\clftsWidth3\clwWidth1276\clpadt57\clpadr57\clpadft3\clpadfr3 \cellx4387\clvertalc\clbrdrt\brdrs\brdrw10\brdrcf1 \clbrdrl\brdrs\brdrw10\brdrcf1 \clbrdrb\brdrs\brdrw10\brdrcf1 \clbrdrr\brdrs\brdrw10\brdrcf1 
\cltxlrtb\clftsWidth3\clwWidth1134\clpadt57\clpadr57\clpadft3\clpadfr3 \cellx5614\clvertalc\clbrdrt\brdrs\brdrw10\brdrcf1 \clbrdrl\brdrs\brdrw10\brdrcf1 \clbrdrb\brdrs\brdrw10\brdrcf1 \clbrdrr\brdrs\brdrw10\brdrcf1 
\cltxlrtb\clftsWidth3\clwWidth993\clpadt57\clpadr57\clpadft3\clpadfr3 \cellx6689\clvertalc\clbrdrt\brdrs\brdrw10\brdrcf1 \clbrdrl\brdrs\brdrw10\brdrcf1 \clbrdrb\brdrs\brdrw10\brdrcf1 \clbrdrr\brdrs\brdrw10\brdrcf1 
\cltxlrtb\clftsWidth3\clwWidth1134\clshdrawnil \cellx7916\clvertalc\clbrdrt\brdrs\brdrw10\brdrcf1 \clbrdrl\brdrs\brdrw10\brdrcf1 \clbrdrb\brdrs\brdrw10\brdrcf1 \clbrdrr\brdrs\brdrw10\brdrcf1 
\cltxlrtb\clftsWidth3\clwWidth1275\clpadt57\clpadr57\clpadft3\clpadfr3 MERGEPAY \cellx9296\clvertalc\clbrdrt\brdrs\brdrw10\brdrcf1 \clbrdrl\brdrs\brdrw10\brdrcf1 \clbrdrb\brdrs\brdrw10\brdrcf1 \clbrdrr\brdrs\brdrw10\brdrcf1 
\cltxlrtb\clftsWidth3\clwWidth1134\clpadt57\clpadr57\clpadft3\clpadfr3 MERGEPAY \cellx10523\pard\plain \ltrpar\qc \li0\ri0\sl276\slmult1\widctlpar\intbl\wrapdefault\aspalpha\aspnum\faauto\adjustright\rin0\lin0\pararsid8337278 \rtlch\fcs1 \af0\afs24\alang1025 
\ltrch\fcs0 \fs24\lang1049\langfe1033\loch\af39\hich\af39\dbch\af39\cgrid\langnp1049\langfenp1033 {\rtlch\fcs1 \af1\afs22 \ltrch\fcs0 \f1\fs22\insrsid6884740\charrsid12926852 \hich\af1\dbch\af39\loch\f1 ROWNUM\cell }\pard \ltrpar\qc \li0\ri0\sl276\slmult1
\widctlpar\intbl\wrapdefault\aspalpha\aspnum\faauto\adjustright\rin0\lin0\pararsid1538448 {\rtlch\fcs1 \af1\afs22 \ltrch\fcs0 \f1\fs22\insrsid6884740\charrsid12926852 NUMCASE \cell }\pard \ltrpar\qc \li0\ri0\sl276\slmult1
\widctlpar\intbl\wrapdefault\aspalpha\aspnum\faauto\adjustright\rin0\lin0\pararsid8337278 {\rtlch\fcs1 \af1\afs22 \ltrch\fcs0 \f1\fs22\insrsid6884740\charrsid12926852 PERIOD \cell }{\rtlch\fcs1 \af1\afs22 \ltrch\fcs0 
\f1\fs22\lang1033\langfe1033\langnp1033\insrsid6884740\charrsid8269614 DOCDEBT \cell }{\rtlch\fcs1 \af1\afs22 \ltrch\fcs0 \f1\fs22\insrsid6884740\charrsid8269614 PENYDEBT \cell }\pard \ltrpar\qc \li0\ri0\sl276\slmult1
\widctlpar\intbl\wrapdefault\aspalpha\aspnum\faauto\adjustright\rin0\lin0\pararsid13114049 {\rtlch\fcs1 \af1\afs22 \ltrch\fcs0 \f1\fs22\insrsid6884740\charrsid12926852 GOVTOLL \cell }\pard \ltrpar\qc \li0\ri0\sl276\slmult1
\widctlpar\intbl\wrapdefault\aspalpha\aspnum\faauto\adjustright\rin0\lin0\pararsid8337278 {\rtlch\fcs1 \af1\afs22 \ltrch\fcs0 \f1\fs22\insrsid6884740\charrsid12926852 PENYCOURT \cell }{\rtlch\fcs1 \af1\afs22 \ltrch\fcs0 \f1\fs22\insrsid6884740\charrsid8269614 
PAYSUM \cell }{\rtlch\fcs1 \af1\afs22 \ltrch\fcs0 \f1\fs22\insrsid6884740\charrsid12926852 PAYDATE \cell }\pard\plain \ltrpar\ql \li0\ri0\sa160\sl259\slmult1\widctlpar\intbl\wrapdefault\aspalpha\aspnum\faauto\adjustright\rin0\lin0 \rtlch\fcs1 \af0\afs24\alang1025 
\ltrch\fcs0 \fs24\lang1049\langfe1033\loch\af39\hich\af39\dbch\af39\cgrid\langnp1049\langfenp1033 {\rtlch\fcs1 \af0 \ltrch\fcs0 \insrsid6884740\charrsid12926852 \trowd \irow1\irowband1\ltrrow\ts11\trgaph108\trrh592\trleft-57\trbrdrt\brdrs\brdrw10\brdrcf1 
\trbrdrl\brdrs\brdrw10\brdrcf1 \trbrdrb\brdrs\brdrw10\brdrcf1 \trbrdrr\brdrs\brdrw10\brdrcf1 \trbrdrh\brdrs\brdrw10\brdrcf1 \trbrdrv\brdrs\brdrw10\brdrcf1 
\trftsWidth2\trwWidth5000\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddft3\trpaddfb3\trpaddfr3\tblrsid3293709\tbllkhdrrows\tbllkhdrcols\tbllknocolband\tblind0\tblindtype3 \clvertalc\clbrdrt\brdrs\brdrw10\brdrcf1 \clbrdrl\brdrs\brdrw10\brdrcf1 
\clbrdrb\brdrs\brdrw10\brdrcf1 \clbrdrr\brdrs\brdrw10\brdrcf1 \cltxlrtb\clftsWidth3\clwWidth562\clpadt57\clpadr57\clpadft3\clpadfr3 MERGEPAY \cellx552\clvertalc\clbrdrt\brdrs\brdrw10\brdrcf1 \clbrdrl\brdrs\brdrw10\brdrcf1 \clbrdrb\brdrs\brdrw10\brdrcf1 \clbrdrr
\brdrs\brdrw10\brdrcf1 \cltxlrtb\clftsWidth3\clwWidth1134\clshdrawnil MERGECASE \cellx1779\clvertalc\clbrdrt\brdrs\brdrw10\brdrcf1 \clbrdrl\brdrs\brdrw10\brdrcf1 \clbrdrb\brdrs\brdrw10\brdrcf1 \clbrdrr\brdrs\brdrw10\brdrcf1 
\cltxlrtb\clftsWidth3\clwWidth1134\clpadt57\clpadr57\clpadft3\clpadfr3 MERGEDOC \cellx3006\clvertalc\clbrdrt\brdrs\brdrw10\brdrcf1 \clbrdrl\brdrs\brdrw10\brdrcf1 \clbrdrb\brdrs\brdrw10\brdrcf1 \clbrdrr\brdrs\brdrw10\brdrcf1 
\cltxlrtb\clftsWidth3\clwWidth1276\clpadt57\clpadr57\clpadft3\clpadfr3 \cellx4387\clvertalc\clbrdrt\brdrs\brdrw10\brdrcf1 \clbrdrl\brdrs\brdrw10\brdrcf1 \clbrdrb\brdrs\brdrw10\brdrcf1 \clbrdrr\brdrs\brdrw10\brdrcf1 
\cltxlrtb\clftsWidth3\clwWidth1134\clpadt57\clpadr57\clpadft3\clpadfr3 \cellx5614\clvertalc\clbrdrt\brdrs\brdrw10\brdrcf1 \clbrdrl\brdrs\brdrw10\brdrcf1 \clbrdrb\brdrs\brdrw10\brdrcf1 \clbrdrr\brdrs\brdrw10\brdrcf1 
\cltxlrtb\clftsWidth3\clwWidth993\clpadt57\clpadr57\clpadft3\clpadfr3 \cellx6689\clvertalc\clbrdrt\brdrs\brdrw10\brdrcf1 \clbrdrl\brdrs\brdrw10\brdrcf1 \clbrdrb\brdrs\brdrw10\brdrcf1 \clbrdrr\brdrs\brdrw10\brdrcf1 
\cltxlrtb\clftsWidth3\clwWidth1134\clshdrawnil \cellx7916\clvertalc\clbrdrt\brdrs\brdrw10\brdrcf1 \clbrdrl\brdrs\brdrw10\brdrcf1 \clbrdrb\brdrs\brdrw10\brdrcf1 \clbrdrr\brdrs\brdrw10\brdrcf1 
\cltxlrtb\clftsWidth3\clwWidth1275\clpadt57\clpadr57\clpadft3\clpadfr3 MERGEPAY \cellx9296\clvertalc\clbrdrt\brdrs\brdrw10\brdrcf1 \clbrdrl\brdrs\brdrw10\brdrcf1 \clbrdrb\brdrs\brdrw10\brdrcf1 \clbrdrr\brdrs\brdrw10\brdrcf1 
\cltxlrtb\clftsWidth3\clwWidth1134\clpadt57\clpadr57\clpadft3\clpadfr3 MERGEPAY \cellx10523\row \ltrrow}';

  L_FOOTER := '\trowd \irow2\irowband2\lastrow \ltrrow\ts11\trgaph108\trleft-57\trbrdrt\brdrs\brdrw10\brdrcf1 \trbrdrl\brdrs\brdrw10\brdrcf1 \trbrdrb\brdrs\brdrw10\brdrcf1 
\trbrdrr\brdrs\brdrw10\brdrcf1 \trbrdrh\brdrs\brdrw10\brdrcf1 \trbrdrv\brdrs\brdrw10\brdrcf1 
\trftsWidth2\trwWidth5000\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddft3\trpaddfb3\trpaddfr3\tblrsid3293709\tbllkhdrrows\tbllkhdrcols\tbllknocolband\tblind0\tblindtype3 \clvertalc\clbrdrt\brdrs\brdrw10\brdrcf1 \clbrdrl\brdrs\brdrw10\brdrcf1 
\clbrdrb\brdrs\brdrw10\brdrcf1 \clbrdrr\brdrs\brdrw10\brdrcf1 \cltxlrtb\clftsWidth3\clwWidth562\clpadt57\clpadr57\clpadft3\clpadfr3 \cellx552\clvertalc\clbrdrt\brdrs\brdrw10\brdrcf1 \clbrdrl\brdrs\brdrw10\brdrcf1 \clbrdrb\brdrs\brdrw10\brdrcf1 \clbrdrr
\brdrs\brdrw10\brdrcf1 \cltxlrtb\clftsWidth3\clwWidth1134\clshdrawnil \cellx1779\clvertalc\clbrdrt\brdrs\brdrw10\brdrcf1 \clbrdrl\brdrs\brdrw10\brdrcf1 \clbrdrb\brdrs\brdrw10\brdrcf1 \clbrdrr\brdrs\brdrw10\brdrcf1 
\cltxlrtb\clftsWidth3\clwWidth1134\clpadt57\clpadr57\clpadft3\clpadfr3 \cellx3006\clvertalc\clbrdrt\brdrs\brdrw10\brdrcf1 \clbrdrl\brdrs\brdrw10\brdrcf1 \clbrdrb\brdrs\brdrw10\brdrcf1 \clbrdrr\brdrs\brdrw10\brdrcf1 
\cltxlrtb\clftsWidth3\clwWidth1276\clpadt57\clpadr57\clpadft3\clpadfr3 \cellx4387\clvertalc\clbrdrt\brdrs\brdrw10\brdrcf1 \clbrdrl\brdrs\brdrw10\brdrcf1 \clbrdrb\brdrs\brdrw10\brdrcf1 \clbrdrr\brdrs\brdrw10\brdrcf1 
\cltxlrtb\clftsWidth3\clwWidth1134\clpadt57\clpadr57\clpadft3\clpadfr3 \cellx5614\clvertalc\clbrdrt\brdrs\brdrw10\brdrcf1 \clbrdrl\brdrs\brdrw10\brdrcf1 \clbrdrb\brdrs\brdrw10\brdrcf1 \clbrdrr\brdrs\brdrw10\brdrcf1 
\cltxlrtb\clftsWidth3\clwWidth993\clpadt57\clpadr57\clpadft3\clpadfr3 \cellx6689\clvertalc\clbrdrt\brdrs\brdrw10\brdrcf1 \clbrdrl\brdrs\brdrw10\brdrcf1 \clbrdrb\brdrs\brdrw10\brdrcf1 \clbrdrr\brdrs\brdrw10\brdrcf1 
\cltxlrtb\clftsWidth3\clwWidth1134\clshdrawnil \cellx7916\clvertalc\clbrdrt\brdrs\brdrw10\brdrcf1 \clbrdrl\brdrs\brdrw10\brdrcf1 \clbrdrb\brdrs\brdrw10\brdrcf1 \clbrdrr\brdrs\brdrw10\brdrcf1 
\cltxlrtb\clftsWidth3\clwWidth1275\clpadt57\clpadr57\clpadft3\clpadfr3 \cellx9296\clvertalc\clbrdrt\brdrs\brdrw10\brdrcf1 \clbrdrl\brdrs\brdrw10\brdrcf1 \clbrdrb\brdrs\brdrw10\brdrcf1 \clbrdrr\brdrs\brdrw10\brdrcf1 
\cltxlrtb\clftsWidth3\clwWidth1134\clpadt57\clpadr57\clpadft3\clpadfr3 \cellx10523\pard\plain \ltrpar\qc \li0\ri0\sl276\slmult1\widctlpar\intbl\wrapdefault\aspalpha\aspnum\faauto\adjustright\rin0\lin0\pararsid8337278 \rtlch\fcs1 \af0\afs24\alang1025 
\ltrch\fcs0 \fs24\lang1049\langfe1033\loch\af39\hich\af39\dbch\af39\cgrid\langnp1049\langfenp1033 {\rtlch\fcs1 \af1\afs22 \ltrch\fcs0 \f1\fs22\insrsid14366354\charrsid12926852 \cell }\pard \ltrpar\qc \li0\ri0\sl276\slmult1
\widctlpar\intbl\wrapdefault\aspalpha\aspnum\faauto\adjustright\rin0\lin0\pararsid1538448 {\rtlch\fcs1 \af1\afs22 \ltrch\fcs0 \f1\fs22\insrsid6884740 \loch\af1\dbch\af39\hich\f1 \''c8\''f2\''ee\''e3\''ee}{\rtlch\fcs1 \af1\afs22 \ltrch\fcs0 
\f1\fs22\insrsid14366354\charrsid12926852 \cell }\pard \ltrpar\qc \li0\ri0\sl276\slmult1\widctlpar\intbl\wrapdefault\aspalpha\aspnum\faauto\adjustright\rin0\lin0\pararsid8337278 {\rtlch\fcs1 \af1\afs22 \ltrch\fcs0 
\f1\fs22\insrsid14366354\charrsid12926852 \cell }{\rtlch\fcs1 \af1\afs22 \ltrch\fcs0 \f1\fs22\insrsid14366354\charrsid8269614 TOTDEBT \cell TOTPENY\cell }{\rtlch\fcs1 \af1\afs22 \ltrch\fcs0 \f1\fs22\insrsid14366354\charrsid12926852 TOTTOLL\cell }{\rtlch\fcs1 \af1\afs22 
\ltrch\fcs0 \f1\fs22\insrsid14366354\charrsid12926852 TOTCOURTPENY \cell }{\rtlch\fcs1 \af1\afs22 \ltrch\fcs0 \f1\fs22\insrsid14366354\charrsid8269614 PAYTOT \cell }{\rtlch\fcs1 \af1\afs22 \ltrch\fcs0 \f1\fs22\insrsid14366354\charrsid12926852 \cell }\pard\plain \ltrpar
\ql \li0\ri0\sa160\sl259\slmult1\widctlpar\intbl\wrapdefault\aspalpha\aspnum\faauto\adjustright\rin0\lin0 \rtlch\fcs1 \af0\afs24\alang1025 \ltrch\fcs0 \fs24\lang1049\langfe1033\loch\af39\hich\af39\dbch\af39\cgrid\langnp1049\langfenp1033 {\rtlch\fcs1 \af0 
\ltrch\fcs0 \insrsid14366354\charrsid12926852 \trowd \irow2\irowband2\lastrow \ltrrow\ts11\trgaph108\trleft-57\trbrdrt\brdrs\brdrw10\brdrcf1 \trbrdrl\brdrs\brdrw10\brdrcf1 \trbrdrb\brdrs\brdrw10\brdrcf1 \trbrdrr\brdrs\brdrw10\brdrcf1 \trbrdrh
\brdrs\brdrw10\brdrcf1 \trbrdrv\brdrs\brdrw10\brdrcf1 \trftsWidth2\trwWidth5000\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddft3\trpaddfb3\trpaddfr3\tblrsid3293709\tbllkhdrrows\tbllkhdrcols\tbllknocolband\tblind0\tblindtype3 \clvertalc\clbrdrt
\brdrs\brdrw10\brdrcf1 \clbrdrl\brdrs\brdrw10\brdrcf1 \clbrdrb\brdrs\brdrw10\brdrcf1 \clbrdrr\brdrs\brdrw10\brdrcf1 \cltxlrtb\clftsWidth3\clwWidth562\clpadt57\clpadr57\clpadft3\clpadfr3 \cellx552\clvertalc\clbrdrt\brdrs\brdrw10\brdrcf1 \clbrdrl
\brdrs\brdrw10\brdrcf1 \clbrdrb\brdrs\brdrw10\brdrcf1 \clbrdrr\brdrs\brdrw10\brdrcf1 \cltxlrtb\clftsWidth3\clwWidth1134\clshdrawnil \cellx1779\clvertalc\clbrdrt\brdrs\brdrw10\brdrcf1 \clbrdrl\brdrs\brdrw10\brdrcf1 \clbrdrb\brdrs\brdrw10\brdrcf1 \clbrdrr
\brdrs\brdrw10\brdrcf1 \cltxlrtb\clftsWidth3\clwWidth1134\clpadt57\clpadr57\clpadft3\clpadfr3 \cellx3006\clvertalc\clbrdrt\brdrs\brdrw10\brdrcf1 \clbrdrl\brdrs\brdrw10\brdrcf1 \clbrdrb\brdrs\brdrw10\brdrcf1 \clbrdrr\brdrs\brdrw10\brdrcf1 
\cltxlrtb\clftsWidth3\clwWidth1276\clpadt57\clpadr57\clpadft3\clpadfr3 \cellx4387\clvertalc\clbrdrt\brdrs\brdrw10\brdrcf1 \clbrdrl\brdrs\brdrw10\brdrcf1 \clbrdrb\brdrs\brdrw10\brdrcf1 \clbrdrr\brdrs\brdrw10\brdrcf1 
\cltxlrtb\clftsWidth3\clwWidth1134\clpadt57\clpadr57\clpadft3\clpadfr3 \cellx5614\clvertalc\clbrdrt\brdrs\brdrw10\brdrcf1 \clbrdrl\brdrs\brdrw10\brdrcf1 \clbrdrb\brdrs\brdrw10\brdrcf1 \clbrdrr\brdrs\brdrw10\brdrcf1 
\cltxlrtb\clftsWidth3\clwWidth993\clpadt57\clpadr57\clpadft3\clpadfr3 \cellx6689\clvertalc\clbrdrt\brdrs\brdrw10\brdrcf1 \clbrdrl\brdrs\brdrw10\brdrcf1 \clbrdrb\brdrs\brdrw10\brdrcf1 \clbrdrr\brdrs\brdrw10\brdrcf1 
\cltxlrtb\clftsWidth3\clwWidth1134\clshdrawnil \cellx7916\clvertalc\clbrdrt\brdrs\brdrw10\brdrcf1 \clbrdrl\brdrs\brdrw10\brdrcf1 \clbrdrb\brdrs\brdrw10\brdrcf1 \clbrdrr\brdrs\brdrw10\brdrcf1 
\cltxlrtb\clftsWidth3\clwWidth1275\clpadt57\clpadr57\clpadft3\clpadfr3 \cellx9296\clvertalc\clbrdrt\brdrs\brdrw10\brdrcf1 \clbrdrl\brdrs\brdrw10\brdrcf1 \clbrdrb\brdrs\brdrw10\brdrcf1 \clbrdrr\brdrs\brdrw10\brdrcf1 
\cltxlrtb\clftsWidth3\clwWidth1134\clpadt57\clpadr57\clpadft3\clpadfr3 \cellx10523\row }';
		
    L_RET := L_HEADER;
		
		
		SELECT SUM(ex.court_toll), SUM(ex.court_peny)
		INTO L_SUM_TOLL, L_SUM_PENY
    FROM  T_CLI_DEBTS D1,
          T_CLI_CASES S, 
          (SELECT EX.ID_CASE, EX.NUM_EXEC, ex.court_peny, EX.COURT_TOLL
           FROM  T_CLI_COURT_EXEC EX
           LEFT OUTER JOIN T_CLI_COURT_EXEC EX2
           ON (EX.ID_CASE = EX2.ID_CASE AND EX.DATE_END < EX2.DATE_END)            
           WHERE EX2.ID IS NULL) EX            
 	  WHERE S.ID_CASE = D1.ID_WORK
	  AND   EX.ID_CASE = S.ID_CASE
		AND   D1.ID_DOC IN (SELECT CD.ID_DOC FROM T_CLI_DEBTS CD WHERE CD.ID_WORK = P_ID_REST);
		
		
		FOR I IN (SELECT T.*,   
										 LEAD(T.NUM_EXEC,1) OVER(ORDER BY T.NUM_EXEC) NEXT_EXEC,
										 LEAD(T.PAY_DATE)  OVER(ORDER BY T.NUM_EXEC, T.DOC_DATE,/* T.ID_DEBT,*/ T.PAY_DATE) NEXT_PAY,
										 LEAD(T.DOC_DATE) OVER(ORDER BY T.NUM_EXEC, T.DOC_DATE, /*T.ID_DEBT,*/ T.PAY_DATE) NEXT_DOC
							FROM
							(SELECT T.NUM_EXEC, 							      
										 TO_CHAR(FD.DOC_DATE,'FMMonth YYYY') DOC_DATE_CHAR,
										 SUM(PD.SUM_PAY) SUM_PAY,
										 SUM(PD.PENY_SUM) PENY_SUM,    
										 FD.DOC_DATE,
										 T.COURT_TOLL,                             
								--		 RD.ID_DEBT,   
										 T.COURT_PENY,
					     
										 SUM(RP.PENY_SUM) PAY_PENY,
										 RP.PAY_DATE,
										 rp.id id_pay
							FROM T_CLI_DEBTS RD 
									,T_CLI_FINDOCS    FD   
									,T_CLI_REST_PAY_DET PD              
									,T_CLI_REST_PAYMENTS RP
									,(SELECT D1.ID_DOC, ex.num_exec, ex.court_toll, ex.court_peny
											FROM  T_CLI_DEBTS D1,
														T_CLI_CASES S, 
														(SELECT EX.ID_CASE, EX.NUM_EXEC, ex.court_peny, EX.COURT_TOLL
														FROM  T_CLI_COURT_EXEC EX
														LEFT OUTER JOIN T_CLI_COURT_EXEC EX2
														ON (EX.ID_CASE = EX2.ID_CASE AND EX.DATE_END < EX2.DATE_END)            
														WHERE EX2.ID IS NULL) EX            
											WHERE S.ID_CASE = D1.ID_WORK
											AND   EX.ID_CASE = S.ID_CASE) T         
							WHERE 1=1
							AND   RD.ID_DOC = FD.ID_DOC                  
							AND   RD.ID_WORK = P_ID_REST
							AND   RP.ID = PD.ID_PAY
							AND   FD.ID_DOC = T.ID_DOC(+)
							AND   PD.ID_DEBT = RD.ID_DEBT      
							GROUP BY T.NUM_EXEC,FD.DOC_DATE,T.COURT_TOLL,T.COURT_PENY,RP.PAY_DATE,rp.id 
							ORDER BY T.NUM_EXEC, DOC_DATE, /*RD.ID_DEBT,*/ PAY_DATE)T)
		LOOP 
			L_RET := L_RET||L_ROW;
			
			IF I.NUM_EXEC = nvl(I.NEXT_EXEC,I.NUM_EXEC) AND NOT L_CASE_MRG  THEN
 		    L_RET := REPLACE(L_RET,'MERGECASE','\clvmgf');            
				L_CASE_MRG := TRUE;                                       
				L_FIRST_ROW_CASE := TRUE;
			ELSIF I.NUM_EXEC = nvl(I.NEXT_EXEC,I.NUM_EXEC) AND L_CASE_MRG THEN
		    L_RET := REPLACE(L_RET,'MERGECASE','\clvmrg');						
			ELSIF (I.NUM_EXEC != I.NEXT_EXEC OR I.NEXT_EXEC IS NULL) AND L_CASE_MRG  THEN
		    L_RET := REPLACE(L_RET,'MERGECASE','\clvmrg');														                        
				L_CASE_MRG := FALSE;						
			ELSE
				L_RET := REPLACE(L_RET,'MERGECASE','');		
			END IF;
				
				
			IF I.DOC_DATE = I.NEXT_DOC AND NOT L_DOC_MRG THEN
		    L_RET := REPLACE(L_RET,'MERGEDOC','\clvmgf');            
				L_DOC_MRG := TRUE;
			ELSIF I.DOC_DATE = I.NEXT_DOC AND L_DOC_MRG THEN
		    L_RET := REPLACE(L_RET,'MERGEDOC','\clvmrg');						
			ELSIF (I.DOC_DATE != I.NEXT_DOC OR I.NEXT_DOC IS NULL) AND L_DOC_MRG THEN
		    L_RET := REPLACE(L_RET,'MERGEDOC','\clvmrg');														                        
				L_DOC_MRG := FALSE;						
			ELSE
				L_RET := REPLACE(L_RET,'MERGEDOC','');		
			END IF;
		  
			IF I.PAY_DATE = NVL(I.NEXT_PAY,I.PAY_DATE) AND NOT L_PAY_MRG THEN 
				
			  L_CNT := L_CNT + 1;
			 
			  WITH X AS (SELECT DISTINCT P.PAY_DATE, P.PAY_SUM, TE.court_peny, TE.COURT_TOLL, TE.ID_EXEC, TE.NUM_EXEC,P.ID
														FROM T_CLI_REST_PAYMENTS P,  
																 T_CLI_REST_PAY_DET PD,
																 T_CLI_DEBTS CD,
																 (SELECT D1.ID_DOC, ex.court_toll, ex.court_peny, EX.ID_EXEC, EX.NUM_EXEC
																	FROM  T_CLI_DEBTS D1,
																				T_CLI_CASES S, 
																				(SELECT EX.ID_CASE, EX.NUM_EXEC, ex.court_peny, EX.COURT_TOLL, EX.ID ID_EXEC
																				FROM  T_CLI_COURT_EXEC EX
																				LEFT OUTER JOIN T_CLI_COURT_EXEC EX2
																				ON (EX.ID_CASE = EX2.ID_CASE AND EX.DATE_END < EX2.DATE_END)            
																				WHERE EX2.ID IS NULL) EX            
																	WHERE S.ID_CASE = D1.ID_WORK
																	AND   EX.ID_CASE = S.ID_CASE) TE         
														WHERE P.ID_REST = P_ID_REST
														AND   P.ID = PD.ID_PAY     
														AND   PD.ID_DEBT = CD.ID_DEBT
														AND   TE.ID_DOC = CD.ID_DOC)
						SELECT SUM(T.COURT_TOLL), SUM(T.COURT_PENY) 
						INTO L_SUM_TOLL, L_SUM_PENY
						FROM X	T					                         				
						WHERE T.ID = I.ID_PAY
						AND NOT EXISTS(SELECT 1 FROM X T2 WHERE T2.ID_EXEC = T.ID_EXEC AND  T2.PAY_DATE < T.PAY_DATE);
			
				L_RET := REPLACE(L_RET,'MERGEPAY','\clvmgf');                                            
				L_PAY_MRG := TRUE;                            
				
				L_SUM_PAY := I.SUM_PAY + I.PENY_SUM + NVL(L_SUM_PENY,0)+NVL(L_SUM_TOLL,0);
							
				 			
			ELSIF I.PAY_DATE = NVL(I.NEXT_PAY,I.PAY_DATE) AND L_PAY_MRG THEN
				L_RET := REPLACE(L_RET,'MERGEPAY','\clvmrg');   
				L_SUM_PAY := L_SUM_PAY + I.SUM_PAY + I.PENY_SUM;
			ELSIF I.PAY_DATE != NVL(I.NEXT_PAY,I.PAY_DATE) AND L_PAY_MRG THEN					
			  L_RET := REPLACE(L_RET,'MERGEPAY','\clvmrg');                          
				L_SUM_PAY := L_SUM_PAY + I.SUM_PAY + I.PENY_SUM; --+ NVL(L_SUM_PENY,0)+NVL(L_SUM_TOLL,0);
				L_RET := REPLACE(L_RET,'PAYSUM',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(TO_CHAR(L_SUM_PAY,L_FMT,L_NLS)));  
				L_PAY_MRG := FALSE;			
			ELSE
				L_RET := REPLACE(L_RET,'MERGEPAY','');   
				L_SUM_PAY := I.SUM_PAY + I.PENY_SUM;                       				                             
			  L_CNT := L_CNT + 1;     
				L_RET := REPLACE(L_RET,'PAYSUM',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(TO_CHAR(L_SUM_PAY,L_FMT,L_NLS)));  
			END IF;      			
			
			
			
			L_RET := REPLACE(L_RET,'ROWNUM',TRIM(TO_CHAR(L_CNT)));   
			L_RET := REPLACE(L_RET,'NUMCASE',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(I.NUM_EXEC));  
			L_RET := REPLACE(L_RET,'PERIOD',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(I.DOC_DATE_CHAR));  
			L_RET := REPLACE(L_RET,'DOCDEBT',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(TO_CHAR(I.SUM_PAY,L_FMT,L_NLS)));
 		  L_RET := REPLACE(L_RET,'PENYDEBT',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(TO_CHAR(I.PENY_SUM,L_FMT,L_NLS)));    
			
			IF L_FIRST_ROW_CASE THEN
				L_RET := REPLACE(L_RET,'GOVTOLL',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(TO_CHAR(I.COURT_TOLL,L_FMT,L_NLS)));  
				L_RET := REPLACE(L_RET,'PENYCOURT',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(TO_CHAR(I.court_peny,L_FMT,L_NLS)));  
				L_FIRST_ROW_CASE := FALSE;                                                                                
				
			  L_TOT_TOLL := L_TOT_TOLL + I.COURT_TOLL;
			  L_TOT_PENY_COURT := L_TOT_PENY_COURT + I.COURT_PENY;       
				
						  
								
		  ELSE		                                                                                                    
				L_RET := REPLACE(L_RET,'GOVTOLL','');  
				L_RET := REPLACE(L_RET,'PENYCOURT','');  
			END IF;
			
							       
			
 			 /* L_RET := REPLACE(L_RET,'PAYSUM',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(TO_CHAR(I.SUM_PAY+I.PAY_PENY + NVL(L_SUM_PENY,0)+NVL(L_SUM_TOLL,0),L_FMT,L_NLS)));  */
			
			
			L_RET := REPLACE(L_RET,'PAYDATE',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(TO_CHAR(I.PAY_DATE,'DD.MM.YYYY')));  
			
			L_TOT_DEBT := L_TOT_DEBT + I.SUM_PAY;
			L_TOT_PENY := L_TOT_PENY + I.PENY_SUM;
		  
		END LOOP;			
    L_TOT_PAY := L_TOT_DEBT + NVL(L_TOT_PENY,0) + NVL(L_TOT_TOLL,0) + NVL(L_TOT_PENY_COURT,0);
		L_RET := REPLACE(L_RET,'PAYSUM',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(TO_CHAR(L_SUM_PAY,L_FMT,L_NLS)));  
		L_RET := L_RET||L_FOOTER;		
		L_RET := REPLACE(L_RET,'TOTDEBT',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(TO_CHAR(L_TOT_DEBT,L_FMT,L_NLS)));
	  L_RET := REPLACE(L_RET,'TOTPENY',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(TO_CHAR(L_TOT_PENY,L_FMT,L_NLS)));  
	  L_RET := REPLACE(L_RET,'TOTTOLL',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(TO_CHAR(L_TOT_TOLL,L_FMT,L_NLS)));  
	  L_RET := REPLACE(L_RET,'TOTCOURTPENY',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(TO_CHAR(L_TOT_PENY_COURT,L_FMT,L_NLS)));  		
	  L_RET := REPLACE(L_RET,'PAYTOT',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(TO_CHAR(L_TOT_PAY,L_FMT,L_NLS)));  		
		RETURN L_RET;					  
	END;
	
	PROCEDURE P_ADD_DATA(P_ID_REST NUMBER) IS
    L_BLOB BLOB;
		L_RTF  CLOB;  
    L_SQL  VARCHAR2(32767); 
		L_NLS     VARCHAR2(50) := 'NLS_NUMERIC_CHARACTERS='', ''';
		L_FMT     VARCHAR2(50) := 'FM999G999G999G999G990D00';

	BEGIN
	
		SELECT T.FILE_CONTENT INTO L_RTF FROM T_RTF_TEMPLATES T WHERE  T.ID = 24;
	  
		FOR I IN (SELECT  CLI.CLI_NAME CLINAME
										 ,CT.CTR_NUMBER CTRNUMBER
										 ,R.DEBT_DATE DEBTDATE
										 ,R.REST_DEBT RESTDEBT                           							 
										 ,R.PENY_CALC  										 
										 ,R.PENY_SUM     
										 ,(SELECT lower(pkg_utils.F_GET_NAME_MONTHS(MIN(FD.DOC_DATE),P_CASE => 'R'))||' '||to_char(MIN(FD.DOC_DATE),'YYYY"г"')||' по '
															||TO_CHAR(MAX(FD.DOC_DATE),'fmmonth YYYY"г"')
											 FROM T_CLI_DEBTS D,
														T_CLI_FINDOCS FD  
											 WHERE D.ID_WORK = R.ID_REST
											 AND   D.ID_DOC = FD.ID_DOC) PERIOD			
								FROM T_CLI_REST R,
										 T_CONTRACTS CT,
										 T_CLIENTS CLI										    										          
								WHERE R.ID_REST =  P_ID_REST
								AND   R.ID_CONTRACT = CT.ID_CONTRACT          
								AND   CT.ID_CLIENT = CLI.ID_CLIENT)
		LOOP
			L_RTF := REPLACE(L_RTF,'CLINAME',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(P_WORD => I.CLINAME));
			L_RTF := REPLACE(L_RTF,'CTRNUMBER',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(P_WORD => I.CTRNUMBER));
			L_RTF := REPLACE(L_RTF,'DEBTDATE',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(P_WORD => TO_CHAR(I.DEBTDATE,'DD.MM.YYYY')));
			L_RTF := REPLACE(L_RTF,'RESTDEBT',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(P_WORD => TO_CHAR(I.RESTDEBT,L_FMT,L_NLS)));
			L_RTF := REPLACE(L_RTF,'RESTPENY',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(P_WORD => TO_CHAR(I.PENY_SUM,L_FMT,L_NLS)));
		  L_RTF := REPLACE(L_RTF,'PERIOD',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(P_WORD => I.PERIOD));
      L_RTF := REPLACE(L_RTF,'ORGANIZATION',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(P_WORD => PKG_PREF.F$C2('REQUISITES')));
			
			FOR J IN (SELECT LISTAGG(ex.num_exec,', ') WITHIN GROUP (ORDER BY EX.NUM_EXEC) NUM_EXEC
										         ,SUM(ex.court_toll) COURT_TOLL, SUM(ex.court_peny) COURT_PENY
									FROM  T_CLI_CASES S, 												
												(SELECT EX.ID_CASE, EX.NUM_EXEC, ex.court_peny, EX.COURT_TOLL
												FROM  T_CLI_COURT_EXEC EX
												LEFT OUTER JOIN T_CLI_COURT_EXEC EX2
												ON (EX.ID_CASE = EX2.ID_CASE AND EX.DATE_END < EX2.DATE_END)            
												WHERE EX2.ID IS NULL) EX            
									WHERE EX.ID_CASE = S.ID_CASE
									AND EXISTS (SELECT 1 FROM T_CLI_DEBTS CD, T_CLI_DEBTS RD
									            WHERE CD.ID_WORK = S.ID_CASE
															AND   RD.ID_WORK = P_ID_REST
															AND   CD.ID_DOC = RD.ID_DOC))
			LOOP
	      	L_RTF := REPLACE(L_RTF,'NUMCOURTCASES',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(P_WORD => J.NUM_EXEC));
			    L_RTF := REPLACE(L_RTF,'COURTTOLL',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(P_WORD => TO_CHAR(J.COURT_TOLL,L_FMT,L_NLS)));
			    L_RTF := REPLACE(L_RTF,'PENYTOLL',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(P_WORD => TO_CHAR(J.COURT_PENY,L_FMT,L_NLS)));			
	    END LOOP;								
		END LOOP;						
		
		FOR J IN (SELECT NVL2(B.BIK,'БИК '||B.BIK,NULL) BIK
			              ,B.BANK_NAME
										,NVL2(B.COR_ACC,'к/сч '||B.COR_ACC,NULL) COR_ACC
										,NVL2(AC.ACC_NUMBER,'р/сч '||AC.ACC_NUMBER,NULL) ACC_NUMBER
			              ,PKG_CLIENTS.F_GET_ADDRESS(CT.ID_CLIENT) ADDRESS
										,NVL2(CLI.INN,'ИНН '||CLI.INN,NULL) INN
										,NVL2(CLI.KPP,'КПП '||CLI.KPP,NULL) KPP
							FROM T_SETL_ACCOUNTS AC,
							      T_CLIENTS CLI, 
										T_CONTRACTS CT , 
										T_CLI_REST R, 
										T_BANKS B
							WHERE AC.ID_CLIENT(+) = CLI.ID_CLIENT 
							AND   CT.ID_CLIENT = CLI.ID_CLIENT 
							AND   R.ID_CONTRACT = CT.ID_CONTRACT
							AND   B.ID_BANK(+) = AC.ID_BANK
							AND   R.ID_REST = P_ID_REST
							AND   ROWNUM = 1)
		LOOP         
	    L_RTF := REPLACE(L_RTF,'CLIADDRESS',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(P_WORD => J.ADDRESS));
			L_RTF := REPLACE(L_RTF,'CLIINN',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(P_WORD => J.INN));
			L_RTF := REPLACE(L_RTF,'CLIKPP',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(P_WORD => J.KPP));
			L_RTF := REPLACE(L_RTF,'CORACC',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(P_WORD => J.COR_ACC));
			L_RTF := REPLACE(L_RTF,'BANKBIK',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(P_WORD => J.BIK));
			L_RTF := REPLACE(L_RTF,'BANKNAME',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(P_WORD => J.BANK_NAME)); 
			L_RTF := REPLACE(L_RTF,'SETLACC',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(P_WORD => J.ACC_NUMBER));										  
		END LOOP;	
		
		FOR J IN (SELECT B.BIK, B.BANK_NAME, B.COR_ACC, A.ACC_NUMBER
              FROM T_CTR_JOURNAL J
              LEFT OUTER JOIN  T_CTR_JOURNAL J2
              ON J.ID_CONTRACT = J2.ID_CONTRACT AND J.JRNL_DATE < J2.JRNL_DATE
              INNER JOIN T_SETL_ACCOUNTS A ON A.ID_ACC = J.MAIN_ACC
              INNER JOIN T_BANKS B ON B.ID_BANK = A.ID_BANK
							INNER JOIN T_CLI_REST R ON (J.ID_CONTRACT = R.ID_CONTRACT)
              WHERE J2.ID IS NULL
							AND R.ID_REST = P_ID_REST)
		LOOP         
	    L_RTF := REPLACE(L_RTF,'ORGBIK',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(P_WORD => J.BIK));
			L_RTF := REPLACE(L_RTF,'ORGBANK',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(P_WORD => J.BANK_NAME));
			L_RTF := REPLACE(L_RTF,'ORGACC',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(P_WORD => J.ACC_NUMBER));
		END LOOP;									
		
							
		--PKG_DOC_REPORTS.P_REPLACE_SUBSTITUTION(P_SQL => L_SQL,L_CLOB => L_RTF);					
		
		L_RTF := PKG_DOC_REPORTS.REPLACE_CLOB(L_RTF,'TABLE',F_MAKE_TABLE(P_ID_REST));
			
		L_BLOB         := PKG_DOC_REPORTS.CLOB2BLOB(PAR_CLOB => L_RTF);
		RTF_DOC.P_BLOB := L_BLOB;
	
	END P_ADD_DATA;

	---------------------------------------------------------------------------------------------------------------------------------

	FUNCTION RUN_REPORT(P_PAR_01 VARCHAR2 DEFAULT NULL
										 ,P_PAR_02 VARCHAR2 DEFAULT NULL
										 ,P_PAR_03 VARCHAR2 DEFAULT NULL
										 ,P_PAR_04 VARCHAR2 DEFAULT NULL
										 ,P_PAR_05 VARCHAR2 DEFAULT NULL
										 ,P_PAR_06 VARCHAR2 DEFAULT NULL
										 ,P_PAR_07 VARCHAR2 DEFAULT NULL
										 ,P_PAR_08 VARCHAR2 DEFAULT NULL
										 ,P_PAR_09 VARCHAR2 DEFAULT NULL
										 ,P_PAR_10 VARCHAR2 DEFAULT NULL)
		RETURN LAWSUP.PKG_FILES.REC_DOC IS
		REC_DOC LAWSUP.PKG_FILES.REC_DOC;
	BEGIN
	
		RTF_DOC := NULL;
		P_ADD_DATA(P_PAR_01);
		REC_DOC.P_BLOB      := RTF_DOC.P_BLOB;
		REC_DOC.P_FILE_NAME := 'RESTRUCTURIZACIYA_' || P_PAR_01;
	
		RETURN REC_DOC;
	
	END RUN_REPORT;

END PKG_DOC_014;
/

