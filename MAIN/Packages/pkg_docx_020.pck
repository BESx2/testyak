i�?create or replace package lawmain.PKG_DOCX_020 is

  -- Author  : EUGEN
  -- Created : 01.04.2020 15:02:04
  -- Purpose : Уведомление органам
	-- CODE    : INFORM_GOV
  
  FUNCTION RUN_REPORT(P_PAR_01  IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_02 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_03 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_04 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_05 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_06 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_07 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_08 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_09 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_10 IN VARCHAR2 DEFAULT NULL) RETURN LAWSUP.PKG_FILES.REC_DOC;		                           

end PKG_DOCX_020;
/

create or replace package body lawmain.PKG_DOCX_020 is

  FUNCTION RUN_REPORT(P_PAR_01  IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_02 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_03 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_04 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_05 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_06 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_07 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_08 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_09 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_10 IN VARCHAR2 DEFAULT NULL) RETURN LAWSUP.PKG_FILES.REC_DOC
   IS  
   L_XML XMLTYPE;                                        
	 L_TAB XMLTYPE;
   L_SQL SYS_REFCURSOR;     
   L_REP LAWSUP.PKG_FILES.REC_DOC;
  BEGIN  						
    OPEN L_SQL FOR 
      SELECT PKG_PREF.F$C2('ORG_MAIN_FIO') "ген_дир",
			       TO_CHAR(SYSDATE,'"«"DD"»"')||' '||LOWER(PKG_UTILS.F_GET_NAME_MONTHS(SYSDATE,'R'))||' '||TO_CHAR(SYSDATE,'YYYY" г."') "текущая_дата",
						 INITCAP(L.LAST_NAME||' '||SUBSTR(L.FIRST_NAME,1,1)||'. '||SUBSTR(L.SECOND_NAME,1,1)||'.') "исполнитель",
						 L.PHONE "телефон",
						 DECODE(F.TYPE_CODE,'PT_SHUTDOWN_ALL','холодного водоснабжения','водоотведения') "услуга",
						 DECODE(F.TYPE_CODE,'PT_SHUTDOWN_ALL','true','false') "если_хвс",
 						 DECODE(F.TYPE_CODE,'PT_SHUTDOWN_SEW','true','false') "если_во",
 						 DECODE(F.TYPE_CODE,'PT_SHUTDOWN_ALL','об ограничении (прекращении) холодного водоснабжения','о прекращении водоотведения') "тип_уведомления",
 						 upper(DECODE(F.TYPE_CODE,'PT_SHUTDOWN_ALL','об ограничении (прекращении) холодного водоснабжения','о прекращении водоотведения')) "заголовок",
						 DECODE(F.TYPE_CODE,'PT_SHUTDOWN_ALL','ограничении','прекращении') "мера"
			FROM T_USER_LIST L,
			     V_FOLDERS F
			WHERE L.ID = F$_USR_ID
			AND   F.ID_FOLDER = P_PAR_01;             
		
		--ТАБЛИЧНАЯ ЧАСТЬ ОТЧЕТА
     SELECT XMLELEMENT("rows", 
            XMLAGG(XMLELEMENT("row", 
        			XMLELEMENT("cell", XMLATTRIBUTES('пункт' AS "tag"), ROWNUM), 
							XMLELEMENT("cell", XMLATTRIBUTES('абонент' AS "tag"), s.CLI_ALT_NAME),
						  XMLELEMENT("cell", XMLATTRIBUTES('адрес' AS "tag"), PKG_CLI_SHUTDOWN.F_GET_OBJECT_LIST(S.ID_WORK,10,'Y'))
							)))
		INTO   L_TAB
		FROM V_CLI_SHUTDOWN S,
		    APEX_COLLECTIONS C 
		WHERE C.collection_name = 'SHUT_IDS'
		AND   s.ID_WORK = TO_NUMBER(C.c001);
		
		L_TAB := XMLTYPE('<tables><table name="таблица1"></table></tables>').appendChildXML('/tables/table',l_tab,NULL);			
    L_XML := PKG_CONTROLLER.F_DOCX_XML(P_SQL => L_SQL,P_REP_CODE => 'INFORM_GOV');    		                          
		L_XML := L_XML.APPENDCHILDXML('/request/input-data',l_tab,NULL);
		
    L_REP.P_BLOB := PKG_CONTROLLER.F_GET_REPORT_XML(L_XML.getClobVal());
    L_REP.P_FILE_NAME := 'Уведомление органам';
		
		RETURN l_rep;
	END;										
	
end PKG_DOCX_020;
/

