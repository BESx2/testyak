i�?create or replace package lawmain.PKG_PREF is

  -- Created : 6/19/2013 6:49:44 PM
  -- Purpose :

	FUNCTION F_GET_FILE_NAME(P_FILE_NAME IN VARCHAR2) RETURN VARCHAR2;

	FUNCTION F_INSERT_PARAM(p_id_fk NUMBER
													,p_code VARCHAR2
													,p_name VARCHAR2
													,p_id_object NUMBER
													,p_num_order NUMBER DEFAULT NULL
													,p_html_text VARCHAR2 DEFAULT NULL
													,p_desc1  VARCHAR2 DEFAULT NULL
													,p_c1     VARCHAR2 DEFAULT NULL
													,p_n1 NUMBER DEFAULT NULL
													,p_d1 VARCHAR2 DEFAULT NULL
													,p_desc2  VARCHAR2 DEFAULT NULL
													,p_c2  VARCHAR2 DEFAULT NULL
													,p_n2 NUMBER DEFAULT NULL
													,p_d2 VARCHAR2 DEFAULT NULL
													,p_desc3  VARCHAR2 DEFAULT NULL
													,p_c3  VARCHAR2 DEFAULT NULL
													,p_n3 NUMBER DEFAULT NULL
													,p_d3 VARCHAR2 DEFAULT NULL
													,p_desc4  VARCHAR2 DEFAULT NULL
													,p_c4  VARCHAR2 DEFAULT NULL
													,p_n4 NUMBER DEFAULT NULL
													,p_d4 VARCHAR2 DEFAULT NULL
													,p_desc5  VARCHAR2 DEFAULT NULL
													,p_c5  VARCHAR2 DEFAULT NULL
													,p_n5 NUMBER DEFAULT NULL
													,p_d5 VARCHAR2 DEFAULT NULL
													,p_desc6  VARCHAR2 DEFAULT NULL
													,p_c6  VARCHAR2 DEFAULT NULL
													,p_n6 NUMBER DEFAULT NULL
													,p_d6 VARCHAR2 DEFAULT NULL
													,p_desc7  VARCHAR2 DEFAULT NULL
													,p_c7  VARCHAR2 DEFAULT NULL
													,p_n7 NUMBER DEFAULT NULL
													,p_d7 VARCHAR2 DEFAULT NULL
													,p_desc8   VARCHAR2 DEFAULT NULL
													,p_c8  VARCHAR2 DEFAULT NULL
													,p_n8 NUMBER DEFAULT NULL
													,p_d8 VARCHAR2 DEFAULT NULL
													,p_desc9  VARCHAR2 DEFAULT NULL
													,p_c9  VARCHAR2 DEFAULT NULL
													,p_n9 NUMBER DEFAULT NULL
													,p_d9 VARCHAR2 DEFAULT NULL
													,p_desc10  VARCHAR2 DEFAULT NULL
													,p_c10  VARCHAR2 DEFAULT NULL
													,p_n10 NUMBER DEFAULT NULL
													,p_d10 VARCHAR2 DEFAULT NULL
													,p_html_text_header VARCHAR2 DEFAULT NULL
													,p_html_text_footer VARCHAR2 DEFAULT NULL
													,p_html_text2 VARCHAR2 DEFAULT NULL
													,p_html_text3 VARCHAR2 DEFAULT NULL
													,p_html_text4 VARCHAR2 DEFAULT NULL
													,p_html_text5 VARCHAR2 DEFAULT NULL
													,p_html_text6 VARCHAR2 DEFAULT NULL
													,p_html_text7 VARCHAR2 DEFAULT NULL
													,p_html_text8 VARCHAR2 DEFAULT NULL
													,p_html_text9 VARCHAR2 DEFAULT NULL
													,p_html_text10 VARCHAR2 DEFAULT NULL
													,p_DESC11to20 VARCHAR2 DEFAULT NULL
													,p_DESC21to30 VARCHAR2 DEFAULT NULL
													,p_c11 VARCHAR2 DEFAULT NULL
													,p_c12 VARCHAR2 DEFAULT NULL
													,p_c13 VARCHAR2 DEFAULT NULL
													,p_c14 VARCHAR2 DEFAULT NULL
													,p_c15 VARCHAR2 DEFAULT NULL
													,p_c16 VARCHAR2 DEFAULT NULL
													,p_c17 VARCHAR2 DEFAULT NULL
													,p_c18 VARCHAR2 DEFAULT NULL
													,p_c19 VARCHAR2 DEFAULT NULL
													,p_c20 VARCHAR2 DEFAULT NULL
													,p_c21 VARCHAR2 DEFAULT NULL
													,p_c22 VARCHAR2 DEFAULT NULL
													,p_c23 VARCHAR2 DEFAULT NULL
													,p_c24 VARCHAR2 DEFAULT NULL
													,p_c25 VARCHAR2 DEFAULT NULL
													,p_c26 VARCHAR2 DEFAULT NULL
													,p_c27 VARCHAR2 DEFAULT NULL
													,p_c28 VARCHAR2 DEFAULT NULL
													,p_c29 VARCHAR2 DEFAULT NULL
													,p_c30 VARCHAR2 DEFAULT NULL
													,p_n11 NUMBER DEFAULT NULL
													,p_n12 NUMBER DEFAULT NULL
													,p_n13 NUMBER DEFAULT NULL
													,p_n14 NUMBER DEFAULT NULL
													,p_n15 NUMBER DEFAULT NULL
													,p_n16 NUMBER DEFAULT NULL
													,p_n17 NUMBER DEFAULT NULL
													,p_n18 NUMBER DEFAULT NULL
													,p_n19 NUMBER DEFAULT NULL
													,p_n20 NUMBER DEFAULT NULL
													,p_n21 NUMBER DEFAULT NULL
													,p_n22 NUMBER DEFAULT NULL
													,p_n23 NUMBER DEFAULT NULL
													,p_n24 NUMBER DEFAULT NULL
													,p_n25 NUMBER DEFAULT NULL
													,p_n26 NUMBER DEFAULT NULL
													,p_n27 NUMBER DEFAULT NULL
													,p_n28 NUMBER DEFAULT NULL
													,p_n29 NUMBER DEFAULT NULL
													,p_n30 NUMBER DEFAULT NULL
													,P_ID  NUMBER DEFAULT NULL
													,P_USER VARCHAR2 DEFAULT NULL
													) RETURN NUMBER;

	PROCEDURE P_UPDATE_PARAM(p_id NUMBER
													,p_id_fk NUMBER
													,p_code VARCHAR2
													,p_name VARCHAR2
													,p_id_object NUMBER
													,p_num_order NUMBER DEFAULT NULL
													,p_html_text VARCHAR2 DEFAULT NULL
													,p_desc1  VARCHAR2 DEFAULT NULL
													,p_c1     VARCHAR2 DEFAULT NULL
													,p_n1 NUMBER DEFAULT NULL
													,p_d1 VARCHAR2 DEFAULT NULL
													,p_desc2  VARCHAR2 DEFAULT NULL
													,p_c2  VARCHAR2 DEFAULT NULL
													,p_n2 NUMBER DEFAULT NULL
													,p_d2 VARCHAR2 DEFAULT NULL
													,p_desc3  VARCHAR2 DEFAULT NULL
													,p_c3  VARCHAR2 DEFAULT NULL
													,p_n3 NUMBER DEFAULT NULL
													,p_d3 VARCHAR2 DEFAULT NULL
													,p_desc4  VARCHAR2 DEFAULT NULL
													,p_c4  VARCHAR2 DEFAULT NULL
													,p_n4 NUMBER DEFAULT NULL
													,p_d4 VARCHAR2 DEFAULT NULL
													,p_desc5  VARCHAR2 DEFAULT NULL
													,p_c5  VARCHAR2 DEFAULT NULL
													,p_n5 NUMBER DEFAULT NULL
													,p_d5 VARCHAR2 DEFAULT NULL
													,p_desc6  VARCHAR2 DEFAULT NULL
													,p_c6  VARCHAR2 DEFAULT NULL
													,p_n6 NUMBER DEFAULT NULL
													,p_d6 VARCHAR2 DEFAULT NULL
													,p_desc7  VARCHAR2 DEFAULT NULL
													,p_c7  VARCHAR2 DEFAULT NULL
													,p_n7 NUMBER DEFAULT NULL
													,p_d7 VARCHAR2 DEFAULT NULL
													,p_desc8   VARCHAR2 DEFAULT NULL
													,p_c8  VARCHAR2 DEFAULT NULL
													,p_n8 NUMBER DEFAULT NULL
													,p_d8 VARCHAR2 DEFAULT NULL
													,p_desc9  VARCHAR2 DEFAULT NULL
													,p_c9  VARCHAR2 DEFAULT NULL
													,p_n9 NUMBER DEFAULT NULL
													,p_d9 VARCHAR2 DEFAULT NULL
													,p_desc10  VARCHAR2 DEFAULT NULL
													,p_c10  VARCHAR2 DEFAULT NULL
													,p_n10 NUMBER DEFAULT NULL
													,p_d10 VARCHAR2 DEFAULT NULL
													,p_html_text_header VARCHAR2 DEFAULT NULL
													,p_html_text_footer VARCHAR2 DEFAULT NULL
													,p_html_text2 VARCHAR2 DEFAULT NULL
													,p_html_text3 VARCHAR2 DEFAULT NULL
													,p_html_text4 VARCHAR2 DEFAULT NULL
													,p_html_text5 VARCHAR2 DEFAULT NULL
													,p_html_text6 VARCHAR2 DEFAULT NULL
													,p_html_text7 VARCHAR2 DEFAULT NULL
													,p_html_text8 VARCHAR2 DEFAULT NULL
													,p_html_text9 VARCHAR2 DEFAULT NULL
													,p_html_text10 VARCHAR2 DEFAULT NULL
													,p_DESC11to20 VARCHAR2 DEFAULT NULL
													,p_DESC21to30 VARCHAR2 DEFAULT NULL
													,p_c11 VARCHAR2 DEFAULT NULL
													,p_c12 VARCHAR2 DEFAULT NULL
													,p_c13 VARCHAR2 DEFAULT NULL
													,p_c14 VARCHAR2 DEFAULT NULL
													,p_c15 VARCHAR2 DEFAULT NULL
													,p_c16 VARCHAR2 DEFAULT NULL
													,p_c17 VARCHAR2 DEFAULT NULL
													,p_c18 VARCHAR2 DEFAULT NULL
													,p_c19 VARCHAR2 DEFAULT NULL
													,p_c20 VARCHAR2 DEFAULT NULL
													,p_c21 VARCHAR2 DEFAULT NULL
													,p_c22 VARCHAR2 DEFAULT NULL
													,p_c23 VARCHAR2 DEFAULT NULL
													,p_c24 VARCHAR2 DEFAULT NULL
													,p_c25 VARCHAR2 DEFAULT NULL
													,p_c26 VARCHAR2 DEFAULT NULL
													,p_c27 VARCHAR2 DEFAULT NULL
													,p_c28 VARCHAR2 DEFAULT NULL
													,p_c29 VARCHAR2 DEFAULT NULL
													,p_c30 VARCHAR2 DEFAULT NULL
													,p_n11 NUMBER DEFAULT NULL
													,p_n12 NUMBER DEFAULT NULL
													,p_n13 NUMBER DEFAULT NULL
													,p_n14 NUMBER DEFAULT NULL
													,p_n15 NUMBER DEFAULT NULL
													,p_n16 NUMBER DEFAULT NULL
													,p_n17 NUMBER DEFAULT NULL
													,p_n18 NUMBER DEFAULT NULL
													,p_n19 NUMBER DEFAULT NULL
													,p_n20 NUMBER DEFAULT NULL
													,p_n21 NUMBER DEFAULT NULL
													,p_n22 NUMBER DEFAULT NULL
													,p_n23 NUMBER DEFAULT NULL
													,p_n24 NUMBER DEFAULT NULL
													,p_n25 NUMBER DEFAULT NULL
													,p_n26 NUMBER DEFAULT NULL
													,p_n27 NUMBER DEFAULT NULL
													,p_n28 NUMBER DEFAULT NULL
													,p_n29 NUMBER DEFAULT NULL
													,p_n30 NUMBER DEFAULT NULL
													);

	PROCEDURE P_DEL_NODE(P_ID NUMBER);

	PROCEDURE P_PRINT_TREE;

	FUNCTION F_GET_PARAM(P_CODE IN VARCHAR2) RETURN T_USER_PARAMETER%ROWTYPE;
	FUNCTION F_GET_PARAM_BY_ID(P_ID IN NUMBER) RETURN T_USER_PARAMETER%ROWTYPE;

	FUNCTION F$NAME(P_CODE IN VARCHAR2) RETURN VARCHAR2;

	FUNCTION F$C1(P_CODE IN VARCHAR2) RETURN VARCHAR2;
	FUNCTION F$C2(P_CODE IN VARCHAR2) RETURN VARCHAR2;
	FUNCTION F$C3(P_CODE IN VARCHAR2) RETURN VARCHAR2;
	FUNCTION F$C4(P_CODE IN VARCHAR2) RETURN VARCHAR2;
	FUNCTION F$C5(P_CODE IN VARCHAR2) RETURN VARCHAR2;
	FUNCTION F$C6(P_CODE IN VARCHAR2) RETURN VARCHAR2;
	FUNCTION F$C7(P_CODE IN VARCHAR2) RETURN VARCHAR2;
	FUNCTION F$C8(P_CODE IN VARCHAR2) RETURN VARCHAR2;
	FUNCTION F$C9(P_CODE IN VARCHAR2) RETURN VARCHAR2;
	FUNCTION F$C10(P_CODE IN VARCHAR2) RETURN VARCHAR2;

	FUNCTION F$N1(P_CODE IN VARCHAR2) RETURN VARCHAR2;
	FUNCTION F$N2(P_CODE IN VARCHAR2) RETURN VARCHAR2;
	FUNCTION F$N3(P_CODE IN VARCHAR2) RETURN VARCHAR2;
	FUNCTION F$N4(P_CODE IN VARCHAR2) RETURN VARCHAR2;
	FUNCTION F$N5(P_CODE IN VARCHAR2) RETURN VARCHAR2;
	FUNCTION F$N6(P_CODE IN VARCHAR2) RETURN VARCHAR2;
	FUNCTION F$N7(P_CODE IN VARCHAR2) RETURN VARCHAR2;
	FUNCTION F$N8(P_CODE IN VARCHAR2) RETURN VARCHAR2;
	FUNCTION F$N9(P_CODE IN VARCHAR2) RETURN VARCHAR2;
	FUNCTION F$N10(P_CODE IN VARCHAR2) RETURN VARCHAR2;

	FUNCTION F$D1(P_CODE IN VARCHAR2) RETURN VARCHAR2;
	FUNCTION F$D2(P_CODE IN VARCHAR2) RETURN VARCHAR2;
	FUNCTION F$D3(P_CODE IN VARCHAR2) RETURN VARCHAR2;
	FUNCTION F$D4(P_CODE IN VARCHAR2) RETURN VARCHAR2;
	FUNCTION F$D5(P_CODE IN VARCHAR2) RETURN VARCHAR2;
	FUNCTION F$D6(P_CODE IN VARCHAR2) RETURN VARCHAR2;
	FUNCTION F$D7(P_CODE IN VARCHAR2) RETURN VARCHAR2;
	FUNCTION F$D8(P_CODE IN VARCHAR2) RETURN VARCHAR2;
	FUNCTION F$D9(P_CODE IN VARCHAR2) RETURN VARCHAR2;
	FUNCTION F$D10(P_CODE IN VARCHAR2) RETURN VARCHAR2;

	FUNCTION F$DESC1(P_CODE IN VARCHAR2) RETURN VARCHAR2;
	FUNCTION F$DESC2(P_CODE IN VARCHAR2) RETURN VARCHAR2;
	FUNCTION F$DESC3(P_CODE IN VARCHAR2) RETURN VARCHAR2;
	FUNCTION F$DESC4(P_CODE IN VARCHAR2) RETURN VARCHAR2;
	FUNCTION F$DESC5(P_CODE IN VARCHAR2) RETURN VARCHAR2;
	FUNCTION F$DESC6(P_CODE IN VARCHAR2) RETURN VARCHAR2;
	FUNCTION F$DESC7(P_CODE IN VARCHAR2) RETURN VARCHAR2;
	FUNCTION F$DESC8(P_CODE IN VARCHAR2) RETURN VARCHAR2;
	FUNCTION F$DESC9(P_CODE IN VARCHAR2) RETURN VARCHAR2;
	FUNCTION F$DESC10(P_CODE IN VARCHAR2) RETURN VARCHAR2;

	FUNCTION F$BLOB(P_CODE IN VARCHAR2) RETURN BLOB;

	FUNCTION F$HTML(P_CODE IN VARCHAR2) RETURN VARCHAR2;
	FUNCTION F$HTML_H(P_CODE IN VARCHAR2) RETURN VARCHAR2;
	FUNCTION F$HTML_F(P_CODE IN VARCHAR2) RETURN VARCHAR2;

	FUNCTION F$ID(P_CODE IN VARCHAR2) RETURN NUMBER;

	FUNCTION F_GET_FIRST_CODE_IN_GROUP(P_CODE IN VARCHAR2) RETURN VARCHAR2;

	FUNCTION F_UPLOAD_FILE(P_ROOT_NODE_CODE VARCHAR2 DEFAULT NULL,P_TITLE VARCHAR2,P_FILE_PATH VARCHAR2) RETURN NUMBER;

	PROCEDURE P_DOWNLOAD_FILE (P_ID IN NUMBER);

	FUNCTION F$NUM(p VARCHAR,p_def NUMBER) RETURN NUMBER;

	FUNCTION F_GENERATE_PRINT_FORMS(P_PAR_01 VARCHAR2 DEFAULT NULL
	                                ,P_PAR_02 VARCHAR2  DEFAULT NULL
																	,P_PAR_03 VARCHAR2  DEFAULT NULL
																	,P_PAR_04 VARCHAR2  DEFAULT NULL
																	,P_PAR_05 VARCHAR2  DEFAULT NULL) RETURN VARCHAR2;

	PROCEDURE P_PRINT_NEWS;

end PKG_PREF;
/

create or replace package body lawmain.PKG_PREF is


	FUNCTION F_GET_FILE_NAME(P_FILE_NAME IN VARCHAR2) RETURN VARCHAR2
		IS
		L_RET VARCHAR2(32000);
	BEGIN
		IF INSTR (P_FILE_NAME, '/') > 0 THEN
			L_RET :=
			REPLACE (REPLACE (SUBSTR (P_FILE_NAME,
																							 INSTR (P_FILE_NAME,'/',-1) + 1
																							),
																			 CHR (10),
																			 NULL
																			),
															CHR (13),
															NULL
														 );
		ELSE
			L_RET :=
			REPLACE (REPLACE (SUBSTR (P_FILE_NAME,
																							 INSTR (P_FILE_NAME,'\',-1) + 1
																							),
																			 CHR (10),
																			 NULL
																			),
															CHR (13),
															NULL
														 );
		END IF;
		--L_RET := CONVERT(L_RET,'CL8MSWIN1251');
		RETURN L_RET;
	END;

	FUNCTION F_INSERT_PARAM(p_id_fk NUMBER
													,p_code VARCHAR2
													,p_name VARCHAR2
													,p_id_object NUMBER
													,p_num_order NUMBER DEFAULT NULL
													,p_html_text VARCHAR2 DEFAULT NULL
													,p_desc1  VARCHAR2 DEFAULT NULL
													,p_c1     VARCHAR2 DEFAULT NULL
													,p_n1 NUMBER DEFAULT NULL
													,p_d1 VARCHAR2 DEFAULT NULL
													,p_desc2  VARCHAR2 DEFAULT NULL
													,p_c2  VARCHAR2 DEFAULT NULL
													,p_n2 NUMBER DEFAULT NULL
													,p_d2 VARCHAR2 DEFAULT NULL
													,p_desc3  VARCHAR2 DEFAULT NULL
													,p_c3  VARCHAR2 DEFAULT NULL
													,p_n3 NUMBER DEFAULT NULL
													,p_d3 VARCHAR2 DEFAULT NULL
													,p_desc4  VARCHAR2 DEFAULT NULL
													,p_c4  VARCHAR2 DEFAULT NULL
													,p_n4 NUMBER DEFAULT NULL
													,p_d4 VARCHAR2 DEFAULT NULL
													,p_desc5  VARCHAR2 DEFAULT NULL
													,p_c5  VARCHAR2 DEFAULT NULL
													,p_n5 NUMBER DEFAULT NULL
													,p_d5 VARCHAR2 DEFAULT NULL
													,p_desc6  VARCHAR2 DEFAULT NULL
													,p_c6  VARCHAR2 DEFAULT NULL
													,p_n6 NUMBER DEFAULT NULL
													,p_d6 VARCHAR2 DEFAULT NULL
													,p_desc7  VARCHAR2 DEFAULT NULL
													,p_c7  VARCHAR2 DEFAULT NULL
													,p_n7 NUMBER DEFAULT NULL
													,p_d7 VARCHAR2 DEFAULT NULL
													,p_desc8   VARCHAR2 DEFAULT NULL
													,p_c8  VARCHAR2 DEFAULT NULL
													,p_n8 NUMBER DEFAULT NULL
													,p_d8 VARCHAR2 DEFAULT NULL
													,p_desc9  VARCHAR2 DEFAULT NULL
													,p_c9  VARCHAR2 DEFAULT NULL
													,p_n9 NUMBER DEFAULT NULL
													,p_d9 VARCHAR2 DEFAULT NULL
													,p_desc10  VARCHAR2 DEFAULT NULL
													,p_c10  VARCHAR2 DEFAULT NULL
													,p_n10 NUMBER DEFAULT NULL
													,p_d10 VARCHAR2 DEFAULT NULL
													,p_html_text_header VARCHAR2 DEFAULT NULL
													,p_html_text_footer VARCHAR2 DEFAULT NULL
													,p_html_text2 VARCHAR2 DEFAULT NULL
													,p_html_text3 VARCHAR2 DEFAULT NULL
													,p_html_text4 VARCHAR2 DEFAULT NULL
													,p_html_text5 VARCHAR2 DEFAULT NULL
													,p_html_text6 VARCHAR2 DEFAULT NULL
													,p_html_text7 VARCHAR2 DEFAULT NULL
													,p_html_text8 VARCHAR2 DEFAULT NULL
													,p_html_text9 VARCHAR2 DEFAULT NULL
													,p_html_text10 VARCHAR2 DEFAULT NULL
													,p_DESC11to20 VARCHAR2 DEFAULT NULL
													,p_DESC21to30 VARCHAR2 DEFAULT NULL
													,p_c11 VARCHAR2 DEFAULT NULL
													,p_c12 VARCHAR2 DEFAULT NULL
													,p_c13 VARCHAR2 DEFAULT NULL
													,p_c14 VARCHAR2 DEFAULT NULL
													,p_c15 VARCHAR2 DEFAULT NULL
													,p_c16 VARCHAR2 DEFAULT NULL
													,p_c17 VARCHAR2 DEFAULT NULL
													,p_c18 VARCHAR2 DEFAULT NULL
													,p_c19 VARCHAR2 DEFAULT NULL
													,p_c20 VARCHAR2 DEFAULT NULL
													,p_c21 VARCHAR2 DEFAULT NULL
													,p_c22 VARCHAR2 DEFAULT NULL
													,p_c23 VARCHAR2 DEFAULT NULL
													,p_c24 VARCHAR2 DEFAULT NULL
													,p_c25 VARCHAR2 DEFAULT NULL
													,p_c26 VARCHAR2 DEFAULT NULL
													,p_c27 VARCHAR2 DEFAULT NULL
													,p_c28 VARCHAR2 DEFAULT NULL
													,p_c29 VARCHAR2 DEFAULT NULL
													,p_c30 VARCHAR2 DEFAULT NULL
													,p_n11 NUMBER DEFAULT NULL
													,p_n12 NUMBER DEFAULT NULL
													,p_n13 NUMBER DEFAULT NULL
													,p_n14 NUMBER DEFAULT NULL
													,p_n15 NUMBER DEFAULT NULL
													,p_n16 NUMBER DEFAULT NULL
													,p_n17 NUMBER DEFAULT NULL
													,p_n18 NUMBER DEFAULT NULL
													,p_n19 NUMBER DEFAULT NULL
													,p_n20 NUMBER DEFAULT NULL
													,p_n21 NUMBER DEFAULT NULL
													,p_n22 NUMBER DEFAULT NULL
													,p_n23 NUMBER DEFAULT NULL
													,p_n24 NUMBER DEFAULT NULL
													,p_n25 NUMBER DEFAULT NULL
													,p_n26 NUMBER DEFAULT NULL
													,p_n27 NUMBER DEFAULT NULL
													,p_n28 NUMBER DEFAULT NULL
													,p_n29 NUMBER DEFAULT NULL
													,p_n30 NUMBER DEFAULT NULL
													,P_ID  NUMBER DEFAULT NULL
													,P_USER VARCHAR2 DEFAULT NULL
													) RETURN NUMBER
		IS
		l_id NUMBER;
	BEGIN
		INSERT INTO t_user_parameter(id,id_fk,code,NAME
																	,id_object
																	,num_order
																	,html_text
																	,created_by_id
																	,created_date
																	,modified_by_id
																	,modified_date
																	,desc1
																	,c1
																	,n1
																	,d1
																	,desc2
																	,c2
																	,n2
																	,d2
																	,desc3
																	,c3
																	,n3
																	,d3
																	,desc4
																	,c4
																	,n4
																	,d4
																	,desc5
																	,c5
																	,n5
																	,d5
																	,desc6
																	,c6
																	,n6
																	,d6
																	,desc7
																	,c7
																	,n7
																	,d7
																	,desc8
																	,c8
																	,n8
																	,d8
																	,desc9
																	,c9
																	,n9
																	,d9
																	,desc10
																	,c10
																	,n10
																	,d10
																	,html_text_header
																	,html_text_footer
																	,html_text2
																	,html_text3
																	,html_text4
																	,html_text5
																	,html_text6
																	,html_text7
																	,html_text8
																	,html_text9
																	,html_text10
																	,DESC11to20
																	,DESC21to30
																	,c11
																	,c12
																	,c13
																	,c14
																	,c15
																	,c16
																	,c17
																	,c18
																	,c19
																	,c20
																	,c21
																	,c22
																	,c23
																	,c24
																	,c25
																	,c26
																	,c27
																	,c28
																	,c29
																	,c30
																	,n11
																	,n12
																	,n13
																	,n14
																	,n15
																	,n16
																	,n17
																	,n18
																	,n19
																	,n20
																	,n21
																	,n22
																	,n23
																	,n24
																	,n25
																	,n26
																	,n27
																	,n28
																	,n29
																	,n30
																	)
		VALUES(	NVL(P_ID,seq_main.nextval)
						,p_id_fk
						,upper(TRIM(P_CODE))
						,replace(TRIM(P_NAME),'/','\')
						,p_id_object
						,p_num_order
						,p_html_text
						,DECODE(P_USER,NULL,f$_usr_id,PKG_USERS.F_GET_USER_ID_BY_USERNAME(P_USER))-- p_created_by_id
						,sysdate--p_created_date
						,null--p_modified_by_id
						,null--p_modified_date
						,trim(p_desc1)
						,trim(p_c1)
						,p_n1
						,to_date(p_d1,'dd.mm.yyyy hh24:mi:ss')
						,trim(p_desc2 )
						,trim(p_c2)
						,p_n2
						,to_date(p_d2,'dd.mm.yyyy hh24:mi:ss')
						,trim(p_desc3 )
						,trim(p_c3)
						,p_n3
						,to_date(p_d3,'dd.mm.yyyy hh24:mi:ss')
						,trim(p_desc4 )
						,trim(p_c4 )
						,p_n4
						,to_date(p_d4,'dd.mm.yyyy hh24:mi:ss')
						,trim(p_desc5 )
						,trim(p_c5)
						,p_n5
						,to_date(p_d5,'dd.mm.yyyy hh24:mi:ss')
						,trim(p_desc6 )
						,trim(p_c6)
						,p_n6
						,to_date(p_d6,'dd.mm.yyyy hh24:mi:ss')
						,trim(p_desc7)
						,trim(p_c7 )
						,p_n7
						,to_date(p_d7,'dd.mm.yyyy hh24:mi:ss')
						,trim(p_desc8 )
						,trim(p_c8)
						,p_n8
						,to_date(p_d8,'dd.mm.yyyy hh24:mi:ss')
						,trim(p_desc9 )
						,trim(p_c9)
						,p_n9
						,to_date(p_d9,'dd.mm.yyyy hh24:mi:ss')
						,trim(p_desc10)
						,trim(p_c10)
						,p_n10
						,to_date(p_d10,'dd.mm.yyyy hh24:mi:ss')
						,p_html_text_header
						,P_html_text_footer
						,p_html_text2
						,p_html_text3
						,p_html_text4
						,p_html_text5
						,p_html_text6
						,p_html_text7
						,p_html_text8
						,p_html_text9
						,p_html_text10
						,trim(p_DESC11to20)
						,trim(p_DESC21to30)
						,trim(p_c11)
						,trim(p_c12)
						,trim(p_c13)
						,trim(p_c14)
						,trim(p_c15)
						,trim(p_c16)
						,trim(p_c17)
						,trim(p_c18)
						,trim(p_c19)
						,trim(p_c20)
						,trim(p_c21)
						,trim(p_c22)
						,trim(p_c23)
						,trim(p_c24)
						,trim(p_c25)
						,trim(p_c26)
						,trim(p_c27)
						,trim(p_c28)
						,trim(p_c29)
						,trim(p_c30)
						,P_n11
						,P_n12
						,P_n13
						,P_n14
						,P_n15
						,P_n16
						,P_n17
						,P_n18
						,P_n19
						,P_n20
						,P_n21
						,P_n22
						,P_n23
						,P_n24
						,P_n25
						,P_n26
						,P_n27
						,P_n28
						,P_n29
						,P_n30
						) RETURNING id INTO l_id;
		RETURN l_id;
	END;

	PROCEDURE P_UPDATE_PARAM(p_id NUMBER
													,p_id_fk NUMBER
													,p_code VARCHAR2
													,p_name VARCHAR2
													,p_id_object NUMBER
													,p_num_order NUMBER DEFAULT NULL
													,p_html_text VARCHAR2 DEFAULT NULL
													,p_desc1  VARCHAR2 DEFAULT NULL
													,p_c1     VARCHAR2 DEFAULT NULL
													,p_n1 NUMBER DEFAULT NULL
													,p_d1 VARCHAR2 DEFAULT NULL
													,p_desc2  VARCHAR2 DEFAULT NULL
													,p_c2  VARCHAR2 DEFAULT NULL
													,p_n2 NUMBER DEFAULT NULL
													,p_d2 VARCHAR2 DEFAULT NULL
													,p_desc3  VARCHAR2 DEFAULT NULL
													,p_c3  VARCHAR2 DEFAULT NULL
													,p_n3 NUMBER DEFAULT NULL
													,p_d3 VARCHAR2 DEFAULT NULL
													,p_desc4  VARCHAR2 DEFAULT NULL
													,p_c4  VARCHAR2 DEFAULT NULL
													,p_n4 NUMBER DEFAULT NULL
													,p_d4 VARCHAR2 DEFAULT NULL
													,p_desc5  VARCHAR2 DEFAULT NULL
													,p_c5  VARCHAR2 DEFAULT NULL
													,p_n5 NUMBER DEFAULT NULL
													,p_d5 VARCHAR2 DEFAULT NULL
													,p_desc6  VARCHAR2 DEFAULT NULL
													,p_c6  VARCHAR2 DEFAULT NULL
													,p_n6 NUMBER DEFAULT NULL
													,p_d6 VARCHAR2 DEFAULT NULL
													,p_desc7  VARCHAR2 DEFAULT NULL
													,p_c7  VARCHAR2 DEFAULT NULL
													,p_n7 NUMBER DEFAULT NULL
													,p_d7 VARCHAR2 DEFAULT NULL
													,p_desc8   VARCHAR2 DEFAULT NULL
													,p_c8  VARCHAR2 DEFAULT NULL
													,p_n8 NUMBER DEFAULT NULL
													,p_d8 VARCHAR2 DEFAULT NULL
													,p_desc9  VARCHAR2 DEFAULT NULL
													,p_c9  VARCHAR2 DEFAULT NULL
													,p_n9 NUMBER DEFAULT NULL
													,p_d9 VARCHAR2 DEFAULT NULL
													,p_desc10  VARCHAR2 DEFAULT NULL
													,p_c10  VARCHAR2 DEFAULT NULL
													,p_n10 NUMBER DEFAULT NULL
													,p_d10 VARCHAR2 DEFAULT NULL
													,p_html_text_header VARCHAR2 DEFAULT NULL
													,p_html_text_footer VARCHAR2 DEFAULT NULL
													,p_html_text2 VARCHAR2 DEFAULT NULL
													,p_html_text3 VARCHAR2 DEFAULT NULL
													,p_html_text4 VARCHAR2 DEFAULT NULL
													,p_html_text5 VARCHAR2 DEFAULT NULL
													,p_html_text6 VARCHAR2 DEFAULT NULL
													,p_html_text7 VARCHAR2 DEFAULT NULL
													,p_html_text8 VARCHAR2 DEFAULT NULL
													,p_html_text9 VARCHAR2 DEFAULT NULL
													,p_html_text10 VARCHAR2 DEFAULT NULL
													,p_DESC11to20 VARCHAR2 DEFAULT NULL
													,p_DESC21to30 VARCHAR2 DEFAULT NULL
													,p_c11 VARCHAR2 DEFAULT NULL
													,p_c12 VARCHAR2 DEFAULT NULL
													,p_c13 VARCHAR2 DEFAULT NULL
													,p_c14 VARCHAR2 DEFAULT NULL
													,p_c15 VARCHAR2 DEFAULT NULL
													,p_c16 VARCHAR2 DEFAULT NULL
													,p_c17 VARCHAR2 DEFAULT NULL
													,p_c18 VARCHAR2 DEFAULT NULL
													,p_c19 VARCHAR2 DEFAULT NULL
													,p_c20 VARCHAR2 DEFAULT NULL
													,p_c21 VARCHAR2 DEFAULT NULL
													,p_c22 VARCHAR2 DEFAULT NULL
													,p_c23 VARCHAR2 DEFAULT NULL
													,p_c24 VARCHAR2 DEFAULT NULL
													,p_c25 VARCHAR2 DEFAULT NULL
													,p_c26 VARCHAR2 DEFAULT NULL
													,p_c27 VARCHAR2 DEFAULT NULL
													,p_c28 VARCHAR2 DEFAULT NULL
													,p_c29 VARCHAR2 DEFAULT NULL
													,p_c30 VARCHAR2 DEFAULT NULL
													,p_n11 NUMBER DEFAULT NULL
													,p_n12 NUMBER DEFAULT NULL
													,p_n13 NUMBER DEFAULT NULL
													,p_n14 NUMBER DEFAULT NULL
													,p_n15 NUMBER DEFAULT NULL
													,p_n16 NUMBER DEFAULT NULL
													,p_n17 NUMBER DEFAULT NULL
													,p_n18 NUMBER DEFAULT NULL
													,p_n19 NUMBER DEFAULT NULL
													,p_n20 NUMBER DEFAULT NULL
													,p_n21 NUMBER DEFAULT NULL
													,p_n22 NUMBER DEFAULT NULL
													,p_n23 NUMBER DEFAULT NULL
													,p_n24 NUMBER DEFAULT NULL
													,p_n25 NUMBER DEFAULT NULL
													,p_n26 NUMBER DEFAULT NULL
													,p_n27 NUMBER DEFAULT NULL
													,p_n28 NUMBER DEFAULT NULL
													,p_n29 NUMBER DEFAULT NULL
													,p_n30 NUMBER DEFAULT NULL
													)
		IS
	BEGIN
		UPDATE t_user_parameter s
			SET s.code = TRIM(upper(p_code))
					,s.name = replace(TRIM(P_NAME),'/','\')
					,id_object = P_id_object
					,num_order = p_num_order
					,html_text = p_html_text
					,desc1 = TRIM(p_desc1)
					,c1 = trim(p_c1)
					,n1 = p_n1
					,d1 = to_date(p_d1,'dd.mm.yyyy hh24:mi:ss')
					,desc2 = TRIM(p_desc2)
					,c2 = trim(p_c2)
					,n2 = p_n2
					,d2 = to_date(p_d2,'dd.mm.yyyy hh24:mi:ss')
					,desc3 = TRIM(p_desc3)
					,c3 = trim(p_c3)
					,n3 = p_n3
					,d3 = to_date(p_d3,'dd.mm.yyyy hh24:mi:ss')
					,desc4 = TRIM(p_desc4)
					,c4 = trim(p_c4)
					,n4 = p_n4
					,d4 = to_date(p_d4,'dd.mm.yyyy hh24:mi:ss')
					,desc5 = TRIM(p_desc5)
					,c5 = trim(p_c5)
					,n5 = p_n5
					,d5 = to_date(p_d5,'dd.mm.yyyy hh24:mi:ss')
					,desc6 = TRIM(p_desc6)
					,c6 = trim(p_c6)
					,n6 = p_n6
					,d6 = to_date(p_d6,'dd.mm.yyyy hh24:mi:ss')
					,desc7 = TRIM(p_desc7)
					,c7 = trim(p_c7)
					,n7 = p_n7
					,d7 = to_date(p_d7,'dd.mm.yyyy hh24:mi:ss')
					,desc8 = TRIM(p_desc8)
					,c8 = trim(p_c8)
					,n8 = p_n8
					,d8 = to_date(p_d8,'dd.mm.yyyy hh24:mi:ss')
					,desc9 = TRIM(p_desc9)
					,c9 = trim(p_c9)
					,n9 = p_n9
					,d9 = to_date(p_d9,'dd.mm.yyyy hh24:mi:ss')
					,desc10 = TRIM(p_desc10)
					,c10 = trim(p_c10)
					,n10 = p_n10
					,d10 = to_date(p_d10,'dd.mm.yyyy hh24:mi:ss')
					,html_text_header = p_html_text_header
					,html_text_footer = p_html_text_footer
					,html_text2 = p_html_text2
					,html_text3 = p_html_text3
					,html_text4 = p_html_text4
					,html_text5 = p_html_text5
					,html_text6 = p_html_text6
					,html_text7 = p_html_text7
					,html_text8 = p_html_text8
					,html_text9 = p_html_text9
					,html_text10 = p_html_text10
					,DESC11to20 = TRIM(p_DESC11to20)
					,DESC21to30 = TRIM(p_DESC21to30)
					,c11 = trim(p_c11)
					,c12 = trim(p_c12)
					,c13 = trim(p_c13)
					,c14 = trim(p_c14)
					,c15 = trim(p_c15)
					,c16 = trim(p_c16)
					,c17 = trim(p_c17)
					,c18 = trim(p_c18)
					,c19 = trim(p_c19)
					,c20 = trim(p_c20)
					,c21 = trim(p_c21)
					,c22 = trim(p_c22)
					,c23 = trim(p_c23)
					,c24 = trim(p_c24)
					,c25 = trim(p_c25)
					,c26 = trim(p_c26)
					,c27 = trim(p_c27)
					,c28 = trim(p_c28)
					,c29 = trim(p_c29)
					,c30 = trim(p_c30)
					,n11 = P_n11
					,n12 = P_n12
					,n13 = P_n13
					,n14 = P_n14
					,n15 = P_n15
					,n16 = P_n16
					,n17 = P_n17
					,n18 = P_n18
					,n19 = P_n19
					,n20 = P_n20
					,n21 = P_n21
					,n22 = P_n22
					,n23 = P_n23
					,n24 = P_n24
					,n25 = P_n25
					,n26 = P_n26
					,n27 = P_n27
					,n28 = P_n28
					,n29 = P_n29
					,n30 = P_n30
					,s.id_fk = p_id_fk
		WHERE s.id = p_id;
	END;

	PROCEDURE P_DEL_NODE(P_ID NUMBER)
		IS
	BEGIN
    FOR i IN (
								SELECT SYS_CONNECT_BY_PATH(NAME, '/') PATH
											, LEVEL
											,id
									FROM t_user_parameter  t
										START WITH ID = P_ID
										CONNECT BY PRIOR ID = id_fk
										FOR UPDATE NOWAIT
										ORDER BY LEVEL DESC
							)
		LOOP
			DELETE FROM t_user_parameter d WHERE d.id = i.id;
		END LOOP;

	--EXCEPTION
	--	WHEN OTHERS THEN
	--		RETURN SQLERRM;
	END;


	PROCEDURE P_PRINT_TREE
		IS
		t varchar2(32000);
		cnt NUMBER := 0;
	BEGIN
		IF SUBSTR(APEX_APPLICATION.G_x01,1,2) = '-1' THEN
			t :=   '[';
				FOR I IN
				(
						SELECT T.ID,
									 APEX_JAVASCRIPT.escape(
									 														substr(t.name,1,30)||decode(sign(length(t.name)-30),1,' ...','')
									 												) short_name
							FROM T_USER_PARAMETER T
						 WHERE t.code = 'PREF'
						 AND F$OBJ_ID_YN(t.ID_OBJECT) = 'Y'
						ORDER BY t.num_order,t.name
				)
				LOOP
					IF cnt = 1 THEN
						t := t || ',';
					END IF;
					t := t||'{"attr":{"id":"'||i.ID||'","timestemp":"'||to_char(SYSDATE,'mm:ss')||'"}
									,"data":{"title" : "'||i.short_name||'","timestemp":"'||to_char(SYSDATE,'mm:ss')||'"}
									,"state":"closed"
									}';
						cnt := 1;
					END LOOP;
				t := t || ']';
		ELSE
			t :=   '[';
			FOR i IN (
						SELECT T.ID,
									 APEX_JAVASCRIPT.escape(
									 														substr(t.name,1,30)||decode(sign(length(t.name)-30),1,' ...','')
									 												) short_name
									,(SELECT COUNT(1) FROM t_user_parameter ff WHERE ff.id_fk = t.id AND rownum < 2 ) cnt
							FROM T_USER_PARAMETER T
						 WHERE t.id_fk = TO_NUMBER(APEX_APPLICATION.G_x01)
						 AND F$OBJ_ID_YN(t.ID_OBJECT) = 'Y'
						 ORDER BY t.num_order,t.name
			)
			LOOP
				IF cnt = 1 THEN
					t := t || ',';
				END IF;
					t := t ||
								'{"attr":{"id":"'||i.ID||'","timestemp":"'||to_char(SYSDATE,'mm:ss')||'"}
									,"data":{"title" : "'||i.short_name||'","timestemp":"'||to_char(SYSDATE,'mm:ss')||'"}';
					IF i.cnt > 0 THEN
						t := t || '	,"state":"closed"
											';
					END IF;
					t := t ||'}';
				cnt := 1;
			END LOOP;
			t := t || ']';
		END IF;
		htp.prn(t);
	END;

	FUNCTION F_GET_PARAM(P_CODE IN VARCHAR2) RETURN T_USER_PARAMETER%ROWTYPE
		IS
		L$ T_USER_PARAMETER%ROWTYPE;
	BEGIN
		SELECT * INTO L$
			FROM t_User_Parameter d WHERE d.code = TRIM(UPPER(P_CODE));
		RETURN L$;
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			raise_application_error(-20000,'Параметр с кодом "'||UPPER(TRIM(P_CODE))||'" не найден.');
	END;

	FUNCTION F_GET_PARAM_BY_ID(P_ID IN NUMBER) RETURN T_USER_PARAMETER%ROWTYPE
		IS
		L$ T_USER_PARAMETER%ROWTYPE;
	BEGIN
		SELECT * INTO L$
			FROM t_User_Parameter d WHERE d.ID = P_ID;
		RETURN L$;
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			raise_application_error(-20000,'Параметр с Ид. "'||UPPER(TRIM(P_ID))||'" не найден.');
	END;

	FUNCTION F$NAME(P_CODE IN VARCHAR2) RETURN VARCHAR2 IS BEGIN RETURN F_GET_PARAM(P_CODE).NAME; 	END;

	FUNCTION F$C1(P_CODE IN VARCHAR2) RETURN VARCHAR2 IS BEGIN RETURN F_GET_PARAM(P_CODE).C1; 	END;
	FUNCTION F$C2(P_CODE IN VARCHAR2) RETURN VARCHAR2 IS BEGIN RETURN F_GET_PARAM(P_CODE).C2; 	END;
	FUNCTION F$C3(P_CODE IN VARCHAR2) RETURN VARCHAR2 IS BEGIN RETURN F_GET_PARAM(P_CODE).C3; 	END;
	FUNCTION F$C4(P_CODE IN VARCHAR2) RETURN VARCHAR2 IS BEGIN RETURN F_GET_PARAM(P_CODE).C4; 	END;
	FUNCTION F$C5(P_CODE IN VARCHAR2) RETURN VARCHAR2 IS BEGIN RETURN F_GET_PARAM(P_CODE).C5; 	END;
	FUNCTION F$C6(P_CODE IN VARCHAR2) RETURN VARCHAR2 IS BEGIN RETURN F_GET_PARAM(P_CODE).C6; 	END;
	FUNCTION F$C7(P_CODE IN VARCHAR2) RETURN VARCHAR2 IS BEGIN RETURN F_GET_PARAM(P_CODE).C7; 	END;
	FUNCTION F$C8(P_CODE IN VARCHAR2) RETURN VARCHAR2 IS BEGIN RETURN F_GET_PARAM(P_CODE).C8; 	END;
	FUNCTION F$C9(P_CODE IN VARCHAR2) RETURN VARCHAR2 IS BEGIN RETURN F_GET_PARAM(P_CODE).C9; 	END;
	FUNCTION F$C10(P_CODE IN VARCHAR2) RETURN VARCHAR2 IS BEGIN RETURN F_GET_PARAM(P_CODE).C10; 	END;

	FUNCTION F$N1(P_CODE IN VARCHAR2) RETURN VARCHAR2 IS BEGIN RETURN F_GET_PARAM(P_CODE).N1; 	END;
	FUNCTION F$N2(P_CODE IN VARCHAR2) RETURN VARCHAR2 IS BEGIN RETURN F_GET_PARAM(P_CODE).N2; 	END;
	FUNCTION F$N3(P_CODE IN VARCHAR2) RETURN VARCHAR2 IS BEGIN RETURN F_GET_PARAM(P_CODE).N3; 	END;
	FUNCTION F$N4(P_CODE IN VARCHAR2) RETURN VARCHAR2 IS BEGIN RETURN F_GET_PARAM(P_CODE).N4; 	END;
	FUNCTION F$N5(P_CODE IN VARCHAR2) RETURN VARCHAR2 IS BEGIN RETURN F_GET_PARAM(P_CODE).N5; 	END;
	FUNCTION F$N6(P_CODE IN VARCHAR2) RETURN VARCHAR2 IS BEGIN RETURN F_GET_PARAM(P_CODE).N6; 	END;
	FUNCTION F$N7(P_CODE IN VARCHAR2) RETURN VARCHAR2 IS BEGIN RETURN F_GET_PARAM(P_CODE).N7; 	END;
	FUNCTION F$N8(P_CODE IN VARCHAR2) RETURN VARCHAR2 IS BEGIN RETURN F_GET_PARAM(P_CODE).N8; 	END;
	FUNCTION F$N9(P_CODE IN VARCHAR2) RETURN VARCHAR2 IS BEGIN RETURN F_GET_PARAM(P_CODE).N9; 	END;
	FUNCTION F$N10(P_CODE IN VARCHAR2) RETURN VARCHAR2 IS BEGIN RETURN F_GET_PARAM(P_CODE).N10; 	END;

	FUNCTION F$D1(P_CODE IN VARCHAR2) RETURN VARCHAR2 IS BEGIN RETURN F_GET_PARAM(P_CODE).D1; 	END;
	FUNCTION F$D2(P_CODE IN VARCHAR2) RETURN VARCHAR2 IS BEGIN RETURN F_GET_PARAM(P_CODE).D2; 	END;
	FUNCTION F$D3(P_CODE IN VARCHAR2) RETURN VARCHAR2 IS BEGIN RETURN F_GET_PARAM(P_CODE).D3; 	END;
	FUNCTION F$D4(P_CODE IN VARCHAR2) RETURN VARCHAR2 IS BEGIN RETURN F_GET_PARAM(P_CODE).D4; 	END;
	FUNCTION F$D5(P_CODE IN VARCHAR2) RETURN VARCHAR2 IS BEGIN RETURN F_GET_PARAM(P_CODE).D5; 	END;
	FUNCTION F$D6(P_CODE IN VARCHAR2) RETURN VARCHAR2 IS BEGIN RETURN F_GET_PARAM(P_CODE).D6; 	END;
	FUNCTION F$D7(P_CODE IN VARCHAR2) RETURN VARCHAR2 IS BEGIN RETURN F_GET_PARAM(P_CODE).D7; 	END;
	FUNCTION F$D8(P_CODE IN VARCHAR2) RETURN VARCHAR2 IS BEGIN RETURN F_GET_PARAM(P_CODE).D8; 	END;
	FUNCTION F$D9(P_CODE IN VARCHAR2) RETURN VARCHAR2 IS BEGIN RETURN F_GET_PARAM(P_CODE).D9; 	END;
	FUNCTION F$D10(P_CODE IN VARCHAR2) RETURN VARCHAR2 IS BEGIN RETURN F_GET_PARAM(P_CODE).D10; 	END;

	FUNCTION F$DESC1(P_CODE IN VARCHAR2) RETURN VARCHAR2 IS BEGIN RETURN F_GET_PARAM(P_CODE).DESC1; 	END;
	FUNCTION F$DESC2(P_CODE IN VARCHAR2) RETURN VARCHAR2 IS BEGIN RETURN F_GET_PARAM(P_CODE).DESC2; 	END;
	FUNCTION F$DESC3(P_CODE IN VARCHAR2) RETURN VARCHAR2 IS BEGIN RETURN F_GET_PARAM(P_CODE).DESC3; 	END;
	FUNCTION F$DESC4(P_CODE IN VARCHAR2) RETURN VARCHAR2 IS BEGIN RETURN F_GET_PARAM(P_CODE).DESC4; 	END;
	FUNCTION F$DESC5(P_CODE IN VARCHAR2) RETURN VARCHAR2 IS BEGIN RETURN F_GET_PARAM(P_CODE).DESC5; 	END;
	FUNCTION F$DESC6(P_CODE IN VARCHAR2) RETURN VARCHAR2 IS BEGIN RETURN F_GET_PARAM(P_CODE).DESC6; 	END;
	FUNCTION F$DESC7(P_CODE IN VARCHAR2) RETURN VARCHAR2 IS BEGIN RETURN F_GET_PARAM(P_CODE).DESC7; 	END;
	FUNCTION F$DESC8(P_CODE IN VARCHAR2) RETURN VARCHAR2 IS BEGIN RETURN F_GET_PARAM(P_CODE).DESC8; 	END;
	FUNCTION F$DESC9(P_CODE IN VARCHAR2) RETURN VARCHAR2 IS BEGIN RETURN F_GET_PARAM(P_CODE).DESC9; 	END;
	FUNCTION F$DESC10(P_CODE IN VARCHAR2) RETURN VARCHAR2 IS BEGIN RETURN F_GET_PARAM(P_CODE).DESC10; 	END;

	FUNCTION F$BLOB(P_CODE IN VARCHAR2) RETURN BLOB IS BEGIN RETURN F_GET_PARAM(P_CODE).B_LOB; 	END;

	FUNCTION F$HTML(P_CODE IN VARCHAR2) RETURN VARCHAR2
		IS
		L$ t_User_Parameter%ROWTYPE;
	BEGIN
		l$ := F_GET_PARAM(P_CODE);
		RETURN 	L$.HTML_TEXT||
						L$.HTML_TEXT2||
						L$.HTML_TEXT3||
						L$.HTML_TEXT4||
						L$.HTML_TEXT5||
						L$.HTML_TEXT6||
						L$.HTML_TEXT7||
						L$.HTML_TEXT8||
						L$.HTML_TEXT9||
						L$.HTML_TEXT10;
	END;

	FUNCTION F$HTML_H(P_CODE IN VARCHAR2) RETURN VARCHAR2 IS BEGIN RETURN F_GET_PARAM(P_CODE).HTML_TEXT_HEADER ; 	END;
	FUNCTION F$HTML_F(P_CODE IN VARCHAR2) RETURN VARCHAR2 IS BEGIN RETURN F_GET_PARAM(P_CODE).HTML_TEXT_FOOTER; 	END;

	FUNCTION F$ID(P_CODE IN VARCHAR2) RETURN NUMBER IS BEGIN RETURN F_GET_PARAM(P_CODE).ID; 	END;

	FUNCTION F_GET_FIRST_CODE_IN_GROUP(P_CODE IN VARCHAR2) RETURN VARCHAR2
		IS
	BEGIN
		FOR i IN (SELECT * FROM t_user_parameter d WHERE d.id_fk = F$ID(P_CODE) ORDER BY NUM_ORDER) LOOP
			RETURN i.Code;
		END LOOP;

		RETURN NULL;
	END;

	FUNCTION F_UPLOAD_FILE(P_ROOT_NODE_CODE VARCHAR2 DEFAULT NULL,P_TITLE VARCHAR2,P_FILE_PATH VARCHAR2) RETURN NUMBER
		IS
		L_RET NUMBER;
		L_SIZE NUMBER;
	BEGIN 	L_RET :=
		pkg_pref.F_INSERT_PARAM(
															p_id_fk				=> PKG_PREF.F$ID(NVL(P_ROOT_NODE_CODE,'FILES'))
															,p_code				=> NVL(P_ROOT_NODE_CODE,'FILES_')||SEQ_MAIN.Nextval
															,p_name				=> NVL(P_ROOT_NODE_CODE,'FILES_')||SEQ_MAIN.CURRVAL
															,p_id_object 	=> PKG_OBJECTS.F_GET_OBJ_ID_BY_CODE('PAGE_1019')
															,p_c1					=> TRIM(P_TITLE)
		);
 
		UPDATE T_USER_PARAMETER d SET
		(
			d.name--d.filename
			,d.b_lob--d.blb
			,d.c2--d.mime_type
--			,d.n1--d.doc_size
--			,d.c3--d.dad_charset
--			,d.c4--d.file_type
--			,d.c5--d.file_charset
		)
		=
		(
			SELECT 	WWV.filename--WWV.name
							,WWV.blob_content
							,WWV.mime_type
							--, WWV.doc_size, WWV.dad_charset, WWV.file_type, WWV.file_charset
				FROM APEX_APPLICATION_TEMP_FILES WWV
			WHERE WWV.NAME  IN (P_FILE_PATH)
			AND ROWNUM = 1
		)
		WHERE id = L_RET;

	--	DELETE		FROM APEX_APPLICATION_TEMP_FILES WWV	WHERE WWV.NAME  IN (P_FILE_PATH)
	--	;

		/*SELECT dbms_lob.getlength(d.b_lob) INTO L_SIZE FROM t_user_parameter d WHERE id = L_RET;
 
		IF L_SIZE  > pkg_pref.F$N1(P_ROOT_NODE_CODE)*1024   THEN-- 500kb
			DELETE FROM t_user_parameter d WHERE id = L_RET;
      COMMIT;
			raise_application_error(-20200,'Размер файла должен быть меньше '||pkg_pref.F$N1(P_ROOT_NODE_CODE)||' kBytes! Размер загружаемого файла '||round(L_SIZE/1024,2)||' kBytes. Уменьшите размер файла!');
		END IF;      */

		RETURN l_ret;

	END;

	PROCEDURE P_DOWNLOAD_FILE (P_ID IN NUMBER)
	AS
		 V_MIME        VARCHAR2 (100);
		 V_LENGTH      NUMBER;
		 V_FILE_NAME   VARCHAR2 (2000);
		 L_FILE_NAME		VARCHAR2 (2000);
		 LOB_LOC       BLOB;
	BEGIN
		 SELECT d.c2,--f.MIME_TYPE,
						d.b_lob,--f.blb,
						d.name,--f.filename,
						d.n1--f.doc_size
			 INTO V_MIME,
						LOB_LOC,
						V_FILE_NAME,
						V_LENGTH
			 FROM T_USER_PARAMETER d
			WHERE ID = P_ID;
		 --
		 -- SET UP HTTP HEADER
		 --
					 -- USE AN NVL AROUND THE MIME TYPE AND
					 -- IF IT IS A NULL SET IT TO APPLICATION/OCTECT
					 -- APPLICATION/OCTECT MAY LAUNCH A DOWNLOAD WINDOW FROM WINDOWS

		 OWA_UTIL.MIME_HEADER (NVL (V_MIME, 'APPLICATION/OCTET'), FALSE);
		 OWA_UTIL.MIME_HEADER (NVL (V_MIME, 'charset=Windows-1251'), FALSE);

		 -- SET THE SIZE SO THE BROWSER KNOWS HOW MUCH TO DOWNLOAD
		 HTP.P ('CONTENT-LENGTH: ' || V_LENGTH);
		 -- THE FILENAME WILL BE USED BY THE BROWSER IF THE USERS DOES A SAVE AS
		 HTP.P (   'CONTENT-DISPOSITION:  ATTACHMENT; FILENAME="'
						|| F_GET_FILE_NAME(V_FILE_NAME)
						|| '"'
					 );
		 -- CLOSE THE HEADERS
		 OWA_UTIL.HTTP_HEADER_CLOSE;
		 -- DOWNLOAD THE BLOB
		 WPG_DOCLOAD.DOWNLOAD_FILE (LOB_LOC);

	END P_DOWNLOAD_FILE;

	FUNCTION F$NUM(p VARCHAR,p_def NUMBER) RETURN NUMBER
		IS
		L_NUM NUMBER;
	BEGIN
		IF P IS NULL THEN
			RETURN P_DEF;
		END IF;
		L_NUM := to_NUMBER(p);
		RETURN L_NUM;
	EXCEPTION
		WHEN OTHERS THEN
			RETURN P_DEF;
	END;

	FUNCTION F_GENERATE_PRINT_FORMS(P_PAR_01 VARCHAR2 DEFAULT NULL
	                                ,P_PAR_02 VARCHAR2 DEFAULT NULL
																	,P_PAR_03 VARCHAR2 DEFAULT NULL
																	,P_PAR_04 VARCHAR2 DEFAULT NULL
																	,P_PAR_05 VARCHAR2 DEFAULT NULL) RETURN VARCHAR2
	IS
	l_ret VARCHAR2(32000):='';
	l_cond VARCHAR2(2000);
	l_result VARCHAR2(1);
	BEGIN
	  l_ret:= 'SELECT NULL
		               ,p.name LABEL
									 ,''javascript:printPDF(''''''||p.code||'''''',$(''''#P1038_ID_CONTRACT'''').val(),''''''||p.c1||'''''')'' TARGET
									 FROM T_USER_PARAMETER p
									 WHERE CODE IN (';

		FOR i IN (
			SELECT p.code, p.c5 FROM t_user_parameter p
			WHERE p.id_fk = (SELECT id FROM t_user_parameter WHERE CODE = 'PRINT_FORMS')
      ORDER BY p.num_order
			    )
		LOOP
		   IF i.c5 IS NOT NULL THEN
			   l_cond:= REPLACE(i.c5,'#PAR1#',P_PAR_01);
				 l_cond:= REPLACE(l_cond,'#PAR2#',P_PAR_02);
				 l_cond:=	REPLACE(l_cond,'#PAR3#',P_PAR_03);
				 l_cond:= REPLACE(l_cond,'#PAR4#',P_PAR_04);
				 l_cond:= REPLACE(l_cond,'#PAR5#',P_PAR_05);

			   EXECUTE IMMEDIATE l_cond USING  OUT l_result;


				 IF l_result = 'Y' THEN
				  l_ret:=l_ret||''''||i.code||''',';
				 END IF;
			 ELSE
			    l_ret:=l_ret||''''||i.code||''',';
			 END IF;
		END LOOP;
		l_ret:= RTRIM(l_ret,',');

		l_ret:=l_ret||') AND f$obj_id_yn(p.id_object) = ''Y''
                     ORDER BY p.num_order';
		RETURN l_ret;
	END F_GENERATE_PRINT_FORMS;


	PROCEDURE P_PRINT_NEWS
	IS

	L_APP VARCHAR2(200);

	BEGIN

	L_APP :=  pkg_pref.F$c1('APP_CODE');

/*	htp.print('<div class="t-Region t-Region--noPadding t-Region--scrollBody" id="news_region" role="group" aria-labelledby="news_region_heading" aria-live="polite">
 <div class="t-Region-header">
  <div class="t-Region-headerItems t-Region-headerItems--title">
    <h2 class="t-Region-title" id="news_region_heading">Новости</h2>
  </div>
  <div class="t-Region-headerItems t-Region-headerItems--buttons"><span class="js-maximizeButtonContainer"></span></div>
 </div>
 <div class="t-Region-bodyWrap">
   <div class="t-Region-buttons t-Region-buttons--top">
    <div class="t-Region-buttons-left"></div>
    <div class="t-Region-buttons-right"></div>
   </div>
   <div class="t-Region-body">
     <div id="report_6688127836195319_catch"><div class="t-Report   t-Report--stretch t-Report--altRowsDefault t-Report--rowHighlight t-Report--noBorders" id="report_news_region">
  <div class="t-Report-wrap">
    <table class="t-Report-pagination" role="presentation"><tbody><tr><td></td></tr></tbody></table>
    <div class="t-Report-tableWrap">
    <table class="t-Report-report" summary="Новости">
<tbody><tr style="
    border-bottom: 1px solid #e6e6e6;"><td class="t-Report-cell" headers="ID">123769</td></tr>
<tr style="
    border-bottom: 1px solid #e6e6e6;"><td class="t-Report-cell" headers="ID">123770</td></tr>
      </tbody>
    </table>
    </div>
    <div class="t-Report-links"></div>
    <table class="t-Report-pagination t-Report-pagination--bottom" role="presentation"><tbody><tr><td colspan="4" align="right"><table role="presentation"><tbody><tr><td class="pagination"></td><td class="pagination"></td><td nowrap="nowrap" class="pagination"><span class="t-Report-paginationText">1 - 2</span></td><td class="pagination"></td><td class="pagination"></td></tr></tbody></table></td></tr></tbody></table>
  </div>
</div></div>
     
   </div>
   <div class="t-Region-buttons t-Region-buttons--bottom">
    <div class="t-Region-buttons-left"></div>
    <div class="t-Region-buttons-right"></div>
   </div>
 </div>
</div>');*/
	
	
	htp.print('<div class="t-Region t-Region--noPadding t-Region--scrollBody" id="news_region" role="group" aria-labelledby="news_region_heading" aria-live="polite">
 <div class="t-Region-header">
  <div class="t-Region-headerItems t-Region-headerItems--title">
    <h2 class="t-Region-title" id="news_region_heading">Новости</h2>
  </div>
  <div class="t-Region-headerItems t-Region-headerItems--buttons"><span class="js-maximizeButtonContainer"></span></div>
 </div>
 <div class="t-Region-bodyWrap">
   <div class="t-Region-buttons t-Region-buttons--top">
    <div class="t-Region-buttons-left"></div>
    <div class="t-Region-buttons-right"></div>
   </div>
   <div class="t-Region-body">
     <div id="report_6688127836195319_catch"><div class="t-Report   t-Report--stretch t-Report--altRowsDefault t-Report--rowHighlight t-Report--noBorders" id="report_news_region">
  <div class="t-Report-wrap">
    <table class="t-Report-pagination" role="presentation"><tbody><tr><td></td></tr></tbody></table>
    <div class="t-Report-tableWrap">
    <table class="t-Report-report" summary="Новости">
<tbody>');

		FOR i IN (Select A.*, rownum rw FROM (
							SELECT p.id, p.name, p.desc1, p.d1 FROM t_user_parameter p
								WHERE p.id_fk = PKG_PREF.F$ID(P_CODE => 'NEWS')
									AND p.c1 = 'Y'
									ORDER BY p.d1 DESC) A
							WHERE rownum <= 10)
		LOOP
     
			htp.print('<tr style="border-bottom: 1px solid #e6e6e6; color: #404040">
			<td class="t-Report-cell" headers="TEXT"><a href="'||apex_util.prepare_url('f?p='||L_APP||':2001:'||V('SESSION')||'::NO:0:P2001_ID:'||i.id)||'" style="text-decoration: none; color: #404040">
										<div style="color: #AAAAAA;">'||i.d1||'</div>
										<div>'||i.name||'</div>
									</a></td>
			</tr>');
		
		/*	htp.print('<tr>');
			htp.print('<td headers="TEXT">
									<a href="'||apex_util.prepare_url('f?p='||L_APP||':2001:'||V('SESSION')||'::NO:0:P2001_ID:'||i.id)||'" style="text-decoration: none;">
										<div style="color: #AAAAAA;">'||i.d1||'</div>
										<div>'||i.name||'</div>
									</a>
									</td>');
			htp.print('</tr>'); 
*/

			--<a href="f?p='||L_APP||':1052:'||V('SESSION')||'::NO:0:P1052_ID_MESSAGE:'||i.id||'">

		END LOOP;

	--<div style="color: #AAAAAA;">'||i.d1||'</div>
	--<div style="color: #343434;">'||i.desc1||'</div>


/*				htp.print('
      </tbody>
    </table>
    </div>
    <div class="t-Report-links"></div>
    <table class="t-Report-pagination t-Report-pagination--bottom" role="presentation"><tbody><tr><td colspan="4" align="right"><table role="presentation"><tbody><tr><td class="pagination"></td><td class="pagination"></td><td nowrap="nowrap" class="pagination"><span class="t-Report-paginationText">1 - 2</span></td><td class="pagination"></td><td class="pagination"></td></tr></tbody></table></td></tr></tbody></table>
  </div>
</div></div>
     
   </div>
   <div class="t-Region-buttons t-Region-buttons--bottom">
    <div class="t-Region-buttons-left"></div>
    <div class="t-Region-buttons-right"></div>
   </div>
 </div>
</div>');*/

	htp.print('</tbody>
    </table>
    </div>
    <div class="t-Report-links"></div>
    <table class="t-Report-pagination t-Report-pagination--bottom" role="presentation">
		<tbody>
			<tr>
				<td colspan="4" align="right">
					<table role="presentation">
						<tbody>
							<tr>
								<td class="pagination"></td>
								<td class="pagination"></td>
								<td nowrap="nowrap" class="pagination">
									<span class="t-Report-paginationText">1 - 2</span>
								</td>
								<td class="pagination">
									<a href="javascript:apex.widget.report.paginate(''news_region'', {min:3,max:2,fetched:2});" class="t-Button t-Button--small t-Button--noUI t-Report-paginationLink t-Report-paginationLink--next">Next<span class="a-Icon icon-right-arrow"></span></a>
								</td>
								<td class="pagination"></td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
		</tbody>
		</table>
  </div>
</div></div>
     
   </div>
   <div class="t-Region-buttons t-Region-buttons--bottom">
    <div class="t-Region-buttons-left"></div>
    <div class="t-Region-buttons-right"></div>
   </div>
 </div>
</div>');



	END;

end PKG_PREF;
/

