i�?CREATE OR REPLACE PACKAGE LAWMAIN.PKG_POCHTA IS

-- Author  : EUGEN
-- Created : 27.03.2019 13:11:12
-- Purpose : Пакет для работы с почтой

  PROCEDURE P_CREATE_POST_ORDERS(P_ORDERS POCHTA_API.ORDERS);              
																
	PROCEDURE P_CREATE_BATCH(P_ORDERS POCHTA_API.ORDER_LIST, P_DATE DATE);																
  
	PROCEDURE P_CHECKIN_BATCH(P_ID_BATCH VARCHAR2, P_DATE DATE);
END PKG_POCHTA;
/

CREATE OR REPLACE PACKAGE BODY LAWMAIN.PKG_POCHTA IS

	PROCEDURE P_CREATE_POST_ORDERS(P_ORDERS POCHTA_API.ORDERS)
		 IS     
	  L_RESP  POCHTA_API.ORDER_RESP;        
		L_ORDER POCHTA_API.R_ORDER;
		L_TYPE  VARCHAR2(50);
	BEGIN
		L_RESP := POCHTA_API.f_create_order(P_ORDERS);                          		
		FOR I IN 1..L_RESP.LIST.COUNT LOOP                                                 
			L_ORDER := POCHTA_API.F_SEARCH_ORDER(P_ID_ORDER => L_RESP.LIST(I));
			
			IF P_ORDERS(I).SIMPLE_NOT THEN 
				L_TYPE := 'SIMPLE';
			ELSE
				L_TYPE := 'ORDER'; 
			END IF;
			
			INSERT INTO T_CLI_POST_ORDERS(ID,ID_CASE,ADDRESS,TYPE_NOTIF,MASS,TYPE_ENVELOP,SUM_ORDER)
			VALUES(L_RESP.LIST(I),P_ORDERS(I).ORDER_NUM,P_ORDERS(I).ADDRESS.ORIG_ADDRESS
		        ,L_TYPE,P_ORDERS(I).MASS ,P_ORDERS(I).ENVELOPE,(L_ORDER.SUM_ORDER+L_ORDER.SUM_NDS)/100);  
		END LOOP;
	END;            
	
	PROCEDURE P_CREATE_BATCH(P_ORDERS POCHTA_API.ORDER_LIST, P_DATE DATE)
		IS
		L_BATCHES POCHTA_API.BATCH_RESP;
	BEGIN
		L_BATCHES := POCHTA_API.F_CREATE_BATCH(P_ORDERS,P_DATE);           
		
		INSERT INTO T_POST_BATCHES(ID,DATE_POST,CREATED_BY,DATE_CREATED,CODE_STATUS)
		VALUES(L_BATCHES.BATCH(1),P_DATE,f$_usr_id,SYSDATE,'WAIT_CHECKIN');
		
		FORALL I IN 1..P_ORDERS.COUNT
		  UPDATE T_CLI_POST_ORDERS O SET O.ID_BATCH = L_BATCHES.BATCH(1) 
			WHERE O.ID = P_ORDERS(I);
	END;            
	
	PROCEDURE P_CHECKIN_BATCH(P_ID_BATCH VARCHAR2, P_DATE DATE)
		IS   
		L_RET VARCHAR2(500);
	BEGIN
		L_RET := POCHTA_API.F_SET_BATCH_DATE(P_ID_BATCH,P_DATE);
		IF L_RET IS NOT NULL THEN
			 RAISE_APPLICATION_ERROR(-20200,L_RET);
		END IF;                                  
 
    L_RET := POCHTA_API.F_CHECKIN_BATCH(P_ID_BATCH);
    
		IF L_RET IS NOT NULL THEN
			 RAISE_APPLICATION_ERROR(-20200,L_RET);
		END IF;                                              
		
		UPDATE T_POST_BATCHES B SET B.CODE_STATUS = 'CHECKED', B.DATE_POST = P_DATE WHERE B.ID = P_ID_BATCH;
	END;

END PKG_POCHTA;
/

