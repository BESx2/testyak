i�?create or replace package lawmain.PKG_DOCX_007 is

  -- Author  : EUGEN
  -- Created : 04.03.2020 15:02:04
  -- Purpose : ЗАЯВЛЕНИЕ В ФНС
	-- CODE    : CLAIM_FNS
  
  FUNCTION RUN_REPORT(P_PAR_01  IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_02 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_03 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_04 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_05 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_06 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_07 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_08 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_09 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_10 IN VARCHAR2 DEFAULT NULL) RETURN LAWSUP.PKG_FILES.REC_DOC;		                           

end PKG_DOCX_007;
/

create or replace package body lawmain.PKG_DOCX_007 is

  FUNCTION RUN_REPORT(P_PAR_01  IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_02 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_03 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_04 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_05 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_06 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_07 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_08 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_09 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_10 IN VARCHAR2 DEFAULT NULL) RETURN LAWSUP.PKG_FILES.REC_DOC
   IS  
   L_XML XMLTYPE;
   L_SQL SYS_REFCURSOR;     
	 L_GROUP VARCHAR2(50);
   L_REP LAWSUP.PKG_FILES.REC_DOC;    
  BEGIN         
    OPEN L_SQL FOR 
      SELECT (SELECT F.FNS_NAME FROM T_FNS F WHERE F.ID_FNS = TO_NUMBER(P_PAR_04)) "фнс",
       			 (SELECT F.ADDRESS FROM T_FNS F WHERE F.ID_FNS = TO_NUMBER(P_PAR_04)) "адрес_фнс"
						 ,S.CLI_ALT_NAME "абонент"
						 ,'«' || TO_CHAR(SYSDATE, 'dd') || '» ' ||
						 LOWER(PKG_UTILS.F_GET_NAME_MONTHS(SYSDATE, 'R')) || ' ' ||
						 TO_CHAR(SYSDATE, 'YYYY') || 'г.' "дата_заявления1"
						 ,S.OGRN "огрн_абон"
						 ,S.INN "инн_абон"
						 ,PKG_CLIENTS.F_GET_ADDRESS(S.ID_CLIENT) "адрес_абон"
						 ,LL.LEVY_DATE "дата_исполдок"
						 ,(SELECT EX.NUM_EXEC
							FROM   T_CLI_COURT_EXEC EX
							WHERE  EX.ID = S.LAST_EXEC) "номер_дела"
						 ,LL.LEVY_SERIES "серия_исполлиста"
						 ,LL.LEVY_NUM "номер_исполлиста"       
						 ,LL.LEVY_DEBT "общая_сумма_долга"
						 ,LL.LEVY_PENY "общая_сумма_неустойки"
						 ,LL.LEVY_GOV_TOLL "общая_сумма_госпошлины"          
						 ,INITCAP(L.LAST_NAME||' '||SUBSTR(L.FIRST_NAME,1,1)||'. '||SUBSTR(L.SECOND_NAME,1,1)) "исполнитель"
             ,L.POSITION "должность"
						 ,'тел.: '||L.PHONE "телефон"
			FROM   V_CLI_CASES S
						,T_USER_LIST L
						,(SELECT L.ID_CASE
										,L.LEVY_NUM
										,L.LEVY_DATE
										,L.LEVY_SERIES
										,L.LEVY_DEBT
										,L.LEVY_PENY
										,L.LEVY_GOV_TOLL
							FROM   T_CLI_LEVY L
							WHERE  L.ID_LEVY = (SELECT MAX(L.ID_LEVY)
																	FROM   T_CLI_LEVY L
																	WHERE  L.ID_CASE = P_PAR_01)) LL
			WHERE  S.ID_CASE = TO_NUMBER(P_PAR_01)
			AND    LL.ID_CASE = S.ID_CASE
			AND    S.CURATOR_FSSP = L.ID(+);
			
    L_XML := PKG_CONTROLLER.F_DOCX_XML(P_SQL => L_SQL,P_REP_CODE => 'FSSP_FNS');    		
    L_REP.P_BLOB := PKG_CONTROLLER.F_GET_REPORT_XML(L_XML.getClobVal());
    L_REP.P_FILE_NAME := 'Заявление в ФНС';
		
		RETURN l_rep;
	END;										
	
end PKG_DOCX_007;
/

