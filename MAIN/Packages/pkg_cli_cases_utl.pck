i�?CREATE OR REPLACE PACKAGE LAWMAIN.PKG_CLI_CASES_UTL IS

	-- Author  : EUGEN
	-- Created : 25.10.2018 9:01:41
	-- Purpose : Пакет для работы с делами юр. лиц

  FUNCTION F_CREATE_CASE_PRETRIAL(P_ID_CLIENT NUMBER, 
		                              P_PRETEN_DATE DATE) RETURN NUMBER;	                              

	
  FUNCTION F_GET_CONTRACTS(P_ID_CASE NUMBER
													,P_SEP     VARCHAR2 DEFAULT ', ') RETURN VARCHAR2;		
	
	FUNCTION F_GET_CONTRACT_ID(P_ID_CASE NUMBER, P_TYPE VARCHAR2) RETURN NUMBER;																							 														

	--Установка получения претензии
  PROCEDURE P_SET_PRET_GET_DATE(P_ID_CASE NUMBER
		                           ,P_DATE_GET DATE
															 ,P_SEND_SERV VARCHAR2);         
														 
  FUNCTION F_GET_DEBT_PERIOD(P_ID_WORK NUMBER,P_ID_CONTRACT NUMBER DEFAULT NULL) RETURN VARCHAR2;														 
END PKG_CLI_CASES_UTL;
/

CREATE OR REPLACE PACKAGE BODY LAWMAIN.PKG_CLI_CASES_UTL IS

	FUNCTION F_CREATE_CASE_PRETRIAL(P_ID_CLIENT   NUMBER, 
		                              P_PRETEN_DATE DATE) RETURN NUMBER	                              
	  IS	
    L_ID_CASE   NUMBER;
    L_SUM       NUMBER := 0;        
    L_PREF      VARCHAR2(100);   
		L_GROUP     VARCHAR2(50);
  BEGIN
    
		INSERT INTO T_CLI_DEBT_WORK(ID_WORK,ID_CLIENT,TYPE_WORK,DEPT,DATE_CREATED,CREATED_BY)
		VALUES(SEQ_CLI_DEBT_WORK.NEXTVAL,P_ID_CLIENT,'CASE','PRE_TRIAL',SYSDATE,F$_USR_ID)
		RETURNING ID_WORK INTO L_ID_CASE;
		
    INSERT INTO T_CLI_CASES(ID_CASE,ID_CLIENT,CASE_NAME,DATE_CREATED,CREATED_BY,DEP,CURATOR_PRE)
    VALUES(L_ID_CASE, P_ID_CLIENT, TO_CHAR(L_ID_CASE),SYSDATE,F$_USR_ID,'PRE_TRIAL',F$_USR_ID);
  
    FOR I IN (SELECT S.n001, D.debt, D.ID_CONTRACT,CG.ABON_GROUP, 
			               DECODE(CG.ABON_GROUP,'UK','УК','ТСЖ','TSG','GOV','БДЖ','ПР') PREF 
			        FROM APEX_COLLECTIONS S, 
							     V_CLI_DEBTS D, 
									 T_CONTRACTS CT,
									 T_CUSTOMER_GROUP CG
			        WHERE S.collection_name = 'DEBTS_CLI_CASES' 
							AND   CT.ID_CONTRACT = D.ID_CONTRACT
							AND   CT.CUST_GROUP = CG.GROUP_NAME 
							AND   D.id_doc = S.n001)
		LOOP
      INSERT INTO T_CLI_DEBTS(ID_DEBT,ID_WORK,ID_DOC,DATE_CREATED,CREATED_BY
                              ,ID_CONTRACT,ID_CLIENT,DEBT_CURRENT,DEBT_CREATED,DEPT
                              ,DATE_PRETRIAL,DEBT_PRETRIAL)
      VALUES(SEQ_CLI_CASE_DEBTS.NEXTVAL,L_ID_CASE,I.N001,SYSDATE,F$_USR_ID,I.ID_CONTRACT
            ,P_ID_CLIENT,I.DEBT,I.DEBT,'PRE_TRIAL',SYSDATE,I.DEBT);
						          
      L_SUM   := L_SUM + I.DEBT;                                                      
			L_PREF  := I.PREF; 
			L_GROUP := I.ABON_GROUP;
    END LOOP;
    
		UPDATE T_CLI_DEBT_WORK W
		  SET  W.DEBT_CREATED = L_SUM,
			     W.CURRENT_DEBT = L_SUM
		 WHERE W.ID_WORK = L_ID_CASE;                                                                 
		
    UPDATE T_CLI_CASES S
    SET   S.SUM_DEBT  = L_SUM,
		      S.CASE_NAME = L_PREF||'-'||S.CASE_NAME,
          S.DEBT_PRE_TRIAL  = L_SUM,
          S.DEBT_PRETENSION = NVL2(P_PRETEN_DATE,PKG_CLI_CASES.F_GET_DEBT_ON_DATE(S.ID_CASE,P_PRETEN_DATE),NULL),
          S.DATE_PRETENSION = P_PRETEN_DATE,
					S.ID_CWS_CONTRACT = F_GET_CONTRACT_ID(S.ID_CASE,'Основной в ХВС'),
					S.ID_SEW_CONTRACT = F_GET_CONTRACT_ID(S.ID_CASE,'Основной в Водоотведение'),
					S.GROUP_CODE =  L_GROUP					                
    WHERE S.ID_CASE = L_ID_CASE;     
		
  
    PKG_CLI_CASES.P_CHANGE_STATUS(L_ID_CASE,'DOCS_TO_COURT');
    PKG_EVENTS.P_CREATE_CLI_EVENT(P_ID_WORK => L_ID_CASE,P_CODE => 'CREATE');
        
    RETURN L_ID_CASE;
  EXCEPTION
    WHEN OTHERS THEN
    	RAISE_APPLICATION_ERROR(-20200,SQLERRM);
	END;		
	
															 
	
------------------------------------------------------------------------------------------	
	                        
	FUNCTION F_GET_CONTRACTS(P_ID_CASE NUMBER
													,P_SEP     VARCHAR2 DEFAULT ', ') RETURN VARCHAR2 
	IS
		L_RET VARCHAR2(32767);
	BEGIN
		SELECT LISTAGG(CT.CTR_NUMBER, P_SEP) WITHIN GROUP (ORDER BY CT.ID_CONTRACT)
		INTO   L_RET
		FROM   T_CONTRACTS CT
		WHERE  CT.ID_CONTRACT IN (SELECT D.ID_CONTRACT
															FROM   T_CLI_DEBTS D
															WHERE  D.ID_WORK = P_ID_CASE);  
		RETURN L_RET;													
	END;                      

------------------------------------------------------------------------
	
	FUNCTION F_GET_CONTRACT_ID(P_ID_CASE NUMBER, P_TYPE VARCHAR2) RETURN NUMBER
		IS
		L_RET NUMBER;
	BEGIN
		SELECT CT.ID_CONTRACT
		INTO L_RET
		FROM T_CONTRACTS CT, T_CLI_DEBTS D 
		WHERE D.ID_CONTRACT = CT.ID_CONTRACT                       
		AND  CT.CTR_TYPE = P_TYPE
		AND D.ID_WORK = P_ID_CASE
		AND  ROWNUM = 1;
		
		RETURN L_RET;
		
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			RETURN NULL;
	END;                       
	
--------------------------------------------------------------------------

  PROCEDURE P_SET_PRET_GET_DATE(P_ID_CASE NUMBER
		                           ,P_DATE_GET DATE
															 ,P_SEND_SERV VARCHAR2)
		IS                     
		L_ID_EVEN     NUMBER;        
		L_CODE_STATUS VARCHAR2(100);
	BEGIN            		
 	  SELECT S.CODE_STATUS INTO L_CODE_STATUS
		FROM V_CLI_CASES S WHERE S.ID_CASE = P_ID_CASE;
				
		IF L_CODE_STATUS != 'PRET_SEND' THEN 
			RETURN;
		END IF;                                                 
		
    PKG_CLI_CASES.P_CHANGE_STATUS(P_ID_CASE,'PRET_GET');			
		PKG_EVENTS.P_CREATE_CLI_EVENT(P_ID_CASE,'PRET_GET',P_DATE_GET);	
		
		UPDATE T_CLI_CASES S SET S.SEND_SERV = P_SEND_SERV 
		WHERE S.ID_CASE = P_ID_CASE;
		
	END;

-------------------------------------------------------------------------

  FUNCTION F_GET_DEBT_PERIOD(P_ID_WORK NUMBER
		                        ,P_ID_CONTRACT NUMBER DEFAULT NULL) RETURN VARCHAR2
	  IS                  
		PRAGMA UDF;
		L_RET VARCHAR2(500);
	BEGIN
	  SELECT TO_CHAR(TRUNC(MIN(FD.DOC_DATE),'MM'),'DD.MM.YYYY')||' - '||
		       TO_CHAR(LAST_DAY(MAX(FD.DOC_DATE)),'DD.MM.YYYY')                
		INTO L_RET			 
		FROM T_CLI_FINDOCS FD, 		
		     T_CLI_DEBTS D
		WHERE D.ID_WORK = P_ID_WORK
		AND   FD.ID_DOC = D.ID_DOC
		AND   (D.ID_CONTRACT = P_ID_CONTRACT OR P_ID_CONTRACT IS NULL);
		
		RETURN L_RET;          
		
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			RETURN '-';
	END;
	
	
END PKG_CLI_CASES_UTL;
/

