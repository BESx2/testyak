i�?create or replace package lawmain.PKG_CLAIMS is

  -- Author  : MM
  -- Created : 02/06/17
  -- Purpose : пакет для работы с заявками
  

  

	--Процедура создаёт заявку
	        PROCEDURE P_CREATE_CLAIM(P_ID_TYPE 		NUMBER,
													 			   P_TITLE      VARCHAR2,          
																	 P_ID_CREATOR VARCHAR2 DEFAULT f$_usr_id,
                                   P_MESSAGE    VARCHAR2 DEFAULT NULL,
																	 P_FILE_PATH VARCHAR2 DEFAULT NULL);
	--Процедура создаёт заявку
	PROCEDURE P_CREATE_MESSAGE(P_ID_СLM 		NUMBER,
													 	 P_TEXT      VARCHAR2 DEFAULT '',
														 P_FILE_PATH VARCHAR2 DEFAULT NULL,          
														 P_ID_CREATOR VARCHAR2 DEFAULT f$_usr_id);
	
	--Процедура редактирует тип заявки 
	PROCEDURE P_EDIT_CLAIM_TYPE(P_ID_CLM 	 NUMBER,
													   P_ID_TYPE   NUMBER);													 																	 	

	--процедура редактирует сообщение
	PROCEDURE P_UPDATE_MESSAGE(P_ID_MSG 		NUMBER,
													 	 P_TEXT      VARCHAR2,
														 P_FILE_PATH VARCHAR2,          
														 P_ID_MODIFIER VARCHAR2 DEFAULT f$_usr_id);
														 		
	--процедура удаляет сообщение													 
  PROCEDURE P_DELETE_MESSAGE(P_ID_MSG NUMBER, 
		                         P_ID_MODIFIER VARCHAR2 DEFAULT f$_usr_id);
															
	--процедура изменяет статус заявки
	PROCEDURE P_CHANGE_CLAIM_STATUS(P_ID_CLAIM NUMBER, 
		                              P_ID_STATUS_CODE VARCHAR2,
																	P_ID_MODIFIER VARCHAR2 DEFAULT f$_usr_id);															                                                                                      
	

  PROCEDURE F_DOWNLOAD_BLOB(P_ID_MSG NUMBER);
	--процедера изменяем приоритет заявки
	PROCEDURE P_CHANGE_PRIORITY(P_ID_CLAIM NUMBER, 
		                          P_PRIORITY VARCHAR2);

end PKG_CLAIMS;
/

create or replace package body lawmain.PKG_CLAIMS is

    PROCEDURE p_send_mail(P_CLM_ID IN NUMBER, P_TYPE IN VARCHAR2 DEFAULT 'NOTIF')
        IS
			L_RET NUMBER;
			L_STR VARCHAR2(32767);
			L_HEADER     VARCHAR2(500);
			PRAGMA AUTONOMOUS_TRANSACTION;
			L_eMAIL VARCHAR2(4000); 
			G$HTML VARCHAR2(32000);
    BEGIN 
			 
		    
				G$HTML := '#USERNAME#, в заявке №#CLAIM# добавлено новое сообщение. Вы можете посмотреть его в своем личном кабинете. 
				           С уважением, группа поддержки ПИР.';
				FOR i IN (SELECT initcap(UL.FIRST_NAME||' '||UL.Second_Name) AS username,
					        c.id AS claim,
									ul.email
					        FROM T_CLM_CLAIMS C,
									     t_User_List UL
									WHERE C.CREATED_BY_ID = UL.ID
									AND C.ID=P_CLM_ID		  
										)
            LOOP
							G$HTML := REPLACE(G$HTML,'#USERNAME#',i.username);
							G$HTML := REPLACE(G$HTML,'#CLAIM#', i.claim);
							L_eMAIL := I.EMAIL;
            END LOOP;

            IF L_eMAIL IS NULL THEN
							RETURN;
            END IF;

            L_RET := PKG_MAIL.F_MAKE_EMAIL(P_HEADER => 'Ответ по заявке ПИР'
                                           ,P_FROM => PKG_PREF.F$C1('USER_MAIL') -- do not change !!!! if change => change password in sup.pkg_mail!!!
                                           ,P_BODY => G$HTML
                                          );
            -- кому
            PKG_MAIL.P_ATTACHE_EMAIL_ADDR(    P_EMAIL         => L_EMAIL
                                             ,P_ID_MAIL      => L_RET
                                             ,P_EMAIL_TYPE   => PKG_MAIL.LIST_EMAIL_TYPE.TO_
                                        );

        PKG_MAIL.P_SEND(P_ID => L_RET,P_SEND_TYPE => PKG_MAIL.LIST_TYPE_SEND.LTE);
 				commit;
    END;

 --процедура создает новую заявку	
 PROCEDURE P_CREATE_CLAIM(P_ID_TYPE 	 NUMBER,
													P_TITLE      VARCHAR2,          
													P_ID_CREATOR VARCHAR2 DEFAULT f$_usr_id,
													P_MESSAGE    VARCHAR2 DEFAULT NULL,
													P_FILE_PATH VARCHAR2 DEFAULT NULL)
	IS
	l_id NUMBER;                             
	BEGIN		  
		 l_id:= claim_seq.nextval;
            INSERT INTO t_clm_claims( id, id_type, id_clm_status, title, date_created, created_by_id, date_modified, modified_by_id,Priority)
            VALUES(l_id, P_ID_TYPE, 4, P_TITLE, SYSDATE, P_ID_CREATOR, NULL, NULL, 0);
	IF P_MESSAGE IS NOT NULL THEN
	   P_CREATE_MESSAGE(l_id, P_MESSAGE, P_FILE_PATH, P_ID_CREATOR);	
	END IF;							
	END;
	
	 --процедура создает новую заявку	
 PROCEDURE P_EDIT_CLAIM_TYPE(P_ID_CLM 	 NUMBER,
													   P_ID_TYPE   NUMBER)
	IS                          
	BEGIN		  
  
	UPDATE t_clm_claims
	SET   id_type = P_ID_TYPE 					
	WHERE id = P_ID_CLM;						
	
	END;	
		
	--процедура создает новое сообщение
	PROCEDURE P_CREATE_MESSAGE(P_ID_СLM 		NUMBER,
													 	 P_TEXT      VARCHAR2 DEFAULT '',
														 P_FILE_PATH VARCHAR2 DEFAULT NULL,          
														 P_ID_CREATOR VARCHAR2 DEFAULT f$_usr_id)
	IS 	   
		 L_ID_FILE   NUMBER;
		 L_CLAIM_CREATOR NUMBER;                            
	BEGIN		
	
	IF P_FILE_PATH IS NOT NULL THEN  
	   L_ID_FILE := PKG_FILES.F_UPLOAD_FILE(P_FILE_PATH,'CLM_FILE');
	END IF;	
	
  INSERT INTO t_clm_claim_message(id, id_clm, text,  date_created,created_by_id,Id_File)
            VALUES(claim_seq.nextval, P_ID_СLM, P_TEXT,  SYSDATE, P_ID_CREATOR, L_ID_FILE);
	
	UPDATE t_clm_claims
	SET    modified_by_id = P_ID_CREATOR, 					
				 date_modified	 = SYSDATE
	WHERE id = P_ID_СLM;
	
	SELECT C.CREATED_BY_ID INTO L_CLAIM_CREATOR FROM T_CLM_CLAIMS C WHERE C.ID = P_ID_СLM; 
	
	IF P_ID_CREATOR!= L_CLAIM_CREATOR THEN
     p_send_mail(P_ID_СLM,'NOTIF');
	END IF;
				  		
	END;
	
	--процедура редактирует сообщение
	PROCEDURE P_UPDATE_MESSAGE(P_ID_MSG 		NUMBER,
													 	 P_TEXT      VARCHAR2,
														 P_FILE_PATH VARCHAR2,          
														 P_ID_MODIFIER VARCHAR2 DEFAULT f$_usr_id)
	IS 
		 L_BLOB BLOB;
		 L_MIME_TYPE VARCHAR2(255);
		 L_FILE_NAME VARCHAR2(400);                                                                     
		 L_ID_FILE   NUMBER;                              
	BEGIN		 

	UPDATE t_clm_claim_message
	SET    text = P_TEXT,
	       modified_by_id = P_ID_MODIFIER, 					
				 date_modified	 = SYSDATE
	WHERE id = P_ID_MSG
	RETURNING ID_FILE INTO L_ID_FILE;
	
	IF P_FILE_PATH IS NOT NULL THEN      
		IF L_ID_FILE IS NOT NULL THEN
		  PKG_FILES.P_DELETE_FILE(L_ID_FILE);
		END IF;
		
		L_ID_FILE := PKG_FILES.F_UPLOAD_FILE(P_FILE_PATH,'CLM_FILE');
		 
		UPDATE t_clm_claim_message
  	SET  ID_FILE = L_ID_FILE
	  WHERE id = P_ID_MSG;	 		 			 
	END IF;
	 				  		
	END;
	
	--процедура удаляет сообщение
	PROCEDURE P_DELETE_MESSAGE(P_ID_MSG NUMBER, 
		                         P_ID_MODIFIER VARCHAR2 DEFAULT f$_usr_id)
	IS                             
	BEGIN		 

	DELETE FROM  t_clm_claim_message
	WHERE id = P_ID_MSG;
	
	UPDATE t_clm_claims
	SET   modified_by_id = P_ID_MODIFIER, 					
				date_modified	 = SYSDATE
	WHERE id = (SELECT m.id_clm FROM t_clm_claim_message m WHERE m.id = P_ID_MSG) ;
				  		
	END;
	
	--процедура изменяет статус заявки
	PROCEDURE P_CHANGE_CLAIM_STATUS(P_ID_CLAIM NUMBER, 
		                              P_ID_STATUS_CODE VARCHAR2,
																	P_ID_MODIFIER VARCHAR2 DEFAULT f$_usr_id)
	IS                             
	BEGIN		 
  
	IF P_ID_STATUS_CODE = 'RETURN_TO_WORK' THEN --не затираем данные при возвращении в работу
		UPDATE t_clm_claims
	  SET   id_clm_status = (SELECT s.id FROM t_clm_claim_status s WHERE s.code = P_ID_STATUS_CODE)
	WHERE id = P_ID_CLAIM ;
	
	ELSE 	 
	UPDATE t_clm_claims
	SET   modified_by_id = P_ID_MODIFIER,
	      id_clm_status = (SELECT s.id FROM t_clm_claim_status s WHERE s.code = P_ID_STATUS_CODE), 					
				date_modified	 = SYSDATE
	WHERE id = P_ID_CLAIM ;
	END IF;
	--если закрываем заявку, обнуляем приоритет
	IF P_ID_STATUS_CODE = 'CLOSED' THEN 	 
		P_CHANGE_PRIORITY(P_ID_CLAIM,0); 
	END IF;			  		
	END;
	
	--процедура изменяет статус заявки
	PROCEDURE P_CHANGE_PRIORITY(P_ID_CLAIM NUMBER, 
		                          P_PRIORITY VARCHAR2)
	IS                             
	BEGIN		 
  	UPDATE t_clm_claims c
    SET c.priority = P_PRIORITY
    WHERE c.id =  P_ID_CLAIM; 
	END;																				 	                                  
																		 	                                  
	PROCEDURE F_DOWNLOAD_BLOB(P_ID_MSG NUMBER)
			IS
			L_ID_FILE NUMBER;
	BEGIN 
		SELECT M.ID_FILE
		INTO   L_ID_FILE
		FROM t_clm_claim_message m 
		WHERE m.id = P_ID_MSG;
		
		PKG_FILES.P_DOWNLOAD_FILE(P_ID_FILE => L_ID_FILE);

	END;			
					
	
	

  
	                      
end PKG_CLAIMS;
/

