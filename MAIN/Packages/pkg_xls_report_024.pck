i�?create or replace package lawmain.PKG_XLS_REPORT_024 is

  -- Author  : BES
  -- Created : 17.02.2017
  -- Purpose : Расчет суммы пени для исп. произво.
	-- Code:    PENY_CALC_UL_FSSP
	
	G$MAX_INT NUMBER := 2147483647;
	
	TYPE R_DATA IS RECORD (DATE_CHANGE DATE, VAL NUMBER);
	TYPE ARR_DATA IS TABLE OF R_DATA INDEX BY PLS_INTEGER;
	TYPE R_CALC_PENY_PERIOD IS RECORD (DATE_START DATE, DATE_END DATE, PENY_RATE NUMBER, PENY_PRC NUMBER,SUM_PAY NUMBER, TYPE_CALC VARCHAR2(50));
	TYPE PERIOD_CALC IS TABLE OF R_CALC_PENY_PERIOD;
  TYPE R_CALC_PERIOD IS RECORD(DEBT_DATE DATE, SUM_DOC NUMBER, NUM_DOC VARCHAR2(100), ID_DOC NUMBER, CALC PERIOD_CALC);
	TYPE PENY_CALC IS TABLE OF R_CALC_PERIOD INDEX BY PLS_INTEGER;
	
	FUNCTION F_GET_DEBT_DATE(P_DATE DATE) RETURN DATE;
	
	
	
	
  
  FUNCTION RUN_REPORT(P_PAR_01 VARCHAR2 DEFAULT NULL
											,P_PAR_02 VARCHAR2 DEFAULT NULL
											,P_PAR_03 VARCHAR2 DEFAULT NULL
											,P_PAR_04 VARCHAR2 DEFAULT NULL
											,P_PAR_05 VARCHAR2 DEFAULT NULL
											,P_PAR_06 VARCHAR2 DEFAULT NULL
											,P_PAR_07 VARCHAR2 DEFAULT NULL
											,P_PAR_08 VARCHAR2 DEFAULT NULL
											,P_PAR_09 VARCHAR2 DEFAULT NULL
											,P_PAR_10 VARCHAR2 DEFAULT NULL) RETURN LAWSUP.PKG_FILES.REC_DOC;	  

end PKG_XLS_REPORT_024;
/

create or replace package body lawmain.PKG_XLS_REPORT_024 is

	TYPE REC_DOC IS RECORD(	P_BLOB BLOB DEFAULT NULL );

	EXCEL_DOC REC_DOC := NULL;
------------------------------------------------------------------------

  FUNCTION F_GET_DEBT_DATE(P_DATE DATE) RETURN DATE
		IS
	BEGIN
		IF PKG_CALENDAR.F_CHK_BUISNESS_DAY(P_DATE) THEN
			RETURN P_DATE;
		ELSE            
			RETURN F_GET_DEBT_DATE(P_DATE+1);
		END IF;
	END;

------------------------------------------------------------------------

	PROCEDURE p$s( ps_value IN CLOB )  -- отправляет накопленное значение ps_value в буфер и записывает
	IS
		BUFFER    	RAW(32767);
    l_offset    NUMBER := 1;
    BUF_SIZE    NUMBER := 8000;
    BUF_VAR    VARCHAR2(32767);
  BEGIN
    LOOP
    EXIT WHEN L_OFFSET > DBMS_LOB.getlength(PS_VALUE);
    BUF_VAR := DBMS_LOB.substr(PS_VALUE,BUF_SIZE,L_OFFSET);
		BUFFER := UTL_RAW.cast_to_raw( CONVERT( BUF_VAR, 'UTF8'/*'CL8MSWIN1251'*/ ) );
		DBMS_LOB.WRITEAPPEND( EXCEL_DOC.P_BLOB, UTL_RAW.LENGTH( BUFFER ), BUFFER );
    L_OFFSET := L_OFFSET + BUF_SIZE;
    END LOOP;
  END;
------------------------------------------------------------------------
	PROCEDURE P_ADD_DATA(
	P_ID_CASE NUMBER
	) IS
	  L_XML CLOB;
		l_contract_number t_contracts.ctr_number%TYPE;
		l_ctr_date DATE;
		l_peny_date DATE; --дата формирования дела, на которую считается пени
		l_group_code VARCHAR2(50); --группа, к которой относится клиент
		l_sum       NUMBER := 0;
		l_prc_refin NUMBER; --процент рефинансирования
 		l_total         NUMBER := 0;
		l_date_on       DATE;
		l_curator       VARCHAR2(50);
    l_row_cnt       NUMBER := 0;
		l_row_num       NUMBER := 0;       
		l_id_refin      NUMBER;   
		l_min_per       NUMBER;  
		l_sum_pay       NUMBER;
		l_debt_date     DATE;  
		l_debt          NUMBER;   
		l_prev_date     DATE;    
		l_rate          NUMBER;
		l_case_type				t_Cli_Cases.Case_Type%TYPE;
		l_peny_refin_rate NUMBER;
		L_CNT NUMBER := 0;         
		L_CALC PENY_CALC;   
		L_CALC_IDX NUMBER := 0;
		L_PAYS ARR_DATA;
		L_RATES ARR_DATA;   
		L_PENY_GROUP VARCHAR2(50);    
		L_RET     NUMBER := 0;		      
		L_PRC_REF NUMBER;                
		L_DATE_FROM DATE;           
		L_GROUP   VARCHAR2(50);
	BEGIN
		L_XML :=
			'<?xml version="1.0"?>
<?mso-application progid="Excel.Sheet"?>
<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:o="urn:schemas-microsoft-com:office:office"
 xmlns:x="urn:schemas-microsoft-com:office:excel"
 xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:html="http://www.w3.org/TR/REC-html40">
 <DocumentProperties xmlns="urn:schemas-microsoft-com:office:office">
  <Author></Author>
  <LastAuthor></LastAuthor>
  <LastPrinted>2017-10-25T11:02:36Z</LastPrinted>
  <Created>2016-03-14T07:23:01Z</Created>
  <LastSaved>2017-10-27T12:25:52Z</LastSaved>
  <Version>16.00</Version>
 </DocumentProperties>
 <OfficeDocumentSettings xmlns="urn:schemas-microsoft-com:office:office">
  <AllowPNG/>
 </OfficeDocumentSettings>
 <ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel">
  <WindowHeight>8055</WindowHeight>
  <WindowWidth>21570</WindowWidth>
  <WindowTopX>0</WindowTopX>
  <WindowTopY>0</WindowTopY>
  <ProtectStructure>False</ProtectStructure>
  <ProtectWindows>False</ProtectWindows>
 </ExcelWorkbook>
 <Styles>
  <Style ss:ID="Default" ss:Name="Normal">
   <Alignment ss:Vertical="Bottom"/>
   <Borders/>
   <Font ss:FontName="Arial Cyr" x:CharSet="204" x:Family="Swiss"/>
   <Interior/>
   <NumberFormat/>
   <Protection/>
  </Style>
  <Style ss:ID="m321210644">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="12"/>
   <NumberFormat ss:Format="@"/>
  </Style>   
	<Style ss:ID="s61">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="1" x:Family="Roman" ss:Size="12"/>
   <NumberFormat ss:Format="Percent"/>
  </Style>
  <Style ss:ID="s62">
   <Alignment ss:Vertical="Top" ss:WrapText="1"/>
  </Style>
  <Style ss:ID="s64">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders/>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="14" ss:Bold="1"/>
  </Style>
  <Style ss:ID="s65">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
  </Style>
  <Style ss:ID="s66">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="12"/>
  </Style>
  <Style ss:ID="s67">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="12"/>
  </Style>
  <Style ss:ID="s68">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="1" x:Family="Roman" ss:Size="12"/>
  </Style>
  <Style ss:ID="s69">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="12"/>
  </Style>
  <Style ss:ID="s77">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="12"/>
   <NumberFormat ss:Format="Standard"/>
  </Style>
  <Style ss:ID="s78">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="12"/>
   <NumberFormat ss:Format="Standard"/>
  </Style>
  <Style ss:ID="s79">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="12"/>
   <NumberFormat ss:Format="Short Date"/>
  </Style>
  <Style ss:ID="s80">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="12"/>
   <NumberFormat/>
  </Style>
  <Style ss:ID="s81">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="12"/>
   <NumberFormat ss:Format="Percent"/>
  </Style>
  <Style ss:ID="s82">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="1" x:Family="Roman" ss:Size="12"/>
   <NumberFormat ss:Format="Standard"/>
  </Style>
  <Style ss:ID="s83">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="12"/>
   <NumberFormat ss:Format="Standard"/>
  </Style>
  <Style ss:ID="s84">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="12"/>
   <NumberFormat ss:Format="Standard"/>
  </Style>
  <Style ss:ID="s85">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="12"/>
   <NumberFormat ss:Format="Short Date"/>
  </Style>
  <Style ss:ID="s86">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="12"/>
   <NumberFormat/>
  </Style>
  <Style ss:ID="s88">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="12"/>
   <NumberFormat ss:Format="@"/>
  </Style>
  <Style ss:ID="s90">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="1" x:Family="Roman" ss:Size="12"/>
   <NumberFormat ss:Format="Standard"/>
  </Style>
  <Style ss:ID="s91">
   <Alignment ss:Horizontal="Center" ss:Vertical="Top" ss:WrapText="1"/>
  </Style>
  <Style ss:ID="s92">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="12" ss:Bold="1"/>
   <NumberFormat ss:Format="@"/>
  </Style>
  <Style ss:ID="s93">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="12"/>
  </Style>
  <Style ss:ID="s94">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
  </Style>
  <Style ss:ID="s95">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="12"/>
   <NumberFormat ss:Format="Fixed"/>
  </Style>
  <Style ss:ID="s96">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="12"/>
   <NumberFormat ss:Format="Short Date"/>
  </Style>
  <Style ss:ID="s97">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="12"/>
   <NumberFormat ss:Format="0"/>
  </Style>
  <Style ss:ID="s98">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="12"/>
   <NumberFormat ss:Format="Percent"/>
  </Style>
  <Style ss:ID="s99">
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="1" x:Family="Roman" ss:Size="12"
    ss:Bold="1"/>
  </Style>
  <Style ss:ID="s100">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="12" ss:Bold="1"/>
   <NumberFormat ss:Format="Standard"/>
  </Style>
  <Style ss:ID="s102">
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="12"/>
  </Style>
  <Style ss:ID="s103">
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="14"/>
  </Style>
  <Style ss:ID="s104">
   <Font ss:FontName="Arial Cyr" x:CharSet="204" x:Family="Swiss" ss:Size="14"/>
  </Style>
  <Style ss:ID="s105">
   <Alignment ss:Horizontal="Center" ss:Vertical="Top" ss:WrapText="1"/>
   <Font ss:FontName="Arial Cyr" x:CharSet="204" x:Family="Swiss" ss:Size="14"/>
  </Style>
  <Style ss:ID="s106">
   <Alignment ss:Vertical="Top" ss:WrapText="1"/>
   <Borders/>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="12" ss:Bold="1"/>
  </Style>
  <Style ss:ID="s107">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders/>
  </Style>
 </Styles>
 <Worksheet ss:Name="ставка">
  <Table ss:ExpandedColumnCount="15" ss:ExpandedRowCount="100000" x:FullColumns="1"
   x:FullRows="1">              
   <Column ss:AutoFitWidth="0" ss:Width="81"/>
   <Column ss:AutoFitWidth="0" ss:Width="71.25"/>
   <Column ss:AutoFitWidth="0" ss:Width="84.75"/>
   <Column ss:AutoFitWidth="0" ss:Width="77.25" ss:Span="1"/>
    <Column ss:AutoFitWidth="0" ss:Width="84.75"/>
   <Column ss:AutoFitWidth="0" ss:Width="200"/>
   <Column ss:AutoFitWidth="0" ss:Width="112.5"/>
   <Column ss:AutoFitWidth="0" ss:Width="114"/>';
   p$s(L_XML);
   
	 
 	--Пишем группу потребителей в l_group_code
	--дата формирования отчета в l_date_on
  	SELECT S.ABON_GROUP,S.PENY_DATE,S.CTR_NUMBER,S.CTR_DATE,S.PENY_GROUP_CALC
		INTO  l_group_code,L_DATE_ON,l_contract_number,L_CTR_DATE, L_PENY_GROUP 	
		FROM  V_CLI_CASES S
		WHERE S.ID_CASE = P_ID_CASE;			
		
	
		SELECT L.LEVY_DEBT, L.LEVY_PENY_DATE 
		INTO L_DEBT, L_DATE_FROM 
		FROM T_CLI_LEVY L WHERE L.ID_CASE = P_ID_CASE;
	
		SELECT R.PRC_REF, R.ID_REFIN INTO L_PRC_REF, L_ID_REFIN FROM T_REFIN R 
		WHERE L_DATE_ON BETWEEN  R.DATE_REF_BEG AND NVL(R.DATE_REF_END,L_DATE_ON);
		
		SELECT PR.RATE INTO L_RATE FROM T_PENY_RATE PR 
		WHERE PR.ID_REFIN = L_ID_REFIN AND PR.ABON_GROUP = l_group_code
		AND   PR.DAYS_TO IS NULL;

	

	 L_XML:='  <Row ss:Index="4" ss:AutoFitHeight="0" ss:Height="50.25" ss:StyleID="s62">';
	 L_XML:= L_XML||' <Cell ss:MergeAcross="7" ss:StyleID="s64"><Data ss:Type="String">Расчет суммы пени  по договору № '||l_contract_number||
	 								' от '||TO_CHAR(l_ctr_date,'DD.MM.YYYY')||' по состоянию на '||TO_CHAR(l_date_on,'DD.MM.YYYY')||' (c НДС)</Data></Cell>';
   L_XML:= L_XML||'</Row>';
   p$s(L_XML);

	 L_XML:='<Row ss:AutoFitHeight="0" ss:Height="66.75" ss:StyleID="s65">
    <Cell ss:StyleID="s67"><Data ss:Type="String">Задолженность</Data></Cell>
    <Cell ss:StyleID="s67"><Data ss:Type="String">с</Data></Cell>
    <Cell ss:StyleID="s67"><Data ss:Type="String">по</Data></Cell>
    <Cell ss:StyleID="s67"><Data ss:Type="String">дней</Data></Cell>
    <Cell ss:StyleID="s67"><Data ss:Type="String">Ставка</Data></Cell>
    <Cell ss:StyleID="s68"><Data ss:Type="String">Доля ставка </Data></Cell>  
    <Cell ss:StyleID="s68"><Data ss:Type="String">Формула </Data></Cell>  
    <Cell ss:StyleID="s69"><Data ss:Type="String">Пени</Data></Cell>
   </Row>';
	 p$s(L_XML);
    


		FOR I IN (SELECT T.DATE_CHNG, T.PAYED, T.TYPE_CHNG, NVL(LEAD(T.DATE_CHNG) OVER(ORDER BY T.DATE_CHNG),L_DATE_ON) NEXT_DATE		               
			        FROM(SELECT L_DATE_FROM DATE_CHNG, 0 PAYED, 'START' TYPE_CHNG FROM DUAL
							      UNION ALL								
									 SELECT FDC.DATE_CON, SUM(-FDC.AMOUNT) PAYED, 'PAY' TYPE_CHNG			       
									 FROM T_CLI_DEBTS D,
									 		  T_CLI_FINDOC_CONS FDC 
									 WHERE D.ID_WORK = P_ID_CASE
									 AND   (FDC.ID_DOC_TO = D.ID_DOC OR FDC.ID_DOC_FROM = D.ID_DOC)
									 AND    FDC.ID_DOC_TO != FDC.ID_DOC_FROM
									 AND    FDC.AMOUNT < 0               
									 AND    FDC.DOC_TYPE != 'Корректировка реализации' 
									 AND FDC.DATE_CON BETWEEN L_DATE_FROM AND L_DATE_ON
									 GROUP BY FDC.DATE_CON) T        
							ORDER BY T.DATE_CHNG		 
							)
		LOOP			       			                                                                   
			L_DEBT := L_DEBT - I.PAYED;
   	  L_XML := '<Row ss:AutoFitHeight="0" ss:Height="39.75" ss:StyleID="s65">';	
			l_row_num := l_row_num + 1;		
      IF I.TYPE_CHNG = 'PAY' THEN
		  	L_XML := L_XML||'<Cell ss:StyleID="s77"><Data ss:Type="Number">'||REPLACE(TO_CHAR(-I.PAYED),',','.')||'</Data></Cell>
					               <Cell ss:StyleID="s79"><Data ss:Type="DateTime">'||TO_CHAR(I.DATE_CHNG,'YYYY-MM-DD')||'T00:00:00.000</Data></Cell>      
					               <Cell ss:MergeAcross="5" ss:StyleID="s77"><Data ss:Type="String">Погашение части долга</Data></Cell>
					               </Row>
												 <Row ss:AutoFitHeight="0" ss:Height="39.75" ss:StyleID="s65">'; 			 
	 			l_row_num := l_row_num + 1;		
		  END IF;		

 		 L_XML := L_XML||'<Cell ss:StyleID="s77"><Data ss:Type="Number">'||REPLACE(TO_CHAR(L_DEBT),',','.')||'</Data></Cell>';
		 IF I.TYPE_CHNG = 'PAY' THEN
       L_XML := L_XML||'<Cell ss:StyleID="s79"><Data ss:Type="DateTime">'||TO_CHAR(I.DATE_CHNG+1,'YYYY-MM-DD')||'T00:00:00.000</Data></Cell>';
		 ELSE
       L_XML := L_XML||'<Cell ss:StyleID="s79"><Data ss:Type="DateTime">'||TO_CHAR(I.DATE_CHNG,'YYYY-MM-DD')||'T00:00:00.000</Data></Cell>';				
		 END IF; 
     L_XML := L_XML||'<Cell ss:StyleID="s79"><Data ss:Type="DateTime">'||TO_CHAR(I.NEXT_DATE,'YYYY-MM-DD')||'T00:00:00.000</Data></Cell>';
		 L_XML := L_XML||'<Cell ss:StyleID="s80" ss:Formula="=RC[-1]-RC[-2]+1"><Data ss:Type="Number"></Data></Cell>';
 		 L_XML := L_XML||'<Cell ss:StyleID="s61"><Data ss:Type="Number">'||REPLACE(TO_CHAR(l_prc_ref/100),',','.')||'</Data></Cell>';
 		 L_XML := L_XML||'<Cell ss:StyleID="s82"><Data ss:Type="String">'||'1/'||TO_CHAR(L_RATE)||'</Data></Cell>';	
		 L_XML := L_XML||'<Cell ss:StyleID="s83" ss:Formula="=CONCATENATE(RC[-6],&quot;*&quot;,RC[-3],&quot;*&quot;,RC[-2],&quot;*&quot;,RC[-1])"><Data ss:Type="String">0</Data></Cell>';
     L_XML := L_XML||'<Cell ss:StyleID="s83" ss:Formula="=SUM(RC[-7]/'||trim(to_char(L_RATE,'999999990D9','NLS_NUMERIC_CHARACTERS=''. '''))||'*RC[-3]*RC[-4])"><Data ss:Type="Number"></Data></Cell>';									 
     L_XML := L_XML||'</Row>';
	   p$s(L_XML);
		 
	 END LOOP;
    

    L_XML := '<Row ss:AutoFitHeight="0" ss:Height="26.25" ss:StyleID="s91">
									<Cell ss:StyleID="s92"><Data ss:Type="String">ИТОГО</Data></Cell>
									<Cell ss:StyleID="s93"/>
									<Cell ss:StyleID="s94"/>
									<Cell ss:StyleID="s95"/>
									<Cell ss:StyleID="s96"/>
									<Cell ss:StyleID="s96"/>
									<Cell ss:StyleID="s97"/>
									<Cell ss:StyleID="s100" ss:Formula="=SUM(R[-'||l_row_num||']C:R[-1]C)"><Data ss:Type="Number"></Data></Cell>
								 </Row>';

	  
		p$s( L_XML );
	

		--куратор 
		FOR z IN(
	  SELECT substr(ul.first_name,1,1)||'.'||substr(ul.second_name,1,1)||'.'||INITCAP(ul.last_name) AS CURATOR
		FROM  t_cli_cases cc,t_user_list ul
		WHERE ul.id = cc.curator_COURT
	  AND cc.id_case = P_ID_CASE)
		LOOP
      l_curator := z.curator;
		END LOOP;
		

	 L_XML := ' <Row ss:AutoFitHeight="0" ss:Height="26.25" ss:StyleID="s91">
    <Cell ss:StyleID="Default"/>
    <Cell ss:StyleID="Default"/>
    <Cell ss:StyleID="Default"/>
    <Cell ss:StyleID="Default"/>
    <Cell ss:StyleID="Default"/>
    <Cell ss:StyleID="Default"/>
    <Cell ss:StyleID="Default"/>
    <Cell ss:StyleID="Default"/>
    <Cell ss:StyleID="s102"/>
    <Cell ss:StyleID="Default"/>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="26.25" ss:StyleID="s91">
    <Cell ss:StyleID="Default"/>
    <Cell ss:StyleID="s103"><Data ss:Type="String">Представитель по доверенности</Data></Cell>
    <Cell ss:StyleID="s104"/>
    <Cell ss:StyleID="s104"/>
    <Cell ss:StyleID="s105"/>
    <Cell ss:StyleID="s104"/>
    <Cell ss:StyleID="s104"/>
    <Cell ss:StyleID="s103"><Data ss:Type="String">'||l_curator||'</Data></Cell>
    <Cell ss:StyleID="s104"/>
    <Cell ss:StyleID="Default"/>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="35.25"/>
  </Table>
  <WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">
   <PageSetup>
    <Header x:Margin="0.51180555555555551"/>
    <Footer x:Margin="0.51180555555555551"/>
    <PageMargins x:Bottom="0.98402777777777772" x:Left="0.52986111111111112"
     x:Right="0.40972222222222221" x:Top="0.52986111111111112"/>
   </PageSetup>
   <FitToPage/>
   <Print>
    <FitHeight>0</FitHeight>
    <ValidPrinterInfo/>
    <PaperSizeIndex>9</PaperSizeIndex>
    <Scale>60</Scale>
    <HorizontalResolution>600</HorizontalResolution>
    <VerticalResolution>600</VerticalResolution>
   </Print>
   <Selected/>
   <TopRowVisible>9</TopRowVisible>
   <Panes>
    <Pane>
     <Number>3</Number>
     <ActiveRow>13</ActiveRow>
     <ActiveCol>6</ActiveCol>
    </Pane>
   </Panes>
   <ProtectObjects>False</ProtectObjects>
   <ProtectScenarios>False</ProtectScenarios>
   <EnableSelection>NoSelection</EnableSelection>
  </WorksheetOptions>
 </Worksheet>
</Workbook>';

		p$s( L_XML );
	
	
	END P_ADD_DATA;

---------------------------------------------------------------------------------------------------------------------------------

  FUNCTION RUN_REPORT(P_PAR_01 VARCHAR2 DEFAULT NULL
											,P_PAR_02 VARCHAR2 DEFAULT NULL
											,P_PAR_03 VARCHAR2 DEFAULT NULL
											,P_PAR_04 VARCHAR2 DEFAULT NULL
											,P_PAR_05 VARCHAR2 DEFAULT NULL
											,P_PAR_06 VARCHAR2 DEFAULT NULL
											,P_PAR_07 VARCHAR2 DEFAULT NULL
											,P_PAR_08 VARCHAR2 DEFAULT NULL
											,P_PAR_09 VARCHAR2 DEFAULT NULL
											,P_PAR_10 VARCHAR2 DEFAULT NULL) RETURN LAWSUP.PKG_FILES.REC_DOC
	IS
	l_BLOB LAWSUP.PKG_FILES.REC_DOC;
  BEGIN

	  EXCEL_DOC := NULL;
		DBMS_LOB.CREATETEMPORARY( EXCEL_DOC.P_BLOB, TRUE );
    DBMS_LOB.OPEN( EXCEL_DOC.P_BLOB, DBMS_LOB.LOB_READWRITE );

		P_ADD_DATA(TO_NUMBER(P_PAR_01));

		DBMS_LOB.CLOSE(EXCEL_DOC.P_BLOB);

		    l_BLOB.P_BLOB := EXCEL_DOC.P_BLOB;
	      l_BLOB.P_FILE_NAME := 'RASCHET_PENY_'||P_PAR_01;
				RETURN L_BLOB;

  END RUN_REPORT;

end PKG_XLS_REPORT_024;
/

