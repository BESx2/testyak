i�?create or replace package lawmain.PKG_SEND_SMS is

  -- Author  : EUGEN
  -- Created : 11.01.2017 12:27:46
  -- Purpose : Пакет для отправки SMS уведомлений
  
  PROCEDURE P_SEND_SMS (P_SMS_CODE   VARCHAR2,
												P_PAR_01 			VARCHAR2 DEFAULT NULL,
												P_PAR_02 			VARCHAR2 DEFAULT NULL,
												P_PAR_03 		  VARCHAR2 DEFAULT NULL,
												P_PAR_04 			VARCHAR2 DEFAULT NULL,
												P_PAR_05 			VARCHAR2 DEFAULT NULL,
												P_PAR_06 			VARCHAR2 DEFAULT NULL,
												P_PAR_07 			VARCHAR2 DEFAULT NULL,
												P_PAR_08		 	VARCHAR2 DEFAULT NULL,											
												P_PAR_09 			VARCHAR2 DEFAULT NULL,
												P_PAR_10 			VARCHAR2 DEFAULT NULL);  
	
	--Процедура связывает дело юр.лица c SMS											
  PROCEDURE P_INSERT_CLI_CASE_SMS(P_ID_CASE NUMBER      --Ид дела
																 ,P_ID_SMS NUMBER       --ИД СМС
																 ,P_SMS_TYPE VARCHAR2); --Тип смски (код в T_USER_PARAMETERS)
        
	 
	--Привязка к самому юр лицу. досудебка
	PROCEDURE P_INSERT_CLI_SMS(P_ID_CLIENT VARCHAR2
													  ,P_ID_SMS NUMBER
														,P_SMS_TYPE VARCHAR2);	
																											 
end PKG_SEND_SMS;
/

create or replace package body lawmain.PKG_SEND_SMS IS

	PROCEDURE P_SEND_SMS(P_SMS_CODE   VARCHAR2,
												P_PAR_01 			VARCHAR2 DEFAULT NULL,
												P_PAR_02 			VARCHAR2 DEFAULT NULL,
												P_PAR_03 		  VARCHAR2 DEFAULT NULL,
												P_PAR_04 			VARCHAR2 DEFAULT NULL,
												P_PAR_05 			VARCHAR2 DEFAULT NULL,
												P_PAR_06 			VARCHAR2 DEFAULT NULL,
												P_PAR_07 			VARCHAR2 DEFAULT NULL,
												P_PAR_08		 	VARCHAR2 DEFAULT NULL,											
												P_PAR_09 			VARCHAR2 DEFAULT NULL,
												P_PAR_10 			VARCHAR2 DEFAULT NULL)
	IS  
		EX_CODE_NOT_DEF EXCEPTION ;        
	BEGIN
		  PKG_LOG.P$LOG(p_PROG_UNIT =>  'PKG_SEND_SMS'
											,p_MESS => 	'P_SMS_CODE:'||P_SMS_CODE||CHR(10)||CHR(13)||
																'P_PAR_01:'||P_PAR_01||CHR(10)||
																'P_PAR_02:'||P_PAR_02||CHR(10)||
																'P_PAR_03:'||P_PAR_03||CHR(10)||
																'P_PAR_04:'||P_PAR_04||CHR(10)||
																'P_PAR_05:'||P_PAR_05||CHR(10)||
																'P_PAR_06:'||P_PAR_06||CHR(10)||
																'P_PAR_07:'||P_PAR_07||CHR(10)||
																'P_PAR_08:'||P_PAR_08||CHR(10)||
																'P_PAR_09:'||P_PAR_09||CHR(10)||
																'P_PAR_10:'||P_PAR_10||CHR(10)
										  ,P_d1 => SYSDATE); 
	 --Массовая рассылка СМС уведомлений для ЮЛ
	 IF P_SMS_CODE = 'FSSP_SMS_EXEC_START'  THEN
		 PKG_SMS_01.P_SEND_SMS(P_ID_CASE => P_PAR_01,P_PHONE => P_PAR_02);				
	 ELSIF P_SMS_CODE = 'COURT_SMS_NOTIF'  THEN		                        
		 PKG_SMS_02.P_SEND_SMS(P_ID_CASE => P_PAR_01,P_PHONE => P_PAR_02);	
	 ELSIF P_SMS_CODE = 'PRE_TRIAL_DEBT_SMS'  THEN	
		 PKG_SMS_03.P_SEND_SMS(P_ID_CLIENT => P_PAR_01,P_ID_CONTRACT =>P_PAR_03,P_PHONE => P_PAR_02); 	 
	 ELSIF P_SMS_CODE = 'PRE_TRIAL_GARANT_SMS' THEN
		 PKG_SMS_04.P_SEND_SMS(P_ID_CLIENT => P_PAR_01,P_PHONE => P_PAR_02);	 
	 ELSIF P_SMS_CODE = 'PRE_TRIAL_RESTR_SMS' THEN                           
		 PKG_SMS_05.P_SEND_SMS(P_ID_CLIENT => P_PAR_01,P_PHONE => P_PAR_02);
	 ELSIF P_SMS_CODE = 'PRET_SMS_DEBT' THEN                            
		 IF P_PAR_03 IS NULL THEN
		    PKG_SMS_06.P_SEND_SMS(P_ID_CASE => P_PAR_01,P_PHONE => P_PAR_02);	 
		 ELSE
			  PKG_SMS_06.P_SEND_MASS(P_ID_FOLDER => P_PAR_03);
		 END IF;
	 ELSE
		RAISE EX_CODE_NOT_DEF;		
	 END IF;

											
		
	EXCEPTION
		WHEN EX_CODE_NOT_DEF THEN 
	     RAISE_APPLICATION_ERROR(-20200,'Неправильно указан код СМС');	     									
	END;
	
	PROCEDURE P_INSERT_CLI_CASE_SMS(P_ID_CASE NUMBER
																 ,P_ID_SMS NUMBER
																 ,P_SMS_TYPE VARCHAR2)
	 IS               
	 L_CNT NUMBER;
	BEGIN     

		 
		 		 
			 INSERT INTO T_CLI_CASE_SMS(ID_CASE_SMS,ID_CASE,ID_SMS,SMS_TYPE)
			 VALUES(SEQ_CLI_CASE_SMS.NEXTVAL,P_ID_CASE,P_ID_SMS,P_SMS_TYPE);
		 
	END;          
	
	PROCEDURE P_INSERT_CLI_SMS(P_ID_CLIENT VARCHAR2
													  ,P_ID_SMS NUMBER
														,P_SMS_TYPE VARCHAR2)
	 IS               
	 L_CNT NUMBER;
	BEGIN     

		 
			 INSERT INTO T_CLI_SMS(ID_CLI_SMS,ID_CLIENT,ID_SMS,SMS_TYPE)
			 VALUES(SEQ_CLI_CASE_SMS.NEXTVAL,P_ID_CLIENT,P_ID_SMS,P_SMS_TYPE);

	
	END;      
	
end PKG_SEND_SMS;
/

