i�?CREATE OR REPLACE PACKAGE LAWMAIN.PKG_PDF_001
  IS
 -- Author  : EUGEN
 -- Created : 13.12.2016 
 -- Purpose : Претензия для юридического лица
 -- Code    : PRETENSION


	l_pdf_1 blob;
	v_tpl_1   plpdf_type.tr_tpl_data;
	v_tpl_2   plpdf_type.tr_tpl_data;
	l_tpl_id_1 number;
	l_tpl_id_2 number;

  TYPE R_D_T IS RECORD (CLI_ALT_NAME VARCHAR2(500), 
	                      DATE_PRET    VARCHAR2(100),										
												ADDRESS      VARCHAR2(2000),                       
												PERIOD       VARCHAR2(500), 
												CONTRACTS    VARCHAR2(500),
												SUM_DEBT     VARCHAR2(100),
												CWS_DEBT     VARCHAR2(100),
												SEW_DEBT     VARCHAR2(100),
												SUM_PENY     VARCHAR2(100),
												CWS_PENY     VARCHAR2(100),
												SEW_PENY     VARCHAR2(100),
												TOTAL_DEBT   VARCHAR2(100),
												CURATOR      VARCHAR2(500),											
												PHONE        VARCHAR2(200),												
												ABON_GROUP   VARCHAR2(50),
												HAS_CWS      VARCHAR2(1),
												HAS_SEW      VARCHAR2(1),     
												PENY_DATE    VARCHAR2(500),
												CWS_NUM      VARCHAR2(100),
												SEW_NUM      VARCHAR2(100),
												SOI_TEXT     VARCHAR2(1000));
  R_D R_D_T;

  FUNCTION RUN_REPORT(P_PAR_01 VARCHAR2 DEFAULT NULL
											,P_PAR_02 VARCHAR2 DEFAULT NULL
											,P_PAR_03 VARCHAR2 DEFAULT NULL
											,P_PAR_04 VARCHAR2 DEFAULT NULL
											,P_PAR_05 VARCHAR2 DEFAULT NULL
											,P_PAR_06 VARCHAR2 DEFAULT NULL
											,P_PAR_07 VARCHAR2 DEFAULT NULL
											,P_PAR_08 VARCHAR2 DEFAULT NULL
											,P_PAR_09 VARCHAR2 DEFAULT NULL
											,P_PAR_10 VARCHAR2 DEFAULT NULL) RETURN LAWSUP.PKG_FILES.REC_DOC;

END PKG_PDF_001;
/

CREATE OR REPLACE PACKAGE BODY LAWMAIN.PKG_PDF_001
    IS

  ----------------------------------------------------------------------
  FUNCTION AD(P1 IN VARCHAR2,P2 IN VARCHAR2) RETURN VARCHAR2
    IS
  BEGIN
    IF P2 IS NOT NULL THEN
      RETURN p1;
    END IF;
    RETURN p2;
  END;

  PROCEDURE P_ADD_DATA(P_ID_CASE NUMBER) 
		IS  
		L_NLS VARCHAR2(50) := 'NLS_NUMERIC_CHARACTERS='', ''';
		L_FMT VARCHAR2(50) := 'FM999G999G999G999G999G990D00';                         
		L_TYPE VARCHAR2(50);
	BEGIN    
		SELECT W.TYPE_WORK INTO L_TYPE FROM T_CLI_DEBT_WORK W WHERE W.ID_WORK = P_ID_CASE;
		IF L_TYPE = 'PRETEN' THEN
			  FOR I IN (SELECT CLI.CLI_ALT_NAME 
                       ,S.DATE_PRETENSION DATE_PRET
                       ,PKG_CLIENTS.F_GET_ADDRESS(S.ID_CLIENT) ADDRESS
                       ,(SELECT TO_CHAR(TRUNC(MIN(FD.DOC_DATE), 'MM'), 'DD.MM.YYYY') ||
                               ' года по ' ||
                               TO_CHAR(LAST_DAY(MAX(FD.DOC_DATE)), 'DD.MM.YYYY') || ' года'
                        FROM   T_CLI_DEBTS D, T_CLI_FINDOCS FD
                        WHERE  D.ID_WORK = S.ID_WORK
                        AND    FD.ID_DOC = D.ID_DOC) PERIOD
                       ,PKG_CLI_CASES_UTL.F_GET_CONTRACTS(S.ID_WORK, ' и ') CONTRACTS
                       ,S.SUM_DEBT SUM_DEBT
                       ,(SELECT SUM(D.DEBT_CURRENT)
                        FROM   T_CLI_DEBTS D
                        WHERE  D.ID_WORK = S.ID_WORK
                        AND    D.ID_CONTRACT = S.ID_CWS_CONTRACT) CWS_DEBT
                       ,(SELECT SUM(D.DEBT_CURRENT)
                        FROM   T_CLI_DEBTS D
                        WHERE  D.ID_WORK = S.ID_WORK
                        AND    D.ID_CONTRACT = S.ID_SEW_CONTRACT) SEW_DEBT
                       ,S.PENY SUM_PENY
                       ,(SELECT SUM(D.PENY)
                        FROM   T_CLI_DEBTS D
                        WHERE  D.ID_WORK = S.ID_WORK
                        AND    D.ID_CONTRACT = S.ID_CWS_CONTRACT) CWS_PENY
                       ,(SELECT SUM(D.PENY)
                        FROM   T_CLI_DEBTS D
                        WHERE  D.ID_WORK = S.ID_WORK
                        AND    D.ID_CONTRACT = S.ID_SEW_CONTRACT) SEW_PENY
                       ,NVL(S.SUM_DEBT, 0) + NVL(S.PENY, 0) TOTAL_DEBT
                       ,INITCAP(L.LAST_NAME || ' ' || L.FIRST_NAME || ' ' || L.SECOND_NAME) CURATOR
                       ,NVL2(L.PHONE, ' ,' || L.PHONE, NULL) PHONE
                       ,S.ABON_GROUP GROUP_CODE 
                       ,NVL2(S.ID_CWS_CONTRACT,'Y','N') HAS_CWS
                       ,NVL2(S.ID_SEW_CONTRACT,'Y','N') HAS_SEW                
                       ,(SELECT CT.CTR_NUMBER FROM T_CONTRACTS CT WHERE CT.ID_CONTRACT = S.ID_CWS_CONTRACT) CWS_NUM
                       ,(SELECT CT.CTR_NUMBER FROM T_CONTRACTS CT WHERE CT.ID_CONTRACT = S.ID_SEW_CONTRACT) SEW_NUM  
                       ,'«'||TO_CHAR(S.PENY_DATE,'DD')||'» '||LOWER(PKG_UTILS.F_GET_NAME_MONTHS(S.PENY_DATE,'R'))||' '||TO_CHAR(S.PENY_DATE,'YYYY"г."') PENY_DATE                   
                FROM   T_CLI_PRETENSION S, T_CLIENTS CLI, T_USER_LIST L
                WHERE  CLI.ID_CLIENT = S.ID_CLIENT
                AND    S.CURATOR = L.ID(+)
                AND    S.ID_WORK = P_ID_CASE)
      LOOP
        R_D.CLI_ALT_NAME := I.CLI_ALT_NAME;
        R_D.DATE_PRET    := TO_CHAR(I.DATE_PRET,'DD.MM.YYYY');
        R_D.ADDRESS      := I.ADDRESS;
        R_D.PERIOD       := I.PERIOD;
        R_D.CONTRACTS    := I.CONTRACTS;
        R_D.SUM_DEBT     := TO_CHAR(I.SUM_DEBT,L_FMT,L_NLS);
        R_D.CWS_DEBT     := TO_CHAR(I.CWS_DEBT,L_FMT,L_NLS);
        R_D.SEW_DEBT     := TO_CHAR(I.SEW_DEBT,L_FMT,L_NLS);
        R_D.SUM_PENY     := TO_CHAR(I.SUM_PENY,L_FMT,L_NLS);
        R_D.CWS_PENY     := TO_CHAR(I.CWS_PENY,L_FMT,L_NLS);
        R_D.SEW_PENY     := TO_CHAR(I.SEW_PENY,L_FMT,L_NLS);
				R_D.TOTAL_DEBT   := TO_CHAR(I.TOTAL_DEBT,L_FMT,L_NLS);
				R_D.CURATOR      := I.CURATOR;
				R_D.PHONE        := I.PHONE;
				R_D.ABON_GROUP   := I.GROUP_CODE;                      
				R_D.HAS_CWS      := I.HAS_CWS;
				R_D.HAS_SEW      := I.HAS_SEW;     
				R_D.CWS_NUM      := I.CWS_NUM;
				R_D.SEW_NUM      := I.SEW_NUM;                                                                                                                           
				R_D.PENY_DATE    := I.PENY_DATE;
				IF R_D.CWS_NUM LIKE '%СОИ%' THEN
					R_D.SOI_TEXT := ' на содержание общего имущества';
				END IF;			
			END LOOP;
		ELSE
			FOR I IN (SELECT CLI.CLI_ALT_NAME 
											 ,S.DATE_PRETENSION DATE_PRET
											 ,PKG_CLIENTS.F_GET_ADDRESS(S.ID_CLIENT) ADDRESS
											 ,(SELECT TO_CHAR(TRUNC(MIN(FD.DOC_DATE), 'MM'), 'DD.MM.YYYY') ||
															 ' года по ' ||
															 TO_CHAR(LAST_DAY(MAX(FD.DOC_DATE)), 'DD.MM.YYYY') || ' года'
												FROM   T_CLI_DEBTS D, T_CLI_FINDOCS FD
												WHERE  D.ID_WORK = S.ID_CASE
												AND    FD.ID_DOC = D.ID_DOC) PERIOD
											 ,PKG_CLI_CASES_UTL.F_GET_CONTRACTS(S.ID_CASE, ' и ') CONTRACTS
											 ,S.SUM_DEBT SUM_DEBT
											 ,(SELECT SUM(D.DEBT_CURRENT)
												FROM   T_CLI_DEBTS D
												WHERE  D.ID_WORK = S.ID_CASE
												AND    D.ID_CONTRACT = S.ID_CWS_CONTRACT) CWS_DEBT
											 ,(SELECT SUM(D.DEBT_CURRENT)
												FROM   T_CLI_DEBTS D
												WHERE  D.ID_WORK = S.ID_CASE
												AND    D.ID_CONTRACT = S.ID_SEW_CONTRACT) SEW_DEBT
											 ,S.PENY SUM_PENY
											 ,(SELECT SUM(D.PENY)
												FROM   T_CLI_DEBTS D
												WHERE  D.ID_WORK = S.ID_CASE
												AND    D.ID_CONTRACT = S.ID_CWS_CONTRACT) CWS_PENY
											 ,(SELECT SUM(D.PENY)
												FROM   T_CLI_DEBTS D
												WHERE  D.ID_WORK = S.ID_CASE
												AND    D.ID_CONTRACT = S.ID_SEW_CONTRACT) SEW_PENY
											 ,NVL(S.SUM_DEBT, 0) + NVL(S.PENY, 0) TOTAL_DEBT
											 ,INITCAP(L.LAST_NAME || ' ' || L.FIRST_NAME || ' ' || L.SECOND_NAME) CURATOR
											 ,NVL2(L.PHONE, ' ,' || L.PHONE, NULL) PHONE
											 ,S.GROUP_CODE 
											 ,NVL2(S.ID_CWS_CONTRACT,'Y','N') HAS_CWS
											 ,NVL2(S.ID_SEW_CONTRACT,'Y','N') HAS_SEW                
											 ,(SELECT CT.CTR_NUMBER FROM T_CONTRACTS CT WHERE CT.ID_CONTRACT = S.ID_CWS_CONTRACT) CWS_NUM
											 ,(SELECT CT.CTR_NUMBER FROM T_CONTRACTS CT WHERE CT.ID_CONTRACT = S.ID_SEW_CONTRACT) SEW_NUM	
											 ,'«'||TO_CHAR(S.PENY_DATE,'DD')||'» '||LOWER(PKG_UTILS.F_GET_NAME_MONTHS(S.PENY_DATE,'R'))||' '||TO_CHAR(S.PENY_DATE,'YYYY"г."') PENY_DATE									 
								FROM   T_CLI_CASES S, T_CLIENTS CLI, T_USER_LIST L
								WHERE  CLI.ID_CLIENT = S.ID_CLIENT
								AND    S.CURATOR_PRE = L.ID(+)
								AND    S.ID_CASE = P_ID_CASE)
			LOOP
				R_D.CLI_ALT_NAME := I.CLI_ALT_NAME;
				R_D.DATE_PRET    := TO_CHAR(I.DATE_PRET,'DD.MM.YYYY');
				R_D.ADDRESS      := I.ADDRESS;
				R_D.PERIOD       := I.PERIOD;
				R_D.CONTRACTS    := I.CONTRACTS;
				R_D.SUM_DEBT     := TO_CHAR(I.SUM_DEBT,L_FMT,L_NLS);
				R_D.CWS_DEBT     := TO_CHAR(I.CWS_DEBT,L_FMT,L_NLS);
				R_D.SEW_DEBT     := TO_CHAR(I.SEW_DEBT,L_FMT,L_NLS);
				R_D.SUM_PENY     := TO_CHAR(I.SUM_PENY,L_FMT,L_NLS);
				R_D.CWS_PENY     := TO_CHAR(I.CWS_PENY,L_FMT,L_NLS);
				R_D.SEW_PENY     := TO_CHAR(I.SEW_PENY,L_FMT,L_NLS);
				R_D.TOTAL_DEBT   := TO_CHAR(I.TOTAL_DEBT,L_FMT,L_NLS);
				R_D.CURATOR      := I.CURATOR;
				R_D.PHONE        := I.PHONE;
				R_D.ABON_GROUP   := I.GROUP_CODE;                      
				R_D.HAS_CWS      := I.HAS_CWS;
				R_D.HAS_SEW      := I.HAS_SEW;     
				R_D.CWS_NUM      := I.CWS_NUM;
				R_D.SEW_NUM      := I.SEW_NUM;                                                                                                                           
				R_D.PENY_DATE    := I.PENY_DATE;
				IF R_D.CWS_NUM LIKE '%СОИ%' THEN
					R_D.SOI_TEXT := ' на содержание общего имущества';
				END IF;			
			END LOOP;					
		END IF;
	END;

	FUNCTION F_GET_PDF(P_OPTION VARCHAR2) RETURN BLOB
	IS

		l_blob blob;

		l_ttf_t    		Plpdf_Type.t_addfont;
		l_ttf_tbd    	Plpdf_Type.t_addfont;   
	 	
		l$_tw_id    NUMBER          := PLPDF_FONTS.l$_tw_id;
    l$_tw       VARCHAR2(40)    := PLPDF_FONTS.l$_tw; 
    l$_twbd_id    NUMBER        := PLPDF_FONTS.l$_twbd_id; 
    l$_twbd     VARCHAR2(40)    := PLPDF_FONTS.l$_twbd;
		
		v_tpl_1   plpdf_type.tr_tpl_data;
		l_tpl_id NUMBER;
		C_BORDER NUMBER := 0;
	  L_W NUMBER;
		L_H NUMBER;
    L_Y NUMBER;
		L_IMG BLOB;
  BEGIN
   IF P_OPTION = 'TEMPLATE' THEN
		  NULL;
   ELSE
			Plpdf.init;
			Plpdf.setEncoding(p_enc => 'CP1251');

			l_ttf_t       :=  Plpdf_Ttf.GetTTF(l$_tw_id);
			l_ttf_tbd     :=  Plpdf_Ttf.GetTTF(l$_twbd_id);
			plpdf.addTTF(p_family => l$_tw,p_data => l_ttf_t);
			plpdf.addTTF(p_family => l$_twbd,p_data => l_ttf_tbd);   	 	
			
			Plpdf.NewPage;
			
			v_tpl_1 := PLPDF_PARSER.LoadTemplate(p_id => 1);
			l_tpl_id := PLPDF.InsTemplate(v_tpl_1);
			PLPDF.setPageTemplate(p_tplidx => l_tpl_id);			
			
    END IF;
    
		PLPDF.setLeftMargin(29); 
		PLPDF.setRightMargin(15); 
		
		PLPDF.setPrintFont(l$_tw,null,10); 
		PLPDF.setCurrentY(53);
		PLPDF.PrintCell(p_w => 22,p_h => 5,p_txt => R_D.DATE_PRET,p_border => C_BORDER,p_align => 'L',p_ln => 0,p_clipping => FALSE);
		PLPDF.PrintCell(p_w => 40,p_h => 5,p_txt => '№ ___________',p_border => C_BORDER,p_align => 'L',p_clipping => FALSE,p_ln => 2);
		PLPDF.LineBreak(2);
		PLPDF.setCurrentX(p_x => plpdf.getPageWidth-107);
		PLPDF.PrintMultiLineCell(p_w => 92,p_h => 5,P_TXT=>R_D.CLI_ALT_NAME,p_border => C_BORDER,p_align => 'C',p_clipping => FALSE,p_ln => 2);
		PLPDF.PrintMultiLineCell(p_w => 92,p_h => 5,P_TXT=>R_D.ADDRESS,p_border => C_BORDER,p_align => 'C',p_clipping => FALSE,p_ln => 1);
		
		PLPDF.LineBreak(8); 
		PLPDF.setPrintFont(l$_tw,null,11); 
		PLPDF.PrintCell(p_txt => 'ПРЕТЕНЗИЯ',p_w => plpdf.getPageWidth-42,p_h => 5,p_clipping => FALSE,p_align => 'C',p_border => C_BORDER);
		
		PLPDF.LineBreak(9);     
		L_W := plpdf.getPageWidth-42;          
		
		--------ПЕРЫЙ И ВТОРОЙ АБЗАЦ С УСЛОВИЯМИ------------
		IF R_D.HAS_CWS = 'Y' AND R_D.HAS_SEW = 'Y' THEN
			PLPDF.PrintMultiLineCell(p_w => L_W,p_h => 4.3,p_indent => 13
																,p_txt => 'В период с '||R_D.PERIOD||' года АО «Водоканал» в соответствии с договором'||
																' № '||R_D.CONTRACTS||' осуществляло для '||R_D.CLI_ALT_NAME||' подачу холодной воды и прием сточных вод. '
																,p_border => C_BORDER,p_ln => 2,p_clipping => FALSE,p_align => 'J');	
																
			PLPDF.PrintMultiLineCell(p_w => L_W,p_h => 4.3,p_indent => 13
																,p_txt => 'В результате неоплаты поданной за указанный период холодной воды и принятых сточных вод,'||
																' у '||R_D.CLI_ALT_NAME||' перед АО «Водоканал» образовалась задолженность в размере '||R_D.SUM_DEBT||' рублей'||
																' (основной долг), в том числе:'
																,p_border => C_BORDER,p_ln => 2,p_clipping => FALSE,p_align => 'J');     
		ELSIF R_D.HAS_CWS = 'Y' THEN														
			PLPDF.PrintMultiLineCell(p_w => L_W,p_h => 4.3,p_indent => 13
																,p_txt => 'В период с '||R_D.PERIOD||' года АО «Водоканал» в соответствии с договором'||
																' № '||R_D.CONTRACTS||' осуществляло для '||R_D.CLI_ALT_NAME||' подачу холодной воды'||R_D.SOI_TEXT||'. '
																,p_border => C_BORDER,p_ln => 2,p_clipping => FALSE,p_align => 'J');	
																
			PLPDF.PrintMultiLineCell(p_w => L_W,p_h => 4.3,p_indent => 13
																,p_txt => 'В результате неоплаты поданной за указанный период холодной воды'||R_D.SOI_TEXT||','||
																' у '||R_D.CLI_ALT_NAME||' перед АО «Водоканал» образовалась задолженность в размере '||R_D.SUM_DEBT||' рублей'||
																' (основной долг), в том числе:'
																,p_border => C_BORDER,p_ln => 2,p_clipping => FALSE,p_align => 'J');     
		ELSIF R_D.HAS_SEW = 'Y' THEN
   	  PLPDF.PrintMultiLineCell(p_w => L_W,p_h => 4.3,p_indent => 13
																,p_txt => 'В период с '||R_D.PERIOD||' года АО «Водоканал» в соответствии с договором'||
																' № '||R_D.CONTRACTS||' осуществляло для '||R_D.CLI_ALT_NAME||' прием сточных вод. '
																,p_border => C_BORDER,p_ln => 2,p_clipping => FALSE,p_align => 'J');	
																
			PLPDF.PrintMultiLineCell(p_w => L_W,p_h => 4.3,p_indent => 13
																,p_txt => 'В результате неоплаты принятых за указанный период сточных вод,'||
																' у '||R_D.CLI_ALT_NAME||' перед АО «Водоканал» образовалась задолженность в размере '||R_D.SUM_DEBT||' рублей'||
																' (основной долг), в том числе:'
																,p_border => C_BORDER,p_ln => 2,p_clipping => FALSE,p_align => 'J');         		
		END IF;                                                     
		
		------------------------------------------------------------		
		
		IF R_D.HAS_CWS = 'Y' THEN																											
			PLPDF.PrintMultiLineCell(p_w => L_W,p_h => 4.3,p_indent => 19
																,p_txt => '—  основной долг за холодное водоснабжение'||R_D.SOI_TEXT||': '||R_D.CWS_DEBT||' рублей;'
																,p_border => C_BORDER,p_ln => 2,p_clipping => FALSE,p_align => 'J');	
		END IF;
		
		IF R_D.HAS_SEW = 'Y' THEN																											
			PLPDF.PrintMultiLineCell(p_w => L_W,p_h => 4.3,p_indent => 19
																,p_txt => '—  основной долг за приём сточных вод: '||R_D.SEW_DEBT||' рублей;'
																,p_border => C_BORDER,p_ln => 2,p_clipping => FALSE,p_align => 'J');	
		END IF;																																								
		
	  ----------ТРЕТИЙ АБЗАЦ --------------
		IF R_D.HAS_CWS = 'Y' AND R_D.HAS_SEW = 'Y' THEN
			PLPDF.PrintMultiLineCell(p_w => L_W,p_h => 4.3,p_indent => 13
																,p_txt => 'В связи с просрочкой исполнения обязательства по оплате поданной'||
																' холодной воды и принятых сточных вод, '||R_D.CLI_ALT_NAME||' должно оплатить АО «Водоканал»'||
																' неустойку (пени) по состоянию на '||R_D.PENY_DATE||' в размере '||R_D.SUM_PENY||' рублей, в том числе:'
																,p_border => C_BORDER,p_ln => 2,p_clipping => FALSE,p_align => 'J');
		ELSIF R_D.HAS_CWS = 'Y' THEN 														
			PLPDF.PrintMultiLineCell(p_w => L_W,p_h => 4.3,p_indent => 13
															,p_txt => 'В связи с просрочкой исполнения обязательства по оплате поданной'||
															' холодной воды'||R_D.SOI_TEXT||', '||R_D.CLI_ALT_NAME||' должно оплатить АО «Водоканал»'||
															' неустойку (пени) по состоянию на '||R_D.PENY_DATE||' в размере '||R_D.SUM_PENY||' рублей, в том числе:'
															,p_border => C_BORDER,p_ln => 2,p_clipping => FALSE,p_align => 'J');
		ELSIF R_D.HAS_SEW = 'Y' THEN
			PLPDF.PrintMultiLineCell(p_w => L_W,p_h => 4.3,p_indent => 13
															,p_txt => 'В связи с просрочкой исполнения обязательства по оплате'||
															' принятых сточных вод, '||R_D.CLI_ALT_NAME||' должно оплатить АО «Водоканал»'||
															' неустойку (пени) по состоянию на '||R_D.PENY_DATE||' в размере '||R_D.SUM_PENY||' рублей, в том числе:'
															,p_border => C_BORDER,p_ln => 2,p_clipping => FALSE,p_align => 'J');
		END IF;													
		
		IF R_D.HAS_CWS = 'Y' THEN																											
			PLPDF.PrintMultiLineCell(p_w => L_W,p_h => 4.3,p_indent => 19
																,p_txt => '—  пени за просрочку оплаты за холодное водоснабжение'||R_D.SOI_TEXT||': '||R_D.CWS_PENY||' рублей;'
																,p_border => C_BORDER,p_ln => 2,p_clipping => FALSE,p_align => 'J');	
		END IF;
		
		IF R_D.HAS_SEW = 'Y' THEN																											
			PLPDF.PrintMultiLineCell(p_w => L_W,p_h => 4.3,p_indent => 19
																,p_txt => '—  пени за просрочку оплаты за приём сточных вод: '||R_D.SEW_PENY||' рублей;'
																,p_border => C_BORDER,p_ln => 2,p_clipping => FALSE,p_align => 'J');	
		END IF;		
		
		IF R_D.ABON_GROUP IN ('UK','TSG') OR R_D.CWS_NUM LIKE '%СОИ%' OR R_D.SEW_NUM LIKE '%СОИ%' THEN
			PLPDF.PrintMultiLineCell(p_w => L_W,p_h => 4.3,p_indent => 13
															,p_txt => 'Учитывая вышеизложенное, руководствуясь ст.ст. 309-310, 539-548 Гражданского кодекса'||
															' Российской Федерации, Федеральным законом «О водоснабжении и водоотведении» от 07.12.2011 г.'||
															' № 416-ФЗ, Постановлением Правительства РФ от 14.02.2012 N 124 "О правилах, обязательных при'||
															' заключении договоров снабжения коммунальными ресурсами", Постановлением Правительства РФ от'||
															' 06.05.2011 N 354 "О предоставлении коммунальных услуг собственникам и пользователям помещений'||
															' в многоквартирных домах и жилых домов", другими нормативными правовыми актами, регламентирующими'||
															' отношения сторон в сфере водоснабжения и водоотведения, Вам необходимо в течение 10 (десяти)'||
															' календарных дней погасить образовавшуюся задолженность (основной долг и пени) в размере '||R_D.TOTAL_DEBT||
															' рублей перед АО «Водоканал», в противном случае будем вынуждены обратиться в суд с исковым'||
															' заявлением о взыскании с Вас данной задолженности в принудительном порядке, с отнесением судебных расходов на Вашу сторону.'
															,p_border => C_BORDER,p_ln => 2,p_clipping => FALSE,p_align => 'J');
		ELSE
			PLPDF.PrintMultiLineCell(p_w => L_W,p_h => 4.3,p_indent => 13
															,p_txt => 'Учитывая вышеизложенное, руководствуясь ст.ст. 309-310, 539-548 Гражданского кодекса'||
															' Российской Федерации, Федеральным законом «О водоснабжении и водоотведении» от 07.12.2011 г.'||
															' № 416-ФЗ, другими нормативными правовыми актами, регламентирующими отношения сторон в сфере водоснабжения'||
															' и водоотведения, Вам необходимо в течение 10 (десяти) календарных дней погасить образовавшуюся'||
															' задолженность (основной долг и пени) в размере '||R_D.TOTAL_DEBT||' рублей перед АО «Водоканал», в противном'||
															' случае будем вынуждены обратиться в суд с исковым заявлением о взыскании с Вас данной задолженности'||
															' в принудительном порядке, с отнесением судебных расходов на Вашу сторону.'
															,p_border => C_BORDER,p_ln => 2,p_clipping => FALSE,p_align => 'J');   
			
			PLPDF.PrintMultiLineCell(p_w => L_W,p_h => 4.3,p_indent => 13
															,p_txt => 'Также сообщаем, что в соответствии п. 8 ч. 3. ст. 21 Федеральным закона «О водоснабжении'||
															' и водоотведении» от 07.12.2011 г. № 416-ФЗ в случае неуплаты задолженности в установленные сроки,'||
															' будет произведено ограничение подачи холодного водоснабжения'
															,p_border => C_BORDER,p_ln => 2,p_clipping => FALSE,p_align => 'J');												
		END IF;	
		
		PLPDF.LineBreak(15);
		
		PLPDF.PrintCell(p_w => L_W/3,p_h => 4.3,p_txt => 'Начальник управления сбытом',p_border => C_BORDER,p_clipping => FALSE,p_align => 'L',p_ln => 0);	
	  L_Y := PLPDF.getCurrentY;
		BEGIN
		  SELECT P.B_LOB INTO L_IMG FROM T_USER_PARAMETER P WHERE P.CODE = 'FILESMALL313893';	
			PLPDF.setCurrentXY(110,L_Y-10);		
			PLPDF.PrintImageCell(p_w =>60,p_h => 40,p_name => 'Подпись',p_data => L_IMG);
	  
		EXCEPTION
			WHEN NO_DATA_FOUND THEN
				NULL;
		END;
		PLPDF.setCurrentXY(p_x => (2*L_W/3)+29,p_y => L_Y);
		PLPDF.PrintCell(p_w => L_W/3,p_h => 4.3,p_txt => 'Дураева Н.С.',p_border => C_BORDER,p_clipping => FALSE,p_align => 'R',p_ln => 1);                          
		
		PLPDF.LineBreak(15);
		PLPDF.PrintCell(p_w => L_W/2,p_h => 4.3,p_txt => 'Отметка о получении претензии:',p_border => C_BORDER,p_clipping => FALSE,p_align => 'L',p_ln => 0);	
		PLPDF.LineBreak(7);
		PLPDF.PrintCell(p_w => L_W,p_h => 4.3,p_border => 'B',p_clipping => FALSE,p_align => 'L',p_ln => 0);	                                                
		PLPDF.LineBreak(7);      
		PLPDF.PrintCell(p_w => L_W/2,p_h => 4.3,p_txt => 'Дата: «___» __________ 20__ г.',p_border => C_BORDER,p_clipping => FALSE,p_align => 'L',p_ln => 0);	
		
		PLPDF.setPrintFont(l$_tw,null,9);
		PLPDF.LineBreak(7);                                                                                                                                  
		PLPDF.PrintCell(p_w => L_W,p_h => 4.3,p_txt => R_D.CURATOR||' '||R_D.PHONE,p_border => C_BORDER,p_clipping => FALSE,p_align => 'L',p_ln => 0);	
		
		
	 IF P_OPTION = 'TEMPLATE' THEN	         		
			 NULL;
	 ELSE
		plpdf.SendDoc (p_blob => l_blob);
   END IF;        
	 
		RETURN l_blob;

	END F_GET_PDF;

  ----------------------------------------------------------------------
  FUNCTION RUN_REPORT(P_PAR_01 VARCHAR2 DEFAULT NULL
											,P_PAR_02 VARCHAR2 DEFAULT NULL
											,P_PAR_03 VARCHAR2 DEFAULT NULL
											,P_PAR_04 VARCHAR2 DEFAULT NULL
											,P_PAR_05 VARCHAR2 DEFAULT NULL
											,P_PAR_06 VARCHAR2 DEFAULT NULL
											,P_PAR_07 VARCHAR2 DEFAULT NULL
											,P_PAR_08 VARCHAR2 DEFAULT NULL
											,P_PAR_09 VARCHAR2 DEFAULT NULL
											,P_PAR_10 VARCHAR2 DEFAULT NULL) RETURN LAWSUP.PKG_FILES.REC_DOC

      IS
        		PROGRAM_UNIT VARCHAR2(200) DEFAULT 'pkg_pdf_001.RUN_REPORT';
            L_BLOB LAWSUP.PKG_FILES.REC_DOC;
            s_filename VARCHAR2(200) := 'Pretenziya';
    BEGIN
        
				DBMS_LOB.createtemporary(lob_loc => L_BLOB.P_BLOB,cache => TRUE);
        L_BLOB.P_FILE_NAME := s_filename;      
				P_ADD_DATA(P_PAR_01);
				L_BLOB.P_BLOB := F_GET_PDF(P_PAR_02);
        
				RETURN L_BLOB;
 /*  EXCEPTION 
		 WHEN OTHERS THEN
			  RAISE_APPLICATION_ERROR(-20200,DBMS_UTILITY.format_error_backtrace);   */
				
  END RUN_REPORT;

END PKG_PDF_001;
/

