i�?create or replace package lawmain.PKG_XLS_REPORT_002 is

  -- Author  : BES
  -- Created : 17.02.2017
  -- Purpose : Расчет суммы пени за просрочку платежей по договору для ВИВ
	-- Code:    PENY_CALC_UL 
	
	G$MAX_INT NUMBER := 2147483647;
  
  TYPE R_DATA IS RECORD (DATE_CHANGE DATE, VAL NUMBER);
  TYPE ARR_DATA IS TABLE OF R_DATA INDEX BY PLS_INTEGER;
  TYPE R_CALC_PENY_PERIOD IS RECORD (DATE_START DATE, DATE_END DATE, PENY_RATE NUMBER, PENY_PRC NUMBER,SUM_PAY NUMBER, TYPE_CALC VARCHAR2(50));
  TYPE PERIOD_CALC IS TABLE OF R_CALC_PENY_PERIOD;
  TYPE R_CALC_PERIOD IS RECORD(DEBT_DATE DATE, SUM_DOC NUMBER, NUM_DOC VARCHAR2(100), EXT_ID VARCHAR2(50), CALC PERIOD_CALC);
  TYPE PENY_CALC IS TABLE OF R_CALC_PERIOD INDEX BY PLS_INTEGER;
  
  FUNCTION F_GET_DEBT_DATE(P_DATE DATE) RETURN DATE;
  
  
  
  
  
  FUNCTION RUN_REPORT(P_PAR_01 VARCHAR2 DEFAULT NULL
                      ,P_PAR_02 VARCHAR2 DEFAULT NULL
                      ,P_PAR_03 VARCHAR2 DEFAULT NULL
                      ,P_PAR_04 VARCHAR2 DEFAULT NULL
                      ,P_PAR_05 VARCHAR2 DEFAULT NULL
                      ,P_PAR_06 VARCHAR2 DEFAULT NULL
                      ,P_PAR_07 VARCHAR2 DEFAULT NULL
                      ,P_PAR_08 VARCHAR2 DEFAULT NULL
                      ,P_PAR_09 VARCHAR2 DEFAULT NULL
                      ,P_PAR_10 VARCHAR2 DEFAULT NULL) RETURN LAWSUP.PKG_FILES.REC_DOC;    

end PKG_XLS_REPORT_002;
/

create or replace package body lawmain.PKG_XLS_REPORT_002 is

	TYPE REC_DOC IS RECORD(	P_BLOB BLOB DEFAULT NULL );

	EXCEL_DOC REC_DOC := NULL;
------------------------------------------------------------------------

  FUNCTION F_GET_DEBT_DATE(P_DATE DATE) RETURN DATE
		IS
	BEGIN
		IF PKG_CALENDAR.F_CHK_BUISNESS_DAY(P_DATE) THEN
			RETURN P_DATE;
		ELSE            
			RETURN F_GET_DEBT_DATE(P_DATE+1);
    END IF;
  END;

------------------------------------------------------------------------

  PROCEDURE p$s( ps_value IN CLOB )  -- отправляет накопленное значение ps_value в буфер и записывает
  IS
    BUFFER      RAW(32767);
    l_offset    NUMBER := 1;
    BUF_SIZE    NUMBER := 8000;
    BUF_VAR    VARCHAR2(32767);
  BEGIN
    LOOP
    EXIT WHEN L_OFFSET > DBMS_LOB.getlength(PS_VALUE);
    BUF_VAR := DBMS_LOB.substr(PS_VALUE,BUF_SIZE,L_OFFSET);
    BUFFER := UTL_RAW.cast_to_raw( CONVERT( BUF_VAR, 'UTF8'/*'CL8MSWIN1251'*/ ) );
    DBMS_LOB.WRITEAPPEND( EXCEL_DOC.P_BLOB, UTL_RAW.LENGTH( BUFFER ), BUFFER );
    L_OFFSET := L_OFFSET + BUF_SIZE;
    END LOOP;
  END;
------------------------------------------------------------------------
  PROCEDURE P_ADD_DATA(
  P_ID_CASE NUMBER
  ) IS
    L_XML CLOB;
    l_contract_number t_contracts.ctr_number%TYPE;
    l_ctr_date DATE;
    l_peny_date DATE; --дата формирования дела, на которую считается пени
    l_group_code VARCHAR2(50); --группа, к которой относится клиент
    l_sum       NUMBER := 0;
    l_prc_refin NUMBER; --процент рефинансирования
     l_total         NUMBER := 0;
    l_date_on       DATE;
    l_curator       VARCHAR2(50);
    l_row_cnt       NUMBER := 0;
    l_row_num       NUMBER := 0;       
    l_id_refin      NUMBER;   
    l_min_per       NUMBER;  
    l_sum_pay       NUMBER;
    l_debt_date     DATE;  
    l_debt          NUMBER;   
    l_prev_date     DATE;    
    l_rate          NUMBER;
    l_peny_refin_rate NUMBER;
    L_CNT NUMBER := 0;         
    L_CALC PENY_CALC;   
    L_CALC_IDX NUMBER := 0;
    L_PAYS ARR_DATA;
    L_RATES ARR_DATA;   
    L_PENY_GROUP VARCHAR2(50);    
    L_KOR        NUMBER;  
  BEGIN
    L_XML :=
      '<?xml version="1.0"?>
<?mso-application progid="Excel.Sheet"?>
<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:o="urn:schemas-microsoft-com:office:office"
 xmlns:x="urn:schemas-microsoft-com:office:excel"
 xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:html="http://www.w3.org/TR/REC-html40">
 <DocumentProperties xmlns="urn:schemas-microsoft-com:office:office">
  <Author></Author>
  <LastAuthor></LastAuthor>
  <LastPrinted>2017-10-25T11:02:36Z</LastPrinted>
  <Created>2016-03-14T07:23:01Z</Created>
  <LastSaved>2017-10-27T12:25:52Z</LastSaved>
  <Version>16.00</Version>
 </DocumentProperties>
 <OfficeDocumentSettings xmlns="urn:schemas-microsoft-com:office:office">
  <AllowPNG/>
 </OfficeDocumentSettings>
 <ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel">
  <WindowHeight>8055</WindowHeight>
  <WindowWidth>21570</WindowWidth>
  <WindowTopX>0</WindowTopX>
  <WindowTopY>0</WindowTopY>
  <ProtectStructure>False</ProtectStructure>
  <ProtectWindows>False</ProtectWindows>
 </ExcelWorkbook>
 <Styles>
  <Style ss:ID="Default" ss:Name="Normal">
   <Alignment ss:Vertical="Bottom"/>
   <Borders/>
   <Font ss:FontName="Arial Cyr" x:CharSet="204" x:Family="Swiss"/>
   <Interior/>
   <NumberFormat/>
   <Protection/>
  </Style>
  <Style ss:ID="m321210644">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="12"/>
   <NumberFormat ss:Format="@"/>
  </Style>   
  <Style ss:ID="s61">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="1" x:Family="Roman" ss:Size="12"/>
   <NumberFormat ss:Format="Percent"/>
  </Style>
  <Style ss:ID="s62">
   <Alignment ss:Vertical="Top" ss:WrapText="1"/>
  </Style>
  <Style ss:ID="s64">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders/>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="14" ss:Bold="1"/>
  </Style>
  <Style ss:ID="s65">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
  </Style>
  <Style ss:ID="s66">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="12"/>
  </Style>
  <Style ss:ID="s67">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="12"/>
  </Style>
  <Style ss:ID="s68">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="1" x:Family="Roman" ss:Size="12"/>
  </Style>
  <Style ss:ID="s69">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="12"/>
  </Style>
  <Style ss:ID="s77">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="12"/>
   <NumberFormat ss:Format="Standard"/>
  </Style>
  <Style ss:ID="s78">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="12"/>
   <NumberFormat ss:Format="Standard"/>
  </Style>
  <Style ss:ID="s79">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="12"/>
   <NumberFormat ss:Format="Short Date"/>
  </Style>
  <Style ss:ID="s80">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="12"/>
   <NumberFormat/>
  </Style>
  <Style ss:ID="s81">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="12"/>
   <NumberFormat ss:Format="Percent"/>
  </Style>
  <Style ss:ID="s82">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="1" x:Family="Roman" ss:Size="12"/>
   <NumberFormat ss:Format="Standard"/>
  </Style>
  <Style ss:ID="s83">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="12"/>
   <NumberFormat ss:Format="Standard"/>
  </Style>
  <Style ss:ID="s84">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="12"/>
   <NumberFormat ss:Format="Standard"/>
  </Style>
  <Style ss:ID="s85">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="12"/>
   <NumberFormat ss:Format="Short Date"/>
  </Style>
  <Style ss:ID="s86">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="12"/>
   <NumberFormat/>
  </Style>
  <Style ss:ID="s88">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="12"/>
   <NumberFormat ss:Format="@"/>
  </Style>
  <Style ss:ID="s90">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="1" x:Family="Roman" ss:Size="12"/>
   <NumberFormat ss:Format="Standard"/>
  </Style>
  <Style ss:ID="s91">
   <Alignment ss:Horizontal="Center" ss:Vertical="Top" ss:WrapText="1"/>
  </Style>
  <Style ss:ID="s92">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="12" ss:Bold="1"/>
   <NumberFormat ss:Format="@"/>
  </Style>
  <Style ss:ID="s93">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="12"/>
  </Style>
  <Style ss:ID="s94">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
  </Style>
  <Style ss:ID="s95">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="12"/>
   <NumberFormat ss:Format="Fixed"/>
  </Style>
  <Style ss:ID="s96">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="12"/>
   <NumberFormat ss:Format="Short Date"/>
  </Style>
  <Style ss:ID="s97">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="12"/>
   <NumberFormat ss:Format="0"/>
  </Style>
  <Style ss:ID="s98">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="12"/>
   <NumberFormat ss:Format="Percent"/>
  </Style>
  <Style ss:ID="s99">
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="1" x:Family="Roman" ss:Size="12"
    ss:Bold="1"/>
  </Style>
  <Style ss:ID="s100">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="12" ss:Bold="1"/>
   <NumberFormat ss:Format="Standard"/>
  </Style>
  <Style ss:ID="s102">
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="12"/>
  </Style>
  <Style ss:ID="s103">
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="14"/>
  </Style>
  <Style ss:ID="s104">
   <Font ss:FontName="Arial Cyr" x:CharSet="204" x:Family="Swiss" ss:Size="14"/>
  </Style>
  <Style ss:ID="s105">
   <Alignment ss:Horizontal="Center" ss:Vertical="Top" ss:WrapText="1"/>
   <Font ss:FontName="Arial Cyr" x:CharSet="204" x:Family="Swiss" ss:Size="14"/>
  </Style>
  <Style ss:ID="s106">
   <Alignment ss:Vertical="Top" ss:WrapText="1"/>
   <Borders/>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="12" ss:Bold="1"/>
  </Style>
  <Style ss:ID="s107">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders/>
  </Style>
 </Styles>
 <Worksheet ss:Name="ставка">
  <Table ss:ExpandedColumnCount="15" ss:ExpandedRowCount="100000" x:FullColumns="1"
   x:FullRows="1">
   <Column ss:AutoFitWidth="0" ss:Width="125"/>
   <Column ss:AutoFitWidth="0" ss:Width="82"/>
   <Column ss:AutoFitWidth="0" ss:Width="71.25"/>
   <Column ss:AutoFitWidth="0" ss:Width="84.75"/>
   <Column ss:AutoFitWidth="0" ss:Width="77.25" ss:Span="1"/>
   <Column ss:Index="7" ss:AutoFitWidth="0" ss:Width="60.75"/>
   <Column ss:AutoFitWidth="0" ss:Width="200"/>
   <Column ss:AutoFitWidth="0" ss:Width="112.5"/>
   <Column ss:AutoFitWidth="0" ss:Width="114"/>';
   p$s(L_XML);
   
   
   --Пишем группу потребителей в l_group_code
  --дата формирования отчета в l_date_on
    SELECT S.ABON_GROUP,S.PENY_DATE
    INTO  l_group_code,L_DATE_ON
    FROM  T_CLI_REST S
    WHERE S.ID_REST = P_ID_CASE;      
    
  
    --ТЕКУЩАЯ СТАВКА РЕФЕНАНСИРОВАНИЯ НА ДЕНЬ РАСЧЕТА
    SELECT R.ID_REFIN,R.PRC_REF 
    INTO L_ID_REFIN,l_prc_refin
    FROM T_REFIN R 
    WHERE L_DATE_ON BETWEEN  R.DATE_REF_BEG AND NVL(R.DATE_REF_END,L_DATE_ON);

    --КОЛИЧЕСТВО ДНЕЙ С МОМЕНТА ВОЗНИКНОВЕНИЯ ЗАДОЛЖЕННСОТИ НАЧИНАЕТСЯ РАСЧЕТ ПЕНИ
    SELECT MIN(RA.DAYS_FROM) INTO l_min_per
    FROM T_PENY_RATE RA
    WHERE RA.ID_REFIN = l_id_refin
    AND   RA.ABON_GROUP = L_GROUP_CODE;

  

   L_XML:='  <Row ss:Index="4" ss:AutoFitHeight="0" ss:Height="50.25" ss:StyleID="s62">';
   L_XML:= L_XML||' <Cell ss:MergeAcross="9" ss:StyleID="s64"><Data ss:Type="String">Расчет суммы пени по состоянию на '||TO_CHAR(l_date_on,'DD.MM.YYYY')||' (c НДС)</Data></Cell>';
   L_XML:= L_XML||'</Row>';
   p$s(L_XML);

   L_XML:='<Row ss:AutoFitHeight="0" ss:Height="66.75" ss:StyleID="s65">
    <Cell ss:StyleID="s66"><Data ss:Type="String">Период</Data></Cell>
    <Cell ss:StyleID="s67"><Data ss:Type="String">Задолженность</Data></Cell>
    <Cell ss:StyleID="s67"><Data ss:Type="String">с</Data></Cell>
    <Cell ss:StyleID="s67"><Data ss:Type="String">по</Data></Cell>
    <Cell ss:StyleID="s67"><Data ss:Type="String">дней</Data></Cell>
    <Cell ss:StyleID="s67"><Data ss:Type="String">Ставка</Data></Cell>
    <Cell ss:StyleID="s68"><Data ss:Type="String">Доля ставка </Data></Cell>  
    <Cell ss:StyleID="s68"><Data ss:Type="String">Формула </Data></Cell>  
    <Cell ss:StyleID="s69"><Data ss:Type="String">Пени</Data></Cell>
    <Cell ss:StyleID="s69"><Data ss:Type="String">Итого</Data></Cell>
   </Row>';
   p$s(L_XML);
  
  -- ПОДГОТАВЛИВАЕМ ДАННЫЕ
  FOR j IN (SELECT F_GET_DEBT_DATE(DECODE(L_GROUP_CODE
                                         ,'UK',LAST_DAY(F.DOC_DATE)+15
                                         ,'TSG',LAST_DAY(F.DOC_DATE)+15
                                         ,LAST_DAY(F.DOC_DATE)+10)
                                  )+1 DEBT_START,   
                     
                    F.AMOUNT , F.EXT_ID, F.DOC_NUMBER
            FROM T_CLI_DEBTS D,
                 T_CLI_FINDOCS F  
            WHERE F.ID_DOC = D.ID_DOC
            AND   D.ID_WORK = P_ID_CASE 
            ORDER BY  F.DOC_DATE)
  LOOP                       
  
    L_CNT := L_CNT + 1;
    L_CALC(L_CNT).DEBT_DATE := J.DEBT_START;
    L_CALC(L_CNT).NUM_DOC   := J.DOC_NUMBER;
    L_CALC(L_CNT).EXT_ID    := J.EXT_ID;             
    L_CALC(L_CNT).SUM_DOC   := J.AMOUNT;
    L_CALC(L_CNT).CALC := PERIOD_CALC();
    
    FOR Z IN (                              
    SELECT T.DATE_CHNG, NVL(LEAD(T.DATE_CHNG-1,1) OVER (ORDER BY T.DATE_CHNG), L_DATE_ON) DATE_TO,
           T.RATE, NULL PENY_PRC,T.PAY_SUM,T.TYPE_CALC     
    FROM (SELECT TRUNC(C.DATE_CON,'DD') DATE_CHNG,  SUM(CASE 
									 WHEN C.DOC_TYPE = 'Изменение состояния долга' THEN C.AMOUNT
									 ELSE  ABS(C.AMOUNT)
								  END) PAY_SUM, 0 RATE, 'PAYMENT' TYPE_CALC
           FROM   T_CLI_FINDOC_CONS C                  
           WHERE C.EXT_ID_TO != C.EXT_ID_FROM
           AND   (C.EXT_ID_TO = J.EXT_ID OR C.EXT_ID_FROM = J.EXT_ID) 
           AND   TRUNC(C.DATE_CON,'DD') <= L_DATE_ON                   
           GROUP BY TRUNC(C.DATE_CON,'DD')  -- ВСЕ ПОСТУПЛЕНИЯ ДС ПО ЗАДОЛЖЕННОСТИ
           UNION                              
           SELECT J.DEBT_START + r.days_from - 1 DATE_CHNG, 0 PAY_SUM, r.rate , 'RATE' TYPE_CALC
           FROM   T_PENY_RATE  r                        
           WHERE  r.abon_group = NVL(L_PENY_GROUP,L_GROUP_CODE)
           AND    r.id_refin = l_id_refin
           AND    r.days_from + J.DEBT_START <= TRUNC(L_DATE_ON,'DD')
           )T
    ORDER BY T.DATE_CHNG ASC, T.TYPE_CALC ASC)
    LOOP                                                                                                      
      IF Z.TYPE_CALC = 'PAYMENT' THEN     
        L_CALC(L_CNT).CALC.EXTEND;
        L_CALC_IDX := L_CALC(L_CNT).CALC.COUNT;
        
        L_CALC(L_CNT).CALC(L_CALC_IDX).DATE_START := Z.DATE_CHNG;
        L_CALC(L_CNT).CALC(L_CALC_IDX).SUM_PAY    := Z.PAY_SUM;                                    
        L_CALC(L_CNT).CALC(L_CALC_IDX).TYPE_CALC  := Z.TYPE_CALC;
        L_CALC(L_CNT).CALC(L_CALC_IDX).PENY_PRC  := l_prc_refin;
        
        IF L_CALC(L_CNT).CALC.EXISTS(L_CALC_IDX-1) AND L_CALC(L_CNT).CALC(L_CALC_IDX-1).TYPE_CALC != 'PAYMENT'  THEN          
          L_CALC(L_CNT).CALC(L_CALC_IDX-1).DATE_END := L_CALC(L_CNT).CALC(L_CALC_IDX-1).DATE_END+1;
          L_CALC(L_CNT).CALC.EXTEND;
          L_CALC_IDX := L_CALC(L_CNT).CALC.COUNT;        
          L_CALC(L_CNT).CALC(L_CALC_IDX).DATE_START  := Z.DATE_CHNG+1;
          L_CALC(L_CNT).CALC(L_CALC_IDX).DATE_END  := Z.DATE_TO;
          L_CALC(L_CNT).CALC(L_CALC_IDX).PENY_RATE := NVL(L_CALC(L_CNT).CALC(L_CALC_IDX-2).PENY_RATE,0);
          L_CALC(L_CNT).CALC(L_CALC_IDX).TYPE_CALC  := 'CALC';  
          L_CALC(L_CNT).CALC(L_CALC_IDX).PENY_PRC  := l_prc_refin;      
        END IF;
        

      ELSE
        L_CALC(L_CNT).CALC.EXTEND;
        L_CALC_IDX := L_CALC(L_CNT).CALC.COUNT;        
        L_CALC(L_CNT).CALC(L_CALC_IDX).DATE_START := Z.DATE_CHNG;
        L_CALC(L_CNT).CALC(L_CALC_IDX).DATE_END   := Z.DATE_TO;
        L_CALC(L_CNT).CALC(L_CALC_IDX).TYPE_CALC  := Z.TYPE_CALC;        
        L_CALC(L_CNT).CALC(L_CALC_IDX).PENY_RATE  := Z.RATE;
        L_CALC(L_CNT).CALC(L_CALC_IDX).PENY_PRC   := l_prc_refin;
      END IF;      
    END LOOP;
                                       
   END LOOP;  
    
   FOR I IN 1..L_CALC.COUNT LOOP   
     L_SUM := L_CALC(I).SUM_DOC;
     L_XML := '<Row ss:AutoFitHeight="0" ss:Height="39.75" ss:StyleID="s65">';
     L_XML := L_XML||'<Cell ss:MergeDown="'||TO_CHAR(L_CALC(I).CALC.COUNT-1)||'" ss:StyleID="m321210644"><Data ss:Type="String">Сч./ф. № '||L_CALC(I).NUM_DOC||chr(13)||
                                              TO_CHAR(TRUNC(L_CALC(I).DEBT_DATE,'MM')-1,'fmMonth YYYY')||'</Data></Cell>';        
     l_row_num := l_row_num + L_CALC(I).CALC.COUNT;                                          
     FOR J IN 1..L_CALC(I).CALC.COUNT LOOP                                         
       IF L_CALC(I).CALC(J).TYPE_CALC != 'PAYMENT' THEN    
         IF J != 1 THEN       
            L_XML := '<Row ss:AutoFitHeight="0" ss:Height="39.75" ss:StyleID="s65">';
         END IF;
          L_XML := L_XML||'<Cell ss:Index="2" ss:StyleID="s77"><Data ss:Type="Number">'||REPLACE(TO_CHAR(L_SUM),',','.')||'</Data></Cell>';
          L_XML := L_XML||'<Cell ss:StyleID="s79"><Data ss:Type="DateTime">'||TO_CHAR(L_CALC(I).CALC(J).DATE_START,'YYYY-MM-DD')||'T00:00:00.000</Data></Cell>';
         L_XML := L_XML||'<Cell ss:StyleID="s79"><Data ss:Type="DateTime">'||TO_CHAR(L_CALC(I).CALC(J).DATE_END,'YYYY-MM-DD')||'T00:00:00.000</Data></Cell>';
         L_XML := L_XML||'<Cell ss:StyleID="s80" ss:Formula="=RC[-1]-RC[-2]+1"><Data ss:Type="Number"></Data></Cell>';
         L_XML := L_XML||'<Cell ss:StyleID="s61"><Data ss:Type="Number">'||REPLACE(TO_CHAR(L_CALC(I).CALC(J).PENY_PRC/100),',','.')||'</Data></Cell>';
          IF L_CALC(I).CALC(J).PENY_RATE = 0 THEN
           L_XML := L_XML||'<Cell ss:StyleID="s82"><Data ss:Type="String">0</Data></Cell>';  
           L_XML := L_XML||'<Cell ss:StyleID="s83" ss:Formula="=CONCATENATE(RC[-6],&quot;*&quot;,RC[-3],&quot;*&quot;,RC[-2],&quot;*&quot;,RC[-1])"><Data ss:Type="String">0</Data></Cell>';
           L_XML := L_XML||'<Cell ss:StyleID="s83" ss:Formula="=SUM(RC[-7]*0*RC[-3]*RC[-4])"><Data ss:Type="Number"></Data></Cell>';                   
         ELSE
            L_XML := L_XML||'<Cell ss:StyleID="s82"><Data ss:Type="String">'||'1/'||TO_CHAR(L_CALC(I).CALC(J).PENY_RATE)||'</Data></Cell>';  
           L_XML := L_XML||'<Cell ss:StyleID="s83" ss:Formula="=CONCATENATE(RC[-6],&quot;*&quot;,RC[-3],&quot;*&quot;,RC[-2],&quot;*&quot;,RC[-1])"><Data ss:Type="String">0</Data></Cell>';
           L_XML := L_XML||'<Cell ss:StyleID="s83" ss:Formula="=SUM(RC[-7]/'||trim(to_char(L_CALC(I).CALC(J).PENY_RATE,'999999990D9','NLS_NUMERIC_CHARACTERS=''. '''))||'*RC[-3]*RC[-4])"><Data ss:Type="Number"></Data></Cell>';                   
         END IF;
      
       ELSE                                                                                              
           IF J != 1 THEN       
             L_XML := '<Row ss:AutoFitHeight="0" ss:Height="39.75" ss:StyleID="s65">';
           END IF;
          L_SUM := L_SUM - L_CALC(I).CALC(J).SUM_PAY;
          L_XML := L_XML||'<Cell ss:Index="2" ss:StyleID="s77"><Data ss:Type="Number">'||REPLACE(TO_CHAR(L_CALC(I).CALC(J).SUM_PAY),',','.')||'</Data></Cell>
                           <Cell ss:StyleID="s79"><Data ss:Type="DateTime">'||TO_CHAR(L_CALC(I).CALC(J).DATE_START,'YYYY-MM-DD')||'T00:00:00.000</Data></Cell>      
                           <Cell ss:MergeAcross="5" ss:StyleID="s77"><Data ss:Type="String">Погашение части долга</Data></Cell>'; 
             
       END IF;             
			 IF J = 1 THEN
				 L_XML :=  L_XML||'<Cell ss:MergeDown="'||TO_CHAR(L_CALC(I).CALC.COUNT-1)||'" ss:StyleID="s83" ss:Formula="=SUM(RC[-1]:R['||TO_CHAR(L_CALC(I).CALC.COUNT-1)||']C[-1])"><Data ss:Type="Number">0</Data></Cell>';
			 END IF;
			 L_XML := L_XML||'</Row>';   
			  p$s( L_XML );
     END LOOP;        
		
   END LOOP;
    
    SELECT SUM(DECODE(FDC.OPER_TYPE,'Приход',FDC.AMOUNT,-FDC.AMOUNT))
    INTO L_DEBT
    FROM T_CLI_DEBTS D
        ,T_CLI_FINDOCS FD
        ,T_CLI_FINDOC_CONS FDC
    WHERE D.ID_WORK = P_ID_CASE
    AND   FD.ID_DOC = D.ID_DOC
    AND   FDC.EXT_ID_TO = FD.EXT_ID
    AND   FDC.DATE_CON <= L_DATE_ON;

    L_XML := ' <Row ss:AutoFitHeight="0" ss:Height="26.25" ss:StyleID="s91">
                  <Cell ss:MergeAcross="1" ss:StyleID="s92"><Data ss:Type="String">Сумма долга</Data></Cell>
                  <Cell ss:StyleID="s94"/>
                  <Cell ss:StyleID="s95"/>
                  <Cell ss:StyleID="s96"/>
                  <Cell ss:StyleID="s96"/>
                  <Cell ss:StyleID="s97"/>
                  <Cell ss:StyleID="s97"/>
                  <Cell ss:StyleID="s100"><Data ss:Type="Number">'||REPLACE(TO_CHAR(L_DEBT),',','.')||'</Data></Cell>
                 </Row>
               <Row ss:AutoFitHeight="0" ss:Height="26.25" ss:StyleID="s91">
                  <Cell ss:MergeAcross="1" ss:StyleID="s92"><Data ss:Type="String">Сумма неустойки</Data></Cell>
                  <Cell ss:StyleID="s94"/>
                  <Cell ss:StyleID="s95"/>
                  <Cell ss:StyleID="s96"/>
                  <Cell ss:StyleID="s96"/>
                  <Cell ss:StyleID="s97"/>
                  <Cell ss:StyleID="s97"/>
                  <Cell ss:StyleID="s100" ss:Formula="=SUM(R[-'||to_char(l_row_num+1)||']C:R[-2]C)"><Data ss:Type="Number"></Data></Cell>
                 </Row>';

    
    p$s( L_XML );
  

    --куратор 
    FOR z IN(
    SELECT substr(ul.first_name,1,1)||'.'||substr(ul.second_name,1,1)||'.'||INITCAP(ul.last_name) AS CURATOR
    FROM  t_cli_cases cc,t_user_list ul
    WHERE ul.id = cc.curator_COURT
    AND cc.id_case = P_ID_CASE)
    LOOP
      l_curator := z.curator;
    END LOOP;
    

   L_XML := ' <Row ss:AutoFitHeight="0" ss:Height="26.25" ss:StyleID="s91">
    <Cell ss:StyleID="Default"/>
    <Cell ss:StyleID="Default"/>
    <Cell ss:StyleID="Default"/>
    <Cell ss:StyleID="Default"/>
    <Cell ss:StyleID="Default"/>
    <Cell ss:StyleID="Default"/>
    <Cell ss:StyleID="Default"/>
    <Cell ss:StyleID="Default"/>
    <Cell ss:StyleID="s102"/>
    <Cell ss:StyleID="Default"/>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="26.25" ss:StyleID="s91">
    <Cell ss:StyleID="Default"/>
    <Cell ss:StyleID="s103"><Data ss:Type="String">Представитель по доверенности</Data></Cell>
    <Cell ss:StyleID="s104"/>
    <Cell ss:StyleID="s104"/>
    <Cell ss:StyleID="s105"/>
    <Cell ss:StyleID="s104"/>
    <Cell ss:StyleID="s104"/>
    <Cell ss:StyleID="s103"><Data ss:Type="String">'||l_curator||'</Data></Cell>
    <Cell ss:StyleID="s104"/>
    <Cell ss:StyleID="Default"/>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="35.25"/>
  </Table>
 </Worksheet>
</Workbook>';

    p$s( L_XML );
  
  
  END P_ADD_DATA;

---------------------------------------------------------------------------------------------------------------------------------

  FUNCTION RUN_REPORT(P_PAR_01 VARCHAR2 DEFAULT NULL
                      ,P_PAR_02 VARCHAR2 DEFAULT NULL
                      ,P_PAR_03 VARCHAR2 DEFAULT NULL
                      ,P_PAR_04 VARCHAR2 DEFAULT NULL
                      ,P_PAR_05 VARCHAR2 DEFAULT NULL
                      ,P_PAR_06 VARCHAR2 DEFAULT NULL
                      ,P_PAR_07 VARCHAR2 DEFAULT NULL
                      ,P_PAR_08 VARCHAR2 DEFAULT NULL
                      ,P_PAR_09 VARCHAR2 DEFAULT NULL
                      ,P_PAR_10 VARCHAR2 DEFAULT NULL) RETURN LAWSUP.PKG_FILES.REC_DOC
  IS
  l_BLOB LAWSUP.PKG_FILES.REC_DOC;
  BEGIN

    EXCEL_DOC := NULL;
    DBMS_LOB.CREATETEMPORARY( EXCEL_DOC.P_BLOB, TRUE );
    DBMS_LOB.OPEN( EXCEL_DOC.P_BLOB, DBMS_LOB.LOB_READWRITE );

    P_ADD_DATA(TO_NUMBER(P_PAR_01));

    DBMS_LOB.CLOSE(EXCEL_DOC.P_BLOB);

        l_BLOB.P_BLOB := EXCEL_DOC.P_BLOB;
        l_BLOB.P_FILE_NAME := 'RASCHET_PENY_'||P_PAR_01;
        RETURN L_BLOB;

  END RUN_REPORT;

end PKG_XLS_REPORT_002;
/

