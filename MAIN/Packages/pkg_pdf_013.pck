i�?CREATE OR REPLACE PACKAGE LAWMAIN.PKG_PDF_013
  IS
 -- Author  : EUGEN
 -- Created : 13.12.2016 
 -- Purpose : Претензия для юридического лица
 -- Code    : SHUT_DETAILS_SEW_MASS
  FUNCTION RUN_REPORT(P_PAR_01 VARCHAR2 DEFAULT NULL
											,P_PAR_02 VARCHAR2 DEFAULT NULL
                      ,P_PAR_03 VARCHAR2 DEFAULT NULL
                      ,P_PAR_04 VARCHAR2 DEFAULT NULL
                      ,P_PAR_05 VARCHAR2 DEFAULT NULL
                      ,P_PAR_06 VARCHAR2 DEFAULT NULL
                      ,P_PAR_07 VARCHAR2 DEFAULT NULL
                      ,P_PAR_08 VARCHAR2 DEFAULT NULL
                      ,P_PAR_09 VARCHAR2 DEFAULT NULL
                      ,P_PAR_10 VARCHAR2 DEFAULT NULL) RETURN LAWSUP.PKG_FILES.REC_DOC;
END PKG_PDF_013;
/

CREATE OR REPLACE PACKAGE BODY LAWMAIN.PKG_PDF_013
    IS
  ----------------------------------------------------------------------
  FUNCTION AD(P1 IN VARCHAR2,P2 IN VARCHAR2) RETURN VARCHAR2
    IS
  BEGIN
    IF P2 IS NOT NULL THEN
      RETURN p1;
    END IF;
    RETURN p2;
  END;
 
	FUNCTION F_GET_PDF(P_ID_FOLDER NUMBER) RETURN BLOB
	IS
		 l_blob blob;

    l_ttf_t        Plpdf_Type.t_addfont;
    l_ttf_tbd      Plpdf_Type.t_addfont;   
     
    l$_tw_id    NUMBER          := PLPDF_FONTS.l$_tw_id;
    l$_tw       VARCHAR2(40)    := PLPDF_FONTS.l$_tw; 
    l$_twbd_id    NUMBER        := PLPDF_FONTS.l$_twbd_id; 
    l$_twbd     VARCHAR2(40)    := PLPDF_FONTS.l$_twbd;
    
    v_tpl_1   plpdf_type.tr_tpl_data;
    l_tpl_id NUMBER;    
		v_tpl_2   plpdf_type.tr_tpl_data;
		l_tpl_id2 NUMBER;
    C_BORDER NUMBER := 0;
    L_W NUMBER;
    L_H NUMBER;
    L_Y NUMBER;
		l_page NUMBER;
    L_IMG BLOB;
    L_RET LAWSUP.PKG_FILES.REC_DOC;
  BEGIN
			
	    DBMS_LOB.createtemporary(L_BLOB,TRUE);
	    
			Plpdf.init;
      Plpdf.setEncoding(p_enc => 'CP1251');

      l_ttf_t       :=  Plpdf_Ttf.GetTTF(l$_tw_id);
      l_ttf_tbd     :=  Plpdf_Ttf.GetTTF(l$_twbd_id);
      plpdf.addTTF(p_family => l$_tw,p_data => l_ttf_t);
      plpdf.addTTF(p_family => l$_twbd,p_data => l_ttf_tbd);              
      
			
      v_tpl_1 := PLPDF_PARSER.LoadTemplate(p_id => 1);
      l_tpl_id := PLPDF.InsTemplate(v_tpl_1);                
			
				  
		FOR I IN (SELECT C.ID_WORK FROM T_CLI_SHUTDOWN C WHERE C.ID_FOLDER = P_ID_FOLDER )
			LOOP      
			    Plpdf.NewPage;
	      	plpdf.setPageTemplate(p_tplidx => l_tpl_id);														
					L_RET := PKG_PDF_012.RUN_REPORT(P_PAR_01 => I.ID_WORK,P_PAR_02 => 'TEMPLATE'); 						
			END LOOP;	
			  
		plpdf.SendDoc (p_blob => l_blob);      
	 
		RETURN l_blob;
	END F_GET_PDF;
  ----------------------------------------------------------------------
  FUNCTION RUN_REPORT(P_PAR_01 VARCHAR2 DEFAULT NULL
										  ,P_PAR_02 VARCHAR2 DEFAULT NULL
                      ,P_PAR_03 VARCHAR2 DEFAULT NULL
                      ,P_PAR_04 VARCHAR2 DEFAULT NULL
                      ,P_PAR_05 VARCHAR2 DEFAULT NULL
                      ,P_PAR_06 VARCHAR2 DEFAULT NULL
                      ,P_PAR_07 VARCHAR2 DEFAULT NULL
                      ,P_PAR_08 VARCHAR2 DEFAULT NULL
                      ,P_PAR_09 VARCHAR2 DEFAULT NULL
                      ,P_PAR_10 VARCHAR2 DEFAULT NULL) RETURN LAWSUP.PKG_FILES.REC_DOC
      IS
            PROGRAM_UNIT VARCHAR2(200) DEFAULT 'pkg_pdf_001.RUN_REPORT';
            L_BLOB LAWSUP.PKG_FILES.REC_DOC;
            s_filename VARCHAR2(200) := 'notification.pdf';
    BEGIN
        
        DBMS_LOB.createtemporary(lob_loc => L_BLOB.P_BLOB,cache => TRUE);
        L_BLOB.P_FILE_NAME := s_filename;
        L_BLOB.P_BLOB := F_GET_PDF(P_PAR_01);
        
        l_BLOB.P_FILE_NAME := 'NARYADI_'||P_PAR_01;
        RETURN L_BLOB;
  /* EXCEPTION 
     WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20200,DBMS_UTILITY.format_error_backtrace);     */
        
  END RUN_REPORT;
END PKG_PDF_013;
/

