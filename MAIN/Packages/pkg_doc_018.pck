i�?CREATE OR REPLACE PACKAGE LAWMAIN.PKG_DOC_018 IS

	-- Author  : BES
	-- Created : 14.02.2019
	-- Purpose : Заявление о признании должника банкротом
	-- CODE    : BNK_REG_CLAIM

	FUNCTION RUN_REPORT(P_PAR_01 VARCHAR2 DEFAULT NULL
										 ,P_PAR_02 VARCHAR2 DEFAULT NULL
										 ,P_PAR_03 VARCHAR2 DEFAULT NULL
										 ,P_PAR_04 VARCHAR2 DEFAULT NULL
										 ,P_PAR_05 VARCHAR2 DEFAULT NULL
										 ,P_PAR_06 VARCHAR2 DEFAULT NULL
										 ,P_PAR_07 VARCHAR2 DEFAULT NULL
										 ,P_PAR_08 VARCHAR2 DEFAULT NULL
										 ,P_PAR_09 VARCHAR2 DEFAULT NULL
										 ,P_PAR_10 VARCHAR2 DEFAULT NULL)
		RETURN LAWSUP.PKG_FILES.REC_DOC;

END PKG_DOC_018;
/

CREATE OR REPLACE PACKAGE BODY LAWMAIN.PKG_DOC_018 IS

	TYPE REC_DOC IS RECORD(
		 P_BLOB BLOB DEFAULT NULL);

	RTF_DOC REC_DOC := NULL;

  FUNCTION F_GET_COURT_TABLE(P_ID_BNK NUMBER) RETURN CLOB
    IS
    L_RET CLOB;   
    L_ROW_TMP CLOB; 
		L_HEADER CLOB;              
		L_FOOTER CLOB;
    L_FMT  VARCHAR2(50) := 'FM999G999G999G999G990D00';
    L_NLS  VARCHAR2(50) := 'NLS_NUMERIC_CHARACTERS='', ''';
		L_DOC_MRG BOOLEAN := FALSE;  
		L_DEBT_SUM NUMBER := 0;
		L_PENY_SUM NUMBER := 0;
		L_TOLL_SUM NUMBER := 0;
  BEGIN                                                                   
    L_HEADER := '{\rtlch\fcs1 \af1\afs24 \ltrch\fcs0\f1 \fs24\insrsid8145372\charrsid16126493 
\par \ltrrow}\trowd \irow0\irowband0\ltrrow\ts33\trgaph108\trrh865\trleft-142\trbrdrt\brdrs\brdrw10 \trbrdrl\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 \trbrdrr\brdrs\brdrw10 \trbrdrh\brdrs\brdrw10 \trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddft3\trpaddfb3\trpaddfr3\tblrsid14894895\tbllkhdrrows\tbllkhdrcols\tbllknocolband\tblind-34\tblindtype3 \clvertalc\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr
\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth1276\clshdrawnil \cellx1134\clvertalc\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth1701\clshdrawnil \cellx2835\clvertalc\clbrdrt
\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth1276\clshdrawnil \cellx4111\clvertalc\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1985\clshdrawnil \cellx6096\clvertalc\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth1134\clshdrawnil \cellx7230\clvertalc\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth1275\clshdrawnil \cellx8505\clvertalc\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1525\clshdrawnil \cellx10030\pard\plain \ltrpar\qc \li0\ri0\sl276\slmult1\widctlpar\intbl\wrapdefault\hyphpar0\aspalpha\aspnum\faauto\adjustright\rin0\lin0\pararsid12871775\yts33 \rtlch\fcs1 \af31507\afs22\alang1025 
\ltrch\fcs0\f1 \f31506\fs22\lang1049\langfe1033\cgrid\langnp1049\langfenp1033 {\rtlch\fcs1 \af237\afs20 \ltrch\fcs0\f1 \fs20\insrsid13763972\charrsid5179790 \''c4\''ee\''e3\''ee\''e2\''ee\''f0}{\rtlch\fcs1 \af1\afs20 \ltrch\fcs0\f1 
\f1\fs20\insrsid12791597\charrsid5179790 \cell }{\rtlch\fcs1 \af237\afs20 \ltrch\fcs0\f1 \fs20\insrsid12791597\charrsid5179790 \''cf\''e5\''f0\''e8\''ee\''e4 }{\rtlch\fcs1 \af237\afs20 \ltrch\fcs0\f1 \fs20\insrsid10971224\charrsid5179790 \''ed\''e5\''ef\''ee
\''e3\''e0\''f8\''e5\''ed\''ed\''ee\''e9 }{\rtlch\fcs1 \af237\afs20 \ltrch\fcs0\f1 \fs20\insrsid12791597\charrsid5179790 \''e7\''e0\''e4\''ee\''eb\''e6\''e5\''ed\''ed\''ee\''f1\''f2\''e8}{\rtlch\fcs1 \af1\afs20 \ltrch\fcs0\f1 \f1\fs20\insrsid12791597\charrsid5179790 \cell }{
\rtlch\fcs1 \af237\afs20 \ltrch\fcs0\f1 \fs20\insrsid12791597\charrsid5179790 \''cd\''ee\''ec\''e5\''f0 \''e4\''e5\''eb\''e0}{\rtlch\fcs1 \af1\afs20 \ltrch\fcs0\f1 \f1\fs20\insrsid12791597\charrsid5179790 \cell }{\rtlch\fcs1 \af237\afs20 \ltrch\fcs0\f1 
\fs20\insrsid12791597\charrsid5179790 \''d1\''f3\''ec\''ec\''e0 \''ed\''e5}{\rtlch\fcs1 \af1\afs20 \ltrch\fcs0\f1 \f1\fs20\insrsid13763972\charrsid5179790  }{\rtlch\fcs1 \af237\afs20 \ltrch\fcs0\f1 \fs20\insrsid12791597\charrsid5179790 \''ee\''ef\''eb\''e0\''f7
\''e5\''ed\''ed\''ee\''e3\''ee \''ef\''ee \''f0\''e5\''f8\''e5\''ed\''e8\''fe }{\rtlch\fcs1 \af237\afs20 \ltrch\fcs0\f1 \fs20\insrsid13763972\charrsid5179790 \''f1\''f3\''e4\''e0 }{\rtlch\fcs1 \af237\afs20 \ltrch\fcs0\f1 \fs20\insrsid12791597\charrsid5179790 \''ee\''f1
\''ed\''ee\''e2\''ed\''ee\''e3\''ee \''e4\''ee\''eb\''e3\''e0 \''e2 (\''f0\''f3\''e1.)}{\rtlch\fcs1 \af1\afs20 \ltrch\fcs0\f1 \f1\fs20\insrsid12791597\charrsid5179790 \cell }{\rtlch\fcs1 \af237\afs20 \ltrch\fcs0\f1 \fs20\insrsid12791597\charrsid5179790 \''d1\''f3\''ec\''ec
\''e0 \''ef\''e5\''ed\''e8 (\''f0\''f3\''e1.)}{\rtlch\fcs1 \af1\afs20 \ltrch\fcs0\f1 \f1\fs20\insrsid12791597\charrsid5179790 \cell }{\rtlch\fcs1 \af237\afs20 \ltrch\fcs0\f1 \fs20\insrsid12791597\charrsid5179790 \''d1\''f3\''ec\''ec\''e0 \''e3\''ee\''f1\''ef\''ee\''f8\''eb
\''e8\''ed\''fb (\''f0\''f3\''e1.)}{\rtlch\fcs1 \af1\afs20 \ltrch\fcs0\f1 \f1\fs20\insrsid12791597\charrsid5179790 \cell }{\rtlch\fcs1 \af237\afs20 \ltrch\fcs0\f1 \fs20\insrsid10971224\charrsid5179790 \''c4\''e0\''f2\''e0 \''e2\''fb\''ed\''e5\''f1\''e5\''ed\''e8\''ff \''f0
\''e5\''f8\''e5\''ed\''e8\''ff}{\rtlch\fcs1 \af1\afs20 \ltrch\fcs0\f1 \f1\fs20\insrsid12791597\charrsid5179790 \cell }\pard\plain \ltrpar\ql \li0\ri0\sl276\slmult1\widctlpar\intbl\wrapdefault\aspalpha\aspnum\faauto\adjustright\rin0\lin0 \rtlch\fcs1 
\af31507\afs22\alang1025 \ltrch\fcs0\f1 \f31506\fs22\lang1049\langfe1033\cgrid\langnp1049\langfenp1033 {\rtlch\fcs1 \af31507 \ltrch\fcs0\f1 \insrsid12791597\charrsid5179790 \trowd \irow0\irowband0\ltrrow\ts33\trgaph108\trrh865\trleft-142\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 \trbrdrr\brdrs\brdrw10 \trbrdrh\brdrs\brdrw10 \trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddft3\trpaddfb3\trpaddfr3\tblrsid14894895\tbllkhdrrows\tbllkhdrcols\tbllknocolband\tblind-34\tblindtype3 \clvertalc\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr
\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth1276\clshdrawnil \cellx1134\clvertalc\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth1701\clshdrawnil \cellx2835\clvertalc\clbrdrt
\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth1276\clshdrawnil \cellx4111\clvertalc\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1985\clshdrawnil \cellx6096\clvertalc\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth1134\clshdrawnil \cellx7230\clvertalc\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth1275\clshdrawnil \cellx8505\clvertalc\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1525\clshdrawnil \cellx10030\row \ltrrow}';
   --шаблон строки
    L_ROW_TMP := '\trowd \irow2\irowband2\ltrrow\ts33\trgaph108\trrh135\trleft-142\trbrdrt\brdrs\brdrw10 \trbrdrl\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 \trbrdrr\brdrs\brdrw10 \trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 \trftsWidth1\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddft3\trpaddfb3\trpaddfr3\tblrsid14894895\tbllkhdrrows\tbllkhdrcols\tbllknocolband\tblind-34\tblindtype3 MERGEDOC \clvertalc\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth1276\clshdrawnil \cellx1134\clvertalc\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth1701\clshdrawnil 
\cellx2835\clvertalc\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth1276\clshdrawnil \cellx4111\clvertalc\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth1985\clshdrawnil \cellx6096\clvertalc\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth1134\clshdrawnil \cellx7230\clvertalc
\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth1275\clshdrawnil \cellx8505\clvertalc\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1525\clshdrawnil \cellx10030\pard\plain \ltrpar\ql \li0\ri0\sl276\slmult1\widctlpar\intbl\wrapdefault\hyphpar0\aspalpha\aspnum\faauto\adjustright\rin0\lin0\pararsid9269622\yts33 \rtlch\fcs1 \af31507\afs22\alang1025 
\ltrch\fcs0\f1 \f31506\fs22\lang1049\langfe1033\cgrid\langnp1049\langfenp1033 {\rtlch\fcs1 \af1 \ltrch\fcs0\f1 \f1\insrsid12791597\charrsid5179790 CTRNUMBER\cell PERIOD\cell CASENUM\cell DEBT\cell PENY\cell TOLL\cell DATECOURT\cell }\pard\plain \ltrpar\ql \li0\ri0\sl276\slmult1
\widctlpar\intbl\wrapdefault\aspalpha\aspnum\faauto\adjustright\rin0\lin0 \rtlch\fcs1 \af31507\afs22\alang1025 \ltrch\fcs0\f1 \f31506\fs22\lang1049\langfe1033\cgrid\langnp1049\langfenp1033 {\rtlch\fcs1 \af31507 \ltrch\fcs0\f1 \insrsid12791597\charrsid5179790 
\trowd \irow2\irowband2\ltrrow\ts33\trgaph108\trrh135\trleft-142\trbrdrt\brdrs\brdrw10 \trbrdrl\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 \trbrdrr\brdrs\brdrw10 \trbrdrh\brdrs\brdrw10 \trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddft3\trpaddfb3\trpaddfr3\tblrsid14894895\tbllkhdrrows\tbllkhdrcols\tbllknocolband\tblind-34\tblindtype3 MERGEDOC \clvertalc\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth1276\clshdrawnil \cellx1134\clvertalc\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth1701\clshdrawnil \cellx2835\clvertalc
\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth1276\clshdrawnil \cellx4111\clvertalc\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1985\clshdrawnil \cellx6096\clvertalc\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth1134\clshdrawnil \cellx7230\clvertalc\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth1275\clshdrawnil \cellx8505\clvertalc\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1525\clshdrawnil \cellx10030\row \ltrrow}';

   L_FOOTER := '\trowd \irow20\irowband20\lastrow \ltrrow\ts33\trgaph108\trleft-142\trbrdrt\brdrs\brdrw10 \trbrdrl\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 \trbrdrr\brdrs\brdrw10 \trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 \trftsWidth1\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddft3\trpaddfb3\trpaddfr3\tblrsid14894895\tbllkhdrrows\tbllkhdrcols\tbllknocolband\tblind-34\tblindtype3 \clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb
\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth1276\clshdrawnil \cellx1134\clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth1701\clshdrawnil \cellx2835
\clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth1276\clshdrawnil \cellx4111\clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr
\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth1985\clshdrawnil \cellx6096\clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth1134\clshdrawnil \cellx7230\clvertalt\clbrdrt
\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth1275\clshdrawnil \cellx8505\clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1525\clshdrawnil \cellx10030\pard\plain \ltrpar\ql \li0\ri0\sl276\slmult1\widctlpar\intbl\wrapdefault\hyphpar0\aspalpha\aspnum\faauto\adjustright\rin0\lin0\pararsid16190848\yts33 \rtlch\fcs1 \af31507\afs22\alang1025 
\ltrch\fcs0\f1 \f31506\fs22\lang1049\langfe1033\cgrid\langnp1049\langfenp1033 {\rtlch\fcs1 \af237 \ltrch\fcs0\f1 \b\insrsid12791597\charrsid14384528 \''c8\''d2\''ce\''c3\''ce:}{\rtlch\fcs1 \af1 \ltrch\fcs0\f1 \b\f1\insrsid12791597\charrsid14384528 \cell 
}\pard \ltrpar\ql\li0\ri0\sl276\slmult1\widctlpar\intbl\wrapdefault\hyphpar0\aspalpha\aspnum\faauto\adjustright\rin0\lin0\pararsid9269622\yts33 {\rtlch\fcs1 \af1\afs24 \ltrch\fcs0\f1 \b\f1\fs24\insrsid12791597\charrsid14384528 \cell }{\rtlch\fcs1 
\af1\afs24 \ltrch\fcs0\f1 \b\f1\fs24\cf6\lang1033\langfe1033\langnp1033\insrsid12791597\charrsid14384528 \cell }{\rtlch\fcs1 \af1\afs24 \ltrch\fcs0\f1 \b\f1\fs24\insrsid12791597\charrsid14384528 DEBT\cell }\pard \ltrpar\qc \li0\ri0\sl276\slmult1
\widctlpar\intbl\wrapdefault\hyphpar0\aspalpha\aspnum\faauto\adjustright\rin0\lin0\pararsid9269622\yts33 {\rtlch\fcs1 \af1\afs24 \ltrch\fcs0\f1 \b\f1\fs24\insrsid12791597\charrsid14384528 PENY\cell }\pard \ltrpar\ql \li0\ri0\sl276\slmult1
\widctlpar\intbl\wrapdefault\hyphpar0\aspalpha\aspnum\faauto\adjustright\rin0\lin0\pararsid9269622\yts33 {\rtlch\fcs1 \af1\afs24 \ltrch\fcs0\f1 \b\f1\fs24\insrsid12791597\charrsid14384528 TOLL\cell \cell }\pard\plain \ltrpar\ql \li0\ri0\sl276\slmult1
\widctlpar\intbl\wrapdefault\aspalpha\aspnum\faauto\adjustright\rin0\lin0 \rtlch\fcs1 \af31507\afs22\alang1025 \ltrch\fcs0\f1 \f31506\fs22\lang1049\langfe1033\cgrid\langnp1049\langfenp1033 {\rtlch\fcs1 \af31507 \ltrch\fcs0\f1 \insrsid12791597\charrsid16126493 
\trowd \irow20\irowband20\lastrow \ltrrow\ts33\trgaph108\trleft-142\trbrdrt\brdrs\brdrw10 \trbrdrl\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 \trbrdrr\brdrs\brdrw10 \trbrdrh\brdrs\brdrw10 \trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddft3\trpaddfb3\trpaddfr3\tblrsid14894895\tbllkhdrrows\tbllkhdrcols\tbllknocolband\tblind-34\tblindtype3 \clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr
\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth1276\clshdrawnil \cellx1134\clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth1701\clshdrawnil \cellx2835\clvertalt\clbrdrt
\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth1276\clshdrawnil \cellx4111\clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1985\clshdrawnil \cellx6096\clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth1134\clshdrawnil \cellx7230\clvertalt\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth1275\clshdrawnil \cellx8505\clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1525\clshdrawnil \cellx10030\row }';

    L_RET := L_HEADER;

    FOR I IN (SELECT CC.SUM_DEBT, CC.PENY, CC.GOV_TOLL, TC.NUM_EXEC, TC.DATE_END, CT.CTR_NUMBER,
                     LEAD(CT.CTR_NUMBER,1) OVER (ORDER BY CT.CTR_NUMBER) NEXT_CTR,
                     (SELECT TO_CHAR(TRUNC(MIN(FD.DOC_DATE),'MM'),'DD.MM.YYYY')||' - '
                             ||TO_CHAR(LAST_DAY(MAX(FD.DOC_DATE)),'DD.MM.YYYY')
                      FROM T_CLI_DEBTS D
                          ,T_CLI_FINDOCS FD
                      WHERE FD.ID_DOC = D.ID_DOC
                      AND   D.ID_WORK = CC.ID_CASE) PERIOD
              FROM T_CLI_CASES CC,                                              
                   T_CONTRACTS CT,
                  (SELECT EX.NUM_EXEC, EX.DATE_END, EX.ID_CASE
                   FROM T_CLI_COURT_EXEC EX
                   LEFT OUTER JOIN T_CLI_COURT_EXEC EX1
                   ON (EX.ID_CASE = EX1.ID_CASE AND EX.DATE_END < EX1.DATE_END)
                   INNER JOIN T_CLI_BANKRUPT_CASES BC ON (BC.ID_CASE = EX.ID_CASE)
                   WHERE EX1.ID IS NULL 
                   AND   BC.ID_BANKRUPT = P_ID_BNK
                   AND   EX.DATE_END IS NOT NULL) TC                   
              WHERE TC.ID_CASE = CC.ID_CASE
              AND   CT.ID_CONTRACT = CC.ID_CONTRACT)
    LOOP
      L_RET := L_RET||L_ROW_TMP;
        
      IF I.CTR_NUMBER = I.NEXT_CTR AND NOT L_DOC_MRG THEN
        L_RET := REPLACE(L_RET,'MERGEDOC','\clvmgf');            
        L_DOC_MRG := TRUE;
      ELSIF I.CTR_NUMBER = I.NEXT_CTR AND L_DOC_MRG THEN
        L_RET := REPLACE(L_RET,'MERGEDOC','\clvmrg');            
      ELSIF I.CTR_NUMBER != I.NEXT_CTR AND L_DOC_MRG THEN
        L_RET := REPLACE(L_RET,'MERGEDOC','\clvmrg');                                                    
        L_DOC_MRG := FALSE;            
      ELSE
        L_RET := REPLACE(L_RET,'MERGEDOC','');    
      END IF;
      
      L_RET := REPLACE(L_RET,'CTRNUMBER',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(I.CTR_NUMBER));
      L_RET := REPLACE(L_RET,'PERIOD',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(I.PERIOD));      
      L_RET := REPLACE(L_RET,'CASENUM',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(I.NUM_EXEC));
      L_RET := REPLACE(L_RET,'DEBT',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(TO_CHAR(I.SUM_DEBT,L_FMT,L_NLS)));
      L_RET := REPLACE(L_RET,'PENY',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(TO_CHAR(I.PENY,L_FMT,L_NLS)));
      L_RET := REPLACE(L_RET,'TOLL',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(TO_CHAR(I.GOV_TOLL,L_FMT,L_NLS)));      
      L_RET := REPLACE(L_RET,'DATECOURT',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(TO_CHAR(I.DATE_END,'DD.MM.YYYY')));
      
      L_DEBT_SUM := L_DEBT_SUM + NVL(I.SUM_DEBT,0);
      L_PENY_SUM := L_PENY_SUM + NVL(I.PENY,0);    
      L_TOLL_SUM := L_TOLL_SUM + NVL(I.GOV_TOLL,0);
    END LOOP;                                                               
      
     L_RET := L_RET||L_FOOTER;    
     L_RET := REPLACE(L_RET,'DEBT',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(TO_CHAR(L_DEBT_SUM,L_FMT,L_NLS)));
     L_RET := REPLACE(L_RET,'PENY',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(TO_CHAR(L_PENY_SUM,L_FMT,L_NLS)));
     L_RET := REPLACE(L_RET,'TOLL',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(TO_CHAR(L_TOLL_SUM,L_FMT,L_NLS)));      
        
    RETURN L_RET;
  END;


  ------------------------------------------------------------------------       
  FUNCTION F_GET_TABLE(P_ID_BANKRUPT NUMBER, P_CIPHER VARCHAR2) RETURN CLOB
    IS
    L_RET CLOB;   
    L_HEADER CLOB;
    L_ROW_TMP CLOB; 
    L_FMT  VARCHAR2(50) := 'FM999G999G999G999G990D00';
    L_NLS  VARCHAR2(50) := 'NLS_NUMERIC_CHARACTERS='', ''';
  BEGIN                                                                  
    --Хедер таблицы
  L_HEADER := q'|\trowd \irow0\irowband0\ltrrow\ts16\trgaph108\trleft0\trbrdrt\brdrs\brdrw10 \trbrdrl\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 \trbrdrr\brdrs\brdrw10 \trbrdrh\brdrs\brdrw10 \trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddft3\trpaddfb3\trpaddfr3\tblrsid12588567\tbllkhdrrows\tbllkhdrcols\tbllknocolband\tblind108\tblindtype3 \clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb
\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth2355\clshdrawnil \cellx2355\clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth2464\clshdrawnil \cellx4819
\clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth2464\clshdrawnil \cellx7283\clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr
\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth2640\clshdrawnil \cellx9923\pard\plain \ltrpar\qc \li0\ri0\sl276\slmult1\widctlpar\intbl\wrapdefault\aspalpha\aspnum\faauto\adjustright\rin0\lin0\pararsid5406410\yts16 \rtlch\fcs1 \af31507\afs22\alang1025 
\ltrch\fcs0\f1 \f31506\fs22\lang1049\langfe1033\cgrid\langnp1049\langfenp1033 {\rtlch\fcs1 \af1\afs24 \ltrch\fcs0\f1 \f1\fs24\insrsid11823939\charrsid15031512 \'cd\'ee\'ec\'e5\'f0 \'f1\'f7\'e5\'f2\'e0-\'f4\'e0\'ea\'f2\'f3\'f0\'fb / \'e4\'e0\'f2\'e0\cell \'d1
\'f3\'ec\'ec\'e0 \'e2\'fb\'f1\'f2\'e0\'e2\'eb\'e5\'ed\'ed\'ee\'e3\'ee \'f1\'f7\'e5\'f2\'e0, \'f0\'f3\'e1.\cell \'ce\'ef\'eb\'e0\'f7\'e5\'ed\'ed\'e0\'ff \'f1\'f3\'ec\'ec\'e0, \'f0\'f3\'e1.\cell \'d1\'f3\'ec\'ec\'e0 \'e7\'e0\'e4\'ee\'eb\'e6\'e5\'ed\'ed\'ee
\'f1\'f2\'e8, \'f0\'f3\'e1.\cell }\pard\plain \ltrpar\ql \li0\ri0\sa200\sl276\slmult1\widctlpar\intbl\wrapdefault\aspalpha\aspnum\faauto\adjustright\rin0\lin0 \rtlch\fcs1 \af31507\afs22\alang1025 \ltrch\fcs0\f1 
\f31506\fs22\lang1049\langfe1033\cgrid\langnp1049\langfenp1033 {\rtlch\fcs1 \af31507 \ltrch\fcs0\f1 \insrsid11823939\charrsid15031512 \trowd \irow0\irowband0\ltrrow\ts16\trgaph108\trleft0\trbrdrt\brdrs\brdrw10 \trbrdrl\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 \trbrdrh\brdrs\brdrw10 \trbrdrv\brdrs\brdrw10 \trftsWidth1\trftsWidthB3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddft3\trpaddfb3\trpaddfr3\tblrsid12588567\tbllkhdrrows\tbllkhdrcols\tbllknocolband\tblind108\tblindtype3 
\clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth2355\clshdrawnil \cellx2355\clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr
\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth2464\clshdrawnil \cellx4819\clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth2464\clshdrawnil \cellx7283\clvertalt\clbrdrt
\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth2640\clshdrawnil \cellx9923\row \ltrrow}|';

   --шаблон строки
    L_ROW_TMP := q'|\pard\plain \ltrpar\qj \li0\ri0\sl276\slmult1
\widctlpar\intbl\wrapdefault\aspalpha\aspnum\faauto\adjustright\rin0\lin0\pararsid5406410\yts16 \rtlch\fcs1 \af31507\afs22\alang1025 \ltrch\fcs0\f1 \f31506\fs22\lang1049\langfe1033\cgrid\langnp1049\langfenp1033 {\rtlch\fcs1 \af1\afs24 \ltrch\fcs0\f1 
\f1\fs24\insrsid11823939\charrsid15031512 INVOICE\cell AMOUNT\cell PAYED\cell DEBT\cell }\pard\plain \ltrpar\ql \li0\ri0\sa200\sl276\slmult1\widctlpar\intbl\wrapdefault\aspalpha\aspnum\faauto\adjustright\rin0\lin0 \rtlch\fcs1 \af31507\afs22\alang1025 \ltrch\fcs0\f1 
\f31506\fs22\lang1049\langfe1033\cgrid\langnp1049\langfenp1033 {\rtlch\fcs1 \af31507 \ltrch\fcs0\f1 \insrsid11823939\charrsid15031512 \trowd \irow1\irowband1\lastrow \ltrrow\ts16\trgaph108\trleft0\trbrdrt\brdrs\brdrw10 \trbrdrl\brdrs\brdrw10 \trbrdrb
\brdrs\brdrw10 \trbrdrr\brdrs\brdrw10 \trbrdrh\brdrs\brdrw10 \trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddft3\trpaddfb3\trpaddfr3\tblrsid12588567\tbllkhdrrows\tbllkhdrcols\tbllknocolband\tblind108\tblindtype3 \clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb
\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth2355\clshdrawnil \cellx2355\clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth2464\clshdrawnil \cellx4819
\clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth2464\clshdrawnil \cellx7283\clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr
\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth2640\clshdrawnil \cellx9923\row }|';

    L_RET := L_HEADER;

    FOR I IN (SELECT FD.DOC_NUMBER, FD.DOC_DATE
                     ,TO_CHAR(FD.AMOUNT,L_FMT,L_NLS) AMOUNT 
                     ,TO_CHAR(FD.DEBT,L_FMT,L_NLS) DEBT
                     ,TO_CHAR(FD.AMOUNT - FD.DEBT,L_FMT,L_NLS) PAYED
                     ,FD.DEBT DEBT_NUM
              FROM T_CLI_DEBTS D, T_CLI_FINDOCS FD, T_CLI_BANKRUPT_CASES BC
              WHERE FD.ID_DOC = D.ID_DOC --AND FD.DEBT > 0
              AND   FD.SERV_TYPE IN (SELECT COLUMN_VALUE FROM TABLE(APEX_STRING.split(P_CIPHER,',')))
              AND   BC.ID_CASE = D.ID_WORK
              AND   BC.ID_BANKRUPT = P_ID_BANKRUPT
              AND NOT EXISTS (SELECT 1 
                    FROM T_CLI_COURT_EXEC EX
                    LEFT OUTER JOIN T_CLI_COURT_EXEC EX1
                    ON (EX.ID_CASE = EX1.ID_CASE AND EX.DATE_END < EX1.DATE_END)
                    WHERE EX1.ID IS NULL AND EX.ID_CASE = BC.ID_CASE 
                    AND EX.DATE_END IS NOT NULL)
              ORDER BY FD.DOC_DATE)                             
    LOOP
      L_RET := L_RET||L_ROW_TMP;
      L_RET := REPLACE(L_RET,'INVOICE',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(I.DOC_NUMBER||' от '||TO_CHAR(I.DOC_DATE,'DD.MM.YYYY')));
      L_RET := REPLACE(L_RET,'AMOUNT',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(I.AMOUNT));      
      L_RET := REPLACE(L_RET,'PAYED',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(I.PAYED));
      L_RET := REPLACE(L_RET,'DEBT',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(I.DEBT));
		END LOOP;					                                                     
		
		   
		RETURN L_RET;
	END;
	
	
	
	------------------------------------------------------------------------
	FUNCTION REPLACE_CLOB(IN_SOURCE  IN CLOB
											 ,IN_SEARCH  IN VARCHAR2
											 ,IN_REPLACE IN CLOB) RETURN CLOB IS
		L_POS PLS_INTEGER;
	BEGIN
		L_POS := INSTR(IN_SOURCE, IN_SEARCH);

		IF L_POS > 0 THEN
			RETURN SUBSTR(IN_SOURCE, 1, L_POS - 1) || IN_REPLACE || SUBSTR(IN_SOURCE, L_POS +
																																			LENGTH(IN_SEARCH));
		END IF;

		RETURN IN_SOURCE;
	END REPLACE_CLOB;

	PROCEDURE P_ADD_DATA(P_ID_BANKRUPT NUMBER, P_ID_COURT NUMBER) IS
		L_BLOB BLOB;
		L_RTF  CLOB;  
		L_BODY CLOB;
		L_TEMP CLOB;
		L_VAL  VARCHAR2(10000);
		L_FMT  VARCHAR2(50) := 'FM999G999G999G999G990D00';
		L_NLS  VARCHAR2(50) := 'NLS_NUMERIC_CHARACTERS='', ''';
		L_CNT  NUMBER;     
		L_SUM  NUMBER;
		L_ABON_GROUP VARCHAR2(50);
		L_PENY_GROUP VARCHAR2(50);
		L_BODY_TXT   CLOB;
	BEGIN           
		L_BODY_TXT := '\pard \ltrpar\qj \fi709\li0\ri0\sl276\slmult1\widctlpar\wrapdefault\hyphpar0\aspalpha\aspnum\faauto\adjustright\rin0\lin0\itap0\pararsid9269622 {\rtlch\fcs1 \af237\afs24 \ltrch\fcs0\f1 \fs24\ul\insrsid16459780\charrsid11225432 \''cf\''ee\''ec\''e8\''ec\''ee
, \''e2\''fb\''f8\''e5\''e8\''e7\''eb\''ee\''e6\''e5\''ed\''ed\''ee\''e3\''ee:}{\rtlch\fcs1 \af1\afs24 \ltrch\fcs0\f1 \f1\fs24\ul\insrsid16459780\charrsid11225432 
\par }\pard\plain \ltrpar\s34\qj \fi709\li0\ri0\sl276\slmult1\widctlpar\tx567\wrapdefault\aspalpha\aspnum\faauto\adjustright\rin0\lin0\itap0\pararsid9269622 \rtlch\fcs1 \af31507\afs22\alang1025 \ltrch\fcs0\f1 
\fs22\lang1049\langfe1033\cgrid\langnp1049\langfenp1033 {\rtlch\fcs1 \af237\afs24 \ltrch\fcs0\f1 \fs24\insrsid2570250\charrsid16126493 \''c2 \''f1\''e2\''ff\''e7\''e8 \''f1 \''ed\''e5\''ed\''e0\''e4\''eb\''e5\''e6\''e0\''f9\''e8\''ec \''e8\''f1\''ef\''ee\''eb\''ed\''e5\''ed\''e8
\''e5\''ec \''c4\''ee\''eb\''e6\''ed\''e8\''ea\''ee\''ec \''ee\''e1\''ff\''e7\''e0\''f2\''e5\''eb\''fc\''f1\''f2\''e2 \''ef\''ee \''ee}{\rtlch\fcs1 \af237\afs24 \ltrch\fcs0\f1 \fs24\insrsid5179790 \''ef\''eb\''e0\''f2\''e5 \''f3\''f1\''eb\''f3\''e3 \''ef\''ee \''e2\''ee\''e4\''ee\''f1\''ed\''e0
\''e1\''e6\''e5\''ed\''e8\''fe \''e8 }{\rtlch\fcs1 \af237\afs24 \ltrch\fcs0\f1 \fs24\insrsid2570250\charrsid16126493 \''e2\''ee\''e4\''ee\''ee\''f2\''e2\''e5\''e4\''e5\''ed\''e8}{\rtlch\fcs1 \af237\afs24 \ltrch\fcs0\f1 \fs24\insrsid5179790 \''fe, \''ef\''f0\''e5\''e4\''ee\''f1
\''f2\''e0\''e2\''eb\''e5\''ed\''ed\''fb\''f5 \''c7\''e0\''ff\''e2\''e8\''f2\''e5\''eb\''e5\''ec \''e2 }{\rtlch\fcs1 \af237\afs24 \ltrch\fcs0\f1 \fs24\insrsid5179790\charrsid16126493 \''f0\''e0\''ec\''ea\''e0\''f5,}{\rtlch\fcs1 \af237\afs24 \ltrch\fcs0\f1 
\fs24\insrsid2570250\charrsid16126493  \''e7\''e0\''ea\''eb\''fe\''f7\''e5\''ed\''ed\''fb\''f5 \''ec\''e5\''e6\''e4\''f3 }{\rtlch\fcs1 \af1\afs24 \ltrch\fcs0\f1 \f1\fs24\lang1033\langfe1033\langnp1033\insrsid5179790 ORGANIZATION}{\rtlch\fcs1 \af237\afs24 \ltrch\fcs0\f1 
\fs24\insrsid2570250\charrsid16126493  \''e8 }{\rtlch\fcs1 \af1\afs24 \ltrch\fcs0\f1 \f1\fs24\lang1033\langfe1033\langnp1033\insrsid5179790 CLINAME}{\rtlch\fcs1 \af1\afs24 \ltrch\fcs0\f1 \f1\fs24\insrsid5179790  }{\rtlch\fcs1 \af237\afs24 \ltrch\fcs0\f1 
\fs24\insrsid5179790 \''c4}{\rtlch\fcs1 \af237\afs24 \ltrch\fcs0\f1 \fs24\insrsid16126493\charrsid16126493 \''ee\''e3\''ee\''e2\''ee\''f0\''ee\''e2 }{\rtlch\fcs1 \af237\afs24 \ltrch\fcs0\f1 \fs24\insrsid7040645\charrsid16126493 \''f3 \''c4\''ee\''eb\''e6\''ed
\''e8\''ea\''e0 \''ef\''e5\''f0\''e5\''e4 \''c7\''e0\''ff\''e2\''e8\''f2\''e5\''eb\''e5\''ec \''e8\''ec\''e5\''e5\''f2\''f1\''ff \''e7\''e0\''e4\''ee\''eb\''e6\''e5\''ed\''ed\''ee\''f1\''f2\''fc}{\rtlch\fcs1 \af237\afs24 \ltrch\fcs0\f1 \fs24\insrsid8718284\charrsid16126493 , \''e2 \''ee\''f2
\''ed\''ee\''f8\''e5\''ed\''e8\''e8 \''ea\''ee\''f2\''ee\''f0\''ee\''e9 }{\rtlch\fcs1 \af1\afs24 \ltrch\fcs0\f1 \f1\fs24\lang1033\langfe1033\langnp1033\insrsid5179790 ORGANIZATION}{\rtlch\fcs1 \af237\afs24 \ltrch\fcs0\f1 \fs24\insrsid7040645\charrsid16126493  \''f0\''e0
\''ed\''e5\''e5 \''e2 \''f1\''f3\''e4 \''ed\''e5 \''ee\''e1\''f0\''e0\''f9\''e0\''eb\''f1\''ff \''ef\''ee \''f1\''eb\''e5\''e4\''f3\''fe\''f9\''e8\''ec \''f1\''f7\''e5\''f2\''e0\''ec \endash  \''f4\''e0\''ea\''f2\''f3\''f0\''e0\''ec:}{\rtlch\fcs1 \af1\afs24 \ltrch\fcs0\f1 
\f1\fs24\insrsid7040645\charrsid16126493 
\par }{\rtlch\fcs1 \af1\afs24 \ltrch\fcs0\f1 \b\i\f1\fs24\ul\insrsid2981141\charrsid16126493 
\par }';
	
    SELECT CG.ABON_GROUP
		INTO   L_ABON_GROUP
		FROM T_CLI_BANKRUPT BK,
		     T_CLIENTS CLI,
				 T_CUSTOMER_GROUP CG 
		WHERE BK.ID_CLIENT = CLI.ID_CLIENT
		AND   CLI.ID_GROUP = CG.ID_GROUP
		AND   BK.ID = P_ID_BANKRUPT;                
		
		SELECT T.FILE_CONTENT INTO L_RTF FROM T_RTF_TEMPLATES T WHERE  T.ID = 39;
					
	
		FOR I IN (SELECT CLI.CLI_NAME,
										 C.COURT_NAME,      
										 BK.BNK_CASE_NUM,
										 BK.DATE_BNK,  
										 CLI.INN,
										 PKG_CLIENTS.F_GET_ADDRESS(CLI.ID_CLIENT) CLI_ADDRESS,
										 C.ADDRESS,               										
										 (SELECT SUM(NVL(CC.SUM_DEBT,0)+NVL(CC.PENY,0)+NVL(CC.GOV_TOLL,0))
											FROM T_CLI_BANKRUPT_CASES BC,
													 T_CLI_CASES CC
											WHERE BC.ID_CASE = CC.ID_CASE
											AND   BC.ID_BANKRUPT = P_ID_BANKRUPT) ISKSUM,                   
											NVL(BK.LAWSUIT_SUM,0) + NVL(BK.LAWSUIT_PENY,0) + NVL(BK.LAWSUIT_TOLL,0) LAWSUITSUM,											
										 (SELECT LISTAGG('№ '||CT.CTR_NUMBER||' от '||TO_CHAR(CT.CTR_DATE,'DD.MM.YYYY'),' и ')
													 WITHIN GROUP (ORDER BY ct.id_contract)
											FROM T_CONTRACTS CT,
												 (SELECT DISTINCT CC.ID_CONTRACT
													FROM T_CLI_BANKRUPT_CASES BC,
															 T_CLI_CASES CC
													WHERE BC.ID_BANKRUPT = P_ID_BANKRUPT
													AND   CC.ID_CASE = BC.ID_CASE) BCT
											WHERE CT.ID_CONTRACT = BCT.ID_CONTRACT) CTRNUMBERS,
											(SELECT LISTAGG(PKG_MORFER.F_GET_DECLENSION(C.COURT_NAME,'Ins','S'),', ')
															WITHIN GROUP (ORDER BY C.ID_COURT)
											FROM T_COURTS C,  
										 (SELECT DISTINCT EX.ID_COURT                                                                                    
											FROM T_CLI_COURT_EXEC EX
											LEFT OUTER JOIN T_CLI_COURT_EXEC EX1
											ON (EX.ID_CASE = EX1.ID_CASE AND EX.DATE_END < EX1.DATE_END)
											INNER JOIN T_CLI_BANKRUPT_CASES BC ON (BC.ID_CASE = EX.ID_CASE)
											WHERE EX1.ID IS NULL
											AND   BC.ID_BANKRUPT = P_ID_BANKRUPT) TC
											WHERE TC.ID_COURT = C.ID_COURT) COURTBNKS,
                     (SELECT LISTAGG(EX.NUM_EXEC,',') WITHIN GROUP (ORDER BY EX.ID)
											FROM T_CLI_COURT_EXEC EX
											LEFT OUTER JOIN T_CLI_COURT_EXEC EX1
											ON (EX.ID_CASE = EX1.ID_CASE AND EX.DATE_END < EX1.DATE_END)
											INNER JOIN T_CLI_BANKRUPT_CASES BC ON (BC.ID_CASE = EX.ID_CASE)
											WHERE EX1.ID IS NULL
											AND   BC.ID_BANKRUPT = P_ID_BANKRUPT) CORT_NUMBERS											
							FROM T_CLI_BANKRUPT BK,     		 
									 (SELECT C.COURT_NAME, C.ADDRESS
										FROM T_COURTS C
										WHERE C.ID_COURT = P_ID_COURT) C,
									 T_CLIENTS CLI,
									 T_CUSTOMER_GROUP CG
							WHERE BK.ID_CLIENT = CLI.ID_CLIENT
							AND   CLI.ID_GROUP = CG.ID_GROUP
							AND   BK.ID = P_ID_BANKRUPT)
		LOOP
			L_RTF := REPLACE(L_RTF,'COURTNAME',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(I.COURT_NAME));
			L_RTF := REPLACE(L_RTF,'COURTADDRESS',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(I.ADDRESS));
			L_RTF := REPLACE(L_RTF,'CLIADDRESS',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(I.CLI_ADDRESS));
			L_RTF := REPLACE(L_RTF,'CLINAME',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(I.CLI_NAME));
			L_RTF := REPLACE(L_RTF,'CLIINN',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(I.INN));
			L_RTF := REPLACE(L_RTF,'CLIOGRN','');			
			L_RTF := REPLACE(L_RTF,'CTRNUMBER',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(I.CTRNUMBERS));    
			IF I.LAWSUITSUM = 0 THEN
        L_RTF := REPLACE(L_RTF,'ISKSUM',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(TO_CHAR(I.ISKSUM,L_FMT,L_NLS)));
			ELSE
				L_RTF := REPLACE(L_RTF,'ISKSUM',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(TO_CHAR(I.LAWSUITSUM,L_FMT,L_NLS)));	
			END IF;
		  L_RTF := REPLACE(L_RTF,'COURTSBNK',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(TO_CHAR(I.COURTBNKS)));
			L_RTF := REPLACE(L_RTF,'ORGANIZATION',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(PKG_PREF.F$C2('REQUISITES')));
			L_RTF := REPLACE(L_RTF,'COURTNUMBERS',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(I.CORT_NUMBERS));			
			L_RTF := REPLACE(L_RTF,'BNKCOURTNUM',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(I.BNK_CASE_NUM));
			L_RTF := REPLACE(L_RTF,'BNKCOURTDATE',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(TO_CHAR(I.DATE_BNK,'DD.MM.YYYY')));
			L_BODY_TXT := REPLACE(L_BODY_TXT,'ORGANIZATION',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(PKG_PREF.F$C2('REQUISITES')));
			L_BODY_TXT := REPLACE(L_BODY_TXT,'CLINAME',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(I.CLI_NAME));
		END LOOP;                                                                                           
		
		
		FOR J IN (SELECT NVL(BK.LAWSUIT_SUM,T.SUM_DEBT) ISK_DEBT,
			               NVL(BK.LAWSUIT_PENY,T.SUM_PENY) PENY_DEBT,
										 NVL(BK.LAWSUIT_TOLL,T.SUM_TOLL) TOLL_DEBT                              
			        FROM T_CLI_BANKRUPT BK,
									(SELECT SUM(NVL(CC.SUM_DEBT,0)) SUM_DEBT
									       ,SUM(NVL(CC.PENY,0)) SUM_PENY
												 ,SUM(NVL(CC.GOV_TOLL,0)) SUM_TOLL
									 FROM T_CLI_BANKRUPT_CASES BC,
											  T_CLI_CASES CC
									 WHERE BC.ID_CASE = CC.ID_CASE
									 AND   BC.ID_BANKRUPT = P_ID_BANKRUPT) T
							WHERE BK.ID = P_ID_BANKRUPT)
		LOOP
			L_RTF := REPLACE(L_RTF,'ISKDEBT',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(TO_CHAR(J.ISK_DEBT,L_FMT,L_NLS)));
			L_RTF := REPLACE(L_RTF,'ISKPENY',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(TO_CHAR(J.PENY_DEBT,L_FMT,L_NLS)));
      L_RTF := REPLACE(L_RTF,'ISKTOLL',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(TO_CHAR(J.TOLL_DEBT,L_FMT,L_NLS)));
		END LOOP;					
		
		L_TEMP := F_GET_COURT_TABLE(P_ID_BANKRUPT);
		L_RTF := PKG_DOC_REPORTS.REPLACE_CLOB(L_RTF,'COURTTABLE',L_TEMP);
		
		SELECT COUNT(1), SUM(CC.SUM_DEBT)  
		INTO L_CNT, L_SUM
		FROM T_CLI_CASES CC
		    ,T_CLI_BANKRUPT_CASES BC
		WHERE CC.ID_CASE = BC.ID_CASE
		AND   BC.ID_BANKRUPT = P_ID_BANKRUPT 
		AND NOT EXISTS (SELECT 1 
		                FROM T_CLI_COURT_EXEC EX
										LEFT OUTER JOIN T_CLI_COURT_EXEC EX1
										ON (EX.ID_CASE = EX1.ID_CASE AND EX.DATE_END < EX1.DATE_END)
										WHERE EX1.ID IS NULL AND EX.ID_CASE = CC.ID_CASE 
										AND EX.DATE_END IS NOT NULL);
		
		IF L_CNT > 0 THEN	
					
												
		--ПРОВЕРЯЕМ НА 120 ШИФР
		SELECT COUNT(1),SUM(DEBT) INTO L_CNT,L_SUM	FROM T_CLI_BANKRUPT_CASES BC, T_CLI_DEBTS CD, T_CLI_FINDOCS FD    
		WHERE CD.ID_DOC = FD.ID_DOC	AND   FD.SERV_TYPE = '120'
		AND   FD.DEBT > 0 AND   CD.ID_WORK = BC.ID_CASE AND BC.ID_BANKRUPT = P_ID_BANKRUPT
		AND NOT EXISTS (SELECT 1 
		                FROM T_CLI_COURT_EXEC EX
										LEFT OUTER JOIN T_CLI_COURT_EXEC EX1
										ON (EX.ID_CASE = EX1.ID_CASE AND EX.DATE_END < EX1.DATE_END)
										WHERE EX1.ID IS NULL AND EX.ID_CASE = BC.ID_CASE 
										AND EX.DATE_END IS NOT NULL);
	  
		IF L_CNT > 0 THEN 
			IF L_ABON_GROUP IN ('UK','TSG') THEN
 			  SELECT T.FILE_CONTENT INTO L_TEMP FROM T_RTF_TEMPLATES T WHERE T.ID = 13;  
			ELSE
				SELECT T.FILE_CONTENT INTO L_TEMP FROM T_RTF_TEMPLATES T WHERE T.ID = 29;  
			END IF;	 
			L_TEMP := REPLACE_CLOB(L_TEMP,'TABLE',F_GET_TABLE(P_ID_BANKRUPT,'120'));
			L_TEMP := REPLACE(L_TEMP,'TOTALDEBT',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(TO_CHAR(L_SUM,L_FMT,L_NLS)));			    
			L_BODY := L_BODY||L_TEMP;
		END IF;
		
		--ПРОВЕРЯЕМ НА 142 ШИФР
		SELECT COUNT(1),SUM(DEBT) INTO L_CNT,L_SUM	FROM T_CLI_BANKRUPT_CASES BC, T_CLI_DEBTS CD, T_CLI_FINDOCS FD    
		WHERE CD.ID_DOC = FD.ID_DOC	AND   FD.SERV_TYPE = '142'
		AND   FD.DEBT > 0 AND   CD.ID_WORK = BC.ID_CASE AND BC.ID_BANKRUPT = P_ID_BANKRUPT
		AND NOT EXISTS (SELECT 1 
		                FROM T_CLI_COURT_EXEC EX
										LEFT OUTER JOIN T_CLI_COURT_EXEC EX1
										ON (EX.ID_CASE = EX1.ID_CASE AND EX.DATE_END < EX1.DATE_END)
										WHERE EX1.ID IS NULL AND EX.ID_CASE = BC.ID_CASE 
										AND EX.DATE_END IS NOT NULL);
		IF L_CNT > 0 THEN
			SELECT T.FILE_CONTENT INTO L_TEMP FROM T_RTF_TEMPLATES T WHERE T.ID = 14;  
			L_TEMP := REPLACE_CLOB(L_TEMP,'TABLE',F_GET_TABLE(P_ID_BANKRUPT,'142'));
			L_TEMP := REPLACE(L_TEMP,'TOTALDEBT',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(TO_CHAR(L_SUM,L_FMT,L_NLS)));			    
			L_BODY := L_BODY||L_TEMP;
		END IF;
		
		--ПРОВЕРЯЕМ НА 140 ШИФР
		SELECT COUNT(1),SUM(DEBT) INTO L_CNT,L_SUM	FROM T_CLI_BANKRUPT_CASES BC, T_CLI_DEBTS CD, T_CLI_FINDOCS FD    
		WHERE CD.ID_DOC = FD.ID_DOC	AND   FD.SERV_TYPE = '140'
		AND   FD.DEBT > 0 AND   CD.ID_WORK = BC.ID_CASE AND BC.ID_BANKRUPT = P_ID_BANKRUPT
		AND NOT EXISTS (SELECT 1 
		                FROM T_CLI_COURT_EXEC EX
										LEFT OUTER JOIN T_CLI_COURT_EXEC EX1
										ON (EX.ID_CASE = EX1.ID_CASE AND EX.DATE_END < EX1.DATE_END)
										WHERE EX1.ID IS NULL AND EX.ID_CASE = BC.ID_CASE 
										AND EX.DATE_END IS NOT NULL);
	  
		IF L_CNT > 0 THEN
			SELECT T.FILE_CONTENT INTO L_TEMP FROM T_RTF_TEMPLATES T WHERE T.ID = 15;  
			L_TEMP := REPLACE_CLOB(L_TEMP,'TABLE',F_GET_TABLE(P_ID_BANKRUPT,'140'));
			L_TEMP := REPLACE(L_TEMP,'TOTALDEBT',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(TO_CHAR(L_SUM,L_FMT,L_NLS)));			    
			L_BODY := L_BODY||L_TEMP;
		END IF;   
		
		--ПРОВЕРЯЕМ НА 126 ШИФР
		SELECT COUNT(1),SUM(DEBT) INTO L_CNT,L_SUM	FROM T_CLI_BANKRUPT_CASES BC, T_CLI_DEBTS CD, T_CLI_FINDOCS FD    
		WHERE CD.ID_DOC = FD.ID_DOC	AND   FD.SERV_TYPE = '126'
		AND   FD.DEBT > 0 AND   CD.ID_WORK = BC.ID_CASE AND BC.ID_BANKRUPT = P_ID_BANKRUPT
		AND NOT EXISTS (SELECT 1 
		                FROM T_CLI_COURT_EXEC EX
										LEFT OUTER JOIN T_CLI_COURT_EXEC EX1
										ON (EX.ID_CASE = EX1.ID_CASE AND EX.DATE_END < EX1.DATE_END)
										WHERE EX1.ID IS NULL AND EX.ID_CASE = BC.ID_CASE 
										AND EX.DATE_END IS NOT NULL);
		
		IF L_CNT > 0 THEN
			SELECT T.FILE_CONTENT INTO L_TEMP FROM T_RTF_TEMPLATES T WHERE T.ID = 16;  
			L_TEMP := REPLACE_CLOB(L_TEMP,'TABLE',F_GET_TABLE(P_ID_BANKRUPT,'126'));
			L_TEMP := REPLACE(L_TEMP,'TOTALDEBT',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(TO_CHAR(L_SUM,L_FMT,L_NLS)));			    
			L_BODY := L_BODY||L_TEMP;
		END IF;		

		SELECT COUNT(1),SUM(DEBT) INTO L_CNT,L_SUM	FROM T_CLI_BANKRUPT_CASES BC, T_CLI_DEBTS CD, T_CLI_FINDOCS FD    
		WHERE CD.ID_DOC = FD.ID_DOC	AND  FD.SERV_TYPE IN ('210','220','310')
		AND   FD.DEBT > 0 AND   CD.ID_WORK = BC.ID_CASE AND BC.ID_BANKRUPT = P_ID_BANKRUPT
		AND NOT EXISTS (SELECT 1 
		                FROM T_CLI_COURT_EXEC EX
										LEFT OUTER JOIN T_CLI_COURT_EXEC EX1
										ON (EX.ID_CASE = EX1.ID_CASE AND EX.DATE_END < EX1.DATE_END)
										WHERE EX1.ID IS NULL AND EX.ID_CASE = BC.ID_CASE 
										AND EX.DATE_END IS NOT NULL);
	  
		IF L_CNT > 0 THEN
			SELECT T.FILE_CONTENT INTO L_TEMP FROM T_RTF_TEMPLATES T WHERE T.ID = 30;  
			L_TEMP := REPLACE_CLOB(L_TEMP,'TABLE',F_GET_TABLE(P_ID_BANKRUPT,'210,220,310'));
			L_TEMP := REPLACE(L_TEMP,'TOTALDEBT',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(TO_CHAR(L_SUM,L_FMT,L_NLS)));			    
			L_BODY := L_BODY||L_TEMP;
		END IF;	
		
		SELECT COUNT(1),SUM(DEBT) INTO L_CNT,L_SUM	FROM T_CLI_BANKRUPT_CASES BC, T_CLI_DEBTS CD, T_CLI_FINDOCS FD    
		WHERE CD.ID_DOC = FD.ID_DOC	AND FD.SERV_TYPE  IN ('320')
		AND   FD.DEBT > 0 AND   CD.ID_WORK = BC.ID_CASE AND BC.ID_BANKRUPT = P_ID_BANKRUPT
		AND NOT EXISTS (SELECT 1 
		                FROM T_CLI_COURT_EXEC EX
										LEFT OUTER JOIN T_CLI_COURT_EXEC EX1
										ON (EX.ID_CASE = EX1.ID_CASE AND EX.DATE_END < EX1.DATE_END)
										WHERE EX1.ID IS NULL AND EX.ID_CASE = BC.ID_CASE 
										AND EX.DATE_END IS NOT NULL);
	  
		IF L_CNT > 0 THEN
			SELECT T.FILE_CONTENT INTO L_TEMP FROM T_RTF_TEMPLATES T WHERE T.ID = 31;  
			L_TEMP := REPLACE_CLOB(L_TEMP,'TABLE',F_GET_TABLE(P_ID_BANKRUPT,'320'));
			L_TEMP := REPLACE(L_TEMP,'TOTALDEBT',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(TO_CHAR(L_SUM,L_FMT,L_NLS)));			    
			L_BODY := L_BODY||L_TEMP;
		END IF;			 
		
		SELECT COUNT(1),SUM(DEBT) INTO L_CNT,L_SUM	FROM T_CLI_BANKRUPT_CASES BC, T_CLI_DEBTS CD, T_CLI_FINDOCS FD    
		WHERE CD.ID_DOC = FD.ID_DOC	AND FD.SERV_TYPE  IN ('300')
		AND   FD.DEBT > 0 AND   CD.ID_WORK = BC.ID_CASE AND BC.ID_BANKRUPT = P_ID_BANKRUPT
		AND NOT EXISTS (SELECT 1 
		                FROM T_CLI_COURT_EXEC EX
										LEFT OUTER JOIN T_CLI_COURT_EXEC EX1
										ON (EX.ID_CASE = EX1.ID_CASE AND EX.DATE_END < EX1.DATE_END)
										WHERE EX1.ID IS NULL AND EX.ID_CASE = BC.ID_CASE 
										AND EX.DATE_END IS NOT NULL);

	  
		IF L_CNT > 0 THEN
			SELECT T.FILE_CONTENT INTO L_TEMP FROM T_RTF_TEMPLATES T WHERE T.ID = 32;  
			L_TEMP := REPLACE_CLOB(L_TEMP,'TABLE',F_GET_TABLE(P_ID_BANKRUPT,'300'));
			L_TEMP := REPLACE(L_TEMP,'TOTALDEBT',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(TO_CHAR(L_SUM,L_FMT,L_NLS)));			    
			L_BODY := L_BODY||L_TEMP;
		END IF;			  
		
		
		SELECT COUNT(1),SUM(DEBT) INTO L_CNT,L_SUM	FROM T_CLI_BANKRUPT_CASES BC, T_CLI_DEBTS CD, T_CLI_FINDOCS FD    
		WHERE CD.ID_DOC = FD.ID_DOC	AND FD.SERV_TYPE  IN ('520')
		AND   FD.DEBT > 0 AND   CD.ID_WORK = BC.ID_CASE AND BC.ID_BANKRUPT = P_ID_BANKRUPT
		AND NOT EXISTS (SELECT 1 
		                FROM T_CLI_COURT_EXEC EX
										LEFT OUTER JOIN T_CLI_COURT_EXEC EX1
										ON (EX.ID_CASE = EX1.ID_CASE AND EX.DATE_END < EX1.DATE_END)
										WHERE EX1.ID IS NULL AND EX.ID_CASE = BC.ID_CASE 
										AND EX.DATE_END IS NOT NULL);

	  
		IF L_CNT > 0 THEN
			SELECT T.FILE_CONTENT INTO L_TEMP FROM T_RTF_TEMPLATES T WHERE T.ID = 33;  
			L_TEMP := REPLACE_CLOB(L_TEMP,'TABLE',F_GET_TABLE(P_ID_BANKRUPT,'520'));
			L_TEMP := REPLACE(L_TEMP,'TOTALDEBT',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(TO_CHAR(L_SUM,L_FMT,L_NLS)));			    
			L_BODY := L_BODY||L_TEMP;
		END IF;		
		
		SELECT COUNT(1),SUM(DEBT) INTO L_CNT,L_SUM	FROM T_CLI_BANKRUPT_CASES BC, T_CLI_DEBTS CD, T_CLI_FINDOCS FD    
		WHERE CD.ID_DOC = FD.ID_DOC	AND FD.SERV_TYPE  IN ('531')
		AND   FD.DEBT > 0 AND   CD.ID_WORK = BC.ID_CASE AND BC.ID_BANKRUPT = P_ID_BANKRUPT
		AND NOT EXISTS (SELECT 1 
		                FROM T_CLI_COURT_EXEC EX
										LEFT OUTER JOIN T_CLI_COURT_EXEC EX1
										ON (EX.ID_CASE = EX1.ID_CASE AND EX.DATE_END < EX1.DATE_END)
										WHERE EX1.ID IS NULL AND EX.ID_CASE = BC.ID_CASE 
										AND EX.DATE_END IS NOT NULL);
	  
		IF L_CNT > 0 THEN
			SELECT T.FILE_CONTENT INTO L_TEMP FROM T_RTF_TEMPLATES T WHERE T.ID = 34;  
			L_TEMP := REPLACE_CLOB(L_TEMP,'TABLE',F_GET_TABLE(P_ID_BANKRUPT,'531'));
			L_TEMP := REPLACE(L_TEMP,'TOTALDEBT',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(TO_CHAR(L_SUM,L_FMT,L_NLS)));			    
			L_BODY := L_BODY||L_TEMP;
		END IF;			
		
		
		L_BODY := L_BODY_TXT||L_BODY;
		
		IF L_ABON_GROUP IN ('UK','TSG') THEN  		
				IF L_ABON_GROUP = 'UK' THEN
					SELECT T.FILE_CONTENT INTO L_TEMP FROM T_RTF_TEMPLATES T WHERE  T.ID = 26;
				ELSE
					SELECT T.FILE_CONTENT INTO L_TEMP FROM T_RTF_TEMPLATES T WHERE  T.ID = 27;
				END IF;	                                                                    
				L_BODY  := L_BODY||L_TEMP;
			ELSE
				IF L_ABON_GROUP = 'TSG' THEN
					SELECT T.FILE_CONTENT INTO L_TEMP FROM T_RTF_TEMPLATES T WHERE  T.ID = 36;
				ELSE
					SELECT T.FILE_CONTENT INTO L_TEMP FROM T_RTF_TEMPLATES T WHERE  T.ID = 35;
				END IF;	                                                                    
				L_BODY := L_BODY||L_TEMP;
			END IF;				
			
			L_BODY  := REPLACE(L_BODY ,'ISKDEBT',TO_CHAR(L_SUM,'999G999G999G999G999G990D99','NLS_NUMERIC_CHARACTERS='', '''));	
		
		
		L_RTF := REPLACE_CLOB(L_RTF,'ISKBODY',L_BODY);
		
		ELSE
			L_RTF := REPLACE(L_RTF,'ISKBODY','');	
		END IF;
		
		
			L_TEMP := '{\rtlch\fcs1 \af237\afs24 \ltrch\fcs0\f1 \b\fs24\insrsid2296594\charrsid14384528 - \''ef\''ee \''e4\''ee\''e3\''ee\''e2\''ee\''f0\''f3 \''b9 }{\rtlch\fcs1 \af1\afs24 \ltrch\fcs0\f1 \b\f1\fs24\insrsid3958438\charrsid14384528 CTRNUMBER}{\rtlch\fcs1 \af1\afs24 
\ltrch\fcs0\f1 \b\f1\fs24\insrsid2296594\charrsid14384528  }{\rtlch\fcs1 \af237\afs24 \ltrch\fcs0\f1 \b\fs24\insrsid2296594\charrsid14384528 \''f1\''f3\''ec\''ec\''e0 \''ee\''f1\''ed\''ee\''e2\''ed\''ee\''e3\''ee \''e4\''ee\''eb\''e3\''e0 }{\rtlch\fcs1 \af1\afs24 
\ltrch\fcs0\f1 \b\f1\fs24\insrsid3958438\charrsid14384528 DEBTCTR}{\rtlch\fcs1 \af1\afs24 \ltrch\fcs0\f1 \b\f1\fs24\insrsid2296594\charrsid14384528  }{\rtlch\fcs1 \af237\afs24 \ltrch\fcs0\f1 \b\fs24\insrsid2296594\charrsid14384528 \''f0\''f3\''e1., }{\rtlch\fcs1 
\af1\afs24 \ltrch\fcs0\f1 \b\f1\fs24\insrsid3958438\charrsid14384528 PENYCTR}{\rtlch\fcs1 \af237\afs24 \ltrch\fcs0\f1 \b\fs24\insrsid2296594\charrsid14384528  \''f0\''f3\''e1. }{\rtlch\fcs1 \af237\afs24 \ltrch\fcs0\f1 \b\fs24\insrsid3958438\charrsid14384528 
\''ef\''e5\''ed\''e8}{\rtlch\fcs1 \af1\afs24 \ltrch\fcs0\f1 \b\f1\fs24\insrsid2296594\charrsid14384528 , }{\rtlch\fcs1 \af1\afs24 \ltrch\fcs0\f1 \b\f1\fs24\insrsid3958438\charrsid14384528 TOLLCTR}{\rtlch\fcs1 \af237\afs24 \ltrch\fcs0\f1 
\b\fs24\insrsid2296594\charrsid14384528  \''e3\''ee\''f1\''ef\''ee\''f8\''eb\''e8\''ed\''fb.}{\rtlch\fcs1 \af1\afs24 \ltrch\fcs0\f1 \b\f1\fs24\insrsid2296594\charrsid14384528 
\par }';

    L_BODY := ''; L_SUM := 0;
		
    FOR I IN (SELECT CT.CTR_NUMBER, SUM(S.SUM_DEBT) DEBT, 
			               SUM(S.PENY) PENY, SUM(S.GOV_TOLL) TOLL 
			        FROM T_CLI_BANKRUPT_CASES BC,
			             T_CLI_CASES S,
									 T_CONTRACTS CT 
							WHERE BC.ID_BANKRUPT = P_ID_BANKRUPT
							AND   BC.ID_CASE = S.ID_CASE
							AND   CT.ID_CONTRACT = S.ID_CONTRACT
							GROUP BY CT.CTR_NUMBER)
		LOOP                     
			L_SUM := L_SUM + I.PENY;
		  L_BODY := L_BODY||L_TEMP;                                                                           
			L_BODY := REPLACE(L_BODY,'CTRNUMBER',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(I.CTR_NUMBER));
			L_BODY := REPLACE(L_BODY,'DEBTCTR',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(TO_CHAR(I.DEBT,L_FMT,L_NLS)));   
			L_BODY := REPLACE(L_BODY,'PENYCTR',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(TO_CHAR(I.PENY,L_FMT,L_NLS)));
			L_BODY := REPLACE(L_BODY,'TOLLCTR',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(TO_CHAR(I.TOLL,L_FMT,L_NLS)||' руб.'));			
		END LOOP;					
		
		L_RTF := REPLACE(L_RTF,'TOTALPENY',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(TO_CHAR(L_SUM,L_FMT,L_NLS)));
		L_RTF := PKG_DOC_REPORTS.REPLACE_CLOB(L_RTF,'CTRINFO',L_BODY);
				
		L_BLOB         := PKG_DOC_REPORTS.CLOB2BLOB(PAR_CLOB => L_RTF);
		RTF_DOC.P_BLOB := L_BLOB;
	
	END P_ADD_DATA;

	---------------------------------------------------------------------------------------------------------------------------------

	FUNCTION RUN_REPORT(P_PAR_01 VARCHAR2 DEFAULT NULL
										 ,P_PAR_02 VARCHAR2 DEFAULT NULL
										 ,P_PAR_03 VARCHAR2 DEFAULT NULL
										 ,P_PAR_04 VARCHAR2 DEFAULT NULL
										 ,P_PAR_05 VARCHAR2 DEFAULT NULL
										 ,P_PAR_06 VARCHAR2 DEFAULT NULL
										 ,P_PAR_07 VARCHAR2 DEFAULT NULL
										 ,P_PAR_08 VARCHAR2 DEFAULT NULL
										 ,P_PAR_09 VARCHAR2 DEFAULT NULL
										 ,P_PAR_10 VARCHAR2 DEFAULT NULL)
		RETURN LAWSUP.PKG_FILES.REC_DOC IS
		REC_DOC LAWSUP.PKG_FILES.REC_DOC;
	BEGIN
	
		RTF_DOC := NULL;
		P_ADD_DATA(TO_NUMBER(P_PAR_01),TO_NUMBER(P_PAR_02));
		REC_DOC.P_BLOB      := RTF_DOC.P_BLOB;
		REC_DOC.P_FILE_NAME := 'PRIZNANIE_BANKROTOM_' || P_PAR_01;
	
		RETURN REC_DOC;
	
	END RUN_REPORT;

END PKG_DOC_018;
/

