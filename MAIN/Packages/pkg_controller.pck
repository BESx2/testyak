i�?CREATE OR REPLACE PACKAGE LAWMAIN."PKG_CONTROLLER"
	IS
	
	TYPE CELL_TAGS IS TABLE OF VARCHAR2(1000); 
	
	PROCEDURE P_RUN (	P_REP_CODE IN VARCHAR2
										,P_PAR_01  IN VARCHAR2 DEFAULT NULL
										,P_PAR_02 IN VARCHAR2 DEFAULT NULL
										,P_PAR_03 IN VARCHAR2 DEFAULT NULL
										,P_PAR_04 IN VARCHAR2 DEFAULT NULL
										,P_PAR_05 IN VARCHAR2 DEFAULT NULL
										,P_PAR_06 IN VARCHAR2 DEFAULT NULL
										,P_PAR_07 IN VARCHAR2 DEFAULT NULL
										,P_PAR_08 IN VARCHAR2 DEFAULT NULL
										,P_PAR_09 IN VARCHAR2 DEFAULT NULL
										,P_PAR_10 IN VARCHAR2 DEFAULT NULL   
									);
	--Функция возвращает blob - результат из работы пакета c кодом P_REP_CODE
	---------------------------------------------------------------------------------------
	FUNCTION P_RETURN_BLOB(	P_REP_CODE IN VARCHAR2
										,P_PAR_01  IN VARCHAR2 DEFAULT NULL
										,P_PAR_02 IN VARCHAR2 DEFAULT NULL
										,P_PAR_03 IN VARCHAR2 DEFAULT NULL
										,P_PAR_04 IN VARCHAR2 DEFAULT NULL
										,P_PAR_05 IN VARCHAR2 DEFAULT NULL
										,P_PAR_06 IN VARCHAR2 DEFAULT NULL
										,P_PAR_07 IN VARCHAR2 DEFAULT NULL
										,P_PAR_08 IN VARCHAR2 DEFAULT NULL
										,P_PAR_09 IN VARCHAR2 DEFAULT NULL
										,P_PAR_10 IN VARCHAR2 DEFAULT NULL
									) RETURN LAWSUP.PKG_FILES.REC_DOC;
  
		--пРОЦЕДУРА ДОБАВЛЯЕТ ОТЧЕТ В ОЧЕРЕДЬ НА ФОРМИРОВАНИЕ
	PROCEDURE P_ADD_REPORT_TO_QUEUE(P_REP_CODE IN VARCHAR2
																,P_PAR_01  IN VARCHAR2 DEFAULT NULL
																,P_PAR_02 IN VARCHAR2 DEFAULT NULL
																,P_PAR_03 IN VARCHAR2 DEFAULT NULL
																,P_PAR_04 IN VARCHAR2 DEFAULT NULL
																,P_PAR_05 IN VARCHAR2 DEFAULT NULL
																,P_PAR_06 IN VARCHAR2 DEFAULT NULL
																,P_PAR_07 IN VARCHAR2 DEFAULT NULL
																,P_PAR_08 IN VARCHAR2 DEFAULT NULL
																,P_PAR_09 IN VARCHAR2 DEFAULT NULL
																,P_PAR_10 IN VARCHAR2 DEFAULT NULL
																,P_REP_NAME  VARCHAR2 DEFAULT NULL);
																	
--ФУНКЦИЯ ПРОВЕРЯЕТ КОЛИЧЕСТВО ЗАПУЩЕННЫХ НА ВЫПОЛНЕНИЕ ОТЧЕТОВ.																
	FUNCTION F_CHECK_QUEUE RETURN NUMBER;																							
  
	PROCEDURE P_CHANGE_REP_STATUS(P_ID_REP NUMBER, P_STATUS VARCHAR2);
	
	PROCEDURE P_JOB_RUN_QUEUE_REPORT(P_ID_REP NUMBER);
	
	PROCEDURE P_RUN_REPORT_FROM_QUEUE;   
	
	FUNCTION F_XLSX_XML(p_sql            IN OUT SYS_REFCURSOR
										 ,p_rep_code       VARCHAR2      
										 ,p_table_name     VARCHAR2    
										 ,p_tags           CELL_TAGS
										 ,p_date_format    VARCHAR2 DEFAULT 'DD.MM.YYYY'
										 ,p_number_format  VARCHAR2 DEFAULT '999G999G999G990D99') RETURN XMLTYPE;        
										 
  FUNCTION F_DOCX_XML(p_sql            IN OUT SYS_REFCURSOR
   	                 ,p_rep_code       VARCHAR2
						   	     ,p_date_format    VARCHAR2 DEFAULT 'DD.MM.YYYY'
									   ,p_number_format  VARCHAR2 DEFAULT '999G999G999G990D99'
									 	 ,p_merge_cells    BOOLEAN DEFAULT FALSE) RETURN XMLTYPE;	
										
  FUNCTION F_GET_REPORT_XML(L_REQ CLOB, P_PATH VARCHAR2 DEFAULT 'word_report_xml') RETURN BLOB;																	 
  
	
	
END PKG_CONTROLLER;
/

CREATE OR REPLACE PACKAGE BODY LAWMAIN."PKG_CONTROLLER"
	IS
	---------------------------------------------------------------------------------------
	PROCEDURE P_RUN (	P_REP_CODE IN VARCHAR2
										,P_PAR_01  IN VARCHAR2 DEFAULT NULL
										,P_PAR_02 IN VARCHAR2 DEFAULT NULL
										,P_PAR_03 IN VARCHAR2 DEFAULT NULL
										,P_PAR_04 IN VARCHAR2 DEFAULT NULL
										,P_PAR_05 IN VARCHAR2 DEFAULT NULL
										,P_PAR_06 IN VARCHAR2 DEFAULT NULL
										,P_PAR_07 IN VARCHAR2 DEFAULT NULL
										,P_PAR_08 IN VARCHAR2 DEFAULT NULL
										,P_PAR_09 IN VARCHAR2 DEFAULT NULL
										,P_PAR_10 IN VARCHAR2 DEFAULT NULL
									)
		IS	
		L_BLOB BLOB;
		L_NAME VARCHAR2(1000);
		
		EX_REP_NOT_DEF EXCEPTION ;
		l_cotnr_rec    T_REPORT_LIST%ROWTYPE;
		L_SQL          VARCHAR2(32767);
		
		V_LENGTH      NUMBER;
		l_format      T_REPORT_LIST.file_extension%TYPE;

	BEGIN
	  PKG_LOG.P$LOG(p_PROG_UNIT =>  'PKG_CONTROLLER'
											,p_MESS => 	'P_REP_CODE:'||P_REP_CODE||CHR(10)||CHR(13)||
																'P_PAR_01:'||P_PAR_01||CHR(10)||
																'P_PAR_02:'||P_PAR_02||CHR(10)||
																'P_PAR_03:'||P_PAR_03||CHR(10)||
																'P_PAR_04:'||P_PAR_04||CHR(10)||
																'P_PAR_05:'||P_PAR_05||CHR(10)||
																'P_PAR_06:'||P_PAR_06||CHR(10)||
																'P_PAR_07:'||P_PAR_07||CHR(10)||
																'P_PAR_08:'||P_PAR_08||CHR(10)||
																'P_PAR_09:'||P_PAR_09||CHR(10)||
																'P_PAR_10:'||P_PAR_10||CHR(10)
										  ,P_d1 => SYSDATE);
											
	  --1. достаем запись из T_REPORT_LIST
		SELECT * INTO l_cotnr_rec FROM T_REPORT_LIST c WHERE c.code = P_REP_CODE;
		
		
		--2. Формируем и выполняем конструктор RUN_REPORT. Пишем результат в L_REP
		L_SQL := 'DECLARE 
          		 L_REP LAWSUP.PKG_FILES.REC_DOC;
		BEGIN L_REP:='||l_cotnr_rec.package||'.RUN_REPORT(';
		IF P_PAR_01 IS NOT NULL THEN 
			 	L_SQL := L_SQL||'P_PAR_01=>'''||P_PAR_01||''''||', ';
		ELSE
				L_SQL := L_SQL||'P_PAR_01=>NULL'||', ';	
		END IF;
		IF P_PAR_02 IS NOT NULL THEN 
			 	L_SQL := L_SQL||'P_PAR_02=>'''||P_PAR_02||''''||', ';
		ELSE
  			L_SQL := L_SQL||'P_PAR_02=>NULL'||', ';			
		END IF;
		IF P_PAR_03 IS NOT NULL THEN 
			 	L_SQL := L_SQL||'P_PAR_03=>'''||P_PAR_03||''''||', ';
		ELSE
				L_SQL := L_SQL||'P_PAR_03=>NULL'||', ';					
		END IF;
		IF P_PAR_04 IS NOT NULL THEN 
			 	L_SQL := L_SQL||'P_PAR_04=>'''||P_PAR_04||''''||', ';
		ELSE
				L_SQL := L_SQL||'P_PAR_04=>NULL'||', ';					
		END IF;
		IF P_PAR_05 IS NOT NULL THEN 
			 	L_SQL := L_SQL||'P_PAR_05=>'''||P_PAR_05||''''||', ';
		ELSE
				L_SQL := L_SQL||'P_PAR_05=>NULL'||', ';					
		END IF;
		IF P_PAR_06 IS NOT NULL THEN 
			 	L_SQL := L_SQL||'P_PAR_06=>'''||P_PAR_06||''''||', ';
		ELSE
				L_SQL := L_SQL||'P_PAR_06=>NULL'||', ';					
		END IF;
		IF P_PAR_07 IS NOT NULL THEN 
			 	L_SQL := L_SQL||'P_PAR_07=>'''||P_PAR_07||''''||', ';
		ELSE
				L_SQL := L_SQL||'P_PAR_07=>NULL'||', ';					
		END IF;
		IF P_PAR_08 IS NOT NULL THEN 
			 	L_SQL := L_SQL||'P_PAR_08=>'''||P_PAR_08||''''||', ';
		ELSE
				L_SQL := L_SQL||'P_PAR_08=>NULL'||', ';					
		END IF;	
		IF P_PAR_09 IS NOT NULL THEN 
			 	L_SQL := L_SQL||'P_PAR_09=>'''||P_PAR_09||''''||', ';
		ELSE
				L_SQL := L_SQL||'P_PAR_09=>NULL'||', ';					
		END IF;						
		IF P_PAR_10 IS NOT NULL THEN 
			 	L_SQL := L_SQL||'P_PAR_10=>'''||P_PAR_10||'''';
		ELSE
				L_SQL := L_SQL||'P_PAR_10=>NULL';					
		END IF;			                                              
		L_SQL := L_SQL||');
		    :L_BLOB  := L_REP.P_BLOB;
				:L_NAME  := L_REP.P_FILE_NAME;		
		 END;';				
		--11 ВЕРСИЯ НЕ УМЕЕТ В СТРУКТУРЫ :(	  		 
		EXECUTE IMMEDIATE L_SQL USING OUT L_BLOB, OUT	L_NAME; 									
																						
	  --3.Формируем Response		
		V_LENGTH := DBMS_LOB.getlength(L_BLOB);		
		IF UPPER(l_cotnr_rec.file_extension) = 'DOC' THEN
			 l_format:= 'word';
		END IF;	
		IF UPPER(l_cotnr_rec.file_extension)='XLS' THEN
			 l_format:= 'excel';
		END IF;
		IF UPPER(l_cotnr_rec.file_extension)='PDF' THEN
			 l_format:= 'pdf';
		END IF;
		IF UPPER(l_cotnr_rec.file_extension)='ZIP' THEN
			 l_format:= 'zip';
		END IF;	 	 		 
		
		--перевод русскоязычного названия в транслит
		L_NAME := replace(PKG_UTILS.F_MAKE_TRANSLIT(P_STR =>L_NAME||'.'||l_cotnr_rec.file_extension),' ','_');  

		OWA_UTIL.MIME_HEADER (NVL ('RRRR', 'document/'||l_format), FALSE);
		OWA_UTIL.MIME_HEADER (NVL ('RRRR', 'charset=Windows-1251'), FALSE);
	  HTP.P ('CONTENT-LENGTH: ' || V_LENGTH);
		HTP.P ('CONTENT-DISPOSITION:  ATTACHMENT; FILENAME="'
						|| L_NAME
						|| '"'
					 );			 		 					 
		OWA_UTIL.HTTP_HEADER_CLOSE;
		--4. Отправляем Response 
		WPG_DOCLOAD.DOWNLOAD_FILE (L_BLOB);
		
	EXCEPTION
		WHEN EX_REP_NOT_DEF THEN 
			NULL;     
	  WHEN NO_DATA_FOUND THEN
				RAISE_APPLICATION_ERROR(-20200,'Не правильно указан код отчета');			
		WHEN OTHERS THEN 
			  RAISE_APPLICATION_ERROR(-20200,L_SQL||CHR(10)||DBMS_UTILITY.format_error_backtrace);		     
	END; 
  ---------------------------------------------------------------------------------------
	--Функция возвращает результат из работы пакета c кодом P_REP_CODE
	---------------------------------------------------------------------------------------
	FUNCTION P_RETURN_BLOB(	P_REP_CODE IN VARCHAR2
										,P_PAR_01  IN VARCHAR2 DEFAULT NULL
										,P_PAR_02 IN VARCHAR2 DEFAULT NULL
										,P_PAR_03 IN VARCHAR2 DEFAULT NULL
										,P_PAR_04 IN VARCHAR2 DEFAULT NULL
										,P_PAR_05 IN VARCHAR2 DEFAULT NULL
										,P_PAR_06 IN VARCHAR2 DEFAULT NULL
										,P_PAR_07 IN VARCHAR2 DEFAULT NULL
										,P_PAR_08 IN VARCHAR2 DEFAULT NULL
										,P_PAR_09 IN VARCHAR2 DEFAULT NULL
										,P_PAR_10 IN VARCHAR2 DEFAULT NULL
									) RETURN LAWSUP.PKG_FILES.REC_DOC
		IS              
		L_REP LAWSUP.PKG_FILES.REC_DOC;
  	L_BLOB BLOB;
		L_NAME VARCHAR2(1000);
		EX_REP_NOT_DEF EXCEPTION ;
		l_cotnr_rec    T_REPORT_LIST%ROWTYPE;
		L_SQL          VARCHAR2(32767);

	BEGIN
	  --1. достаем запись из T_REPORT_LIST
		SELECT * INTO l_cotnr_rec FROM T_REPORT_LIST c WHERE c.code = P_REP_CODE;

		--2. Формируем и выполняем конструктор RUN_REPORT. Пишем результат в L_REP
		L_SQL := 'DECLARE 		
		        L_REP LAWSUP.PKG_FILES.REC_DOC;
		BEGIN L_REP:='||l_cotnr_rec.package||'.RUN_REPORT(';
			 	L_SQL := L_SQL||'P_PAR_01=>'''||P_PAR_01||'''';
			 	L_SQL := L_SQL||', '||'P_PAR_02=>'''||P_PAR_02||'''';
			 	L_SQL := L_SQL||', '||'P_PAR_03=>'''||P_PAR_03||'''';
			 	L_SQL := L_SQL||', '||'P_PAR_04=>'''||P_PAR_04||'''';
			 	L_SQL := L_SQL||', '||'P_PAR_05=>'''||P_PAR_05||'''';
			 	L_SQL := L_SQL||', '||'P_PAR_06=>'''||P_PAR_06||'''';
			 	L_SQL := L_SQL||', '||'P_PAR_07=>'''||P_PAR_07||'''';
			 	L_SQL := L_SQL||', '||'P_PAR_08=>'''||P_PAR_08||'''';
			 	L_SQL := L_SQL||', '||'P_PAR_09=>'''||P_PAR_09||'''';
			 	L_SQL := L_SQL||', '||'P_PAR_10=>'''||P_PAR_10||'''';
		L_SQL := L_SQL||'); 
		    :L_BLOB  := L_REP.P_BLOB;
				:L_NAME  := L_REP.P_FILE_NAME;		
		 END;';					
		EXECUTE IMMEDIATE L_SQL USING OUT L_BLOB, OUT	L_NAME; 				
		--перевод русскоязычного названия в транслит
		L_NAME := replace(PKG_UTILS.F_MAKE_TRANSLIT(P_STR =>L_NAME||'.'||l_cotnr_rec.file_extension),' ','_');  																				
		L_REP.P_BLOB := L_BLOB;
		L_REP.P_FILE_NAME := L_NAME;
	  --3.Формируем Response
		RETURN L_REP;     
	
	EXCEPTION
		WHEN EX_REP_NOT_DEF THEN 
			NULL;     
	  WHEN NO_DATA_FOUND THEN
				RAISE_APPLICATION_ERROR(-20200,'Не правильно указан код отчета');			
		WHEN OTHERS THEN 
			  RAISE_APPLICATION_ERROR(-20200,L_SQL||CHR(10)||SQLERRM);		     
	END;                  
	
	PROCEDURE P_ADD_REPORT_TO_QUEUE(	P_REP_CODE IN VARCHAR2
																,P_PAR_01  IN VARCHAR2 DEFAULT NULL
																,P_PAR_02 IN VARCHAR2 DEFAULT NULL
																,P_PAR_03 IN VARCHAR2 DEFAULT NULL
																,P_PAR_04 IN VARCHAR2 DEFAULT NULL
																,P_PAR_05 IN VARCHAR2 DEFAULT NULL
																,P_PAR_06 IN VARCHAR2 DEFAULT NULL
																,P_PAR_07 IN VARCHAR2 DEFAULT NULL
																,P_PAR_08 IN VARCHAR2 DEFAULT NULL
																,P_PAR_09 IN VARCHAR2 DEFAULT NULL
																,P_PAR_10 IN VARCHAR2 DEFAULT NULL
																,P_REP_NAME  VARCHAR2 DEFAULT NULL)
    IS
	BEGIN
		INSERT INTO T_REQUESTED_REPORTS(ID, STATUS, REP_CODE, PAR_01,PAR_02, PAR_03,PAR_04,PAR_05
		                               ,PAR_06,PAR_07,PAR_08,PAR_09,PAR_10,CREATED_BY,DATE_CREATED,REP_NAME)
    VALUES(SEQ_REQ_REP.NEXTVAL,'QUEUE',P_REP_CODE,P_PAR_01,P_PAR_02,P_PAR_03,P_PAR_04,P_PAR_05
		      ,P_PAR_06,P_PAR_07,P_PAR_08,P_PAR_09,P_PAR_10,f$_usr_id,SYSDATE,P_REP_NAME); 						
	END;                                                                              

---------------------------------------------------------------------------------------------------------
	
	FUNCTION F_CHECK_QUEUE RETURN NUMBER
		IS
		L_RET NUMBER;
	BEGIN
		SELECT COUNT(1) INTO L_RET FROM T_REQUESTED_REPORTS S WHERE S.STATUS = 'RUNNING';
		RETURN L_RET;
	END;                                                                           

---------------------------------------------------------------------------------------------------------
	
	PROCEDURE P_RUN_REPORT_FROM_QUEUE
		IS
		L_CNT NUMBER;		
	BEGIN
		L_CNT := F_CHECK_QUEUE;
		IF L_CNT < 10 THEN
			 FOR I IN(SELECT * FROM (
				        SELECT S.ID FROM T_REQUESTED_REPORTS S WHERE S.STATUS = 'QUEUE'
				        ORDER BY S.DATE_CREATED DESC )
								WHERE ROWNUM <=  10-L_CNT )
			 LOOP
				   PKG_CONTROLLER.P_JOB_RUN_QUEUE_REPORT(I.ID);
			 END LOOP;					        
		END IF;
	END;                                

-------------------------------------------------------------------------------------------------
	
	PROCEDURE P_CHANGE_REP_STATUS(P_ID_REP NUMBER, P_STATUS VARCHAR2)
		IS    
		PRAGMA AUTONOMOUS_TRANSACTION;
	BEGIN
		 UPDATE T_REQUESTED_REPORTS S SET S.STATUS = P_STATUS WHERE S.ID = P_ID_REP;
		 COMMIT;
	END;

-------------------------------------------------------------------------------------------------
	
	PROCEDURE P_JOB_RUN_QUEUE_REPORT(P_ID_REP NUMBER)
		IS
	BEGIN                                                           
		  PKG_CONTROLLER.P_CHANGE_REP_STATUS(P_ID_REP,'RUNNING');
		  DBMS_SCHEDULER.create_job(job_name => 'RUN_REP_'||P_ID_REP,job_type => 'PLSQL_BLOCK',start_date => SYSTIMESTAMP,auto_drop => TRUE
			                         ,enabled => TRUE,job_class => 'SCHED$_LOG_ON_ERRORS_CLASS',job_action => 
			'DECLARE
			  L_RET LAWSUP.PKG_FILES.REC_DOC;
			BEGIN                                                       				
				FOR I IN (SELECT S.REP_CODE, S.PAR_01, S.PAR_02, S.PAR_03, S.PAR_04, S.PAR_05
												,S.PAR_06,S.PAR_07, S.PAR_08, S.PAR_09, S.PAR_10 
												 FROM T_REQUESTED_REPORTS S WHERE S.ID = '||P_ID_REP||')
				LOOP                                                      					
					L_RET := 	PKG_CONTROLLER.P_RETURN_BLOB(P_REP_CODE => I.REP_CODE,
																								 P_PAR_01   => I.PAR_01,
																								 P_PAR_02   => I.PAR_02,
																								 P_PAR_03   => I.PAR_03,
																								 P_PAR_04   => I.PAR_04,
																								 P_PAR_05   => I.PAR_05,
																								 P_PAR_06   => I.PAR_06,
																								 P_PAR_07   => I.PAR_07,
																								 P_PAR_08   => I.PAR_08,
																								 P_PAR_09   => I.PAR_09,
																								 P_PAR_10   => I.PAR_10);
					UPDATE T_REQUESTED_REPORTS R 
					SET R.FILE_NAME = L_RET.P_FILE_NAME
					   ,R.FILE_CONTENT = L_RET.P_BLOB  
						 ,R.STATUS = ''DONE''
						 ,R.DATE_COMPLETE = SYSDATE
				  WHERE R.ID = '||P_ID_REP||';                                              					
				END LOOP;                                                         
			EXCEPTION
				WHEN OTHERS THEN          					  
					  PKG_CONTROLLER.P_CHANGE_REP_STATUS('||P_ID_REP||',''FAILED'');
						PKG_LOG.P$LOG(p_PROG_UNIT => ''RUN_REP_JOB'',p_MESS => SQLERRM,P_C1 => DBMS_UTILITY.format_error_backtrace);
			END;');											
	END;                                          
	
-------------------------------------------------------------------------------------------------	               
	
	FUNCTION F_DOCX_XML(p_sql            IN OUT SYS_REFCURSOR
		                 ,p_rep_code       VARCHAR2
						   	     ,p_date_format    VARCHAR2 DEFAULT 'DD.MM.YYYY'
									   ,p_number_format  VARCHAR2 DEFAULT '999G999G999G990D99'
									 	 ,p_merge_cells    BOOLEAN DEFAULT FALSE) 
										 RETURN XMLTYPE
	  IS
	  l_xmltype XMLTYPE;
    l_domdoc dbms_xmldom.DOMDocument;
    l_root_node dbms_xmldom.DOMNode;
   
    l_req_element dbms_xmldom.DOMElement;
    l_req_node dbms_xmldom.DOMNode;
 
    l_element dbms_xmldom.DOMElement;
    l_node dbms_xmldom.DOMNode;
    l_text dbms_xmldom.DOMText;
	  
		l_option_elem dbms_xmldom.DOMElement;
		l_option_node dbms_xmldom.DOMNode;
		
	  l_tags_elem dbms_xmldom.DOMElement;
	  l_tags_node dbms_xmldom.DOMNode;	        
		
		l_inp_elem  dbms_xmldom.DOMElement;
		l_inp_node  dbms_xmldom.DOMNode;	        
	 
	  l_columns   		dbms_sql.desc_tab2;
	  l_cursor_id 		integer;
	  l_col_count 		integer;
	  l_varchar2  		varchar2(32767) := ' ';
	  l_clob  		    CLOB;
	  l_date      	 	date := sysdate; 
	  l_number    	 	number := '0';
	  l_dummy     	 	number;      
	  l_subs     	  varchar2(128);
  BEGIN                 

		--СОЗДАНИЕ ДОКУМЕНТА
		 l_domdoc := dbms_xmldom.newDomDocument;
	 		 
		 l_root_node := dbms_xmldom.makeNode(l_domdoc);
		 dbms_xmldom.setVersion( l_domdoc, '1.0" encoding="UTF-8' );
     dbms_xmldom.setCharset( l_domdoc, 'UTF-8' );
		 
	 	--REQUEST	 
		 l_req_element := dbms_xmldom.createElement(l_domdoc, 'request' );
		 l_req_node := dbms_xmldom.appendChild(l_root_node,dbms_xmldom.makeNode(l_req_element));      
		 
		--TEMPALTE
		 l_element := dbms_xmldom.createElement(l_domdoc, 'template' );
		 dbms_xmldom.setAttribute(l_element, 'uri', 'local');
		 dbms_xmldom.setAttribute(l_element, 'id', PKG_PREF.F$C1('TEMPLATE_DIR')||p_rep_code);
		 l_node := dbms_xmldom.appendChild(l_req_node,dbms_xmldom.makeNode(l_element));      
		 
		--input-data 
     l_inp_elem := dbms_xmldom.createElement(l_domdoc, 'input-data' );
		 l_inp_node := dbms_xmldom.appendChild(l_req_node,dbms_xmldom.makeNode(l_inp_elem));      
		 
		--TAGS
		 l_tags_elem := dbms_xmldom.createElement(l_domdoc, 'tags' );
		 l_tags_node := dbms_xmldom.appendChild(l_inp_node,dbms_xmldom.makeNode(l_tags_elem));      
		 
		 l_cursor_id := DBMS_SQL.TO_CURSOR_NUMBER(p_sql);
	   --ЗАПОЛНЯЕМ ТЭГИ
/*		 l_cursor_id := dbms_sql.open_cursor;
		 dbms_sql.parse(l_cursor_id,p_sql,dbms_sql.native);*/
				--получаем описание колонок                      
		 dbms_sql.DESCRIBE_COLUMNS2(l_cursor_id,l_col_count,l_columns); 
			--определяем колонки
		 for i in 1..l_col_count loop   
		 		if l_columns(i).col_type = dbms_types.typecode_date then
		 			dbms_sql.define_column(l_cursor_id,i,l_date);
		 		elsif l_columns(i).col_type = dbms_types.typecode_number then
		 			dbms_sql.define_column(l_cursor_id,i,l_number);
		 		elsif l_columns(i).col_type in (dbms_types.typecode_varchar2,dbms_types.typecode_varchar,dbms_types.TYPECODE_CHAR) then
		 			dbms_sql.define_column(l_cursor_id,i,l_varchar2,4000);
				ELSIF l_columns(i).col_type IN (dbms_types.TYPECODE_CLOB) THEN
					dbms_sql.define_column(l_cursor_id,i,l_clob);	
		 		end if;
		 end loop;			                           	
					
		--l_dummy := dbms_sql.execute(l_cursor_id);
			-- цикл по курсору
	
		 	if dbms_sql.fetch_rows(l_cursor_id) = 0 THEN  
			  FOR i IN 1..l_col_count LOOP  
					l_element := dbms_xmldom.createelement(l_domdoc, 'tag' );
				  dbms_xmldom.setattribute(l_element, 'name', l_columns(i).col_name);
					l_node := dbms_xmldom.appendchild(l_tags_node,dbms_xmldom.makenode(l_element));       
				END LOOP;
		 	ELSE
				for i in 1..l_col_count loop   
					--определяем тип и пишем XML
					if l_columns(i).col_type = dbms_types.typecode_date then
						dbms_sql.column_value(l_cursor_id,i,l_date);
						l_element := dbms_xmldom.createelement(l_domdoc, 'tag' );
						dbms_xmldom.setattribute(l_element, 'name', l_columns(i).col_name);                                     
						l_node := dbms_xmldom.appendchild(l_tags_node,dbms_xmldom.makenode(l_element));      									  
						l_text := dbms_xmldom.createtextnode(l_domdoc, to_char(l_date,p_date_format));
						l_node := dbms_xmldom.appendchild(l_node,dbms_xmldom.makenode(l_text));      									  
					elsif l_columns(i).col_type = dbms_types.typecode_number then
						dbms_sql.column_value(l_cursor_id,i,l_number);
						l_element := dbms_xmldom.createelement(l_domdoc, 'tag' );
						dbms_xmldom.setattribute(l_element, 'name', l_columns(i).col_name);
						l_node := dbms_xmldom.appendchild(l_tags_node,dbms_xmldom.makenode(l_element));      		
						l_text := dbms_xmldom.createtextnode(l_domdoc, to_char(l_number,p_number_format,'NLS_NUMERIC_CHARACTERS = '', '''));
						l_node := dbms_xmldom.appendchild(l_node,dbms_xmldom.makenode(l_text)); 
					elsif l_columns(i).col_type in (dbms_types.typecode_varchar2,dbms_types.typecode_varchar,dbms_types.typecode_char) then
						dbms_sql.column_value(l_cursor_id,i,l_varchar2);    
						l_element := dbms_xmldom.createelement(l_domdoc, 'tag' );
						dbms_xmldom.setattribute(l_element, 'name', l_columns(i).col_name);
						l_node := dbms_xmldom.appendchild(l_tags_node,dbms_xmldom.makenode(l_element));     
						l_text := dbms_xmldom.createtextnode(l_domdoc, l_varchar2);
						l_node := dbms_xmldom.appendchild(l_node,dbms_xmldom.makenode(l_text));  	        
					ELSIF l_columns(i).col_type IN (dbms_types.TYPECODE_CLOB) THEN
					  dbms_sql.column_value(l_cursor_id,i,l_clob);    
						l_element := dbms_xmldom.createelement(l_domdoc, 'tag' );
						dbms_xmldom.setattribute(l_element, 'name', l_columns(i).col_name);
						l_node := dbms_xmldom.appendchild(l_tags_node,dbms_xmldom.makenode(l_element));     
						l_text := dbms_xmldom.createtextnode(l_domdoc, l_clob);
						l_node := dbms_xmldom.appendchild(l_node,dbms_xmldom.makenode(l_text));  	   
					end if;
				end loop;			  
		 	end if;         
				--для каждой колонки
				
     dbms_sql.CLOSE_CURSOR(l_cursor_id);
		 --ЗАКОНЧИЛИ ЗАПОЛНЕНИЕ                                   
		 
		 --ОПЦИИ
		 l_option_elem :=  dbms_xmldom.createelement(l_domdoc, 'options' ); 
		 l_option_node    :=  dbms_xmldom.appendchild(l_req_node,dbms_xmldom.makenode(l_option_elem));  	 
		 
		 l_element := dbms_xmldom.createelement(l_domdoc, 'enable-debug-report-save' ); 
 		 l_node    := dbms_xmldom.appendchild(l_option_node,dbms_xmldom.makenode(l_element));  
	 	 l_text    := dbms_xmldom.createtextnode(l_domdoc, 'false');
		 l_node    := dbms_xmldom.appendchild(l_node,dbms_xmldom.makenode(l_text));  			                                                                                    		 
     
		 l_element := dbms_xmldom.createelement(l_domdoc, 'formatting' ); 
 		 l_node    := dbms_xmldom.appendchild(l_option_node,dbms_xmldom.makenode(l_element));  
		 l_element := dbms_xmldom.createelement(l_domdoc, 'tables' ); 
 		 l_node    := dbms_xmldom.appendchild(l_node,dbms_xmldom.makenode(l_element));     
	 	 l_element := dbms_xmldom.createelement(l_domdoc, 'enable-cells-auto-merge' ); 
 		 l_node    := dbms_xmldom.appendchild(l_node,dbms_xmldom.makenode(l_element)); 		 		 
		 
		 IF p_merge_cells THEN
	   	 l_text    := dbms_xmldom.createtextnode(l_domdoc, 'true');
		 ELSE
		 	 l_text    := dbms_xmldom.createtextnode(l_domdoc, 'false');
		 END IF;
		 l_node    := dbms_xmldom.appendchild(l_node,dbms_xmldom.makenode(l_text));  	
		 
		 
		 l_xmltype := dbms_xmldom.getxmltype(l_domdoc);
		 dbms_xmldom.freedocument(l_domdoc);
			
		 RETURN l_xmltype;
  END;     

---------------------------------------------------------------------------------------
	
	FUNCTION F_GET_REPORT_XML(L_REQ CLOB, P_PATH VARCHAR2 DEFAULT 'word_report_xml') RETURN BLOB
		IS  
		L_RES  CLOB;
		L_XML  XMLTYPE;
	BEGIN                                                        
		L_RES := LAWSUP.PKG_WEB.F_SEND_REQUEST(P_URL => PKG_PREF.F$C1('REP_ADDRESS')||'/'||P_PATH
		                                      ,P_METHOD => 'POST'
																					,P_REQ    => L_REQ);							
		L_XML := XMLTYPE(L_RES);                    
		IF L_XML.EXTRACT('/response/error/text()') IS NOT NULL THEN
			RAISE_APPLICATION_ERROR(-20200,'Возникла ошибка во время формирования отчета');
			PKG_LOG.P$LOG(P_PROG_UNIT => 'PKG_REPORTS',P_MESS => 'F_GET_REPORT',P_C1 => L_XML.EXTRACT('/response/error/text()').getStringVal());
		END IF;
				
	  RETURN APEX_WEB_SERVICE.CLOBBASE642BLOB(L_XML.EXTRACT('/response/result/text()').getClobVal());		
	END;            
	
	FUNCTION F_XLSX_XML(p_sql            IN OUT SYS_REFCURSOR
											 ,p_rep_code       VARCHAR2      
											 ,p_table_name     VARCHAR2    
											 ,p_tags           CELL_TAGS
											 ,p_date_format    VARCHAR2 DEFAULT 'DD.MM.YYYY'
											 ,p_number_format  VARCHAR2 DEFAULT '999G999G999G990D99')										 
												RETURN XMLTYPE
	  IS
	  l_xmltype XMLTYPE;
    l_domdoc dbms_xmldom.DOMDocument;
    l_root_node dbms_xmldom.DOMNode;
   
    l_req_element dbms_xmldom.DOMElement;
    l_req_node dbms_xmldom.DOMNode;
 
    l_element dbms_xmldom.DOMElement;
    l_node dbms_xmldom.DOMNode;
    l_text dbms_xmldom.DOMText;
	  
		l_option_elem dbms_xmldom.DOMElement;
		l_option_node dbms_xmldom.DOMNode;
		
	  l_tags_elem dbms_xmldom.DOMElement;
	  l_tags_node dbms_xmldom.DOMNode;	       
		
		l_tabs_elem dbms_xmldom.DOMElement;
	  l_tabs_node dbms_xmldom.DOMNode;	         
		
		l_tab_elem dbms_xmldom.DOMElement;
	  l_tab_node dbms_xmldom.DOMNode;	         
		
		l_inp_elem  dbms_xmldom.DOMElement;
		l_inp_node  dbms_xmldom.DOMNode;	 
		
		l_rows_elem  dbms_xmldom.DOMElement;
		l_rows_node  dbms_xmldom.DOMNode;       
    
		l_row_elem  dbms_xmldom.DOMElement;
		l_row_node  dbms_xmldom.DOMNode;       
			 
	  l_columns   		dbms_sql.desc_tab;
	  l_cursor_id 		integer;
	  l_col_count 		integer;
	  l_varchar2  		varchar2(32767) := ' ';   
		l_clob          CLOB;
	  l_date      	 	date := sysdate; 
	  l_number    	 	number := '0';
	  l_dummy     	 	number;      
	  l_subs     	    varchar2(128);
  BEGIN                 
		 DBMS_LOB.CREATETEMPORARY(L_CLOB,FALSE);
		--СОЗДАНИЕ ДОКУМЕНТА
		 l_domdoc := dbms_xmldom.newDomDocument;
	 		 
		 l_root_node := dbms_xmldom.makeNode(l_domdoc);
		 dbms_xmldom.setVersion( l_domdoc, '1.0" encoding="UTF-8' );
     dbms_xmldom.setCharset( l_domdoc, 'UTF-8' );
		 
	 	--REQUEST	 
		 l_req_element := dbms_xmldom.createElement(l_domdoc, 'request' );
		 l_req_node := dbms_xmldom.appendChild(l_root_node,dbms_xmldom.makeNode(l_req_element));      
		 
		--TEMPALTE
		 l_element := dbms_xmldom.createElement(l_domdoc, 'template' );
		 dbms_xmldom.setAttribute(l_element, 'uri', 'local');
		 dbms_xmldom.setAttribute(l_element, 'id', PKG_PREF.F$C1('TEMPLATE_DIR')||p_rep_code);
		 l_node := dbms_xmldom.appendChild(l_req_node,dbms_xmldom.makeNode(l_element));      
		 
		--input-data 
     l_inp_elem := dbms_xmldom.createElement(l_domdoc, 'input-data' );
		 l_inp_node := dbms_xmldom.appendChild(l_req_node,dbms_xmldom.makeNode(l_inp_elem));      
		 
		 --tables
		 l_tabs_elem := dbms_xmldom.createElement(l_domdoc, 'tables' );
 		 l_tabs_node := dbms_xmldom.appendChild(l_inp_node,dbms_xmldom.makeNode(l_tabs_elem));  
		 
		 --table
		 l_tab_elem := dbms_xmldom.createElement(l_domdoc, 'table' );
		 dbms_xmldom.setAttribute(l_tab_elem, 'name', p_table_name);  
		 l_tab_node :=  dbms_xmldom.appendChild(l_tabs_node,dbms_xmldom.makeNode(l_tab_elem));      
		 		     
		--TAGS
		 l_tags_elem := dbms_xmldom.createElement(l_domdoc, 'cell-tags' );
		 l_tags_node := dbms_xmldom.appendChild(l_tab_node,dbms_xmldom.makeNode(l_tags_elem)); 
		 
		 FOR I IN 1..P_TAGS.COUNT 
		 LOOP
			 l_element := dbms_xmldom.createElement(l_domdoc, 'cell-tag' );    
			 dbms_xmldom.setAttribute(l_element, 'name', p_tags(i));  
			 dbms_xmldom.setAttribute(l_element, 'index', to_char(i-1));  
 		   l_node := dbms_xmldom.appendChild(l_tags_node,dbms_xmldom.makeNode(l_element));  		  
		 END LOOP;
		 
		 l_rows_elem :=   dbms_xmldom.createElement(l_domdoc, 'rows' );    
		 l_rows_node := dbms_xmldom.appendChild(l_tab_node,dbms_xmldom.makeNode(l_rows_elem)); 
		 
		 l_cursor_id := DBMS_SQL.TO_CURSOR_NUMBER(p_sql);
		                   
		 dbms_sql.describe_columns(l_cursor_id,l_col_count,l_columns); 
			--определяем колонки
		 for i in 1..l_col_count loop   
		 		if l_columns(i).col_type = dbms_types.typecode_date then
		 			dbms_sql.define_column(l_cursor_id,i,l_date);
		 		elsif l_columns(i).col_type = dbms_types.typecode_number then
		 			dbms_sql.define_column(l_cursor_id,i,l_number);
		 		elsif l_columns(i).col_type in (dbms_types.typecode_varchar2,dbms_types.typecode_varchar,dbms_types.TYPECODE_CHAR) then
		 			dbms_sql.define_column(l_cursor_id,i,l_varchar2,4000);
        elsif l_columns(i).col_type in (dbms_types.typecode_clob) then
		 			dbms_sql.define_column(l_cursor_id,i,l_clob);	
		 		end if;
		 end loop;			                           	
					
	   LOOP             			
		 	if dbms_sql.fetch_rows(l_cursor_id) = 0 THEN  
			  /*FOR i IN 1..l_col_count LOOP  
					l_element := dbms_xmldom.createelement(l_domdoc, 'cell' );
					l_node := dbms_xmldom.appendchild(l_tags_node,dbms_xmldom.makenode(l_element));       
				END LOOP;*/    
				EXIT;
		 	ELSE            
				l_row_elem :=   dbms_xmldom.createElement(l_domdoc, 'row' );    
		    l_row_node := dbms_xmldom.appendChild(l_rows_node,dbms_xmldom.makeNode(l_row_elem)); 
		 
				for i in 1..l_col_count loop          
					--определяем тип и пишем XML
					if l_columns(i).col_type = dbms_types.typecode_date then
						dbms_sql.column_value(l_cursor_id,i,l_date);
						l_element := dbms_xmldom.createelement(l_domdoc, 'cell' );
						l_node := dbms_xmldom.appendchild(l_row_node,dbms_xmldom.makenode(l_element));      									  
						l_text := dbms_xmldom.createtextnode(l_domdoc, to_char(l_date,p_date_format));
						l_node := dbms_xmldom.appendchild(l_node,dbms_xmldom.makenode(l_text));      									  
					elsif l_columns(i).col_type = dbms_types.typecode_number then
						dbms_sql.column_value(l_cursor_id,i,l_number);
						l_element := dbms_xmldom.createelement(l_domdoc, 'cell' );
						l_node := dbms_xmldom.appendchild(l_row_node,dbms_xmldom.makenode(l_element));      		
						l_text := dbms_xmldom.createtextnode(l_domdoc, to_char(l_number,p_number_format,'NLS_NUMERIC_CHARACTERS = '', '''));
						l_node := dbms_xmldom.appendchild(l_node,dbms_xmldom.makenode(l_text)); 
					elsif l_columns(i).col_type in (dbms_types.typecode_varchar2,dbms_types.typecode_varchar,dbms_types.TYPECODE_CHAR) then
						dbms_sql.column_value(l_cursor_id,i,l_varchar2);    
						l_element := dbms_xmldom.createelement(l_domdoc, 'cell' );
						l_node := dbms_xmldom.appendchild(l_row_node,dbms_xmldom.makenode(l_element));     
						l_text := dbms_xmldom.createtextnode(l_domdoc, l_varchar2);
						l_node := dbms_xmldom.appendchild(l_node,dbms_xmldom.makenode(l_text));  	  	
          elsif l_columns(i).col_type in (dbms_types.typecode_clob) then
						dbms_sql.column_value(l_cursor_id,i,l_clob);    
						l_element := dbms_xmldom.createelement(l_domdoc, 'cell' );
						l_node := dbms_xmldom.appendchild(l_row_node,dbms_xmldom.makenode(l_element));     
						l_text := dbms_xmldom.createtextnode(l_domdoc, l_varchar2);
						l_node := dbms_xmldom.appendchild(l_node,dbms_xmldom.makenode(l_text));  	  							
					end if;
				end loop;			  
		 	end if;  
		 END LOOP;	       
				--для каждой колонки
				
     dbms_sql.CLOSE_CURSOR(l_cursor_id);
		 --ЗАКОНЧИЛИ ЗАПОЛНЕНИЕ                                   
		 
		 --ОПЦИИ
		 l_option_elem :=  dbms_xmldom.createelement(l_domdoc, 'options' ); 
		 l_option_node    :=  dbms_xmldom.appendchild(l_req_node,dbms_xmldom.makenode(l_option_elem));  	 
		 
		 l_element := dbms_xmldom.createelement(l_domdoc, 'enable-debug-report-save' ); 
 		 l_node    := dbms_xmldom.appendchild(l_option_node,dbms_xmldom.makenode(l_element));  
	 	 l_text    := dbms_xmldom.createtextnode(l_domdoc, 'false');
		 l_node    := dbms_xmldom.appendchild(l_node,dbms_xmldom.makenode(l_text));  			                                                                                    		 
     
		 
		 l_xmltype := dbms_xmldom.getxmltype(l_domdoc);
		 dbms_xmldom.freedocument(l_domdoc);
			
		 RETURN l_xmltype;
  END;	

END PKG_CONTROLLER;
/

