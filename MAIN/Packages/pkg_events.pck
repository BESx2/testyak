i�?create or replace package lawmain.PKG_EVENTS is

  -- Author  : EUGEN
  -- Created : 10.01.2017 11:21:04
  -- Purpose : Пакет для работы с событиями по делам физических лиц.
  
	--Функция достаёт ид типа события по заданному коду события
  FUNCTION F_GET_EVENT_TYPE_BY_CODE(P_CODE VARCHAR2) RETURN NUMBER;
	
	
	--Процедура создаёт события по делу юр. лица
	PROCEDURE P_CREATE_CLI_EVENT(P_ID_WORK NUMBER,
				     									 P_CODE    VARCHAR2,
															 P_DATE    DATE DEFAULT NULL,
															 P_AUTOR   T_CLI_EVENTS.Created_By%TYPE DEFAULT f$_usr_id);
	
	--Процедура проверяет наличие события по делу юр. лица														 
	FUNCTION F_CHECK_CLI_EVENT(P_ID_WORK     NUMBER,      						 --ИД дела
														 P_EVENT_CODE  VARCHAR2) RETURN BOOLEAN; --Код дела	
														 
	--Процедура изменяет дату события ЮЛ
	PROCEDURE P_CHANGE_CLI_EVENT(P_ID_EVENT NUMBER,
															 P_DATE     DATE DEFAULT NULL,
															 P_ID_TYPE  NUMBER);
															 
															 
	PROCEDURE P_CREATE_EVENT(P_EVENT_NAME VARCHAR2
		                      ,P_EVENT_DESC VARCHAR2
													,P_EVENT_TYPE VARCHAR2);
													
	PROCEDURE P_DELETE_CLI_EVENT(P_ID_EVENT NUMBER);												
													
  PROCEDURE P_UPDATE_EVENT(P_ID_EVENT NUMBER,
		                       P_EVENT_NAME VARCHAR2,
													 P_EVENT_DESC VARCHAR2,
													 P_IS_ACTIVE  VARCHAR2);
													 
	PROCEDURE P_CREATE_BNK_EVENT(P_ID_BANKRUPT NUMBER,
				     									 P_CODE    VARCHAR2,
															 P_DATE    DATE DEFAULT NULL,
															 P_AUTOR   NUMBER DEFAULT f$_usr_id);												 
end PKG_EVENTS;
/

create or replace package body lawmain.PKG_EVENTS is

  PROCEDURE P_CREATE_EVENT(P_EVENT_NAME VARCHAR2
		                      ,P_EVENT_DESC VARCHAR2
													,P_EVENT_TYPE VARCHAR2)
  	IS
		L_CODE VARCHAR2(50);
  BEGIN
    L_CODE := REPLACE(SUBSTR(PKG_UTILS.F_MAKE_TRANSLIT(P_EVENT_NAME),1,50),' ','_');
		INSERT INTO T_EVENT_TYPES(ID_EVENT_TYPE,CODE_EVENT,SHORT_NAME,EVENT_DESC,TYPE_EVENT,CREATED_BY,DATE_CREATED)
    VALUES(SEQ_EVENTS.NEXTVAL,L_CODE,P_EVENT_NAME,P_EVENT_DESC,P_EVENT_TYPE,F$_USR_ID,SYSDATE);
	END;							
	
	PROCEDURE P_UPDATE_EVENT(P_ID_EVENT NUMBER,
		                       P_EVENT_NAME VARCHAR2,
													 P_EVENT_DESC VARCHAR2,
													 P_IS_ACTIVE  VARCHAR2)
	  IS
	BEGIN
		 UPDATE T_EVENT_TYPES T
		 SET T.EVENT_DESC = P_EVENT_DESC,
		     T.SHORT_NAME = P_EVENT_NAME,
				 T.IS_ACTIVE  = P_IS_ACTIVE,
				 T.MODIFIED_BY = F$_USR_ID,
				 T.DATE_MODIFIED = SYSDATE
		 WHERE T.ID_EVENT_TYPE = P_ID_EVENT;
	END;												 

	--Функция достаёт ид типа события по заданному коду события
	FUNCTION F_GET_EVENT_TYPE_BY_CODE(P_CODE VARCHAR2) RETURN NUMBER 
	IS
		L_RET NUMBER;
	BEGIN                 
		--Достаём ид события
	  SELECT ID_EVENT_TYPE
		INTO L_RET 
		FROM T_EVENT_TYPES 
		WHERE CODE_EVENT = P_CODE;
		 
		RETURN L_RET;
  EXCEPTION 
			WHEN NO_DATA_FOUND THEN
					RAISE_APPLICATION_ERROR(-20200,'Тип события не найден');
	END;                                                       
	
	--------------------------------------------------------------------------
	
	--Процедура создаёт события для заданного дела по заданному коду события
	
	-----------------------------------------------------------------------------
	
	--Процедура создаёт события по делу юр. лица
	PROCEDURE P_CREATE_CLI_EVENT(P_ID_WORK NUMBER,
				     									 P_CODE    VARCHAR2,
															 P_DATE    DATE DEFAULT NULL,
															 P_AUTOR   T_CLI_EVENTS.Created_By%TYPE DEFAULT f$_usr_id)
	IS                                       
	L_TYPE_ID NUMBER;
	BEGIN            
		 --Ид события
		 L_TYPE_ID := F_GET_EVENT_TYPE_BY_CODE(P_CODE);
		 --Создание события
		 INSERT INTO T_CLI_EVENTS(ID_EVENT,ID_WORK,ID_EVENT_TYPE,DATE_CREATED ,CREATED_BY, DATE_EVENT)
		 VALUES(SEQ_CLI_EVENT.NEXTVAL,P_ID_WORK,L_TYPE_ID,SYSDATE,P_AUTOR,NVL(P_DATE,SYSDATE));
	END;
	
	
		--Процедура создаёт события по банкротству
	PROCEDURE P_CREATE_BNK_EVENT(P_ID_BANKRUPT NUMBER,
				     									 P_CODE    VARCHAR2,
															 P_DATE    DATE DEFAULT NULL,
															 P_AUTOR   NUMBER DEFAULT f$_usr_id)
	IS                                       
	L_TYPE_ID NUMBER;
	BEGIN            
		 --Ид события
		 L_TYPE_ID := F_GET_EVENT_TYPE_BY_CODE(P_CODE);
		 --Создание события
		 INSERT INTO T_BANKRUPT_EVENTS(ID_EVENT,ID_BANKRUPT,ID_EVENT_TYPE,DATE_CREATED ,CREATED_BY, DATE_EVENT)
		 VALUES(SEQ_CLI_EVENT.NEXTVAL,P_ID_BANKRUPT,L_TYPE_ID,SYSDATE,P_AUTOR,NVL(P_DATE,SYSDATE));
	END;		                                                                  
	
	-----------------------------------------------------------------------------------
	
	
	--Процедура изменяет дату события ЮЛ
	PROCEDURE P_CHANGE_CLI_EVENT(P_ID_EVENT NUMBER,
															 P_DATE     DATE DEFAULT NULL,
															 P_ID_TYPE  NUMBER)															 															 
	IS                                       
	BEGIN            
 		 UPDATE T_CLI_EVENTS EV
		 SET EV.DATE_EVENT = NVL(P_DATE,SYSDATE),
		 		 EV.ID_EVENT_TYPE = P_ID_TYPE,
				 EV.MODIFIED_BY = F$_USR_ID,
				 EV.DATE_MODIFIED = SYSDATE
		 WHERE EV.ID_EVENT = P_ID_EVENT; 
	END;                                           
	
	PROCEDURE P_DELETE_CLI_EVENT(P_ID_EVENT NUMBER)
		IS
	BEGIN
		DELETE FROM T_CLI_EVENTS EV WHERE EV.ID_EVENT = P_ID_EVENT;
	END;
	                                                          
	
	-----------------------------------------------------------------------------------
	                                                                                   
	-- Процедура проверяет наличие события по делу юр. лица.
	FUNCTION F_CHECK_CLI_EVENT(P_ID_WORK NUMBER, 
														 P_EVENT_CODE  VARCHAR2) RETURN BOOLEAN
  IS
	BEGIN    
		 FOR i IN(SELECT 1
							FROM T_CLI_EVENTS ev,
									 T_EVENT_TYPES t
							WHERE t.id_event_type = ev.id_event_type
							AND   ev.ID_WORK = P_ID_WORK
							AND   t.code_event = P_EVENT_CODE)
     LOOP
			  RETURN TRUE;	   
		 END LOOP;    
		   
		 RETURN FALSE;
		 
	END F_CHECK_CLI_EVENT;
														
	END PKG_EVENTS;
/

