i�?CREATE OR REPLACE PACKAGE LAWMAIN.PKG_MAIL_03
IS
/* Автор: Бородулин Е.С.
   Дата:  21.12.2016
	 Назначение: Пакет отсылает email уведомление для испол. произоводства
*/

   PROCEDURE p_send_mail(P_ID_CLI_CASE IN NUMBER, P_EMAIL VARCHAR2, P_ATTACH VARCHAR2 DEFAULT NULL);

END;
/

CREATE OR REPLACE PACKAGE BODY LAWMAIN.PKG_MAIL_03
IS

	G$HTML VARCHAR2(32000);

  PROCEDURE p_send_mail(P_ID_CLI_CASE IN NUMBER, P_EMAIL VARCHAR2, P_ATTACH VARCHAR2)
        IS
			L_RET    NUMBER;
			L_REP    LAWSUP.PKG_FILES.REC_DOC;
    BEGIN                                                        
			IF p_email IS NULL THEN 
				raise_application_error(-20200,'email не указан');
		  END IF;                
			
				G$HTML := PKG_PREF.F$HTML('FSSP_EXEC_NOTIF');
				FOR i IN (SELECT '№ '||L.LEVY_SERIES||' '||L.LEVY_NUM LEVY_NUM,
				                 S.CLI_ALT_NAME, 
												 (SELECT C.DATE_END FROM T_CLI_COURT_EXEC C WHERE C.ID = S.LAST_EXEC) DATE_EXEC,
												 S.CODE_STATUS												 
					        FROM V_CLI_CASES S,
									     T_CLI_LEVY L
									WHERE S.ID_CASE = P_ID_CLI_CASE
									AND   L.ID_CASE = S.ID_CASE)
            LOOP                      
							IF I.CODE_STATUS NOT IN ('LEVY_CLAIM_DEPFIN','LEVY_CLAIM_UFK','LEVY_CLAIM_BANK','LEVY_CLAIM_ROSP') THEN
								RAISE_APPLICATION_ERROR(-20200,'Нельзя отправить сообщению по делу, по которому не предъявлен исполнительный документ');
							END IF;
							IF I.CODE_STATUS = 'LEVY_CLAIM_DEPFIN' THEN
								G$HTML := REPLACE(G$HTML,'#PLACE#','Департамент финансов');
							ELSIF i.code_status = 'LEVY_CLAIM_UFK' THEN
								G$HTML := REPLACE(G$HTML,'#PLACE#','Управление федерального казначейства');
							ELSIF i.code_status = 'LEVY_CLAIM_BANK' THEN
								G$HTML := REPLACE(G$HTML,'#PLACE#','банк');
							ELSIF i.code_status IN ('LEVY_CLAIM_ROSP','EXEC_GOING') THEN									
								G$HTML := REPLACE(G$HTML,'#PLACE#','отдел судебных приставов');
							END IF;                                                
							
							G$HTML := REPLACE(G$HTML,'#LEVY#',I.LEVY_NUM); -- договор.
							G$HTML := REPLACE(G$HTML,'#CLINAME#',I.CLI_ALT_NAME); -- Абонент
							G$HTML := REPLACE(G$HTML,'#COURTDATE#',TO_CHAR(I.DATE_EXEC,'DD.MM.YYYY')); -- текущая дата.
            END LOOP;
           
            L_RET := PKG_MAIL.F_MAKE_EMAIL(P_HEADER => PKG_PREF.F$C1('FSSP_EXEC_NOTIF')
                                           ,P_FROM => PKG_PREF.F$C1('USER_MAIL') -- do not change !!!! if change => change password in sup.pkg_mail!!!
                                           ,P_BODY => G$HTML
                                          );
            PKG_MAIL.P_ATTACHE_EMAIL_ADDR(P_EMAIL      => P_EMAIL
                                         ,P_ID_MAIL    => L_RET
                                         ,P_EMAIL_TYPE => PKG_MAIL.LIST_EMAIL_TYPE.TO_);
																				 
				FOR I IN (SELECT COLUMN_VALUE FROM TABLE(APEX_STRING.split(P_ATTACH,':')))																 
				LOOP
						L_REP := PKG_CONTROLLER.P_RETURN_BLOB(P_REP_CODE => I.COLUMN_VALUE
						                                     ,P_PAR_01 => P_ID_CLI_CASE);    
																								 
						PKG_MAIL.P_ATTACHE_FILE(P_ID_MAIL => L_RET
						                       ,P_BLOB => L_REP.P_BLOB
																	 ,P_FILE_NAME => L_REP.P_FILE_NAME);																		 
				END LOOP;

        PKG_MAIL.P_SEND(P_ID => L_RET,P_SEND_TYPE => PKG_MAIL.LIST_TYPE_SEND.LTE);
				PKG_SEND_MAIL.P_INSERT_CLI_CASE_MAIL(P_ID_CLI_CASE,L_RET,'FSSP_EXEC_NOTIF' );
 				COMMIT;
    END;
    
		
END;
/

