i�?create or replace package lawmain.PKG_DEPARTMENTS is


	PROCEDURE P_INSERT_DEPARTMENT(p_id_fk NUMBER
															,p_code VARCHAR2
															,p_name VARCHAR2
															,p_full_name VARCHAR2
															,p_time_zone NUMBER
															);


	PROCEDURE P_UPDATE_DEPARTMENT(  p_id NUMBER
																,p_id_fk NUMBER
																,p_code VARCHAR2
																,p_name VARCHAR2
																,p_full_name VARCHAR2
																,p_time_zone NUMBER
																);

	PROCEDURE P_DEL_DEPARTMENT(P_ID NUMBER);

	PROCEDURE P_PRINT_TREE;
	PROCEDURE P_PRINT_TREE_WITH_SCOPE;

	FUNCTION F_GET_PATH_BY_ID (P_ID_DEP NUMBER) RETURN VARCHAR2;

end PKG_DEPARTMENTS;
/

create or replace package body lawmain.PKG_DEPARTMENTS is

	PROCEDURE P_INSERT_DEPARTMENT(p_id_fk NUMBER
															,p_code VARCHAR2
															,p_name VARCHAR2
															,p_full_name VARCHAR2
															,p_time_zone NUMBER
															)
	IS
	BEGIN
		INSERT INTO T_DEP_DEPARTMENTS(id
															,id_fk
															,code
															,NAME
															,full_name
															,time_zone
															,date_created
															,created_by_id
														)
		VALUES(	seq_main.nextval
						,p_id_fk
						,TRIM(UPPER(p_code))
						,TRIM(REPLACE(p_name,'/','\'))
						,TRIM(REPLACE(p_full_name,'/','\'))
						,p_time_zone
						,SYSDATE
						,f$_usr_id
						);
	END;

	PROCEDURE P_UPDATE_DEPARTMENT(p_id NUMBER
															,p_id_fk NUMBER
															,p_code VARCHAR2
															,p_name VARCHAR2
															,p_full_name VARCHAR2
															,p_time_zone NUMBER
															)
		IS
	BEGIN
		UPDATE T_DEP_DEPARTMENTS d
			SET d.id_fk = p_id_fk
					,d.code = TRIM(UPPER(p_code))
					,d.name = TRIM(replace(p_name,'/','\'))
					,d.date_modified = SYSDATE
					,d.modified_by_id = f$_usr_id
					,d.full_name = TRIM(replace(p_full_name,'/','\'))
					,d.time_zone = p_time_zone
		WHERE d.id = p_id;
	END;


	PROCEDURE P_DEL_DEPARTMENT (P_ID NUMBER)
		IS
	BEGIN
    FOR i IN (
								SELECT SYS_CONNECT_BY_PATH(NAME, '/') PATH
											, LEVEL
											,id
									FROM T_DEP_DEPARTMENTS  t
										START WITH ID = P_ID
										CONNECT BY PRIOR ID = id_fk
										FOR UPDATE NOWAIT
										ORDER BY LEVEL DESC
							)
		LOOP
			DELETE FROM T_DEP_DEPARTMENTS d WHERE d.id = i.id;
		END LOOP;
	END;


	PROCEDURE P_PRINT_TREE
		IS
		t varchar2(32000);
		cnt NUMBER := 0;
	BEGIN
		IF SUBSTR(APEX_APPLICATION.G_x01,1,2) = '-1' THEN
			t :=   '[';
				FOR I IN
				(
							SELECT T.ID,
									 APEX_JAVASCRIPT.escape(
									 														substr(t.name,1,50)||decode(sign(length(t.name)-50),1,' ...','')
									 												) short_name
							FROM T_DEP_DEPARTMENTS T
						 WHERE T.ID_FK IS NULL
						ORDER BY t.name
				)
				LOOP
					IF cnt = 1 THEN
						t := t || ',';
					END IF;
					t := t||'{"attr":{"id":"'||i.ID||'","timestemp":"'||to_char(SYSDATE,'mm:ss')||'"}
									,"data":{"title" : "'||i.short_name||'","timestemp":"'||to_char(SYSDATE,'mm:ss')||'"}
									,"state":"closed"
									}';
						cnt := 1;
					END LOOP;
				t := t || ']';
		ELSE
			t :=   '[';
			FOR i IN (
						SELECT T.ID,
									 APEX_JAVASCRIPT.escape(
									 														substr(t.name,1,50)||decode(sign(length(t.name)-50),1,' ...','')
									 												) short_name
									,(SELECT COUNT(1) FROM T_DEP_DEPARTMENTS ff WHERE ff.id_fk = t.id AND rownum < 2 ) cnt
							FROM T_DEP_DEPARTMENTS T
						 WHERE t.id_fk = TO_NUMBER(APEX_APPLICATION.G_x01)
						 ORDER BY t.name
			)
			LOOP
				IF cnt = 1 THEN
					t := t || ',';
				END IF;
					t := t ||
								'{"attr":{"id":"'||i.ID||'","timestemp":"'||to_char(SYSDATE,'mm:ss')||'"}
									,"data":{"title" : "'||i.short_name||'","timestemp":"'||to_char(SYSDATE,'mm:ss')||'"}';
					IF i.cnt > 0 THEN
						t := t || '	,"state":"closed"
											';
					END IF;
					t := t ||'}';
				cnt := 1;
			END LOOP;
			t := t || ']';
		END IF;

		htp.prn(t);
	END;


PROCEDURE P_PRINT_TREE_WITH_SCOPE
		IS
		t varchar2(32000);
		cnt NUMBER := 0;
		chk_scope NUMBER;
	BEGIN

	Select COUNT(1) INTO chk_scope FROM t_User_Scope s
		WHERE s.id_user = f$_usr_id;

	IF chk_scope > 0 THEN   -- Если есть область видимости, то показываем только область видимости
		IF SUBSTR(APEX_APPLICATION.G_x01,1,2) = '-1' THEN      -- если ее нет, то только отделение в котором сидит человек
			t :=   '[';
				FOR I IN
				(
							SELECT T.ID,
									 APEX_JAVASCRIPT.escape(
									 														substr(t.name,1,50)||decode(sign(length(t.name)-50),1,' ...','')
									 												) short_name
							FROM T_DEP_DEPARTMENTS T
						 WHERE T.ID IN (Select s.id_dep from t_User_Scope s
															WHERE s.id_user = f$_usr_id)
						ORDER BY t.name
				)
				LOOP
					IF cnt = 1 THEN
						t := t || ',';
					END IF;
					t := t||'{"attr":{"id":"'||i.ID||'","timestemp":"'||to_char(SYSDATE,'mm:ss')||'"}
									,"data":{"title" : "'||i.short_name||'","timestemp":"'||to_char(SYSDATE,'mm:ss')||'"}
									,"state":"closed"
									}';
						cnt := 1;
				END LOOP;
				t := t || ']';
		ELSE
			t :=   '[';
			FOR i IN (
						SELECT T.ID,
									 APEX_JAVASCRIPT.escape(
									 														substr(t.name,1,50)||decode(sign(length(t.name)-50),1,' ...','')
									 												) short_name
									,(SELECT COUNT(1) FROM T_DEP_DEPARTMENTS ff WHERE ff.id_fk = t.id AND rownum < 2 ) cnt
							FROM T_DEP_DEPARTMENTS T
						 WHERE t.id_fk = TO_NUMBER(APEX_APPLICATION.G_x01)
						 ORDER BY t.name
			)
			LOOP
				IF cnt = 1 THEN
					t := t || ',';
				END IF;
					t := t ||
								'{"attr":{"id":"'||i.ID||'","timestemp":"'||to_char(SYSDATE,'mm:ss')||'"}
									,"data":{"title" : "'||i.short_name||'","timestemp":"'||to_char(SYSDATE,'mm:ss')||'"}';
					IF i.cnt > 0 THEN
						t := t || '	,"state":"closed"
											';
					END IF;
					t := t ||'}';
				cnt := 1;
			END LOOP;
			t := t || ']';
		END IF;

	ELSE

			FOR k IN
					(
								SELECT T.ID,
										 APEX_JAVASCRIPT.escape(
																								substr(t.name,1,50)||decode(sign(length(t.name)-50),1,' ...','')
																						) short_name
								FROM T_DEP_DEPARTMENTS T
							 WHERE T.ID = f$_usr_id_dep(f$_usr_id)
							ORDER BY t.name
					)
			LOOP
					t :='[{"attr":{"id":"'||k.ID||'","timestemp":"'||to_char(SYSDATE,'mm:ss')||'"}
						,"data":{"title" : "'||k.short_name||'","timestemp":"'||to_char(SYSDATE,'mm:ss')||'"}
						}]';
			END LOOP;

	END IF;

	htp.prn(t);

	END P_PRINT_TREE_WITH_SCOPE;



	FUNCTION F_GET_PATH_BY_ID (P_ID_DEP NUMBER) RETURN VARCHAR2
	IS
			l_ret VARCHAR2(1000);
	BEGIN

		FOR i IN (SELECT sys_connect_by_path(d.name,'/') path
			 					FROM t_dep_departments d
								WHERE d.id = P_ID_DEP
								START WITH d.id_fk IS NULL
								CONNECT BY PRIOR d.id = d.id_fk)
		LOOP
			 l_ret := i.path;
		END LOOP;

		RETURN l_ret;

	END F_GET_PATH_BY_ID;

	


end PKG_DEPARTMENTS;
/

