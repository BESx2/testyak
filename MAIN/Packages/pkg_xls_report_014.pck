i�?create or replace package lawmain.PKG_XLS_REPORT_014 is

  -- Author  : BES
  -- Created : 17.02.2017
  -- Purpose : Отчет по дебиторке
	-- Code:     DEBT_REPORT
	
  
  FUNCTION RUN_REPORT(P_PAR_01 VARCHAR2 DEFAULT NULL
											,P_PAR_02 VARCHAR2 DEFAULT NULL
											,P_PAR_03 VARCHAR2 DEFAULT NULL
											,P_PAR_04 VARCHAR2 DEFAULT NULL
											,P_PAR_05 VARCHAR2 DEFAULT NULL
											,P_PAR_06 VARCHAR2 DEFAULT NULL
											,P_PAR_07 VARCHAR2 DEFAULT NULL
											,P_PAR_08 VARCHAR2 DEFAULT NULL
											,P_PAR_09 VARCHAR2 DEFAULT NULL
											,P_PAR_10 VARCHAR2 DEFAULT NULL) RETURN LAWSUP.PKG_FILES.REC_DOC;	  

end PKG_XLS_REPORT_014;
/

create or replace package body lawmain.PKG_XLS_REPORT_014 is

	TYPE REC_DOC IS RECORD(	P_BLOB BLOB DEFAULT NULL );

	EXCEL_DOC REC_DOC := NULL;
------------------------------------------------------------------------

	PROCEDURE p$s( ps_value IN CLOB )  -- отправляет накопленное значение ps_value в буфер и записывает
	IS
		BUFFER    	RAW(32767);
    l_offset    NUMBER := 1;
    BUF_SIZE    NUMBER := 8000;
    BUF_VAR    VARCHAR2(32767);
  BEGIN
    LOOP
    EXIT WHEN L_OFFSET > DBMS_LOB.getlength(PS_VALUE);
    BUF_VAR := DBMS_LOB.substr(PS_VALUE,BUF_SIZE,L_OFFSET);
		BUFFER := UTL_RAW.cast_to_raw( CONVERT( BUF_VAR, 'UTF8'/*'CL8MSWIN1251'*/ ) );
		DBMS_LOB.WRITEAPPEND( EXCEL_DOC.P_BLOB, UTL_RAW.LENGTH( BUFFER ), BUFFER );
    L_OFFSET := L_OFFSET + BUF_SIZE;
    END LOOP;
  END;
------------------------------------------------------------------------
	PROCEDURE P_ADD_DATA(P_DATE_ON   DATE, 
											 P_DATE_FROM DATE,
											 P_DATE_TO   DATE,
											 P_GROUP     VARCHAR2) IS 
	  L_XML CLOB;
		L_CNT NUMBER := 0;
		L_FMT VARCHAR2(50) := '9999999999999D00';
		L_NLS VARCHAR2(50) := 'NLS_NUMERIC_CHARACTERS=''. ''';		
		L_DATE_START DATE;
    L_DATE_END   DATE;                
		L_CLIENT     VARCHAR2(500);  
		L_CTR_NUMBER VARCHAR2(100);   
		L_DATE1       DATE;       
		L_DATE2       DATE;
		L_DAY         NUMBER;
	BEGIN       
		L_DATE_START := NVL(P_DATE_FROM,DATE '2000-01-01');
		L_DATE_END   := NVL(P_DATE_TO+1-(INTERVAL '1' SECOND) ,SYSDATE);   
		
		L_XML :=
			'<?xml version="1.0"?>
<?mso-application progid="Excel.Sheet"?>
<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:o="urn:schemas-microsoft-com:office:office"
 xmlns:x="urn:schemas-microsoft-com:office:excel"
 xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:html="http://www.w3.org/TR/REC-html40">
 <DocumentProperties xmlns="urn:schemas-microsoft-com:office:office">
  <Author></Author>
  <LastAuthor></LastAuthor>
  <Created>2019-05-08T06:13:45Z</Created>
  <Company>SPecialiST RePack</Company>
  <Version>16.00</Version>
 </DocumentProperties>
 <OfficeDocumentSettings xmlns="urn:schemas-microsoft-com:office:office">
  <AllowPNG/>
 </OfficeDocumentSettings>
 <ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel">
  <WindowHeight>12300</WindowHeight>
  <WindowWidth>28800</WindowWidth>
  <WindowTopX>0</WindowTopX>
  <WindowTopY>0</WindowTopY>
  <RefModeR1C1/>
  <ProtectStructure>False</ProtectStructure>
  <ProtectWindows>False</ProtectWindows>
 </ExcelWorkbook>
 <Styles>
  <Style ss:ID="Default" ss:Name="Normal">
   <Alignment ss:Vertical="Bottom"/>
   <Borders/>
   <Font ss:FontName="Calibri" x:CharSet="204" x:Family="Swiss" ss:Size="11"
    ss:Color="#000000"/>
   <Interior/>
   <NumberFormat/>
   <Protection/>
  </Style>
  <Style ss:ID="s76">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Interior ss:Color="#C6E0B4" ss:Pattern="Solid"/>
  </Style>    
	<Style ss:ID="s77">
   <Alignment ss:Vertical="Top" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <NumberFormat ss:Format="0"/>
  </Style>
  <Style ss:ID="s83">    
	   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
  </Style>
  <Style ss:ID="s85">
   <Alignment ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <NumberFormat ss:Format="Standard"/>
  </Style>
  <Style ss:ID="s86">
   <Alignment ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <NumberFormat ss:Format="Short Date"/>
  </Style>
  <Style ss:ID="s87">
  <Alignment ss:Vertical="Center" />
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <NumberFormat ss:Format="Short Date"/>
  </Style>
  <Style ss:ID="s95">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Font ss:FontName="Calibri" x:CharSet="204" x:Family="Swiss" ss:Size="14"
    ss:Color="#000000" ss:Bold="1"/>
  </Style>
  <Style ss:ID="s100">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders/>
   <Font ss:FontName="Calibri" x:CharSet="204" x:Family="Swiss" ss:Size="14"
    ss:Color="#000000" ss:Bold="1"/>
  </Style>
  <Style ss:ID="s102">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders/>
   <Font ss:FontName="Calibri" x:CharSet="204" x:Family="Swiss" ss:Size="14"
    ss:Color="#000000" ss:Bold="1"/>
  </Style>
  <Style ss:ID="s104">
   <Alignment ss:Horizontal="Left" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
  </Style>
 </Styles>
 <Worksheet ss:Name="Лист1">
  <Table>
   <Column ss:AutoFitWidth="0" ss:Width="27.75"/>
   <Column ss:AutoFitWidth="0" ss:Width="192.75"/>
   <Column ss:AutoFitWidth="0" ss:Width="74.25"/>
   <Column ss:AutoFitWidth="0" ss:Width="263.25"/>
   <Column ss:AutoFitWidth="0" ss:Width="111.75"/>
   <Column ss:AutoFitWidth="0" ss:Width="105.75" ss:Span="2"/>
   <Column ss:Index="9" ss:AutoFitWidth="0" ss:Width="114.75"/>
   <Column ss:AutoFitWidth="0" ss:Width="84"/>
   <Column ss:AutoFitWidth="0" ss:Width="93"/>
   <Row ss:Height="18.75">
    <Cell ss:Index="2" ss:MergeAcross="9" ss:StyleID="s95"><Data ss:Type="String">Отчет по дебиторской задолженности</Data></Cell>
   </Row>
   <Row ss:Height="18.75">
    <Cell ss:Index="2" ss:MergeAcross="8" ss:StyleID="s100"><Data ss:Type="String">за период с '||
		      TO_CHAR(L_DATE_START,'DD.MM.YYYY')||' по '||TO_CHAR(L_DATE_END,'DD.MM.YYYY')||' на дату '||TO_CHAR(NVL(P_DATE_ON,SYSDATE),'DD.MM.YYYY')||'</Data></Cell>
   </Row>
   <Row ss:Height="18.75">
    <Cell ss:Index="2" ss:StyleID="s102"/>
    <Cell ss:StyleID="s102"/>
    <Cell ss:StyleID="s102"/>
    <Cell ss:StyleID="s102"/>
    <Cell ss:StyleID="s102"/>
    <Cell ss:StyleID="s102"/>
   </Row>
   <Row>
    <Cell ss:Index="2" ss:StyleID="s76"><Data ss:Type="String">Категория потребителя</Data></Cell>
    <Cell ss:StyleID="s76"><Data ss:Type="String">Договор</Data></Cell>
    <Cell ss:StyleID="s76"><Data ss:Type="String">Абонент</Data></Cell>                   
    <Cell ss:StyleID="s76"><Data ss:Type="String">Общая задолженность</Data></Cell>
    <Cell ss:StyleID="s76"><Data ss:Type="String">Просроченная задолженность</Data></Cell>
    <Cell ss:StyleID="s76"><Data ss:Type="String">Текущая задолженность</Data></Cell>
    <Cell ss:StyleID="s76"><Data ss:Type="String">Кредиторская задолженность</Data></Cell>
    <Cell ss:StyleID="s76"><Data ss:Type="String">Период с</Data></Cell>
    <Cell ss:StyleID="s76"><Data ss:Type="String">Период по</Data></Cell>
    <Cell ss:StyleID="s76"><Data ss:Type="String">Количество периодов</Data></Cell>
   </Row>';
   p$s(L_XML);
   	
      L_DAY   := EXTRACT(DAY FROM NVL(P_DATE_ON,SYSDATE));  	
			L_DATE1 := TRUNC(ADD_MONTHS(NVL(P_DATE_ON,SYSDATE),-3),'MM');
			L_DATE2 := TRUNC(ADD_MONTHS(NVL(P_DATE_ON,SYSDATE),-2),'MM');


   	FOR I IN (WITH X AS (SELECT FD.ID_CONTRACT, 
                  FD.DOC_DATE,                                                                                      
									FD.AMOUNT,    
									FD.DOC_TYPE,          
									FD.OPER_TYPE, 
									FD.DOC_PERIOD,
									CG.ABON_GROUP,
								 (SELECT SUM(DECODE(FDC.OPER_TYPE,'Приход',FDC.AMOUNT,'Расход',-FDC.AMOUNT))
									FROM T_CLI_FINDOC_CONS FDC                                               
									WHERE FDC.Ext_Id_To = FD.EXT_ID
									AND   FDC.DATE_CON <= NVL(P_DATE_ON,SYSDATE)) DEBT
           FROM  T_CLI_FINDOCS FD,
					       T_CONTRACTS CT ,
								 T_CLIENTS CLI,
								 T_CUSTOMER_GROUP CG
           WHERE FD.DOC_DATE BETWEEN L_DATE_START AND L_DATE_END
					 AND   FD.ID_CONTRACT = CT.ID_CONTRACT
					 AND   CT.ID_CLIENT = CLI.ID_CLIENT
					 AND   CT.CUST_GROUP = CG.GROUP_NAME
					 AND   (CG.GROUP_NAME IN (SELECT COLUMN_VALUE FROM TABLE(APEX_STRING.split(P_GROUP,':'))) OR P_GROUP IS NULL)--INSTR(NVL(P_GROUP,CG.GROUP_NAME),CG.GROUP_NAME)!=0					 
					 )			    
					 SELECT * FROM (
			          SELECT CG.GROUP_NAME 
										,CLI.CLI_NAME
										,ct.ctr_number
										,T2.DEBT COM_DEBT
										,T.DEBT
										,T.MIN_PER
										,T.MAX_PER         
										,T.CNT_PER
										,T3.DEBT_CRED
							FROM T_CLIENTS CLI
									,T_CONTRACTS CT
									,T_CUSTOMER_GROUP CG
									,(SELECT FD.ID_CONTRACT, SUM(FD.DEBT) DEBT                                         
										FROM X FD WHERE FD.DEBT > 0   
										AND FD.OPER_TYPE = 'Приход'
										GROUP BY FD.ID_CONTRACT) T2 -- ОБЩАЯ ЗАДОЛЖЕННОСТЬ
									,(SELECT T1.ID_CONTRACT, 
													 MIN(T1.DOC_DATE) MIN_PER, 
													 MAX(T1.DOC_DATE) MAX_PER,
													 SUM(T1.DEBT) DEBT,
													 COUNT(DISTINCT T1.DOC_DATE) CNT_PER
										FROM X T1
										 WHERE T1.DEBT > 0  
										 AND T1.OPER_TYPE = 'Приход'                    
										 AND T1.DOC_PERIOD < CASE 
										                      WHEN (T1.ABON_GROUP IN ('TSG','UK') AND L_DAY < 16) OR
																					     (T1.ABON_GROUP NOT IN ('TSG','UK') AND L_DAY < 11) THEN
																							 L_DATE1
																					ELSE	L_DATE2
																					END
										 GROUP BY T1.ID_CONTRACT) T -- ПРОСРОЧЕННАЯ
                 ,(SELECT FD.ID_CONTRACT, SUM(FD.DEBT) DEBT_CRED                                         
                   FROM X FD WHERE FD.OPER_TYPE = 'Расход' GROUP BY FD.ID_CONTRACT) T3 -- КРЕДИТОРКА	 										 
							WHERE CT.CUST_GROUP = CG.GROUP_NAME
							AND  CLI.ID_CLIENT = CT.ID_CLIENT
							AND  T2.DEBT > 0
							AND  T2.ID_CONTRACT = CT.ID_CONTRACT
							AND  CT.ID_CONTRACT = T.ID_CONTRACT(+)
							AND  CT.ID_CONTRACT = T3.ID_CONTRACT(+))	
						  ORDER BY GROUP_NAME, DEBT DESC NULLS LAST)					
		LOOP					         		                                                 
				L_XML:='<Row ss:AutoFitHeight="1">'||CHR(13);
				L_XML := L_XML ||'<Cell ss:Index="2" ss:StyleID="s83"><Data ss:Type="String">'||i.GROUP_NAME||'</Data></Cell>'||CHR(13);
				L_XML := L_XML ||'<Cell ss:StyleID="s83"><Data ss:Type="String">'||I.CTR_NUMBER||'</Data></Cell>'||CHR(13);
				L_XML := L_XML ||'<Cell ss:StyleID="s104"><Data ss:Type="String">'||I.CLI_NAME||'</Data></Cell>'||CHR(13);            
				L_XML := L_XML ||'<Cell ss:StyleID="s85"><Data ss:Type="Number">'||TO_CHAR(I.COM_DEBT,L_FMT,L_NLS)||'</Data></Cell>'||CHR(13);
				L_XML := L_XML ||'<Cell ss:StyleID="s85"><Data ss:Type="Number">'||TO_CHAR(I.DEBT,L_FMT,L_NLS)||'</Data></Cell>'||CHR(13);
				L_XML := L_XML ||'<Cell ss:StyleID="s85" ss:Formula="=RC[-2]-RC[-1]"><Data ss:Type="Number">0</Data></Cell>'||CHR(13);
				L_XML := L_XML ||'<Cell ss:StyleID="s85"><Data ss:Type="Number">'||TO_CHAR(I.DEBT_CRED,L_FMT,L_NLS)||'</Data></Cell>'||CHR(13);
			  IF I.MIN_PER IS NOT NULL THEN
			    L_XML := L_XML ||'<Cell ss:StyleID="s86"><Data ss:Type="DateTime">'||TO_CHAR(I.MIN_PER,'YYYY-MM-DD')||'T00:00:00.000</Data></Cell>'||CHR(13);
				ELSE
					L_XML := L_XML ||'<Cell ss:StyleID="s86"/>'||CHR(13);
				END IF;                                     
				IF I.MAX_PER IS NOT NULL THEN
				  L_XML := L_XML ||'<Cell ss:StyleID="s87"><Data ss:Type="DateTime">'||TO_CHAR(I.MAX_PER,'YYYY-MM-DD')||'T00:00:00.000</Data></Cell>'||CHR(13);
				ELSE
					L_XML := L_XML ||'<Cell ss:StyleID="s87"/>'||CHR(13);
				END IF;                                            
				L_XML := L_XML ||'<Cell ss:StyleID="s77"><Data ss:Type="Number">'||TO_CHAR(I.CNT_PER)||'</Data></Cell>'||CHR(13);
				L_XML:=L_XML||'</Row>'||CHR(13);
			  P$S(L_XML);
     END LOOP; 		                 
		

	 L_XML := '</Table>
  <WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">
   <PageSetup>
    <Header x:Margin="0.3"/>
    <Footer x:Margin="0.3"/>
    <PageMargins x:Bottom="0.75" x:Left="0.7" x:Right="0.7" x:Top="0.75"/>
   </PageSetup>   
   <Selected/>   
   <ProtectObjects>False</ProtectObjects>
   <ProtectScenarios>False</ProtectScenarios>
  </WorksheetOptions>
 </Worksheet>
</Workbook>';

		p$s( L_XML );
	
	
	END P_ADD_DATA;

---------------------------------------------------------------------------------------------------------------------------------

  FUNCTION RUN_REPORT(P_PAR_01 VARCHAR2 DEFAULT NULL
											,P_PAR_02 VARCHAR2 DEFAULT NULL
											,P_PAR_03 VARCHAR2 DEFAULT NULL
											,P_PAR_04 VARCHAR2 DEFAULT NULL
											,P_PAR_05 VARCHAR2 DEFAULT NULL
											,P_PAR_06 VARCHAR2 DEFAULT NULL
											,P_PAR_07 VARCHAR2 DEFAULT NULL
											,P_PAR_08 VARCHAR2 DEFAULT NULL
											,P_PAR_09 VARCHAR2 DEFAULT NULL
											,P_PAR_10 VARCHAR2 DEFAULT NULL) RETURN LAWSUP.PKG_FILES.REC_DOC
	IS
	l_BLOB LAWSUP.PKG_FILES.REC_DOC;
  BEGIN

	  EXCEL_DOC := NULL;
		DBMS_LOB.CREATETEMPORARY( EXCEL_DOC.P_BLOB, TRUE );
    DBMS_LOB.OPEN( EXCEL_DOC.P_BLOB, DBMS_LOB.LOB_READWRITE );

		P_ADD_DATA(P_DATE_ON => P_PAR_01,
		           P_DATE_FROM => P_PAR_02,
							 P_DATE_TO => P_PAR_03,
							 P_GROUP   => P_PAR_07);

		DBMS_LOB.CLOSE(EXCEL_DOC.P_BLOB);

		    l_BLOB.P_BLOB := EXCEL_DOC.P_BLOB;
	      l_BLOB.P_FILE_NAME := 'Дебиторская задолженность';
				RETURN L_BLOB;

  END RUN_REPORT;

end PKG_XLS_REPORT_014;
/

