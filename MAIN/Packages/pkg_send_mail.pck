i�?create or replace package lawmain.PKG_SEND_MAIL is

  -- Author  : EUGEN
  -- Created : 11.01.2017 12:27:46
  -- Purpose : Пакет для отправки Email уведомлений
  
  PROCEDURE P_SEND_MAIL(P_MAIL_CODE   VARCHAR2,
												P_PAR_01 			VARCHAR2 DEFAULT NULL,
												P_PAR_02 			VARCHAR2 DEFAULT NULL,
												P_PAR_03 		  VARCHAR2 DEFAULT NULL,
												P_PAR_04 			VARCHAR2 DEFAULT NULL,
												P_PAR_05 			VARCHAR2 DEFAULT NULL,
												P_PAR_06 			VARCHAR2 DEFAULT NULL,
												P_PAR_07 			VARCHAR2 DEFAULT NULL,
												P_PAR_08		 	VARCHAR2 DEFAULT NULL,											
												P_PAR_09 			VARCHAR2 DEFAULT NULL,
												P_PAR_10 			VARCHAR2 DEFAULT NULL,
												P_USER        NUMBER   DEFAULT NULL,
												P_PRINT_LIST  VARCHAR2 DEFAULT NULL);
												
	--процедура создает job с отправкой писем, используюя тот же конструктор, что и P_SEND_MAIL 
	PROCEDURE P_SEND_MAIL_IN_JOB(P_MAIL_CODE   VARCHAR2,
 															 P_PAR_01 			VARCHAR2 DEFAULT NULL,
															 P_PAR_02 			VARCHAR2 DEFAULT NULL,
															 P_PAR_03 		  VARCHAR2 DEFAULT NULL,
															 P_PAR_04 			VARCHAR2 DEFAULT NULL,
															 P_PAR_05 			VARCHAR2 DEFAULT NULL,
															 P_PAR_06 			VARCHAR2 DEFAULT NULL,
															 P_PAR_07 			VARCHAR2 DEFAULT NULL,
															 P_PAR_08		 	VARCHAR2 DEFAULT NULL,											
															 P_PAR_09 			VARCHAR2 DEFAULT NULL,
															 P_PAR_10 			VARCHAR2 DEFAULT NULL,
															 P_PRINT_LIST  VARCHAR2 DEFAULT NULL,
															 P_USER        NUMBER,
															 P_SEQ         NUMBER);    
	
  PROCEDURE P_START_MAIL_JOB(P_ID NUMBER);											

	-- Процедура связывает письмо с делом юридического лица											
	PROCEDURE P_INSERT_CLI_CASE_MAIL(P_ID_CASE NUMBER   		--ИД дела
																	,P_ID_MAIL NUMBER   		--ИД email
																	,P_MAIL_TYPE VARCHAR2
																	,P_USER    NUMBER DEFAULT NULL);  --Код письма   
	
	PROCEDURE P_INSERT_CLI_SHUT_MAIL(P_ID_SHUT NUMBER
																	,P_ID_MAIL NUMBER
																	,P_MAIL_TYPE VARCHAR2
																	,P_USER NUMBER DEFAULT NULL);																

	-- Связь письма с клиентом																
	PROCEDURE P_INSERT_CLI_MAIL(P_ID_CLIENT VARCHAR2
														,P_ID_MAIL NUMBER
														,P_MAIL_TYPE VARCHAR2
														,P_COMMENT   VARCHAR2 DEFAULT NULL
														,P_ID_FOLDER NUMBER DEFAULT NULL
														,P_USER NUMBER DEFAULT NULL);																																	
																

end PKG_SEND_MAIL;
/

create or replace package body lawmain.PKG_SEND_MAIL IS

	PROCEDURE P_SEND_MAIL(P_MAIL_CODE   VARCHAR2,
												P_PAR_01 			VARCHAR2 DEFAULT NULL,
												P_PAR_02 			VARCHAR2 DEFAULT NULL,
												P_PAR_03 		  VARCHAR2 DEFAULT NULL,
												P_PAR_04 			VARCHAR2 DEFAULT NULL,
												P_PAR_05 			VARCHAR2 DEFAULT NULL,
												P_PAR_06 			VARCHAR2 DEFAULT NULL,
												P_PAR_07 			VARCHAR2 DEFAULT NULL,
												P_PAR_08		 	VARCHAR2 DEFAULT NULL,											
												P_PAR_09 			VARCHAR2 DEFAULT NULL,
												P_PAR_10 			VARCHAR2 DEFAULT NULL,
                        P_USER        NUMBER   DEFAULT NULL,
												P_PRINT_LIST  VARCHAR2 DEFAULT NULL)
	IS  
		EX_CODE_NOT_DEF EXCEPTION ;
	BEGIN
		  PKG_LOG.P$LOG(p_PROG_UNIT =>  'PKG_SEND_MAIL'
											,p_MESS => 	'P_MAIL_CODE:'||P_MAIL_CODE||CHR(10)||CHR(13)||
																'P_PAR_01:'||P_PAR_01||CHR(10)||
																'P_PAR_02:'||P_PAR_02||CHR(10)||
																'P_PAR_03:'||P_PAR_03||CHR(10)||
																'P_PAR_04:'||P_PAR_04||CHR(10)||
																'P_PAR_05:'||P_PAR_05||CHR(10)||
																'P_PAR_06:'||P_PAR_06||CHR(10)||
																'P_PAR_07:'||P_PAR_07||CHR(10)||
																'P_PAR_08:'||P_PAR_08||CHR(10)||
																'P_PAR_09:'||P_PAR_09||CHR(10)||
																'P_PAR_10:'||P_PAR_10||CHR(10)
										  ,P_d1 => SYSDATE);           

	 IF P_MAIL_CODE = 'DEBT_INFORM' THEN                                                       
		 IF P_PAR_10 = 'MASS' THEN
  		 PKG_MAIL_02.P_SEND_MASS(P_ID_FOLDER => P_PAR_01,P_REPEAT => P_PAR_02,P_ATTACH => P_PRINT_LIST,P_USER => P_USER);
		 ELSE
 			 PKG_MAIL_02.p_send_mail(P_ID_CLI_CASE => P_PAR_01,P_EMAIL => P_PAR_02,P_ATTACH => P_PRINT_LIST);	
		 END IF;  
	 ELSIF P_MAIL_CODE = 'FSSP_EXEC_NOTIF' THEN
			PKG_MAIL_03.P_SEND_MAIL(P_ID_CLI_CASE => P_PAR_01,P_EMAIL => P_PAR_02,P_ATTACH => P_PRINT_LIST); 
	 ELSIF P_MAIL_CODE = 'COURT_EMAIL_NOTIF' THEN
			PKG_MAIL_04.p_send_mail(P_ID_CLI_CASE => P_PAR_01,P_EMAIL => P_PAR_02,P_ATTACH => P_PRINT_LIST);	
	 ELSIF P_MAIL_CODE = 'SHUT_NOTIF' THEN
			PKG_MAIL_05.p_send_mail(P_ID_FOLDER => P_PAR_01,P_ATTACH => P_PRINT_LIST);
 	 ELSIF P_MAIL_CODE = 'GARANT_MAILING' THEN		 
		  PKG_MAIL_06.p_send_mail(P_ID_CLIENT => P_PAR_01,P_EMAIL => P_PAR_02,P_ATTACH => P_PRINT_LIST);
	 ELSIF P_MAIL_CODE = 'RESTRUCT_MAILING' THEN		 
		  PKG_MAIL_07.p_send_mail(P_ID_CLIENT => P_PAR_01,P_EMAIL => P_PAR_02,P_ATTACH => P_PRINT_LIST);		
 	 ELSIF P_MAIL_CODE = 'WARNING_TO_COURT' THEN		 
		  PKG_MAIL_08.p_send_mail(P_ID_CLI_CASE => P_PAR_01,P_EMAIL => P_PAR_02,P_ATTACH => P_PRINT_LIST);				
	 ELSIF P_MAIL_CODE = 'CNTR_MAIL_DEBT_NOTIF' THEN
			PKG_MAIL_09.p_send_mail(P_ID_CLIENT =>  P_PAR_01,P_EMAIL => P_PAR_02,P_ID_CONTRACT => P_PAR_03);					
	 ELSE		 
		RAISE EX_CODE_NOT_DEF;		
	 END IF;
		EXCEPTION
		WHEN EX_CODE_NOT_DEF THEN 
			NULL;             
			--ERM_PKG.raise_Error('REP_NO_DEFINE',G_F_N,P_REP_CODE); 	     									
	END;       
	
--процедура создает job с отправкой писем, используюя тот же конструктор, что и P_SEND_MAIL 
	PROCEDURE P_SEND_MAIL_IN_JOB(P_MAIL_CODE   VARCHAR2,
																P_PAR_01 			VARCHAR2 DEFAULT NULL,
																P_PAR_02 			VARCHAR2 DEFAULT NULL,
																P_PAR_03 		  VARCHAR2 DEFAULT NULL,
																P_PAR_04 			VARCHAR2 DEFAULT NULL,
																P_PAR_05 			VARCHAR2 DEFAULT NULL,
																P_PAR_06 			VARCHAR2 DEFAULT NULL,
																P_PAR_07 			VARCHAR2 DEFAULT NULL,
																P_PAR_08		 	VARCHAR2 DEFAULT NULL,											
																P_PAR_09 			VARCHAR2 DEFAULT NULL,
																P_PAR_10 			VARCHAR2 DEFAULT NULL,
																P_PRINT_LIST  VARCHAR2 DEFAULT NULL,
																P_USER        NUMBER,
																P_SEQ         NUMBER)
	IS
		
	BEGIN
    INSERT INTO T_MAIL_JOB(MAIL_CODE,PAR_01,PAR_02,PAR_03,PAR_04,PAR_05,PAR_06,
                           PAR_07,PAR_08,PAR_09,PAR_10,CREATED_BY,JOB_SEQ,PRINT_LIST) 
    VALUES (P_MAIL_CODE,P_PAR_01,P_PAR_02,P_PAR_03,P_PAR_04,P_PAR_05,P_PAR_06   
            ,P_PAR_07,P_PAR_08,P_PAR_09,P_PAR_10,P_USER ,P_SEQ,P_PRINT_LIST);    
	END;  
	
	PROCEDURE P_START_MAIL_JOB(P_ID NUMBER)
    IS
	BEGIN                                                         
		DBMS_SCHEDULER.create_job(job_name => 'MAIL_SEND_'||P_ID,job_type => 'PLSQL_BLOCK'
		                         ,enabled =>  TRUE,start_date => SYSTIMESTAMP,auto_drop => TRUE
														 ,job_action => 
   'BEGIN
		 FOR I IN (SELECT * FROM T_MAIL_JOB J WHERE J.JOB_SEQ = '||P_ID||')
		 LOOP                                                        
			 BEGIN
		    PKG_SEND_MAIL.P_SEND_MAIL(P_MAIL_CODE => I.MAIL_CODE,
																	 P_PAR_01    => I.PAR_01,
																	 P_PAR_02    => I.PAR_02,
																	 P_PAR_03    => I.PAR_03,
																	 P_PAR_04    => I.PAR_04,
																	 P_PAR_05    => I.PAR_05,
																	 P_PAR_06    => I.PAR_06,
																	 P_PAR_07    => I.PAR_07,
																	 P_PAR_08    => I.PAR_08,
																	 P_PAR_09    => I.PAR_09,
																	 P_PAR_10    => I.PAR_10,
																	 P_USER      => I.CREATED_BY,
																	 P_PRINT_LIST => I.PRINT_LIST);   
				COMMIT;
			 EXCEPTION 
				 WHEN OTHERS THEN 
					 CONTINUE;
		   END;
		 END LOOP;
		 DELETE FROM T_MAIL_JOB J WHERE J.JOB_SEQ = '||P_ID||';
	  END;');               
	END;				 
	
	PROCEDURE P_INSERT_CLI_CASE_MAIL(P_ID_CASE NUMBER
																	,P_ID_MAIL NUMBER
																	,P_MAIL_TYPE VARCHAR2
																	,P_USER NUMBER DEFAULT NULL)
	IS
	BEGIN
		 INSERT INTO T_CLI_CASE_MAILS(ID_CASE_MAIL,ID_MAIL,ID_CASE,MAIL_TYPE,CREATED_BY)
		 VALUES(SEQ_CLI_CASE_MAIL.NEXTVAL,P_ID_MAIL,P_ID_CASE,P_MAIL_TYPE,NVL(P_USER,f$_usr_id));
	END;	   
	
	PROCEDURE P_INSERT_CLI_SHUT_MAIL(P_ID_SHUT NUMBER
																	,P_ID_MAIL NUMBER
																	,P_MAIL_TYPE VARCHAR2
																	,P_USER NUMBER DEFAULT NULL)
	IS
	BEGIN
		 INSERT INTO T_CLI_SHUT_MAILS(ID_SHUT_MAIL,ID_MAIL,ID_SHUT,MAIL_TYPE,CREATED_BY)
		 VALUES(SEQ_CLI_CASE_MAIL.NEXTVAL,P_ID_MAIL,P_ID_SHUT,P_MAIL_TYPE,NVL(P_USER,f$_usr_id));
	END;	  
	
	PROCEDURE P_INSERT_CLI_MAIL(P_ID_CLIENT VARCHAR2
																	,P_ID_MAIL NUMBER
																	,P_MAIL_TYPE VARCHAR2
																	,P_COMMENT   VARCHAR2 DEFAULT NULL
																	,P_ID_FOLDER NUMBER DEFAULT NULL
																	,P_USER NUMBER DEFAULT NULL)
	IS
	BEGIN
		 INSERT INTO T_CLI_MAILS(ID_CLI_MAIL,ID_MAIL,ID_CLIENT,MAIL_TYPE,CREATED_BY,COMMENTARY,ID_FOLDER)
		 VALUES(SEQ_CLI_CASE_MAIL.NEXTVAL,P_ID_MAIL,P_ID_CLIENT,P_MAIL_TYPE,NVL(P_USER,f$_usr_id),P_COMMENT,P_ID_FOLDER);
	END;	  
	
	
end PKG_SEND_MAIL;
/

