i�?create or replace package lawmain.PKG_XLS_REPORT_009 is

  -- Author  : BES
  -- Created : 17.02.2017
  -- Purpose : Информация о возможности отключения
	-- Code:     SHUTDOWN_INFO
	
  
  FUNCTION RUN_REPORT(P_PAR_01 VARCHAR2 DEFAULT NULL
											,P_PAR_02 VARCHAR2 DEFAULT NULL
											,P_PAR_03 VARCHAR2 DEFAULT NULL
											,P_PAR_04 VARCHAR2 DEFAULT NULL
											,P_PAR_05 VARCHAR2 DEFAULT NULL
											,P_PAR_06 VARCHAR2 DEFAULT NULL
											,P_PAR_07 VARCHAR2 DEFAULT NULL
											,P_PAR_08 VARCHAR2 DEFAULT NULL
											,P_PAR_09 VARCHAR2 DEFAULT NULL
											,P_PAR_10 VARCHAR2 DEFAULT NULL) RETURN LAWSUP.PKG_FILES.REC_DOC;	  

end PKG_XLS_REPORT_009;
/

create or replace package body lawmain.PKG_XLS_REPORT_009 is

	TYPE REC_DOC IS RECORD(	P_BLOB BLOB DEFAULT NULL );

	EXCEL_DOC REC_DOC := NULL;
------------------------------------------------------------------------

	PROCEDURE p$s( ps_value IN CLOB )  -- отправляет накопленное значение ps_value в буфер и записывает
	IS
		BUFFER    	RAW(32767);
    l_offset    NUMBER := 1;
    BUF_SIZE    NUMBER := 8000;
    BUF_VAR    VARCHAR2(32767);
  BEGIN
    LOOP
    EXIT WHEN L_OFFSET > DBMS_LOB.getlength(PS_VALUE);
    BUF_VAR := DBMS_LOB.substr(PS_VALUE,BUF_SIZE,L_OFFSET);
		BUFFER := UTL_RAW.cast_to_raw( CONVERT( BUF_VAR, 'UTF8'/*'CL8MSWIN1251'*/ ) );
		DBMS_LOB.WRITEAPPEND( EXCEL_DOC.P_BLOB, UTL_RAW.LENGTH( BUFFER ), BUFFER );
    L_OFFSET := L_OFFSET + BUF_SIZE;
    END LOOP;
  END;
------------------------------------------------------------------------
	PROCEDURE P_ADD_DATA IS
	  L_XML CLOB;
		L_CNT NUMBER := 0;
		L_FMT VARCHAR2(50) := '9999999999999D00';
		L_NLS VARCHAR2(50) := 'NLS_NUMERIC_CHARACTERS=''. ''';		
		L_DATE_START DATE;
    L_DATE_END   DATE;                
		L_CLIENT     VARCHAR2(500);  
		L_CTR_NUMBER VARCHAR2(100);
	BEGIN    
		L_XML :=
			'<?xml version="1.0"?>
<?mso-application progid="Excel.Sheet"?>
<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:o="urn:schemas-microsoft-com:office:office"
 xmlns:x="urn:schemas-microsoft-com:office:excel"
 xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:html="http://www.w3.org/TR/REC-html40">
 <DocumentProperties xmlns="urn:schemas-microsoft-com:office:office">
  <Author></Author>
  <LastAuthor></LastAuthor>
  <Created>2019-05-06T12:50:24Z</Created>
  <Company></Company>
  <Version>16.00</Version>
 </DocumentProperties>
 <OfficeDocumentSettings xmlns="urn:schemas-microsoft-com:office:office">
  <AllowPNG/>
 </OfficeDocumentSettings>
 <ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel">
  <WindowHeight>12300</WindowHeight>
  <WindowWidth>28800</WindowWidth>
  <WindowTopX>0</WindowTopX>
  <WindowTopY>0</WindowTopY>
  <RefModeR1C1/>
  <ProtectStructure>False</ProtectStructure>
  <ProtectWindows>False</ProtectWindows>
 </ExcelWorkbook>
 <Styles>
  <Style ss:ID="Default" ss:Name="Normal">
   <Alignment ss:Vertical="Bottom"/>
   <Borders/>
   <Font ss:FontName="Calibri" x:CharSet="204" x:Family="Swiss" ss:Size="11"
    ss:Color="#000000"/>
   <Interior/>
   <NumberFormat/>
   <Protection/>
  </Style>
  <Style ss:ID="s119">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Interior ss:Color="#A9D08E" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s126">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
  </Style>
  <Style ss:ID="s127">
   <Alignment ss:Horizontal="Left" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
  </Style>
  <Style ss:ID="s129">
   <Alignment ss:Horizontal="Right" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <NumberFormat ss:Format="Standard"/>
  </Style>
 </Styles>
 <Worksheet ss:Name="Лист1">
  <Table>
   <Column ss:Width="92.25"/>
   <Column ss:AutoFitWidth="0" ss:Width="232.5"/>
   <Column ss:AutoFitWidth="0" ss:Width="72.75"/>
   <Column ss:AutoFitWidth="0" ss:Width="297.75"/>
   <Column ss:AutoFitWidth="0" ss:Width="384.75"/>
   <Column ss:Width="118.5"/>
   <Row ss:AutoFitHeight="0" ss:Height="33.75">
    <Cell ss:StyleID="s119"><Data ss:Type="String">Тип подключения</Data></Cell>
    <Cell ss:StyleID="s119"><Data ss:Type="String">Контрагент</Data></Cell>
    <Cell ss:StyleID="s119"><Data ss:Type="String">Договор</Data></Cell>
    <Cell ss:StyleID="s119"><Data ss:Type="String">Объект</Data></Cell>
    <Cell ss:StyleID="s119"><Data ss:Type="String">Адрес</Data></Cell>
    <Cell ss:StyleID="s119"><Data ss:Type="String">Сумма взаиморасчетов</Data></Cell>
   </Row>';
   p$s(L_XML);
   	 

   	FOR I IN (SELECT D.CON_TYPE
										,CLI.CLI_ALT_NAME
										,CT.CTR_NUMBER
										,O.OBJ_NAME
										,O.ADDRESS_NAME
										,(SELECT SUM(FD.AMOUNT)
											FROM T_CLI_FINDOCS FD
											WHERE FD.ID_CONTRACT = CT.ID_CONTRACT) AM
							FROM T_CTR_OBJ_DISABLE D
									,T_CLIENTS         CLI
									,T_CONTRACTS       CT
									,T_CONTRACT_OBJ    O
							WHERE CT.ID_CONTRACT = O.ID_CONTRACT
							AND D.ID_OBJECT = O.ID_OBJECT
							AND CLI.ID_CLIENT = CT.ID_CLIENT
              AND D.IS_ABLE = 'Y')						
		LOOP					         		                                                 
				L_XML:='<Row ss:AutoFitHeight="1">'||CHR(13);
				L_XML:=L_XML||'   <Cell ss:StyleID="s126"><Data ss:Type="String">'||I.CON_TYPE||'</Data></Cell>'||CHR(13);
				L_XML:=L_XML||'   <Cell ss:StyleID="s127"><Data ss:Type="String">'||I.CLI_ALT_NAME||'</Data></Cell>'||CHR(13);
				L_XML:=L_XML||'   <Cell ss:StyleID="s126"><Data ss:Type="Number">'||I.CTR_NUMBER||'</Data></Cell>'||CHR(13);
				L_XML:=L_XML||'   <Cell ss:StyleID="s127"><Data ss:Type="String">'||I.OBJ_NAME||'</Data></Cell>'||CHR(13);
				L_XML:=L_XML||'   <Cell ss:StyleID="s127"><Data ss:Type="String">'||I.ADDRESS_NAME||'</Data></Cell>'||CHR(13);
				L_XML:=L_XML||'   <Cell ss:StyleID="s129"><Data ss:Type="Number">'||TO_CHAR(I.AM,L_FMT,L_NLS)||'</Data></Cell>'||CHR(13);
				L_XML:=L_XML||'</Row>'||CHR(13);
			  P$S(L_XML);
     END LOOP; 		                 
		

	 L_XML := '</Table>
  <WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">
   <PageSetup>
    <Header x:Margin="0.3"/>
    <Footer x:Margin="0.3"/>
    <PageMargins x:Bottom="0.75" x:Left="0.7" x:Right="0.7" x:Top="0.75"/>
   </PageSetup>
   <Selected/>
   <Panes>
    <Pane>
     <Number>3</Number>
     <ActiveRow>11</ActiveRow>
     <ActiveCol>4</ActiveCol>
    </Pane>
   </Panes>
   <ProtectObjects>False</ProtectObjects>
   <ProtectScenarios>False</ProtectScenarios>
  </WorksheetOptions>
 </Worksheet>
</Workbook>';

		p$s( L_XML );
	
	
	END P_ADD_DATA;

---------------------------------------------------------------------------------------------------------------------------------

  FUNCTION RUN_REPORT(P_PAR_01 VARCHAR2 DEFAULT NULL
											,P_PAR_02 VARCHAR2 DEFAULT NULL
											,P_PAR_03 VARCHAR2 DEFAULT NULL
											,P_PAR_04 VARCHAR2 DEFAULT NULL
											,P_PAR_05 VARCHAR2 DEFAULT NULL
											,P_PAR_06 VARCHAR2 DEFAULT NULL
											,P_PAR_07 VARCHAR2 DEFAULT NULL
											,P_PAR_08 VARCHAR2 DEFAULT NULL
											,P_PAR_09 VARCHAR2 DEFAULT NULL
											,P_PAR_10 VARCHAR2 DEFAULT NULL) RETURN LAWSUP.PKG_FILES.REC_DOC
	IS
	l_BLOB LAWSUP.PKG_FILES.REC_DOC;
  BEGIN

	  EXCEL_DOC := NULL;
		DBMS_LOB.CREATETEMPORARY( EXCEL_DOC.P_BLOB, TRUE );
    DBMS_LOB.OPEN( EXCEL_DOC.P_BLOB, DBMS_LOB.LOB_READWRITE );

		P_ADD_DATA();

		DBMS_LOB.CLOSE(EXCEL_DOC.P_BLOB);

		    l_BLOB.P_BLOB := EXCEL_DOC.P_BLOB;
	      l_BLOB.P_FILE_NAME := 'Возможность отключения';
				RETURN L_BLOB;

  END RUN_REPORT;

end PKG_XLS_REPORT_009;
/

