i�?create or replace package lawmain.PKG_FILES is

  -- Author  : EUGEN
  -- Created : 21.11.2016 17:20:54
  -- Purpose : пакет для работы с пользовательскими файлами 
  
	TYPE FILE IS RECORD(FILE_NAME VARCHAR2(500),
	                    MIME_TYPE VARCHAR2(100),
											FILE_CONT BLOB);
	
  --ВСТАВКА ФАЙЛА  
  FUNCTION F_INSERT_FILE(P_FILE      BLOB
		                    ,P_FILE_NAME VARCHAR2
												,P_MIME_TYPE VARCHAR2
												,P_DESC      VARCHAR2
												,P_TYPE      VARCHAR2
												,P_USER      NUMBER DEFAULT f$_usr_id) RETURN NUMBER;
	
	--ЗАГРУЗКА ФАЙЛА
	FUNCTION F_UPLOAD_FILE(P_FILE_PATH VARCHAR2,						
												 P_TYPE_CODE VARCHAR2,  
												 P_DESC      VARCHAR2 DEFAULT NULL) RETURN NUMBER;
	
	--ДОБАВИТЬ ФАЙЛ К ДЕЛУ
	PROCEDURE P_ADD_FILE_TO_CASE(P_ID_FILE NUMBER,
                               P_ID_CASE NUMBER);

	--ДОБАВИТЬ ФАЙЛ К КЛИЕНТУ  
	PROCEDURE P_ADD_FILE_TO_CLIENT(P_ID_FILE   NUMBER,
		                             P_ID_CLIENT NUMBER);
	--ДОБАВИТЬ ФАЙЛ К ДОГОВОРУ															 
	PROCEDURE P_ADD_FILE_TO_CONTRACT(P_ID_FILE     NUMBER,
		                               P_ID_CONTRACT NUMBER);															 
 --УДАЛИТЬ ФАЙЛ ИЗ ДЕЛА 
  PROCEDURE P_DELETE_CLI_CASE_DOC(P_ID_LINK NUMBER);
   
 --УДАЛИТЬ ФАЙЛ ИЗ КЛИЕНТА  
  PROCEDURE P_DELETE_CLI_DOC(P_ID_LINK NUMBER);
   
 --ЗАГРУЗИТЬ ФАЙЛ  
  PROCEDURE P_DOWNLOAD_FILE(P_ID_FILE NUMBER);

--Создать тип файла    
	PROCEDURE P_CREATE_FILE_TYPE(P_TYPE VARCHAR2,
 		                           P_NAME VARCHAR2,
															 P_DESCR VARCHAR2);	

--ОБНОВЛЕНИЕ ТИПА ФАЙЛА	
  PROCEDURE P_UPDATE_FILE_TYPE(P_ID_TYPE   NUMBER,	  
		                           P_TYPE      VARCHAR2,
		                           P_NAME      VARCHAR2,
															 P_DESCR     VARCHAR2,
															 P_IS_ACTIVE VARCHAR2);		
															 
  PROCEDURE P_ADD_FILE_TO_BNK(P_ID_FILE NUMBER,
		                          P_ID_BANKRUPT NUMBER);	
	
		
	PROCEDURE P_ADD_SIGN_TO_FILE(P_FILE BLOB, 
		                           P_FILE_NAME VARCHAR2, 
															 P_HASH VARCHAR2,
															 P_BASE BLOB, 															 
															 P_ID_FILE NUMBER);  
	
	PROCEDURE P_DELETE_FILE(P_ID_FILE NUMBER);														 
	
	PROCEDURE P_DOWNLOAD_SIGN(P_ID_FILE NUMBER);			  
	
	FUNCTION F_GET_FILE(P_ID_FILE NUMBER) RETURN FILE;											 																												 													 														 
end PKG_FILES;
/

create or replace package body lawmain.PKG_FILES is


  FUNCTION F_INSERT_FILE(P_FILE      BLOB
                        ,P_FILE_NAME VARCHAR2
                        ,P_MIME_TYPE VARCHAR2
                        ,P_DESC      VARCHAR2
                        ,P_TYPE      VARCHAR2
												,P_USER      NUMBER DEFAULT f$_usr_id) RETURN NUMBER
    IS                                                                                             
    L_RET NUMBER;                         
		L_PATH VARCHAR2(1000);                     
		L_NAME VARCHAR2(1000);    
		L_FILE UTL_FILE.FILE_TYPE;
		L_LEN  NUMBER;   
	  l_amount    BINARY_INTEGER := 32767;
		l_buffer    RAW(32767);			  
		l_pos       NUMBER := 1;
  BEGIN                   
		 L_PATH := '/u02/files01/'||TO_CHAR(SYSDATE,'YYYY/MM/DD');    		 
		 EXECUTE IMMEDIATE 'CREATE OR REPLACE DIRECTORY FILES01 AS '''||L_PATH||'''';
		 L_NAME := SEQ_FILES.NEXTVAL||'_'||REPLACE(P_FILE_NAME,',','_');               
		              
		 L_LEN  := DBMS_LOB.GETLENGTH(P_FILE);
	 	 L_FILE := UTL_FILE.FOPEN('FILES01',L_NAME,'WB',32767);		
		
		 WHILE l_pos < L_LEN
			LOOP
				DBMS_LOB.read(P_FILE,l_amount,l_pos,l_buffer);				
				UTL_FILE.put_raw(l_file, l_buffer, TRUE);
				l_pos := l_pos + l_amount;
		 END LOOP;		
			
		 UTL_FILE.fclose(l_file);
		 
     INSERT INTO T_FILES(ID_FILE,FILE_PATH,FILE_NAME,MIME_TYPE,DATE_CREATED,CREATED_BY,FILE_DESCRIPTION,TYPE_CODE)
     VALUES (seq_files.CURRVAL,L_PATH,L_NAME,P_MIME_TYPE,SYSDATE,P_USER,P_DESC, P_TYPE) 
     RETURN ID_FILE INTO L_RET;
		
	   RETURN L_RET;        
	EXCEPTION
		 WHEN OTHERS THEN
				IF UTL_FILE.is_open (l_file) THEN
					 UTL_FILE.fclose (l_file);
				END IF;
				RAISE;		 
	END;
	
----------------------------------------------------------------------------------------------	
	
	FUNCTION F_UPLOAD_FILE(P_FILE_PATH VARCHAR2,						
												 P_TYPE_CODE VARCHAR2,  
												 P_DESC      VARCHAR2 DEFAULT NULL) RETURN NUMBER
		 IS     
     L_BLOB BLOB;
		 L_MIME_TYPE VARCHAR2(255);
		 L_FILE_NAME VARCHAR2(400);                                                                     
	BEGIN                                              
		 SELECT WWV.filename, WWV.mime_type, WWV.blob_content 
		 INTO L_FILE_NAME,L_MIME_TYPE,L_BLOB 
		 FROM APEX_APPLICATION_TEMP_FILES WWV
		 WHERE WWV.name = P_FILE_PATH;
		 
		 
		 RETURN F_INSERT_FILE(L_BLOB,L_FILE_NAME,L_MIME_TYPE,P_DESC,P_TYPE_CODE);
		 
		 EXCEPTION
			  WHEN OTHERS THEN
					 PKG_LOG.P$LOG(p_PROG_UNIT => 'PKG_FILES',p_MESS => SUBSTR(SQLERRM,1,4000),P_C1 => DBMS_UTILITY.FORMAT_ERROR_STACK);
					 RAISE_APPLICATION_ERROR(-20200,'Ошибка при загруке файла');
	END;
	
	PROCEDURE P_ADD_SIGN_TO_FILE(P_FILE BLOB, 
		                           P_FILE_NAME VARCHAR2, 
															 P_HASH VARCHAR2,
															 P_BASE BLOB, 															 
															 P_ID_FILE NUMBER)
    IS        
    L_ID       NUMBER;                                           
		L_HASH  VARCHAR2(200);
		WRONG_HASH EXCEPTION;
  BEGIN
    
		L_HASH := DBMS_CRYPTO.HASH(P_BASE,DBMS_CRYPTO.HASH_MD5);
		IF UPPER(L_HASH) != UPPER(P_HASH) THEN
			 RAISE  WRONG_HASH;
		END IF;
				            
    SELECT F.ID_FILE INTO L_ID 
		FROM T_FILE_SIGN F 
		WHERE F.ID_FILE = P_ID_FILE;
		    
    UPDATE T_FILE_SIGN F
    SET    F.SIGN_CONTENT  = P_FILE,
           F.SIGN_NAME    = P_FILE_NAME,
           F.SIGN_MIME    = 'APPLICATION/OCTET',
           F.CREATED_BY  = f$_usr_id,
           F.DATE_CREATED = SYSDATE
    WHERE  F.ID_FILE = L_ID;     
		
		UPDATE T_FILES S 
		   SET S.DATE_MODIFIED = SYSDATE
			    ,S.MODIFIED_BY = F$_USR_ID
		WHERE S.ID_FILE = P_ID_FILE;

    EXCEPTION
      WHEN NO_DATA_FOUND THEN    
        INSERT INTO T_FILE_SIGN(ID_FILE,SIGN_CONTENT,SIGN_NAME,SIGN_MIME,DATE_CREATED,CREATED_BY)
				VALUES(P_ID_FILE,P_FILE,P_FILE_NAME,'APPLICATION/OCTET',SYSDATE,F$_USR_ID);
		
				UPDATE T_FILES S 
					 SET S.DATE_MODIFIED = SYSDATE
							,S.MODIFIED_BY = F$_USR_ID
				WHERE S.ID_FILE = P_ID_FILE;
			WHEN WRONG_HASH THEN
				RAISE_APPLICATION_ERROR(-20200,'Ошибка при загрузке подписи. Не совпадает хэш. '||L_HASH);	
  END;
	
---------------------------------------------------------------------------------------------	
	
	PROCEDURE P_ADD_FILE_TO_CASE(P_ID_FILE NUMBER,
		                           P_ID_CASE NUMBER)
	  IS
	BEGIN
		INSERT INTO T_LINK_FILE_CLI_WORK(ID_LINK,ID_FILE,ID_WORK)
		VALUES(SEQ_DOC_FILE.NEXTVAL,P_ID_FILE,P_ID_CASE);
	END;		 
	
--------------------------------------------------------------------------------------------

	PROCEDURE P_ADD_FILE_TO_BNK(P_ID_FILE NUMBER,
		                          P_ID_BANKRUPT NUMBER)
	  IS
	BEGIN
		INSERT INTO T_LINK_FILE_CLI_BNK(ID_LINK,ID_FILE,ID_BANKRUPT)
		VALUES(SEQ_DOC_FILE.NEXTVAL,P_ID_FILE,P_ID_BANKRUPT);
	END;		

---------------------------------------------------------------------------------------------
	
	PROCEDURE P_ADD_FILE_TO_CLIENT(P_ID_FILE   NUMBER,
		                             P_ID_CLIENT NUMBER)
	  IS
	BEGIN
		INSERT INTO T_LINK_FILE_CLIENT(ID_LINK,ID_FILE,ID_CLIENT)
		VALUES(SEQ_DOC_FILE.NEXTVAL,P_ID_FILE,P_ID_CLIENT);
	END;		
	
--------------------------------------------------------------------------------------------

	PROCEDURE P_ADD_FILE_TO_CONTRACT(P_ID_FILE     NUMBER,
		                               P_ID_CONTRACT NUMBER)
	  IS
	BEGIN
		INSERT INTO T_LINK_FILE_CONTRACT(ID_LINK,ID_FILE,ID_CONTRACT)
		VALUES(SEQ_DOC_FILE.NEXTVAL,P_ID_FILE,P_ID_CONTRACT);
	END;														 
													 

---------------------------------------------------------------------------------------------															  
	
	PROCEDURE P_DELETE_CLI_CASE_DOC(P_ID_LINK NUMBER)
		 IS                                                                       
		 L_ID_FILE    NUMBER;        
		 L_COUNT_CASE NUMBER;
 		 L_COUNT_CLI  NUMBER;     
		 l_file_path  t_files.FILE_PATH%TYPE;    
		 l_file_name  t_files.file_name%TYPE;     
	BEGIN                                                             
		SELECT F.ID_FILE INTO L_ID_FILE FROM T_LINK_FILE_CLI_WORK F WHERE F.ID_LINK = P_ID_LINK;
		
    DELETE FROM T_LINK_FILE_CLI_WORK C WHERE C.ID_LINK = P_ID_LINK;
		
		SELECT COUNT(1) INTO L_COUNT_CASE FROM T_LINK_FILE_CLI_WORK CC WHERE CC.ID_FILE = L_ID_FILE;
		SELECT COUNT(1) INTO L_COUNT_CLI FROM T_LINK_FILE_CLIENT CC WHERE CC.ID_FILE = L_ID_FILE;
		
		IF L_COUNT_CASE = 0 AND L_COUNT_CLI = 0 THEN               
			 P_DELETE_FILE(L_ID_FILE);
			 
       DELETE FROM T_FILE_SIGN S WHERE S.ID_FILE = L_ID_FILE;    
			 DELETE FROM T_FILE_SIGN S WHERE S.ID_FILE = L_ID_FILE;
			 DELETE FROM T_FILES F WHERE F.ID_FILE = L_ID_FILE;
		END IF;
		
  EXCEPTION
	  WHEN OTHERS THEN
					 PKG_LOG.P$LOG(p_PROG_UNIT => 'PKG_FILES',p_MESS => SUBSTR(SQLERRM,1,4000),P_C1 => P_ID_LINK);
					 RAISE_APPLICATION_ERROR(-20200,'Ошибка при удалении файла'); 
	END;                                            

---------------------------------------------------------------------------------------------
	
  PROCEDURE P_DELETE_CLI_DOC(P_ID_LINK NUMBER) 
		 IS                                                                       
		 L_ID_FILE    NUMBER;        
		 L_COUNT_CASE NUMBER;
 		 L_COUNT_CLI  NUMBER;    
 		 l_file_path  t_files.FILE_PATH%TYPE;         
	 	 l_file_name  t_files.file_name%TYPE;       
	BEGIN                                                             
		SELECT F.ID_FILE INTO L_ID_FILE FROM T_LINK_FILE_CLIENT F WHERE F.ID_LINK = P_ID_LINK;
		
    DELETE FROM T_LINK_FILE_CLIENT C WHERE C.ID_LINK = P_ID_LINK;
		
		SELECT COUNT(1) INTO L_COUNT_CASE FROM T_LINK_FILE_CLI_WORK CC WHERE CC.ID_FILE = L_ID_FILE;
		SELECT COUNT(1) INTO L_COUNT_CLI FROM T_LINK_FILE_CLIENT CC WHERE CC.ID_FILE = L_ID_FILE;
		
		IF L_COUNT_CASE = 0 AND L_COUNT_CLI = 0 THEN                 			
			 P_DELETE_FILE(L_ID_FILE);
       DELETE FROM T_FILE_SIGN S WHERE S.ID_FILE = L_ID_FILE;    			 
			 DELETE FROM T_FILES F WHERE F.ID_FILE = L_ID_FILE;        			 			 
		END IF;
		
  EXCEPTION
	  WHEN OTHERS THEN
					 PKG_LOG.P$LOG(p_PROG_UNIT => 'PKG_FILES',p_MESS => SUBSTR(SQLERRM,1,4000));
					 RAISE_APPLICATION_ERROR(-20200,'Ошибка при удалении файла'); 
	END;                      

---------------------------------------------------------------------------------------------
	
  FUNCTION F_GET_FILE(P_ID_FILE NUMBER) RETURN FILE
		IS    
		l_file_path     t_files.FILE_PATH%TYPE;     
	  l_amount    BINARY_INTEGER := 32767;
		l_buffer    RAW(32767);			
		L_FILE      UTL_FILE.FILE_TYPE;   
		L_RET       FILE;
	BEGIN
		DBMS_LOB.CREATETEMPORARY(L_RET.FILE_CONT,FALSE);
	  
		SELECT file_path,mime_type,FILE_NAME
		INTO   l_file_path,L_RET.MIME_TYPE,L_RET.FILE_NAME
		FROM   T_FILES
		WHERE  ID_FILE = P_ID_FILE;
		                                 
		EXECUTE IMMEDIATE 'CREATE OR REPLACE DIRECTORY FILES02 AS '''||L_FILE_PATH||'''';
		L_FILE := UTL_FILE.FOPEN('FILES02',L_RET.FILE_NAME,'RB',32767);
		
		LOOP
      BEGIN
          UTL_FILE.GET_RAW(L_FILE,l_buffer,l_amount);
					DBMS_LOB.APPEND(L_RET.FILE_CONT,L_BUFFER);
      EXCEPTION 
				WHEN NO_DATA_FOUND THEN 
					EXIT; 
			END;
    END LOOP;
		
	 UTL_FILE.fclose(l_file);
	 RETURN L_RET; 
	 
	EXCEPTION
		 WHEN OTHERS THEN
				IF UTL_FILE.is_open (l_file) THEN

					 UTL_FILE.fclose (l_file);
				END IF;
				RAISE;		 
	END;

-------------------------------------------------------------------------

	PROCEDURE P_DOWNLOAD_FILE(P_ID_FILE NUMBER) 
		 IS
		 L_FILE FILE;			
	BEGIN        
    
	  L_FILE := F_GET_FILE(P_ID_FILE);
		
    htp.flush;
    htp.init;
		--OWA_UTIL.mime_header('APPLICATION/OCTET', FALSE);
		OWA_UTIL.mime_header('document/octet-stream', FALSE);
		HTP.p('Content-Length: ' || DBMS_LOB.getlength(L_FILE.FILE_CONT));
		HTP.p('Content-Disposition: filename="' || L_FILE.FILE_NAME || '"');
		OWA_UTIL.http_header_close;

		WPG_DOCLOAD.download_file(L_FILE.FILE_CONT);
    APEX_APPLICATION.stop_apex_engine;

		
	END;                 
	
	
	PROCEDURE P_DOWNLOAD_SIGN(P_ID_FILE NUMBER) 
		 IS
		l_blob_content  T_FILE_SIGN.SIGN_CONTENT%TYPE;
		l_mime_type     T_FILE_SIGN.SIGN_MIME%TYPE;
		l_file_name     T_FILE_SIGN.SIGN_NAME%TYPE;
	BEGIN
		SELECT S.SIGN_CONTENT,
					 S.SIGN_MIME,
					 S.SIGN_NAME
		INTO   l_blob_content,
					 l_mime_type,
					 l_file_name
		FROM   T_FILE_SIGN S
		WHERE  S.ID_FILE = P_ID_FILE;
		
    htp.flush;
    htp.init;
		--OWA_UTIL.mime_header('APPLICATION/OCTET', FALSE);
		OWA_UTIL.mime_header('document/octet-stream', FALSE);
		HTP.p('Content-Length: ' || DBMS_LOB.getlength(l_blob_content));
		HTP.p('Content-Disposition: filename="' || l_file_name || '"');
		OWA_UTIL.http_header_close;

		WPG_DOCLOAD.download_file(l_blob_content);
    APEX_APPLICATION.stop_apex_engine;
	END;                                                                                       
	
---------------------------------------------------------------------------------	
	
	PROCEDURE P_CREATE_FILE_TYPE(P_TYPE VARCHAR2,
		                           P_NAME VARCHAR2,
															 P_DESCR VARCHAR2)															 
		IS                                         
		L_CODE VARCHAR2(50);
	BEGIN                 
		 L_CODE := REPLACE(SUBSTR(PKG_UTILS.F_MAKE_TRANSLIT(P_NAME),1,50),' ','_');
		 
		 INSERT INTO T_FILE_TYPES(ID_TYPE,TYPE_CODE,TYPE_NAME,TYPE_LINK,DATE_CREATED,CREATED_BY,TYPE_DESCR)    
		 VALUES(SEQ_MAIN.NEXTVAL,L_CODE,P_NAME,P_TYPE,SYSDATE,F$_USR_ID,P_DESCR);												
	END;

--------------------------------------------------------------------------------------
	
  PROCEDURE P_UPDATE_FILE_TYPE(P_ID_TYPE NUMBER,
		                           P_TYPE VARCHAR2,
		                           P_NAME VARCHAR2,
															 P_DESCR VARCHAR2,
															 P_IS_ACTIVE VARCHAR2)															 
		IS                                         
		L_CODE VARCHAR2(50);
	BEGIN                 
		 UPDATE T_FILE_TYPES  F
		    SET F.TYPE_NAME = P_NAME
				   ,F.DATE_MODIFIED = SYSDATE
					 ,F.MODIFIED_BY = f$_usr_id          
					 ,F.TYPE_DESCR = P_DESCR  
					 ,F.IS_ACTIVE  = P_IS_ACTIVE
			WHERE	F.ID_TYPE = P_ID_TYPE;
	END;      

------------------------------------------------------------------------------------
	
	PROCEDURE P_DELETE_FILE(P_ID_FILE NUMBER)
		IS        
		L_FILE_PATH VARCHAR2(500);
		L_FILE_NAME VARCHAR2(500);
	BEGIN
		   SELECT file_path,file_name INTO l_file_path, l_file_name FROM T_FILES WHERE  ID_FILE = P_ID_FILE;		                                 			 			 
		   EXECUTE IMMEDIATE 'CREATE OR REPLACE DIRECTORY FILES03 AS '''||L_FILE_PATH||'''';
       UTL_FILE.FREMOVE('FILES03',L_FILE_NAME);           			 
	END;

	
end PKG_FILES;
/

