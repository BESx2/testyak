i�?CREATE OR REPLACE PACKAGE LAWMAIN.PKG_LEVY IS

	-- Author  : EUGEN
	-- Created : 13.03.2019 20:06:50
	-- Purpose : Для работы с исполнительными документами

	-- Процедура создаёт исполнительный лист для дела
	PROCEDURE P_CREATE_CLI_LEVY(P_ID_CASE        NUMBER
														 ,P_LEVY_NUM       VARCHAR2
														 ,P_LEVY_DATE      DATE
														 ,P_LEVY_PENY      NUMBER
														 ,P_LEVY_PENY_DATE DATE
														 ,P_LEVY_GOV_TOLL  NUMBER
														 ,P_LEVY_DEBT      NUMBER
														 ,P_LEVY_SERIES    VARCHAR2
														 ,P_DATE_RECEIPT   DATE
														 ,P_LEVY_TYPE      VARCHAR2);

	-- Процедура обновляет информацию по испол листу
	PROCEDURE P_UPDATE_CLI_LEVY(P_ID_LEVY        NUMBER
														 ,P_LEVY_NUM       VARCHAR2
														 ,P_LEVY_DATE      DATE
														 ,P_LEVY_PENY      NUMBER
														 ,P_LEVY_PENY_DATE DATE
														 ,P_LEVY_GOV_TOLL  NUMBER
														 ,P_LEVY_DEBT      NUMBER
														 ,P_LEVY_SERIES    VARCHAR2
                             ,P_DATE_RECEIPT   DATE
														 ,P_LEVY_TYPE      VARCHAR2);

	--СОЗДАЁТ ДВИЖЕНИЕ ПО ИСПОЛНИТЕЛЬНОМУ ЛИСТУ													 
	PROCEDURE P_CREATE_CLI_LEVY_MOV(P_ID_LEVY       NUMBER,  
		                              P_ID_PLACE      NUMBER, 
		                              P_MOV_TYPE      VARCHAR2,
																	P_MOV_COMMENTS  VARCHAR2 DEFAULT NULL,
																	P_MOV_SEND_DATE DATE,
																	P_DEBT          NUMBER,
																	P_PENY          NUMBER,
																	P_TOLL          NUMBER);     

  --ОБНОВЛЕНИЕ ДВИЖЕНИЯ ПО ИСПОЛ ЛИСТУ																	
 	PROCEDURE P_UPDATE_CLI_LEVY_MOV(P_ID_MOV       NUMBER,  
		                              P_ID_PLACE      NUMBER, 
		                              P_MOV_TYPE      VARCHAR2,
																	P_MOV_COMMENTS  VARCHAR2 DEFAULT NULL,
																	P_MOV_SEND_DATE DATE,
																	P_DEBT          NUMBER,
																	P_PENY          NUMBER,
																	P_TOLL          NUMBER);																	
	
	--СОЗДАНИЕ ИСПОЛ. ПРОИЗВОДСТВА																
	PROCEDURE P_CREATE_CLI_LEVY_EXEC(P_EXEC_NUM        VARCHAR2
																	,P_EXEC_START_DATE DATE						
																	,P_ID_BAILIFF      NUMBER
																	,P_ID_LEVY         NUMBER);		
	
	--ОБНОВЛЕНИЕ ИСПОЛ ПРОИЗВОДСТВА																
  PROCEDURE P_UPDATE_CLI_LEVY_EXEC(P_EXEC_NUM        VARCHAR2
																	,P_EXEC_START_DATE DATE
																	,P_ID_BAILIFF      NUMBER
																	,P_ID_EXEC         NUMBER); 		

	--ОКОНЧАНИЕ ИСПОЛ. ПРОИЗВОДСТВА																
	PROCEDURE P_END_CLI_EXEC(P_ID_EXEC NUMBER,
													 P_DATE_END DATE,
													 P_REASON   NUMBER,
													 P_COMMENT  VARCHAR2 DEFAULT NULL);  																																																				
  														 

END PKG_LEVY;
/

CREATE OR REPLACE PACKAGE BODY LAWMAIN.PKG_LEVY IS

	PROCEDURE P_CREATE_CLI_LEVY(P_ID_CASE        NUMBER
														 ,P_LEVY_NUM       VARCHAR2
														 ,P_LEVY_DATE      DATE
														 ,P_LEVY_PENY      NUMBER
														 ,P_LEVY_PENY_DATE DATE
														 ,P_LEVY_GOV_TOLL  NUMBER
														 ,P_LEVY_DEBT      NUMBER
														 ,P_LEVY_SERIES    VARCHAR2
														 ,P_DATE_RECEIPT   DATE
														 ,P_LEVY_TYPE      VARCHAR2) IS
	BEGIN
		INSERT INTO T_CLI_LEVY (ID_LEVY,ID_CASE,LEVY_NUM,LEVY_DATE,LEVY_PENY,LEVY_GOV_TOLL,
                            LEVY_DEBT,DATE_CREATED,CREATED_BY,LEVY_STATUS,LEVY_SERIES,
														LEVY_PENY_DATE, LEVY_TYPE)
		VALUES (SEQ_LEVY.NEXTVAL,P_ID_CASE,P_LEVY_NUM,P_LEVY_DATE,P_LEVY_PENY,P_LEVY_GOV_TOLL,
   			    P_LEVY_DEBT,SYSDATE,F$_USR_ID,'AT_FSSP',P_LEVY_SERIES,P_LEVY_PENY_DATE,P_LEVY_TYPE);
	
		UPDATE T_CLI_CASES S
		SET    S.GOV_TOLL = P_LEVY_GOV_TOLL
		      ,S.PENY_COURT = P_LEVY_PENY
					,S.PENY = P_LEVY_PENY     
		WHERE  S.ID_CASE = P_ID_CASE;
	
		PKG_EVENTS.P_CREATE_CLI_EVENT(P_ID_CASE, 'LEVY_GET', P_DATE_RECEIPT);
	END;

---------------------------------------------------------------------------------------

	PROCEDURE P_UPDATE_CLI_LEVY(P_ID_LEVY        NUMBER
														 ,P_LEVY_NUM       VARCHAR2
														 ,P_LEVY_DATE      DATE
														 ,P_LEVY_PENY      NUMBER
														 ,P_LEVY_PENY_DATE DATE
														 ,P_LEVY_GOV_TOLL  NUMBER
														 ,P_LEVY_DEBT      NUMBER
														 ,P_LEVY_SERIES    VARCHAR2
														 ,P_DATE_RECEIPT   DATE
														 ,P_LEVY_TYPE      VARCHAR2) 
	 IS
	 L_ID_CASE NUMBER;
	BEGIN
		UPDATE T_CLI_LEVY L
		SET    L.LEVY_NUM       = P_LEVY_NUM,
					 L.LEVY_DATE      = P_LEVY_DATE,
					 L.LEVY_PENY      = P_LEVY_PENY,
					 L.LEVY_PENY_DATE = P_LEVY_PENY_DATE,
					 L.LEVY_GOV_TOLL  = P_LEVY_GOV_TOLL,
					 L.LEVY_DEBT      = P_LEVY_DEBT,
					 L.LEVY_SERIES    = P_LEVY_SERIES,
					 L.MODIFIED_BY    = F$_USR_ID,
					 L.DATE_MODIFIED  = SYSDATE,
					 L.LEVY_TYPE      = P_LEVY_TYPE      
		WHERE  L.ID_LEVY = P_ID_LEVY
		RETURNING L.ID_CASE INTO L_ID_CASE;              
		
  	UPDATE T_CLI_CASES S
	   	 SET  S.GOV_TOLL = P_LEVY_GOV_TOLL
			     ,S.PENY_COURT = P_LEVY_PENY     
					 ,S.PENY       = P_LEVY_PENY     
		 WHERE  S.ID_CASE = L_ID_CASE;
		
		
		UPDATE T_CLI_EVENTS EV                       
		   SET EV.DATE_EVENT = P_DATE_RECEIPT
		WHERE EV.ID_EVENT = (SELECT MAX(EV1.id_event) FROM V_CLI_EVENTS EV1
		                     WHERE EV1.code_event = 'LEVY_GET'
												 AND   EV1.ID_WORK = L_ID_CASE);
		
	END;    

-------------------------------------------------------------------------------------
	
	PROCEDURE P_CREATE_CLI_LEVY_MOV(P_ID_LEVY       NUMBER,  
		                              P_ID_PLACE      NUMBER, 
		                              P_MOV_TYPE      VARCHAR2,
																	P_MOV_COMMENTS  VARCHAR2 DEFAULT NULL,
																	P_MOV_SEND_DATE DATE,
																	P_DEBT          NUMBER,
																	P_PENY          NUMBER,
																	P_TOLL          NUMBER)
	 IS
	 L_ID_CASE NUMBER;
	BEGIN  
		SELECT L.ID_CASE INTO L_ID_CASE FROM T_CLI_LEVY L WHERE L.ID_LEVY = P_ID_LEVY;       
		
    INSERT INTO T_CLI_LEVY_MOVING(ID_MOV,ID_LEVY,ID_PLACE,MOV_TYPE,MOV_SEND_DATE,
                                  MOV_COMMENTS,CREATED_BY,DATE_CREATED,DEBT,PENY,GOV_TOLL)
  	VALUES(SEQ_LEVY_MOV.NEXTVAL,P_ID_LEVY,P_ID_PLACE,P_MOV_TYPE,P_MOV_SEND_DATE,
		       P_MOV_COMMENTS,f$_usr_id,SYSDATE,P_DEBT,P_PENY,P_TOLL);
					 
		IF P_MOV_TYPE = 'ROSP' THEN
			PKG_EVENTS.P_CREATE_CLI_EVENT(P_ID_WORK =>  L_ID_CASE,P_CODE => 'LEVY_SEND_FSSP',P_DATE => P_MOV_SEND_DATE);
	  ELSIF P_MOV_TYPE = 'BANK' THEN                                                                               
			PKG_EVENTS.P_CREATE_CLI_EVENT(P_ID_WORK => L_ID_CASE,P_CODE => 'LEVY_SEND_BANK',P_DATE => P_MOV_SEND_DATE);
		ELSIF P_MOV_TYPE = 'UFK' THEN                                                                                
			PKG_EVENTS.P_CREATE_CLI_EVENT(P_ID_WORK => L_ID_CASE,P_CODE => 'LEVY_SEND_UFK',P_DATE => P_MOV_SEND_DATE);
		ELSIF P_MOV_TYPE = 'DEPFIN' THEN
			PKG_EVENTS.P_CREATE_CLI_EVENT(P_ID_WORK => L_ID_CASE,P_CODE => 'LEVY_SEND_DEPFIN',P_DATE => P_MOV_SEND_DATE);
		END IF; 								 
	END;																                                                                                                  

----------------------------------------------------------------------------------------------
	
	PROCEDURE P_UPDATE_CLI_LEVY_MOV(P_ID_MOV       NUMBER,  
		                              P_ID_PLACE      NUMBER, 
		                              P_MOV_TYPE      VARCHAR2,
																	P_MOV_COMMENTS  VARCHAR2 DEFAULT NULL,
																	P_MOV_SEND_DATE DATE,
																	P_DEBT          NUMBER,
																	P_PENY          NUMBER,
																	P_TOLL          NUMBER)
	 IS
	 L_ID_CASE NUMBER;
	 L_ID_LEVY NUMBER;
	BEGIN  
		
		UPDATE T_CLI_LEVY_MOVING G
		   SET G.ID_PLACE = P_ID_PLACE,
			     G.MOV_TYPE = P_MOV_TYPE,
					 G.MOV_SEND_DATE = P_MOV_SEND_DATE,
					 G.MOV_COMMENTS = P_MOV_COMMENTS,
					 G.DEBT         = P_DEBT,
					 G.PENY         = P_PENY,
					 G.GOV_TOLL     = P_TOLL,
					 G.MODIFIED_BY = F$_USR_ID,
					 G.DATE_MODIFIED = SYSDATE
		 WHERE G.ID_MOV = P_ID_MOV
		 RETURNING G.ID_LEVY INTO L_ID_LEVY;		 
				
		SELECT L.ID_CASE INTO L_ID_CASE FROM T_CLI_LEVY L WHERE L.ID_LEVY = L_ID_LEVY;       
					 
		UPDATE T_CLI_EVENTS E                      
		   SET E.DATE_EVENT = P_MOV_SEND_DATE
		 WHERE E.ID_EVENT = (SELECT MAX(EV.id_event) FROM V_CLI_EVENTS EV
		                     WHERE EV.code_event = 'LEVY_SEND'
												 AND   EV.ID_WORK = L_ID_CASE);
																	
	END;	
	
--------------------------------------------------------------------------------------------	
	
	PROCEDURE P_CREATE_CLI_LEVY_EXEC(P_EXEC_NUM        VARCHAR2
																	,P_EXEC_START_DATE DATE
																	,P_ID_BAILIFF      NUMBER
																	,P_ID_LEVY         NUMBER) 
	  IS                          
		L_ID_CASE NUMBER;															
	BEGIN              
		SELECT L.ID_CASE INTO L_ID_CASE FROM T_CLI_LEVY L WHERE L.ID_LEVY = P_ID_LEVY;       
		
		INSERT INTO T_CLI_LEVY_EXECS(ID_EXEC,EXEC_NUM,EXEC_START_DATE,
                          			 ID_BAILIFF,ID_LEVY,DATE_CREATED,CREATED_BY)
		VALUES (SEQ_LEVY_EXEC.NEXTVAL,P_EXEC_NUM,P_EXEC_START_DATE,
     			  P_ID_BAILIFF,P_ID_LEVY,SYSDATE,F$_USR_ID);
						
    PKG_EVENTS.P_CREATE_CLI_EVENT(P_ID_WORK => L_ID_CASE,P_CODE => 'EXEC_START',P_DATE => P_EXEC_START_DATE);
	END;			
	
------------------------------------------------------------------------------------------
	
	PROCEDURE P_UPDATE_CLI_LEVY_EXEC(P_EXEC_NUM        VARCHAR2
																	,P_EXEC_START_DATE DATE
																	,P_ID_BAILIFF      NUMBER
																	,P_ID_EXEC         NUMBER) 
	  IS                          
		L_ID_CASE NUMBER;									
		L_ID_LEVY NUMBER;						
	BEGIN              
		
    UPDATE T_CLI_LEVY_EXECS S
		   SET S.EXEC_NUM = P_EXEC_NUM
			    ,S.EXEC_START_DATE = P_EXEC_START_DATE
					,S.ID_BAILIFF = P_ID_BAILIFF                    
					,S.MODIFIED_BY = f$_usr_id
					,S.DATE_MODIFIED = SYSDATE
		 WHERE S.ID_EXEC = P_ID_EXEC
		 RETURNING S.ID_LEVY INTO L_ID_LEVY;
		
		SELECT L.ID_CASE INTO L_ID_CASE FROM T_CLI_LEVY L WHERE L.ID_LEVY = L_ID_LEVY;        
						
   	UPDATE T_CLI_EVENTS E                      
		   SET E.DATE_EVENT = P_EXEC_START_DATE
		 WHERE E.ID_EVENT = (SELECT MAX(EV.id_event) FROM V_CLI_EVENTS EV
		                     WHERE EV.code_event = 'EXEC_START'
												 AND   EV.ID_WORK = L_ID_CASE);
	END;															

----------------------------------------------------------------------------------------
	
	PROCEDURE P_END_CLI_EXEC(P_ID_EXEC NUMBER,
		                       P_DATE_END DATE,
													 P_REASON   NUMBER,
													 P_COMMENT  VARCHAR2 DEFAULT NULL)  
		IS                               
		L_ID_CASE  NUMBER; 
		L_ID_LEVY  NUMBER;
		L_ID_EVENT NUMBER;
	BEGIN
		UPDATE T_CLI_LEVY_EXECS S
		SET    S.EXEC_END_DATE   = P_DATE_END,
					 S.ID_CLOSE_REASON = P_REASON,
					 S.END_COMMENT     = P_COMMENT,
					 S.DATE_MODIFIED   = SYSDATE,
					 S.MODIFIED_BY     = F$_USR_ID
		WHERE  S.ID_EXEC = P_ID_EXEC
		RETURNING ID_LEVY INTO L_ID_LEVY;		 
		
		SELECT L.ID_CASE INTO L_ID_CASE FROM T_CLI_LEVY L WHERE L.ID_LEVY = L_ID_LEVY;
		
		FOR i IN (SELECT MAX(EV.id_event) id_event
							FROM V_CLI_EVENTS EV WHERE EV.ID_WORK = L_ID_CASE 
							AND EV.code_event = 'EXEC_CLOSE')
		LOOP
			 L_ID_EVENT := i.id_event;
		END LOOP;	
		 
		IF L_ID_EVENT IS NOT NULL THEN
			UPDATE T_CLI_EVENTS EV SET EV.DATE_EVENT = P_DATE_END WHERE EV.ID_EVENT = L_ID_EVENT; 
		ELSE
		  PKG_EVENTS.P_CREATE_CLI_EVENT(P_ID_WORK => L_ID_CASE,P_CODE => 'EXEC_CLOSE',P_DATE => P_DATE_END);
		END IF;		 		 
	END;											 
		
END PKG_LEVY;
/

