i�?create or replace package lawmain.PKG_AUTO_CALL is

  -- Author  : EUGEN
  -- Created : 13.05.2019 9:17:34
  -- Purpose : 
  
  PROCEDURE P_CREATE_CLI_AUTO_CALL(P_CONTRACTS      wwv_flow_t_varchar2
                                  ,P_PER_START      DATE DEFAULT NULL
                                  ,P_PER_END        DATE DEFAULT NULL
																	,P_DEBT_DATE      DATE DEFAULT NULL
																	,P_ID_FOLDER       NUMBER);
	
  FUNCTION F_MONEY_TO_STRING(P_NUM NUMBER) RETURN VARCHAR2;
	
	PROCEDURE P_DELETE_CALL(P_ID_CALL VARCHAR2);
	
  FUNCTION F_CHECK_PHONE(P_PHONE VARCHAR2) RETURN VARCHAR2;
	
  PROCEDURE P_UPDATE_PHONE(P_ID_CALL VARCHAR2, P_PHONE VARCHAR2, P_UTC VARCHAR2);
	
	PROCEDURE P_CREATE_TASK(P_ID_FOLDER  NUMBER,		                      
													P_TEMPLATE   VARCHAR2);
	
	PROCEDURE P_START_CALLING(P_ID_FOLDER  NUMBER,
		                        P_DATE_START DATE,
		                        P_CALL_INTER NUMBER);
														
  PROCEDURE P_GET_RESULT(P_ID_FOLDER NUMBER);	
	
	PROCEDURE P_STOP_CALL(P_ID_FOLDER NUMBER);	
	
  PROCEDURE P_JOB_CALL_RESULT;											
	
end PKG_AUTO_CALL;
/

create or replace package body lawmain.PKG_AUTO_CALL
IS

  FUNCTION F_MONEY_TO_STRING(P_NUM NUMBER) RETURN VARCHAR2
		IS
		L_RET VARCHAR2(4000);                                                     
		L_RUB NUMBER;
		L_KOP NUMBER;
	BEGIN          
		L_RUB := TRUNC(P_NUM);
		L_KOP := NVL(MOD(P_NUM,1)*100,0);
		
		L_RET := TO_CHAR(L_RUB); 
		IF MOD(L_RUB,10) = 1 THEN 
			L_RET := L_RET||' рубля';	 
		ELSE
		  L_RET := L_RET||' рублей';
		END IF;	
		
		L_RET := L_RET||' '||TO_CHAR(L_KOP);
		
		IF MOD(L_KOP,10) = 1 THEN
			L_RET := L_RET||' копейки';	 
		ELSE
		 	L_RET := L_RET||' копеек';
		END IF;	
		
		RETURN L_RET; 
	END;
	
------------------------------------------------------------------------------------------	

  FUNCTION F_CHECK_PHONE(P_PHONE VARCHAR2) RETURN VARCHAR2
		IS
	BEGIN
		IF REGEXP_LIKE(P_PHONE,'^(7|8)9\d{9}$') OR REGEXP_LIKE(P_PHONE,'^(7|8)4112\d{6}$') THEN RETURN 'Y';
		ELSE RETURN 'N';
		END IF;  
	END;

  PROCEDURE P_CREATE_CLI_AUTO_CALL(P_CONTRACTS      wwv_flow_t_varchar2
																	,P_PER_START      DATE DEFAULT NULL
																	,P_PER_END        DATE DEFAULT NULL
																	,P_DEBT_DATE      DATE DEFAULT NULL
																	,P_ID_FOLDER       NUMBER) 
	 IS          
		L_DEBT_CREATED  NUMBER := 0;
	  L_CURRENT_DEBT  NUMBER := 0;      
		L_ID_CLIENT NUMBER;       
		L_ID_WORK   NUMBER;     
		L_PHONES    VARCHAR2(500);
		L_VALID     VARCHAR2(1);  
		L_TYPE      VARCHAR2(100);
		L_ID_CTR_CWS NUMBER;
		L_ID_CTR_SEW NUMBER;
		L_PHONE      VARCHAR2(500);
	BEGIN

   --Определяем договор
    FOR I IN 1..P_CONTRACTS.COUNT LOOP
      SELECT CT.CTR_TYPE, CT.ID_CLIENT INTO L_TYPE, L_ID_CLIENT
      FROM T_CONTRACTS CT WHERE CT.ID_CONTRACT = P_CONTRACTS(I);
      
      IF L_TYPE = 'Основной в ХВС' THEN
        L_ID_CTR_CWS := TO_NUMBER(P_CONTRACTS(I));
      ELSE
        L_ID_CTR_SEW := TO_NUMBER(P_CONTRACTS(I));
      END IF;                                  
    END LOOP;
       
		
		BEGIN
			SELECT D.DET_VAL INTO L_PHONES FROM T_CLI_CONTACT_DETAILS D WHERE D.DET_TYPE = 'Телефон' 
			AND D.DET_DESC = 'Телефон для автодозвона' AND D.ID_CLIENT = L_ID_CLIENT AND ROWNUM = 1;
		EXCEPTION
			WHEN NO_DATA_FOUND THEN
				L_PHONES := PKG_CLIENTS.F_GET_PHONE(L_ID_CLIENT);	
		END;                                                
		
		L_PHONES := REGEXP_REPLACE(L_PHONES,'[^0-9,]','');
		
		INSERT INTO T_CLI_DEBT_WORK(ID_WORK,ID_CLIENT,TYPE_WORK,DEPT,DATE_CREATED,CREATED_BY)
	 	VALUES(SEQ_CLI_DEBT_WORK.NEXTVAL,L_ID_CLIENT,'AUTO_CALL','PRE_TRIAL',SYSDATE,F$_USR_ID)
	  RETURNING ID_WORK INTO L_ID_WORK;
					
    --долги      
    FOR I IN (SELECT * FROM 
			       (SELECT DEB.ID_DOC,DEB.DEBT ,DEB.ID_CONTRACT,DEB.ID_CLIENT
                    ,(SELECT NVL(SUM(DECODE(C.OPER_TYPE, 'Приход', C.AMOUNT,-C.AMOUNT)), 0)
											FROM   T_CLI_FINDOC_CONS C WHERE  C.EXT_ID_TO = DEB.EXT_ID
											AND    C.DATE_CON <= P_DEBT_DATE) DEBT_ON_DATE
              FROM  T_CLI_FINDOCS  DEB
              WHERE DEB.ID_CONTRACT IN (SELECT TO_NUMBER(COLUMN_VALUE) FROM TABLE(P_CONTRACTS))
							AND DEB.OPER_TYPE = 'Приход'
              -- за заданный период
              AND trunc(DEB.doc_date,'MM') BETWEEN NVL(P_PER_START,DATE '2000-01-01') AND NVL(P_PER_END,DATE '3000-01-01')
              -- но не больше даты на которую хотим выпустить претензию, тут без транка
              AND (deb.doc_date <= P_DEBT_DATE OR P_DEBT_DATE IS NULL)
              AND    NOT EXISTS  (SELECT 1
																	FROM   T_CLI_DEBTS CCD
																	LEFT OUTER JOIN T_CLI_DEBT_DOCS CC ON (CCD.ID_WORK = CC.ID AND CC.IS_ACTIVE = 'Y')
																	LEFT OUTER JOIN T_CLI_REST R ON (CCD.ID_WORK = R.ID_REST AND R.CODE_STATUS = 'VALID') 
																	LEFT OUTER JOIN T_CLI_CASES S ON (S.ID_CASE = CCD.ID_WORK)
																	WHERE CCD.ID_DOC = DEB.ID_DOC
																	AND  (CC.ID IS NOT NULL OR R.ID_REST IS NOT NULL OR S.ID_CASE IS NOT NULL)
																	))T
					    WHERE T.DEBT_ON_DATE > 0)

		LOOP      
				INSERT INTO T_CLI_DEBTS(ID_DEBT,ID_WORK,ID_DOC,DATE_CREATED,CREATED_BY,ID_CONTRACT,
				                        ID_CLIENT,DEBT_CURRENT,DEBT_CREATED,DEBT_PRETRIAL,DATE_PRETRIAL,DEPT)
				VALUES (SEQ_CLI_CASE_DEBTS.NEXTVAL,L_ID_WORK,I.ID_DOC,SYSDATE,F$_USR_ID,I.ID_CONTRACT,
				        I.ID_CLIENT,I.DEBT,I.DEBT_ON_DATE,I.DEBT_ON_DATE,SYSDATE,'PRE_TRIAL');
				
				L_DEBT_CREATED := L_DEBT_CREATED + I.DEBT_ON_DATE;
				L_CURRENT_DEBT := L_CURRENT_DEBT + I.DEBT;
		END LOOP;
	
		  
		UPDATE T_CLI_DEBT_WORK W
			SET  W.DEBT_CREATED = L_DEBT_CREATED
					,W.CURRENT_DEBT = L_CURRENT_DEBT
		WHERE  W.ID_WORK = L_ID_WORK;			                              
		
		FOR J IN (SELECT COLUMN_VALUE PHONE FROM TABLE(APEX_STRING.split(L_PHONES,',')))
		LOOP 
			
			IF LENGTH(J.PHONE) = 6 THEN 
				L_PHONE := '84112'||J.PHONE;
			ELSIF LENGTH(J.PHONE) >= 10 THEN    
				L_PHONE := '8'||SUBSTR(J.PHONE,-10);
			END IF;	
			
			L_VALID := F_CHECK_PHONE(L_PHONE);
			
			INSERT INTO T_CLI_AUTO_CALL(ID_CALL,CREATED_BY,DATE_CREATED,DEBT_ON_DATE,ID_FOLDER,PHONE
			                            ,IS_VALID,ID_WORK,ID_CWS_CONTRACT,ID_SEW_CONTRACT,DEBT_CREATED
																	,CURRENT_DEBT,UTC)
			VALUES(RAWTOHEX(SYS_GUID()),F$_USR_ID,SYSDATE,P_DEBT_DATE,P_ID_FOLDER,L_PHONE
			      ,L_VALID,L_ID_WORK,L_ID_CTR_CWS,L_ID_CTR_SEW,l_debt_created, L_CURRENT_DEBT,'9');

		END LOOP;       
		
		PKG_EVENTS.P_CREATE_CLI_EVENT(L_ID_WORK,'AUTO_CALL');
	EXCEPTION
    WHEN OTHERS THEN
    	RAISE_APPLICATION_ERROR(-20200,SQLERRM);	
	END;		
	
--------------------------------------------------------------------------------------------	
	
  PROCEDURE P_UPDATE_PHONE(P_ID_CALL VARCHAR2, P_PHONE VARCHAR2, P_UTC VARCHAR2)
		IS
		L_VALID VARCHAR2(1);        
	BEGIN                         
		
		L_VALID := F_CHECK_PHONE(P_PHONE);
      		
			UPDATE T_CLI_AUTO_CALL C SET C.PHONE = P_PHONE, C.IS_VALID = L_VALID,C.UTC = P_UTC WHERE C.ID_CALL = P_ID_CALL;
												
	END; 

--------------------------------------------------------------------------------------------       
	
	PROCEDURE P_DELETE_CALL(P_ID_CALL VARCHAR2)
		IS                                                                                          
		L_ID_WORK NUMBER;         
		L_CNT NUMBER;
	BEGIN          																										
    DELETE FROM T_CLI_AUTO_CALL C WHERE C.ID_CALL = P_ID_CALL RETURNING C.ID_WORK INTO L_ID_WORK;
		SELECT COUNT(1) INTO L_CNT FROM T_CLI_AUTO_CALL C WHERE C.ID_WORK = L_ID_WORK;
		IF L_CNT = 0 THEN
			DELETE FROM T_CLI_DEBTS D WHERE D.ID_WORK = L_ID_WORK;
			DELETE FROM T_CLI_EVENTS EV WHERE EV.ID_WORK = L_ID_WORK;
			DELETE FROM T_CLI_DEBT_WORK W WHERE W.ID_WORK = L_ID_WORK;
		END IF;
	END;		

-----------------------------------------------------------------------------------------------

	PROCEDURE P_CREATE_TASK(P_ID_FOLDER  NUMBER,		                      
													P_TEMPLATE   VARCHAR2)
		IS
	  L_TASK LAWSUP.PKG_CALL_API.CALL_TASK;   
		L_RESP LAWSUP.PKG_CALL_API.REQ_RESP;
		L_CNT  NUMBER;		
		L_XML  XMLTYPE;
	BEGIN 
		
		SELECT COUNT(1) INTO L_CNT FROM T_CLI_AUTO_CALL AC WHERE AC.ID_FOLDER = P_ID_FOLDER AND AC.IS_VALID = 'N';   
		
		IF L_CNT > 0 THEN
			RAISE_APPLICATION_ERROR(-20200,'В реестре присутствуют невыверенные телефоны');
		END IF;
		
	  UPDATE T_CLI_AUTO_CALL C
			SET    C.MESSAGE = REPLACE(REPLACE(PKG_PREF.F$DESC1(P_TEMPLATE), '[сумма_долга]', F_MONEY_TO_STRING(C.DEBT_CREATED))
																 ,'[услуги]', CASE
																		 WHEN C.ID_CWS_CONTRACT IS NOT NULL AND C.ID_SEW_CONTRACT IS NOT NULL THEN
																			'водоснабжения и водоотведения'
																		 WHEN C.ID_CWS_CONTRACT IS NOT NULL THEN
																			'водоснабжения'
																		 WHEN C.ID_SEW_CONTRACT IS NOT NULL THEN
																			'водоотведения'
																	 END)
		WHERE  C.ID_FOLDER = P_ID_FOLDER;
		
		L_TASK.LOGIN     := PKG_PREF.F$C1('CALL_USER');
		L_TASK.PASSWORD  := PKG_PREF.F$C1('CALL_PWD');
		L_TASK.CAMPAIGN  := PKG_PREF.F$C1('CALL_CAMPAIGN');
		L_TASK.FILE_NAME := 'YKT_CALL_'||P_ID_FOLDER||'.xml';
		
		SELECT SUBSTR(AC.PHONE,-10),AC.ID_CALL,AC.UTC,AC.MESSAGE
		BULK COLLECT INTO L_TASK.CALLS
		FROM T_CLI_AUTO_CALL AC WHERE AC.ID_FOLDER = P_ID_FOLDER; 
		
		L_RESP := LAWSUP.PKG_CALL_API.F_CREATE_TASK(L_TASK);  
		
		L_XML := XMLTYPE(L_RESP.RESPONSE);
		
		IF L_XML.extract('/Response/Code/text()').getStringVal != 'OK' THEN
      RAISE_APPLICATION_ERROR(-20201,L_XML.extract('/Response/Message/text()').getStringVal);
    END IF;                                                                    
		
		UPDATE T_FOLDERS S SET S.ID_TASK = L_XML.extract('/Response/Message/text()')
		WHERE S.ID_FOLDER = P_ID_FOLDER;
	
	EXCEPTION 
		WHEN OTHERS THEN 
			IF SQLCODE NOT IN (-20200,-20201) THEN
				PKG_LOG.P$LOG(p_PROG_UNIT => 'PKG_AUTO_CALL',p_MESS => 'P_CREATE_TASK',P_C1 => P_ID_FOLDER
										 ,P_C2 => SQLERRM,P_C3 => SUBSTR(L_RESP.RESPONSE,1,2000),P_C4 => DBMS_UTILITY.format_error_stack);														
        RAISE_APPLICATION_ERROR(-20203,'Ошибка создания задания на обзвон');						 
			ELSE RAISE;
			END IF;					
	END;		                                                                                      
	
------------------------------------------------------------------------------------------------	

	PROCEDURE P_START_CALLING(P_ID_FOLDER  NUMBER,
		                        P_DATE_START DATE,
		                        P_CALL_INTER NUMBER)
	 IS     	 
	 	L_WORK LAWSUP.PKG_CALL_API.WORK_REQUEST;    
		L_RESP LAWSUP.PKG_CALL_API.REQ_RESP;
		L_TASK NUMBER;     
		L_XML  XMLTYPE;
	BEGIN                     
		SELECT S.ID_TASK INTO L_TASK FROM T_FOLDERS S WHERE S.ID_FOLDER = P_ID_FOLDER;
		
		L_WORK.LOGIN      := PKG_PREF.F$C1('CALL_USER');
    L_WORK.PASSWORD   := PKG_PREF.F$C1('CALL_PWD');
    L_WORK.DATE_START := P_DATE_START;          
		L_WORK.UTC        := '9';
		L_WORK.TASK_ID    := L_TASK;
		L_WORK.CALL_INTER := P_CALL_INTER;   
		
		L_RESP := LAWSUP.PKG_CALL_API.F_SEND_TO_WORK(L_WORK);
		
		L_XML := XMLTYPE(L_RESP.RESPONSE);
		
		IF L_XML.extract('/Response/Code/text()').getStringVal != 'OK' THEN
      RAISE_APPLICATION_ERROR(-20201,L_XML.extract('/Response/Message/text()').getStringVal);
    END IF;  
		
		UPDATE T_FOLDERS S SET S.DATE_CALL_START = P_DATE_START WHERE S.ID_FOLDER = P_ID_FOLDER;
		
		PKG_FOLDERS.P_CHANGE_FOLDER_STATUS(P_ID_FOLDER,'CALLING');
		PKG_FOLDERS.P_CREATE_FOLDER_EVENT(P_ID_FOLDER,'CALLING');
		
		EXCEPTION 
		WHEN OTHERS THEN 
			IF SQLCODE != -20201 THEN
				PKG_LOG.P$LOG(p_PROG_UNIT => 'PKG_AUTO_CALL',p_MESS => 'P_START_CALLING',P_C1 => P_ID_FOLDER
										 ,P_C2 => SQLERRM,P_C3 => SUBSTR(L_RESP.RESPONSE,1,2000));														
				RAISE_APPLICATION_ERROR(-20200,'Ошибка запуска обзвона по реестру');		
			ELSE RAISE;
			END IF;
	END;
	
---------------------------------------------------------------------------------------

  PROCEDURE P_GET_RESULT(P_ID_FOLDER NUMBER)
		IS        
		L_ID_TASK NUMBER;                                        
		L_RESP    LAWSUP.PKG_CALL_API.REQ_RESP;  
		L_XML     XMLTYPE;
		L_STAT    VARCHAR2(50);
		L_UTC     VARCHAR2(2);      
		L_LOCAL_UTC VARCHAR2(2) := '9';  
		L_DIFF    NUMBER;
	BEGIN              
		SELECT S.ID_TASK INTO L_ID_TASK FROM T_FOLDERS S WHERE S.ID_FOLDER = P_ID_FOLDER;
		
		L_RESP := LAWSUP.PKG_CALL_API.F_GET_RESULT(P_USER => PKG_PREF.F$C1('CALL_USER'),
                                               P_PASS => PKG_PREF.F$C1('CALL_PWD'),
                                               P_TASK_ID => L_ID_TASK);
		
		L_XML := XMLTYPE(L_RESP.RESPONSE);
		
		IF L_XML.extract('/Response/Code/text()').getStringVal != 'OK' THEN
      RAISE_APPLICATION_ERROR(-20201,L_XML.extract('/Response/Message/text()').getStringVal);
    END IF;  																					 
		
		L_STAT := L_XML.extract('/Response/LDR/@Status').getStringVal;
		L_UTC  := L_XML.extract('/Response/LDR/@UTC').getStringVal;
		L_DIFF := TO_NUMBER(L_LOCAL_UTC) - TO_NUMBER(L_UTC);                              
		
		
		--Если обзвон не запущен то и нечего парсить
		IF L_STAT IN ('UPLOAD','CANCEL') THEN
			 RETURN;
		END IF;   

		
		FOR I IN (SELECT X.ID_CALL, X.CALL_STATUS, 
										 TO_NUMBER(X.CALL_CNT) CALL_CNT, 
										 TO_DATE(X.LAST_CALL,'YYYY-MM-DD"T"HH24:MI:SS') + L_DIFF/24 LAST_CALL
							FROM XMLTABLE('/Response/LDR/Call'
														PASSING L_XML
														COLUMNS ID_CALL     VARCHAR2(500) PATH './@UUID',
																		CALL_STATUS VARCHAR2(500) PATH './@CodeResult',
																		CALL_CNT   VARCHAR2(500) PATH './@CallsCount',
																		LAST_CALL   VARCHAR2(500) PATH './@LastCallDate') X)
		LOOP
			 UPDATE T_CLI_AUTO_CALL C 
			    SET C.CALL_STATUS = I.CALL_STATUS,
					    C.DATE_CALL   = I.LAST_CALL,
							C.CALL_CNT    = I.CALL_CNT
			 WHERE C.ID_CALL = I.ID_CALL;
		END LOOP;      
		
		IF L_STAT = 'DONE' THEN   
			UPDATE T_FOLDERS S 
			   SET S.DATE_CALL_END = (SELECT MAX(C.DATE_CALL) FROM T_CLI_AUTO_CALL C WHERE C.ID_FOLDER = S.ID_FOLDER) 
		 	 WHERE S.ID_FOLDER = P_ID_FOLDER;    
			 
			PKG_FOLDERS.P_CHANGE_FOLDER_STATUS(P_ID_FOLDER,'CALL_DONE');
			PKG_FOLDERS.P_CREATE_FOLDER_EVENT(P_ID_FOLDER,'CALL_DONE');
		END IF;
																									 
  EXCEPTION 
		WHEN OTHERS THEN    
			IF SQLCODE != -20201 THEN
			PKG_LOG.P$LOG(p_PROG_UNIT => 'PKG_AUTO_CALL',p_MESS => 'P_GET_RESULT',P_C1 => P_ID_FOLDER
			             ,P_C2 => SQLERRM,P_C3 => SUBSTR(L_RESP.RESPONSE,1,2000));														
			RAISE_APPLICATION_ERROR(-20200,'Ошибка получения результатов автообзвона');	                                
			ELSE RAISE;
			END IF; 
	END;		
	
------------------------------------------------------------------------------------------------

  PROCEDURE P_STOP_CALL(P_ID_FOLDER NUMBER)
		IS
		L_ID_TASK NUMBER;                                        
		L_RESP    LAWSUP.PKG_CALL_API.REQ_RESP;  
		L_XML     XMLTYPE;		
	BEGIN              
		SELECT S.ID_TASK INTO L_ID_TASK FROM T_FOLDERS S WHERE S.ID_FOLDER = P_ID_FOLDER;
		
		L_RESP := LAWSUP.PKG_CALL_API.F_STOP_CALL(P_USER => PKG_PREF.F$C1('CALL_USER'),
                                               P_PASS => PKG_PREF.F$C1('CALL_PWD'),
                                               P_TASK_ID => L_ID_TASK);
		
		L_XML := XMLTYPE(L_RESP.RESPONSE);
		
		IF L_XML.extract('/Response/Code/text()').getStringVal != 'OK' THEN
      RAISE_APPLICATION_ERROR(-20201,L_XML.extract('/Response/Message/text()').getStringVal);
    END IF;
		
		PKG_FOLDERS.P_CHANGE_FOLDER_STATUS(P_ID_FOLDER,'CALL_STOP');
		PKG_FOLDERS.P_CREATE_FOLDER_EVENT(P_ID_FOLDER,'CALL_STOP');
		  	
	EXCEPTION 
		WHEN OTHERS THEN  
			IF SQLCODE != -20201 THEN			
				PKG_LOG.P$LOG(p_PROG_UNIT => 'PKG_AUTO_CALL'
				             ,p_MESS => 'P_STOP_CALL'
										 ,P_C1 => P_ID_FOLDER
										 ,P_C2 => SQLERRM
										 ,P_C3 => SUBSTR(L_RESP.RESPONSE,1,2000));														
				RAISE_APPLICATION_ERROR(-20200,'Ошибка остановки обзвона');
			ELSE 
		    RAISE;
			END IF;																								
	END;					
	
------------------------------------------------------------------------

  PROCEDURE P_JOB_CALL_RESULT					
		IS
	BEGIN
		FOR I IN (SELECT S.ID_FOLDER FROM V_FOLDERS S 
			        WHERE S.TYPE_CODE = 'AUTO_CALL' 
							AND S.CODE_STATUS = 'CALLING')
		LOOP
			PKG_AUTO_CALL.P_GET_RESULT(P_ID_FOLDER => I.ID_FOLDER);
			COMMIT;
		END LOOP;					
	END;			
	
end PKG_AUTO_CALL;
/

