i�?CREATE OR REPLACE PACKAGE LAWMAIN.PKG_PDF_003
  IS
 -- Author  : EUGEN
 -- Created : 13.12.2016 
 -- Purpose : Реестр почтовой корреспонденции (простой)
 -- Code    : MAIL_REGISTRY



  FUNCTION RUN_REPORT(P_PAR_01 VARCHAR2 DEFAULT NULL
											,P_PAR_02 VARCHAR2 DEFAULT NULL
											,P_PAR_03 VARCHAR2 DEFAULT NULL
											,P_PAR_04 VARCHAR2 DEFAULT NULL
											,P_PAR_05 VARCHAR2 DEFAULT NULL
											,P_PAR_06 VARCHAR2 DEFAULT NULL
											,P_PAR_07 VARCHAR2 DEFAULT NULL
											,P_PAR_08 VARCHAR2 DEFAULT NULL
											,P_PAR_09 VARCHAR2 DEFAULT NULL
											,P_PAR_10 VARCHAR2 DEFAULT NULL) RETURN LAWSUP.PKG_FILES.REC_DOC;

END PKG_PDF_003;
/

CREATE OR REPLACE PACKAGE BODY LAWMAIN.PKG_PDF_003
    IS

  ----------------------------------------------------------------------
  FUNCTION AD(P1 IN VARCHAR2,P2 IN VARCHAR2) RETURN VARCHAR2
    IS
  BEGIN
    IF P2 IS NOT NULL THEN
      RETURN p1;
    END IF;
    RETURN p2;
  END;

 
	FUNCTION F_GET_PDF(P_ID_FOLDER NUMBER, P_FROM NUMBER, P_TO NUMBER, P_OPTION VARCHAR2, P_NUM VARCHAR2) RETURN BLOB
	IS
		l_blob        blob;
    L_RET         LAWSUP.PKG_FILES.REC_DOC;
		l_ttf_t    		Plpdf_Type.t_addfont;
		l_ttf_tbd    	Plpdf_Type.t_addfont;   
		l_ttf_ti    		Plpdf_Type.t_addfont;
		l_ttf_tibd    	Plpdf_Type.t_addfont;
		l_ttf_barcode Plpdf_Type.t_addfont;


		l$_t        	VARCHAR2(40)	:= PLPDF_FONTS.l$_tw;
		l$_t_id     	NUMBER      	:= PLPDF_FONTS.l$_tw_id;
		l$_tbd        VARCHAR2(40) 	:= PLPDF_FONTS.l$_twbd;
		l$_tbd_id     NUMBER       	:= PLPDF_FONTS.l$_twbd_id;

	

		l_pdf_1 blob;
		v_tpl_1   plpdf_type.tr_tpl_data;
		l_tpl_id_1 number;

		C_BORDER NUMBER := 0;
	  L_W NUMBER;
		L_H NUMBER;
    
		l_data   Plpdf_Type.t_row_datas;
    l_width  Plpdf_Type.t_row_widths;
    l_align  Plpdf_Type.t_row_aligns;
		
		l_cnt   NUMBER := 0;
		
  BEGIN
    IF P_OPTION = 'TEMPLATE' THEN
		  NULL;
	  ELSE
			Plpdf.init;
			Plpdf.setEncoding(p_enc => 'CP1251');
			l_ttf_t       :=  Plpdf_Ttf.GetTTF(l$_t_id);
			l_ttf_tbd     :=  Plpdf_Ttf.GetTTF(l$_tbd_id);
			plpdf.addTTF(p_family => l$_t,p_data => l_ttf_t);
			plpdf.addTTF(p_family => l$_tbd,p_data => l_ttf_tbd);  

						PLPDF.NewPage;   
		END IF;
			 
         
			
			L_W := PLPDF.getPageWidth;
			PLPDF.setAllMargin(p_left => 20,p_top => 10,p_right => 10);
			
			PLPDF.setPrintFont(l$_t,null,8);
			PLPDF.PrintCell(p_w => L_W-30,p_h => 4,p_txt => 'Приложение №3 к Контракту на оказание услуг почтовой связи № 23-18-272 от 14.03.2018'
			               ,p_border => C_BORDER,p_align => 'R',p_clipping => FALSE,p_ln => 1);
			
			PLPDF.LineBreak(4);
			PLPDF.setPrintFont(l$_tbd,null,12);

			PLPDF.PrintCell(p_w => L_W-30,p_h => 5,p_txt => 'Список №'||P_NUM
			               ,p_border => C_BORDER,p_align => 'C',p_clipping => FALSE,p_ln => 1);
			PLPDF.PrintCell(p_w => L_W-30,p_h => 5,p_txt => 'На отправку простой корреспонденции'
			               ,p_border => C_BORDER,p_align => 'C',p_clipping => FALSE,p_ln => 2);                            
										 
  		PLPDF.PrintCell(p_w => (L_W-30)/3,p_h => 5,p_border => C_BORDER,p_align => 'C',p_clipping => FALSE,p_ln => 0);							 							 							 
			PLPDF.PrintCell(p_w => (L_W-30)/3,p_h => 5,p_txt => PKG_PREF.F$C2('REQUISITES'),p_border => C_BORDER,p_align => 'C',p_clipping => FALSE,p_ln => 0);							 
			PLPDF.PrintCell(p_w => (L_W-30)/3,p_h => 5,p_txt => 'ГСП № 1152' ,p_border => C_BORDER,p_align => 'R',p_clipping => FALSE,p_ln => 1);							 							     
			
			PLPDF.PrintCell(p_w => L_W-30,p_h => 5,p_txt => 'от ___'||LOWER(pkg_utils.F_GET_NAME_MONTHS(SYSDATE,'R'))||' '||to_char(SYSDATE,'YYYY')||' г.'
			               ,p_border => C_BORDER,p_align => 'C',p_clipping => FALSE,p_ln => 0);							 							 										 
			
			plpdf.LineBreak(10);

			PLPDF.setPrintFont(l$_tbd,null,10);			
			l_data(1) := '';
			l_data(2) := 'Куда';
			l_data(3) := 'Кому';
			l_data(4) := 'Вид';
			l_data(5) := 'Вес';
			l_data(6) := 'Плата за пересылку';
			l_data(7) := '№ отправления';
			
			l_align(1) := 'C';
			l_align(2) := 'C';
			l_align(3) := 'C';
			l_align(4) := 'L';
			l_align(5) := 'L';
			l_align(6) := 'L';
			l_align(7) := 'L';
			
			l_width(1) :=  8;
			l_width(2) :=  70;
  		l_width(3) :=  52;
  		l_width(4) :=  9;
  		l_width(5) :=  8;
  		l_width(6) :=  18;
  		l_width(7) :=  15;
			
			
			plpdf_row_print.Row_Print(p_data => l_data,p_width => l_width,p_align => l_align,p_h => 3.8);
						
			PLPDF.setPrintFont(l$_t,null,10);			
			
			l_align(1) := 'R';
			l_align(2) := 'L';
			l_align(3) := 'L';
			l_data(4) := '';
			l_data(5) := '';
			l_data(6) := '';
			l_data(7) := '';			
		FOR I IN (SELECT * FROM (
			        SELECT ROWNUM RN, T.* FROM(
			        SELECT S.CLI_NAME, AD_T.DET_VAL
              FROM V_CLI_PRETENSION S
              INNER JOIN T_FOLDER_CLI_CASES FCC ON (S.ID_WORK = FCC.ID_CASE AND FCC.ID_FOLDER = P_ID_FOLDER)
              LEFT OUTER JOIN (SELECT CD.DET_VAL, cd.id_client
                                     ,row_number() over (PARTITION BY cd.id_client order BY t.order_num ) as rn
                                 FROM T_CLI_CONTACT_DETAILS CD
                                     ,T_CLI_CONTACT_TYPES T    
                                WHERE T.DET_TYPE = 'Адрес'
                                  AND CD.DET_DESC = T.DET_DESC ) ad_t    
              ON (ad_t.id_client = s.ID_CLIENT AND ad_t.rn = 1)							              
							ORDER BY S.DEBT_PRETENSION ASC ) T)T1
							WHERE T1.RN BETWEEN P_FROM AND P_TO)										
		LOOP                      
      l_cnt := l_cnt+1;
			l_data(1):= l_cnt;
			l_data(2):= i.det_val;
			l_data(3):= i.cli_name;	
 			plpdf_row_print.Row_Print(p_data => l_data,p_width => l_width,p_align => l_align,p_h => 4);
		END LOOP;				  
		
		PLPDF.LineBreak(7);
  	PLPDF.setPrintFont(l$_tBD,null,11);				
    PLPDF.PrintCell(p_w => L_W,p_h => 5,p_txt => 'Всего '||l_cnt||' шт.',p_border => C_BORDER,p_align => 'L',p_clipping => FALSE,p_ln => 1);							 
    PLPDF.PrintCell(p_w => L_W,p_h => 5,p_txt => 'Исполнитель: '||INITCAP(PKG_USERS.F_GET_FIO_BY_ID(F$_USR_ID)),p_border => C_BORDER,p_align => 'L',p_clipping => FALSE,p_ln => 1);							 
		
	  IF P_OPTION = 'TEMPLATE' THEN
		  NULL;
	  ELSE
  		plpdf.SendDoc (p_blob => l_blob);      
		END IF;	
	 
		RETURN l_blob;

	END F_GET_PDF;

  ----------------------------------------------------------------------
  FUNCTION RUN_REPORT(P_PAR_01 VARCHAR2 DEFAULT NULL
											,P_PAR_02 VARCHAR2 DEFAULT NULL
											,P_PAR_03 VARCHAR2 DEFAULT NULL
											,P_PAR_04 VARCHAR2 DEFAULT NULL
											,P_PAR_05 VARCHAR2 DEFAULT NULL
											,P_PAR_06 VARCHAR2 DEFAULT NULL
											,P_PAR_07 VARCHAR2 DEFAULT NULL
											,P_PAR_08 VARCHAR2 DEFAULT NULL
											,P_PAR_09 VARCHAR2 DEFAULT NULL
											,P_PAR_10 VARCHAR2 DEFAULT NULL) RETURN LAWSUP.PKG_FILES.REC_DOC

      IS
        		PROGRAM_UNIT VARCHAR2(200) DEFAULT 'pkg_pdf_003.RUN_REPORT';
            L_BLOB LAWSUP.PKG_FILES.REC_DOC;
            s_filename VARCHAR2(200) := 'notification.pdf';
    BEGIN
        
				DBMS_LOB.createtemporary(lob_loc => L_BLOB.P_BLOB,cache => TRUE);
        L_BLOB.P_FILE_NAME := s_filename;
				L_BLOB.P_BLOB := F_GET_PDF(P_PAR_01,P_PAR_02,P_PAR_03,P_PAR_04,P_PAR_05);
        
				l_BLOB.P_FILE_NAME := 'POCHTOVIY_REESTR'||P_PAR_01;
				RETURN L_BLOB;
   EXCEPTION 
		 WHEN OTHERS THEN
			  RAISE_APPLICATION_ERROR(-20200,DBMS_UTILITY.format_error_backtrace);
				
  END RUN_REPORT;

END PKG_PDF_003;
/

