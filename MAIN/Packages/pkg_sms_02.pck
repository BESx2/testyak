i�?create or replace package lawmain.PKG_SMS_02 is

  -- Author  : EUGEN
  -- Created : 12.01.2017 10:44:26
  -- Purpose : пакет для отправки СМС юридическому лицу сотрудником судебного отдела
  
  PROCEDURE P_SEND_SMS(P_ID_CASE NUMBER,
		                   P_PHONE VARCHAR2 DEFAULT NULL);

end PKG_SMS_02;
/

create or replace package body lawmain.PKG_SMS_02 is

	PROCEDURE P_SEND_SMS(P_ID_CASE NUMBER,
		                   P_PHONE VARCHAR2 DEFAULT NULL)
		 IS
		G$SMS_TEXT VARCHAR2(2000);
		L_PHONE    VARCHAR2(12);                                     
		L_SMS_ID   NUMBER;
	BEGIN                       
		 G$SMS_TEXT := PKG_PREF.F$DESC1('COURT_SMS_NOTIF');  
		 
		FOR i IN (SELECT CT.CTR_NUMBER,
					               (SELECT MAX(EV.DATE_EVENT) 
												  FROM V_CLI_EVENTS EV 
												  WHERE EV.id_cli_case = S.ID_CASE 
												  AND EV.code_event = 'IN_COURT') DATE_EVENT													
					        FROM T_CLI_CASES S,
									     T_CONTRACTS CT
					        WHERE S.ID_CASE = P_ID_CASE
									AND   CT.ID_CONTRACT = S.ID_CONTRACT)
            LOOP                      
						  IF I.DATE_EVENT IS NULL THEN
								RAISE_APPLICATION_ERROR(-20200,'Нельзя отправить сообщению по делу, по которому не подавалось исковое заявление');
							END IF;                                           
							
							G$SMS_TEXT := REPLACE(G$SMS_TEXT,'#CTRNUMBER#',I.Ctr_Number); 
							G$SMS_TEXT := REPLACE(G$SMS_TEXT,'#COURTDATE#',TO_CHAR(I.DATE_EVENT,'DD.MM.YYYY')); -- текущая дата.
            END LOOP;            
		
		IF P_PHONE IS NULL THEN
			 RAISE NO_DATA_FOUND;
		END IF;
		--Отсылка СМС
		L_SMS_ID := LAWSUP.PKG_SMS.F_PUT_MESSAGE(P_PHONE => P_PHONE,P_MESSAGE => G$SMS_TEXT);	
		--Событие и привязка к делу
		--PKG_EVENTS.P_CREATE_CLI_EVENT(P_CASE_ID => P_ID_CASE,P_CODE => 'INFORM_SMS_LEG');
		
		PKG_SEND_SMS.P_INSERT_CLI_CASE_SMS(P_ID_CASE,L_SMS_ID,'COURT_SMS_NOTIF');
	
	EXCEPTION 
		 WHEN NO_DATA_FOUND THEN
			  PKG_LOG.P$LOG(p_PROG_UNIT => 'PKG_SMS_01',p_MESS => 'Не найден телефон для отправки'
										 ,P_C1 => 'ID_CASE: '||P_ID_CASE,P_d1 => SYSDATE);   
				 RAISE_APPLICATION_ERROR(-20200,'Не найден телефон для отправки'); 
	END;
  
end PKG_SMS_02;
/

