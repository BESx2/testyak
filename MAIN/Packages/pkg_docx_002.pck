i�?create or replace package lawmain.PKG_DOCX_002 is

  -- Author  : EUGEN
  -- Created : 25.02.2020 15:02:04
  -- Purpose : Соглашение реструктуризации 	                                
	-- CODE    : RESTRUCT
  
  FUNCTION RUN_REPORT(P_PAR_01  IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_02 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_03 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_04 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_05 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_06 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_07 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_08 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_09 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_10 IN VARCHAR2 DEFAULT NULL) RETURN LAWSUP.PKG_FILES.REC_DOC;		                           

end PKG_DOCX_002;
/

create or replace package body lawmain.PKG_DOCX_002 is

  FUNCTION RUN_REPORT(P_PAR_01  IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_02 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_03 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_04 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_05 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_06 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_07 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_08 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_09 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_10 IN VARCHAR2 DEFAULT NULL) RETURN LAWSUP.PKG_FILES.REC_DOC
   IS  
   L_XML XMLTYPE;          
	 L_TAB XMLTYPE;
   L_SQL SYS_REFCURSOR;     
	 L_GROUP VARCHAR2(50);
   L_REP LAWSUP.PKG_FILES.REC_DOC;    
	 L_PENY_CALC VARCHAR2(1);   
	 L_ID_CWS NUMBER;
	 L_ID_SEW NUMBER;
  BEGIN         
    OPEN L_SQL FOR 
     SELECT  T.DATE_AGREE "дата_соглашения1"
						 ,PKG_PREF.F$C3('ORG_MAIN_FIO') "директор"
						 ,PKG_PREF.F$C2('ORG_MAIN_FIO') "директор_кр"
						 ,(SELECT R.POSITION FROM T_CLI_REPRES R WHERE R.ID_REPRES = T.ID_REP) "долж_абон"
             ,(SELECT LAWSUP.PKG_MORFER.F_GET_DECLENSION(INITCAP(R.FIO),'Gen','S') FROM T_CLI_REPRES R WHERE R.ID_REPRES = T.ID_REP) "предст_абон" 
						 ,(SELECT INITCAP(R.FIO) FROM T_CLI_REPRES R WHERE R.ID_REPRES = T.ID_REP) "предст_абон_им"
             ,INITCAP(PKG_CLIENTS_UTL.F_GET_REPRES_FIO_REVERSE(T.ID_REP)) "предст_абон_кр"
						 ,T.CLI_ALT_NAME	"абонент"
						 ,T.REST_DEBT + NVL(T.PENY_SUM,0) "общая_сумма_долга"  
						 ,T.PER_REST "период"  
						 ,T.PENY_DATE "дата_пени"
						 ,T.CONTRACTS "договоры" 
						 ,NVL2(T.ID_CWS_CONTRACT,'true','false') "если_водоснабжение"
						 ,T.DEBT_CWS "итого_долг_хвс"
					   ,NVL2(T.ID_SEW_CONTRACT,'true','false') "если_водоотведение" 
						 ,T.DEBT_SEW "итого_долг_во"
						 ,T.PENY_SUM "общая_сумма_пеней"
						 ,T.PENY_CWS "итого_пени_хвс"
						 ,T.PENY_SEW "итого_пени_во"
						 ,T.REST_START "дата_соглашения"
						 ,T.REST_DEBT "итого_долг_итог"
						 ,T.PENY_SUM  "итого_пени_итог"   
						 ,T.TOT_SUM "итого_сумма_итог"     
						 ,T.DEBT_CWS + DECODE(T.PENY_CALC,'Y',T.PENY_CWS,0) "итого_сумма_хвс"
						 ,T.DEBT_SEW + DECODE(T.PENY_CALC,'Y',T.PENY_SEW,0) "итого_сумма_во"						 
						 ,DECODE(T.PENY_CALC,'Y','false','true') "если_без_пени"
						 ,PKG_PREF.F$C1('ORG_LEG_ADDRESS') "адрес_орг"
						 ,PKG_PREF.F$C1('ORG_KPP') "кпп_орг"
						 ,PKG_PREF.F$C1('ORG_INN') "инн_орг"
						 ,PKG_PREF.F$C1('ORG_BANK') "банк_орг"
						 ,PKG_PREF.F$C2('ORG_BANK') "рс_орг"
						 ,PKG_PREF.F$C3('ORG_BANK') "кс_орг"
						 ,PKG_PREF.F$C4('ORG_BANK') "бик_орг"
					   ,PKG_PREF.F$C2('ORG_MAIN_FIO') "директор_кр" 
						 ,PKG_CLIENTS.F_GET_ADDRESS(T.ID_CLIENT) "адрес_абон"
						 ,T.KPP  "кпп_абон"
						 ,T.INN "инн_абон"
						 ,BNK.ACC_NUMBER "рс_абон"
						 ,BNK.COR_ACC  "кс_абон"
						 ,BNK.BANK_NAME "банк_абон"
						 ,BNK.BIK "бик_абон"
						 ,TO_CHAR(SYSDATE,'DD.MM.YYYY') "текущая_дата"
						 ,INITCAP(PKG_USERS.F_GET_FIO_BY_ID(F$_USR_ID,'Y')) "ФИО"
						 ,(SELECT L.PHONE FROM T_USER_LIST L WHERE L.ID = F$_USR_ID) "телефон"	
						 	--таблицы-----------
							,CASE WHEN T.ID_CWS_CONTRACT IS NOT NULL AND T.ID_SEW_CONTRACT IS NOT NULL AND T.PENY_CALC = 'Y' THEN 'true'  ELSE 'false' END "если_хвс_во_пени"
							,CASE WHEN T.ID_CWS_CONTRACT IS NOT NULL AND T.ID_SEW_CONTRACT IS NULL AND T.PENY_CALC = 'Y' THEN 'true' ELSE 'false' END "если_хвс_пени"
							,CASE WHEN T.ID_CWS_CONTRACT IS NULL AND T.ID_SEW_CONTRACT IS NOT NULL AND T.PENY_CALC = 'Y' THEN 'true' ELSE 'false' END "если_во_пени"											
							,CASE WHEN T.ID_CWS_CONTRACT IS NOT NULL AND T.ID_SEW_CONTRACT IS NOT NULL AND T.PENY_CALC = 'N' THEN  'true' ELSE 'false' END "если_хвс_во"
							,CASE WHEN T.ID_CWS_CONTRACT IS NOT NULL AND T.ID_SEW_CONTRACT IS NULL AND T.PENY_CALC = 'N' THEN 'true' ELSE 'false' END "если_хвс"	      
							,CASE WHEN T.ID_CWS_CONTRACT IS NULL AND T.ID_SEW_CONTRACT IS NOT NULL AND T.PENY_CALC = 'N' THEN 'true' ELSE 'false' END "если_во"				
						 ------------------------
						  ,LAWSUP.PKG_FMT_RU.number_in_words(FLOOR(T.TOT_SUM))||' рублей '||TO_CHAR(MOD(ROUND(T.TOT_SUM,2),1)*100)||' коп.' "общая_сумма_прописью"
							,T.REST_START||' - '||T.REST_END "срок_соглашения"							  										 
		FROM (SELECT CLI.CLI_ALT_NAME
								,CLI.OGRN
								,CLI.INN
								,CLI.KPP  
								,CLI.ID_CLIENT
								,R.PENY_SUM
								,R.REST_DEBT       
								,to_char(r.peny_date,'dd.mm.yyyy') PENY_DATE
								,TO_CHAR(R.REST_END,'DD.MM.YYYY') REST_END
								,R.ID_CWS_CONTRACT
								,R.ID_SEW_CONTRACT
								,R.PENY_CALC
								,R.REST_DEBT + DECODE(R.PENY_CALC,'Y',R.PENY_SUM,0) TOT_SUM
								,TO_CHAR(R.REST_START,'DD')||' '||
								 LOWER(PKG_UTILS.F_GET_NAME_MONTHS(R.REST_START,'R'))||' '||
								 TO_CHAR(R.REST_START,'YYYY')||'г.' DATE_AGREE
								,TO_CHAR(R.REST_START,'DD.MM.YYYY') REST_START  
								,(SELECT LISTAGG('№ '||CT1.CTR_NUMBER||' от '||to_char(ct1.ctr_date,'dd.mm.yyyy'),' и ')
												WITHIN GROUP (ORDER BY CT1.CTR_TYPE DESC)
									FROM 			
								 (SELECT DISTINCT CT.CTR_NUMBER, CT.CTR_DATE, CT.CTR_TYPE				
								 FROM T_CLI_DEBTS D, T_CONTRACTS CT
								 WHERE D.ID_WORK = R.ID_REST AND CT.ID_CONTRACT = D.ID_CONTRACT) CT1) CONTRACTS
							 ,(SELECT MAX(R.ID_REPRES) FROM T_CLI_REPRES R 
								 WHERE LOWER(R.ROLE) IN ('директор','руководитель','генеральный директор')
								 AND R.ID_CLIENT = CLI.ID_CLIENT) ID_REP
							 ,(SELECT MAX(A.ID_ACC) FROM T_SETL_ACCOUNTS A WHERE A.ID_CLIENT = CLI.ID_CLIENT) ID_ACC	
							 ,PKG_CLI_CASES_UTL.F_GET_DEBT_PERIOD(R.ID_REST,R.ID_CWS_CONTRACT) PER_CWS
							 ,PKG_CLI_CASES_UTL.F_GET_DEBT_PERIOD(R.ID_REST,R.ID_SEW_CONTRACT) PER_SEW	
							 ,PKG_CLI_CASES_UTL.F_GET_DEBT_PERIOD(R.ID_REST) PER_REST
							 ,(SELECT SUM(D.DEBT_CREATED) FROM T_CLI_DEBTS D WHERE D.ID_WORK = R.ID_REST AND D.ID_CONTRACT = R.ID_CWS_CONTRACT) DEBT_CWS
							 ,(SELECT SUM(D.PENY) FROM T_CLI_DEBTS D WHERE D.ID_WORK = R.ID_REST AND D.ID_CONTRACT = R.ID_CWS_CONTRACT) PENY_CWS
							 ,(SELECT SUM(D.DEBT_CREATED) FROM T_CLI_DEBTS D WHERE D.ID_WORK = R.ID_REST AND D.ID_CONTRACT = R.ID_SEW_CONTRACT) DEBT_SEW
							 ,(SELECT SUM(D.PENY) FROM T_CLI_DEBTS D WHERE D.ID_WORK = R.ID_REST AND D.ID_CONTRACT = R.ID_SEW_CONTRACT) PENY_SEW								 				 
				  FROM T_CLI_REST R, 
					     T_CLI_DEBTS D,      
							 T_CLIENTS CLI	
				  WHERE R.ID_REST = TO_NUMBER(P_PAR_01)
					AND   R.ID_REST = D.ID_WORK
				  AND   D.ID_CLIENT = CLI.ID_CLIENT) T,
				 (SELECT A.ACC_NUMBER, B.COR_ACC, B.BANK_NAME, B.BIK, A.ID_ACC
				  FROM T_SETL_ACCOUNTS A, T_BANKS B 
					WHERE A.ID_BANK = B.ID_BANK) BNK
		WHERE T.ID_ACC = BNK.ID_ACC(+);
		
		SELECT R.PENY_CALC, R.ID_CWS_CONTRACT, R.ID_SEW_CONTRACT 
		INTO L_PENY_CALC,L_ID_CWS,L_ID_SEW 
		FROM T_CLI_REST R WHERE R.ID_REST = TO_NUMBER(P_PAR_01);
		
		SELECT XMLELEMENT("rows", 
						XMLAGG(XMLELEMENT("row", 
							XMLELEMENT("cell", XMLATTRIBUTES('пункт' AS "tag"), ROWNUM), 
							XMLELEMENT("cell", XMLATTRIBUTES('дата_платежа' AS "tag"), 'до '||TO_CHAR(T.PAY_DATE, 'dd.mm.yyyy') || 'г.'),
							XMLELEMENT("cell", XMLATTRIBUTES('долг_хвс_таб' AS "tag"), TO_CHAR(T.CWS_PAY, 'FM999G999G999G999G999G990D00')), 							
							XMLELEMENT("cell", XMLATTRIBUTES('пени_хвс_таб' AS "tag"), TO_CHAR(T.CWS_PENY, 'FM999G999G999G999G999G990D00')),
							XMLELEMENT("cell", XMLATTRIBUTES('сумма_хвс_таб' AS "tag"), TO_CHAR(DECODE(L_PENY_CALC,'Y',T.CWS_PENY,0)+T.CWS_PAY, 'FM999G999G999G999G999G990D00')),							
							XMLELEMENT("cell", XMLATTRIBUTES('долг_во_таб' AS "tag"), TO_CHAR(T.SEW_PAY, 'FM999G999G999G999G999G990D00')), 
							XMLELEMENT("cell", XMLATTRIBUTES('пени_во_таб' AS "tag"), TO_CHAR(T.SEW_PENY, 'FM999G999G999G999G999G990D00')), 
							XMLELEMENT("cell", XMLATTRIBUTES('сумма_во_таб' AS "tag"), TO_CHAR(DECODE(L_PENY_CALC,'Y',T.SEW_PENY,0)+T.SEW_PAY, 'FM999G999G999G999G999G990D00')),
							XMLELEMENT("cell", XMLATTRIBUTES('итого_долг_таб' AS "tag"), TO_CHAR(T.CWS_PAY+T.SEW_PAY, 'FM999G999G999G999G999G990D00')),	                                                                  
							XMLELEMENT("cell", XMLATTRIBUTES('итого_пени_таб' AS "tag"), TO_CHAR(T.CWS_PENY+T.SEW_PENY, 'FM999G999G999G999G999G990D00')),							
							XMLELEMENT("cell", XMLATTRIBUTES('итого_сумма_таб' AS "tag"), TO_CHAR(T.TOTAL_SUM, 'FM999G999G999G999G999G990D00'))
							)))
		INTO   L_TAB
		FROM (SELECT COALESCE(T1.PAY_DATE, T2.PAY_DATE) PAY_DATE
								,T1.PAY_SUM CWS_PAY
								,T1.PENY_SUM CWS_PENY
								,T2.PAY_SUM SEW_PAY
								,T2.PENY_SUM SEW_PENY
								, NVL(T1.PAY_SUM, 0) + NVL(T2.PAY_SUM, 0) 
									+ CASE 
											WHEN L_PENY_CALC = 'Y' THEN NVL(T2.PENY_SUM, 0) + NVL(T1.PENY_SUM, 0)
										ELSE 0
										END TOTAL_SUM
					FROM   (SELECT RP.PAY_DATE,RP.PAY_SUM,RP.PENY_SUM
									FROM   T_CLI_REST_PAYMENTS RP, T_CLI_REST R
									WHERE  R.ID_REST = TO_NUMBER(P_PAR_01)
									AND    RP.ID_REST = R.ID_REST
									AND    RP.ID_CONTRACT = R.ID_CWS_CONTRACT) T1
					FULL   OUTER JOIN 
								 (SELECT RP.PAY_DATE,RP.PAY_SUM,RP.PENY_SUM
									FROM   T_CLI_REST_PAYMENTS RP, T_CLI_REST R
									WHERE  R.ID_REST = TO_NUMBER(P_PAR_01)
									AND    RP.ID_REST = R.ID_REST
									AND    RP.ID_CONTRACT = R.ID_SEW_CONTRACT) T2
					ON     T1.PAY_DATE = T2.PAY_DATE
					ORDER BY COALESCE(T1.PAY_DATE, T2.PAY_DATE)) T;			
		
		
		IF L_PENY_CALC = 'Y' THEN 
			IF L_ID_CWS IS NOT NULL AND L_ID_SEW IS NOT NULL THEN       
				 L_TAB := XMLTYPE('<tables><table name="таблица1"></table></tables>').appendChildXML('/tables/table',l_tab,NULL);
			ELSIF L_ID_CWS IS NOT NULL AND L_ID_SEW IS NULL THEN    																																	 
				 L_TAB := L_TAB.deleteXML('rows/row/cell[@tag="долг_таб_во" or @tag="пени_таб_во" or @tag="сумма_во_таб"]',NULL);	
 				 L_TAB := L_TAB.deleteXML('rows/row/cell[@tag="итого_долг_таб" or @tag="итого_пени_таб" or @tag="итого_сумма_таб"]',NULL);
 				 L_TAB := XMLTYPE('<tables><table name="таблица2"></table></tables>').appendChildXML('/tables/table',l_tab,NULL);
			ELSIF L_ID_CWS IS NULL AND L_ID_SEW IS NOT NULL THEN	                                                             
				 L_TAB := L_TAB.deleteXML('rows/row/cell[@tag="долг_таб_хвс" or @tag="пени_таб_хвс" or @tag="сумма_хвс_таб"]',NULL);	
 				 L_TAB := L_TAB.deleteXML('rows/row/cell[@tag="итого_долг_таб" or @tag="итого_пени_таб" or @tag="итого_сумма_таб"]',NULL);
 				 L_TAB := XMLTYPE('<tables><table name="таблица3"></table></tables>').appendChildXML('/tables/table',l_tab,NULL);
			END IF;
		ELSE
			L_TAB := L_TAB.deleteXML('rows/row/cell[@tag="пени_таб_хвс" or @tag="пени_таб_во" or @tag="итого_пени_таб"]',NULL);
			IF L_ID_CWS IS NOT NULL AND L_ID_SEW IS NOT NULL THEN       
				 L_TAB := XMLTYPE('<tables><table name="таблица4"></table></tables>').appendChildXML('/tables/table',l_tab,NULL);	
			ELSIF L_ID_CWS IS NOT NULL AND L_ID_SEW IS NULL THEN    																																	 
				 L_TAB := L_TAB.deleteXML('rows/row/cell[@tag="долг_таб_во" or @tag="сумма_во_таб"]',NULL);	
 				 L_TAB := L_TAB.deleteXML('rows/row/cell[@tag="итого_долг_таб" or @tag="итого_сумма_таб"]',NULL);
 				 L_TAB := XMLTYPE('<tables><table name="таблица5"></table></tables>').appendChildXML('/tables/table',l_tab,NULL);	
			ELSIF L_ID_CWS IS NULL AND L_ID_SEW IS NOT NULL THEN    																																	 
				 L_TAB := L_TAB.deleteXML('rows/row/cell[@tag="долг_таб_хвс" or @tag="сумма_хвс_таб"]',NULL);	
 				 L_TAB := L_TAB.deleteXML('rows/row/cell[@tag="итого_долг_таб" or @tag="итого_сумма_таб"]',NULL);
 				 L_TAB := XMLTYPE('<tables><table name="таблица6"></table></tables>').appendChildXML('/tables/table',l_tab,NULL);
			END IF;	 	 	  
		END IF;		              
		
		L_XML := PKG_CONTROLLER.F_DOCX_XML(P_SQL => L_SQL,P_REP_CODE => 'REST_AGREE');   
  	L_XML := L_XML.APPENDCHILDXML('/request/input-data',l_tab,NULL);   	     		  										  

    L_REP.P_BLOB := PKG_CONTROLLER.F_GET_REPORT_XML(L_XML.getClobVal());
    L_REP.P_FILE_NAME := 'Соглашение о реструктуризации';
		
		RETURN l_rep;
	END;										
	
end PKG_DOCX_002;
/

