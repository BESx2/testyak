i�?create or replace package lawmain.PKG_XLS_REPORT_022 is

  -- Author  : BES
  -- Created : 17.02.2017
  -- Purpose : Сводный отчет по исполнительному производству
	-- Code:    FSSP_CURRENT
	
	

  FUNCTION RUN_REPORT(P_PAR_01 VARCHAR2 DEFAULT NULL
											,P_PAR_02 VARCHAR2 DEFAULT NULL
											,P_PAR_03 VARCHAR2 DEFAULT NULL
											,P_PAR_04 VARCHAR2 DEFAULT NULL
											,P_PAR_05 VARCHAR2 DEFAULT NULL
											,P_PAR_06 VARCHAR2 DEFAULT NULL
											,P_PAR_07 VARCHAR2 DEFAULT NULL
											,P_PAR_08 VARCHAR2 DEFAULT NULL
											,P_PAR_09 VARCHAR2 DEFAULT NULL
											,P_PAR_10 VARCHAR2 DEFAULT NULL) RETURN LAWSUP.PKG_FILES.REC_DOC;	  

end PKG_XLS_REPORT_022;
/

create or replace package body lawmain.PKG_XLS_REPORT_022 is

	TYPE REC_DOC IS RECORD(	P_BLOB BLOB DEFAULT NULL );

	EXCEL_DOC REC_DOC := NULL;
------------------------------------------------------------------------

	PROCEDURE p$s( ps_value IN CLOB )  -- отправляет накопленное значение ps_value в буфер и записывает
	IS
		BUFFER    	RAW(32767);
    l_offset    NUMBER := 1;
    BUF_SIZE    NUMBER := 8000;
    BUF_VAR    VARCHAR2(32767);
  BEGIN
    LOOP
    EXIT WHEN L_OFFSET > DBMS_LOB.getlength(PS_VALUE);
    BUF_VAR := DBMS_LOB.substr(PS_VALUE,BUF_SIZE,L_OFFSET);
		BUFFER := UTL_RAW.cast_to_raw( CONVERT( BUF_VAR, 'UTF8'/*'CL8MSWIN1251'*/ ) );
		DBMS_LOB.WRITEAPPEND( EXCEL_DOC.P_BLOB, UTL_RAW.LENGTH( BUFFER ), BUFFER );
    L_OFFSET := L_OFFSET + BUF_SIZE;
    END LOOP;
  END;
------------------------------------------------------------------------
	PROCEDURE P_ADD_DATA IS
	  L_XML CLOB;
		L_CNT NUMBER := 0;
		L_FMT VARCHAR2(50) := '9999999999999D00';
		L_NLS VARCHAR2(50) := 'NLS_NUMERIC_CHARACTERS=''. ''';         
		L_TOT_CNT1 NUMBER := 0; 
		L_TOT_SUM1 NUMBER := 0;
		L_TOT_CNT2 NUMBER := 0; 
		L_TOT_SUM2 NUMBER := 0;  
		
		L_PAYED_BEFORE NUMBER;
		L_PAYED_AFTER NUMBER;
		L_PENY_BEFORE NUMBER;
		L_PENY_AFTER NUMBER;
	BEGIN    
	
		L_XML :=
			'<?xml version="1.0"?>
<?mso-application progid="Excel.Sheet"?>
<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:o="urn:schemas-microsoft-com:office:office"
 xmlns:x="urn:schemas-microsoft-com:office:excel"
 xmlns:dt="uuid:C2F41010-65B3-11d1-A29F-00AA00C14882"
 xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:html="http://www.w3.org/TR/REC-html40">
 <DocumentProperties xmlns="urn:schemas-microsoft-com:office:office">
  <Created>2006-09-16T00:00:00Z</Created>
  <LastSaved>2019-04-25T11:50:38Z</LastSaved>
  <Version>16.00</Version>
 </DocumentProperties>
 <CustomDocumentProperties xmlns="urn:schemas-microsoft-com:office:office">
  <WorkbookGuid dt:dt="string">8e96cef1-3e20-481a-bf42-8981b7d25b59</WorkbookGuid>
 </CustomDocumentProperties>
 <OfficeDocumentSettings xmlns="urn:schemas-microsoft-com:office:office">
  <AllowPNG/>
  <RemovePersonalInformation/>
 </OfficeDocumentSettings>
 <ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel">
  <WindowHeight>12300</WindowHeight>
  <WindowWidth>28800</WindowWidth>
  <WindowTopX>0</WindowTopX>
  <WindowTopY>0</WindowTopY>
  <RefModeR1C1/>
  <ProtectStructure>False</ProtectStructure>
  <ProtectWindows>False</ProtectWindows>
 </ExcelWorkbook>
 <Styles>
  <Style ss:ID="Default" ss:Name="Normal">
   <Alignment ss:Vertical="Bottom"/>
   <Borders/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"/>
   <Interior/>
   <NumberFormat/>
   <Protection/>
  </Style>
  <Style ss:ID="s57">
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Calibri" x:CharSet="204" x:Family="Swiss" ss:Size="12"
    ss:Color="#000000"/>
   <Interior ss:Color="#DDD9C4" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s58">
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
	  <NumberFormat ss:Format="Short Date"/>
  </Style>
  <Style ss:ID="s59">
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <NumberFormat ss:Format="#,##0.00\ _?"/>
  </Style>
 </Styles>
 <Worksheet ss:Name="Отчет">
  <Table>
     <Column ss:Index="3" ss:Width="216.75"/>
   <Column ss:Width="67.5"/>
   <Column ss:Width="69"/>
   <Column ss:Width="129.75"/>
   <Column ss:Width="84"/>
   <Column ss:Width="84"/>
   <Column ss:Width="67.5"/>
   <Column ss:Width="76.5"/>
   <Column ss:Width="190.5"/>
   <Column ss:Width="98.25"/>
   <Column ss:Width="102.75"/>
   <Column ss:Width="105.75"/>
   <Column ss:Width="146.25"/>
   <Column ss:Width="77.25"/>
   <Column ss:Width="71.25"/>
   <Column ss:Width="114.75"/>
   <Column ss:Width="193.5"/>
   <Column ss:Width="201.75"/>
   <Column ss:Width="47.25"/>
   <Column ss:Width="121.5"/>
   <Column ss:Width="121.5"/>
	 <Column ss:Width="121.5"/>
   <Row ss:AutoFitHeight="0" ss:Height="15.75">
    <Cell ss:StyleID="s57"><Data ss:Type="String">Статус</Data></Cell>
    <Cell ss:StyleID="s57"><Data ss:Type="String">Наименование клиента</Data></Cell>
    <Cell ss:StyleID="s57"><Data ss:Type="String">Группа потребителей</Data></Cell>
    <Cell ss:StyleID="s57"><Data ss:Type="String">№ договора ХВС</Data></Cell>
    <Cell ss:StyleID="s57"><Data ss:Type="String">№ договора ВО</Data></Cell>
    <Cell ss:StyleID="s57"><Data ss:Type="String">ИНН</Data></Cell>
    <Cell ss:StyleID="s57"><Data ss:Type="String">Период задолженности</Data></Cell>
    <Cell ss:StyleID="s57"><Data ss:Type="String">Основной долг</Data></Cell>
    <Cell ss:StyleID="s57"><Data ss:Type="String">Пени</Data></Cell>
    <Cell ss:StyleID="s57"><Data ss:Type="String">Гос. Пошлина</Data></Cell>
    <Cell ss:StyleID="s57"><Data ss:Type="String">№ дела арбитр. суда</Data></Cell>
    <Cell ss:StyleID="s57"><Data ss:Type="String">Номер документа</Data></Cell>
    <Cell ss:StyleID="s57"><Data ss:Type="String">Долг по документу</Data></Cell>
    <Cell ss:StyleID="s57"><Data ss:Type="String">Пени по документу</Data></Cell>
    <Cell ss:StyleID="s57"><Data ss:Type="String">Гос.пошлина по документу</Data></Cell>>
    <Cell ss:StyleID="s57"><Data ss:Type="String">Оплата</Data></Cell>
    <Cell ss:StyleID="s57"><Data ss:Type="String">Оплата пени</Data></Cell>
    <Cell ss:StyleID="s57"><Data ss:Type="String">Оплата гос.пошлины</Data></Cell>
    <Cell ss:StyleID="s57"><Data ss:Type="String">Дата предъявления исп. документа</Data></Cell>
    <Cell ss:StyleID="s57"><Data ss:Type="String">Место предъявления исп. документа</Data></Cell>
    <Cell ss:StyleID="s57"><Data ss:Type="String">Пристав</Data></Cell>
    <Cell ss:StyleID="s57"><Data ss:Type="String">Текущий исполнитель</Data></Cell>
		<Cell ss:StyleID="s57"><Data ss:Type="String">Причина закрытия</Data></Cell>
		<Cell ss:StyleID="s57"><Data ss:Type="String">Дата закрытия</Data></Cell>
   </Row>
'; 
   p$s(L_XML);
   	 

   	FOR I IN (SELECT CC.NAME_STATUS,
			               CC.CLI_ALT_NAME, 
										 CC.GROUP_NAME,
										 CC.CWS_CTR_CODE, 
										 CC.SEW_CTR_CODE,
										 CC.INN,
										 (SELECT TO_CHAR(TRUNC(MIN(FD.DOC_DATE),'MM'),'DD.MM.YYYY')||' - '
										         ||TO_CHAR(LAST_DAY(MAX(FD.DOC_DATE)),'DD.MM.YYYY')
										  FROM T_CLI_FINDOCS FD,
											     T_CLI_DEBTS CD
											WHERE FD.ID_DOC = CD.ID_DOC
											AND   CD.ID_WORK = CC.ID_CASE) PERIOD,
										 CC.SUM_DEBT,
										 CC.PENY,
										 CC.GOV_TOLL,    
										 CASE 
                       WHEN CC.CODE_STATUS = 'CLOSE' THEN CC.CLOSE_REASON
										   ELSE NULL
										 END CLOSE_REASON,
										 CASE 
                       WHEN CC.CODE_STATUS = 'CLOSE' THEN (SELECT TO_CHAR(MAX(EV.DATE_EVENT),'YYYY-MM-DD"T00:00:00.000"') 
												                                   FROM V_CLI_EVENTS EV 
																													 WHERE EV.ID_WORK = CC.ID_CASE 
																													 AND EV.code_event = 'CLOSE')
										   ELSE NULL
										 END CLOSE_DATE,																																												 
										 E.NUM_EXEC,
										 CL.LEVY_NUM,    
										 CL.LEVY_DEBT,
										 CL.LEVY_PENY,
										 CL.LEVY_GOV_TOLL,
										 LM.MOV_SEND_DATE,										 										 
										 LM.PLACE,										 
										 CASE 
											 WHEN LM.MOV_TYPE = 'ROSP' THEN
												 (SELECT INITCAP(B.LAST_NAME||' '||B.FIRST_NAME||' '||B.SECOND_NAME)
												  FROM T_CLI_LEVY_EXECS LE,
													     T_BAILIFFS B
													WHERE LE.ID_EXEC = (SELECT MAX(EX.ID_EXEC) FROM T_CLI_LEVY_EXECS EX WHERE EX.ID_LEVY = CL.ID_LEVY)
													AND   B.ID_BAILIFF = LE.ID_BAILIFF)
											 ELSE NULL
											END	BAILIF,
											INITCAP(PKG_USERS.F_GET_FIO_BY_ID(CC.CURATOR_FSSP,'Y')) CURATOR												         
							FROM V_CLI_CASES CC,
									 T_CLI_LEVY CL,          
									 T_CLI_COURT_EXEC E,		 
									 (SELECT LM1.ID_LEVY, LM1.MOV_SEND_DATE, LM1.MOV_TYPE,
									          CASE
															 WHEN LM1.MOV_TYPE = 'ROSP' THEN R.ROSP_NAME
														   WHEN LM1.MOV_TYPE = 'BANK' THEN B.BANK_NAME
														   WHEN LM1.MOV_TYPE = 'UFK' THEN U.UFK_NAME
													  END PLACE
										FROM T_CLI_LEVY_MOVING LM1
										LEFT OUTER JOIN T_CLI_LEVY_MOVING LM2   										
										ON (LM1.ID_LEVY = LM2.ID_LEVY AND LM1.ID_MOV < LM2.ID_MOV)
										LEFT OUTER JOIN T_ROSP R ON (LM1.ID_PLACE = R.ID_ROSP AND LM1.MOV_TYPE = 'ROSP')
										LEFT OUTER JOIN T_BANKS B ON (LM1.ID_PLACE = B.ID_BANK AND LM1.MOV_TYPE = 'BANK')
									  LEFT OUTER JOIN T_UFK U ON (LM1.ID_PLACE = U.ID_UFK AND LM1.MOV_TYPE = 'UFK')
										WHERE LM2.ID_MOV IS NULL) LM
							WHERE 1=1
							AND   CC.DEP IN ('FSSP','BANKRUPT')
							AND   CL.ID_CASE = CC.ID_CASE
							AND   CL.ID_LEVY = (SELECT MAX(L1.ID_LEVY) FROM T_CLI_LEVY L1 WHERE L1.ID_CASE = CC.ID_CASE)
							AND   E.ID = CC.LAST_EXEC    
--							AND   CC.CODE_STATUS  != 'CLOSE'
							AND   CL.ID_LEVY = LM.ID_LEVY(+))
		LOOP					         		                                                 
			  L_XML := '<Row ss:AutoFitHeight="0">'||CHR(13);
				L_XML := L_XML ||'   <Cell ss:StyleID="s58"><Data ss:Type="String">'||I.NAME_STATUS||'</Data></Cell>'||CHR(13);
				L_XML := L_XML ||'   <Cell ss:StyleID="s58"><Data ss:Type="String">'||I.CLI_ALT_NAME||'</Data></Cell>'||CHR(13);
				L_XML := L_XML ||'   <Cell ss:StyleID="s58"><Data ss:Type="String">'||I.GROUP_NAME||'</Data></Cell>'||CHR(13);
				L_XML := L_XML ||'   <Cell ss:StyleID="s58"><Data ss:Type="String">'||I.CWS_CTR_CODE||'</Data></Cell>'||CHR(13);
        L_XML := L_XML ||'   <Cell ss:StyleID="s58"><Data ss:Type="String">'||I.SEW_CTR_CODE||'</Data></Cell>'||CHR(13);
				L_XML := L_XML ||'   <Cell ss:StyleID="s58"><Data ss:Type="String">'||I.INN||'</Data></Cell>'||CHR(13);
				L_XML := L_XML ||'   <Cell ss:StyleID="s58"><Data ss:Type="String">'||I.PERIOD||'</Data></Cell>'||CHR(13);
				L_XML := L_XML ||'   <Cell ss:StyleID="s59"><Data ss:Type="Number">'||TO_CHAR(I.SUM_DEBT,L_FMT,L_NLS)||'</Data></Cell>'||CHR(13);
				L_XML := L_XML ||'   <Cell ss:StyleID="s59"><Data ss:Type="Number">'||TO_CHAR(I.PENY,L_FMT,L_NLS)||'</Data></Cell>'||CHR(13);
				L_XML := L_XML ||'   <Cell ss:StyleID="s59"><Data ss:Type="Number">'||TO_CHAR(I.GOV_TOLL,L_FMT,L_NLS)||'</Data></Cell>'||CHR(13);
				L_XML := L_XML ||'   <Cell ss:StyleID="s58"><Data ss:Type="String">'||I.NUM_EXEC||'</Data></Cell>'||CHR(13);
				L_XML := L_XML ||'   <Cell ss:StyleID="s58"><Data ss:Type="String">'||I.LEVY_NUM||'</Data></Cell>'||CHR(13);
				L_XML := L_XML ||'   <Cell ss:StyleID="s59"><Data ss:Type="Number">'||TO_CHAR(NVL(I.LEVY_DEBT,0),L_FMT,L_NLS)||'</Data></Cell>'||CHR(13);
				L_XML := L_XML ||'   <Cell ss:StyleID="s59"><Data ss:Type="Number">'||TO_CHAR(NVL(I.LEVY_PENY,0),L_FMT,L_NLS)||'</Data></Cell>'||CHR(13);
				L_XML := L_XML ||'   <Cell ss:StyleID="s59"><Data ss:Type="Number">'||TO_CHAR(NVL(I.LEVY_GOV_TOLL,0),L_FMT,L_NLS)||'</Data></Cell>'||CHR(13);
				L_XML := L_XML ||'   <Cell ss:StyleID="s59"><Data ss:Type="Number">'||TO_CHAR(NVL(I.LEVY_DEBT,0)-NVL(I.SUM_DEBT,0),L_FMT,L_NLS)||'</Data></Cell>'||CHR(13);
				L_XML := L_XML ||'   <Cell ss:StyleID="s59"><Data ss:Type="Number">'||TO_CHAR(NVL(I.LEVY_PENY,0)-NVL(I.PENY,0),L_FMT,L_NLS)||'</Data></Cell>'||CHR(13);
				L_XML := L_XML ||'   <Cell ss:StyleID="s59"><Data ss:Type="Number">'||TO_CHAR(NVL(I.LEVY_GOV_TOLL,0)-NVL(I.GOV_TOLL,0),L_FMT,L_NLS)||'</Data></Cell>'||CHR(13);
				L_XML := L_XML ||'   <Cell ss:StyleID="s58"><Data ss:Type="String">'||TO_CHAR(I.MOV_SEND_DATE,'DD.MM.YYYY')||'</Data></Cell>'||CHR(13);
				L_XML := L_XML ||'   <Cell ss:StyleID="s58"><Data ss:Type="String">'||I.PLACE||'</Data></Cell>'||CHR(13);
				L_XML := L_XML ||'   <Cell ss:StyleID="s58"><Data ss:Type="String">'||I.BAILIF||'</Data></Cell>'||CHR(13);
				L_XML := L_XML ||'   <Cell ss:StyleID="s58"><Data ss:Type="String">'||I.CURATOR||'</Data></Cell>'||CHR(13);
				L_XML := L_XML ||'   <Cell ss:StyleID="s58"><Data ss:Type="String">'||I.CLOSE_REASON||'</Data></Cell>'||CHR(13); 
				IF I.CLOSE_DATE IS NOT NULL THEN
				  L_XML := L_XML ||'   <Cell ss:StyleID="s58"><Data ss:Type="DateTime">'||I.CLOSE_DATE||'</Data></Cell>'||CHR(13);
				ELSE
					L_XML := L_XML ||'   <Cell ss:StyleID="s58"/>'||CHR(13);
				END IF;
				L_XML := L_XML ||'  </Row>'||CHR(13);
				P$S(L_XML);
		END LOOP;		
		
	 L_XML := ' </Table>
  <WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">
   <PageSetup>
    <Header x:Margin="0.3"/>
    <Footer x:Margin="0.3"/>
    <PageMargins x:Bottom="0.75" x:Left="0.7" x:Right="0.7" x:Top="0.75"/>
   </PageSetup>
   <Unsynced/>
   <Print>
    <ValidPrinterInfo/>
    <PaperSizeIndex>9</PaperSizeIndex>
    <VerticalResolution>0</VerticalResolution>
   </Print>
   <Selected/>
   <LeftColumnVisible>2</LeftColumnVisible>
   <Panes>
    <Pane>
     <Number>3</Number>
     <ActiveRow>27</ActiveRow>
     <ActiveCol>3</ActiveCol>
    </Pane>
   </Panes>
   <ProtectObjects>False</ProtectObjects>
   <ProtectScenarios>False</ProtectScenarios>
  </WorksheetOptions>
 </Worksheet>
</Workbook>

';

		p$s( L_XML );
	
	
	END P_ADD_DATA;

---------------------------------------------------------------------------------------------------------------------------------

  FUNCTION RUN_REPORT(P_PAR_01 VARCHAR2 DEFAULT NULL
											,P_PAR_02 VARCHAR2 DEFAULT NULL
											,P_PAR_03 VARCHAR2 DEFAULT NULL
											,P_PAR_04 VARCHAR2 DEFAULT NULL
											,P_PAR_05 VARCHAR2 DEFAULT NULL
											,P_PAR_06 VARCHAR2 DEFAULT NULL
											,P_PAR_07 VARCHAR2 DEFAULT NULL
											,P_PAR_08 VARCHAR2 DEFAULT NULL
											,P_PAR_09 VARCHAR2 DEFAULT NULL
											,P_PAR_10 VARCHAR2 DEFAULT NULL) RETURN LAWSUP.PKG_FILES.REC_DOC
	IS
	l_BLOB LAWSUP.PKG_FILES.REC_DOC;
  BEGIN

	  EXCEL_DOC := NULL;
		DBMS_LOB.CREATETEMPORARY( EXCEL_DOC.P_BLOB, TRUE );
    DBMS_LOB.OPEN( EXCEL_DOC.P_BLOB, DBMS_LOB.LOB_READWRITE );

		P_ADD_DATA();

		DBMS_LOB.CLOSE(EXCEL_DOC.P_BLOB);

		    l_BLOB.P_BLOB := EXCEL_DOC.P_BLOB;
	      l_BLOB.P_FILE_NAME := 'Общий отчет исп производства'||P_PAR_01;
				RETURN L_BLOB;

  END RUN_REPORT;

end PKG_XLS_REPORT_022;
/

