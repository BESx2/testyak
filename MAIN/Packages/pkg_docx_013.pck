i�?create or replace package lawmain.PKG_DOCX_013 is

  -- Author  : EUGEN
  -- Created : 04.03.2020 15:02:04
  -- Purpose : Заявление о розыске должника
	-- CODE    : FSSP_KNOWLEDGE
  
  FUNCTION RUN_REPORT(P_PAR_01  IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_02 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_03 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_04 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_05 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_06 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_07 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_08 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_09 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_10 IN VARCHAR2 DEFAULT NULL) RETURN LAWSUP.PKG_FILES.REC_DOC;		                           

end PKG_DOCX_013;
/

create or replace package body lawmain.PKG_DOCX_013 is

  FUNCTION RUN_REPORT(P_PAR_01  IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_02 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_03 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_04 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_05 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_06 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_07 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_08 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_09 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_10 IN VARCHAR2 DEFAULT NULL) RETURN LAWSUP.PKG_FILES.REC_DOC
   IS  
   L_XML XMLTYPE;
   L_SQL SYS_REFCURSOR;     
	 L_GROUP VARCHAR2(50);
   L_REP LAWSUP.PKG_FILES.REC_DOC;    
  BEGIN         
    OPEN L_SQL FOR 
      SELECT r.ROSP_NAME "росп"
						 ,T.CLI_ALT_NAME "абонент"
						 ,'«' || TO_CHAR(SYSDATE, 'dd') || '» ' ||
						 LOWER(PKG_UTILS.F_GET_NAME_MONTHS(SYSDATE, 'R')) || ' ' ||
						 TO_CHAR(SYSDATE, 'YYYY') || 'г.' "дата_заявления"
						 ,T.LEVY_DATE "дата_исполлиста"
						 ,EX.EXEC_NUM "номер_исполпроиз"
						 ,EX.EXEC_START_DATE "дата_исполпроиз"
						 ,EX.EXEC_END_DATE "дата_окон_исполпроиз"
						 ,INITCAP(PKG_MORFER.F_GET_DECLENSION(PKG_COURTS.F_GET_FIO_BAILIFF(EX.ID_BAILIFF,'Y'),'Ins','S')) "ФИО_СП"
						 ,T.LEVY_SERIES "серия_исполлиста"
						 ,T.LEVY_NUM "номер_исполлиста"       
						 ,T.LEVY_DEBT "общая_сумма_долга"
						 ,T.LEVY_PENY "общая_сумма_неустойки"
						 ,T.LEVY_GOV_TOLL "общая_сумма_госпошлины"          
						 ,T.CURATOR "ФИО"
             ,T.POSITION "должность"
						 ,'тел.: '||T.PHONE "телефон"
						 ,T.DOVER_INFO "доверенность"                                                     
						 ,T.CUR_FULL  "исполнитель"
			FROM  (SELECT S.CLI_ALT_NAME
                    ,(SELECT MAX(E.ID_EXEC) FROM T_CLI_LEVY_EXECS E
										  WHERE E.ID_LEVY = L.ID_LEVY) ID_LEVY_EXEC
                    ,(SELECT MAX(E.ID)
                      FROM T_CLI_COURT_EXEC E 
                      WHERE E.ID_CASE = S.ID_CASE) ID_EXEC
										,(SELECT MAX(LM.ID_MOV)
										  FROM T_CLI_LEVY_MOVING LM
											WHERE LM.ID_LEVY = L.ID_LEVY
											AND   LM.MOV_TYPE = 'ROSP') ID_MOV
                     ,L.LEVY_SERIES
                     ,L.LEVY_NUM   
										 ,L.LEVY_DATE     
										 ,L.LEVY_GOV_TOLL
										 ,L.LEVY_PENY
										 ,L.LEVY_DEBT                          
                     ,INITCAP(UL.LAST_NAME || ' ' ||
                              SUBSTR(UL.FIRST_NAME,1,1) || '. ' || 
                              SUBSTR(UL.SECOND_NAME,1,1)||'.') CURATOR
											,INITCAP(UL.LAST_NAME||' '||UL.FIRST_NAME||' '||UL.SECOND_NAME) CUR_FULL
                      ,UL.PHONE              
										  ,UL.DOVER_INFO
											,UL.POSITION
                    FROM V_CLI_CASES S
                    LEFT OUTER JOIN T_USER_LIST UL ON S.CURATOR_FSSP = UL.ID                
                    INNER JOIN T_CLI_LEVY L ON L.ID_CASE = S.ID_CASE 
										WHERE S.ID_CASE = TO_NUMBER(P_PAR_01)) T
			     ,T_CLI_LEVY_EXECS EX                     
           ,T_CLI_COURT_EXEC E                         
					 ,T_CLI_LEVY_MOVING M 
					 ,T_ROSP R
      WHERE EX.ID_EXEC = T.ID_LEVY_EXEC
			AND   M.ID_MOV = T.ID_MOV
			AND   R.ID_ROSP = M.ID_PLACE              
      AND   E.ID = T.ID_EXEC;
			
    L_XML := PKG_CONTROLLER.F_DOCX_XML(P_SQL => L_SQL,P_REP_CODE => 'FSSP_KNOWLEDGE');    		
    L_REP.P_BLOB := PKG_CONTROLLER.F_GET_REPORT_XML(L_XML.getClobVal());
    L_REP.P_FILE_NAME := 'Заявление об ознакомлении';
		
		RETURN l_rep;
	END;										
	
end PKG_DOCX_013;
/

