i�?CREATE OR REPLACE PACKAGE LAWMAIN.PKG_PDF_010
  IS
 -- Author  : EUGEN
 -- Created : 13.12.2016 
 -- Purpose : Претензия для юридического лица
 -- Code    : SHUT_NOTIF_SEW


	l_pdf_1 blob;
	v_tpl_1   plpdf_type.tr_tpl_data;
	v_tpl_2   plpdf_type.tr_tpl_data;
	l_tpl_id_1 number;
	l_tpl_id_2 number;

  TYPE R_D_T IS RECORD (CLI_ALT_NAME VARCHAR2(4000)
												,ADDRESS      VARCHAR2(4000)
												,CONTRACTS    VARCHAR2(4000)    
												,DEBT_DATE    VARCHAR2(4000)
												,SUM_DEBT     VARCHAR2(4000)
												,DEBT_WORDS   VARCHAR2(4000)                     
												,DEBT_KOP     VARCHAR2(4000)
												,DIR          VARCHAR2(4000)
												,CURATOR      VARCHAR2(4000)
												,PHONE        VARCHAR2(4000) 
												,CURRENT_DATE VARCHAR2(4000)
												,OBJ_ADDRESS  CLOB);
  R_D R_D_T;
  
	
  FUNCTION RUN_REPORT(P_PAR_01 VARCHAR2 DEFAULT NULL
											,P_PAR_02 VARCHAR2 DEFAULT NULL
											,P_PAR_03 VARCHAR2 DEFAULT NULL
											,P_PAR_04 VARCHAR2 DEFAULT NULL
											,P_PAR_05 VARCHAR2 DEFAULT NULL
											,P_PAR_06 VARCHAR2 DEFAULT NULL
											,P_PAR_07 VARCHAR2 DEFAULT NULL
											,P_PAR_08 VARCHAR2 DEFAULT NULL
											,P_PAR_09 VARCHAR2 DEFAULT NULL
											,P_PAR_10 VARCHAR2 DEFAULT NULL) RETURN LAWSUP.PKG_FILES.REC_DOC;

END PKG_PDF_010;
/

CREATE OR REPLACE PACKAGE BODY LAWMAIN.PKG_PDF_010
    IS

  ----------------------------------------------------------------------
  FUNCTION AD(P1 IN VARCHAR2,P2 IN VARCHAR2) RETURN VARCHAR2
    IS
  BEGIN
    IF P2 IS NOT NULL THEN
      RETURN p1;
    END IF;
    RETURN p2;
  END;

  PROCEDURE P_ADD_DATA(P_ID_SHUT NUMBER) 
		IS  
		L_NLS VARCHAR2(50) := 'NLS_NUMERIC_CHARACTERS='', ''';
		L_FMT VARCHAR2(50) := 'FM999G999G999G999G999G990D00';                         
	BEGIN  
		FOR I IN (SELECT S.CLI_ALT_NAME 
									 ,PKG_CLIENTS.F_GET_ADDRESS(S.ID_CLIENT) ADDRESS
									 ,'№ ' || S.CWS_CTR_NUMBER || ' от ' ||TO_CHAR(S.CWS_CTR_DATE, 'dd.mm.yyyy"г."') CONTRACTS
									 ,TO_CHAR(S.DEBT_DATE, 'DD.MM.YYYY"г."') DEBT_DATE
									 ,TO_CHAR(TRUNC(S.DEBT_CREATED), 'FM999G999G999G999') SUM_DEBT
									 ,LAWSUP.PKG_FMT_RU.NUMBER_IN_WORDS(TRUNC(S.DEBT_CREATED)) DEBT_WORDS
									 ,TO_CHAR(MOD(S.DEBT_CREATED, 1) * 100) DEBT_KOP
									 ,PKG_PREF.F$C2('ORG_MAIN_FIO') DIR
									 ,(SELECT INITCAP(L.LAST_NAME || ' ' || L.FIRST_NAME || ' ' ||L.SECOND_NAME)
										FROM   T_USER_LIST L WHERE  L.ID = F$_USR_ID) CURATOR
									 ,(SELECT L.PHONE FROM T_USER_LIST L WHERE  L.ID = F$_USR_ID) PHONE
									 ,PKG_CLI_SHUTDOWN.F_GET_OBJECT_LIST(S.ID_WORK,10) OBJ_ADDRESS
						FROM   V_CLI_SHUTDOWN S
						WHERE  S.ID_WORK = P_ID_SHUT)
    LOOP
      R_D.CLI_ALT_NAME := I.CLI_ALT_NAME;
			R_D.ADDRESS      := I.ADDRESS;
      R_D.CONTRACTS    := I.CONTRACTS;      
      R_D.DEBT_DATE    := I.DEBT_DATE;
      R_D.SUM_DEBT     := I.SUM_DEBT;
      R_D.DEBT_WORDS   := I.DEBT_WORDS;                      
			R_D.DEBT_KOP     := I.DEBT_KOP;
      R_D.DIR          := I.DIR;
      R_D.CURATOR      := I.CURATOR;
      R_D.PHONE        := I.PHONE;
      R_D.OBJ_ADDRESS  := I.OBJ_ADDRESS;
			R_D.CURRENT_DATE := TO_CHAR(SYSDATE,'DD.MM.YYYY"г"'); 
    END LOOP;          
  END;

  FUNCTION F_GET_PDF(P_OPTION VARCHAR2) RETURN BLOB
  IS

    l_blob blob;

    l_ttf_t        Plpdf_Type.t_addfont;
    l_ttf_tbd      Plpdf_Type.t_addfont;   
     
    l$_tw_id    NUMBER          := PLPDF_FONTS.l$_tw_id;
    l$_tw       VARCHAR2(40)    := PLPDF_FONTS.l$_tw; 
    l$_twbd_id    NUMBER        := PLPDF_FONTS.l$_twbd_id; 
    l$_twbd     VARCHAR2(40)    := PLPDF_FONTS.l$_twbd;
    
    v_tpl_1   plpdf_type.tr_tpl_data;
    l_tpl_id NUMBER;
    C_BORDER NUMBER := 0;
    L_W NUMBER;
    L_H NUMBER;
    L_Y NUMBER;
    L_IMG BLOB;
  BEGIN
   IF P_OPTION = 'TEMPLATE' THEN
      NULL;
   ELSE
      Plpdf.init;
      Plpdf.setEncoding(p_enc => 'CP1251');

      l_ttf_t       :=  Plpdf_Ttf.GetTTF(l$_tw_id);
      l_ttf_tbd     :=  Plpdf_Ttf.GetTTF(l$_twbd_id);
      plpdf.addTTF(p_family => l$_tw,p_data => l_ttf_t);
      plpdf.addTTF(p_family => l$_twbd,p_data => l_ttf_tbd);        
      
      Plpdf.NewPage;
      
      v_tpl_1 := PLPDF_PARSER.LoadTemplate(p_id => 1);
      l_tpl_id := PLPDF.InsTemplate(v_tpl_1);
      PLPDF.setPageTemplate(p_tplidx => l_tpl_id);      
      
    END IF;
    
    PLPDF.setLeftMargin(29); 
    PLPDF.setRightMargin(15); 
    
    PLPDF.setPrintFont(l$_tw,null,12); 
    PLPDF.setCurrentY(55);
    PLPDF.PrintCell(p_w => 22,p_h => 5,p_txt => R_D.CURRENT_DATE,p_border => C_BORDER,p_align => 'L',p_ln => 0,p_clipping => FALSE);
    PLPDF.PrintCell(p_w => 40,p_h => 5,p_txt => '№ ___________',p_border => C_BORDER,p_align => 'L',p_clipping => FALSE,p_ln => 2);
    PLPDF.LineBreak(2);
    PLPDF.setCurrentX(p_x => plpdf.getPageWidth-93);
    PLPDF.PrintMultiLineCell(p_w => 80,p_h => 5,P_TXT=>R_D.CLI_ALT_NAME,p_border => C_BORDER,p_align => 'C',p_clipping => FALSE,p_ln => 2);
    PLPDF.PrintMultiLineCell(p_w => 80,p_h => 5,P_TXT=>R_D.ADDRESS,p_border => C_BORDER,p_align => 'C',p_clipping => FALSE,p_ln => 1);
    
    PLPDF.LineBreak(9); 
    PLPDF.setPrintFont(l$_tw,null,12); 
    PLPDF.PrintMultiLineCell(p_txt => 'УВЕДОМЛЕНИЕ'||chr(13)||'о прекращении водоотведения'
		                        ,p_w => plpdf.getPageWidth-42,p_h => 5,p_clipping => FALSE,p_align => 'C',p_border => C_BORDER);
    
    PLPDF.LineBreak(10);     
    L_W := plpdf.getPageWidth-42;          
    
		PLPDF.PrintMultiLineCell(p_w => L_W,p_h => 4.85,p_indent => 13
															,p_txt => 'АО «Водоканал», руководствуясь п. п. 1-3 ст. 21 Федерального закона от 07.12.2011г.'||
																				' «О водоснабжении и водоотведении» №416-ФЗ, и в связи с неоплатой Вами услуг водоотведения более'||
																				' двух расчетных периодов по заключенному договору водоотведения '||R_D.CONTRACTS
																				||', уведомляет Вас о временном прекращении с [дата_ограничения] года'||
																				' водоотведения на объекте, расположенном по адресу: '||R_D.OBJ_ADDRESS
															,p_border => C_BORDER,p_ln => 2,p_clipping => FALSE,p_align => 'J');  
                                
		PLPDF.PrintMultiLineCell(p_w => L_W,p_h => 4.85,p_indent => 13
															,p_txt => 'Задолженность '||R_D.CLI_ALT_NAME||' перед АО «Водоканал» за услуги водоотведения по состоянию'||
															          ' на '||R_D.DEBT_DATE||' составляет '||R_D.SUM_DEBT||' ('||R_D.DEBT_WORDS||') рубля '||R_D.DEBT_KOP||' копеек,'||
																				' что говорит о ненадлежащем исполнении '||R_D.CLI_ALT_NAME||' своих обязательств по заключенному договору'||
																				' в части своевременной и полной оплаты предоставленных услуг водоотведения. '
															,p_border => C_BORDER,p_ln => 2,p_clipping => FALSE,p_align => 'J');     
															
  	PLPDF.PrintMultiLineCell(p_w => L_W,p_h => 4.85,p_indent => 13
															,p_txt => 'Задолженность Вашей организации за предоставленные услуги перед АО «Водоканал»'||
															' на '||R_D.DEBT_DATE||' составляет '||R_D.SUM_DEBT||' ('||R_D.DEBT_WORDS||') рубля '||R_D.DEBT_KOP||' коп.'
															,p_border => C_BORDER,p_ln => 2,p_clipping => FALSE,p_align => 'J');  
		
  	PLPDF.PrintMultiLineCell(p_w => L_W,p_h => 4.85,p_indent => 13
															,p_txt => 'В соответствии с ч. 4 ст. 21 Федерального закона от 07.12.2011 г. № 416-ФЗ «О водоснабжении'||
															          ' и водоотведении», прекращение водоотведения вводится до устранения обстоятельств, явившихся'||
																				' причиной такого прекращения, то есть до оплаты задолженности. '
															,p_border => C_BORDER,p_ln => 2,p_clipping => FALSE,p_align => 'J');            
				
  	PLPDF.PrintMultiLineCell(p_w => L_W,p_h => 4.85,p_indent => 13
															,p_txt => 'В соответствии с п. 67 Правил холодного водоснабжения и водоотведения, утвержденных Постановлением'||
															          ' Правительства Российской Федерации от 29.07.2013 г. № 644, в случае временного прекращения водоотведения, '||
																				R_D.CLI_ALT_NAME||' также будет обязано возместить АО «Водоканал» расходы на введение временного'||
																				' прекращения и восстановления водоотведения. Возмещение указанных расходов производится на основании'||
																				' расчета, произведенного АО «Водоканал».  '
															,p_border => C_BORDER,p_ln => 2,p_clipping => FALSE,p_align => 'J');  													

 
    PLPDF.LineBreak(10);    
    PLPDF.PrintCell(p_w => L_W/3,p_h => 4.85,p_txt => 'Генеральный директор',p_border => C_BORDER,p_clipping => FALSE,p_align => 'L',p_ln => 0);  
    L_Y := PLPDF.getCurrentY;
		
    PLPDF.setCurrentXY(p_x => (2*L_W/3)+29,p_y => L_Y);
    PLPDF.PrintCell(p_w => L_W/3,p_h => 4.85,p_txt => PKG_PREF.F$C2('ORG_MAIN_FIO'),p_border => C_BORDER,p_clipping => FALSE,p_align => 'R',p_ln => 1);                          
    
    PLPDF.LineBreak(10);
    PLPDF.PrintCell(p_w => L_W/2,p_h => 4.85,p_txt => 'Уведомление получил представитель Абонента: _______________(__________________)',p_border => C_BORDER,p_clipping => FALSE,p_align => 'L',p_ln => 0);  
    PLPDF.LineBreak(5);      
    PLPDF.PrintCell(p_w => L_W/2,p_h => 4.85,p_txt => 'Дата:____________________',p_border => C_BORDER,p_clipping => FALSE,p_align => 'L',p_ln => 0);      
    PLPDF.setPrintFont(l$_tw,null,10);
    PLPDF.LineBreak(7);                                                                                                                                  
    PLPDF.PrintCell(p_w => L_W,p_h => 4.85,p_txt => R_D.CURATOR||' '||R_D.PHONE,p_border => C_BORDER,p_clipping => FALSE,p_align => 'L',p_ln => 0);  
    
    
   IF P_OPTION = 'TEMPLATE' THEN               
       NULL;
   ELSE
    plpdf.SendDoc (p_blob => l_blob);
   END IF;        
   
    RETURN l_blob;

  END F_GET_PDF;

  ----------------------------------------------------------------------
  FUNCTION RUN_REPORT(P_PAR_01 VARCHAR2 DEFAULT NULL
                      ,P_PAR_02 VARCHAR2 DEFAULT NULL
                      ,P_PAR_03 VARCHAR2 DEFAULT NULL
                      ,P_PAR_04 VARCHAR2 DEFAULT NULL
                      ,P_PAR_05 VARCHAR2 DEFAULT NULL
                      ,P_PAR_06 VARCHAR2 DEFAULT NULL
                      ,P_PAR_07 VARCHAR2 DEFAULT NULL
                      ,P_PAR_08 VARCHAR2 DEFAULT NULL
                      ,P_PAR_09 VARCHAR2 DEFAULT NULL
                      ,P_PAR_10 VARCHAR2 DEFAULT NULL) RETURN LAWSUP.PKG_FILES.REC_DOC

      IS
            PROGRAM_UNIT VARCHAR2(200) DEFAULT 'PKG_PDF_010.RUN_REPORT';
            L_BLOB LAWSUP.PKG_FILES.REC_DOC;
            s_filename VARCHAR2(200) := 'Uvedomlenie_na_ogranichenie';
    BEGIN
        
        DBMS_LOB.createtemporary(lob_loc => L_BLOB.P_BLOB,cache => TRUE);
        L_BLOB.P_FILE_NAME := s_filename;      
        P_ADD_DATA(P_PAR_01);
        L_BLOB.P_BLOB := F_GET_PDF(P_PAR_02);
        
        RETURN L_BLOB;
 /*  EXCEPTION 
     WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20200,DBMS_UTILITY.format_error_backtrace);   */
        
  END RUN_REPORT;

END PKG_PDF_010;
/

