i�?CREATE OR REPLACE PACKAGE LAWMAIN.PKG_RESTRUC IS

	-- Author  : EUGEN
	-- Created : 24.03.2019 11:23:29
	-- Purpose : Пакет для работы с рестуктуризацией  	
	
	TYPE PAY_ROW IS RECORD(DATE_PAY DATE,
	                       PAY_CWS  NUMBER,
	                       PENY_CWS NUMBER,
												 PAY_SEW  NUMBER,
												 PENY_SEW NUMBER,
												 ID_PAY_CWS NUMBER,
												 ID_PAY_SEW NUMBER);
	
	TYPE R_CALC_RES IS RECORD (PAY_SUM NUMBER,
	                           PENY_SUM NUMBER,
	                           DATE_CALC DATE);
	
	TYPE DEBT_CALC IS TABLE OF R_CALC_RES INDEX BY PLS_INTEGER;													 
	
	TYPE R_DEBT_REST IS RECORD(ID_DEBT  NUMBER,
	                           DOC_DATE DATE,
														 DEBT     NUMBER,
														 PENY     NUMBER);
															
	TYPE REST_DEBTS IS TABLE OF R_DEBT_REST;				
	
	PROCEDURE P_CALC_RESTRUCT_PERIOD(P_DATE_START DATE
																 ,P_DATE_END   DATE
																 ,P_PAY_TO     NUMBER
																 ,P_ID_REST    NUMBER);
	
  PROCEDURE P_CALC_RESTRUCT_AMOUNT(P_DATE_START DATE,
						 											 P_SUM_PAY NUMBER,
																	 P_PAY_TO NUMBER,
																	 P_ID_REST NUMBER) ;
																	   	
	PROCEDURE P_CREATE_RESTRUCT(P_REST_NUM     VARCHAR2
														 ,P_START_DATE   DATE
														 ,P_DEBTS        wwv_flow_global.vc_arr2
														 ,P_DATE_CREATED DATE DEFAULT SYSDATE);
	
  PROCEDURE P_UPDATE_REST_DEBTS(P_ID_REST     NUMBER
		                           ,P_DEBTS       wwv_flow_global.vc_arr2);			
	
 	PROCEDURE P_RECALC_PAYMENT(P_ID_REST NUMBER,
														 P_DATA    PAY_ROW);
														 
  --PROCEDURE P_CREATE_REST_PAY_DET(P_ID_REST NUMBER);	
	
  PROCEDURE P_BEGIN_WORK(P_ID_REST NUMBER, P_NUM_REST VARCHAR2);												 											 										 
	
	PROCEDURE P_CHANGE_STATUS(P_ID_REST NUMBER, P_CODE_STATUS VARCHAR2);  
	
  PROCEDURE P_DELETE_REST(P_ID_REST NUMBER);
END PKG_RESTRUC;
/

CREATE OR REPLACE PACKAGE BODY LAWMAIN.PKG_RESTRUC IS

	PROCEDURE P_CALC_RESTRUCT_PERIOD(P_DATE_START DATE
																 ,P_DATE_END   DATE
																 ,P_PAY_TO     NUMBER
																 ,P_ID_REST    NUMBER)
		IS       
		L_CALC DEBT_CALC;
		L_PENY NUMBER;                 
		L_PENY_DATE DATE;
		L_ABON_GROUP VARCHAR2(100);                                          
    L_PENY_SUM NUMBER;
		L_DEBT_SUM NUMBER;
	BEGIN        
    SELECT R.ABON_GROUP,NVL(R.PENY_DATE,TRUNC(SYSDATE,'DD')) 
		INTO L_ABON_GROUP,L_PENY_DATE
	  FROM T_CLI_REST R WHERE R.ID_REST = P_ID_REST;
		
	  L_PENY := PKG_REFIN.F_GET_CLI_PENY(P_ID_CASE => P_ID_REST,P_DATE => L_PENY_DATE,P_GROUP => L_ABON_GROUP);	
					
		DELETE FROM T_CLI_REST_PAY_DET DET WHERE DET.ID_PAY IN (SELECT P.ID FROM T_CLI_REST_PAYMENTS P WHERE P.ID_REST = P_ID_REST);
	  DELETE FROM T_CLI_REST_PAYMENTS P WHERE P.ID_REST = P_ID_REST;
		
		UPDATE T_CLI_REST R
		   SET R.REST_START = P_DATE_START,
		  	   R.REST_END   = P_DATE_END,
		 			 R.PAY_DAY    = P_PAY_TO,
 					 R.TYPE_CALC  = 'PERIOD',
					 R.PENY_SUM   = L_PENY
		 WHERE R.ID_REST = P_ID_REST;
		
		FOR I IN (SELECT DISTINCT D.ID_CONTRACT FROM T_CLI_DEBTS D WHERE D.ID_WORK = P_ID_REST)
			LOOP
				SELECT SUM(D.PENY), SUM(D.DEBT_CREATED) INTO L_PENY_SUM, L_DEBT_SUM 
			  FROM T_CLI_DEBTS D WHERE D.ID_WORK = P_ID_REST AND D.ID_CONTRACT = I.ID_CONTRACT;   
				
				SELECT FLOOR(L_DEBT_SUM / (COUNT(*) OVER()) * 100) / 100 + CASE
								 WHEN ROW_NUMBER() OVER(ORDER BY T.PERIOD) <=
											(L_DEBT_SUM - (FLOOR(L_DEBT_SUM / (COUNT(*) OVER()) * 100) / 100) *
											 (COUNT(*) OVER())) / 0.01 THEN    0.01
								 ELSE    0
							 END PAY_SUM   --РАСЧЕТ СУММЫ ПЛАТЕЖА, МАХИНАЦИИ С КОПЕЙКАМИ					
							,FLOOR(L_PENY_SUM / (COUNT(*) OVER()) * 100) / 100 + CASE
								 WHEN ROW_NUMBER() OVER(ORDER BY T.PERIOD) <=
											(L_PENY_SUM - (FLOOR(L_PENY_SUM / (COUNT(*) OVER()) * 100) / 100) *
											 (COUNT(*) OVER())) / 0.01 THEN    0.01
								 ELSE    0
							 END PENY_SUM   
							,T.PERIOD
				BULK COLLECT INTO L_CALC
				FROM 
						 ---СПИСОК ВСЕХ ДНЕЙ МЕЖДУ ДВУМЯ ДАТАМИ
						 (SELECT START_DATE + LEVEL - 1 PERIOD
							FROM (SELECT P_DATE_START START_DATE
													,P_DATE_END   END_DATE
										FROM DUAL)
							CONNECT BY LEVEL <= TRUNC(END_DATE) - TRUNC(START_DATE) + 1) T
							
				WHERE EXTRACT(DAY FROM PERIOD) =    --ЕСЛИ ОПЛАТА ПО ИЗ ЧИСЛА ПОСЛЕДНИХ ДЕНЙ МЕСЯЦА
								CASE                                                                                            
									--ЕЛСИ ФЕВРАЛЬ И ОПЛАТА ПО > 29, ТОГДА БЕРЁМ ВСЕГДА ПОСЛЕДНИЙ ДЕНЬ МЕСЯЦА
									WHEN P_PAY_TO >= 29 AND EXTRACT(MONTH FROM PERIOD) = 2 THEN EXTRACT(DAY FROM LAST_DAY(PERIOD))
									--ЕСЛИ ОПЛАТИТЬ ПО 31, А ПОСЛЕДНИЙ ДЕНЬ МЕСЯЦА 30, ТО УСТАНАВЛИВАЕМ 30
									WHEN P_PAY_TO = 31 AND EXTRACT(DAY FROM LAST_DAY(PERIOD)) = 30 THEN 30
									--ИНАЧЕ ПРОСТО ОТБИРАЕМ ДАТУ В ЭТОМ МЕСЯЦЕ
									ELSE P_PAY_TO
								END    
				--ДОБАВЛЯЯ ПЕРИОД ПО, Т.К. ДАТА МОЖЕТ ОТЛИЧАТЬСЯ ОТ "ОПЛАТИТЬ ПО" 				
				OR T.PERIOD = P_DATE_END; 
				
				IF L_CALC.COUNT > 200 THEN
					RAISE_APPLICATION_ERROR(-20200,'Чрезмерное количество взаиморасчетов. Установите другие критерии соглашения');
				END IF;
				
				FORALL J IN 1..L_CALC.COUNT
					INSERT INTO T_CLI_REST_PAYMENTS(ID,ID_REST,PAY_DATE,PAY_SUM,PENY_SUM,ID_CONTRACT)
					VALUES(SEQ_REST_PAY.NEXTVAL,P_ID_REST,L_CALC(J).DATE_CALC,L_CALC(J).PAY_SUM,L_CALC(J).PENY_SUM,I.ID_CONTRACT);	
			END LOOP;
		
	--	P_CREATE_REST_PAY_DET(P_ID_REST);		
	END;            

---------------------------------------------------------------------------------------------
	
  PROCEDURE P_CALC_RESTRUCT_AMOUNT(P_DATE_START DATE,
						 											 P_SUM_PAY NUMBER,
																	 P_PAY_TO NUMBER,
																	 P_ID_REST NUMBER) 
	 IS
	 L_CNT NUMBER := 0;         
	 L_CALC DEBT_CALC;   
	 L_DEBT NUMBER;     
	 L_PENY NUMBER;                 
	 L_PENY_DATE DATE;
	 L_ABON_GROUP VARCHAR2(100);                                                 
	 L_PENY_SUM NUMBER;	 
	 L_DEBT_SUM NUMBER;
	 L_PENY_CALC VARCHAR2(1);
	BEGIN
    
	  SELECT R.ABON_GROUP,NVL(R.PENY_DATE,TRUNC(SYSDATE,'DD')), R.PENY_CALC, R.REST_DEBT  
		INTO L_ABON_GROUP,L_PENY_DATE, L_PENY_CALC, L_DEBT
	  FROM T_CLI_REST R WHERE R.ID_REST = P_ID_REST;
		
	  L_PENY := PKG_REFIN.F_GET_CLI_PENY(P_ID_CASE => P_ID_REST,P_DATE => L_PENY_DATE,P_GROUP => L_ABON_GROUP);	
		
		IF L_PENY_CALC = 'Y' THEN
			L_DEBT := L_PENY + L_DEBT;
		END IF;
		
		DELETE FROM T_CLI_REST_PAY_DET DET WHERE DET.ID_PAY IN (SELECT P.ID FROM T_CLI_REST_PAYMENTS P WHERE P.ID_REST = P_ID_REST);
	  DELETE FROM T_CLI_REST_PAYMENTS P WHERE P.ID_REST = P_ID_REST;
	  
   	UPDATE T_CLI_REST R
		   SET R.REST_START = P_DATE_START,
		 			 R.PAY_DAY    = P_PAY_TO,
					 R.MONTH_PAY  = P_SUM_PAY,
					 R.TYPE_CALC  = 'PAYMENT',
					 R.PENY_SUM   = L_PENY
		 WHERE R.ID_REST = P_ID_REST;
		
    L_CNT :=  CEIL(L_DEBT/P_SUM_PAY);
	  
		FOR I IN (SELECT DISTINCT D.ID_CONTRACT FROM T_CLI_DEBTS D WHERE D.ID_WORK = P_ID_REST)
			LOOP
				SELECT SUM(D.PENY), SUM(D.DEBT_CREATED) INTO L_PENY_SUM, L_DEBT_SUM 
			  FROM T_CLI_DEBTS D WHERE D.ID_WORK = P_ID_REST AND D.ID_CONTRACT = I.ID_CONTRACT;
				   
				SELECT CASE
								WHEN P_SUM_PAY * (L_DEBT_SUM/L_DEBT) > L_DEBT_SUM -P_SUM_PAY * (L_DEBT_SUM/L_DEBT) * (ROW_NUMBER() OVER(ORDER BY T.PERIOD)-1) THEN  
									   L_DEBT_SUM - P_SUM_PAY * (L_DEBT_SUM/L_DEBT) * (ROW_NUMBER() OVER(ORDER BY T.PERIOD)-1)
								ELSE P_SUM_PAY * (L_DEBT_SUM/L_DEBT)
							 END PAY_SUM,					
							 CASE
								WHEN P_SUM_PAY * (L_PENY_SUM/L_DEBT) > L_PENY_SUM - P_SUM_PAY * (L_PENY_SUM/L_DEBT) * (ROW_NUMBER() OVER(ORDER BY T.PERIOD)-1)  THEN  
									   L_PENY_SUM - P_SUM_PAY * (L_PENY_SUM/L_DEBT) * (ROW_NUMBER() OVER(ORDER BY T.PERIOD)-1)
								ELSE P_SUM_PAY * (L_PENY_SUM/L_DEBT)
							 END PENY_SEW,
							 T.PERIOD
				BULK COLLECT INTO L_CALC					 
				FROM   (SELECT START_DATE + LEVEL - 1 PERIOD
								FROM   (SELECT P_DATE_START START_DATE
															,ADD_MONTHS(P_DATE_START,L_CNT)   END_DATE
												FROM   DUAL)
								CONNECT BY LEVEL <=	 TRUNC(END_DATE) - TRUNC(START_DATE)) T
				WHERE  EXTRACT(DAY FROM PERIOD) = CASE
																					 WHEN P_PAY_TO >= 28 AND EXTRACT(MONTH FROM PERIOD) = 2 THEN EXTRACT(DAY FROM LAST_DAY(PERIOD))
																					 WHEN P_PAY_TO = 31 AND  EXTRACT(DAY FROM LAST_DAY(PERIOD)) = 30 THEN 30
																					 ELSE P_PAY_TO
																					END;
				IF L_CALC.COUNT > 200 THEN
					 RAISE_APPLICATION_ERROR(-20200,'Чрезмерное количество взаиморасчетов. Установите другие критерии соглашения');
				END IF;																	
		
				FORALL J IN 1..L_CALC.COUNT
					INSERT INTO T_CLI_REST_PAYMENTS(ID,ID_REST,PAY_DATE,PAY_SUM,PENY_SUM,ID_CONTRACT)
					VALUES(SEQ_REST_PAY.NEXTVAL,P_ID_REST,L_CALC(J).DATE_CALC,L_CALC(J).PAY_SUM,L_CALC(J).PENY_SUM,I.ID_CONTRACT);
					
		END LOOP;		
		                                   
		UPDATE T_CLI_REST R
		   SET R.REST_END = (SELECT MAX(P.PAY_DATE) FROM T_CLI_REST_PAYMENTS P WHERE P.ID_REST = R.ID_REST)
		 WHERE R.ID_REST = P_ID_REST;	
		 
  	--P_CREATE_REST_PAY_DET(P_ID_REST);		
  END;																										 

-----------------------------------------------------------------------------------
	
	PROCEDURE P_CREATE_RESTRUCT(P_REST_NUM     VARCHAR2
														 ,P_START_DATE   DATE
														 ,P_DEBTS        wwv_flow_global.vc_arr2
														 ,P_DATE_CREATED DATE DEFAULT SYSDATE)
	 IS                      
	 L_ID_REST NUMBER;              
	 L_ID_CWS  NUMBER;
	 L_ID_SEW  NUMBER;    
	 L_TYPE    VARCHAR2(100);
	 L_ID_CLI  NUMBER;      
	 L_GROUP   VARCHAR2(100);
	BEGIN                                                          
		
	  FOR I IN (SELECT DISTINCT FD.ID_CONTRACT FROM T_CLI_FINDOCS FD 
			        WHERE FD.ID_DOC IN (SELECT TO_NUMBER(COLUMN_VALUE) FROM TABLE(P_DEBTS)))
    LOOP
      SELECT CT.CTR_TYPE, CT.ID_CLIENT, NVL(CG.ABON_GROUP,'OTHER')
		  INTO L_TYPE, L_ID_CLI, L_GROUP
      FROM T_CONTRACTS CT, T_CUSTOMER_GROUP CG 
			WHERE CT.ID_CONTRACT = I.ID_CONTRACT
			AND   CT.CUST_GROUP = CG.GROUP_NAME(+);
			
		  IF L_TYPE = 'Основной в ХВС' THEN
        L_ID_CWS := TO_NUMBER(I.ID_CONTRACT);
      ELSE
        L_ID_SEW := TO_NUMBER(I.ID_CONTRACT);
      END IF;                                  
    END LOOP;
	
	  INSERT INTO T_CLI_DEBT_WORK(ID_WORK,ID_CLIENT,TYPE_WORK,DEPT,DATE_CREATED,CREATED_BY)
	  VALUES(SEQ_CLI_DEBT_WORK.NEXTVAL,L_ID_CLI,'RESTRUCT','PRE_TRIAL',P_DATE_CREATED,F$_USR_ID)
		RETURNING ID_WORK INTO L_ID_REST;	  		
		
		INSERT INTO T_CLI_REST(ID_REST,REST_NUM,REST_START,CODE_STATUS,CREATED_BY,DATE_CREATED,
		                       DEBT_DATE,ID_CWS_CONTRACT,ID_SEW_CONTRACT,ABON_GROUP,PENY_DATE)
		VALUES(L_ID_REST,NVL(P_REST_NUM,L_ID_REST||'-ПР'),NVL(P_START_DATE,SYSDATE),'PROJECT',F$_USR_ID,
		       P_DATE_CREATED,P_DATE_CREATED,L_ID_CWS,L_ID_SEW,L_GROUP,TRUNC(SYSDATE,'DD'));
				
		FORALL I IN P_DEBTS.FIRST..P_DEBTS.LAST
			INSERT INTO T_CLI_DEBTS(ID_DEBT,ID_WORK,ID_DOC,DEBT_CREATED,DEBT_CURRENT,Date_Created, Created_By,Id_Contract,Id_Client)
			SELECT SEQ_CLI_CASE_DEBTS.NEXTVAL,L_ID_REST,FD.ID_DOC, FD.DEBT,FD.DEBT, P_DATE_CREATED,f$_usr_id,fd.id_contract, L_ID_CLI		       
			FROM T_CLI_FINDOCS FD WHERE FD.ID_DOC = P_DEBTS(I);
		
		UPDATE T_CLI_REST R 
		   SET R.REST_DEBT = (SELECT SUM(FD.DEBT) FROM T_CLI_FINDOCS FD,T_CLI_DEBTS RD 
			                    WHERE RD.ID_DOC = FD.ID_DOC AND RD.ID_WORK = L_ID_REST)
		 WHERE R.ID_REST = L_ID_REST;							
		
		UPDATE T_CLI_DEBT_WORK W
		  SET  W.DEBT_CREATED = (SELECT R.REST_DEBT FROM T_CLI_REST R WHERE R.ID_REST = W.ID_WORK),
			     W.CURRENT_DEBT = (SELECT R.REST_DEBT FROM T_CLI_REST R WHERE R.ID_REST = W.ID_WORK)
		 WHERE W.ID_WORK = L_ID_REST;	
		 
		PKG_EVENTS.P_CREATE_CLI_EVENT(P_ID_WORK => L_ID_REST,P_CODE => 'RESTRUCT_CREATED'); 	  			
	END;                                         

----------------------------------------------------------------------------------------------
	
  PROCEDURE P_UPDATE_REST_DEBTS(P_ID_REST     NUMBER
		                           ,P_DEBTS       wwv_flow_global.vc_arr2)
		IS 
		L_ID_CWS  NUMBER;
	  L_ID_SEW  NUMBER;
		L_TYPE    VARCHAR2(100);  
		L_ID_CLI  NUMBER; 
	BEGIN
			 
		FOR I IN (SELECT DISTINCT FD.ID_CONTRACT FROM T_CLI_FINDOCS FD 
								WHERE FD.ID_DOC IN (SELECT TO_NUMBER(COLUMN_VALUE) FROM TABLE(P_DEBTS)))
			LOOP
				SELECT CT.CTR_TYPE, CT.ID_CLIENT INTO L_TYPE, L_ID_CLI FROM T_CONTRACTS CT WHERE CT.ID_CONTRACT = I.ID_CONTRACT;
				
				IF L_TYPE = 'Основной в ХВС' THEN
					L_ID_CWS := TO_NUMBER(I.ID_CONTRACT);
				ELSE
					L_ID_SEW := TO_NUMBER(I.ID_CONTRACT);
				END IF;                                  
			END LOOP;
		 																														 		
		 DELETE FROM T_CLI_REST_PAYMENTS P WHERE P.ID_REST = P_ID_REST;
		 DELETE FROM T_CLI_DEBTS D WHERE D.ID_WORK = P_ID_REST;
     
	FORALL I IN P_DEBTS.FIRST..P_DEBTS.LAST
			INSERT INTO T_CLI_DEBTS(ID_DEBT,ID_WORK,ID_DOC,DEBT_CREATED,DEBT_CURRENT,Date_Created, Created_By,Id_Contract,Id_Client)
			SELECT SEQ_CLI_CASE_DEBTS.NEXTVAL,P_ID_REST,FD.ID_DOC, FD.DEBT,FD.DEBT, SYSDATE,f$_usr_id,fd.id_contract, L_ID_CLI		       
			FROM T_CLI_FINDOCS FD WHERE FD.ID_DOC = P_DEBTS(I);
		
	   UPDATE T_CLI_REST R 
		   SET R.REST_DEBT = (SELECT SUM(FD.DEBT) FROM T_CLI_FINDOCS FD,T_CLI_DEBTS RD 
			                    WHERE RD.ID_DOC = FD.ID_DOC AND RD.ID_WORK = R.ID_REST)
			    ,R.ID_CWS_CONTRACT = L_ID_CWS
					,R.ID_SEW_CONTRACT = L_ID_SEW                                         
					,R.DEBT_DATE = SYSDATE                                                                     
		 WHERE R.ID_REST = P_ID_REST;								 					     
	END; 

---------------------------------------------------------------------------------
	
 /* PROCEDURE P_CREATE_REST_PAY_DET(P_ID_REST NUMBER)
		IS                                                                                                                
		L_BAL_PAY  NUMBER;
		L_BAL_PENY NUMBER;
		L_REST_DEBTS REST_DEBTS := REST_DEBTS();
	BEGIN          
		FOR J IN (SELECT DISTINCT D.ID_CONTRACT FROM T_CLI_DEBTS D WHERE D.ID_WORK = P_ID_REST)
		LOOP
			SELECT CCD.ID_DEBT, FD.DOC_DATE, CCD.DEBT_CURRENT,CCD.PENY
			BULK COLLECT INTO L_REST_DEBTS
			FROM T_CLI_REST R, 
					 T_CLI_DEBTS CCD, 
					 T_CLI_FINDOCS FD
			WHERE R.ID_REST = P_ID_REST 
			AND R.ID_REST = CCD.ID_WORK 
			AND FD.ID_DOC = CCD.ID_DOC            
			AND CCD.ID_CONTRACT = J.ID_CONTRACT
			AND CCD.DEBT_CURRENT > 0
			ORDER BY FD.DOC_DATE, CCD.ID_DEBT;						
			
			FOR I IN (SELECT P.ID, P.PAY_SUM, P.PENY_SUM FROM T_CLI_REST_PAYMENTS P 
				        WHERE P.ID_REST = P_ID_REST AND P.ID_CONTRACT = J.ID_CONTRACT
							  ORDER BY P.PAY_DATE)
			LOOP                 
				L_BAL_PAY  := I.PAY_SUM;
				L_BAL_PENY := I.PENY_SUM;
				FOR J IN 1..L_REST_DEBTS.COUNT 
				LOOP                            
					IF L_REST_DEBTS(J).DEBT <= 0 THEN 
						CONTINUE; 
					ELSE
						 IF L_REST_DEBTS(J).DEBT + L_REST_DEBTS(J).PENY >= L_BAL_PAY + L_BAL_PENY THEN
							 INSERT INTO T_CLI_REST_PAY_DET(ID,ID_DEBT,ID_PAY,SUM_PAY)
							 VALUES (SEQ_REST_PAY_DET.NEXTVAL,L_REST_DEBTS(J).ID_DEBT, I.ID,L_BAL_PAY,L_BAL_PENY);
							 L_BAL_PAY  := L_REST_DEBTS(J).DEBT - L_BAL_PAY;
							 L_BAL_PENY := L_REST_DEBTS(J).PENY - L_BAL_PENY;
							 L_REST_DEBTS(J).DEBT := L_BAL_PAY;
							 L_REST_DEBTS(J).PENY := L_BAL_PENY;
							 EXIT;
						 ELSE                          
							 INSERT INTO T_CLI_REST_PAY_DET(ID,ID_DEBT,ID_PAY,SUM_PAY)
							 VALUES (SEQ_REST_PAY_DET.NEXTVAL,L_REST_DEBTS(J).ID_DEBT, I.ID,L_REST_DEBTS(J).DEBT);
							 L_BALANCE := L_BALANCE - L_REST_DEBTS(J).DEBT;
							 L_REST_DEBTS(J).DEBT := 0;
						 END IF;
					END IF; 
				END LOOP;
			END LOOP; 
		END LOOP;
	END; */

---------------------------------------------------------------------------
	
	PROCEDURE P_RECALC_PAYMENT(P_ID_REST NUMBER,
														 P_DATA    PAY_ROW)
	  IS                               
		L_ID_REST NUMBER;
		L_PAYMENT NUMBER;
		L_OLD_PAY PAY_ROW;	
		L_PENY_SUM NUMBER;
		L_DEBT_SUM NUMBER;	
	BEGIN              

	  IF P_DATA.ID_PAY_CWS IS NOT NULL THEN
				UPDATE T_CLI_REST_PAYMENTS S
					 SET S.PAY_SUM = NVL(P_DATA.PAY_CWS,S.PAY_SUM)
							,S.PAY_DATE = NVL(P_DATA.DATE_PAY,S.PAY_DATE)
							,S.PENY_SUM = NVL(P_DATA.PENY_CWS,S.PENY_SUM)
							,S.IS_MANUAL = 'Y' 
				 WHERE S.ID = P_DATA.ID_PAY_CWS;
		END IF;			                        
		
		IF P_DATA.ID_PAY_SEW IS NOT NULL THEN
				UPDATE T_CLI_REST_PAYMENTS S
					 SET S.PAY_SUM = NVL(P_DATA.PAY_SEW,S.PAY_SUM)
							,S.PAY_DATE = NVL(P_DATA.DATE_PAY,S.PAY_DATE)
							,S.PENY_SUM = NVL(P_DATA.PENY_SEW,S.PENY_SUM)
							,S.IS_MANUAL = 'Y' 
				 WHERE S.ID = P_DATA.ID_PAY_SEW;
		END IF;		
		
	  FOR I IN (SELECT DISTINCT D.ID_CONTRACT FROM T_CLI_DEBTS D WHERE D.ID_WORK = P_ID_REST)
		LOOP
			SELECT SUM(D.PENY), SUM(D.DEBT_CREATED) INTO L_PENY_SUM, L_DEBT_SUM 
			FROM T_CLI_DEBTS D WHERE D.ID_WORK = P_ID_REST AND D.ID_CONTRACT = I.ID_CONTRACT;   
				 
			SELECT L_DEBT_SUM 
						 - 
						 (SELECT NVL(SUM(P.PAY_SUM),0) FROM T_CLI_REST_PAYMENTS P 
							WHERE P.IS_MANUAL = 'Y' AND P.ID_REST = P_ID_REST
							AND   P.ID_CONTRACT = I.ID_CONTRACT),
						 L_PENY_SUM 	                        
						 -
						 (SELECT NVL(SUM(P.PENY_SUM),0) FROM T_CLI_REST_PAYMENTS P 
							WHERE P.IS_MANUAL = 'Y' AND P.ID_REST = P_ID_REST
							AND   P.ID_CONTRACT = I.ID_CONTRACT)
			INTO L_DEBT_SUM, L_PENY_SUM
			FROM DUAL;
		  
			FOR J IN (SELECT  S.ID,
												FLOOR(L_DEBT_SUM / (COUNT(*) OVER()) * 100) / 100 + CASE
													 WHEN ROW_NUMBER() OVER(ORDER BY S.ID) <=
																(L_DEBT_SUM - (FLOOR(L_DEBT_SUM / (COUNT(*) OVER()) * 100) / 100) *
																 (COUNT(*) OVER())) / 0.01 THEN		0.01
													 ELSE		0
												 END PAYMENT   --РАСЧЕТ СУММЫ ПЛАТЕЖА, МАХИНАЦИИ С КОПЕЙКАМИ 
												,FLOOR(L_PENY_SUM / (COUNT(*) OVER()) * 100) / 100 + CASE
													 WHEN ROW_NUMBER() OVER(ORDER BY S.ID) <=
																(L_PENY_SUM - (FLOOR(L_PENY_SUM / (COUNT(*) OVER()) * 100) / 100) *
																 (COUNT(*) OVER())) / 0.01 THEN		0.01
													 ELSE		0
												 END PENY  
								FROM T_CLI_REST_PAYMENTS  S 
								WHERE S.ID_REST = P_ID_REST
								AND   s.id_contract = i.id_contract
								AND S.IS_MANUAL = 'N')
			LOOP
				UPDATE T_CLI_REST_PAYMENTS P 
					 SET P.PAY_SUM = J.PAYMENT,
					     P.PENY_SUM = J.PENY
				 WHERE P.ID = J.ID;
			END LOOP;
		END LOOP;
		--P_CREATE_REST_PAY_DET(L_ID_REST);
	END;             

-----------------------------------------------------------------------------------------------------
	
	PROCEDURE P_CHANGE_STATUS(P_ID_REST NUMBER, P_CODE_STATUS VARCHAR2)
		 IS
	BEGIN
		 UPDATE T_CLI_REST R SET R.CODE_STATUS = P_CODE_STATUS WHERE R.Id_Rest = P_ID_REST;
	END;                                                                                               

-----------------------------------------------------------------------------------------------------

  PROCEDURE P_BEGIN_WORK(P_ID_REST NUMBER, P_NUM_REST VARCHAR2)
		IS
	BEGIN
		UPDATE T_CLI_REST R SET R.REST_NUM = P_NUM_REST
		WHERE R.ID_REST = P_ID_REST;
		P_CHANGE_STATUS(P_ID_REST,'VALID');      
		PKG_EVENTS.P_CREATE_CLI_EVENT(P_ID_REST,'RESTRUCT_VALID');
	END;

----------------------------------------------------------------------------------------------------
  
  PROCEDURE P_DELETE_REST(P_ID_REST NUMBER)
		IS
	BEGIN
		DELETE FROM T_CLI_EVENTS EV WHERE EV.ID_WORK = P_ID_REST;
		DELETE FROM T_CLI_REST_PAYMENTS P WHERE P.ID_REST = P_ID_REST;
		DELETE FROM T_CLI_DEBTS D WHERE D.ID_WORK = P_ID_REST;
		DELETE FROM T_CLI_REST R WHERE R.ID_REST = P_ID_REST;
		DELETE FROM T_CLI_DEBT_WORK W WHERE W.ID_WORK = P_ID_REST;
	END;															
	      		
END PKG_RESTRUC;
/

