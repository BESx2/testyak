i�?CREATE OR REPLACE PACKAGE LAWMAIN.PKG_MAIL_05
IS
/* Автор: Бородулин Е.С.
   Дата:  21.12.2016
	 Назначение: Пакет отсылает email уведомление на отключение
*/

   PROCEDURE p_send_mail(P_ID_FOLDER NUMBER, P_ATTACH VARCHAR2 DEFAULT NULL);

END;
/

CREATE OR REPLACE PACKAGE BODY LAWMAIN.PKG_MAIL_05
IS

	G$HTML VARCHAR2(32000);

  PROCEDURE p_send_mail( P_ID_FOLDER NUMBER, P_ATTACH VARCHAR2)
        IS
			L_RET    NUMBER;
			L_REP    LAWSUP.PKG_FILES.REC_DOC;
    BEGIN                                                        
			
				
				FOR i IN (SELECT CT.CTR_NUMBER, UL.PHONE, S.ID_WORK,
																 PKG_CLIENTS.F_GET_EMAIL(CT.ID_CLIENT) EMAIL
													FROM   T_CLI_SHUTDOWN S,
																 T_CONTRACTS CT,       
																 T_USER_LIST UL												 
													WHERE  S.ID_CONTRACT = CT.ID_CONTRACT
													AND    S.CURATOR = UL.ID
													AND    S.ID_FOLDER = P_ID_FOLDER)
            LOOP     
              IF I.EMAIL IS NULL THEN
								CONTINUE;															 
							END IF;
								
							G$HTML := PKG_PREF.F$HTML('SHUT_NOTIF');
							G$HTML := REPLACE(G$HTML,'#CTRNUMBER#',I.Ctr_Number); 
							G$HTML := REPLACE(G$HTML,'#PHONE#',I.PHONE); -- текущая дата.

           
            L_RET := PKG_MAIL.F_MAKE_EMAIL(P_HEADER => PKG_PREF.F$C1('SHUT_NOTIF')
                                           ,P_FROM  => PKG_PREF.F$C1('USER_MAIL') -- do not change !!!! if change => change password in sup.pkg_mail!!!
                                           ,P_BODY  => G$HTML
                                          );
            PKG_MAIL.P_ATTACHE_EMAIL_ADDR(P_EMAIL      => I.EMAIL
                                         ,P_ID_MAIL    => L_RET
                                         ,P_EMAIL_TYPE => PKG_MAIL.LIST_EMAIL_TYPE.TO_);
																				 
				FOR J IN (SELECT COLUMN_VALUE FROM TABLE(APEX_STRING.split(P_ATTACH,':')))																 
				LOOP
						L_REP := PKG_CONTROLLER.P_RETURN_BLOB(P_REP_CODE => J.COLUMN_VALUE
						                                     ,P_PAR_01 => I.ID_WORK);    
																								 
						PKG_MAIL.P_ATTACHE_FILE(P_ID_MAIL => L_RET
						                       ,P_BLOB => L_REP.P_BLOB
																	 ,P_FILE_NAME => L_REP.P_FILE_NAME);																		 
				END LOOP;

        PKG_MAIL.P_SEND(P_ID => L_RET,P_SEND_TYPE => PKG_MAIL.LIST_TYPE_SEND.LTE);
				PKG_SEND_MAIL.P_INSERT_CLI_SHUT_MAIL(I.ID_WORK,L_RET,'SHUT_NOTIF' );
 				COMMIT;
       END LOOP;
    END;    		
END;
/

