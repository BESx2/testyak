i�?create or replace package lawmain.PKG_COURT_EXEC is

  -- Author  : EUGEN
  -- Created : 06.03.2019 8:58:02
  -- Purpose :  ДЛЯ СОЗДАНИЯ И РЕДАКТИРОВАНИЯ ДВИЖЕНИЙ В СУДЕ
  
  PROCEDURE P_CREATE_CLI_EXEC_STAGE(P_ID_EXEC NUMBER,
		                                P_CODE    VARCHAR2,
																		P_COMMENT VARCHAR2,
																		P_DATE    DATE,
                                    P_CALEND  VARCHAR2 DEFAULT 'N');
																		
	PROCEDURE P_UPDATE_CLI_EXEC_STAGE(P_ID_EXEC_STAGE NUMBER,
		                                P_CODE          VARCHAR2,
																		P_COMMENT       VARCHAR2,
																		P_DATE          DATE); 
																		
  PROCEDURE P_UPDATE_COURT_EXEC(P_ID_EXEC NUMBER,
		                            P_DATE_EXEC DATE,
															  P_TYPE_EXEC VARCHAR2,
                                P_NUM_EXEC  VARCHAR2,
																P_ID_JUDGE  NUMBER,
																P_ID_COURT  NUMBER);	
																
																
  PROCEDURE P_CREATE_COURT_EXEC(P_ID_CASE NUMBER,
		                            P_DATE_EXEC DATE,
																P_TYPE_EXEC VARCHAR2,
																P_NUM_EXEC  VARCHAR2,
																P_ID_JUDGE  NUMBER DEFAULT NULL,
																P_ID_COURT  NUMBER);	
																
	PROCEDURE P_FINISH_COURT_EXEC(P_ID_EXEC   NUMBER,
		                            P_DATE_END  DATE,
																P_END_CODE  VARCHAR2,
																P_COMMENT   VARCHAR2 DEFAULT NULL,
																P_DEBT      NUMBER DEFAULT NULL,
																P_GOV_TOLL  NUMBER DEFAULT NULL,
																P_PENY      NUMBER DEFAULT NULL,
																P_PENY_DATE DATE DEFAULT NULL);																																													
	
  PROCEDURE P_CLOSE_LAST_EXEC(P_ID_CASE NUMBER);	                                  																	

end PKG_COURT_EXEC;
/

create or replace package body lawmain.PKG_COURT_EXEC is
 
  PROCEDURE P_CREATE_CLI_EXEC_STAGE(P_ID_EXEC NUMBER,
                                    P_CODE    VARCHAR2,
																		P_COMMENT VARCHAR2,
																		P_DATE    DATE,
																		P_CALEND  VARCHAR2 DEFAULT 'N')
		IS                            
		L_ID_STAGE   NUMBER;            
		L_STAGE_NAME VARCHAR2(200); 
		L_NUM_EXEC   VARCHAR2(200);
		L_CASE_NAME  VARCHAR2(200);
		L_ID_CAL     NUMBER;                
		L_ID_EXEC    NUMBER;   
		L_CLI_NAME   VARCHAR2(500);
		L_CURATOR    VARCHAR2(500);
	BEGIN               
	   SELECT ST.ID_STAGE, ST.NAME_STAGE INTO L_ID_STAGE, L_STAGE_NAME
		 FROM T_COURT_STAGES ST WHERE ST.CODE_STAGE = P_CODE;	
	
		 INSERT INTO T_CLI_COURT_EXEC_STAGE(ID_EXEC_STAGE,ID_STAGE,ID_EXEC,COMMENTARY
		                                   ,CREATED_BY,DATE_CREATED,DATE_STAGE)
		 VALUES(SEQ_CLI_STAGE.NEXTVAL, L_ID_STAGE,P_ID_EXEC,P_COMMENT,F$_USR_ID,SYSDATE, P_DATE)
		 RETURNING ID_EXEC_STAGE INTO L_ID_EXEC;                   
		 
		 IF P_CALEND = 'Y' THEN						      
				SELECT E.NUM_EXEC, CC.CASE_NAME,CC.CLI_ALT_NAME, INITCAP(PKG_USERS.F_GET_FIO_BY_ID(CC.CURATOR_COURT,'Y')) 
				INTO   L_NUM_EXEC, L_CASE_NAME, L_CLI_NAME, L_CURATOR
				FROM T_CLI_COURT_EXEC E, V_CLI_CASES CC
				WHERE E.ID = P_ID_EXEC AND E.ID_CASE = CC.ID_CASE;
		      
				L_ID_CAL := PKG_CALENDAR.F_CREATE_EVENT(P_EVENT_NAME => L_STAGE_NAME
																							 ,P_START_DATE => TO_DATE(P_DATE,'DD.MM.YYYY HH24:MI:SS')
																							 ,P_END_DATE   => TO_DATE(P_DATE||' 23:59:59','DD.MM.YYYY HH24:MI:SS')
																							 ,P_COMMENTARY => 'По делу '||L_NUM_EXEC||' ('||L_CASE_NAME||'), абонента '||L_CLI_NAME||', исп. '||l_curator
																							 ,P_NOTIF      => 'Y');       
																							                          
				INSERT INTO T_CALEND_COURT_EXEC(ID,ID_CALENDAR,ID_EXEC)																			 
				VALUES(SEQ_MAIN.NEXTVAL,L_ID_CAL,L_ID_EXEC);
		 END IF;
		 
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			RAISE_APPLICATION_ERROR(-20200,'Не правильно указан код события. Обратитесь к администратору системы');			 
	END;								

----------------------------------------------------------------------------------------------------------------
	
	PROCEDURE P_UPDATE_CLI_EXEC_STAGE(P_ID_EXEC_STAGE NUMBER,
		                                P_CODE          VARCHAR2,
																		P_COMMENT       VARCHAR2,
																		P_DATE          DATE)
		IS
		L_ID_STAGE NUMBER;
	BEGIN                                                                                       
	   SELECT ST.ID_STAGE INTO L_ID_STAGE FROM T_COURT_STAGES ST WHERE ST.CODE_STAGE = P_CODE;	
		 
		 UPDATE T_CLI_COURT_EXEC_STAGE ST
		    SET ST.COMMENTARY = P_COMMENT,
				    ST.ID_STAGE   = L_ID_STAGE,
						ST.DATE_STAGE = P_DATE
		  WHERE ST.ID_EXEC_STAGE = P_ID_EXEC_STAGE;
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			RAISE_APPLICATION_ERROR(-20200,'Не правильно указан код события. Обратитесь к администратору системы');		
	END;  
	
---------------------------------------------------------------------------------------------------------------------


  
  PROCEDURE P_CREATE_COURT_EXEC(P_ID_CASE NUMBER,
		                            P_DATE_EXEC DATE,
																P_TYPE_EXEC VARCHAR2,
																P_NUM_EXEC  VARCHAR2,
																P_ID_JUDGE  NUMBER DEFAULT NULL,
																P_ID_COURT  NUMBER)
  	IS
  BEGIN
		INSERT INTO T_CLI_COURT_EXEC(ID,ID_CASE,NUM_EXEC,TYPE_EXEC,DATE_EXEC,CREATED_BY,DATE_CREATED,ID_JUDGE,ID_COURT)
		VALUES (SEQ_COURT_EXEC.NEXTVAL,P_ID_CASE,P_NUM_EXEC,P_TYPE_EXEC,P_DATE_EXEC,F$_USR_ID,SYSDATE,P_ID_JUDGE,P_ID_COURT);
	END;																                                                                        
	
	---------------------------------------------------------------------------------------------------
	
  PROCEDURE P_UPDATE_COURT_EXEC(P_ID_EXEC NUMBER,
		                            P_DATE_EXEC DATE,
															  P_TYPE_EXEC VARCHAR2,
                                P_NUM_EXEC  VARCHAR2,
																P_ID_JUDGE  NUMBER,
																P_ID_COURT  NUMBER)
  	IS
	BEGIN
	  UPDATE T_CLI_COURT_EXEC C 
		SET C.NUM_EXEC = P_NUM_EXEC,
		    C.DATE_EXEC = P_DATE_EXEC,
				C.TYPE_EXEC = P_TYPE_EXEC,
				C.ID_JUDGE  = P_ID_JUDGE,
				C.MODIFIED_BY = F$_USR_ID,
				C.DATE_MODIFIED = SYSDATE,
				C.ID_COURT = P_ID_COURT
		WHERE C.ID = P_ID_EXEC;
	END;				   
	
------------------------------------------------------------------------------------------------------

  PROCEDURE P_FINISH_COURT_EXEC(P_ID_EXEC   NUMBER,
		                            P_DATE_END  DATE,
																P_END_CODE  VARCHAR2,
																P_COMMENT   VARCHAR2 DEFAULT NULL,
																P_DEBT      NUMBER DEFAULT NULL,
																P_GOV_TOLL  NUMBER DEFAULT NULL,
																P_PENY      NUMBER DEFAULT NULL,
																P_PENY_DATE DATE DEFAULT NULL)    
  	IS                       
		L_ID_CASE NUMBER;
	BEGIN
		UPDATE T_CLI_COURT_EXEC E
		   SET E.DATE_END  = P_DATE_END,
			     E.END_CODE  = P_END_CODE,
					 E.EXEC_COMM = P_COMMENT,
					 E.COURT_DEBT= P_DEBT,
					 E.COURT_PENY_DATE = P_PENY_DATE,
					 E.COURT_PENY = P_PENY,
					 E.COURT_TOLL = P_GOV_TOLL,
					 E.FLG_ACTIVE = 'N'
	 	 WHERE E.ID = P_ID_EXEC
		 RETURNING E.ID_CASE INTO L_ID_CASE;        
		 
		 UPDATE T_CLI_CASES S
		    SET S.PENY_COURT = P_PENY
			WHERE S.ID_CASE = L_ID_CASE;
		 
	END;			
	
-----------------------------------------------------------------------------------------
   
  PROCEDURE P_CLOSE_LAST_EXEC(P_ID_CASE NUMBER)
		IS                      
		L_ID NUMBER;
	BEGIN
		 SELECT MAX(CC.ID) INTO L_ID FROM T_CLI_COURT_EXEC CC WHERE CC.ID_CASE = P_ID_CASE;
		 IF L_ID IS NOT NULL THEN
   		 UPDATE T_CLI_COURT_EXEC E SET E.FLG_ACTIVE = 'N', E.DATE_END = SYSDATE, E.END_CODE = 'RETURN' WHERE E.ID = L_ID;
		 END IF;
	END;													
	
																								
end PKG_COURT_EXEC;
/

