i�?create or replace package lawmain.PKG_BILLING is

  -- Author  : EUGEN
  -- Created : 02.11.2016 10:18:29
  -- Purpose : Пакет для работы с билинговыми системами.
  
	--Ошибка
	G$BILLING_ERROR EXCEPTION;	
	
	TYPE UID IS TABLE OF VARCHAR2(36);
	
  PROCEDURE P_GET_DISTRICTS;
	
	PROCEDURE P_GET_BANKS;
	
	PROCEDURE P_GET_CLIENTS(P_CLIENT_ID NUMBER DEFAULT NULL);  
	
	PROCEDURE P_GET_CONTRACTS(P_CLIENT_ID NUMBER DEFAULT NULL);	 
														
	PROCEDURE P_GET_FINDOCS_CONS(P_DATE_BEG    DATE DEFAULT NULL
																 ,P_DATE_END    DATE DEFAULT NULL);	
																 
	PROCEDURE P_GET_FINDOCS_CONS(P_DATE_BEG    DATE DEFAULT NULL
															,P_DATE_END    DATE DEFAULT NULL
															,P_ID_CONTRACT NUMBER); 
	
	PROCEDURE P_CREATE_FINDOCS(P_ID_CONTRACT NUMBER );	
	PROCEDURE P_CREATE_FINDOCS;
	
	FUNCTION F_GET_PRINT_DOC(P_PRINT_CODE VARCHAR2,
	                            P_EXT_ID     VARCHAR2,
															P_DOC_TYPE   VARCHAR2) RETURN LAWSUP.PKG_FILES.REC_DOC;
	
	PROCEDURE P_GET_IFNS;														 																									 																							
	
	FUNCTION F_REGISTER_NEW_CLAIM(P_ID_IFNS NUMBER,
	                            P_SUM     NUMBER,
															P_COMMENT VARCHAR2,
															P_USER    VARCHAR2) RETURN VARCHAR2;
															
	--FUNCTION F_GET_PROHIBITION_DATE RETURN DATE;		
	
	PROCEDURE P_GET_FINDOC_DETAILS(P_ID_CLIENT NUMBER);		
	
	PROCEDURE P_GET_FINDOC_DETAILS(P_UID UID); 									
												
end PKG_BILLING;
/

create or replace package body lawmain.PKG_BILLING is

  G$URL  VARCHAR2(500);
	G$USER VARCHAR2(500);
	G$PWD  VARCHAR2(500);

------------------------------------------------------------------------------------------

PROCEDURE P_GET_DISTRICTS 
	IS
	L_CLOB CLOB; 
	L_URL  VARCHAR2(500);
	L_XML  XMLTYPE;
BEGIN
	L_URL  := G$URL || '/Areas';         
	L_CLOB := PKG_WEB.F_SEND_REQUEST(P_URL => L_URL, P_METHOD => 'GET',
																	 P_USER => G$USER, P_PWD => G$PWD);
	L_XML := XMLTYPE(L_CLOB);																 
	MERGE INTO T_DISTRICT D
	USING (SELECT C.ID, C.AREA
				 FROM   XMLTABLE('/areas/area' PASSING L_XML
				                 COLUMNS ID   VARCHAR2(100) PATH 'UID',
												         AREA VARCHAR2(100) PATH 'Name') C) T
	ON (D.EXT_ID = T.ID)
	WHEN NOT MATCHED THEN
		INSERT (D.ID_DISTRICT, D.DISTR_NAME, D.EXT_ID)
		VALUES (SEQ_MAIN.NEXTVAL, T.AREA, T.ID);

END P_GET_DISTRICTS;

-------------------------------------------------------------------------------------------

PROCEDURE P_GET_BANKS IS
	L_CLOB CLOB;    
	L_URL  VARCHAR2(500);
BEGIN
	L_URL  := G$URL || '/Bank';
	L_CLOB := PKG_WEB.F_SEND_REQUEST(P_URL => L_URL, P_METHOD => 'GET',
																	 P_USER => G$USER, P_PWD => G$PWD);
	MERGE INTO T_BANKS B
	USING (SELECT *
				 FROM   XMLTABLE('/Banks/Bank' 
				                PASSING XMLTYPE(L_CLOB) 
												COLUMNS ID     VARCHAR2(100) PATH 'UID'
												      ,NameBank VARCHAR2(500) PATH 'NameBank'
															,KS_ACC   VARCHAR2(50)  PATH 'CorrespondentAccount'
															,BankCity VARCHAR2(100) PATH 'BankCity'
															,ADDRESS  VARCHAR2(500) PATH 'BankAddress'
															,Bik      VARCHAR2(100) PATH 'Bik')) T
	ON (B.EXT_ID = T.ID)
	WHEN NOT MATCHED THEN
		INSERT (ID_BANK,BANK_NAME,COR_ACC,BIK,CITY,ADDRESS,EXT_ID)
		VALUES (SEQ_MAIN.NEXTVAL,T.NAMEBANK,T.KS_ACC,T.BIK,T.BANKCITY,T.ADDRESS,T.ID);

END P_GET_BANKS;

--------------------------------------------------------------------------------------------	

PROCEDURE P_GET_CLIENTS(P_CLIENT_ID NUMBER DEFAULT NULL) 
	IS
	L_CLOB        CLOB;    
	L_URL  VARCHAR2(500);
	L_EXT_ID      VARCHAR2(100);
BEGIN
	-- Достаем URL  
	L_URL := G$URL || '/Clients';  
	IF P_CLIENT_ID IS NOT NULL THEN
		SELECT S.EXT_ID INTO L_EXT_ID FROM T_CLIENTS S WHERE S.ID_CLIENT = P_CLIENT_ID;
		L_URL := L_URL||'?id='||L_EXT_ID;
	END IF;
	--Парсим XML
	L_CLOB := PKG_WEB.F_SEND_REQUEST(P_URL => L_URL, P_METHOD => 'GET',
																	 P_USER => G$USER, P_PWD => G$PWD);    

--ОСНОВНАЯ ИНФОРМАЦИЯ
	MERGE INTO T_CLIENTS CLI
	USING (SELECT T.*
				 FROM   XMLTABLE('/Clients/Client' PASSING XMLTYPE(L_CLOB) 
				         COLUMNS ID        VARCHAR2(100) PATH 'UID',
												 CODE       VARCHAR2(100) PATH 'Code',
												 SHORTNAME  VARCHAR2(500) PATH 'Name',
												 FULLNAME   VARCHAR2(500) PATH 'FullName',
												 INN        VARCHAR2(100) PATH 'INN',
												 KPP        VARCHAR2(100) PATH 'KPP',
												 OKPO       VARCHAR2(100) PATH 'OKPO',
												 OGRN       VARCHAR2(500) PATH 'OGRN',
												 KIND       VARCHAR2(50) PATH 'yurfizlitso',
												 COMMENTARY VARCHAR2(500) PATH 'Comment',
												 CUST_GROUP VARCHAR2(100) PATH 'CustomerCategory'
												,LAST_NAME     VARCHAR2(1000) PATH 'Surname'
												,FIRST_NAME    VARCHAR2(1000) PATH 'FirstName'
												,SECOND_NAME   VARCHAR2(1000) PATH 'MiddleName'
												,BIRTHDATE     VARCHAR2(1000) PATH 'DateBirth'
												,BIRTH_PACE    VARCHAR2(1000) PATH 'PlaceBirth'
												,PASS_NUM      VARCHAR2(1000) PATH 'Number'
												,PASS_SERIES   VARCHAR2(1000) PATH 'Series'
												,PASS_DATE     VARCHAR2(1000) PATH 'DateIssue'
												,PASS_ISSUER   VARCHAR2(1000) PATH 'KemVydan'
												,PASS_DEP      VARCHAR2(1000) PATH 'UnitCode'
												,PASS_PERIOD   VARCHAR2(1000) PATH 'Period'
												,DOC_TYPE      VARCHAR2(1000) PATH 'view') T) X
	ON (CLI.EXT_ID = X.ID)
	WHEN MATCHED THEN
		UPDATE
		SET    CLI.CLI_NAME     = X.FULLNAME
					,CLI.CLI_ALT_NAME = X.SHORTNAME
					,CLI.INN          = X.INN
					,CLI.KPP          = X.KPP
					,CLI.OKPO         = X.OKPO
					,CLI.CLI_KIND     = X.KIND
					,CLI.DATE_UPD     = SYSDATE
					,CLI.OGRN         = X.OGRN
					,CLI.CUST_GROUP   = X.CUST_GROUP
					,CLI.LAST_NAME    =  X.LAST_NAME
					,CLI.FIRST_NAME   =  X.FIRST_NAME
					,CLI.SECOND_NAME  =  X.SECOND_NAME
					,CLI.BIRTHDATE    =  TO_DATE(X.BIRTHDATE,'DD.MM.YYYY')
					,CLI.BIRTH_PACE   =  X.BIRTH_PACE
					,CLI.PASS_NUM     =  X.PASS_NUM
					,CLI.PASS_SERIES  =  X.PASS_SERIES
					,CLI.PASS_DATE    =  TO_DATE(X.PASS_DATE,'DD.MM.YYYY')
					,CLI.PASS_ISSUER  =  X.PASS_ISSUER
					,CLI.PASS_DEP     =  X.PASS_DEP
					,CLI.PASS_PERIOD  =  TO_DATE(X.PASS_PERIOD,'DD.MM.YYYY')
					,CLI.DOC_TYPE     =  X.DOC_TYPE        
	WHEN NOT MATCHED THEN
		INSERT (ID_CLIENT, CLI_NAME, CLI_ALT_NAME, INN, KPP, OKPO, JOB_NUM,
			      DATE_LOAD, EXT_ID, OGRN, CUST_GROUP, CLI_KIND,LAST_NAME,   
            FIRST_NAME,SECOND_NAME,BIRTHDATE,BIRTH_PACE,PASS_NUM,
            PASS_SERIES,PASS_DATE,PASS_ISSUER,PASS_DEP,PASS_PERIOD,DOC_TYPE)
		VALUES (SEQ_CLIENTS.NEXTVAL, X.FULLNAME, X.SHORTNAME, X.INN, X.KPP, X.OKPO,
			      TRUNC(DBMS_RANDOM.VALUE(1, 21)), SYSDATE, X.id, X.OGRN, X.CUST_GROUP, X.KIND,
						X.LAST_NAME,X.FIRST_NAME,X.SECOND_NAME,TO_DATE(X.BIRTHDATE,'DD.MM.YYYY'),X.BIRTH_PACE,X.PASS_NUM,
            X.PASS_SERIES,TO_DATE(X.PASS_DATE,'DD.MM.YYYY'),X.PASS_ISSUER,X.PASS_DEP,TO_DATE(X.PASS_PERIOD,'DD.MM.YYYY'),X.DOC_TYPE);        
											
--ПРЕДСТАВИТЕЛИ КЛИЕНТОВ
	MERGE INTO T_CLI_REPRES CC
	USING (SELECT (SELECT CLI.ID_CLIENT FROM T_CLIENTS CLI WHERE CLI.EXT_ID = M.cl_id) ID_CLIENT,
	              C.* 	               
	       FROM XMLTABLE('/Clients/Client' PASSING XMLTYPE(L_CLOB)
	                     COLUMNS cl_id VARCHAR2(100) PATH 'UID',
				 							 CONTACTS  XMLTYPE PATH 'ContactFaces') M
						 ,XMLTABLE('/ContactFaces/ContactFace' PASSING M.CONTACTS
						           COLUMNS id VARCHAR2(100)  PATH 'UID'
                              ,Code VARCHAR2(50)  PATH 'Code'
                              ,FIO  VARCHAR2(500) PATH 'Name'
														  ,POS  VARCHAR2(200) PATH 'position'
														  ,ROLE VARCHAR2(200) PATH 'role') C) X
	ON (CC.EXT_ID = X.id AND CC.ID_CLIENT = X.ID_CLIENT)														
	WHEN  MATCHED THEN
		UPDATE SET CC.FIO = X.FIO,
							 CC.CODE = X.CODE,
							 CC.POSITION = X.POS,
							 CC.ROLE = X.ROLE
	WHEN NOT MATCHED THEN
		INSERT (id_REPRES, fio, id_client, date_created, code, position, ext_id, role) 						 
		VALUES(SEQ_REPRES.NEXTVAL,X.FIO,X.ID_CLIENT,SYSDATE, X.CODE,X.POS,X.id,X.ROLE);       
	
--КОНТАКТЫ КОНТКТОВ  
  IF P_CLIENT_ID IS NOT NULL THEN
		DELETE FROM T_CLI_REP_CONTACTS RC WHERE RC.CONT_SOURCE = 'R' 
		AND RC.ID_REPRES IN (SELECT R.ID_REPRES FROM T_CLI_REPRES R WHERE R.ID_CLIENT = P_CLIENT_ID);
	ELSE
		DELETE FROM T_CLI_REP_CONTACTS RC WHERE RC.CONT_SOURCE = 'R' ;
	END IF;	                                                        
	
  INSERT INTO T_CLI_REP_CONTACTS (ID,ID_REPRES,DET_TYPE,DET_VAL,DET_DESC,EXT_ID)
	SELECT SEQ_CLI_CONTACT.NEXTVAL,X.ID_REPRES,X.C_TYPE,X.C_VALUE,X.C_VIEW,X.C_UID 
	FROM (SELECT DISTINCT (SELECT R.ID_REPRES FROM T_CLI_REPRES R WHERE R.EXT_ID = M.ID AND ROWNUM = 1) ID_REPRES,
	                C.* 	               
				FROM XMLTABLE('//ContactFaces/ContactFace' PASSING XMLTYPE(L_CLOB)
														 COLUMNS   ID VARCHAR2(100) PATH 'UID',
														 CONTACTS  XMLTYPE PATH 'ContactInformations') M
						,XMLTABLE('/ContactInformations/ContactInformation' PASSING M.CONTACTS
														 COLUMNS C_UID   VARCHAR2(100) PATH 'UID'
																		,C_VIEW  VARCHAR2(100)  PATH 'view'
																		,C_TYPE  VARCHAR2(50)  PATH 'type'
																		,C_VALUE VARCHAR2(500) PATH 'value') C) X
	WHERE X.C_VALUE IS NOT NULL;														
	
		
--КОНТАКТНЫЕ ДАННЫЕ		
  IF P_CLIENT_ID IS NOT NULL THEN
		DELETE FROM T_CLI_CONTACT_DETAILS RC WHERE RC.CONT_SOURCE = 'R' 
		AND RC.ID_CLIENT = P_CLIENT_ID;
	ELSE
		DELETE FROM T_CLI_CONTACT_DETAILS RC WHERE RC.CONT_SOURCE = 'R' ;
	END IF;	              

  INSERT INTO T_CLI_CONTACT_DETAILS(ID,ID_CLIENT,DET_TYPE,DET_VAL,DET_DESC,EXT_ID)
	SELECT SEQ_CLI_CONTACT.NEXTVAL,T1.ID_CLIENT,T1.C_TYPE,T1.C_VALUE,T1.C_VIEW,T1.C_UID
	FROM(SELECT DISTINCT (SELECT CLI.ID_CLIENT FROM T_CLIENTS CLI WHERE CLI.EXT_ID = M.ID) ID_CLIENT,
								C.*                  
				FROM XMLTABLE('/Clients/Client' PASSING XMLTYPE(L_CLOB)
											COLUMNS   ID VARCHAR2(100) PATH 'UID',
																CONTACTS  XMLTYPE PATH 'ContactInformations') M
						,XMLTABLE('/ContactInformations/ContactInformation' PASSING M.CONTACTS
								 COLUMNS C_UID   VARCHAR2(100) PATH 'UID'
												,C_VIEW  VARCHAR2(100)  PATH 'view'
												,C_TYPE  VARCHAR2(50)  PATH 'type'
												,C_VALUE VARCHAR2(500) PATH 'value') C)T1;
																
--БАНКОВСКИЕ СЧЕТА
  MERGE INTO T_SETL_ACCOUNTS A
	USING (SELECT (SELECT CLI.ID_CLIENT FROM T_CLIENTS CLI WHERE CLI.EXT_ID = M.cl_id) ID_CLIENT,
	              (SELECT B.ID_BANK FROM T_BANKS B WHERE B.BIK = C.BIK AND ROWNUM = 1) ID_BANK,
								 C.id, C.ACC_NUM, C.BIK, c.ACC_TYPE
								,TO_DATE(C.OpeningDate,'DD.MM.YYYY') OP_DATE
								,TO_DATE(C.ClosingDate,'DD.MM.YYYY') CL_DATE     
	       FROM XMLTABLE('/Clients/Client' PASSING XMLTYPE(L_CLOB)
	                     COLUMNS cl_id VARCHAR2(100) PATH 'UID',
				 							 BankAccounts  XMLTYPE PATH 'BankAccounts') M
						 ,XMLTABLE('/BankAccounts/BankAccount' PASSING M.BankAccounts
						           COLUMNS id VARCHAR2(100)  PATH 'UID'
                              ,ACC_NUM VARCHAR2(50)  PATH 'AccountNumber' 
															,ACC_TYPE VARCHAR2(1000)  PATH 'AccountType'
                              ,BIK  VARCHAR2(500) PATH 'Bik'
														  ,ClosingDate  VARCHAR2(50) PATH 'ClosingDate'
														  ,OpeningDate VARCHAR2(50) PATH 'OpeningDate') C) X
	ON (A.EXT_ID = X.id AND a.id_client = x.id_client)
	WHEN MATCHED THEN 
		UPDATE SET A.ID_BANK     = X.ID_BANK
		          ,A.ACC_NUMBER  = X.ACC_NUM 													
							,A.OPEN_DATE   = X.OP_DATE
							,A.CLOSE_DATE = X.CL_DATE           
							,A.ACC_TYPE = X.ACC_TYPE
	WHEN NOT MATCHED THEN
		INSERT (ID_ACC,ID_BANK,ACC_NUMBER,OPEN_DATE,ID_CLIENT, ACC_SOURCE,EXT_ID,CLOSE_DATE)
		VALUES(SEQ_SETL_ACC.NEXTVAL,X.ID_BANK,X.ACC_NUM,X.OP_DATE,X.ID_CLIENT,'R',X.id,X.CL_DATE);
		      						 
END P_GET_CLIENTS;

------------------------------------------------------------------------------------------------

PROCEDURE P_GET_CONTRACTS(P_CLIENT_ID NUMBER DEFAULT NULL) 
	IS
	L_CLOB CLOB;  
  L_URL  VARCHAR2(500);
	L_EXT_ID VARCHAR2(100);
BEGIN

	-- Достаем URL  
	L_URL := G$URL || '/PersonalAccounts';   
	IF P_CLIENT_ID IS NOT NULL THEN
		SELECT S.EXT_ID INTO L_EXT_ID FROM T_CLIENTS S WHERE S.ID_CLIENT = P_CLIENT_ID;
		L_URL := L_URL||'?id='||L_EXT_ID;
	END IF;
	--Парсим XML
	L_CLOB := PKG_WEB.F_SEND_REQUEST(P_URL => L_URL, P_METHOD => 'GET',
																	 P_USER => G$USER, P_PWD => G$PWD);
  
--ОСНОВНАЯ ИНФОРМАЦИЯ
	MERGE INTO T_CONTRACTS CT
	USING (SELECT * FROM 
	       (SELECT  T.*,
	              (SELECT S.ID_CLIENT FROM T_CLIENTS S WHERE S.EXT_ID = T.ID_CLI) ID_CLIENT								
				 FROM   XMLTABLE('/PersonalAccounts/PersonalAccount' PASSING XMLTYPE(L_CLOB) 
				         COLUMNS ID         VARCHAR2(100) PATH 'UID',
												 ID_CLI     VARCHAR2(100) PATH 'UIDpartner',
												 CTR_CODE   VARCHAR2(100) PATH 'Code',
												 CTR_NAME   VARCHAR2(100) PATH 'Name',
												 CTR_DATE   VARCHAR2(100) PATH 'agreementDate',
												 START_DATE VARCHAR2(100) PATH 'StartDate',
												 END_DATE VARCHAR2(100) PATH 'EndDate',
												 FIN_SOURCE VARCHAR2(100) PATH 'FinancingSource',
												 CTR_TYPE   VARCHAR2(100) PATH 'TypeContract',
												 CATEGORY   VARCHAR2(100) PATH 'Category',
												 STAGE      VARCHAR2(100) PATH 'Stage') T)T1
				 WHERE T1.ID_CLIENT IS NOT NULL) X
	ON (CT.EXT_ID = X.ID)
	WHEN MATCHED THEN
		UPDATE
		SET    CT.CTR_NUMBER = X.CTR_NAME
		      ,CT.CTR_DATE   = TO_DATE(X.CTR_DATE,'DD.MM.YYYY')
					,CT.CTR_CODE   = TRIM(X.CTR_CODE)    
					,CT.ID_CLIENT  = X.ID_CLIENT 
					,CT.START_DATE = TO_DATE(X.START_DATE,'DD.MM.YYYY')
					,CT.END_DATE   = TO_DATE(X.END_DATE,'DD.MM.YYYY')
					,CT.FIN_SOURCE = X.FIN_SOURCE
					,CT.CTR_TYPE   = X.CTR_TYPE 
					,CT.CUST_GROUP = X.CATEGORY
					,CT.CTR_STAGE  = X.STAGE
  WHEN NOT MATCHED THEN
		INSERT (ID_CONTRACT,CTR_NUMBER,ID_CLIENT,CTR_DATE,CTR_CODE, EXT_ID,START_DATE,END_DATE,FIN_SOURCE,CTR_TYPE,CUST_GROUP,CTR_STAGE)
		VALUES (SEQ_CONTRACTS.NEXTVAL,X.CTR_NAME,X.ID_CLIENT,TO_DATE(X.CTR_DATE,'DD.MM.YYYY'),TRIM(X.CTR_CODE),X.ID,
		        TO_DATE(X.START_DATE,'DD.MM.YYYY'),TO_DATE(X.END_DATE,'DD.MM.YYYY'),X.FIN_SOURCE,X.CTR_TYPE,X.CATEGORY,X.STAGE);  
											
--ОБЪЕКТЫ ПО ДОГОВОРУ
	MERGE INTO T_CONTRACT_OBJ CO
	USING (SELECT (SELECT CT.ID_CONTRACT FROM T_CONTRACTS CT  WHERE CT.EXT_ID = M.CTR_ID) ID_CONTRACT,
	              C.* 	               
	       FROM XMLTABLE('/PersonalAccounts/PersonalAccount' PASSING XMLTYPE(L_CLOB)
	                     COLUMNS CTR_ID VARCHAR2(100) PATH 'UID',
				 							 OBJECTS XMLTYPE PATH 'CalculationObjects') M
						 ,XMLTABLE('/CalculationObjects/CalculationObject' PASSING M.OBJECTS
						           COLUMNS id    VARCHAR2(100)  PATH 'UID'
                              ,OBJ_NAME  VARCHAR2(500) PATH 'Name' 
															,DATE_INC  VARCHAR2(100) PATH 'DateInclusionsInContract'
														  ,Address  VARCHAR2(1000) PATH 'Address'  
															,OBJ_TYPE VARCHAR2(100) PATH 'TypeHeatInstallations') C) X
	ON (CO.EXT_ID = X.id)														
	WHEN  MATCHED THEN
		UPDATE SET CO.OBJ_NAME = X.OBJ_NAME,
		           CO.ADDRESS_NAME = X.ADDRESS,
							 CO.ID_CONTRACT = X.ID_CONTRACT,
							 CO.DATE_INC  = TO_DATE(X.DATE_INC,'DD.MM.YYYY'),
							 CO.OBJ_TYPE  = X.OBJ_TYPE							 
	WHEN NOT MATCHED THEN
		INSERT (ID_OBJECT,ID_CONTRACT,OBJ_NAME,ADDRESS_NAME,EXT_ID,DATE_INC,OBJ_TYPE)
		VALUES(SEQ_OBJECTS.NEXTVAL,X.ID_CONTRACT,X.OBJ_NAME,X.ADDRESS,X.ID,TO_DATE(X.DATE_INC,'DD.MM.YYYY'),X.OBJ_TYPE);           
  
	DELETE FROM T_CTR_OBJ_DISABLE D WHERE D.ID_OBJECT IN 
	(SELECT O.ID_OBJECT FROM T_CONTRACT_OBJ O, T_CONTRACTS CT, T_CLIENTS S
	 WHERE (S.ID_CLIENT = P_CLIENT_ID OR P_CLIENT_ID IS NULL)
	 AND   S.ID_CLIENT = CT.ID_CLIENT AND CT.ID_CONTRACT = O.ID_CONTRACT);
	 
--ВОЗМОЖНОСТЬ НА ОТКЛЮЧЕНИЕ 
	INSERT INTO T_CTR_OBJ_DISABLE (ID,ID_OBJECT,BIL_DATE,CON_TYPE,VALUE)
	SELECT SEQ_DISABLE_ABIL.NEXTVAL,X.ID_OBJECT,X.PERIOD,X.CON_TYPE,X.VAL
	FROM (
			SELECT (SELECT CT.ID_OBJECT FROM T_CONTRACT_OBJ CT  WHERE CT.EXT_ID = M.OBJ_ID) ID_OBJECT,
						 TO_DATE(C.PERIOD,'DD.MM.YYYY HH24:MI:SS') PERIOD,
						 C.CON_TYPE,
						 c.VAL 	               
	    FROM XMLTABLE('/PersonalAccounts/PersonalAccount/CalculationObjects/CalculationObject' PASSING XMLTYPE(L_CLOB)
													 COLUMNS OBJ_ID    VARCHAR2(100)  PATH 'UID',
													 OBJECTS XMLTYPE PATH 'Ability_to_disable_services') M
				  ,XMLTABLE('/Ability_to_disable_services/Ability_to_disable_record' PASSING M.OBJECTS
													 COLUMNS PERIOD    VARCHAR2(100) PATH 'Period'
																	,CON_TYPE  VARCHAR2(100) PATH 'Connection_Type'
																	,VAL  VARCHAR2(1000) PATH 'Value') C)X
	 WHERE X.ID_OBJECT IS NOT NULL;

END P_GET_CONTRACTS;

-----------------------------------------------------------------------------------

PROCEDURE P_CREATE_FINDOCS(P_ID_CONTRACT NUMBER)
	IS												       
BEGIN
  --АГГРЕГИРУЕМ ФИНАНСОВЫЕ ДОКУМЕНТЫ
	MERGE INTO T_CLI_FINDOCS D
	--ДЕЛАЕМ СРАВНЕНИЕ СУЩЕСТВУЮЩЕГО НАБОРА СТРОК С ПОЛУЧЕННЫМ
	--ПРИВОДИМ ТЕКУЩИЙ К ПОЛУЧЕННОМУ
	USING (SELECT COALESCE(J.ID_CLIENT, FD.ID_CLIENT) ID_CLIENT
							 ,COALESCE(J.ID_CONTRACT, FD.ID_CONTRACT) ID_CONTRACT
							 ,COALESCE(J.EXT_ID_FROM, FD.EXT_ID) EXT_ID
							 ,COALESCE(J.OPER_TYPE, FD.OPER_TYPE) OPER_TYPE
							 ,J.DOC_NUMBER
							 ,J.DOC_DATE
							 ,J.DOC_COM
							 ,J.DOC_TYPE                          
							 ,J.AMOUNT
							 ,NVL2(J.EXT_ID_FROM, 'N', 'Y') DEL
				 FROM   (SELECT C.ID_CLIENT,C.ID_CONTRACT,C.EXT_ID_FROM
											 ,C.OPER_TYPE,C.DOC_NUMBER,C.DOC_DATE
											 ,C.DOC_COM,C.DOC_TYPE,SUM(C.AMOUNT) AMOUNT
								 FROM   T_CLI_FINDOC_CONS C                
								 WHERE  C.ID_CONTRACT = P_ID_CONTRACT
  						   AND    C.DEBT_TYPE = 'Полезный отпуск'
								 GROUP  BY C.ID_CLIENT,C.ID_CONTRACT,C.EXT_ID_FROM
													,C.OPER_TYPE,C.DOC_NUMBER,C.DOC_DATE
													,C.DOC_COM,C.DOC_TYPE) J 
				 FULL OUTER JOIN (SELECT F.ID_CLIENT,F.ID_CONTRACT,F.EXT_ID,F.OPER_TYPE
													FROM   T_CLI_FINDOCS F
													WHERE  F.ID_CONTRACT =P_ID_CONTRACT) FD
	       ON (J.ID_CLIENT = FD.ID_CLIENT AND J.ID_CONTRACT = FD.ID_CONTRACT 
				    AND J.EXT_ID_FROM = FD.EXT_ID AND J.OPER_TYPE = FD.OPER_TYPE)) T
												
	ON (D.ID_CLIENT = T.ID_CLIENT AND D.ID_CONTRACT = T.ID_CONTRACT AND D.EXT_ID = T.EXT_ID AND D.OPER_TYPE = T.OPER_TYPE)
	WHEN MATCHED THEN
		UPDATE
		SET    D.DOC_NUMBER  = T.DOC_NUMBER
					,D.DOC_DATE    = T.DOC_DATE
					,D.COMMENTARY  = T.DOC_COM
					,D.DOC_TYPE    = T.DOC_TYPE
					,D.AMOUNT      = NVL(T.AMOUNT, 0)
					,D.DOC_PERIOD  = TRUNC(T.DOC_DATE,'MM')
		DELETE	WHERE  T.DEL = 'Y' 
  WHEN NOT MATCHED THEN   
		INSERT (ID_DOC,DOC_NUMBER,DOC_DATE,DOC_TYPE,AMOUNT,ID_CONTRACT,EXT_ID,OPER_TYPE,ID_CLIENT,COMMENTARY,DEBT,DOC_PERIOD)
		VALUES(SEQ_FINDOCS.NEXTVAL,T.DOC_NUMBER,T.DOC_DATE, T.DOC_TYPE,T.AMOUNT,T.ID_CONTRACT,T.EXT_ID,T.OPER_TYPE,T.ID_CLIENT,T.DOC_COM,NVL(T.AMOUNT, 0),TRUNC(T.DOC_DATE,'MM'));
 
--ЗАДОЛЖЕННОСТЬ	
  UPDATE T_CLI_FINDOCS F 
	   SET F.DEBT = NVL((SELECT sum(CASE 
		                                WHEN C.OPER_TYPE = 'Приход' THEN c.amount
																		ELSE -c.amount
																	END) 
		                   FROM t_cli_findoc_cons c
   										 WHERE c.EXT_ID_TO = F.EXT_ID),0) 
	WHERE F.ID_CONTRACT = P_ID_CONTRACT;	   

END;

PROCEDURE P_CREATE_FINDOCS
	IS												       
BEGIN
 --АГГРЕГИРУЕМ ФИНАНСОВЫЕ ДОКУМЕНТЫ
	MERGE INTO T_CLI_FINDOCS D
	--ДЕЛАЕМ СРАВНЕНИЕ СУЩЕСТВУЮЩЕГО НАБОРА СТРОК С ПОЛУЧЕННЫМ
	--ПРИВОДИМ ТЕКУЩИЙ К ПОЛУЧЕННОМУ
	USING (SELECT COALESCE(J.ID_CLIENT, FD.ID_CLIENT) ID_CLIENT
							 ,COALESCE(J.ID_CONTRACT, FD.ID_CONTRACT) ID_CONTRACT
							 ,COALESCE(J.EXT_ID_FROM, FD.EXT_ID) EXT_ID
							 ,COALESCE(J.OPER_TYPE, FD.OPER_TYPE) OPER_TYPE
							 ,J.DOC_NUMBER
							 ,J.DOC_DATE
							 ,J.DOC_COM
							 ,J.DOC_TYPE                          
							 ,J.AMOUNT
							 ,NVL2(J.EXT_ID_FROM, 'N', 'Y') DEL
				 FROM   (SELECT C.ID_CLIENT,C.ID_CONTRACT,C.EXT_ID_FROM
											 ,C.OPER_TYPE,C.DOC_NUMBER,C.DOC_DATE
											 ,C.DOC_COM,C.DOC_TYPE,SUM(C.AMOUNT) AMOUNT
								 FROM   T_CLI_FINDOC_CONS C                      
								 WHERE    C.DEBT_TYPE = 'Полезный отпуск'
								 GROUP  BY C.ID_CLIENT,C.ID_CONTRACT,C.EXT_ID_FROM
													,C.OPER_TYPE,C.DOC_NUMBER,C.DOC_DATE
													,C.DOC_COM,C.DOC_TYPE) J 
				 FULL OUTER JOIN (SELECT F.ID_CLIENT,F.ID_CONTRACT,F.EXT_ID,F.OPER_TYPE
													FROM   T_CLI_FINDOCS F) FD
	       ON (J.ID_CLIENT = FD.ID_CLIENT AND J.ID_CONTRACT = FD.ID_CONTRACT 
				    AND J.EXT_ID_FROM = FD.EXT_ID AND J.OPER_TYPE = FD.OPER_TYPE)) T
												
	ON (D.ID_CLIENT = T.ID_CLIENT AND D.ID_CONTRACT = T.ID_CONTRACT AND D.EXT_ID = T.EXT_ID AND D.OPER_TYPE = T.OPER_TYPE)
	WHEN MATCHED THEN
		UPDATE
		SET    D.DOC_NUMBER  = T.DOC_NUMBER
					,D.DOC_DATE    = T.DOC_DATE
					,D.COMMENTARY  = T.DOC_COM
					,D.DOC_TYPE    = T.DOC_TYPE
					,D.AMOUNT      = NVL(T.AMOUNT, 0)
					,D.DOC_PERIOD  = TRUNC(T.DOC_DATE,'MM')
		DELETE	WHERE  T.DEL = 'Y' 
  WHEN NOT MATCHED THEN   
		INSERT (ID_DOC,DOC_NUMBER,DOC_DATE,DOC_TYPE,AMOUNT,ID_CONTRACT,EXT_ID,OPER_TYPE,ID_CLIENT,COMMENTARY,DEBT,DOC_PERIOD)
		VALUES(SEQ_FINDOCS.NEXTVAL,T.DOC_NUMBER,T.DOC_DATE, T.DOC_TYPE,T.AMOUNT,T.ID_CONTRACT,T.EXT_ID,T.OPER_TYPE,T.ID_CLIENT,T.DOC_COM,NVL(T.AMOUNT, 0),TRUNC(T.DOC_DATE,'MM'));
 
--ЗАДОЛЖЕННОСТЬ	
  UPDATE T_CLI_FINDOCS F 
	   SET F.DEBT = NVL((SELECT sum(CASE 
		                                WHEN C.OPER_TYPE = 'Приход' THEN c.amount
																		ELSE -c.amount
																	END)  
		                   FROM t_cli_findoc_cons c
   										 WHERE c.EXT_ID_TO = F.EXT_ID),0);	 
END;


PROCEDURE P_GET_FINDOCS_CONS(P_DATE_BEG    DATE DEFAULT NULL
														,P_DATE_END    DATE DEFAULT NULL)
	IS												       
	L_XML    XMLTYPE;  
	L_URL    VARCHAR2(500);                                                
	L_CLOB   CLOB;
BEGIN
	-- Достаем URL  
	L_URL := G$URL || '/DebtExtended';  
	
  SELECT XMLELEMENT("Contracts", XMLAGG(XMLELEMENT("Contract", CT.EXT_ID)))
	INTO   L_XML
  FROM   T_CONTRACTS CT;
	
	IF P_DATE_BEG IS NOT NULL THEN
		 SELECT INSERTCHILDXML(L_XML, '/Contracts', 'StartDate'
		                      ,XMLTYPE('<StartDate>' || TO_CHAR(P_DATE_BEG, 'DD.MM.YYYY') || '</StartDate>')) 
	   INTO L_XML FROM DUAL;	
	END IF;
	
	IF P_DATE_END IS NOT NULL THEN
		 SELECT INSERTCHILDXML(L_XML, '/Contracts', 'EndDate'
		                      ,XMLTYPE('<EndDate>' || TO_CHAR(P_DATE_END, 'DD.MM.YYYY') || '</EndDate>')) 
	   INTO L_XML FROM DUAL;	
	END IF;  
	 
	--Парсим XML
	L_CLOB := PKG_WEB.F_SEND_REQUEST(P_URL => L_URL, P_METHOD => 'POST',
																	 P_USER => G$USER, P_PWD => G$PWD
																	,P_REQ => L_XML.getClobVal);
	
  MERGE INTO T_CLI_FINDOC_CONS FC 
	USING (SELECT Z.OPER_TYPE,Z.AMOUNT,Z.BILLING_DATE
								,COALESCE(Z.ID_CLIENT,C.ID_CLIENT) ID_CLIENT
								,COALESCE(Z.ID_CONTRACT,C.ID_CONTRACT) ID_CONTRACT
								,COALESCE(Z.EXT_ID_FROM,C.EXT_ID_FROM) EXT_ID_FROM
								,COALESCE(Z.EXT_ID_TO,C.EXT_ID_TO) EXT_ID_TO
								,Z.DOC_NUMBER,Z.DOC_DATE,Z.DOC_TYPE, Z.DOC_COM
								,Z.DOC_CALC_NUMBER,Z.DOC_CALC_DATE,Z.DOC_CALC_TYPE,Z.DOC_CALC_COM
								,Z.TYPEDEBT,Z.PAYMENTTYPE,Z.ORGANIZATION,Z.DOCPAYMENTS,Z.TYPEOPERATION
								,Z.NDS,COALESCE(Z.ROW_NUM,C.ROW_NUM) ROW_NUM
								,NVL2(Z.EXT_ID_FROM,'N','Y') DEL 
					FROM	
						(SELECT T.OPER_TYPE ,TO_NUMBER(T.AMOUNT,'999999999999999999D99','NLS_NUMERIC_CHARACTERS=''. ''') AMOUNT
									,TO_DATE(T.BDATE,'DD.MM.YYYY') BILLING_DATE
									,(SELECT CT.ID_CONTRACT FROM T_CONTRACTS CT WHERE CT.EXT_ID = T.ID_CONTR) ID_CONTRACT
									,(SELECT S.ID_CLIENT FROM T_CLIENTS S WHERE S.EXT_ID = T.ID_CLI) ID_CLIENT
									,T.EXT_ID_FROM,T.EXT_ID_TO,T.DOC_COM,T.DOC_NUMBER
									,TO_DATE(T.DOC_DATE,'DD.MM.YYYY HH24:MI:SS') DOC_DATE
									,T.DOC_TYPE,T.DOC_CALC_COM,T.DOC_CALC_NUMBER,T.DOC_CALC_TYPE
									,TO_DATE(T.DOC_CALC_DATE,'DD.MM.YYYY HH24:MI:SS') DOC_CALC_DATE
									,T.TYPEDEBT,T.PAYMENTTYPE,T.ORGANIZATION,T.DocPayments  
									,T.TYPEOPERATION,T.NDS,to_number(t.Row_Num) Row_Num   
									FROM XMLTABLE('/MutualSettlements/MutualSettlement' PASSING XMLTYPE(L_CLOB) 
																				 COLUMNS BDATE       VARCHAR2(100) PATH 'Date',
																								 ID_CLI      VARCHAR2(100) PATH 'UIDcontractor',
																								 ID_CONTR    VARCHAR2(100) PATH 'UIDpersonalaccount',
																								 EXT_ID_FROM VARCHAR2(100) PATH 'UIDDocument', 
																								 EXT_ID_TO   VARCHAR2(100) PATH 'UIDCalculationDocument',
																								 AMOUNT      VARCHAR2(100) PATH 'sum',
																								 DOC_NUMBER  VARCHAR2(100) PATH 'Registrar_Number',                        
																								 DOC_DATE    VARCHAR2(100) PATH 'Registrar_Date',
																								 DOC_TYPE    VARCHAR2(200) PATH 'Registrar_type',
																								 DOC_COM     VARCHAR2(500) PATH 'Registrar_showing',
																								 OPER_TYPE   VARCHAR2(100) PATH 'Type_of_movement',
																								 DOC_CALC_NUMBER  VARCHAR2(100) PATH 'CalculationDocument_Number',
																								 DOC_CALC_DATE    VARCHAR2(100) PATH 'CalculationDocument_Date',
																								 DOC_CALC_COM     VARCHAR2(500) PATH 'CalculationDocument_showing', 
																								 DOC_CALC_TYPE    VARCHAR2(200) PATH 'CalculationDocument_type',
																								 TypeDebt      VARCHAR2(1000) PATH 'TypeDebt',
																								 Organization  VARCHAR2(100)  PATH 'Organization',
																								 TypeOperation VARCHAR2(100)  PATH 'TypeOperation',
																								 PaymentType   VARCHAR2(100)  PATH 'PaymentType',
																								 DocPayments   VARCHAR2(100)  PATH 'DocPayments',
																								 NDS           VARCHAR2(100)  PATH 'VATrate',
																								 Row_num        VARCHAR2(100)  PATH 'RowNum') T)Z
						FULL OUTER JOIN (SELECT FDC.ID_CONTRACT,FDC.EXT_ID_FROM,FDC.EXT_ID_TO, FDC.ROW_NUM,FDC.ID_CLIENT 
												FROM T_CLI_FINDOC_CONS FDC WHERE FDC.DOC_DATE 
												BETWEEN NVL(P_DATE_BEG,DATE '2000-01-01') AND NVL(P_DATE_END, DATE '3000-01-01')) C
						ON (Z.EXT_ID_FROM = C.EXT_ID_FROM AND Z.EXT_ID_TO = C.EXT_ID_TO AND Z.ID_CONTRACT = C.ID_CONTRACT AND C.ROW_NUM = Z.ROW_NUM))X
	ON (X.EXT_ID_FROM = FC.EXT_ID_FROM AND X.EXT_ID_TO = FC.EXT_ID_TO AND X.ID_CONTRACT = FC.ID_CONTRACT AND X.ROW_NUM = FC.ROW_NUM)
	WHEN MATCHED THEN 
		UPDATE SET FC.OPER_TYPE       = X.OPER_TYPE
							,FC.AMOUNT          = X.AMOUNT
							,FC.DATE_CON        = X.BILLING_DATE
							,FC.DATE_UPDATE     = SYSDATE
							,FC.DOC_COM         = X.DOC_COM
							,FC.DOC_NUMBER      = X.DOC_NUMBER
							,FC.DOC_DATE        = X.DOC_DATE
							,FC.DOC_TYPE        = X.DOC_TYPE
							,FC.DOC_CALC_COM    = X.DOC_CALC_COM
							,FC.DOC_CALC_NUMBER = X.DOC_CALC_NUMBER
							,FC.DOC_CALC_TYPE   = X.DOC_CALC_TYPE
							,FC.DOC_CALC_DATE   = X.DOC_CALC_DATE
							,FC.DEBT_TYPE       = X.TYPEDEBT
							,FC.PAYMENT_TYPE    = X.PAYMENTTYPE
							,FC.ORGANIZATION    = X.ORGANIZATION
							,FC.PAYMENT_DOC     = X.DOCPAYMENTS
							,FC.OPERATION       = X.TypeOperation
							,FC.NDS             = X.NDS
		DELETE WHERE X.DEL = 'Y'
	WHEN NOT MATCHED THEN 
		INSERT(ID,OPER_TYPE,AMOUNT,DATE_CON,DATE_LOAD,ID_CONTRACT,ID_CLIENT
					,EXT_ID_FROM,EXT_ID_TO,DOC_COM,DOC_NUMBER,DOC_DATE,DOC_TYPE,DOC_CALC_COM,DOC_CALC_NUMBER
					,DOC_CALC_TYPE,DOC_CALC_DATE,DEBT_TYPE,PAYMENT_TYPE,ORGANIZATION,PAYMENT_DOC,OPERATION,NDS,ROW_NUM)						
		VALUES(SEQ_FINDOC_CONS.NEXTVAL,X.OPER_TYPE,X.AMOUNT,X.BILLING_DATE,SYSDATE,X.ID_CONTRACT,X.ID_CLIENT,
					 X.EXT_ID_FROM,X.EXT_ID_TO,X.DOC_COM,X.DOC_NUMBER,X.DOC_DATE,X.DOC_TYPE,X.DOC_CALC_COM,X.DOC_CALC_NUMBER,
					 X.DOC_CALC_TYPE,X.DOC_CALC_DATE,X.TYPEDEBT,X.PAYMENTTYPE,X.ORGANIZATION,X.DOCPAYMENTS,X.TypeOperation,X.NDS,X.ROW_NUM);							
END;

FUNCTION F_GET_PRINT_DOC(P_PRINT_CODE VARCHAR2,
	                          P_EXT_ID     VARCHAR2,
														P_DOC_TYPE   VARCHAR2) RETURN LAWSUP.PKG_FILES.REC_DOC
 IS 
  L_XML    XMLTYPE;  
	L_URL    VARCHAR2(500);                                                
	L_CLOB   CLOB;
	L_RET    LAWSUP.PKG_FILES.REC_DOC;
BEGIN
 	L_URL := G$URL || '/PrintedForm?PrintedFormName='||P_PRINT_CODE||'&UIDDocument='||P_EXT_ID;  	
	
	IF P_DOC_TYPE = 'Корректировка реализации' THEN
		 L_URL := L_URL||'&TypeDoc=КорректировкаРеализации';
	END IF;
	
		--Парсим XML
	L_CLOB := PKG_WEB.F_SEND_REQUEST(P_URL => L_URL, P_METHOD => 'GET',
																	 P_USER => G$USER, P_PWD => G$PWD);            													 
	L_XML := XMLTYPE(L_CLOB);
	IF L_XML.extract('/Data/Status/text()').getStringVal() = 'error' THEN
		 RAISE_APPLICATION_ERROR(-20200,L_XML.extract('/Data/TextError/text()').getStringVal());
	END IF;                                                                                     
	
  L_RET.P_FILE_NAME := L_XML.EXTRACT('/Data/PrintedFormName/text()').getStringVal();
	L_RET.P_FILE_NAME := L_RET.P_FILE_NAME ||'.'|| L_XML.EXTRACT('/Data/Format/text()').getStringVal();               
	L_RET.P_BLOB      := APEX_WEB_SERVICE.clobbase642blob(replace(replace(L_XML.EXTRACT('/Data/body/text()').getClobVal(),chr(13),''),chr(10),''));
	
	RETURN L_RET;											 
	
END;														

PROCEDURE P_GET_IFNS
	IS
	L_URL VARCHAR2(500);
	L_CLOB CLOB;
BEGIN
	 L_URL := PKG_PREF.F$C2('NN_ADDRESS')||'/Clients';
	 L_CLOB := PKG_WEB.F_SEND_REQUEST(P_URL => L_URL,P_METHOD => 'GET'
	                                 ,P_USER => PKG_PREF.F$C1('NN_USER')
																	 ,P_PWD => PKG_PREF.F$C1('NN_PWD'));
	 MERGE INTO	T_IFNS F
	 USING (SELECT *
	        FROM XMLTABLE('/Clients/Client'
					              PASSING XMLTYPE(L_CLOB)
												COLUMNS EXT_ID       PATH 'UID',
												        DEP_Code       PATH 'Code',
																SHORT_NAME PATH 'Name',
																Full_Name  PATH 'FullName',
																INN        PATH 'INN',
																KPP        PATH 'KPP'))	X
	ON (F.EXT_ID = X.EXT_ID)
	WHEN MATCHED THEN
		UPDATE SET F.IFNS_CODE  = TRIM(X.DEP_CODE)
							,F.SHORT_NAME = X.SHORT_NAME
							,F.FULL_NAME  = X.FULL_NAME
							,F.INN        = X.INN
							,F.KPP        = X.KPP								 
	WHEN NOT MATCHED THEN
		INSERT (ID,IFNS_CODE,SHORT_NAME,FULL_NAME,INN,KPP,EXT_ID)
		VALUES (SEQ_MAIN.NEXTVAL,TRIM(X.DEP_CODE),X.SHORT_NAME,X.FULL_NAME,X.INN,X.KPP,X.EXT_ID);
END;

FUNCTION F_REGISTER_NEW_CLAIM(P_ID_IFNS NUMBER,
	                            P_SUM     NUMBER,
															P_COMMENT VARCHAR2,
															P_USER    VARCHAR2) RETURN VARCHAR2
	IS
	L_CLOB CLOB;
	L_URL  VARCHAR2(500);
	L_EXT_ID VARCHAR2(50);
BEGIN
	L_URL := PKG_PREF.F$C2('NN_ADDRESS')||'/ApplicationForSpendingMoney';
	
	SELECT S.EXT_ID INTO L_EXT_ID FROM T_IFNS S WHERE S.ID = P_ID_IFNS; 
	
	L_CLOB := '<Document>
	                <UIDClient>'||L_EXT_ID||'</UIDClient>
									<Sum>'||TO_CHAR(P_SUM,'FM99999999999999999D99','NLS_NUMERIC_CHARACTERS='', ''')||'</Sum>
									<Comment>'||P_COMMENT||'</Comment>   
									<Responsible>'||P_USER||'</Responsible>
						</Document>';
	
	L_CLOB := PKG_WEB.F_SEND_REQUEST(P_URL => L_URL,P_METHOD => 'POST'
	                                 ,P_USER => PKG_PREF.F$C2('NN_USER')
																	 ,P_PWD => PKG_PREF.F$C2('NN_PWD')
																	 ,P_REQ => L_CLOB);
																	 
	RETURN L_CLOB;
END;




PROCEDURE P_GET_FINDOCS_CONS(P_DATE_BEG    DATE DEFAULT NULL
 													  ,P_DATE_END    DATE DEFAULT NULL
														,P_ID_CONTRACT NUMBER)
	IS												       
	L_XML    XMLTYPE;  
	L_URL    VARCHAR2(500);                                                
	L_CLOB   CLOB;
BEGIN
	-- Достаем URL  
	L_URL := G$URL || '/DebtExtended';  
	
  SELECT XMLELEMENT("Contracts", XMLAGG(XMLELEMENT("Contract", CT.EXT_ID)))
	INTO   L_XML
  FROM   T_CONTRACTS CT
	WHERE  CT.ID_CONTRACT = P_ID_CONTRACT;
	
	IF P_DATE_BEG IS NOT NULL THEN
		 SELECT INSERTCHILDXML(L_XML, '/Contracts', 'StartDate'
		                      ,XMLTYPE('<StartDate>' || TO_CHAR(P_DATE_BEG, 'DD.MM.YYYY') || '</StartDate>')) 
	   INTO L_XML FROM DUAL;	
	END IF;
	
	IF P_DATE_END IS NOT NULL THEN
		 SELECT INSERTCHILDXML(L_XML, '/Contracts', 'EndDate'
		                      ,XMLTYPE('<EndDate>' || TO_CHAR(P_DATE_END, 'DD.MM.YYYY') || '</EndDate>')) 
	   INTO L_XML FROM DUAL;	
	END IF;  
	 
	--Парсим XML
	L_CLOB := PKG_WEB.F_SEND_REQUEST(P_URL => L_URL, P_METHOD => 'POST',
																	 P_USER => G$USER, P_PWD => G$PWD
																	,P_REQ => L_XML.getClobVal);
	                                                  
																
	MERGE INTO T_CLI_FINDOC_CONS FC 
	USING (SELECT  COALESCE(Z.OPER_TYPE,C.OPER_TYPE) OPER_TYPE
	              ,Z.AMOUNT,Z.BILLING_DATE
								,COALESCE(Z.ID_CLIENT,C.ID_CLIENT) ID_CLIENT
								,COALESCE(Z.ID_CONTRACT,C.ID_CONTRACT) ID_CONTRACT
								,COALESCE(Z.EXT_ID_FROM,C.EXT_ID_FROM) EXT_ID_FROM
								,COALESCE(Z.EXT_ID_TO,C.EXT_ID_TO) EXT_ID_TO
								,Z.DOC_NUMBER,Z.DOC_DATE,Z.DOC_TYPE, Z.DOC_COM
								,Z.DOC_CALC_NUMBER,Z.DOC_CALC_DATE,Z.DOC_CALC_TYPE,Z.DOC_CALC_COM
								,Z.TYPEDEBT,Z.PAYMENTTYPE,Z.ORGANIZATION,Z.DOCPAYMENTS,Z.TYPEOPERATION
								,Z.NDS,COALESCE(Z.ROW_NUM,C.ROW_NUM) ROW_NUM
								,NVL2(Z.EXT_ID_FROM,'N','Y') DEL 
					FROM	
						(SELECT T.OPER_TYPE ,TO_NUMBER(T.AMOUNT,'999999999999999999D99','NLS_NUMERIC_CHARACTERS=''. ''') AMOUNT
									,TO_DATE(T.BDATE,'DD.MM.YYYY') BILLING_DATE
									,(SELECT CT.ID_CONTRACT FROM T_CONTRACTS CT WHERE CT.EXT_ID = T.ID_CONTR) ID_CONTRACT
									,(SELECT S.ID_CLIENT FROM T_CLIENTS S WHERE S.EXT_ID = T.ID_CLI) ID_CLIENT
									,T.EXT_ID_FROM,T.EXT_ID_TO,T.DOC_COM,T.DOC_NUMBER
									,TO_DATE(T.DOC_DATE,'DD.MM.YYYY HH24:MI:SS') DOC_DATE
									,T.DOC_TYPE,T.DOC_CALC_COM,T.DOC_CALC_NUMBER,T.DOC_CALC_TYPE
									,TO_DATE(T.DOC_CALC_DATE,'DD.MM.YYYY HH24:MI:SS') DOC_CALC_DATE
									,T.TYPEDEBT,T.PAYMENTTYPE,T.ORGANIZATION,T.DocPayments  
									,T.TYPEOPERATION,T.NDS,to_number(t.Row_Num) Row_Num   
									FROM XMLTABLE('/MutualSettlements/MutualSettlement' PASSING XMLTYPE(L_CLOB) 
																				 COLUMNS BDATE       VARCHAR2(100) PATH 'Date',
																								 ID_CLI      VARCHAR2(100) PATH 'UIDcontractor',
																								 ID_CONTR    VARCHAR2(100) PATH 'UIDpersonalaccount',
																								 EXT_ID_FROM VARCHAR2(100) PATH 'UIDDocument', 
																								 EXT_ID_TO   VARCHAR2(100) PATH 'UIDCalculationDocument',
																								 AMOUNT      VARCHAR2(100) PATH 'sum',
																								 DOC_NUMBER  VARCHAR2(100) PATH 'Registrar_Number',                        
																								 DOC_DATE    VARCHAR2(100) PATH 'Registrar_Date',
																								 DOC_TYPE    VARCHAR2(200) PATH 'Registrar_type',
																								 DOC_COM     VARCHAR2(500) PATH 'Registrar_showing',
																								 OPER_TYPE   VARCHAR2(100) PATH 'Type_of_movement',
																								 DOC_CALC_NUMBER  VARCHAR2(100) PATH 'CalculationDocument_Number',
																								 DOC_CALC_DATE    VARCHAR2(100) PATH 'CalculationDocument_Date',
																								 DOC_CALC_COM     VARCHAR2(500) PATH 'CalculationDocument_showing', 
																								 DOC_CALC_TYPE    VARCHAR2(200) PATH 'CalculationDocument_type',
																								 TypeDebt      VARCHAR2(1000) PATH 'TypeDebt',
																								 Organization  VARCHAR2(100)  PATH 'Organization',
																								 TypeOperation VARCHAR2(100)  PATH 'TypeOperation',
																								 PaymentType   VARCHAR2(100)  PATH 'PaymentType',
																								 DocPayments   VARCHAR2(100)  PATH 'DocPayments',
																								 NDS           VARCHAR2(100)  PATH 'VATrate',
																								 Row_num        VARCHAR2(100)  PATH 'RowNum') T)Z
						FULL OUTER JOIN (SELECT FDC.ID_CONTRACT,FDC.EXT_ID_FROM,FDC.EXT_ID_TO, FDC.ROW_NUM,FDC.ID_CLIENT, FDC.OPER_TYPE 
												FROM T_CLI_FINDOC_CONS FDC WHERE FDC.ID_CONTRACT = P_ID_CONTRACT
												AND FDC.DOC_DATE BETWEEN NVL(P_DATE_BEG,DATE '2000-01-01') AND NVL(P_DATE_END, DATE '3000-01-01')) C
						ON (Z.EXT_ID_FROM = C.EXT_ID_FROM AND Z.EXT_ID_TO = C.EXT_ID_TO AND Z.ID_CONTRACT = C.ID_CONTRACT AND C.ROW_NUM = Z.ROW_NUM))X
	ON (X.EXT_ID_FROM = FC.EXT_ID_FROM AND X.EXT_ID_TO = FC.EXT_ID_TO AND X.ID_CONTRACT = FC.ID_CONTRACT AND X.ROW_NUM = FC.ROW_NUM)
	WHEN MATCHED THEN 
		UPDATE SET FC.OPER_TYPE       = X.OPER_TYPE
							,FC.AMOUNT          = X.AMOUNT
							,FC.DATE_CON        = X.BILLING_DATE
							,FC.DATE_UPDATE     = SYSDATE
							,FC.DOC_COM         = X.DOC_COM
							,FC.DOC_NUMBER      = X.DOC_NUMBER
							,FC.DOC_DATE        = X.DOC_DATE
							,FC.DOC_TYPE        = X.DOC_TYPE
							,FC.DOC_CALC_COM    = X.DOC_CALC_COM
							,FC.DOC_CALC_NUMBER = X.DOC_CALC_NUMBER
							,FC.DOC_CALC_TYPE   = X.DOC_CALC_TYPE
							,FC.DOC_CALC_DATE   = X.DOC_CALC_DATE
							,FC.DEBT_TYPE       = X.TYPEDEBT
							,FC.PAYMENT_TYPE    = X.PAYMENTTYPE
							,FC.ORGANIZATION    = X.ORGANIZATION
							,FC.PAYMENT_DOC     = X.DOCPAYMENTS
							,FC.OPERATION       = X.TypeOperation
							,FC.NDS             = X.NDS
		DELETE WHERE X.DEL = 'Y'
	WHEN NOT MATCHED THEN 
		INSERT(ID,OPER_TYPE,AMOUNT,DATE_CON,DATE_LOAD,ID_CONTRACT,ID_CLIENT
					,EXT_ID_FROM,EXT_ID_TO,DOC_COM,DOC_NUMBER,DOC_DATE,DOC_TYPE,DOC_CALC_COM,DOC_CALC_NUMBER
					,DOC_CALC_TYPE,DOC_CALC_DATE,DEBT_TYPE,PAYMENT_TYPE,ORGANIZATION,PAYMENT_DOC,OPERATION,NDS,ROW_NUM)						
		VALUES(SEQ_FINDOC_CONS.NEXTVAL,X.OPER_TYPE,X.AMOUNT,X.BILLING_DATE,SYSDATE,X.ID_CONTRACT,X.ID_CLIENT,
					 X.EXT_ID_FROM,X.EXT_ID_TO,X.DOC_COM,X.DOC_NUMBER,X.DOC_DATE,X.DOC_TYPE,X.DOC_CALC_COM,X.DOC_CALC_NUMBER,
					 X.DOC_CALC_TYPE,X.DOC_CALC_DATE,X.TYPEDEBT,X.PAYMENTTYPE,X.ORGANIZATION,X.DOCPAYMENTS,X.TypeOperation,X.NDS,X.ROW_NUM);	
END;

FUNCTION F_GET_PROHIBITION_DATE RETURN DATE
	IS             
	L_CLOB CLOB;
	L_URL  VARCHAR2(500);
	L_DATE DATE;
BEGIN
	L_URL := G$URL || '/ProhibitionDate';  		 
	--Парсим XML
	L_CLOB := PKG_WEB.F_SEND_REQUEST(P_URL => L_URL, P_METHOD => 'GET',
																	 P_USER => G$USER, P_PWD => G$PWD);
														 
	L_DATE := TO_DATE(SUBSTRB(TO_CHAR(L_CLOB),4),'YYYY-MM-DD"T00:00:00"');															 
  RETURN L_DATE;												 	                                    
END; 

--------------------------------------------------------------------------------------

PROCEDURE P_GET_FINDOC_DETAILS(P_ID_CLIENT NUMBER) 
	IS
	L_XML    XMLTYPE;  
	L_URL    VARCHAR2(500);                                                
	L_CLOB   CLOB;        
	L_FMT    VARCHAR2(100) := 'FM99999999999999999D99999999999999999999';
	L_NLS    VARCHAR2(100) := 'NLS_NUMERIC_CHARACTERS=''. ''';
BEGIN
	-- Достаем URL  
	L_URL := G$URL || '/TariffVolumeData';  
	
	SELECT XMLELEMENT("invoices", XMLAGG(XMLELEMENT("invoice", FD.EXT_ID)))
	INTO L_XML FROM  T_CLI_FINDOCS FD WHERE  FD.ID_CLIENT = P_ID_CLIENT;
	
	IF L_XML.EXISTSNODE('invoices/invoice') != 1 THEN
		RETURN;
	END IF;
		 
	--Парсим XML
	L_CLOB := PKG_WEB.F_SEND_REQUEST(P_URL => L_URL, P_METHOD => 'POST',
																	 P_USER => G$USER, P_PWD => G$PWD
																	,P_REQ => L_XML.getClobVal);
	
	DELETE FROM T_CLI_FINDOCS_DETAILS D WHERE D.EXT_ID IN
	(SELECT FD.EXT_ID FROM T_CLI_FINDOCS FD WHERE FD.ID_CLIENT = P_ID_CLIENT);																
	
	INSERT INTO T_CLI_FINDOCS_DETAILS(EXT_ID,VOLUME,TARIF,NDS)
	SELECT C.EXT_ID,TO_NUMBER(X.VOLUME,L_FMT,L_NLS),TO_NUMBER(X.TARIF,L_FMT,L_NLS),TO_NUMBER(X.VATRate,L_FMT,L_NLS)
  FROM   XMLTABLE('/TariffVolumeData/invoice' PASSING
								XMLTYPE(L_CLOB) 
								COLUMNS EXT_ID   VARCHAR2(100) PATH'@UIDInvoice',
								        TARIF_ROW XMLTYPE PATH 'TariffVolume') C
			,XMLTABLE('/TariffVolume' 
			          PASSING TARIF_ROW 
								COLUMNS VOLUME VARCHAR2(100) PATH 'Volume', 
								        TARIF VARCHAR2(100) PATH 'Tariff',
												VATRate VARCHAR2(100) PATH 'VATRate') X
	WHERE X.VOLUME IS NOT NULL;												
END;

-------------------------------------------------------------------------------

PROCEDURE P_GET_FINDOC_DETAILS(P_UID UID) 
	IS
	L_XML    XMLTYPE;  
	L_URL    VARCHAR2(500);                                                
	L_CLOB   CLOB;        
	L_FMT    VARCHAR2(100) := 'FM99999999999999999D99999999999999999999';
	L_NLS    VARCHAR2(100) := 'NLS_NUMERIC_CHARACTERS=''. ''';
BEGIN
	-- Достаем URL  
	L_URL := G$URL || '/TariffVolumeData';  
	
	SELECT XMLELEMENT("invoices", XMLAGG(XMLELEMENT("invoice", COLUMN_VALUE)))
	INTO L_XML 
	FROM TABLE(P_UID); 

	
	IF L_XML.EXISTSNODE('invoices/invoice') != 1 THEN
		RETURN;
	END IF;
		 
	--Парсим XML
	L_CLOB := PKG_WEB.F_SEND_REQUEST(P_URL => L_URL, P_METHOD => 'POST',
																	 P_USER => G$USER, P_PWD => G$PWD
																	,P_REQ => L_XML.getClobVal);
	
	DELETE FROM T_CLI_FINDOCS_DETAILS D WHERE D.EXT_ID IN (SELECT COLUMN_VALUE FROM TABLE(P_UID));																
	
	INSERT INTO T_CLI_FINDOCS_DETAILS(EXT_ID,VOLUME,TARIF,NDS)
	SELECT C.EXT_ID,TO_NUMBER(X.VOLUME,L_FMT,L_NLS),TO_NUMBER(X.TARIF,L_FMT,L_NLS),TO_NUMBER(X.VATRate,L_FMT,L_NLS)
  FROM   XMLTABLE('/TariffVolumeData/invoice' PASSING
								XMLTYPE(L_CLOB) 
								COLUMNS EXT_ID   VARCHAR2(100) PATH'@UIDInvoice',
								        TARIF_ROW XMLTYPE PATH 'TariffVolume') C
			,XMLTABLE('/TariffVolume' 
			          PASSING TARIF_ROW 
								COLUMNS VOLUME VARCHAR2(100) PATH 'Volume', 
								        TARIF VARCHAR2(100) PATH 'Tariff',
												VATRate VARCHAR2(100) PATH 'VATRate') X
	WHERE X.VOLUME IS NOT NULL;												
END;


BEGIN
  G$URL  := PKG_PREF.F$C1('BIL_ADDRESS');
  G$USER := PKG_PREF.F$C1('BIL_USER');
  G$PWD  := PKG_PREF.F$C1('BIL_PWD');
end PKG_BILLING;
/

