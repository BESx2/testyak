i�?create or replace package lawmain.PKG_MEASURES is

  -- Author  : EUGEN
  -- Created : 10.01.2017 11:21:04
  -- Purpose : Пакет для работы с МЕРОПРИЯТИЯМИ по делам физических лиц.
  
	--Функция достаёт ид типа события по заданному коду события
  FUNCTION F_GET_MEASURE_TYPE_BY_CODE(P_CODE VARCHAR2) RETURN NUMBER;
	
	
	--Процедура создаёт события по делу юр. лица
	PROCEDURE P_CREATE_CLI_MEASURE(P_ID_CLIENT NUMBER,
                                 P_CODE    VARCHAR2,      
																 P_COMMENT VARCHAR2 DEFAULT NULL,
		                             P_ID_WORK NUMBER DEFAULT NULL,
				     								 	   P_DATE    DATE DEFAULT NULL,
															   P_AUTOR   NUMBER DEFAULT f$_usr_id);

	--Процедура изменяет дату события ЮЛ
		PROCEDURE P_CHANGE_CLI_MEASURE(P_ID_MEASURE NUMBER,
		                            P_COMMENT VARCHAR2 DEFAULT NULL,
															  P_DATE     DATE DEFAULT NULL,
															  P_CODE     VARCHAR2);
															 
															 
	PROCEDURE P_CREATE_MEASURE(P_MEASURE_NAME VARCHAR2
		                      ,P_MEASURE_DESC VARCHAR2
													,P_MEASURE_TYPE VARCHAR2);
													
	PROCEDURE P_DELETE_CLI_MEASURE(P_ID_MEASURE NUMBER);												
													
  PROCEDURE P_UPDATE_MEASURE(P_ID_MEASURE NUMBER,
		                       P_MEASURE_NAME VARCHAR2,
													 P_MEASURE_DESC VARCHAR2,
													 P_IS_ACTIVE  VARCHAR2);
													 										 
end PKG_MEASURES;
/

create or replace package body lawmain.PKG_MEASURES is

  PROCEDURE P_CREATE_MEASURE(P_MEASURE_NAME VARCHAR2
		                        ,P_MEASURE_DESC VARCHAR2
													  ,P_MEASURE_TYPE VARCHAR2)
  	IS
		L_CODE VARCHAR2(50);
  BEGIN
    L_CODE := REPLACE(SUBSTR(PKG_UTILS.F_MAKE_TRANSLIT(P_MEASURE_NAME),1,50),' ','_');
		INSERT INTO T_CLI_MEASURE_TYPES(ID_MEASURE_TYPE,CODE_MEASURE,SHORT_NAME,MEASURE_DESC,TYPE_MEASURE,CREATED_BY,DATE_CREATED)
    VALUES(SEQ_EVENTS.NEXTVAL,L_CODE,P_MEASURE_NAME,P_MEASURE_DESC,P_MEASURE_TYPE,F$_USR_ID,SYSDATE);
	END;							
	
	PROCEDURE P_UPDATE_MEASURE(P_ID_MEASURE NUMBER,
		                       P_MEASURE_NAME VARCHAR2,
													 P_MEASURE_DESC VARCHAR2,
													 P_IS_ACTIVE  VARCHAR2)
	  IS
	BEGIN
		 UPDATE T_CLI_MEASURE_TYPES T
		 SET T.MEASURE_DESC = P_MEASURE_DESC,
		     T.SHORT_NAME = P_MEASURE_NAME,
				 T.IS_ACTIVE  = P_IS_ACTIVE,
				 T.MODIFIED_BY = F$_USR_ID,
				 T.DATE_MODIFIED = SYSDATE
		 WHERE T.ID_MEASURE_TYPE = P_ID_MEASURE;
	END;												 

	--Функция достаёт ид типа события по заданному коду события
	FUNCTION F_GET_MEASURE_TYPE_BY_CODE(P_CODE VARCHAR2) RETURN NUMBER 
	IS
		L_RET NUMBER;
	BEGIN                 
		--Достаём ид события
	  SELECT ID_MEASURE_TYPE
		INTO L_RET 
		FROM T_CLI_MEASURE_TYPES 
		WHERE CODE_MEASURE = P_CODE;
		 
		RETURN L_RET;
  EXCEPTION 
			WHEN NO_DATA_FOUND THEN
					RAISE_APPLICATION_ERROR(-20200,'Тип события не найден');
	END;                                                       
	
	--------------------------------------------------------------------------
	
	--Процедура создаёт события для заданного дела по заданному коду события
	
	-----------------------------------------------------------------------------
	
	--Процедура создаёт события по делу юр. лица
	PROCEDURE P_CREATE_CLI_MEASURE(P_ID_CLIENT NUMBER,
                                 P_CODE    VARCHAR2,      
																 P_COMMENT VARCHAR2 DEFAULT NULL,
		                             P_ID_WORK NUMBER DEFAULT NULL,
				     								 	   P_DATE    DATE DEFAULT NULL,
															   P_AUTOR   NUMBER DEFAULT f$_usr_id)
	IS                                       
	L_TYPE_ID NUMBER;
	BEGIN            
		 --Ид события
		 L_TYPE_ID := F_GET_MEASURE_TYPE_BY_CODE(P_CODE);
		 --Создание события
		 INSERT INTO T_CLI_MEASURES(ID_MEASURE,ID_WORK,ID_MEASURE_TYPE,DATE_CREATED ,CREATED_BY, DATE_MEASURE,ID_CLIENT,COMMENTARY)
		 VALUES(SEQ_CLI_MEASURE.NEXTVAL,P_ID_WORK,L_TYPE_ID,SYSDATE,P_AUTOR,NVL(P_DATE,SYSDATE),P_ID_CLIENT,P_COMMENT);
	END;
	
	-----------------------------------------------------------------------------------
	
	
	--Процедура изменяет дату события ЮЛ
	PROCEDURE P_CHANGE_CLI_MEASURE(P_ID_MEASURE NUMBER,
		                            P_COMMENT VARCHAR2 DEFAULT NULL,
															  P_DATE     DATE DEFAULT NULL,
															  P_CODE     VARCHAR2)															 															 
	  IS                                       
	  L_TYPE_ID NUMBER;
	BEGIN            
		 --Ид события
		 L_TYPE_ID := F_GET_MEASURE_TYPE_BY_CODE(P_CODE);
		 
		 UPDATE T_CLI_MEASURES EV
		 SET EV.DATE_MEASURE = NVL(P_DATE,SYSDATE),
		 		 EV.ID_MEASURE_TYPE = L_TYPE_ID,                      
				 EV.COMMENTARY = P_COMMENT,
				 EV.MODIFIED_BY = F$_USR_ID,
				 EV.DATE_MODIFIED = SYSDATE
		 WHERE EV.ID_MEASURE = P_ID_MEASURE; 
	END;                                           

---------------------------------------------------------------
	
	PROCEDURE P_DELETE_CLI_MEASURE(P_ID_MEASURE NUMBER)
		IS
	BEGIN
		DELETE FROM T_CLI_MEASURES EV WHERE EV.ID_MEASURE = P_ID_MEASURE;
	END;
	                                                          
	
	-----------------------------------------------------------------------------------
	                                                                                   
														
	END PKG_MEASURES;
/

