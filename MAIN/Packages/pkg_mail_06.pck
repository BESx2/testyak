i�?CREATE OR REPLACE PACKAGE LAWMAIN.PKG_MAIL_06
IS
/* Автор: Бородулин Е.С.
   Дата:  21.12.2016
	 Назначение: Пакет отсылает email на соблюдение гаранитровачнгого письма
*/

   PROCEDURE p_send_mail(P_ID_CLIENT NUMBER, P_EMAIL VARCHAR2, P_ATTACH VARCHAR2 DEFAULT NULL);

END;
/

CREATE OR REPLACE PACKAGE BODY LAWMAIN.PKG_MAIL_06
IS

	G$HTML VARCHAR2(32000);

  PROCEDURE p_send_mail( P_ID_CLIENT NUMBER, P_EMAIL VARCHAR2, P_ATTACH VARCHAR2)
        IS
			L_RET    NUMBER;
			L_REP    LAWSUP.PKG_FILES.REC_DOC;
      L_PHONE  VARCHAR2(100);
    BEGIN                                                        
			
		  SELECT UL.PHONE
			INTO  L_PHONE
			FROM T_CLIENTS S, T_USER_LIST UL 
			WHERE S.CURATOR_PRE = UL.ID                  
			AND   S.ID_CLIENT = P_ID_CLIENT;
			
      G$HTML := PKG_PREF.F$HTML('GARANT_MAILING');
			G$HTML := REPLACE(G$HTML,'#PHONE#',L_PHONE); 

           
			L_RET := PKG_MAIL.F_MAKE_EMAIL(P_HEADER => PKG_PREF.F$C1('GARANT_MAILING')
																		 ,P_FROM  => PKG_PREF.F$C1('USER_MAIL') -- do not change !!!! if change => change password in sup.pkg_mail!!!
																		 ,P_BODY  => G$HTML
																		);
			PKG_MAIL.P_ATTACHE_EMAIL_ADDR(P_EMAIL      => P_EMAIL
																	 ,P_ID_MAIL    => L_RET
																	 ,P_EMAIL_TYPE => PKG_MAIL.LIST_EMAIL_TYPE.TO_);
																				 
				FOR J IN (SELECT COLUMN_VALUE FROM TABLE(APEX_STRING.split(P_ATTACH,':')))																 
				LOOP
						L_REP := PKG_CONTROLLER.P_RETURN_BLOB(P_REP_CODE => J.COLUMN_VALUE
						                                     ,P_PAR_01 => P_ID_CLIENT);    
																								 
						PKG_MAIL.P_ATTACHE_FILE(P_ID_MAIL => L_RET
						                       ,P_BLOB => L_REP.P_BLOB
																	 ,P_FILE_NAME => L_REP.P_FILE_NAME);																		 
				END LOOP;

        PKG_MAIL.P_SEND(P_ID => L_RET,P_SEND_TYPE => PKG_MAIL.LIST_TYPE_SEND.LTE);
				PKG_SEND_MAIL.P_INSERT_CLI_MAIL(P_ID_CLIENT,L_RET,'GARANT_MAILING' );
 				COMMIT; 
 	 EXCEPTION 
			WHEN NO_DATA_FOUND THEN
				RAISE_APPLICATION_ERROR(-20200,'Необходимо указать ответственного по делу абонента');	
    END;    		
END;
/

