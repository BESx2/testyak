i�?create or replace package lawmain.PKG_CLI_CASES is

  -- Author  : EUGEN
  -- Created : 14.12.2016 15:11:06
  -- Purpose : Пакет для работы с делами юр. лиц
	
 FUNCTION F_CREATE_CLI_CASE(P_CONTRACTS   wwv_flow_t_varchar2,
                            P_DATE_FROM   DATE DEFAULT NULL,
														P_DATE_TO     DATE DEFAULT NULL,									
                            P_DATE_DEBT   DATE) RETURN NUMBER;
	
		
 FUNCTION F_CREATE_CLI_CASE(P_ID_CONTRACT    NUMBER                 --ДОГОВОР
                           ,P_PER_START      DATE DEFAULT NULL      --НАЧАЛО ЗАДОЛЖЕННОСТИ
                           ,P_PER_END        DATE DEFAULT NULL      --КОНЕЦ ПЕРИОДА
                           ,P_CIPHERS        VARCHAR2 DEFAULT NULL  --ШИФРЫ
                           ,P_CURATOR_PRE    NUMBER DEFAULT NULL    --КУРАТОР ДОСУДЕБКИ
                           ,P_CURATOR_COURT  NUMBER DEFAULT NULL    --КУРАТОР СУДА
                           ,P_CURATOR_FSSP   NUMBER DEFAULT NULL    --КУРАТОР ФССП
                           ,P_PRE_TRIAL_DATE DATE DEFAULT NULL      --ДАТА ПЕРЕДАЧИ В СУДЕБКУ
                    			 ,P_COURT_DATE     DATE DEFAULT NULL      --ДАТА ПЕРЕДАЧИ В СУД
													 ,P_FSSP_DATE      DATE DEFAULT NULL      --ДАТА ПЕРЕДАЧИ В ФССП
													 ,P_DEP            VARCHAR2 DEFAULT NULL  --ТЕКУЩИЙ ОТДЕЛ
													 ,P_PRETEN_DATE    DATE DEFAULT NULL      --Дата в претензии
													 ,P_PRETEN_TYPE    VARCHAR2 DEFAULT 'REGULAR'
  												 ,P_CASE_TYPE      VARCHAR2 DEFAULT 'REGULAR') 
													  RETURN NUMBER;
		
	 FUNCTION F_GET_DEBT_ON_DATE(P_ID_CASE NUMBER, P_DATE DATE) RETURN NUMBER;														 																 
 	 PROCEDURE P_DELETE_CASE(P_ID_CASE NUMBER);
 	 FUNCTION F_GET_CLI_CASE_DEBT(P_ID_CASE NUMBER) RETURN NUMBER;
		
		PROCEDURE P_REFRESH_DATA(P_CASE_ID NUMBER);
		
	  PROCEDURE P_CHANGE_STATUS(P_ID_CLI_CASE NUMBER
												 	   ,P_STATUS VARCHAR2
														 	,P_DATE   DATE DEFAULT SYSDATE);
														 

	--засчитать оплату пени/пошлины		
		PROCEDURE P_ADD_PENY_TOLL_PAYMENT(P_ID_CASE NUMBER,
																			P_PENY NUMBER DEFAULT 0,
																			P_TOLL NUMBER DEFAULT 0,
																			P_DATE DATE,
																			P_PENY_FACT NUMBER DEFAULT 0);																																																 
		
  --Процедура создаёт выездное мероприятие
		PROCEDURE P_CREATE_OFFSITE_CLI_EVENT(P_ID_CASE 		NUMBER,                 --Ид дела
														 						 P_EVENT      VARCHAR2,               --Код мероприятия
																				 P_COMMENTARY VARCHAR2 DEFAULT NULL); --Комментарий   
																				 
   PROCEDURE P_SEND_TO_COURT(P_ID_CASE NUMBER);
															
		
		--Процедура обновляет пени по делу юр. лица
	PROCEDURE P_EVALUATE_PENY(P_ID_CASE NUMBER,
											  	  P_DATE    DATE DEFAULT SYSDATE,
														P_GROUP   VARCHAR2 DEFAULT NULL);
	                                 
		--Пени по день фактической оплаты												
		PROCEDURE P_EVALUATE_PENY_FACT(P_ID_CASE NUMBER,
		                               P_DATE    DATE DEFAULT SYSDATE);

															
		--Куратор судебки
    PROCEDURE P_ASSIGN_CURATOR_COURT(P_ID_CASE NUMBER, P_CURATOR NUMBER);
		
		--Куратор ФССП
		PROCEDURE P_ASSIGN_CURATOR_FSSP(P_ID_CASE NUMBER, P_CURATOR NUMBER);													                                            
		
		--Процедура считает госпошлину по делу
		PROCEDURE P_EVALUATE_GOV_TOLL (P_ID_CASE NUMBER,P_TYPE VARCHAR2);
		
		--Процедура закрывает дело.
		PROCEDURE P_CLOSE_CASE(P_ID_CASE NUMBER
													,P_CODE_REASON VARCHAR2
													,P_COMMENTARY VARCHAR2 DEFAULT NULL);	
		
		FUNCTION F_GET_STATUS(P_ID_CASE NUMBER) RETURN VARCHAR2;		
		
		--Процедура передаёт дело в ФССП
  	PROCEDURE P_SEND_TO_FSSP(P_ID_CASE NUMBER);		
																	
		
	  --Процедура возвращает дело в работу после закрытия
	  PROCEDURE P_RETURN_TO_WORK(P_ID_CLI_CASE NUMBER);															 

	--Процедура обновляет значения поля  P_COLUNM на P_VAL для дела с id  = P_ID_CASE
		PROCEDURE P_UPDATE_VALUE(P_ID_CASE NUMBER, P_COLUNM VARCHAR2, P_VAL VARCHAR2);
	--ПРОЦЕДУРА ДОБАВЛЯЕТ ПРЕТЕНЗИЮ В ДЕЛО
		PROCEDURE P_ADD_PRETENSION(P_ID_CASE NUMBER
		                        ,P_EVENT BOOLEAN DEFAULT TRUE
														,P_USER  NUMBER DEFAULT f$_usr_id);	
		
		--ОБЪЕДИНЕНИЕ ДЕЛ												
		FUNCTION F_MERGE_CASES(P_CASES wwv_flow_global.vc_arr2) RETURN NUMBER;																												 
end PKG_CLI_CASES;
/

create or replace package body lawmain.PKG_CLI_CASES is
 
 
 FUNCTION F_GET_DEBT_ON_DATE(P_ID_CASE NUMBER, P_DATE DATE) RETURN NUMBER
	 IS        

	 L_RET NUMBER;
 BEGIN	 
	  SELECT  NVL(SUM(DECODE(FDC.OPER_TYPE,'Приход',FDC.AMOUNT,-FDC.AMOUNT)),0)
		INTO L_RET  		
		FROM T_CLI_DEBTS CCD
		     ,T_CLI_FINDOCS FD
				 ,T_CLI_FINDOC_CONS FDC
		WHERE CCD.ID_WORK = P_ID_CASE 		 
		AND   FD.ID_DOC = CCD.ID_DOC
		AND   FDC.EXT_ID_TO = FD.EXT_ID
		AND   FDC.DATE_CON <= P_DATE;
		
  	RETURN L_RET; 
 EXCEPTION
		WHEN OTHERS THEN
			RETURN 0;
 END;
 
 --ПРОЦЕДУРА СОЗДАЁТ ПРЕТЕНЗИИ ИЗ "РЕЕСТРЫ ПРЕТЕНЗИЙ"
  FUNCTION F_CREATE_CLI_CASE(P_CONTRACTS   wwv_flow_t_varchar2,
                            P_DATE_FROM   DATE DEFAULT NULL,
														P_DATE_TO     DATE DEFAULT NULL,								
                            P_DATE_DEBT   DATE) RETURN NUMBER
    IS                                         
    L_DEBT_CREATED NUMBER := 0; 
    L_DEBT_CURRENT NUMBER := 0; 
    L_ID_CTR_CWS   NUMBER;
    L_ID_CTR_SEW   NUMBER;   
    L_TYPE         VARCHAR2(100);
    L_ID_CASE      NUMBER;
    L_ID_CLIENT NUMBER;   
		L_PREF      VARCHAR2(100);   
		L_GROUP     VARCHAR2(50);
  BEGIN                                                            
    --Определяем договор
    FOR I IN 1..P_CONTRACTS.COUNT LOOP
      SELECT CT.CTR_TYPE, CT.ID_CLIENT INTO L_TYPE, L_ID_CLIENT
      FROM T_CONTRACTS CT WHERE CT.ID_CONTRACT = P_CONTRACTS(I);
      
      IF L_TYPE = 'Основной в ХВС' THEN
        L_ID_CTR_CWS := TO_NUMBER(P_CONTRACTS(I));
      ELSE
        L_ID_CTR_SEW := TO_NUMBER(P_CONTRACTS(I));
      END IF;                                  
    END LOOP;
        
    INSERT INTO T_CLI_DEBT_WORK(ID_WORK,ID_CLIENT,TYPE_WORK,DEPT,DATE_CREATED,CREATED_BY)
    VALUES(SEQ_CLI_DEBT_WORK.NEXTVAL,L_ID_CLIENT,'CASE','PRE_TRIAL',SYSDATE,F$_USR_ID)
    RETURNING ID_WORK INTO L_ID_CASE;
  
    FOR i IN (SELECT *
              FROM   (SELECT FD.ID_DOC,FD.ID_CONTRACT,FD.ID_CLIENT,CG.ABON_GROUP, 
			                       DECODE(CG.ABON_GROUP,'UK','УК','ТСЖ','TSG','GOV','БДЖ','ПР') PREF 
                            ,(SELECT NVL(SUM(DECODE(C.OPER_TYPE, 'Приход', C.AMOUNT,-C.AMOUNT)), 0)
                              FROM   T_CLI_FINDOC_CONS C WHERE  C.EXT_ID_TO = FD.EXT_ID
                              AND    C.DATE_CON <= P_DATE_DEBT) DEBT_ON_DATE
                            ,FD.DEBT
                      FROM   T_CLI_FINDOCS FD,
											       T_CONTRACTS CT,
                             T_CUSTOMER_GROUP CG 
                      WHERE FD.OPER_TYPE = 'Приход'  
											AND   TRUNC(FD.DOC_DATE,'MM') BETWEEN NVL(P_DATE_FROM,DATE '2000-01-01') AND NVL(P_DATE_TO, DATE '3000-01-01') 
											AND   CT.ID_CONTRACT = FD.ID_CONTRACT
							        AND   CT.CUST_GROUP = CG.GROUP_NAME   
											AND    NOT EXISTS (SELECT 1 FROM T_CLI_DEBTS D WHERE D.DEPT != 'PRE_TRIAL' AND FD.ID_DOC = D.ID_DOC) 
                      AND FD.ID_CONTRACT IN (SELECT TO_NUMBER(COLUMN_VALUE) FROM TABLE(P_CONTRACTS)))
              WHERE  DEBT_ON_DATE > 0)
    LOOP            
      L_DEBT_CREATED := I.DEBT_ON_DATE + L_DEBT_CREATED;
      L_DEBT_CURRENT := I.DEBT + L_DEBT_CURRENT;
      L_GROUP := I.ABON_GROUP;
			L_PREF := I.PREF;
			L_ID_CLIENT := I.ID_CLIENT;
			
      INSERT INTO T_CLI_DEBTS(ID_DEBT,ID_WORK,ID_DOC,DATE_CREATED,CREATED_BY,ID_CONTRACT,ID_CLIENT,DEBT_CURRENT,DEPT,DEBT_CREATED)
      VALUES(SEQ_CLI_CASE_DEBTS.NEXTVAL,L_ID_CASE,I.ID_DOC,SYSDATE,F$_USR_ID,I.ID_CONTRACT,I.ID_CLIENT,I.DEBT,'PRE_TRIAL',I.DEBT_ON_DATE);
		END LOOP;					
	
					
	  INSERT INTO T_CLI_CASES(ID_CASE,ID_CLIENT,CASE_NAME,DATE_CREATED,CREATED_BY,DEP,CURATOR_PRE)
    VALUES(L_ID_CASE, L_ID_CLIENT, TO_CHAR(L_ID_CASE),SYSDATE,F$_USR_ID,'PRE_TRIAL',F$_USR_ID);
  
	  UPDATE T_CLI_DEBT_WORK W
		   SET W.DEBT_CREATED = L_DEBT_CURRENT,
			     W.CURRENT_DEBT = L_DEBT_CURRENT
		 WHERE W.ID_WORK = L_ID_CASE;                                                                 
		
    UPDATE T_CLI_CASES S
    SET   S.SUM_DEBT  = L_DEBT_CURRENT,
		      S.CASE_NAME = L_PREF||'-'||S.CASE_NAME,
          S.DEBT_PRE_TRIAL  = L_DEBT_CURRENT,
          S.DEBT_PRETENSION = L_DEBT_CREATED,
          S.DATE_PRETENSION = P_DATE_DEBT,
					S.ID_CWS_CONTRACT = L_ID_CTR_CWS,
					S.ID_SEW_CONTRACT = L_ID_CTR_SEW,
					S.GROUP_CODE =  L_GROUP					                
    WHERE S.ID_CASE = L_ID_CASE;     
		
		PKG_CLI_CASES.P_CHANGE_STATUS(L_ID_CASE,'PRET_PROCESS');
    PKG_EVENTS.P_CREATE_CLI_EVENT(P_ID_WORK => L_ID_CASE,P_CODE => 'CREATE');
  
	 RETURN L_ID_CASE; 
	END;                      
 
 FUNCTION F_CREATE_CLI_CASE(P_ID_CONTRACT    NUMBER
							 						 ,P_PER_START      DATE DEFAULT NULL
													 ,P_PER_END        DATE DEFAULT NULL
													 ,P_CIPHERS        VARCHAR2 DEFAULT NULL
													 ,P_CURATOR_PRE    NUMBER DEFAULT NULL
													 ,P_CURATOR_COURT  NUMBER DEFAULT NULL
													 ,P_CURATOR_FSSP   NUMBER DEFAULT NULL
													 ,P_PRE_TRIAL_DATE DATE DEFAULT NULL
													 ,P_COURT_DATE     DATE DEFAULT NULL
													 ,P_FSSP_DATE      DATE DEFAULT NULL
													 ,P_DEP            VARCHAR2 DEFAULT NULL
													 ,P_PRETEN_DATE    DATE DEFAULT NULL
													 ,P_PRETEN_TYPE    VARCHAR2 DEFAULT 'REGULAR'
													 ,P_CASE_TYPE      VARCHAR2 DEFAULT 'REGULAR') RETURN NUMBER 
   IS
		TYPE DEBT_INVOICES IS TABLE OF NUMBER INDEX BY PLS_INTEGER;
		L_DEBT      DEBT_INVOICES;
		NO_DEBTS    EXCEPTION;
		L_ID_CASE   NUMBER;
		L_INDEX     NUMBER;
		L_SUM       NUMBER := 0;
		L_CTR_NUM   VARCHAR2(50);
		L_ID_CLIENT NUMBER;                
		L_PREF      VARCHAR2(100);   
		L_DEBT_PRE_TRIAL  NUMBER;
		L_DEBT_COURT      NUMBER;
		L_DEBT_FSSP       NUMBER;
		L_DEBT_PRETENSION NUMBER;
	BEGIN
		SELECT CT.CTR_NUMBER
		      ,CT.ID_CLIENT
					,NULL/*(SELECT DECODE(CG.ABON_GROUP,'UK','ДК','GOV','БДЖ','OTHER','ПР','TSG','ТСЖ') 
					  FROM T_CUSTOMER_GROUP CG WHERE CG.ID_GROUP = CLI.ID_GROUP) PREF*/
		INTO L_CTR_NUM, L_ID_CLIENT, L_PREF
		FROM T_CONTRACTS CT, T_CLIENTS CLI	
		WHERE CT.ID_CONTRACT = P_ID_CONTRACT
		AND   CT.ID_CLIENT = CLI.ID_CLIENT;
		
		--долги			
		FOR I IN (SELECT DEB.ID_DOC	
			              ,DEB.DEBT
							FROM  T_CLI_FINDOCS  DEB
							WHERE DEB.ID_CONTRACT = P_ID_CONTRACT
							-- за заданный период
							AND trunc(DEB.doc_date,'MM') BETWEEN NVL(P_PER_START,DATE '2000-01-01') AND NVL(P_PER_END,DATE '3000-01-01')
							-- но не больше даты на которую хотим выпустить претензию, тут без транка
							AND (deb.doc_date <= P_PRETEN_DATE OR P_PRETEN_DATE IS NULL)
							--по заданным шифрам
							-- и которые не в суде или илспол производстве
							AND NOT EXISTS (SELECT 1 FROM T_CLI_DEBTS CCD, T_CLI_CASES CC 
							                WHERE CCD.ID_DOC = DEB.ID_DOC AND CC.ID_CASE = CCD.ID_WORK)
							-- и не те которые ещё не обработаны
  						AND NOT EXISTS (SELECT 1
                              FROM T_FOLDER_CLI_CASES C
                                  ,V_FOLDERS F
                                  ,T_CLI_DEBTS D
                              WHERE F.ID_FOLDER = C.ID_FOLDER
                              AND   F.TYPE_CODE IN ('PRE_TRIAL_PRETEN','PRE_TRIAL_ODN')
                              AND   F.CODE_STATUS IN ('FILLING','WORK')
                              AND   C.ID_CASE = D.ID_WORK
                              AND   DEB.ID_DOC = D.ID_DOC)
						  -- и только по актуальным
							AND DEB.debt > 0)
		LOOP
			L_DEBT(I.ID_DOC) := I.DEBT;
		END LOOP;
	
		IF L_DEBT.COUNT = 0 THEN
			RAISE NO_DEBTS;
		END IF;
	  
		INSERT INTO T_CLI_DEBT_WORK(ID_WORK,ID_CLIENT,TYPE_WORK,DEPT,DATE_CREATED,CREATED_BY)
		VALUES(SEQ_CLI_DEBT_WORK.NEXTVAL,L_ID_CLIENT,'CASE',NVL(P_DEP,'PRE_TRIAL'),SYSDATE,F$_USR_ID)
		RETURNING ID_WORK INTO L_ID_CASE;
		
		INSERT INTO T_CLI_CASES(ID_CASE,CASE_NAME,DATE_CREATED,CREATED_BY
		                       ,DEP,CURATOR_PRE,CURATOR_COURT,CURATOR_FSSP)
		VALUES(L_ID_CASE,L_PREF || '-' || L_ID_CASE,SYSDATE
		      ,F$_USR_ID,NVL(P_DEP,'PRE_TRIAL'),P_CURATOR_PRE,P_CURATOR_COURT,P_CURATOR_FSSP);
		
	
		L_INDEX := L_DEBT.FIRST;
		WHILE L_INDEX IS NOT NULL
		LOOP
			INSERT INTO T_CLI_DEBTS(ID_DEBT,ID_WORK,ID_DOC,DATE_CREATED,CREATED_BY
			                             ,ID_CONTRACT,ID_CLIENT,DEBT_CURRENT,DEBT_CREATED,DEPT
																	 ,DATE_PRETRIAL,DEBT_PRETRIAL,DATE_COURT,DEBT_COURT
																	 ,DATE_FSSP,DEBT_FSSP)
			VALUES(SEQ_CLI_CASE_DEBTS.NEXTVAL,L_ID_CASE,L_INDEX,SYSDATE,F$_USR_ID,P_ID_CONTRACT
			      ,L_ID_CLIENT,L_DEBT(L_INDEX),L_DEBT(L_INDEX),NVL(P_DEP,'PRE_TRIAL')
						,P_PRE_TRIAL_DATE,NVL2(P_PRE_TRIAL_DATE,PKG_FINDOC.F_GET_FINDOC_DEBT_ON_DATE(L_INDEX,P_PRE_TRIAL_DATE),NULL)
						,P_COURT_DATE,NVL2(P_COURT_DATE,PKG_FINDOC.F_GET_FINDOC_DEBT_ON_DATE(L_INDEX,P_COURT_DATE),NULL)
						,P_FSSP_DATE,NVL2(P_FSSP_DATE,PKG_FINDOC.F_GET_FINDOC_DEBT_ON_DATE(L_INDEX,P_FSSP_DATE),NULL));
					
			L_SUM   := L_SUM + L_DEBT(L_INDEX);
			L_INDEX := L_DEBT.NEXT(L_INDEX);
		END LOOP;
	  
		IF P_PRE_TRIAL_DATE IS NOT NULL THEN
  		L_DEBT_PRE_TRIAL  := F_GET_DEBT_ON_DATE(L_ID_CASE,P_PRE_TRIAL_DATE);
		ELSE
			L_DEBT_PRE_TRIAL  := L_SUM;
		END IF;                                                          
		
		IF P_COURT_DATE IS NOT NULL THEN
		  L_DEBT_COURT      := F_GET_DEBT_ON_DATE(L_ID_CASE,P_COURT_DATE);	
		END IF;
    
		IF P_FSSP_DATE IS NOT NULL THEN
			L_DEBT_FSSP := F_GET_DEBT_ON_DATE(L_ID_CASE,P_FSSP_DATE);
		END IF;
		
    IF P_PRETEN_DATE IS NOT NULL THEN
		  L_DEBT_PRETENSION := F_GET_DEBT_ON_DATE(L_ID_CASE,P_PRETEN_DATE);
		END IF;
		
		UPDATE T_CLI_DEBT_WORK W
		   SET W.DEBT_CREATED = L_SUM,
			     W.CURRENT_DEBT = L_SUM
		 WHERE W.ID_WORK = L_ID_CASE;
		
		UPDATE T_CLI_CASES S
		SET   S.SUM_DEBT  = L_SUM,
		      S.DEBT_PRE_TRIAL  = L_DEBT_PRE_TRIAL ,
		      S.DEBT_COURT      = L_DEBT_COURT     ,
		      S.DEBT_FSSP       = L_DEBT_FSSP      ,
          S.DEBT_PRETENSION = L_DEBT_PRETENSION,
					S.DATE_PRETENSION = P_PRETEN_DATE
		WHERE S.ID_CASE = L_ID_CASE;
  
    PKG_CLI_CASES.P_CHANGE_STATUS(L_ID_CASE,'WORK');
    PKG_EVENTS.P_CREATE_CLI_EVENT(P_ID_WORK => L_ID_CASE,P_CODE => 'CREATE');
		
		
    RETURN L_ID_CASE;
  EXCEPTION
    WHEN NO_DEBTS THEN
      RAISE_APPLICATION_ERROR(-20200,'Отсутствуют долги по указанному договору, за указанный период: ' ||L_CTR_NUM);
    WHEN OTHERS THEN
    	RAISE_APPLICATION_ERROR(-20200,SQLERRM);
	END;    

------------------------------------------------------------------------------------------
	
	PROCEDURE P_DELETE_CASE(P_ID_CASE NUMBER)
		IS
	BEGIN
		 FOR I IN (SELECT F.ID_LINK FROM T_LINK_FILE_CLI_WORK F WHERE F.ID_WORK = P_ID_CASE)
		 LOOP
				PKG_FILES.P_DELETE_CLI_CASE_DOC(P_ID_LINK => I.ID_LINK);
		 END LOOP;  			
		 DELETE FROM T_CLI_EVENTS ev WHERE ev.id_work = P_ID_CASE;
		 DELETE FROM T_CLI_DEBTS ccd WHERE ccd.ID_WORK = P_ID_CASE;
		 DELETE FROM T_FOLDER_CLI_CASES fc WHERE fc.id_case = P_ID_CASE;
		 DELETE FROM T_CLI_CASES s WHERE s.id_case = P_ID_CASE;
	END;
	
	--Процедура соединяет дела
/*	PROCEDURE P_MERGE_COURT_CASES(P_ID_CASE_F NUMBER  --В какое дело переносим
															 ,P_ID_CASE_S NUMBER) --Из какого
		IS                            
		l_cnt NUMBER;
		WRONG_CASES EXCEPTION;
		l_case_name_f t_cli_cases.case_name%TYPE;
	BEGIN   
     -- Проверяем чтобы дела были одного и того же типа
		 -- И чтобы по одному и тому же договору																												 
     SELECT COUNT(1)
		 INTO l_cnt
		 FROM T_CLI_CASES s1
		 		 ,T_CLI_CASES s2															
		 WHERE s1.id_contract = s2.id_contract
		 AND   s1.case_type = s2.case_type
		 AND   s1.id_case = P_ID_CASE_F
		 AND   s2.id_case = P_ID_CASE_S;
		 
		 IF L_CNT = 0 THEN
			RAISE WRONG_CASES;
		 ELSE	 
			 -- Переносим долги
			 UPDATE T_CLI_CASE_DEBTS d SET d.id_case = P_ID_CASE_F WHERE d.id_case = P_ID_CASE_S;
			 -- Переносим суммы задолженности
			 UPDATE T_CLI_CASES s 
			 SET s.debt_court = s.debt_court + (SELECT s2.debt_court FROM T_CLI_CASES s2 WHERE s2.id_case = P_ID_CASE_S) 
			 		,s.sum_debt = F_GET_CLI_CASE_DEBT(P_ID_CASE_F)
			 WHERE s.id_case = P_ID_CASE_F
			 RETURNING s.case_name INTO l_case_name_f;
			 -- Ставим на закрытое дело метку объединения
			 UPDATE T_CLI_CASES s 
			 SET s.is_merged = P_ID_CASE_F
			 WHERE s.id_case = P_ID_CASE_S;          			 
			 P_CLOSE_CASE(P_ID_CASE_S,'MERGE', 'Дело объединено с '||l_case_name_f);   
			 PKG_EVENTS.P_CREATE_CLI_EVENT(P_ID_CASE_S,'CLOSED');
			--переносим ссылки на прикрепленные ранее документы			
				UPDATE t_doc_file f
				SET f.id_link = TO_CHAR(P_ID_CASE_F)
				WHERE f.id_link = TO_CHAR(P_ID_CASE_S)
				AND f.link_type IN('URVIV_UL', 'CLI_CASE', 'COURT_CASE', 'FSSP_CLI_CASE', 'DOP_AGREE', 'DOP_PAY', 'IVR');
			 
		 END IF;
	EXCEPTION
		WHEN WRONG_CASES THEN
			RAISE_APPLICATION_ERROR(-20200,'Невозможно объеденить два дела.');	 
	END;   */
	

	

	
	----------------------------------------------------------------------
PROCEDURE P_ASSIGN_CURATOR_COURT(P_ID_CASE NUMBER, P_CURATOR NUMBER)
	 IS       
	 L_CUR_ST VARCHAR2(100);
	 L_CUR_CURATOR NUMBER;
	BEGIN
			UPDATE T_CLI_CASES s SET s.curator_COURT = P_CURATOR WHERE s.id_case = P_ID_CASE;		
			IF P_CURATOR IS NOT NULL THEN
  			PKG_EVENTS.P_CREATE_CLI_EVENT(P_ID_WORK => P_ID_CASE,P_CODE => 'CUR_ASSIGN');
			END IF;	
	END;
	----------------------------------------------------------------------
	
	----------------------------------------------------------------------
PROCEDURE P_ASSIGN_CURATOR_FSSP(P_ID_CASE NUMBER, P_CURATOR NUMBER)
	 IS       
	 L_CUR_ST VARCHAR2(100);
	 L_CUR_FSSP_CURATOR NUMBER;
	BEGIN		 
			SELECT cs.curator_fssp,  st.code_status
			INTO L_CUR_FSSP_CURATOR, L_CUR_ST
			FROM T_CLI_CASES cs
					,T_CASE_STATUS st
			WHERE cs.id_status = st.id_status
			AND   cs.id_case = P_ID_CASE;
			
			IF L_CUR_ST = 'ASSIGN' and P_CURATOR IS NOT NULL THEN
          PKG_CLI_CASES.P_CHANGE_STATUS(P_ID_CASE,'WORK');
      END IF;		

			UPDATE T_CLI_CASES s SET s.curator_fssp = P_CURATOR WHERE s.id_case = P_ID_CASE;			
	END;    
	
	----------------------------------------------------------------------
	PROCEDURE P_CHANGE_STATUS(P_ID_CLI_CASE NUMBER
													 ,P_STATUS VARCHAR2
													 ,P_DATE   DATE DEFAULT SYSDATE)
	IS          
		L_ID NUMBER;                           
		L_STATUS VARCHAR2(100);
	BEGIN
		
	  SELECT ST.CODE_STATUS INTO L_STATUS FROM T_CLI_CASES S, T_CASE_STATUS ST  
		WHERE S.ID_CASE = P_ID_CLI_CASE AND S.ID_STATUS = ST.ID_STATUS(+);
		
		IF L_STATUS != 'CLOSE' OR L_STATUS IS NULL THEN 
			SELECT s.id_status 
			INTO  L_ID
			FROM T_CASE_STATUS s
			WHERE s.code_status = P_STATUS;   
				
			UPDATE T_CLI_CASES c 
			SET c.id_status = L_ID			 
			WHERE c.id_case = P_ID_CLI_CASE;							
 	  END IF;	
	
	EXCEPTION 
		 WHEN NO_DATA_FOUND THEN
			  RAISE_APPLICATION_ERROR(-20200,'Неверно указан код статуса');
	END;

-----------------------------------------------------------------------------------------
	              
	FUNCTION F_GET_CLI_CASE_DEBT(P_ID_CASE NUMBER) RETURN NUMBER
		 IS
   	L_RET NUMBER;
	BEGIN                                         
		SELECT SUM(t.DEBT)                  
		INTO L_RET  
		FROM( SELECT D.debt
					FROM V_CLI_DEBTS D,
		     			 T_CLI_DEBTS ccd
					WHERE ccd.ID_WORK = P_ID_CASE    
					AND   ccd.ID_DOC = D.id_doc)t
		WHERE t.DEBT > 0;
	IF L_RET IS NULL THEN
		RETURN 0;
	END IF;
		
	RETURN L_RET;
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			RETURN NULL;
	END;            

---------------------------------------------------------------------------

	--Процедура обновляет данные клиента по делу
	PROCEDURE P_REFRESH_DATA(P_CASE_ID NUMBER)
		 IS
	BEGIN                                                  
		FOR i IN (SELECT w.id_client FROM t_cli_debt_work w WHERE w.id_work = P_CASE_ID )
	  LOOP
			  PKG_CLIENTS.P_GET_FULL_CLI_INFO(P_ID_CLIENT => i.id_client);
		END LOOP;
	END;       

------------------------------------------------------------------------------

  --Процедура создаёт выездное мероприятие
	PROCEDURE P_CREATE_OFFSITE_CLI_EVENT(P_ID_CASE 		NUMBER,
													 						 P_EVENT      VARCHAR2,
																			 P_COMMENTARY VARCHAR2 DEFAULT NULL)
	IS                             
	  L_ID NUMBER;
	BEGIN
		 --Достаём ИД мероприятия
		 SELECT ev.id_event INTO L_ID FROM T_OFF_SITE_EVENTS ev WHERE ev.code_event = P_EVENT; 
		 
		 INSERT INTO T_CLI_CASE_OFF_SITE(ID_CASE_EVENT,ID_CASE,COMMENTARY,CREATED_BY,DATE_CREATED,ID_EVENT)
		 VALUES(SEQ_CLI_OFFSITE.NEXTVAL,P_ID_CASE,P_COMMENTARY,f$_usr_id,SYSDATE,L_ID);                
		 
		 PKG_EVENTS.P_CREATE_CLI_EVENT(P_ID_WORK => P_ID_CASE,P_CODE => 'OFF_SITE_EVENT');
  EXCEPTION
			WHEN NO_DATA_FOUND THEN
					RAISE_APPLICATION_ERROR(-20200,'Неверно указан код мероприятия'); 
	END;	   
	
--------------------------------------------------------------------------------	
	
  PROCEDURE P_SEND_TO_COURT(P_ID_CASE NUMBER)
		IS                                
		L_CTR VARCHAR2(500);
		L_CLI VARCHAR2(500);
		L_ID  NUMBER;
	BEGIN  
		
	  UPDATE T_CLI_DEBT_WORK W
		   SET W.DEPT = 'COURT'
		 WHERE W.ID_WORK = P_ID_CASE;		
	
    UPDATE T_CLI_CASES S 
		SET S.DEBT_COURT = S.SUM_DEBT,
		    S.DEP = 'COURT'				                                             
		WHERE S.ID_CASE = P_ID_CASE;
		
		UPDATE T_CLI_DEBTS D 
		SET D.DEBT_COURT = D.DEBT_CURRENT,                                      
		    D.DEPT = 'COURT',
				D.DATE_COURT = SYSDATE
		WHERE D.ID_WORK = P_ID_CASE;      
		
	  P_CHANGE_STATUS(P_ID_CLI_CASE => P_ID_CASE,P_STATUS => 'ASSIGN');
		PKG_EVENTS.P_CREATE_CLI_EVENT(P_ID_WORK => P_ID_CASE,P_CODE => 'TO_COURT');       
		
		
		--Уведомления для Жаровиной и Синицыной
		SELECT S.CLI_ALT_NAME
		INTO L_CLI
		FROM V_CLI_CASES S WHERE S.ID_CASE = P_ID_CASE;
		
		L_ID := PKG_NOTIF.F_CREATE_NOTIFICATION(P_HEADING => 'Новое дело'
		                               ,P_TEXT => 'В отдел поступило новое дело по абоненту '||L_CLI||' договор № '||L_CTR
																	 ,P_TYPE => 'COURT_CASE'
																	 ,P_PARAMS => P_ID_CASE
																	 ,P_USER => 19716);        
	  
		L_ID := PKG_NOTIF.F_CREATE_NOTIFICATION(P_HEADING => 'Новое дело'
		                               ,P_TEXT => 'В отдел поступило новое дело по абоненту '||L_CLI||' договор № '||L_CTR
																	 ,P_TYPE => 'COURT_CASE'
																	 ,P_PARAMS => P_ID_CASE
																	 ,P_USER => 58758);																 
		
	END;

------------------------------------------------------------------------	
	
	PROCEDURE P_EVALUATE_PENY(P_ID_CASE NUMBER,
											  	  P_DATE    DATE DEFAULT SYSDATE,
														P_GROUP   VARCHAR2 DEFAULT NULL)
		 IS
		 L_PENY NUMBER;
		 l_case_type VARCHAR2(10);    
		 L_CNT       NUMBER;
	BEGIN
		 L_PENY := PKG_REFIN.F_GET_CLI_PENY(P_ID_CASE,P_DATE,P_GROUP);
		 
		 UPDATE T_CLI_CASES s
		 SET    s.peny = L_PENY,   
		 				s.peny_date = P_DATE,
						S.PENY_GROUP_CALC = P_GROUP
		 WHERE  s.id_case = P_ID_CASE;
		 	 
	END P_EVALUATE_PENY;                                                  
	  
------------------------------------------------------------------------	
	
	PROCEDURE P_EVALUATE_PENY_FACT(P_ID_CASE NUMBER,
		                             P_DATE    DATE DEFAULT SYSDATE)
	  IS              
	  L_PENY NUMBER;
	BEGIN
		L_PENY := PKG_REFIN.F_GET_PENY_FOR_DEBT(P_ID_CASE => P_ID_CASE,
		                                        P_DATE_TO => P_DATE);
		
		UPDATE T_CLI_CASES S
		   SET S.PENY_FACT = L_PENY,
			     S.PENY_DATE = P_DATE
		WHERE S.ID_CASE = P_ID_CASE;			 
			
	END;															 
	
	-------------------------------------------------------------------------------
	
	PROCEDURE P_EVALUATE_GOV_TOLL (P_ID_CASE NUMBER
		                            ,P_TYPE VARCHAR2)
	  IS
		L_TOLL NUMBER;
	BEGIN
		L_TOLL := PKG_REFIN.F_GET_CLI_GOV_TOLL(P_ID_CASE,P_TYPE); 
		 UPDATE T_CLI_CASES s
		 SET    s.gov_toll = L_TOLL		      
		       ,S.GOV_TOLL_TYPE = P_TYPE  
		 WHERE  s.id_case = P_ID_CASE;
	END;
	            
		
	
	--------------------------------------------------------------------------------------		
			
		PROCEDURE P_ADD_PENY_TOLL_PAYMENT(P_ID_CASE NUMBER,
																			P_PENY NUMBER DEFAULT 0,
																			P_TOLL NUMBER DEFAULT 0,
																			P_DATE DATE,
																			P_PENY_FACT NUMBER DEFAULT 0)
		IS          
	BEGIN             

		IF P_PENY>0 THEN	
			INSERT INTO T_CLI_PENALTY_PAYMENTS(Id,PURPOSE,AMOUNT,DATE_PAY,CREATED_BY,DATE_CREATED,ID_CASE)
			VALUES(PAYMENTS_PENY_TOLL_seq.Nextval, 'PENY', P_PENY, P_DATE, f$_usr_id,SYSDATE, P_ID_CASE);		
		END IF;
		IF P_TOLL>0 THEN	
			INSERT INTO T_CLI_PENALTY_PAYMENTS(Id,PURPOSE,AMOUNT,DATE_PAY,CREATED_BY,DATE_CREATED,ID_CASE)
			VALUES(PAYMENTS_PENY_TOLL_seq.Nextval, 'TOLL', P_TOLL, P_DATE, f$_usr_id,SYSDATE,P_ID_CASE);
		END IF;
		IF P_PENY_FACT>0 THEN	
			INSERT INTO T_CLI_PENALTY_PAYMENTS(Id,PURPOSE,AMOUNT,DATE_PAY,CREATED_BY,DATE_CREATED,ID_CASE)
			VALUES(PAYMENTS_PENY_TOLL_seq.Nextval, 'PENY_FACT', P_PENY_FACT, P_DATE, f$_usr_id,SYSDATE,P_ID_CASE);
		END IF;
							 
	END;
	
	--------------------------------------------------------------------------------------
	PROCEDURE P_CLOSE_CASE(P_ID_CASE NUMBER
												,P_CODE_REASON VARCHAR2
												,P_COMMENTARY VARCHAR2 DEFAULT NULL) 
	IS											
	L_ID NUMBER;                                                               
	L_DEP VARCHAR2(30);
	L_TYPE VARCHAR2(20);
	L_ID_CONTRACT VARCHAR2(36);                        
	BEGIN                                                                                         
		
		SELECT r.id_reason INTO L_ID FROM T_CASE_CLOSE_REASON r WHERE r.code_reason = P_CODE_REASON;

	  UPDATE T_CLI_CASES s 
		SET    s.id_reason_close = L_ID,
		       s.close_comment = P_COMMENTARY,
					 s.last_status = s.id_status		      
		WHERE s.id_case = P_ID_CASE;		     
		
		P_CHANGE_STATUS(P_ID_CASE,'CLOSE');		  
		
		IF P_CODE_REASON = 'SPISANO' THEN
			 UPDATE T_CLI_CASES S 
			    SET S.PENY = 0,     
					    s.PENY_COURT = 0,
							s.PENY_FACT = 0,
					    S.GOV_TOLL = 0
			 WHERE S.ID_CASE = P_ID_CASE;
		END IF;
			
	EXCEPTION 
		WHEN NO_DATA_FOUND THEN
			RAISE_APPLICATION_ERROR(-20200,'Неверно указан код причины закрытия. Обратитесь к администратору.');		
	END;    
	
	FUNCTION F_GET_STATUS(P_ID_CASE NUMBER) RETURN VARCHAR2
		IS
	L_RET VARCHAR2(100);
	BEGIN
		SELECT st.code_status
		INTO L_RET
		FROM T_CLI_CASES s,
		     T_CASE_STATUS st
		WHERE s.id_status = st.id_status
		AND   s.id_case = P_ID_CASE;
		
		RETURN L_RET;    
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			RETURN NULL;	
	END;	          
	
	
	--Процедура передаёт дело в ФССП
	PROCEDURE P_SEND_TO_FSSP(P_ID_CASE NUMBER)
		IS          
		L_CTR VARCHAR2(500);
		L_CLI VARCHAR2(500);
		L_ID  NUMBER;           
	BEGIN            
		UPDATE T_CLI_DEBT_WORK W
		   SET W.DEPT = 'FSSP'
		 WHERE W.ID_WORK = P_ID_CASE;
		 
    UPDATE T_CLI_CASES S 
		SET S.DEBT_FSSP = S.SUM_DEBT,
		    S.DEP = 'FSSP'				                                             
		WHERE S.ID_CASE = P_ID_CASE;
		
		UPDATE T_CLI_DEBTS D 
		SET D.DEBT_FSSP = D.DEBT_CURRENT,                                      
		    D.DEPT = 'FSSP',
				D.DATE_FSSP = SYSDATE
		WHERE D.ID_WORK = P_ID_CASE;      
		
		
			--Уведомления для Кривошеиной и Ефремову
		SELECT S.CLI_ALT_NAME
		INTO L_CLI
		FROM V_CLI_CASES S WHERE S.ID_CASE = P_ID_CASE;
		
		L_ID := PKG_NOTIF.F_CREATE_NOTIFICATION(P_HEADING => 'Новое дело'
		                               ,P_TEXT => 'В отдел поступило новое дело по абоненту '||L_CLI||' договор № '||L_CTR
																	 ,P_TYPE => 'FSSP_CASE'
																	 ,P_PARAMS => P_ID_CASE
																	 ,P_USER => 19710);        
	  
		L_ID := PKG_NOTIF.F_CREATE_NOTIFICATION(P_HEADING => 'Новое дело'
		                               ,P_TEXT => 'В отдел поступило новое дело по абоненту '||L_CLI||' договор № '||L_CTR
																	 ,P_TYPE => 'FSSP_CASE'
																	 ,P_PARAMS => P_ID_CASE
																	 ,P_USER => 19713);				
	  --
	END;  
	
	     
  --процедура возвращает дело в статус до его закрытия
	PROCEDURE P_RETURN_TO_WORK(P_ID_CLI_CASE NUMBER)
		IS                                                                                               
		L_ID NUMBER;
	BEGIN 
			--возвращаем дело в ГФССП и добавляем комментарий														
			UPDATE T_CLI_CASES s 
			   SET s.id_status = s.last_status
			WHERE  s.id_case = P_ID_CLI_CASE
			RETURNING S.LAST_STATUS INTO L_ID;									
			
			--добавление события
			PKG_EVENTS.P_CREATE_CLI_EVENT(P_ID_CLI_CASE,'REOPENED');                                             
	END; 
  
	
	
	
	--Процедура обновляет значения поля  P_COLUNM на P_VAL для дела с id  = P_ID_CASE
	PROCEDURE P_UPDATE_VALUE(P_ID_CASE NUMBER, P_COLUNM VARCHAR2, P_VAL VARCHAR2)
		IS
		l_sql VARCHAR2(5000);
		l_old_val VARCHAR2(1000);
		l_column_count NUMBER:=0;
		WRONG_FIELD EXCEPTION;
		l_table_name VARCHAR2(50):= 'T_CLI_CASES';
	BEGIN
		select COUNT(column_name) INTO l_column_count from user_tab_columns where table_name = upper(l_table_name) AND column_name = upper(P_COLUNM);
		IF l_column_count = 0 THEN	
			RAISE WRONG_FIELD; --проверяем, существует ли такое поле в таблице
		END IF;
	  --достаём старое значение
		l_sql:='select '||P_COLUNM||' from '||l_table_name||' where id_case='||P_ID_CASE;
		EXECUTE IMMEDIATE l_sql into l_old_val;
		IF (l_old_val!=P_VAL)OR(l_old_val IS NULL)OR(P_VAL IS NULL) THEN --обновляем только если новое значение отличается от старого
				--обновляем поле
				l_sql:='UPDATE '||l_table_name||' set '||P_COLUNM||' = '''||P_VAL||''' where id_case='||P_ID_CASE;
				EXECUTE IMMEDIATE l_sql; 
				--пишем изменения в аудит
		END IF;
	EXCEPTION
		WHEN WRONG_FIELD THEN
			RAISE_APPLICATION_ERROR(-20200,'Поля не существует');
		WHEN OTHERS THEN
			RAISE_APPLICATION_ERROR(-20200,'Ошибка обновления. Проверьте корректность ввода');			 	
	END;
	
	--ПРОЦЕДУРА ДОБАВЛЯЕТ ПРЕТЕНЗИЮ В ДЕЛО
	PROCEDURE P_ADD_PRETENSION(P_ID_CASE NUMBER
		                        ,P_EVENT BOOLEAN DEFAULT TRUE
														,P_USER  NUMBER DEFAULT f$_usr_id)
		IS
		L_DESCRIPTION T_FILES.FILE_DESCRIPTION%TYPE;
		L_BLOB LAWSUP.PKG_FILES.REC_DOC;
		L_ID   NUMBER;
	BEGIN
	    
	
			SELECT NVL(TO_CHAR(MIN(FD.DOC_DATE),'fmmonth YYYY')||'-'||TO_CHAR(MAX(FD.DOC_DATE),'fmmonth YYYY'), '-')
			INTO L_DESCRIPTION
			FROM T_CLI_FINDOCS FD, T_CLI_DEBTS CCD
			WHERE CCD.ID_DOC = FD.ID_DOC
			AND  CCD.ID_WORK = P_ID_CASE;
	
		  L_BLOB := Pkg_Controller.P_RETURN_BLOB(P_REP_CODE => 'PRETENSION',P_PAR_01 => P_ID_CASE,P_PAR_03 => 'Y',P_PAR_10 => F$_USR_ID);
   
			L_ID := PKG_FILES.F_INSERT_FILE(P_FILE     => L_BLOB.P_BLOB,
																		 P_FILE_NAME => L_BLOB.P_FILE_NAME,
																		 P_MIME_TYPE => 'application/pdf',
																		 P_DESC      => l_description,
																		 P_TYPE      => 'PRETENSION',
																		 P_USER      =>  P_USER);
			PKG_FILES.P_ADD_FILE_TO_CASE(P_ID_FILE => L_ID,P_ID_CASE => P_ID_CASE);
			
			IF P_EVENT THEN
  			PKG_EVENTS.P_CREATE_CLI_EVENT(P_ID_CASE,'PRETEN_FORMED',P_AUTOR => P_USER);															 
			END IF;	
	END;    
	
-------------------------------------------------------------------------------------	
	
	FUNCTION F_MERGE_CASES(P_CASES wwv_flow_global.vc_arr2)	RETURN NUMBER
		IS                                                                                  
		   L_ID_CASE     NUMBER;
    L_SUM         NUMBER := 0;
    L_CTR_NUM     VARCHAR2(50);
    L_ID_CLIENT   NUMBER;
    L_PREF        VARCHAR2(100);
    L_ID_CONTRACT NUMBER;      
		L_CASE_NAME   VARCHAR2(100);
		L_TABLE       wwv_flow_t_varchar2 := wwv_flow_t_varchar2();
  BEGIN
    --ПРОВЕРЯЕМ НА НАЛИЧИЕ ДЕЛ ДЛЯ ОБЪЕДИНЕНИЯ
    IF P_CASES.COUNT < 2 THEN
       RAISE_APPLICATION_ERROR(-20200,'Необходимо указать более одного дела для объденения');
    END IF;
    
		FOR I IN 1..P_CASES.COUNT LOOP
			  APEX_STRING.PUSH(L_TABLE,TO_CHAR(P_CASES(I)));
		END LOOP;
    --ДЕЛА ДОЛЖНЫ БЫТЬ ПО ОДНОИМУ ДОГОВОРУ
    /*BEGIN
      SELECT DISTINCT S.ID_CONTRACT INTO L_ID_CONTRACT
      FROM T_CLI_CASES S WHERE S.ID_CASE IN (SELECT TO_NUMBER(COLUMN_VALUE) FROM TABLE(L_TABLE));
    EXCEPTION
      WHEN TOO_MANY_ROWS THEN
         RAISE_APPLICATION_ERROR(-20200,'У объединяемых дел должен быть один договор');
    END;*/

    --НЕОБХОДИМЫЕ ДАННЫЕ
    SELECT CT.CTR_NUMBER,CT.ID_CLIENT,NULL
/*           ,(SELECT DECODE(CG.ABON_GROUP,'UK','ДК','GOV','БДЖ','OTHER','ПР','TSG','ТСЖ')
            FROM T_CUSTOMER_GROUP CG WHERE CG.ID_GROUP = CLI.ID_GROUP) PREF*/
    INTO L_CTR_NUM, L_ID_CLIENT, L_PREF
    FROM T_CONTRACTS CT, T_CLIENTS CLI
    WHERE CT.ID_CONTRACT = L_ID_CONTRACT
    AND   CT.ID_CLIENT = CLI.ID_CLIENT;

    INSERT INTO T_CLI_DEBT_WORK(ID_WORK,ID_CLIENT,TYPE_WORK,DEPT,DATE_CREATED,CREATED_BY)
    VALUES(SEQ_CLI_DEBT_WORK.NEXTVAL,L_ID_CLIENT,'CASE','COURT',SYSDATE,F$_USR_ID)
    RETURNING ID_WORK INTO L_ID_CASE;

    INSERT INTO T_CLI_CASES(ID_CASE,CASE_NAME,DATE_CREATED,CREATED_BY
                           ,DEP,CURATOR_PRE,CURATOR_COURT)
    VALUES(L_ID_CASE,L_PREF || '-' || L_ID_CASE||'-О',SYSDATE
          ,F$_USR_ID,'COURT',F$_USR_ID,f$_usr_id)
		RETURNING CASE_NAME INTO L_CASE_NAME;

    --ЗАКРЫВАЕМ ДЕЛА КОТОРЫЕ ОБЪЕДИНЯЮТСЯ ПО ПРИЧИНЕ ОБЪЕДИНЕНИЯ. УКАЗЫВАЕМ ВО ЧТО ОНИ ОБЪЕДИНЕНЫ
    FOR I IN (SELECT TO_NUMBER(COLUMN_VALUE) AS ID_CASE FROM TABLE(L_TABLE))
    LOOP
       PKG_CLI_CASES.P_CLOSE_CASE(I.ID_CASE,'MERGED','Дело было объединено с делом '||L_CASE_NAME);
       UPDATE T_CLI_CASES S SET S.ID_MERGE = L_ID_CASE WHERE S.ID_CASE = I.ID_CASE;
    END LOOP;

    --ПОДТЯГИВАЕМ ДОЛГИ В НОВОЕ ДЕЛО
    INSERT INTO T_CLI_DEBTS(ID_DEBT,ID_WORK,ID_DOC,DATE_CREATED,CREATED_BY,ID_CONTRACT,ID_CLIENT,DEBT_CURRENT,DEBT_CREATED,
                            DEBT_PRETRIAL,DATE_PRETRIAL,DEBT_FSSP,DATE_FSSP,DEPT,DEBT_COURT,DATE_COURT,PENY)
    SELECT SEQ_CLI_CASE_DEBTS.NEXTVAL,L_ID_CASE,CD.ID_DOC,SYSDATE,F$_USR_ID,CD.ID_CONTRACT,CD.ID_CLIENT,CD.DEBT_CURRENT,CD.DEBT_CURRENT,
           CD.DEBT_PRETRIAL,CD.DATE_PRETRIAL,CD.DEBT_FSSP,CD.DATE_FSSP,'COURT',CD.DEBT_COURT,CD.DATE_COURT,CD.PENY
    FROM T_CLI_DEBTS CD WHERE CD.ID_WORK IN (SELECT TO_NUMBER(COLUMN_VALUE) FROM TABLE(L_TABLE));

    SELECT SUM(D.DEBT_CURRENT) INTO L_SUM FROM T_CLI_DEBTS D WHERE D.ID_WORK = L_ID_CASE;

    UPDATE T_CLI_DEBT_WORK W
      SET  W.DEBT_CREATED = L_SUM,
           W.CURRENT_DEBT = L_SUM
     WHERE W.ID_WORK = L_ID_CASE;

    UPDATE T_CLI_CASES S
    SET   S.SUM_DEBT  = L_SUM,
          S.DEBT_COURT = L_SUM
    WHERE S.ID_CASE = L_ID_CASE;

    PKG_CLI_CASES.P_CHANGE_STATUS(P_ID_CLI_CASE => L_ID_CASE,P_STATUS => 'WORK');
    PKG_EVENTS.P_CREATE_CLI_EVENT(P_ID_WORK => L_ID_CASE,P_CODE => 'CREATE');
		
		RETURN L_ID_CASE;
  END;
	
end PKG_CLI_CASES;
/

