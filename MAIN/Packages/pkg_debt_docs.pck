i�?create or replace package lawmain.PKG_DEBT_DOCS is

  -- Author  : EUGEN
  -- Created : 26.04.2019 16:16:44
  -- Purpose : 
  
  PROCEDURE P_CREATE_DOC(P_DEBTS WWV_FLOW_GLOBAL.vc_arr2,
			                     P_TYPE      VARCHAR2,
													 P_ID_CLIENT NUMBER,
													 P_DATE      DATE DEFAULT SYSDATE);

	PROCEDURE P_UPDATE_DOC(P_ID NUMBER,
                         P_COMMENT VARCHAR2,
												 P_IS_ACTIVE VARCHAR2);
		
  PROCEDURE P_DELETE_DOC(P_ID NUMBER);                         
	
	FUNCTION F_RETURN_CTR_LIST(P_ID_WORK NUMBER) RETURN VARCHAR2;
end PKG_DEBT_DOCS;
/

create or replace package body lawmain.PKG_DEBT_DOCS is

		PROCEDURE P_CREATE_DOC(P_DEBTS WWV_FLOW_GLOBAL.vc_arr2,
			                     P_TYPE  VARCHAR2,
													 P_ID_CLIENT NUMBER,
													 P_DATE      DATE DEFAULT SYSDATE)
			IS                                                                         
			L_ID NUMBER;
		BEGIN                  
       INSERT INTO T_CLI_DEBT_WORK(ID_WORK,ID_CLIENT,TYPE_WORK,DEPT,DATE_CREATED,CREATED_BY)																													 
			 VALUES(SEQ_CLI_DEBT_WORK.NEXTVAL,P_ID_CLIENT,P_TYPE,'PRE_TRIAL',SYSDATE,F$_USR_ID)
			 RETURNING ID_WORK INTO L_ID;
			 
			 INSERT INTO T_CLI_DEBT_DOCS(ID,DOC_TYPE,DATE_CREATED,CREATED_BY,ID_CLIENT)
			 VALUES (L_ID,P_TYPE,SYSDATE,F$_USR_ID,P_ID_CLIENT);
			 
			 FORALL I IN P_DEBTS.FIRST..P_DEBTS.LAST 
			   INSERT INTO T_CLI_DEBTS(ID_DEBT,ID_WORK,ID_DOC,DATE_CREATED,CREATED_BY,ID_CONTRACT,ID_CLIENT,DEBT_CURRENT,DEBT_CREATED,DEPT)
				 VALUES(SEQ_CLI_CASE_DEBTS.NEXTVAL,L_ID,P_DEBTS(I),SYSDATE,f$_usr_id
				      ,(SELECT FD.ID_CONTRACT FROM T_CLI_FINDOCS FD WHERE FD.ID_DOC = P_DEBTS(I))
							,P_ID_CLIENT
				      ,(SELECT FD.DEBT FROM T_CLI_FINDOCS FD WHERE FD.ID_DOC =P_DEBTS(I))
							,(SELECT FD.DEBT FROM T_CLI_FINDOCS FD WHERE FD.ID_DOC =P_DEBTS(I))
							,'PRE_TRIAL');
			 	 
			 UPDATE T_CLI_DEBT_DOCS S 
			 SET S.DEBT_CREATED = (SELECT SUM(F.DEBT) 
			                       FROM T_CLI_DEBTS C
														     ,T_CLI_FINDOCS F 
			                       WHERE C.ID_DOC = F.ID_DOC
														 AND   C.ID_WORK = L_ID)
			 WHERE S.ID = L_ID;	  
			 
			 UPDATE T_CLI_DEBT_WORK W
			    SET W.DEBT_CREATED = (SELECT D.DEBT_CREATED FROM t_Cli_Debt_Docs D WHERE D.ID = W.ID_WORK),
					    W.CURRENT_DEBT = (SELECT D.DEBT_CREATED FROM t_Cli_Debt_Docs D WHERE D.ID = W.ID_WORK)
				WHERE W.ID_WORK = L_ID;  
				
			 PKG_EVENTS.P_CREATE_CLI_EVENT(P_ID_WORK => L_ID,P_CODE => P_TYPE,P_DATE => P_DATE);	
		END;												

------------------------------------------------------------------------------
    
		PROCEDURE P_UPDATE_DOC(P_ID NUMBER,
                           P_COMMENT VARCHAR2,
													 P_IS_ACTIVE VARCHAR2)
		  IS
		BEGIN
			UPDATE T_CLI_DEBT_DOCS D SET D.COMMENTARY = P_COMMENT, D.IS_ACTIVE = P_IS_ACTIVE
		  WHERE D.ID = P_ID;
		END;																												 

-------------------------------------------------------------------------------
		
    PROCEDURE P_DELETE_DOC(P_ID NUMBER)
			IS
		BEGIN
			 DELETE FROM T_CLI_DEBTS D WHERE D.ID_WORK = P_ID;
			 DELETE FROM T_CLI_DEBT_DOCS C WHERE C.ID = P_ID;
		END;                                                                         
		
---------------------------------------------------------------------------------		

		FUNCTION F_RETURN_CTR_LIST(P_ID_WORK NUMBER) RETURN VARCHAR2
			IS
			L_RET VARCHAR2(4000);
		BEGIN
			SELECT LISTAGG(T.CTR_NUMBER,', ') WITHIN GROUP(ORDER BY T.CTR_NUMBER) 
			INTO L_RET 
      FROM 
      (SELECT DISTINCT CT.CTR_NUMBER 
       FROM T_CLI_DEBTS D, T_CONTRACTS CT 
       WHERE CT.ID_CONTRACT = D.ID_CONTRACT 
       AND D.ID_WORK = P_ID_WORK) T;
		 RETURN L_RET;
		END;
			
end PKG_DEBT_DOCS;
/

