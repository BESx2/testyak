i�?CREATE OR REPLACE PACKAGE LAWMAIN.PKG_MAIL_09
IS
/* Автор: Бородулин Е.С.
   Дата:  16.10.2019
	 Назначение: Пакет отсылает email уведомление для ю.л 
*/

   PROCEDURE p_send_mail(P_ID_CLIENT NUMBER, P_ID_CONTRACT IN NUMBER, P_EMAIL VARCHAR2);

END;
/

CREATE OR REPLACE PACKAGE BODY LAWMAIN.PKG_MAIL_09
IS

	G$HTML VARCHAR2(32000);

  PROCEDURE p_send_mail(P_ID_CLIENT NUMBER, P_ID_CONTRACT IN NUMBER, P_EMAIL VARCHAR2)
        IS
			L_RET    NUMBER;
			L_REP    LAWSUP.PKG_FILES.REC_DOC;
    BEGIN                                                        
			IF p_email IS NULL THEN 
				raise_application_error(-20200,'email не указан');
		  END IF;                
			IF P_ID_CONTRACT IS NULL THEN 
				raise_application_error(-20200,'Договор не указан');
		  END IF;                

			
				G$HTML := PKG_PREF.F$HTML('CNTR_MAIL_DEBT_NOTIF');
				FOR i IN (SELECT CT.CTR_NUMBER,(SELECT L.PHONE FROM T_USER_LIST L WHERE L.ID = F$_USR_ID) PHONE
					        FROM T_CONTRACTS CT WHERE CT.ID_CONTRACT = P_ID_CONTRACT
									AND CT.ID_CLIENT = P_ID_CLIENT)
            LOOP
							G$HTML := REPLACE(G$HTML,'#CTRNUMBER#',I.CTR_NUMBER); -- договор.
							G$HTML := REPLACE(G$HTML,'#PHONE#',i.phone);
            END LOOP;
           
           L_RET := PKG_MAIL.F_MAKE_EMAIL(P_HEADER => PKG_PREF.F$C1('CNTR_MAIL_DEBT_NOTIF')
                                           ,P_FROM => PKG_PREF.F$C1('USER_MAIL') -- do not change !!!! if change => change password in sup.pkg_mail!!!
                                           ,P_BODY => G$HTML
                                          );
            PKG_MAIL.P_ATTACHE_EMAIL_ADDR(P_EMAIL      => P_EMAIL
                                         ,P_ID_MAIL    => L_RET
                                         ,P_EMAIL_TYPE => PKG_MAIL.LIST_EMAIL_TYPE.TO_);
																				 		
        PKG_MAIL.P_SEND(P_ID => L_RET,P_SEND_TYPE => PKG_MAIL.LIST_TYPE_SEND.LTE);
				PKG_SEND_MAIL.P_INSERT_CLI_MAIL(P_ID_CLIENT,L_RET,'CNTR_MAIL_DEBT_NOTIF');
 				COMMIT;
    END;
    
		
 
		
END;
/

