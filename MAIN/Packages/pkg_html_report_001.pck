i�?create or replace package lawmain.PKG_HTML_REPORT_001 is

  -- Author  : EUGEN
  -- Created : 13.06.2019 16:20:56
  -- Purpose : 
  
 PROCEDURE P_GEN_REPORT(P_ID_CLIENT NUMBER
                       ,P_DATE_ON DATE DEFAULT SYSDATE
                       ,P_SHOW_ZERO VARCHAR2 DEFAULT 'N'
                       ,P_TYPE_WORK VARCHAR2 DEFAULT NULL                       
                       ,P_EXEC   NUMBER DEFAULT NULL       							
											 ,P_ID_CONTRACT NUMBER DEFAULT NULL) ;

end PKG_HTML_REPORT_001;
/

create or replace package body lawmain.PKG_HTML_REPORT_001 is



 PROCEDURE P_GEN_REPORT(P_ID_CLIENT NUMBER
	                     ,P_DATE_ON DATE DEFAULT SYSDATE
											 ,P_SHOW_ZERO VARCHAR2 DEFAULT 'N'
											 ,P_TYPE_WORK VARCHAR2 DEFAULT NULL											 
											 ,P_EXEC   NUMBER DEFAULT NULL       							
											 ,P_ID_CONTRACT NUMBER DEFAULT NULL) 
	 IS              
	 L_CLOB CLOB;  
	 L_FMT  VARCHAR2(50) := 'fm999G999G999G999G990D00';
	 L_NLS  VARCHAR2(50) := 'NLS_NUMERIC_CHARACTERS='', ''';      
	 L_DEBT_TOT NUMBER := 0;
	 L_INV_TOT  NUMBER := 0;     
	 L_CNT      NUMBER; 
	 L_ROW      NUMBER := 0;     
	 L_STATUS   VARCHAR2(500);
 BEGIN  
				
	 L_CLOB := '<div class="contr-table"> 
								<table id="report" width="100%">  
								<thead>
								<tr class="contr-table__head">
										<td rowspan="3">Контрагент </td>
										<td>Категория портебителя </td>
										<td rowspan="3">Документ расчетов </td>
									  <td rowspan="3">№ сч./ф.</td>
										<td rowspan="3">Начисленно</td>
										<td rowspan="3">Долг</td>
										<td>Тип работы</td>    
										<td rowspan="3">Дата проведения</td>
										<td>№ дела</td>
										<td>Основной долг</td>
								</tr>
								<tr class="contr-table__head">
										<td rowspan="2">Договор </td>
										<td rowspan="2">Событие</td>
										<td>№ листа </td>
										<td>Неустойка </td>
								</tr>
								<tr class="contr-table__head">
										<td>Взыскано </td>
										<td>Госпошлина </td>
								</tr>
								</thead>
							  <tbody>';
	 PKG_UTILS.HtpPrn(L_CLOB);

	 FOR I IN ( SELECT * FROM (
		          SELECT CLI.CLI_ALT_NAME,  ct.cust_group, CT.CTR_NUMBER,
									  '<a href="'||APEX_PAGE.get_url(p_application => '2800',p_page =>'1112',p_items => 'P1112_EXT_ID',p_values => FD.EXT_ID)||'">'||fd.commentary||'</a>' COMMENTARY,
										FD.DOC_NUMBER, FD.DOC_DATE, FD.AMOUNT,
										 CASE 
												WHEN SYSDATE < NVL(CD.DATE_COURT,SYSDATE) THEN 'Досудебный отдел'
												WHEN SYSDATE < NVL(CD.DATE_FSSP,SYSDATE)  THEN 'Судебны отдел'
												WHEN SYSDATE >= CD.DATE_FSSP THEN 'Испол производство'
												ELSE  'Досудебный отдел'
										 END DEPT
							--       DECODE(CD.DEPT,'PRE_TRIAL','Досудебный отдел','COURT','Судебный отдел','FSSP','Испол производство') dept       
										,(SELECT WT.NAME_TYPE FROM T_CLI_DEBT_WORK_TYPE WT WHERE WT.CODE_TYPE = W.TYPE_WORK) TYPE_WORK 
										,w.type_work code_type
										,w.date_created   
										,EV.DATE_EVENT 
										,EV.SHORT_NAME LAST_EVENT
										,FD.ID_DOC
										,(SELECT  SUM(CASE 
																 WHEN C.DOC_TYPE = 'Изменение состояния долга' THEN C.AMOUNT
																 ELSE  ABS(C.AMOUNT)
																END)	
											FROM T_CLI_FINDOC_CONS C 
											WHERE (C.EXT_ID_TO = FD.Ext_Id OR C.EXT_ID_FROM = FD.Ext_Id)
											AND   TRUNC(C.DATE_CON) BETWEEN CD.DATE_CREATED AND P_DATE_ON
											AND   C.EXT_ID_FROM != C.EXT_ID_TO) PAYED  
										,(SELECT SUM(DECODE(C.OPER_TYPE,'Приход',C.AMOUNT,-C.AMOUNT))
											FROM T_CLI_FINDOC_CONS C
											WHERE C.EXT_ID_TO = FD.EXT_ID
											AND   TRUNC(C.DATE_CON) <= P_DATE_ON) DEBT    
										,EX.NUM_EXEC, EX.COURT_DEBT, EX.COURT_PENY, EX.COURT_TOLL
										,(SELECT L.LEVY_NUM 
										    FROM T_CLI_LEVY L 
											 WHERE L.ID_LEVY = 
											       (SELECT MAX(L1.ID_LEVY)
														  FROM T_CLI_LEVY L1															
										          WHERE L1.ID_CASE = CC.ID_CASE)) LEVY_NUM           
							FROM T_CLI_FINDOCS FD 
							INNER JOIN T_CONTRACTS CT ON CT.ID_CONTRACT = FD.ID_CONTRACT
							INNER JOIN T_CLIENTS CLI  ON CT.ID_CLIENT = CLI.ID_CLIENT
							/*LEFT OUTER JOIN (SELECT D.ID_DOC, MAX(D.ID_WORK) ID_WORK
															 FROM T_CLI_DEBTS D
															 INNER JOIN T_CLI_DEBT_WORK DW ON (D.ID_WORK = DW.ID_WORK)
															 WHERE   D.ID_CLIENT = P_ID_CLIENT		
															 AND D.ID_WORK NOT IN (SELECT R.ID_REST FROM T_CLI_REST R WHERE R.CODE_STATUS = 'PROJECT')
															 AND D.ID_WORK NOT IN (SELECT DD.ID FROM T_CLI_DEBT_DOCS DD WHERE DD.IS_ACTIVE = 'N')
															 AND TRUNC(DW.DATE_CREATED) <= P_DATE_ON
															 GROUP BY D.ID_DOC) T
															 ON (FD.ID_DOC = T.ID_DOC)*/
							LEFT OUTER JOIN  (SELECT D.ID_DOC, MAX(EV.ID_EVENT) ID_EVENT
							                  FROM T_CLI_DEBTS D,
																     T_CLI_EVENTS EV
																WHERE  D.ID_CLIENT = P_ID_CLIENT								 
																AND    EV.ID_WORK  = D.ID_WORK
																AND    TRUNC(EV.DATE_EVENT)  <=  P_DATE_ON
																GROUP  BY D.ID_DOC) T
																ON (FD.ID_DOC = T.ID_DOC)
							LEFT OUTER JOIN  V_CLI_EVENTS EV ON (EV.ID_EVENT = T.ID_EVENT)									
							LEFT OUTER JOIN  T_CLI_DEBTS CD ON (CD.ID_DOC = T.ID_DOC AND CD.ID_WORK = EV.ID_WORK)
							LEFT OUTER JOIN  T_CLI_DEBT_WORK W ON (W.ID_WORK = EV.ID_WORK)
							LEFT OUTER JOIN  T_CLI_CASES CC ON (CC.ID_CASE = W.ID_WORK)
							LEFT OUTER JOIN  (SELECT EX1.ID_CASE, EX1.NUM_EXEC, EX1.COURT_DEBT,EX1.COURT_PENY,EX1.COURT_TOLL, EX1.ID ID_EXEC
																FROM T_CLI_COURT_EXEC EX1
																LEFT OUTER JOIN T_CLI_COURT_EXEC EX2
																ON (EX1.ID_CASE = EX2.ID_CASE AND EX1.DATE_CREATED < EX2.DATE_CREATED AND trunc(EX2.DATE_EXEC) < P_DATE_ON)
																WHERE EX2.ID IS NULL
																AND  trunc(EX1.Date_Exec) <= P_DATE_ON) EX																																		
															 ON (EX.ID_CASE = CC.ID_CASE)         
							/*LEFT OUTER JOIN (SELECT EV1.ID_WORK, EV1.short_name, EV1.DATE_EVENT
							                 FROM V_CLI_EVENTS EV1
													     LEFT OUTER JOIN V_CLI_EVENTS EV2                                                                       
															 ON (EV1.ID_WORK = EV2.ID_WORK AND EV1.DATE_EVENT < EV2.DATE_EVENT AND trunc(EV2.DATE_EVENT) <= P_DATE_ON)
							                 WHERE EV2.id_event IS NULL
															 AND TRUNC(EV1.DATE_EVENT) <= P_DATE_ON) EV   
															 ON (W.ID_WORK = EV.ID_WORK)*/
							WHERE fd.doc_date <= P_DATE_ON							 
							AND   FD.AMOUNT >= 0                         
						--	AND   (W.TYPE_WORK IS NULL AND P_TYPE_WORK = 'NO_STATUS')
						  AND   (W.TYPE_WORK = P_TYPE_WORK OR (P_TYPE_WORK = 'NO_STATUS' AND W.TYPE_WORK IS NULL) OR  P_TYPE_WORK IS NULL)
							AND   (EX.ID_EXEC = P_EXEC OR P_EXEC IS NULL)
							AND   CT.ID_CONTRACT = NVL(P_ID_CONTRACT,CT.ID_CONTRACT)
							AND   CLI.ID_CLIENT = P_ID_CLIENT) T
						  WHERE   T.DEBT >= CASE WHEN P_SHOW_ZERO = 'N' THEN 0.01 ELSE 0 END	 
							ORDER BY T.DOC_DATE DESC) 
	 LOOP   
		 
       L_ROW := L_ROW + 1;
	 
       L_CLOB :=  '<tr onclick="calcRow(this)" data-row="row'||l_row||'" class="contr-table__row">
			              <td rowspan="3">'||I.CLI_ALT_NAME||'</td>
										<td>'||I.CUST_GROUP||'</td>
										<td rowspan="3">'||I.COMMENTARY||'</td>
									  <td rowspan="3">'||I.DOC_NUMBER||'</td>
										<td rowspan="3" style="text-align:right" class="inv">'||TO_CHAR(I.AMOUNT,L_FMT,L_NLS)||'</td>
										<td rowspan="3" style="text-align:right" class="debt">'||TO_CHAR(nvl(I.DEBT,0),L_FMT,L_NLS)||'</td>
										<td>'||I.TYPE_WORK||'</td>    
										<td rowspan="3">'||TO_CHAR(NVL(I.DATE_EVENT,I.DATE_CREATED),'DD.MM.YYYY')||'</td>
										<td>'||I.NUM_EXEC||'</td>
										<td style="text-align:right">'||TO_CHAR(I.COURT_DEBT,L_FMT,L_NLS)||'</td>                  
              </tr>
              <tr onclick="calcRow(this)" data-row="row'||l_row||'" class="contr-table__row">   
									<td rowspan="2">'||I.CTR_NUMBER||'</td>';           

					L_CLOB := L_CLOB ||'<td rowspan="2">'||I.LAST_EVENT||'</td>';

			 L_CLOB := L_CLOB ||'
									<td>'||I.LEVY_NUM||'</td>
									<td style="text-align:right">'||TO_CHAR(I.COURT_PENY,L_FMT,L_NLS)||'</td>                  
              </tr>
              <tr onclick="calcRow(this)" data-row="row'||l_row||'" class="contr-table__row">
                  <td style="text-align:right">'||TO_CHAR(I.PAYED,L_FMT,L_NLS)||'</td>
                  <td style="text-align:right">'||TO_CHAR(I.COURT_TOLL,L_FMT,L_NLS)||'</td>
              </tr>';                           
							
				L_INV_TOT := L_INV_TOT + I.AMOUNT;
				L_DEBT_TOT := L_DEBT_TOT + NVL(I.DEBT,0);			
				
				PKG_UTILS.HtpPrn(L_CLOB);							
   END LOOP;				
	 HTP.P('<tr class="contr-table__total">
			              <td colspan="4">Итого:</td>
										<td style="text-align:right">'||TO_CHAR(l_inv_tot,L_FMT,L_NLS)||'</td>
										<td style="text-align:right">'||TO_CHAR(l_debt_tot,L_FMT,L_NLS)||'</td>
										<td></td>    
										<td ></td>
										<td></td>
										<td></td>                  
              </tr>');
	 HTP.P('</tbody></table></div>');											 	 
 END;
 
end PKG_HTML_REPORT_001;
/

