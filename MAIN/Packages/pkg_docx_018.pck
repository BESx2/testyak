i�?create or replace package lawmain.PKG_DOCX_018 is

  -- Author  : EUGEN
  -- Created : 01.04.2020 15:02:04
  -- Purpose : Наряд на ограничение
	-- CODE    : ORDER_RESTRICT
  
  FUNCTION RUN_REPORT(P_PAR_01  IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_02 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_03 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_04 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_05 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_06 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_07 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_08 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_09 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_10 IN VARCHAR2 DEFAULT NULL) RETURN LAWSUP.PKG_FILES.REC_DOC;		                           

end PKG_DOCX_018;
/

create or replace package body lawmain.PKG_DOCX_018 is

  FUNCTION RUN_REPORT(P_PAR_01  IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_02 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_03 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_04 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_05 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_06 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_07 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_08 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_09 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_10 IN VARCHAR2 DEFAULT NULL) RETURN LAWSUP.PKG_FILES.REC_DOC
   IS  
   L_XML XMLTYPE;                                        
	 L_TAB XMLTYPE;
   L_SQL SYS_REFCURSOR;     
	 L_GROUP VARCHAR2(50);
   L_REP LAWSUP.PKG_FILES.REC_DOC;
	 L_TYPE  VARCHAR2(50);    
  BEGIN         
    OPEN L_SQL FOR 
      SELECT TO_CHAR(S.DATE_PRET,'DD.MM.YYYY') "дата_долга",
			       LAWSUP.PKG_MORFER.F_GET_DECLENSION(PKG_PREF.F$C2('SBIT_MAIN'),'Dat','S') "начальник_сбыта_дп",
						 LAWSUP.PKG_MORFER.F_GET_DECLENSION(PKG_PREF.F$C2('FIN_DIR'),'Gen','S') "фин_дир_рп",
						 PKG_PREF.F$C2('ORG_MAIN_FIO') "гендир",
						 PKG_PREF.F$C2('FIN_DIR') "фин_дир",
						 PKG_PREF.F$C3('COURT_MAIN') "руководить_юр",
						 PKG_PREF.F$C2('SBIT_MAIN') "начальник_сбыта",
					   TO_CHAR(SYSDATE,'"«"DD"»"')||' '||LOWER(PKG_UTILS.F_GET_NAME_MONTHS(SYSDATE,'R'))||' '||TO_CHAR(SYSDATE,'YYYY" г."') "текущая_дата",
						 (SELECT SUM(S.DEBT_CREATED) FROM T_CLI_SHUTDOWN S,APEX_COLLECTIONS C   		
						  WHERE C.collection_name = 'SHUT_IDS' AND   s.ID_WORK = TO_NUMBER(C.c001)) "общий_долг"
			FROM T_FOLDERS S 
			WHERE S.ID_FOLDER = TO_NUMBER(P_PAR_01);

		SELECT S.TYPE_CODE INTO L_TYPE FROM V_FOLDERS S WHERE S.ID_FOLDER = TO_NUMBER(P_PAR_01);
		
		--ТАБЛИЧНАЯ ЧАСТЬ ОТЧЕТА
    SELECT XMLELEMENT("rows", 
            XMLAGG(XMLELEMENT("row", 
        			XMLELEMENT("cell", XMLATTRIBUTES('пункт' AS "tag"), ROWNUM), 
							XMLELEMENT("cell", XMLATTRIBUTES('договор' AS "tag"), DECODE(L_TYPE,'PT_SHUTDOWN_ALL',s.CWS_CTR_NUMBER,S.SEW_CTR_NUMBER)), 
							XMLELEMENT("cell", XMLATTRIBUTES('дата_договора' AS "tag"), to_char(DECODE(L_TYPE,'PT_SHUTDOWN_ALL',s.CWS_CTR_DATE,S.SEW_CTR_DATE),'dd.mm.yyyy"г."')), 
							XMLELEMENT("cell", XMLATTRIBUTES('абонент' AS "tag"), s.CLI_ALT_NAME),
						  XMLELEMENT("cell", XMLATTRIBUTES('адрес' AS "tag"), PKG_CLI_SHUTDOWN.F_GET_OBJECT_LIST(S.ID_WORK,10,'Y')),
							XMLELEMENT("cell", XMLATTRIBUTES('долг' AS "tag"), TO_CHAR(s.DEBT_CREATED, 'FM999G999G999G999G999G990D00'))												 						                                                                  
							)))
		INTO   L_TAB
		FROM V_CLI_SHUTDOWN S,
		    APEX_COLLECTIONS C 
		WHERE C.collection_name = 'SHUT_IDS'
		AND   s.ID_WORK = TO_NUMBER(C.c001);
		
		L_TAB := XMLTYPE('<tables><table name="таблица1"></table></tables>').appendChildXML('/tables/table',l_tab,NULL);			
		

		IF L_TYPE = 'PT_SHUTDOWN_ALL' THEN
       L_XML := PKG_CONTROLLER.F_DOCX_XML(P_SQL => L_SQL,P_REP_CODE => 'ORDER_RESTRICT');    		                          
		ELSE
			 L_XML := PKG_CONTROLLER.F_DOCX_XML(P_SQL => L_SQL,P_REP_CODE => 'ORDER_RESTRICT_SEW');    		                          
		END IF;	 
		L_XML := L_XML.APPENDCHILDXML('/request/input-data',l_tab,NULL);
		
    L_REP.P_BLOB := PKG_CONTROLLER.F_GET_REPORT_XML(L_XML.getClobVal());
    L_REP.P_FILE_NAME := 'Приказ на ограничение';
		
		RETURN l_rep;
	END;										
	
end PKG_DOCX_018;
/

