i�?CREATE OR REPLACE PACKAGE LAWMAIN.PKG_MAIL_02
IS
/* Автор: Бородулин Е.С.
   Дата:  21.12.2016
	 Назначение: Пакет отсылает email уведомление для ю.л 
*/

   PROCEDURE p_send_mail(P_ID_CLI_CASE IN NUMBER, P_EMAIL VARCHAR2, P_ATTACH VARCHAR2 DEFAULT NULL);
     PROCEDURE P_SEND_MASS(P_ID_FOLDER NUMBER,P_REPEAT VARCHAR2 DEFAULT 'N', P_ATTACH VARCHAR2, P_USER NUMBER);
END;
/

CREATE OR REPLACE PACKAGE BODY LAWMAIN.PKG_MAIL_02
IS

	G$HTML VARCHAR2(32000);

  PROCEDURE p_send_mail(P_ID_CLI_CASE IN NUMBER, P_EMAIL VARCHAR2, P_ATTACH VARCHAR2)
        IS
			L_RET    NUMBER;
			L_REP    LAWSUP.PKG_FILES.REC_DOC;
			L_TYPE   VARCHAR2(50);
    BEGIN                                                        
			IF p_email IS NULL THEN 
				raise_application_error(-20200,'email не указан');
		  END IF;                
			
				G$HTML := PKG_PREF.F$HTML('DEBT_INFORM');  
				
				SELECT W.TYPE_WORK INTO L_TYPE FROM T_CLI_DEBT_WORK W WHERE W.ID_WORK = P_ID_CLI_CASE;
				IF L_TYPE = 'CASE' THEN
				    FOR i IN (SELECT PKG_CLI_CASES_UTL.F_GET_CONTRACTS(S.ID_CASE,' и ') CTR_NUMBER
					              ,S.DEBT_PRETENSION
					              ,(SELECT L.PHONE FROM T_USER_LIST L WHERE L.ID = F$_USR_ID) PHONE
					        FROM V_CLI_CASES S
									WHERE S.ID_CASE = P_ID_CLI_CASE)
            LOOP
							G$HTML := REPLACE(G$HTML,'#CTRNUMBER#',I.CTR_NUMBER); -- договор.
							G$HTML := REPLACE(G$HTML,'#DEBT#', TO_CHAR(I.DEBT_PRETENSION,'FM999G999G999G999G990D99','NLS_NUMERIC_CHARACTERS='', ''')); -- текущий долг.
							G$HTML := REPLACE(G$HTML,'#PHONE#',i.phone);
            END LOOP;          
				ELSE
					 FOR i IN (SELECT PKG_CLI_CASES_UTL.F_GET_CONTRACTS(S.ID_WORK,' и ') CTR_NUMBER
					              ,S.DEBT_PRETENSION
					              ,(SELECT L.PHONE FROM T_USER_LIST L WHERE L.ID = F$_USR_ID) PHONE
					        FROM V_CLI_PRETENSION S
									WHERE S.ID_WORK = P_ID_CLI_CASE)
            LOOP
							G$HTML := REPLACE(G$HTML,'#CTRNUMBER#',I.CTR_NUMBER); -- договор.
							G$HTML := REPLACE(G$HTML,'#DEBT#', TO_CHAR(I.DEBT_PRETENSION,'FM999G999G999G999G990D99','NLS_NUMERIC_CHARACTERS='', ''')); -- текущий долг.
							G$HTML := REPLACE(G$HTML,'#PHONE#',i.phone);
            END LOOP;
				END IF;		
           
            L_RET := PKG_MAIL.F_MAKE_EMAIL(P_HEADER => PKG_PREF.F$C1('DEBT_INFORM')
                                           ,P_FROM => PKG_PREF.F$C1('USER_MAIL') -- do not change !!!! if change => change password in sup.pkg_mail!!!
                                           ,P_BODY => G$HTML
                                          );
            PKG_MAIL.P_ATTACHE_EMAIL_ADDR(P_EMAIL      => P_EMAIL
                                         ,P_ID_MAIL    => L_RET
                                         ,P_EMAIL_TYPE => PKG_MAIL.LIST_EMAIL_TYPE.TO_);
																				 
				FOR I IN (SELECT COLUMN_VALUE FROM TABLE(APEX_STRING.split(P_ATTACH,':')))																 
				LOOP
						L_REP := PKG_CONTROLLER.P_RETURN_BLOB(P_REP_CODE => I.COLUMN_VALUE
						                                     ,P_PAR_01 => P_ID_CLI_CASE
																								 ,P_PAR_10 => F$_USR_ID);    
																								 
						PKG_MAIL.P_ATTACHE_FILE(P_ID_MAIL => L_RET
						                       ,P_BLOB => L_REP.P_BLOB
																	 ,P_FILE_NAME => L_REP.P_FILE_NAME);																		 
				END LOOP;

        PKG_MAIL.P_SEND(P_ID => L_RET,P_SEND_TYPE => PKG_MAIL.LIST_TYPE_SEND.LTE);
				PKG_SEND_MAIL.P_INSERT_CLI_CASE_MAIL(P_ID_CLI_CASE,L_RET,'DEBT_INFORM');
 				COMMIT;
    END;
    
		
  PROCEDURE P_SEND_MASS(P_ID_FOLDER NUMBER,P_REPEAT VARCHAR2 DEFAULT 'N', P_ATTACH VARCHAR2, P_USER NUMBER)
		IS   
  	L_EMAIL  VARCHAR2(4000);
		L_RET    NUMBER;  
		L_REP    LAWSUP.PKG_FILES.REC_DOC;
	BEGIN         
		FOR J IN (SELECT P.ID_WORK FROM T_CLI_PRETENSION P  
			        WHERE P.ID_FOLDER = P_ID_FOLDER)
		LOOP	      
		 BEGIN				
			  G$HTML := PKG_PREF.F$HTML('DEBT_INFORM');   
				L_EMAIL := NULL;
		   	FOR i IN (SELECT PKG_CLI_CASES_UTL.F_GET_CONTRACTS(S.ID_WORK,' и ') CTR_NUMBER
					              ,S.DEBT_PRETENSION
					              ,(SELECT L.PHONE FROM T_USER_LIST L WHERE L.ID = F$_USR_ID) PHONE					        
												,em_t.det_val EMAIL_ADDRES
					        FROM V_CLI_PRETENSION S 
									LEFT OUTER JOIN (SELECT CD.DET_VAL, cd.id_client
																	 ,row_number() over (PARTITION BY cd.id_client order BY t.order_num ) as rn
																	 FROM T_CLI_CONTACT_DETAILS CD
																			 ,T_CLI_CONTACT_TYPES T    
																	 WHERE T.DET_TYPE = 'Адрес электронной почты'
																	 AND CD.DET_DESC = T.DET_DESC) em_t
									ON (em_t.id_client = s.ID_CLIENT AND em_t.rn = 1)		 								
									WHERE S.ID_WORK = J.ID_WORK
									AND  (NOT EXISTS (SELECT 1 
									                  FROM T_CLI_CASE_MAILS M 
																		WHERE M.ID_CASE = S.ID_WORK
																		AND   M.MAIL_TYPE ='DEBT_INFORM')
											  OR P_REPEAT = 'Y'))
            LOOP
							G$HTML := REPLACE(G$HTML,'#CTRNUMBER#',I.CTR_NUMBER); -- договор.
							G$HTML := REPLACE(G$HTML,'#DEBT#', TO_CHAR(I.DEBT_PRETENSION,'FM999G999G999G999G990D99','NLS_NUMERIC_CHARACTERS='', ''')); -- текущий долг.
							G$HTML := REPLACE(G$HTML,'#PHONE#',i.phone);
							L_EMAIL := I.EMAIL_ADDRES;
            END LOOP;
						
						IF L_EMAIL IS NULL THEN
							 CONTINUE;
						END IF;
           
            L_RET := PKG_MAIL.F_MAKE_EMAIL(P_HEADER => PKG_PREF.F$C1('DEBT_INFORM')
                                           ,P_FROM => PKG_PREF.F$C1('USER_MAIL') -- do not change !!!! if change => change password in sup.pkg_mail!!!
                                           ,P_BODY => G$HTML
                                          );
            PKG_MAIL.P_ATTACHE_EMAIL_ADDR(P_EMAIL      => L_EMAIL
                                         ,P_ID_MAIL    => L_RET
                                         ,P_EMAIL_TYPE => PKG_MAIL.LIST_EMAIL_TYPE.TO_);
																				 
				FOR I IN (SELECT COLUMN_VALUE FROM TABLE(APEX_STRING.split(P_ATTACH,':')))																 
				LOOP
						L_REP := PKG_CONTROLLER.P_RETURN_BLOB(P_REP_CODE => I.COLUMN_VALUE
						                                     ,P_PAR_01   => J.ID_WORK
																								 ,P_PAR_10   => P_USER);    
																								 
						PKG_MAIL.P_ATTACHE_FILE(P_ID_MAIL => L_RET
						                       ,P_BLOB => L_REP.P_BLOB
																	 ,P_FILE_NAME => L_REP.P_FILE_NAME);																		 
				END LOOP;

        PKG_MAIL.P_SEND(P_ID => L_RET,P_SEND_TYPE => PKG_MAIL.LIST_TYPE_SEND.LTE);
				PKG_SEND_MAIL.P_INSERT_CLI_CASE_MAIL(J.ID_WORK,L_RET,'DEBT_INFORM',P_USER);
 				COMMIT;
		 EXCEPTION
				WHEN OTHERS THEN
					PKG_LOG.P$LOG(p_PROG_UNIT => 'PKG_MAIL_02',p_MESS => DBMS_UTILITY.format_error_stack,P_C1 => J.ID_WORK);						
		 END;		
		END LOOP;		
	END;
		
END;
/

