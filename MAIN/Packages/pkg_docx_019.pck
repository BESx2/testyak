i�?create or replace package lawmain.PKG_DOCX_019 is

  -- Author  : EUGEN
  -- Created : 01.04.2020 15:02:04
  -- Purpose : Наряд на ограничение
	-- CODE    : BNK_CLAIM
  
  FUNCTION RUN_REPORT(P_PAR_01  IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_02 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_03 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_04 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_05 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_06 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_07 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_08 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_09 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_10 IN VARCHAR2 DEFAULT NULL) RETURN LAWSUP.PKG_FILES.REC_DOC;		                           

end PKG_DOCX_019;
/

create or replace package body lawmain.PKG_DOCX_019 is

  FUNCTION RUN_REPORT(P_PAR_01  IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_02 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_03 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_04 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_05 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_06 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_07 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_08 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_09 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_10 IN VARCHAR2 DEFAULT NULL) RETURN LAWSUP.PKG_FILES.REC_DOC
   IS  
   L_XML XMLTYPE;                                        
	 L_TAB XMLTYPE;
   L_SQL SYS_REFCURSOR;     
	 L_GROUP VARCHAR2(50);
   L_REP LAWSUP.PKG_FILES.REC_DOC;
	 L_TYPE VARCHAR2(100);    
	 
	 L_FMT VARCHAR2(50) := 'FM999G999G999G999G990D00';
	 L_NLS VARCHAR2(50) := 'NLS_NUMERIC_CHARACTERS='', ''';
	 
	 L_TOTAL_DEBT NUMBER;
	 L_TOTAL_PENY NUMBER;  
	 L_TOTAL_OD   NUMBER; 
	 L_EXECS      VARCHAR2(4000);
	 L_AMOUNT_WO  NUMBER;
	 L_PENY_WO    NUMBER;
	 L_DEBT_WO    NUMBER;
	 L_PAYED_WO   NUMBER;  
	 
	 L_PENY       NUMBER;
	 L_DEBT       NUMBER;  
	 L_GOV_TOLL   NUMBER;
  BEGIN   
    FOR I IN (SELECT SUM(NVL(EX.COURT_DEBT, S.SUM_DEBT) + NVL(NVL(EX.COURT_PENY, S.PENY),0) + NVL(NVL(EX.COURT_TOLL, S.GOV_TOLL),0)) TOTAL_DEBT,
			               SUM(NVL(EX.COURT_PENY, S.PENY)) TOTAL_PENY, 
										 SUM(NVL(EX.COURT_DEBT, S.SUM_DEBT)) TOTAL_OD,									 
										 LISTAGG(EX.NUM_EXEC,', ') WITHIN GROUP (ORDER BY EX.DATE_END) NUM_EXEC
			        FROM   T_CLI_CASES S
						  INNER  JOIN T_CLI_BANKRUPT_CASES BC ON (S.ID_CASE = BC.ID_CASE)
						  LEFT   OUTER JOIN T_CLI_COURT_EXEC EX 
							ON (EX.ID_CASE = S.ID_CASE 
						 			AND EX.ID = (SELECT MAX(EX.ID)
									             FROM   T_CLI_COURT_EXEC EX1
															 WHERE  EX1.ID_CASE = S.ID_CASE
															 AND    EX1.DATE_END IS NOT NULL))
							WHERE BC.ID_BANKRUPT = TO_NUMBER(P_PAR_01))
		LOOP
			L_TOTAL_DEBT := I.TOTAL_DEBT;
			L_TOTAL_PENY := I.TOTAL_PENY;  
			L_TOTAL_OD   := I.TOTAL_OD;
			L_EXECS      := I.NUM_EXEC;
		END LOOP;					
	  
		FOR I IN (SELECT SUM(FD.AMOUNT) AM, SUM(FD.DEBT) DEBT, SUM(D.PENY) PENY,
			               SUM(FD.AMOUNT - FD.DEBT) PAYED
			        FROM   T_CLI_CASES S,
							       T_CLI_DEBTS D,
										 T_CLI_FINDOCS FD,
						         T_CLI_BANKRUPT_CASES BC 
						  WHERE S.ID_CASE = BC.ID_CASE
							AND   S.ID_CASE = D.ID_WORK
							AND   FD.ID_DOC = D.ID_DOC
							AND   BC.ID_BANKRUPT = TO_NUMBER(P_PAR_01) 
							AND   NOT EXISTS (SELECT 1 FROM T_CLI_COURT_EXEC EX 
							                  WHERE EX.ID_CASE = S.ID_CASE 
																AND EX.DATE_END IS NOT NULL))
		LOOP
		  L_AMOUNT_WO := I.AM;
      L_PENY_WO   := I.PENY;
      L_DEBT_WO   := I.DEBT;
      L_PAYED_WO 	:= I.PAYED;	
		END LOOP;			
		
	  FOR I IN (SELECT SUM(EX.COURT_PENY) PENY, SUM(EX.COURT_DEBT) DEBT, SUM(EX.COURT_TOLL) TOLL
			        FROM   T_CLI_CASES S,
						         T_CLI_BANKRUPT_CASES BC,
										 T_CLI_COURT_EXEC EX 
							WHERE BC.ID_BANKRUPT = TO_NUMBER(P_PAR_01)
						  AND   S.ID_CASE = BC.ID_CASE
							AND   EX.ID_CASE = S.ID_CASE
							AND   EX.ID = (SELECT MAX(EX.ID)
									             FROM   T_CLI_COURT_EXEC EX1
															 WHERE  EX1.ID_CASE = S.ID_CASE
															 AND    EX1.DATE_END IS NOT NULL))
		LOOP
		 L_PENY     := I.PENY;  
     L_DEBT     := I.DEBT;
     L_GOV_TOLL	:= I.TOLL;			                                                                                          
		END LOOP;																	
						
    OPEN L_SQL FOR 
     SELECT CLI.CLI_ALT_NAME  "абонент"
		       ,CLI.OGRN "ОГРН_абонента"
					 ,CLI.INN  "ИНН_абонента"
					 ,PKG_CLIENTS.F_GET_ADDRESS(CLI.ID_CLIENT) "адрес_абонента"
					 ,6000 "госпошлина"
					 ,INITCAP(L.LAST_NAME||' '||L.FIRST_NAME||' '||L.SECOND_NAME) "исполнитель"
					 ,L.PHONE "телефон_исполнителя"  
					 ,(SELECT LISTAGG('№ '||CT.CTR_NUMBER||' от '||TO_CHAR(CT.CTR_DATE,'DD.MM.YYYY'),', ')
					         WITHIN GROUP (ORDER BY CT.CTR_NUMBER)
					   FROM T_CONTRACTS CT,
								(SELECT DISTINCT D.ID_CONTRACT
								 FROM T_CLI_DEBTS D,
											T_CLI_BANKRUPT_CASES BC
								 WHERE BC.ID_BANKRUPT = B.ID
								 AND   BC.ID_CASE = D.ID_WORK) T
						 WHERE CT.ID_CONTRACT = T.ID_CONTRACT) "договоры"
					,L_TOTAL_DEBT "общий_долг"
					,(SELECT TO_CHAR(TRUNC(MIN(FD.DOC_DATE),'MM'),'DD.MM.YYYY')||' - '
					         ||TO_CHAR(LAST_DAY(MAX(FD.DOC_DATE)),'DD.MM.YYYY')
					  FROM T_CLI_DEBTS D,
						     T_CLI_FINDOCS FD,
								 T_CLI_BANKRUPT_CASES BC
					  WHERE BC.ID_BANKRUPT = B.ID
					  AND   BC.ID_CASE = D.ID_WORK 
						AND   FD.ID_DOC = D.ID_DOC) "общий_период"
					,L_TOTAL_PENY "общая_сумма_пени"
					,L_EXECS   "решения"
					,L_PENY     "пени_с_реш"
					,L_DEBT     "од_с_реш"
					,L_GOV_TOLL "пош_с_реш"  
					,L_AMOUNT_WO "выст_сумма"
          ,L_PENY_WO   "пени_без_решения"
          ,L_DEBT_WO   "долг_без_решения"
          ,L_PAYED_WO  "опл_сумма"
					,L_TOTAL_OD  "общий_основной_долг"
					,L_GOV_TOLL "общая_госпошлина"		
					,B.BNK_CASE_NUM  "номер_дела_банкротства"
					,B.DATE_BNK  "дата_признания"								
					,INITCAP(L.LAST_NAME||' '||SUBSTR(L.FIRST_NAME,1,1)||'. '||SUBSTR(L.SECOND_NAME,1,1)||'.') "испол_краткое"
		 FROM T_CLI_BANKRUPT B,
		      T_CLIENTS CLI,
					T_USER_LIST L 
		 WHERE B.ID = TO_NUMBER(P_PAR_01)
		 AND   CLI.ID_CLIENT = B.ID_CLIENT
     AND   L.ID = F$_USR_ID;
		
		SELECT B.CLAIM_TYPE INTO L_TYPE FROM T_CLI_BANKRUPT B WHERE B.ID = TO_NUMBER(P_PAR_01);
		
		L_XML := PKG_CONTROLLER.F_DOCX_XML(P_SQL => L_SQL,P_REP_CODE => L_TYPE);    		                          
		
		--ТАБЛИЧНАЯ ЧАСТЬ ОТЧЕТА
    SELECT XMLELEMENT("rows", 
            XMLAGG(XMLELEMENT("row", 
        			XMLELEMENT("cell", XMLATTRIBUTES('догвор_таб' AS "tag"), PKG_CLI_CASES_UTL.F_GET_CONTRACTS(S.ID_CASE,', ')), 
							XMLELEMENT("cell", XMLATTRIBUTES('период_таб' AS "tag"), PKG_CLI_CASES_UTL.F_GET_DEBT_PERIOD(S.ID_CASE)),
							XMLELEMENT("cell", XMLATTRIBUTES('номер_таб' AS "tag"), EX.NUM_EXEC),
							XMLELEMENT("cell", XMLATTRIBUTES('од_таб' AS "tag"), TO_CHAR(EX.COURT_DEBT,L_FMT,L_NLS)),
							XMLELEMENT("cell", XMLATTRIBUTES('пени_таб' AS "tag"), TO_CHAR(EX.COURT_PENY,L_FMT,L_NLS)),
							XMLELEMENT("cell", XMLATTRIBUTES('пош_таб' AS "tag"), TO_CHAR(EX.COURT_TOLL,L_FMT,L_NLS)),
							XMLELEMENT("cell", XMLATTRIBUTES('дата_таб' AS "tag"), TO_CHAR(EX.DATE_END,'DD.MM.YYYY'))
							)))
		INTO   L_TAB
	  FROM   T_CLI_CASES S,
					 T_CLI_BANKRUPT_CASES BC,
					 T_CLI_COURT_EXEC EX 
		WHERE BC.ID_BANKRUPT = TO_NUMBER(P_PAR_01)
		AND   S.ID_CASE = BC.ID_CASE
		AND   EX.ID_CASE = S.ID_CASE
		AND   EX.ID = (SELECT MAX(EX.ID)
		               FROM   T_CLI_COURT_EXEC EX1
									 WHERE  EX1.ID_CASE = S.ID_CASE
									 AND    EX1.DATE_END IS NOT NULL);
		
		IF L_TAB.existsNode('/rows/row') != 1 THEN			
			L_TAB := l_tab.appendChildXML('/rows',XMLTYPE('<rows><row><cell tag="догвор_таб"/><cell tag="период_таб"/><cell tag="номер_таб"/>
			                              <cell tag="од_таб"/><cell tag="пени_таб"/><cell tag="пош_таб"/><cell tag="дата_таб"/></row></rows>'),NULL);
	  END IF;	
		L_TAB := XMLTYPE('<tables><table name="таблица1"></table></tables>').appendChildXML('/tables/table',l_tab,NULL);			
		    
		L_XML := L_XML.APPENDCHILDXML('/request/input-data',l_tab,NULL);
		       
		
		SELECT XMLELEMENT("rows", 
            XMLAGG(XMLELEMENT("row", 
        			XMLELEMENT("cell", XMLATTRIBUTES('счет_таб2' AS "tag"), fd.doc_number||' от '||to_char(fd.doc_date,'dd.mm.yyyy')), 
							XMLELEMENT("cell", XMLATTRIBUTES('выст_таб2' AS "tag"), to_char(fd.amount,l_fmt,l_nls)),
							XMLELEMENT("cell", XMLATTRIBUTES('оплач_таб2' AS "tag"), to_char( fd.amount - fd.debt,l_fmt,l_nls)),
							XMLELEMENT("cell", XMLATTRIBUTES('долг_таб2' AS "tag"), TO_CHAR(fd.debt,L_FMT,L_NLS)),
							XMLELEMENT("cell", XMLATTRIBUTES('пени_таб2' AS "tag"), TO_CHAR(d.peny,L_FMT,L_NLS))
							)))
		INTO   L_TAB
		FROM   T_CLI_CASES S,
					 T_CLI_DEBTS D,
					 T_CLI_FINDOCS FD,
					 T_CLI_BANKRUPT_CASES BC 
		WHERE S.ID_CASE = BC.ID_CASE
		AND   S.ID_CASE = D.ID_WORK
		AND   FD.ID_DOC = D.ID_DOC
		AND   BC.ID_BANKRUPT = TO_NUMBER(P_PAR_01) 
		AND   NOT EXISTS (SELECT 1 FROM T_CLI_COURT_EXEC EX 
											WHERE EX.ID_CASE = S.ID_CASE 
											AND EX.DATE_END IS NOT NULL);
		
		IF L_TAB.existsNode('/rows/row') != 1 THEN 
			L_TAB := l_tab.appendChildXML('/rows',XMLTYPE('<rows><row><cell tag="счет_таб2"/><cell tag="выст_таб2"/><cell tag="оплач_таб2"/>
			                              <cell tag="долг_таб2"/><cell tag="пени_таб2"/></row></rows>'),NULL);
 		END IF;    
    L_TAB := XMLTYPE('<tables><table name="таблица2"></table></tables>').appendChildXML('/tables/table',l_tab,NULL);			
    L_XML := L_XML.APPENDCHILDXML('/request/input-data',l_tab,NULL);
		
						       
		SELECT XMLELEMENT("lists",
		    XMLELEMENT("list", XMLATTRIBUTES('список_долгов' AS "name"), 
            XMLAGG(XMLELEMENT("items", 
        			XMLELEMENT("item", 'по договору '||T.CTR_NUM||' сумма основного долга '||TO_CHAR(T.DEBT,L_FMT,L_NLS)
							                   ||' руб., '||TO_CHAR(T.PENY,L_FMT,L_NLS)||' руб. пени'
																 ||NVL2(T.TOLL,', '||TO_CHAR(T.TOLL,L_FMT,L_NLS)||' руб. государственной пошлины.','.')) 							
							))))
		INTO   L_TAB
		FROM
		(SELECT CASE 
		          WHEN S.CWS_ID_CONTRACT IS NOT NULL AND S.SEW_ID_CONTRACT IS NOT NULL THEN
								  S.CWS_CTR_NUM||' и '||s.SEW_CTR_NUM
							WHEN S.CWS_ID_CONTRACT IS NOT NULL THEN S.CWS_CTR_NUM
							WHEN S.SEW_ID_CONTRACT IS NOT NULL THEN S.SEW_CTR_NUM
						END CTR_NUM,		
		       SUM(NVL(EX.COURT_DEBT, S.SUM_DEBT)) DEBT,
			     SUM(NVL(EX.COURT_PENY, S.PENY)) PENY,
					 SUM(NVL(EX.COURT_TOLL, S.GOV_TOLL)) TOLL					 
		FROM   V_CLI_CASES S
		INNER  JOIN T_CLI_BANKRUPT_CASES BC ON (S.ID_CASE = BC.ID_CASE)
		LEFT   OUTER JOIN T_CLI_COURT_EXEC EX 
		ON (EX.ID_CASE = S.ID_CASE 
				AND EX.ID = (SELECT MAX(EX.ID)
										 FROM   T_CLI_COURT_EXEC EX1
										 WHERE  EX1.ID_CASE = S.ID_CASE
										 AND    EX1.DATE_END IS NOT NULL))
		WHERE BC.ID_BANKRUPT = TO_NUMBER(P_PAR_01)
		GROUP BY S.CWS_ID_CONTRACT, S.SEW_ID_CONTRACT,S.CWS_CTR_NUM,S.SEW_CTR_NUM) T;
		
    L_XML := L_XML.APPENDCHILDXML('/request/input-data',l_tab,NULL);
		
		
    L_REP.P_BLOB := PKG_CONTROLLER.F_GET_REPORT_XML(L_XML.getClobVal());
    L_REP.P_FILE_NAME := 'Заявление';
		
		RETURN l_rep;
	END;										
	
end PKG_DOCX_019;
/

