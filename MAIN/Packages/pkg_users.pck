i�?CREATE OR REPLACE PACKAGE LAWMAIN.pkg_users
IS

  TYPE TPE_USER IS RECORD(
													ID  t_user_list.ID%TYPE
													,PHONE  t_user_list.PHONE%TYPE
													,PHONE_PRIV  t_user_list.PHONE_PRIV%TYPE
													,COMMENTS  t_user_list.COMMENTS%TYPE
													,LAST_NAME  t_user_list.LAST_NAME%TYPE
													,USERNAME  t_user_list.USERNAME%TYPE
													,PASSWORD  t_user_list.PASSWORD%TYPE
													,EMAIL  t_user_list.EMAIL%TYPE
													,DATE_CREATED  t_user_list.DATE_CREATED%TYPE
													,CREATED_BY  t_user_list.CREATED_BY%TYPE
													,DATE_MOFIFIED  t_user_list.DATE_MOFIFIED%TYPE
													,MODIFIED_BY  t_user_list.MODIFIED_BY%TYPE
													,IS_FIRST_LOGIN  t_user_list.IS_FIRST_LOGIN%TYPE
													,FIRST_NAME  t_user_list.FIRST_NAME%TYPE
													,SECOND_NAME	t_user_list.SECOND_NAME%TYPE
													,CREATED_BY_NAME  VARCHAR2(2000)
													,MODIFIED_BY_NAME  VARCHAR2(2000)
													,TIME_ZONE NUMBER
													,is_locked VARCHAR2(1)
													,id_dep NUMBER
													,id_block_reason NUMBER              
													,position    T_USER_LIST.POSITION%TYPE
													,dover_info  T_USER_LIST.DOVER_INFO%TYPE
												);

 
 FUNCTION CHK_PWD(P_PWD VARCHAR2) RETURN VARCHAR2;
 FUNCTION F_CREATE_USER(	p_username IN VARCHAR2
													,p_password IN VARCHAR2
													,P_email IN VARCHAR2
													,p_last_name IN VARCHAR2
													,p_first_name IN VARCHAR2
													,p_second_name IN VARCHAR2
													,p_phone IN VARCHAR2
													,p_phone_priv IN VARCHAR2
													,p_comments IN VARCHAR2
													,p_is_locked IN VARCHAR2
													,p_id_dep IN NUMBER
													,p_id_block_reason NUMBER DEFAULT NULL												
                          ,p_dover_info      VARCHAR2 DEFAULT NULL
													,p_position        VARCHAR2 DEFAULT NULL
                         ) RETURN NUMBER;

 FUNCTION F_EDIT_USER(	p_id IN NUMBER
 												,p_username IN VARCHAR2
												,p_password IN VARCHAR2
												,P_email IN VARCHAR2
												,p_last_name IN VARCHAR2
												,p_first_name IN VARCHAR2
												,p_second_name IN VARCHAR2
												,p_phone IN VARCHAR2
												,p_phone_priv IN VARCHAR2
												,p_comments IN VARCHAR2
												,p_is_locked IN VARCHAR2
												,p_id_dep IN NUMBER
												,p_id_block_reason NUMBER
                        ,p_dover_info      VARCHAR2 DEFAULT NULL
												,p_position        VARCHAR2 DEFAULT NULL
											 ) RETURN NUMBER;

	FUNCTION p_user_info(P_ID IN NUMBER
												) RETURN TPE_USER;
												
	FUNCTION F_GET_USR_FRSTNAME_BY_ID(P_ID IN NUMBER) RETURN VARCHAR2;
	FUNCTION F_GET_USR_USERNAME_BY_ID(P_ID IN NUMBER) RETURN VARCHAR2;
	FUNCTION F_GET_FIO_BY_ID(P_ID IN NUMBER,P_SOCR IN VARCHAR2 DEFAULT 'N', P_ORDER IN VARCHAR2 DEFAULT 'FIO') RETURN VARCHAR2;

	FUNCTION CUSTOM_AUTH (P_USERNAME IN VARCHAR2
												,P_PASSWORD IN VARCHAR2)	RETURN BOOLEAN;

	FUNCTION F_GET_USER_ID_DEP(P_User_id NUMBER) RETURN NUMBER;

	PROCEDURE P_CHANGE_PASS(P_USR_ID IN NUMBER,P_PASS IN VARCHAR2);
	PROCEDURE P_CHANGE_PHONE(P_USR_ID IN NUMBER,P_PHONE IN VARCHAR2, P_TYPE VARCHAR2 DEFAULT 'JOB');
	PROCEDURE P_CHANGE_EMAIL(P_USR_ID IN NUMBER,P_EMAIL IN VARCHAR2);

	-- For page 102
	/*FUNCTION F_IS_USER_EXIST_BY_EMAIL(P_EMAIL IN VARCHAR2) RETURN BOOLEAN;*/

	FUNCTION F_GET_USER_ID_BY_USERNAME(P_NAME IN VARCHAR2) RETURN NUMBER;

	/*FUNCTION F_BLOCK_USER(P_ID NUMBER) RETURN NUMBER;
	FUNCTION F_UNBLOCK_USER(P_ID NUMBER) RETURN NUMBER;*/


	PROCEDURE P_INSERT_SCOPE (P_ID_USER NUMBER
													 ,P_ID_DEP NUMBER);

	PROCEDURE P_DELETE_SCOPE(P_ID NUMBER);
  /*
	FUNCTION F_INSERT_SIGNER (P_ID_USER NUMBER
													,P_ID_SIGNER NUMBER) RETURN NUMBER;

	PROCEDURE P_DELETE_SIGNER(P_ID NUMBER);

	FUNCTION F_INSERT_AGENT (P_ID_USER NUMBER
													,P_ID_AGENT NUMBER) RETURN NUMBER;

	PROCEDURE P_DELETE_AGENT(P_ID NUMBER);

	PROCEDURE P_JOB_LOCK_USERS;

	-- Перенос клиентов, договоров и реестров созданных пользователем в его отделение
	PROCEDURE P_MOVE_CLI_CNTR_PCK_TO_DEP (P_ID_USER NUMBER, P_ID_DEP NUMBER);*/
  FUNCTION F_GET_PHONE_BY_ID(P_ID IN NUMBER, P_WITH_FORMATING IN VARCHAR2 DEFAULT 'Y') RETURN VARCHAR2;

END;
/

CREATE OR REPLACE PACKAGE BODY LAWMAIN.pkg_users
IS

	FUNCTION F_GET_USR_FRSTNAME_BY_ID(P_ID IN NUMBER) RETURN VARCHAR2
	IS
	BEGIN
		FOR i IN (SELECT * FROM T_USER_LIST WHERE id = P_ID)
		LOOP
			RETURN i.first_name;
		END LOOP;
		RETURN '-';
	END; 
	
	FUNCTION F_GET_PHONE_BY_ID(P_ID IN NUMBER, P_WITH_FORMATING IN VARCHAR2 DEFAULT 'Y') RETURN VARCHAR2
	IS
	BEGIN
		IF P_WITH_FORMATING = 'Y' THEN
			FOR i IN (SELECT NVL(PKG_UTILS.F_MAKE_PHONE(L.PHONE),'-') PHONE FROM T_USER_LIST L WHERE L.id = P_ID)
			LOOP
				RETURN i.PHONE;
			END LOOP;
		ELSE
			FOR i IN (SELECT NVL(L.PHONE,'-') PHONE FROM T_USER_LIST L WHERE L.id = P_ID)
			LOOP
				RETURN i.PHONE;
			END LOOP;			
		END IF;
		RETURN '-';
	END;

	FUNCTION F_GET_FIO_BY_ID(P_ID IN NUMBER,P_SOCR IN VARCHAR2 DEFAULT 'N', P_ORDER IN VARCHAR2 DEFAULT 'FIO') RETURN VARCHAR2
	IS
	BEGIN
		FOR i IN (SELECT * FROM T_USER_LIST WHERE id = P_ID)
		LOOP
			
			IF P_ORDER = 'FIO' THEN
				IF P_SOCR = 'Y' THEN
					RETURN i.last_name||' '||substr(i.first_name,1,1)||'. '||substr(i.second_name,1,1)||'.';
				ELSE
					RETURN i.last_name||' '||i.first_name||' '||i.second_name;
				END IF;
			ELSIF P_ORDER = 'IOF' THEN
				IF P_SOCR = 'Y' THEN
					RETURN substr(i.first_name,1,1)||'. '||substr(i.second_name,1,1)||'. '||i.last_name;
				ELSE
					RETURN i.first_name||' '||i.second_name||' '||i.last_name;
				END IF;				
			END IF;
		END LOOP;
		RETURN '-';
	END F_GET_FIO_BY_ID;

	FUNCTION F_GET_USR_USERNAME_BY_ID(P_ID IN NUMBER) RETURN VARCHAR2
	IS
	BEGIN
		FOR i IN (SELECT * FROM T_USER_LIST WHERE id = P_ID)
		LOOP
			RETURN i.username;
		END LOOP;
		RETURN '-';
	END;


 FUNCTION CHK_PWD(P_PWD VARCHAR2) RETURN VARCHAR2
 IS
 	l_ret VARCHAR2(100) := NULL;
	chk_number VARCHAR2(1) := 'N';
 BEGIN

 	IF length(P_PWD) < 8 THEN
		l_ret := 'Кол-во символов должно быть не меньше 8';
	END IF;

  FOR i IN 1..length(P_PWD)
	LOOP

	  IF substr(P_PWD,i,1) IN ('1','2','3','4','5','6','7','8','9','0')
		THEN
			chk_number := 'Y';
		END IF;

	END LOOP;

	IF chk_number = 'N' THEN
		l_ret := 'В пароле должны быть цифры';
	END IF;

	RETURN l_ret;

 END;



 FUNCTION F_CREATE_USER(	p_username IN VARCHAR2
													,p_password IN VARCHAR2
													,P_email IN VARCHAR2
													,p_last_name IN VARCHAR2
													,p_first_name IN VARCHAR2
													,p_second_name IN VARCHAR2
													,p_phone IN VARCHAR2
													,p_phone_priv IN VARCHAR2
													,p_comments IN VARCHAR2
													,p_is_locked IN VARCHAR2
													,p_id_dep IN NUMBER
													,p_id_block_reason NUMBER DEFAULT NULL
													,p_dover_info      VARCHAR2 DEFAULT NULL
													,p_position        VARCHAR2 DEFAULT NULL
                         ) RETURN NUMBER
	IS
	L_id NUMBER;
	BEGIN
 INSERT INTO T_USER_LIST
		 (ID
		 ,USERNAME
		 ,PASSWORD
		 ,EMAIL
		 ,DATE_CREATED
		 ,CREATED_BY
		 ,DATE_MOFIFIED
		 ,MODIFIED_BY
		 ,IS_FIRST_LOGIN
		 ,LAST_NAME
		 ,FIRST_NAME
		 ,SECOND_NAME
		 ,phone
		 ,comments
		 ,IS_LOCKED
		 ,ID_DEP
		 ,LAST_LOGIN
		 ,ID_BLOCK_REASON  
		 ,PHONE_PRIV
		 ,dover_info
		 ,POSITION
		 )
	 VALUES
		 (seq_main.nextval
		 ,TRIM(upper(P_USERNAME))
		 ,TRIM(lower(P_password))
		 ,trim(lower(P_email))
		 ,SYSDATE
		 ,F$_USR_ID
		 ,NULL
		 ,NULL
		 ,'N'
		 ,trim(upper(P_LAST_NAME))
		 ,TRIM(UPPER(P_FIRST_NAME))
		 ,TRIM(UPPER(P_SECOND_NAME))
		 ,TRIM(p_phone)
		 ,TRIM(p_comments)
		 ,UPPER(TRIM(P_IS_LOCKED))
		 ,p_id_dep
		 ,SYSDATE
		 ,p_id_block_reason 
		 ,TRIM(p_phone_priv)  
		 ,p_dover_info
		 ,p_position
		 )
	 RETURNING ID INTO L_ID;
	RETURN l_id;

	END;

 FUNCTION F_EDIT_USER(	p_id IN NUMBER
 												,p_username IN VARCHAR2
												,p_password IN VARCHAR2
												,P_email IN VARCHAR2
												,p_last_name IN VARCHAR2
												,p_first_name IN VARCHAR2
												,p_second_name IN VARCHAR2
												,p_phone IN VARCHAR2
												,p_phone_priv IN VARCHAR2
												,p_comments IN VARCHAR2
												,p_is_locked IN VARCHAR2
												,p_id_dep IN NUMBER
												,p_id_block_reason NUMBER
                        ,p_dover_info      VARCHAR2 DEFAULT NULL
												,p_position        VARCHAR2 DEFAULT NULL
											 ) RETURN NUMBER
	IS
	L$ID NUMBER := f$_usr_id;
	BEGIN
		UPDATE t_user_list ss
		 SET 	ss.username = TRIM(upper(P_USERNAME))
		 			,ss.password = TRIM(lower(p_password))
					,ss.email = TRIM(lower(P_email))
					,ss.date_mofified = SYSDATE
					,ss.modified_by =  L$ID
					,ss.last_name = TRIM(upper(P_LAST_NAME))
					,ss.first_name = TRIM(upper(P_FIRST_NAME))
					,ss.second_name = TRIM(upper(P_SECOND_NAME))
				  ,ss.phone = TRIM(p_phone)
					,ss.comments = TRIM(p_comments)
					,ss.is_locked = UPPER(TRIM(P_IS_LOCKED))
					,ss.id_dep = p_id_dep
					,ss.id_block_reason = p_id_block_reason
					,ss.phone_priv = TRIM(p_phone_priv)                   
					,SS.DOVER_INFO = p_dover_info
					,SS.POSITION =   p_position
		WHERE id = p_id;
		RETURN p_ID;
	END;
  
	
	FUNCTION p_user_info(P_ID IN NUMBER
												) RETURN TPE_USER
		IS
		L$ TPE_USER;
	BEGIN
		FOR i IN (SELECT * FROM T_USER_LIST s WHERE s.id = P_ID)
		LOOP
			l$.PHONE  := i.PHONE;
			l$.COMMENTS  := i.COMMENTS;
			l$.ID  := i.ID;
			l$.LAST_NAME  := i.LAST_NAME;
			l$.USERNAME  := i.USERNAME;
			l$.PASSWORD  := i.PASSWORD;
			l$.EMAIL  := i.EMAIL;
			l$.DATE_CREATED  := i.DATE_CREATED;
			l$.CREATED_BY  := i.CREATED_BY;
			l$.DATE_MOFIFIED  := i.DATE_MOFIFIED;
			l$.MODIFIED_BY  := i.MODIFIED_BY;
			l$.IS_FIRST_LOGIN  := i.IS_FIRST_LOGIN;
			l$.FIRST_NAME  := i.FIRST_NAME;
			l$.SECOND_NAME  := i.SECOND_NAME;  
			l$.PHONE_PRIV   := i.phone_priv;

			l$.CREATED_BY_NAME  := F_GET_USR_FRSTNAME_BY_ID(i.created_by);
			l$.MODIFIED_BY_NAME  := F_GET_USR_FRSTNAME_BY_ID(i.modified_by);

			l$.is_locked := i.is_locked;
			l$.id_dep := i.id_dep;
			l$.id_block_reason := i.id_block_reason;                
		END LOOP;
		RETURN l$;
	END; 
  
	
	FUNCTION F_IS_USER_EXIST(P_USER IN VARCHAR2) RETURN BOOLEAN
		IS
		L_RET	BOOLEAN := FALSE;
		L_CNT	NUMBER;
	BEGIN
		SELECT COUNT(1) INTO L_CNT
				FROM T_USER_LIST LST
			WHERE LST.USERNAME = UPPER(TRIM(P_USER));

		IF L_CNT = 0 THEN
			L_RET := FALSE;
		ELSE
			L_RET := TRUE;
		END IF;

		RETURN L_RET;
	END; 
   
	
	FUNCTION CUSTOM_AUTH (P_USERNAME IN VARCHAR2
												,P_PASSWORD IN VARCHAR2)	RETURN BOOLEAN
		is
		l_password 				varchar2(4000);
		l_first_login     VARCHAR2(1);
		L_ID_AUTH 				NUMBER;
	BEGIN 	
		IF F_IS_USER_EXIST(P_USERNAME) = FALSE THEN
			RETURN FALSE;
		END IF;

		FOR I IN (
									SELECT LST.Password
											FROM T_USER_LIST LST
										WHERE LST.USERNAME = TRIM(UPPER(P_USERNAME)) AND lst.is_locked = 'Y'
							)
		LOOP
			RETURN FALSE;
		END LOOP;


		FOR I IN (
									SELECT LST.Password, lst.is_first_login, lst.id
											FROM T_USER_LIST LST
										WHERE LST.USERNAME = TRIM(UPPER(P_USERNAME))
							)
		LOOP
			l_password := i.password;
			l_first_login := i.is_first_login;
			L_ID_AUTH := i.id;
		END LOOP;

		IF trim(lower(l_password)) = TRIM(lower(P_PASSWORD)) THEN

		  IF l_first_login = 'N' THEN
				UPDATE t_user_list l
					SET l.is_first_login = 'Y'
					WHERE l.username = TRIM(UPPER(P_USERNAME));
			END IF;

			UPDATE t_user_list l
				SET l.last_login = SYSDATE
				WHERE l.username = TRIM(UPPER(P_USERNAME));

			INSERT INTO t_user_auth_log (id,id_user,date_created,ip_addr)
					VALUES(seq_main.nextval,L_ID_AUTH,SYSDATE,OWA_UTIL.get_cgi_env ('REMOTE_ADDR'));

			RETURN TRUE;
		ELSE
			RETURN FALSE;
		END IF;

	END CUSTOM_AUTH;


 
	FUNCTION F_GET_USER_ID_DEP(P_User_id NUMBER) RETURN NUMBER
		IS
		L_ret NUMBER;
	BEGIN

		SELECT l.id_dep
			INTO L_ret
			FROM t_user_list l
		 WHERE l.id = P_User_id;

		RETURN l_ret;
	END F_GET_USER_ID_DEP;




	PROCEDURE P_CHANGE_PASS(P_USR_ID IN NUMBER,P_PASS IN VARCHAR2)
		IS
	BEGIN
		UPDATE t_user_list d
		 SET d.password = P_PASS
		WHERE id = P_USR_ID;
	END P_CHANGE_PASS;
  
	
	PROCEDURE P_CHANGE_PHONE(P_USR_ID IN NUMBER,P_PHONE IN VARCHAR2, P_TYPE VARCHAR2 DEFAULT 'JOB')
		IS
	BEGIN
	IF P_TYPE = 'JOB' THEN	
		UPDATE t_user_list d
		 SET d.phone = P_PHONE
		WHERE to_char(id) = P_USR_ID; 
	END IF;
	IF P_TYPE = 'PRIV' THEN	
		UPDATE t_user_list d
		 SET d.phone_priv = P_PHONE
		WHERE to_char(id) = P_USR_ID; 
	END IF;				
	END P_CHANGE_PHONE;

		
	PROCEDURE P_CHANGE_EMAIL(P_USR_ID IN NUMBER,P_EMAIL IN VARCHAR2)
		IS
	BEGIN
		UPDATE t_user_list d
		 SET d.email = P_EMAIL
		WHERE id = P_USR_ID;
	END P_CHANGE_EMAIL;


  /*
	FUNCTION F_IS_USER_EXIST_BY_EMAIL(P_EMAIL IN VARCHAR2) RETURN BOOLEAN
		IS
	BEGIN
		if TRIM(P_EMAIL) is null then
		 apex_application.g_notification :='Введите EMail';
		 RETURN FALSE;
		end if;

		FOR i IN (SELECT * FROM t_user_list s WHERE s.email = TRIM(LOWER(P_EMAIL)) AND s.Is_Locked = 'Y')
		LOOP
		 apex_application.g_notification :='Аккаунт заблокирован';
		 RETURN FALSE;
		END LOOP;

		FOR i IN (SELECT * FROM t_user_list s WHERE s.email = TRIM(LOWER(P_EMAIL)))
		LOOP
		 apex_application.g_notification := NULL;
		 RETURN TRUE;
		END LOOP;

		 apex_application.g_notification :='Пользователь с таким e-mail отсутствует. Проверьте email';
		RETURN FALSE;
	END F_IS_USER_EXIST_BY_EMAIL; */


  
	FUNCTION F_GET_USER_ID_BY_USERNAME(P_NAME IN VARCHAR2) RETURN NUMBER
		IS
	BEGIN
		FOR i IN (SELECT * FROM t_User_List S WHERE s.username = TRIM(UPPER(P_NAME))) LOOP
			RETURN i.id;
		END LOOP;
		RETURN NULL;
	END F_GET_USER_ID_BY_USERNAME;


  /*

	FUNCTION F_BLOCK_USER(P_ID NUMBER) RETURN NUMBER
		IS
	BEGIN

		UPDATE t_user_list l
			SET l.is_locked = 'Y'
			WHERE l.id = P_ID;

		RETURN '1';

	END F_BLOCK_USER;




	FUNCTION F_UNBLOCK_USER(P_ID NUMBER) RETURN NUMBER
		IS
	BEGIN

		UPDATE t_user_list l
			SET l.is_locked = 'N',
			    l.id_block_reason = NULL
			WHERE l.id = P_ID;

		RETURN '1';
	END F_UNBLOCK_USER;
*/


	PROCEDURE P_INSERT_SCOPE (P_ID_USER NUMBER
													,P_ID_DEP NUMBER)
	IS
	BEGIN

		INSERT INTO t_user_scope(id
														,id_user
														,id_dep)
				VALUES(seq_main.nextval
							,P_ID_USER
							,P_ID_DEP);
	END;

	PROCEDURE P_DELETE_SCOPE(P_ID NUMBER)
	IS
	BEGIN

		DELETE FROM t_user_scope s
			WHERE s.id  = P_ID;

	END;

/*
	FUNCTION F_INSERT_SIGNER (P_ID_USER NUMBER
													,P_ID_SIGNER NUMBER) RETURN NUMBER
	IS
		l_ret NUMBER;
	BEGIN

		INSERT INTO t_user_signers(id
															,id_user
															,id_signer
															,date_created
															,created_by_id)
				VALUES(seq_main.nextval
							,P_ID_USER
							,P_ID_SIGNER
							,SYSDATE
							,f$_usr_id) RETURNING id INTO l_ret;

		RETURN l_ret;
	END;


	PROCEDURE P_DELETE_SIGNER(P_ID NUMBER)
	IS
	BEGIN

		DELETE FROM t_user_signers s
			WHERE s.id  = P_ID;

	END;

	FUNCTION F_INSERT_AGENT (P_ID_USER NUMBER
													,P_ID_AGENT NUMBER) RETURN NUMBER
	IS
		l_ret NUMBER;
	BEGIN

		INSERT INTO t_user_agents(id
															,id_user
															,id_agent
															,date_created
															,created_by_id)
				VALUES(seq_main.nextval
							,P_ID_USER
							,P_ID_AGENT
							,SYSDATE
							,f$_usr_id) RETURNING id INTO l_ret;

		RETURN l_ret;
	END;


	PROCEDURE P_DELETE_AGENT(P_ID NUMBER)
	IS
	BEGIN

		DELETE FROM t_user_agents s
			WHERE s.id  = P_ID;

	END;

	-- блокировка пользователей по истечению 1 месяца после последнего входа --
	PROCEDURE P_JOB_LOCK_USERS
  IS
	 	l_id NUMBER;
	BEGIN
	  SELECT br.id INTO l_id FROM t_user_block_reason br
		WHERE br.code = 'AUTO_BLOCK';
		FOR i IN (SELECT *
								FROM t_user_list l
							 WHERE months_between(sysdate,l.last_login) >= 1
							 	 AND l.is_locked = 'N')
		LOOP

			UPDATE t_user_list l
				 SET l.is_locked = 'Y'
				 		,l.last_login = SYSDATE
						,l.id_block_reason = l_id
				WHERE l.id = i.id;

		END LOOP;

	END P_JOB_LOCK_USERS;


	-- Перенос клиентов, договоров и реестров созданных пользователем в его отделение
	PROCEDURE P_MOVE_CLI_CNTR_PCK_TO_DEP (P_ID_USER NUMBER, P_ID_DEP NUMBER)
	IS
		l$ PKG_CLIENTS.TPE_CLIENT;
	BEGIN

		FOR i IN (SELECT cl.id FROM t_cli_clients cl
								WHERE cl.created_by_id = P_ID_USER)
		LOOP

			l$ := PKG_CLIENTS.F_GET_CLIENT_INFO(i.id);

			PKG_CLIENTS.P_UPDATE_CLIENTS(P_ID => i.id
																	,P_FIRST_NAME => l$.FIRST_NAME
																	,P_LAST_NAME => l$.LAST_NAME
																	,P_SECOND_NAME => l$.SECOND_NAME
																	,P_SEX => l$.SEX
																	,P_BIRTHDATE => l$.BIRTHDATE
																	,P_DOCUMENT_TYPE_ID => l$.DOCUMENT_TYPE_ID
																	,P_DOCUMENT_SERIES => l$.DOCUMENT_SERIES
																	,P_DOCUMENT_NUMBER => l$.DOCUMENT_NUMBER
																	,P_DOCUMENT_DATE => l$.DOCUMENT_DATE
																	,P_DOCUMENT_ISSUE => l$.DOCUMENT_ISSUE
																	,P_DOCUMENT_SUBDIVISION_CODE => l$.DOCUMENT_SUBDIVISION_CODE
																	,P_EMAIL => l$.EMAIL
																	,P_PHONE_COUNTRY_CODE1 => l$.PHONE_COUNTRY_CODE1
																	,P_PHONE_REGION_CODE1 => l$.PHONE_REGION_CODE1
																	,P_PHONE_NUMBER1 => l$.PHONE_NUMBER1
																	,P_PHONE_COUNTRY_CODE2 => l$.PHONE_COUNTRY_CODE2
																	,P_PHONE_REGION_CODE2 => l$.PHONE_REGION_CODE2
																	,P_PHONE_NUMBER2 => l$.PHONE_NUMBER2
																	,P_FIRST_NAME_BIRTH => l$.FIRST_NAME_BIRTH
																	,P_LAST_NAME_BIRTH => l$.LAST_NAME_BIRTH
																	,P_SECOND_NAME_BIRTH => l$.SECOND_NAME_BIRTH
																	,P_ADR_BIRTH =>  l$.ADR_BIRTH
																	,P_ID_ADR_REG => l$.ID_ADR_REG
																	,P_ID_ADR_KORR => l$.ID_ADR_KORR
																	,P_JOB_COUNTRY => l$.JOB_COUNTRY
																	,P_JOB_COMPANY => l$.JOB_COMPANY
																	,P_JOB_POSITION => l$.JOB_POSITION
																	,P_ID_DEP => P_ID_DEP);

		END LOOP;

		UPDATE T_CNTR_CONTRACTS c
			SET c.id_dep = P_ID_DEP
			WHERE c.created_by_id = P_ID_USER;

		UPDATE t_pck_packs p
			SET p.id_dep = P_ID_DEP
			WHERE p.created_by_id = P_ID_USER;

	END P_MOVE_CLI_CNTR_PCK_TO_DEP;*/

END;
/

