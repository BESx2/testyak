i�?create or replace package lawmain.PKG_FOLDERS is

  -- Author  : EUGEN
  -- Created : 07.12.2016 13:04:53
  -- Purpose :  Пакет для работы с папками
  
	--Процедура добавляет дело в папку
  /*PROCEDURE P_ADD_ACC_CASE_TO_FOLDER(P_ID_CASE NUMBER,
															P_ID_FOLDER NUMBER);   */
  
		FUNCTION F_CREATE_FOLDER(P_NAME_FOLDER   VARCHAR2,
														 P_FOLDER_TYPE   VARCHAR2,
														 P_FOLDER_STATUS VARCHAR2 DEFAULT NULL,
														 P_DATE_PRET     DATE DEFAULT NULL,
														 P_ID_PARENT     NUMBER DEFAULT NULL,
														 P_ORDER_NUM     NUMBER DEFAULT NULL) RETURN NUMBER;												 
	
	--Функция возвращает ID типа папки по коду
	FUNCTION F_GET_FOLDER_TYPE_ID(P_CODE VARCHAR2) RETURN NUMBER;
	
	--Функция возвращает ID статуса папки по коду
	FUNCTION F_GET_FOLDER_STATUS(P_CODE VARCHAR2) RETURN NUMBER;      
	
  --Процедура создаёт событие, по папке
	PROCEDURE P_CREATE_FOLDER_EVENT(P_ID_FOLDER NUMBER,
																	P_CODE VARCHAR2);

  --Процедура удаляет дело юр. лица на этапе создания реестра УРВИВ	
	PROCEDURE P_DELETE_CLI_CASE(P_ID_CASE NUMBER);

  --Процедура удаляет дело физ лица на этапе создания реестра УРВИВ	
	--PROCEDURE P_DELETE_ACC_CASE(P_ID_CASE NUMBER);
	
	--Процедура меняет статус папки
	PROCEDURE P_CHANGE_FOLDER_STATUS(P_ID_FOLDER NUMBER,
																	 P_STATUS VARCHAR2); 

	--Процедура добавляет дело в папку																 
	PROCEDURE P_ADD_CLI_CASE_TO_FOLDER(P_ID_CASE NUMBER,
																	 	 P_ID_FOLDER NUMBER);  
																		 
  PROCEDURE P_DELETE_FOLDER(P_ID_FOLDER NUMBER);         
	
	-----ДОБАВЛЯЕТ ПРЕТЕНЗИИ К ДЕЛАМ
	PROCEDURE P_START_JOB_PRETENSION(P_ID_FOLDER NUMBER);	
	
 PROCEDURE P_FILL_REGISTRY(P_ID_SUPER NUMBER
                          ,P_FOLDER_TYPE VARCHAR2
                          ,P_SUB_NAMES VARCHAR2
                          ,P_MAX_CASES NUMBER DEFAULT 35);
	
	/*--Процедура удаляет дело ЮЛ из папки																	 
 	PROCEDURE P_EXCLUDE_CLI_CASE_FROM_FOLDER(P_ID_FOLDER NUMBER,
																					 P_ID_CASE   NUMBER);		

	--Процедура удаляет дело ФЛ из папки 																				 
	PROCEDURE P_EXCLUDE_ACC_CASE_FROM_FOLDER(P_ID_FOLDER NUMBER,
																					 P_ID_CASE   NUMBER);			

	
	-----НАПОЛНЕНИЕ РЕЕСТРА УРВИВ С ЛОГИКОЙ ПОДПАПОК------------
	PROCEDURE P_FILL_REGISTRY(P_ID_SUPER NUMBER									--id суперпапки
													 ,P_CASES 	 wwv_flow_global.vc_arr2--список дел
													 ,P_FOLDER_TYPE VARCHAR2            --тип папки
													 ,P_SUB_NAMES VARCHAR2);						--Наименование подпапок
	
		
														 																														 																																	
	--Расформирование реестра претензии	 
	PROCEDURE P_DISBAND_PRETENS_FOLDER(P_ID_FOLDER NUMBER); 
	
	--Процедура добавляет комментарий по папке
	PROCEDURE P_ADD_FOLDER_COMMENT(P_ID_FOLDER NUMBER, P_TEXT VARCHAR2, P_TYPE VARCHAR2);
  
	--процедура меняет тип задолженности в папке.	
	PROCEDURE P_CHANGE_FOLDER_DEBT_KIND(P_ID_FOLDER NUMBER,P_ID_CASE NUMBER,P_DEBT_KIND VARCHAR2);		
	
	--ПРОЦЕДУРА ОТПРАВЛЯЕТ ДЕЛО В ДОСУДЕБНЫЙ ОТДЕЛ
	PROCEDURE P_SEND_FOLDER_TO_ECV(P_ID_FOLDER NUMBER);		
	
	--ПРОЦЕДУРА ФИКСИРУЕТ СУММЫ ДЛЯ ПРЕТЕНЗИЙ ПО РЕЕСТРУ
	PROCEDURE P_START_PRETEN_WORK(P_ID_FOLDER NUMBER);	*/									 
end PKG_FOLDERS;
/

create or replace package body lawmain.PKG_FOLDERS is
	
	FUNCTION F_GET_FOLDER_TYPE_ID(P_CODE VARCHAR2) RETURN NUMBER
		 IS                   
		 l_ret NUMBER;
	BEGIN
		 SELECT t.id_type INTO l_ret FROM T_FOLDER_TYPES t WHERE t.type_code = P_CODE;
		 RETURN l_ret;
		 EXCEPTION
			  WHEN NO_DATA_FOUND THEN
					 RAISE_APPLICATION_ERROR(-20200,'Не правильно указан код типа папки');
	END F_GET_FOLDER_TYPE_ID;
	
	-----------------------------------------------------------------------------------
	
	FUNCTION F_GET_FOLDER_STATUS(P_CODE VARCHAR2) RETURN NUMBER
		 IS                   
		 l_ret NUMBER;
	BEGIN
		 SELECT t.id_status INTO l_ret FROM T_FOLDER_STATUS t WHERE t.code_status = P_CODE;
		 RETURN l_ret;
		 EXCEPTION
			  WHEN NO_DATA_FOUND THEN
					 RAISE_APPLICATION_ERROR(-20200,'Не правильно указан код статуса папки');
	END F_GET_FOLDER_STATUS;

	-----------------------------------------------------------------------------------
	
	FUNCTION F_CREATE_FOLDER(P_NAME_FOLDER   VARCHAR2,
													 P_FOLDER_TYPE   VARCHAR2,
													 P_FOLDER_STATUS VARCHAR2 DEFAULT NULL,
													 P_DATE_PRET     DATE DEFAULT NULL,
													 P_ID_PARENT     NUMBER DEFAULT NULL,
													 P_ORDER_NUM     NUMBER DEFAULT NULL) RETURN NUMBER
	IS                       
	L_RET NUMBER;
	l_id_type NUMBER;
	l_id_status NUMBER;
	BEGIN                                 
 	   l_id_status := F_GET_FOLDER_STATUS(P_CODE => NVL(P_FOLDER_STATUS,'FILLING'));
		 l_id_type := F_GET_FOLDER_TYPE_ID(P_CODE => P_FOLDER_TYPE);																						 
	 
		 INSERT INTO T_FOLDERS(ID_FOLDER,FOLDER_NAME,ID_TYPE,DATE_CREATED,CREATED_BY
                         ,ID_STATUS,DATE_PRET,ID_PARENT,ORDER_NUM,FLG_FILL)
		 VALUES (SEQ_ACC_FOLDERS.NEXTVAL,P_NAME_FOLDER, l_id_type, SYSDATE,f$_usr_id
		        ,l_id_status,P_DATE_PRET,P_ID_PARENT,P_ORDER_NUM,NVL2(P_ID_PARENT,'N',NULL))
		 RETURN ID_FOLDER INTO L_RET;
		 
  	 P_CREATE_FOLDER_EVENT(L_RET,'CREATED');		   
		 
		 RETURN L_RET;
	END F_CREATE_FOLDER;
	
	-----------------------------------------------------------------------------------
  
	PROCEDURE P_ADD_CLI_CASE_TO_FOLDER(P_ID_CASE NUMBER,
																		 P_ID_FOLDER NUMBER)
	IS
			 l_toll NUMBER; 		 
	BEGIN 		
	  INSERT INTO T_FOLDER_CLI_CASES(ID_FOLD_CASE,ID_FOLDER,ID_CASE,DATE_CREATED,CREATED_BY,Folder_Toll)
		VALUES (SEQ_FOLD_ACC_CASE.NEXTVAL,P_ID_FOLDER,P_ID_CASE,SYSDATE,f$_usr_id, l_toll);
	END;
	
	------------------------------------------------------------------------------------

 	PROCEDURE P_CREATE_FOLDER_EVENT(P_ID_FOLDER NUMBER,
																	P_CODE VARCHAR2)
	IS                     
		l_id NUMBER;
	BEGIN                                   
		 SELECT t.id_type INTO l_id FROM T_FOLDER_EVENT_TYPES t WHERE t.code = P_CODE;
		 INSERT INTO T_FOLDER_EVENTS(ID_FLD_EV, ID_FOLDER,ID_EV_TYPE, DATE_CREATED,CREATED_BY)
		 VALUES(SEQ_MAIN.NEXTVAL, P_ID_FOLDER,L_ID,SYSDATE,f$_usr_id);
		 		 
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
				RAISE_APPLICATION_ERROR(-20200,'Неверно указан код события');
	END;	

	PROCEDURE P_DELETE_CLI_CASE(P_ID_CASE NUMBER)
		 IS
	BEGIN
		FOR I IN (SELECT F.ID_LINK FROM T_LINK_FILE_CLI_WORK F WHERE F.ID_WORK = P_ID_CASE)
		 LOOP
				PKG_FILES.P_DELETE_CLI_CASE_DOC(P_ID_LINK => I.ID_LINK);
		 END LOOP;  																																			
		 DELETE FROM T_CLI_EVENTS ev WHERE ev.ID_WORK = P_ID_CASE;
		 DELETE FROM T_CLI_DEBTS ccd WHERE ccd.id_WORK = P_ID_CASE;
		 DELETE FROM T_FOLDER_CLI_CASES fc WHERE fc.id_case = P_ID_CASE;
		 DELETE FROM T_CLI_CASES s WHERE s.id_case = P_ID_CASE;
		 DELETE FROM T_CLI_DEBT_WORK W WHERE W.ID_WORK = P_ID_CASE;
	END;	  
	
  PROCEDURE P_DELETE_FOLDER(P_ID_FOLDER NUMBER)
	  IS
	BEGIN
		 DELETE FROM T_FOLDER_EVENTS EV WHERE EV.ID_FOLDER = P_ID_FOLDER;
		 DELETE FROM T_FOLDERS S WHERE S.ID_FOLDER = P_ID_FOLDER;  
	END;		 
           /*
	-----------------------------------------------------------------------------------
		
	PROCEDURE P_ADD_ACC_CASE_TO_FOLDER(P_ID_CASE NUMBER,
															P_ID_FOLDER NUMBER)
		 IS
		 l_toll NUMBER; 		 
	BEGIN 
			SELECT ac.gov_toll INTO l_toll FROM t_acc_cases ac WHERE ac.id_case = P_ID_CASE; 	
			INSERT INTO T_FOLDER_ACC_CASES(ID_FOLD_CASE,ID_FOLDER,ID_CASE,DATE_CREATED,CREATED_BY,FOLDER_TOLL)
			VALUES (SEQ_FOLD_ACC_CASE.NEXTVAL,P_ID_FOLDER,P_ID_CASE,SYSDATE,f$_usr_id,l_toll);
			
			 --удаляем метку слияния если была
			 UPDATE T_ACC_CASES s 
			 SET s.is_merged = NULL
			 WHERE s.id_case = P_ID_CASE; 
			  								 
	END P_ADD_ACC_CASE_TO_FOLDER;    

	-----------------------------------------------------------------------------------
		
	PROCEDURE P_DELETE_ACC_CASE(P_ID_CASE NUMBER)
		 IS
	BEGIN   
		 DELETE FROM T_ACC_EVENTS ev WHERE ev.id_acc_case = P_ID_CASE;
		 DELETE FROM T_ACC_CASE_DEBTS acd WHERE acd.id_case = P_ID_CASE;
		 DELETE FROM T_FOLDER_ACC_CASES fa WHERE fa.id_case = P_ID_CASE;
		 DELETE FROM T_ACC_CASES s WHERE s.id_case = P_ID_CASE;
	END;	     

	-----------------------------------------------------------------------------------
	*/	
	PROCEDURE P_CHANGE_FOLDER_STATUS(P_ID_FOLDER NUMBER,
																	 P_STATUS VARCHAR2)
	IS        
	l_id NUMBER;
	l_folder_type VARCHAR2(50);
	BEGIN               
		 SELECT s.id_status INTO l_id FROM T_FOLDER_STATUS s WHERE s.code_status = P_STATUS;		 
		 
	   UPDATE T_FOLDERS s SET s.ID_STATUS = l_id WHERE s.id_folder = P_ID_FOLDER;		 
		 --ОБНОВЛЯЕМ СТАТУС В ДОЧЕРНИХ ПАПКАХ ЕСЛИ ЕСТЬ
		 FOR F IN ( SELECT F.ID_FOLDER FROM T_FOLDERS F WHERE F.ID_PARENT = P_ID_FOLDER )
		 LOOP
				P_CHANGE_FOLDER_STATUS(P_ID_FOLDER => F.ID_FOLDER, P_STATUS => P_STATUS);
		 END LOOP;		 
	EXCEPTION 
		 WHEN NO_DATA_FOUND THEN
			  RAISE_APPLICATION_ERROR(-20200,'Указан неверный статус папки');
	END;  
	
		--прикрепление претензий ко делам папки
	PROCEDURE P_START_JOB_PRETENSION(P_ID_FOLDER NUMBER)
		IS
	BEGIN
		 DBMS_SCHEDULER.create_job(job_name => 'FORM_PRETEN_'||P_ID_FOLDER
		 													,job_type => 'PLSQL_BLOCK'
															,start_date => SYSTIMESTAMP
															,enabled =>  TRUE
															,auto_drop => TRUE
															,job_action => 
		'DECLARE 
		  L_ID_SYS NUMBER := PKG_USERS.F_GET_USER_ID_BY_USERNAME(''SYSTEM'');
		 BEGIN     
			 FOR J IN (SELECT LS.ID_FILE, LS.ID_LINK
								FROM T_FOLDER_CLI_CASES S, T_LINK_FILE_CLI_WORK LS, T_FILES F
								WHERE S.ID_FOLDER = '||P_ID_FOLDER||'
								AND   S.ID_CASE = LS.ID_WORK
								AND   LS.ID_FILE = F.ID_FILE
								AND   F.TYPE_CODE = ''PRETENSION''
								AND   F.CREATED_BY = L_ID_SYS)
			 LOOP
			 	 DELETE FROM T_LINK_FILE_CLI_WORK C WHERE C.ID_LINK = J.ID_LINK;
				 DELETE FROM T_FILES F WHERE F.ID_FILE = J.ID_FILE;
			 END LOOP;					                                 
			 
		   FOR I IN (SELECT FC.ID_CASE 
				         FROM  T_FOLDER_CLI_CASES FC
				 		 		 WHERE FC.ID_FOLDER = '||P_ID_FOLDER||')
			 LOOP  
				  BEGIN			                                                                                                           						 						 
					   PKG_PRETEN.P_ADD_PRETENSION(P_ID_WORK => I.ID_CASE
						                            ,P_USER    => L_ID_SYS
																				,P_CREATOR => '||F$_USR_ID||');
						 COMMIT;
					EXCEPTION
						WHEN OTHERS THEN
							PKG_LOG.P$LOG(p_PROG_UNIT => ''JOB_PRETENS'',p_MESS => SQLERRM,P_C1 =>I.ID_CASE);
					END;
			 END LOOP;
		END;');			          
	END;
	
-------------------------------------------------------------------------------------------	
	
 PROCEDURE P_FILL_REGISTRY(P_ID_SUPER NUMBER
                          ,P_FOLDER_TYPE VARCHAR2
                          ,P_SUB_NAMES VARCHAR2
                          ,P_MAX_CASES NUMBER DEFAULT 35)
    IS                        
    L_CASE_CNT  NUMBER :=0;                                                                                      
    L_ID_SUB  NUMBER;
    L_ID_CASE NUMBER; 
    L_NUM_FLD NUMBER := 0;
  BEGIN                               
   
  --СОЗДАЁМ ДОЧЕРНЮЮ ПАПКУ
   L_ID_SUB :=   PKG_FOLDERS.F_CREATE_FOLDER(P_NAME_FOLDER => P_SUB_NAMES||' '||L_NUM_FLD
                                           ,P_FOLDER_TYPE => P_FOLDER_TYPE
                                           ,P_ID_PARENT =>  P_ID_SUPER
                                           ,P_ORDER_NUM =>  L_NUM_FLD);
                              
   --ЗАПОЛНЕНИЕ ПАПКИ  
   FOR I IN (SELECT S.ID_CASE FROM T_FOLDER_CLI_CASES S WHERE S.ID_FOLDER = P_ID_SUPER)
   LOOP                  
      IF L_CASE_CNT = P_MAX_CASES THEN
        UPDATE T_FOLDERS S SET S.FLG_FILL = 'Y' WHERE S.ID_FOLDER = L_ID_SUB;
        L_NUM_FLD := L_NUM_FLD + 1;
        L_ID_SUB := PKG_FOLDERS.F_CREATE_FOLDER(P_NAME_FOLDER => P_SUB_NAMES||' '||L_NUM_FLD
                                               ,P_FOLDER_TYPE => P_FOLDER_TYPE
                                               ,P_ID_PARENT =>  P_ID_SUPER
                                               ,P_ORDER_NUM =>  L_NUM_FLD);
        L_CASE_CNT := 0;
      END IF;        
        PKG_FOLDERS.P_ADD_CLI_CASE_TO_FOLDER(I.ID_CASE,L_ID_SUB);
        L_CASE_CNT := L_CASE_CNT + 1;
   END LOOP;    
   
   --УДАЛЯЕМ ДЕЛА ИЗ    
   DELETE FROM T_FOLDER_CLI_CASES S WHERE S.ID_FOLDER = P_ID_SUPER;
  END;
  
	
	
 /*
  ------------------------------------------------------------------------------------
		
	PROCEDURE P_EXCLUDE_CLI_CASE_FROM_FOLDER(P_ID_FOLDER NUMBER,
																					 P_ID_CASE   NUMBER)
	IS
	BEGIN
		DELETE FROM T_FOLDER_CLI_CASES	fcc
		WHERE(fcc.id_folder = P_ID_FOLDER OR fcc.id_folder IN (SELECT id_folder FROM t_folders WHERE id_parent=P_ID_FOLDER) )
		AND   fcc.id_case  = P_ID_CASE;
	END;			
	
	---------------------------------------------------------------------
	
	PROCEDURE P_EXCLUDE_ACC_CASE_FROM_FOLDER(P_ID_FOLDER NUMBER,
																					 P_ID_CASE   NUMBER)
	IS
	BEGIN
		DELETE FROM T_FOLDER_ACC_CASES	fcc
		WHERE fcc.id_folder = P_ID_FOLDER
		AND   fcc.id_case  = P_ID_CASE;
	END;								
							
	-----------------------------------------------------------------------------------
	

	
	-----НАПОЛНЕНИЕ РЕЕСТРА УРВИВ С ЛОГИКОЙ ПОДПАПОК------------
	PROCEDURE P_FILL_REGISTRY(P_ID_SUPER NUMBER
													 ,P_CASES 	 wwv_flow_global.vc_arr2
													 ,P_FOLDER_TYPE VARCHAR2
													 ,P_SUB_NAMES VARCHAR2)
		IS                        
		L_FOLDER_CNT 	  NUMBER;
		L_CASE_CNT 	  NUMBER;                                                                                          
		L_ID_SUB  NUMBER;
		L_ID_CASE NUMBER;
		L_FOLDER_MAX_SIZE NUMBER:=70; 
		L_NUM_FLD NUMBER := 1;
		ID_FOLDER_IS_WRONG EXCEPTION;
	BEGIN
		--ПРОВЕРЯЕМ СУЩЕСТВОВАНИЕ ПАПКИ
		SELECT COUNT(1) INTO L_FOLDER_CNT FROM T_FOLDERS S WHERE S.ID_FOLDER = P_ID_SUPER AND s.id_parent IS NULL;
		IF L_FOLDER_CNT = 0 THEN
			RAISE ID_FOLDER_IS_WRONG;
		END IF;
																																
    --НАХОДИМ ДОЧЕРНЮЮ ПАПКУ
	  SELECT COUNT(1) INTO L_FOLDER_CNT FROM T_FOLDERS S WHERE S.ID_PARENT = P_ID_SUPER;
		
		--ЕСЛИ НАШЛИ
		IF L_FOLDER_CNT > 0 THEN			                                                                          
			 --УСТАНАВЛИВАЕМ НОМЕР ПАПКИ
			 L_NUM_FLD := L_FOLDER_CNT;                                                                          
			 --НАХОДИМ ИД ПОСЛЕДНЕЙ ПАПКИ И ПРОВЕРЯЕМ ОТКРЫТА ЛИ ОНА
			 SELECT MAX(S.ID_FOLDER) INTO L_ID_SUB 
			 FROM T_FOLDERS S 
			 WHERE S.ID_PARENT = P_ID_SUPER 
			 AND S.FLG_FILL = 'N';
			 
			 --ЕСЛИ НЕТУ ТАКОЙ ПАПКИ, ТО СОЗДАЁМ НОВУЮ
			 IF L_ID_SUB IS NULL THEN                                                               
				  L_NUM_FLD := L_NUM_FLD+1;
					L_ID_SUB := PKG_FOLDERS.F_CREATE_FOLDER(P_NAME_FOLDER => P_SUB_NAMES||' '||L_NUM_FLD
																								 ,P_FOLDER_TYPE => P_FOLDER_TYPE
																								 ,P_ID_PARENT =>  P_ID_SUPER
																								 ,P_ORDER_NUM =>  L_NUM_FLD); 
			 END IF;					                        
			 
			 --КОЛИЧЕСТВО ДЕЛ В ПАПКЕ
			 SELECT COUNT(1) INTO L_CASE_CNT FROM T_FOLDER_CLI_CASES C WHERE C.ID_FOLDER = L_ID_SUB;				
		ELSE                       
       --ЕСЛИ НЕ НАШЛИ ДОЧЕРНИХ ПАПОК																																						 
			 L_ID_SUB := PKG_FOLDERS.F_CREATE_FOLDER(P_NAME_FOLDER => P_SUB_NAMES||' '||L_NUM_FLD
			 																				,P_FOLDER_TYPE => P_FOLDER_TYPE
																							,P_ID_PARENT =>  P_ID_SUPER
																							,P_ORDER_NUM =>  L_NUM_FLD);
       L_CASE_CNT:= 0;	                                            
		END IF;
 
	
	 --ЗАПОЛНЕНИЕ ПАПКИ	
	 FOR I IN 1..P_CASES.COUNT 
	 LOOP                  
			IF L_CASE_CNT = L_FOLDER_MAX_SIZE THEN
				UPDATE T_FOLDERS S SET S.FLG_FILL = 'Y' WHERE S.ID_FOLDER = L_ID_SUB;
				L_NUM_FLD := L_NUM_FLD + 1;
				L_ID_SUB := PKG_FOLDERS.F_CREATE_FOLDER(P_NAME_FOLDER => P_SUB_NAMES||' '||L_NUM_FLD
																							 ,P_FOLDER_TYPE => P_FOLDER_TYPE
																							 ,P_ID_PARENT =>  P_ID_SUPER
																							 ,P_ORDER_NUM =>  L_NUM_FLD);
				L_CASE_CNT := 0;
			END IF;                                                                  
				L_ID_CASE := PKG_CLI_CASES.F_CREATE_REG_CLI_CASE(P_CASES(i), 'VIV');                                                 
				PKG_FOLDERS.P_ADD_CLI_CASE_TO_FOLDER(L_ID_CASE,L_ID_SUB);
				L_CASE_CNT := L_CASE_CNT + 1;
	 END LOOP;	 
	 EXCEPTION
		WHEN ID_FOLDER_IS_WRONG THEN
			 RAISE_APPLICATION_ERROR(-20200,'Неверная ссылка на родительскую папку');		 
	END;
	

	
	--Расформирование реестра претензии	 
	PROCEDURE P_DISBAND_PRETENS_FOLDER(P_ID_FOLDER NUMBER)
		IS 
	BEGIN		
		--расформирование дел в подпапках реестра--
	  for i in(
			SELECT fcc.id_case 
			FROM t_folder_cli_cases fcc, t_folders f
			where F.id_folder = fcc.id_folder
			and F.ID_PARENT = P_ID_FOLDER
		) 
		loop
			P_DELETE_CLI_CASE(P_ID_CASE => I.ID_CASE);
		end loop;
		
		--удаление папки и подпапок
		DELETE FROM t_folders f WHERE f.id_folder = P_ID_FOLDER	OR f.id_parent = f.id_folder;		
	END;      
	
	
	PROCEDURE P_ADD_FOLDER_COMMENT(P_ID_FOLDER NUMBER, P_TEXT VARCHAR2, P_TYPE VARCHAR2)
		IS
	BEGIN
		 INSERT INTO T_FOLDER_COMMENTS(ID,ID_FOLDER,TEXT,DATE_CREATED,CREATED_BY, TYPE_COMMENT)
     VALUES(SEQ_FOLDER_COMMENT.NEXTVAL,P_ID_FOLDER,P_TEXT,SYSDATE,F$_USR_ID,P_TYPE);		 
	END;
	
	PROCEDURE P_CHANGE_FOLDER_DEBT_KIND(P_ID_FOLDER NUMBER,P_ID_CASE NUMBER,P_DEBT_KIND VARCHAR2)
		IS                                                                                             
		L_TEXT VARCHAR2(200);
		L_CASE VARCHAR2(50);
	BEGIN                
		SELECT S.CASE_NAME INTO L_CASE FROM T_CLI_CASES S WHERE S.ID_CASE = P_ID_CASE;
		L_TEXT := 'Пользователь '||INITCAP(PKG_USERS.F_GET_FIO_BY_ID(F$_USR_ID,'Y'));
		IF P_DEBT_KIND = 'T' THEN 
			L_TEXT := L_TEXT||' переместил дело '||L_CASE||' в проблемную задолженность';
		ELSIF P_DEBT_KIND = 'P' THEN
			L_TEXT := L_TEXT||' переместил дело '||L_CASE||' в просроченную задолженность';
		ELSIF P_DEBT_KIND = 'E' THEN	                                                                         
			L_TEXT := L_TEXT||' исключил дело '||L_CASE;
		END IF;	                                            
		
		P_ADD_FOLDER_COMMENT(P_ID_FOLDER,L_TEXT,'LOG');																					 
		UPDATE T_FOLDER_CLI_CASES S SET S.DEBT_KIND = P_DEBT_KIND WHERE S.ID_FOLDER = P_ID_FOLDER AND S.ID_CASE = P_ID_CASE;
	END;
  
	PROCEDURE P_SEND_FOLDER_TO_ECV(P_ID_FOLDER NUMBER)
		IS
	BEGIN
    FOR I IN (SELECT C.DEBT_KIND,C.ID_CASE
			        FROM T_FOLDER_CLI_CASES C 
							WHERE C.ID_FOLDER = P_ID_FOLDER)
			LOOP                                            
				IF I.DEBT_KIND = 'E' THEN
					DELETE FROM T_FOLDER_CLI_CASES C WHERE C.ID_CASE = I.ID_CASE AND C.ID_FOLDER = P_ID_FOLDER;
				ELSE
				  UPDATE T_CLI_CASES CC 
					SET    CC.DEP = 'URVIV',
								 CC.debt_urviv = CC.SUM_DEBT
					WHERE CC.ID_CASE = I.ID_CASE;			 
					
					UPDATE T_CLI_CASE_DEBTS D 
					SET D.DEPT = 'URVIV'
						 ,D.DEBT_PRETRIAL = (SELECT INV.DEBT FROM V_DEBT_INVOICES INV WHERE INV.id_invoice = D.ID_INVOICE)
						 ,D.DATE_PRETRIAL = SYSDATE         
						 ,D.KIND_DEBT = I.DEBT_KIND     
						 ,D.ID_RPZ = P_ID_FOLDER
					WHERE D.ID_CASE = I.ID_CASE;	 					
					
					PKG_CLI_CASES.P_CHANGE_STATUS(I.ID_CASE,'WORK');
					PKG_EVENTS.P_CREATE_CLI_EVENT(I.ID_CASE,'TO_PRE_TRIAL');
				END IF;	
			END LOOP;
	END;
*/
end PKG_FOLDERS;
/

