i�?create or replace package lawmain.PKG_KLADR is

  -- Author  : EUGEN
  -- Created : 10.04.2017 15:41:52
  -- Purpose : Пакет для работы с КЛАДР
  
	
	--Функция создаёт запись в таблице T_KLADR_ADDRESS
 	FUNCTION F_CREATE_ADDRESS(P_SC_REGION 		T_KLADR_ADDRESS.SCNAME_REGION%TYPE
													 ,P_REGION 				T_KLADR_ADDRESS.REGION%TYPE
													 ,P_SC_DISTRICT 	T_KLADR_ADDRESS.SCNAME_DISTRICT%TYPE
													 ,P_DISTRICT  		T_KLADR_ADDRESS.DISTRICT%TYPE
													 ,P_SC_TOWN   		T_KLADR_ADDRESS.SCNAME_TOWN%TYPE
													 ,P_TOWN      		T_KLADR_ADDRESS.TOWN%TYPE
													 ,P_SC_SETTLEMENT T_KLADR_ADDRESS.SCNAME_SETTLEMENT%TYPE
													 ,P_SETTLEMENT 		T_KLADR_ADDRESS.SETTLEMENT%TYPE
													 ,P_SC_STREET  		T_KLADR_ADDRESS.SCNAME_STREET%TYPE
													 ,P_STREET     		T_KLADR_ADDRESS.STREET%TYPE
													 ,P_HOUSE      		T_KLADR_ADDRESS.HOUSE%TYPE
													 ,P_KORP			 		T_KLADR_ADDRESS.KORP%TYPE	
													 ,P_POSTCODE	 		T_KLADR_ADDRESS.POSTCODE%TYPE													 
													 ,P_BULID    	 		T_KLADR_ADDRESS.BUILD%TYPE
													 ,P_FLAT		   		T_KLADR_ADDRESS.FLAT%TYPE
												 	 ,P_LITERA        T_KLADR_ADDRESS.LITERA%TYPE
													 ,P_LINK          VARCHAR2
													 ,P_CODE          VARCHAR2) RETURN NUMBER;
	
	--Функция возвращает адрес одной строкой
	FUNCTION F_RETURN_ADDRESS(P_ID NUMBER) RETURN VARCHAR2;												 

end PKG_KLADR;
/

create or replace package body lawmain.PKG_KLADR is

	FUNCTION F_CREATE_ADDRESS(P_SC_REGION 		T_KLADR_ADDRESS.SCNAME_REGION%TYPE
													 ,P_REGION 				T_KLADR_ADDRESS.REGION%TYPE
													 ,P_SC_DISTRICT 	T_KLADR_ADDRESS.SCNAME_DISTRICT%TYPE
													 ,P_DISTRICT  		T_KLADR_ADDRESS.DISTRICT%TYPE
													 ,P_SC_TOWN   		T_KLADR_ADDRESS.SCNAME_TOWN%TYPE
													 ,P_TOWN      		T_KLADR_ADDRESS.TOWN%TYPE
													 ,P_SC_SETTLEMENT T_KLADR_ADDRESS.SCNAME_SETTLEMENT%TYPE
													 ,P_SETTLEMENT 		T_KLADR_ADDRESS.SETTLEMENT%TYPE
													 ,P_SC_STREET  		T_KLADR_ADDRESS.SCNAME_STREET%TYPE
													 ,P_STREET     		T_KLADR_ADDRESS.STREET%TYPE
													 ,P_HOUSE      		T_KLADR_ADDRESS.HOUSE%TYPE
													 ,P_KORP			 		T_KLADR_ADDRESS.KORP%TYPE	
													 ,P_POSTCODE	 		T_KLADR_ADDRESS.POSTCODE%TYPE													 
													 ,P_BULID    	 		T_KLADR_ADDRESS.BUILD%TYPE
													 ,P_FLAT		   		T_KLADR_ADDRESS.FLAT%TYPE
													 ,P_LITERA        T_KLADR_ADDRESS.LITERA%TYPE
													 ,P_LINK          VARCHAR2
													 ,P_CODE          VARCHAR2) RETURN NUMBER
		IS                                                                              
		l_ret NUMBER;
	BEGIN     
		-- Тупая проверка, на то чтобы была хоть какая-то информация
		-- ибо ИТшники из КВ  в свлём 1с не умеют формировать нормальный xml
		IF  P_SC_REGION IS NULL
	  AND P_REGION 		IS NULL
		AND P_SC_DISTRICT 	IS NULL
		AND P_DISTRICT  IS NULL
		AND P_SC_TOWN   IS NULL
		AND P_TOWN      IS NULL
		AND P_SC_SETTLEMENT IS NULL
		AND P_SETTLEMENT 	IS NULL
		AND P_SC_STREET  	IS NULL
		AND P_STREET     	IS NULL
	  AND P_HOUSE      	IS NULL
		AND P_KORP			 	IS NULL	
		AND P_POSTCODE	 	IS NULL													 
		AND P_BULID    	 	IS NULL
		AND P_FLAT		   	IS NULL
		AND P_LITERA      IS NULL THEN
		 RETURN NULL;
		END IF; 
													 
		INSERT INTO T_KLADR_ADDRESS(ID,                           
                               SCNAME_REGION,
                               REGION,
                               SCNAME_DISTRICT,
                               DISTRICT,
                               SCNAME_TOWN,
                               TOWN,
                               SCNAME_SETTLEMENT,
                               SETTLEMENT,
                               SCNAME_STREET,
                               STREET,
                               HOUSE,
                               KORP,                               
                               POSTCODE,
															 BUILD,
                               FLAT,
															 LITERA,
															 CODE,
															 LINK) 
  VALUES(seq_kladr.nextval,P_SC_REGION,P_REGION,P_SC_DISTRICT,P_DISTRICT,
				P_SC_TOWN,P_TOWN,P_SC_SETTLEMENT,P_SETTLEMENT,P_SC_STREET,P_STREET,
				P_HOUSE,P_KORP,P_POSTCODE,P_BULID,P_FLAT,P_LITERA,P_CODE,P_LINK) RETURNING ID INTO L_RET;
	RETURN L_RET;																		
	END;     
	
	--Функция возвращает адрес одной строкой
	FUNCTION F_RETURN_ADDRESS(P_ID NUMBER) RETURN VARCHAR2
		IS
	 L_RET VARCHAR2(1000);
	BEGIN                    
		FOR i IN (
		      SELECT a.postcode, a.region,a.scname_region reg, 
									a.town,a.scname_town tow, 
									a.street,a.scname_street st,
									a.house, a.korp, a.build, a.flat  
					FROM t_kladr_address a
					WHERE a.id = P_ID)
		LOOP            
			 IF i.postcode IS NOT NULL THEN
				 L_RET := L_RET || i.postcode||', ';
			 END IF;  
			 IF i.region IS NOT NULL THEN
				 L_RET := L_RET || i.region||' ';
			 END IF;
			 IF i.reg IS NOT NULL THEN 
				 L_RET := L_RET || i.reg||', ';
			 END IF;
 			 IF i.town IS NOT NULL THEN 
				 L_RET := L_RET || i.town||' ';
			 END IF;
			 IF i.tow IS NOT NULL THEN 
				 L_RET := L_RET || i.tow||', ';
			 END IF;       
 			 IF i.street IS NOT NULL THEN
				 L_RET := L_RET || i.street||' ';
			 END IF;
			 IF i.st IS NOT NULL THEN
				 L_RET := L_RET || i.st||', ';
			 END IF;
			 IF i.house IS NOT NULL THEN
				 L_RET := L_RET || 'дом №'||i.house||', ';
			 END IF;                 
 			 IF i.korp IS NOT NULL THEN
 				 L_RET := L_RET || 'корп. '||i.korp||', ';
			 END IF;                                    
		 	 IF i.build IS NOT NULL THEN
 				 L_RET := L_RET || 'стр. '||i.build||', ';
			 END IF;
 		 	 IF i.flat IS NOT NULL THEN
 				 L_RET := L_RET || 'кв. '||i.flat||', ';
			 END IF;
		END LOOP;                                   
		L_RET := SUBSTR(L_RET,1,length(L_RET)-2);
		
		RETURN L_RET;
	  
		EXCEPTION 
			WHEN NO_DATA_FOUND THEN
				RETURN NULL;
	END;	
 
end PKG_KLADR;
/

