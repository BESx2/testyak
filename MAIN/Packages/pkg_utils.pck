i�?CREATE OR REPLACE PACKAGE LAWMAIN.PKG_UTILS
	IS

  FUNCTION f_check_spell_name_russia(pi_val		VARCHAR2) RETURN BOOLEAN;
	FUNCTION f_check_spell_name_english (pi_val VARCHAR2) RETURN BOOLEAN ;	
  FUNCTION f_check_spell_number(pi_val	VARCHAR2) RETURN BOOLEAN;
	
  FUNCTION f_check_spell_email(pi_val	VARCHAR2) RETURN BOOLEAN; 
	FUNCTION F_MAKE_TRANSLIT(P_STR VARCHAR2) RETURN VARCHAR2;
	FUNCTION F_GET_NAME_MONTHS (P_DATE DATE, P_CASE VARCHAR2 DEFAULT 'I') RETURN VARCHAR2;
	FUNCTION F_DATE_WITH_NAME_MONTHS (P_DATE DATE, P_CASE VARCHAR2 DEFAULT 'I') RETURN VARCHAR2;
	FUNCTION F_SAVE_ONLY_DIGITS(P_PHONE VARCHAR2) RETURN VARCHAR2;
 	FUNCTION F_CLEAR_PHONE(P_PHONE VARCHAR2) RETURN VARCHAR2;
	FUNCTION F_MAKE_MAIL_ID(P_IDX VARCHAR2) RETURN VARCHAR2;
	FUNCTION F_MAKE_PHONE(P_PHONE VARCHAR2) RETURN VARCHAR2;
	FUNCTION F_GET_MONEY_TO_STR(P_SUM NUMBER, P_TRIM VARCHAR2 DEFAULT 'Y') RETURN VARCHAR2;
	--функция возвращает требуемый элемент адреса по коду
	FUNCTION F_GET_ADDRESS_ELEMENT(P_ADDRESS VARCHAR2, P_ELEMENT VARCHAR2) RETURN VARCHAR2;
	
	--функции компрессии на основе LobCompressor.java
	  function F_CLOB_COMPRESS(p_clob clob) return blob;
    function F_CLOB_DECOMPRESS(p_blob blob) return clob;
    function F_BLOB_COMPRESS(p_blob blob) return blob;
    function F_BLOB_DECOMPRESS(p_blob blob) return blob; 

	--Проверка корректности ИНН: 0 - ИНН неверный  1 - ИНН корректный NULL - Ошибка                                                         
	FUNCTION F_INN_IS_VALID(p_inn VARCHAR2) RETURN NUMBER; 		

  FUNCTION F_CHECK_PASSPORT_DATE(P_PASS_DATE DATE, P_BIRTH_DATE DATE) RETURN VARCHAR2; 
	
	procedure HtpPrn(p_clob in out nocopy clob);
END;
/

CREATE OR REPLACE PACKAGE BODY LAWMAIN.PKG_UTILS
	IS
  /*
	  Проверка значения на присутствие только кириллицы, пробела и дефиса
		если да, return true
	*/

  FUNCTION f_check_spell_name_russia (pi_val VARCHAR2) RETURN BOOLEAN IS
    p_val VARCHAR2(2000);
		p_s VARCHAR2(10);
	BEGIN
		IF pi_val IS NULL THEN
			RETURN FALSE;
		END IF;

	  p_val := upper(TRIM(pi_val));

		FOR w IN 1..length (p_val)
		LOOP

			p_s := substr (p_val,w,1);
			IF p_s NOT IN ('А','Б','В','Г','Д',
			              'Е','Ё','Ж','З','И','Й',
										'К','Л','М','Н','О',
										'П','Р','С','Т','У',
										'Ф','Х','Ц','Ч','Ш',
										'Щ','Ъ','Ы','Ь','Э',
										'Ю','Я',' ','-')
			THEN

				RETURN FALSE;

		  end IF;

	  END LOOP;
		RETURN TRUE;
	END;

  FUNCTION f_check_spell_name_english (pi_val IN VARCHAR2) RETURN BOOLEAN 
		IS
    p_val VARCHAR2(2000);
		p_s VARCHAR2(10);
	BEGIN
		IF pi_val IS NULL THEN
			RETURN FALSE;
		END IF;


	  p_val := upper(TRIM(pi_val));

		FOR w IN 1..length (p_val)
		LOOP
			p_s := substr (p_val,w,1);
			IF p_s NOT IN ('A','B','C','D','E',
			              'F','G','H','I','J','K',
										'L','M','N','O','P',
										'Q','R','S','T','U',
										'V','W','X','Y','Z',
										' ','-','''','_')
			THEN
				RETURN FALSE;
		  end IF;
	  END LOOP;


		RETURN TRUE;
	END;


  FUNCTION f_check_spell_number (pi_val VARCHAR2) RETURN BOOLEAN IS
    p_val VARCHAR2(2000);
		p_s VARCHAR2(10);
	BEGIN

		IF pi_val IS NULL THEN
			RETURN FALSE;
		END IF;

	  p_val := upper(TRIM(pi_val));

		FOR w IN 1..length (p_val)
		LOOP

			p_s := substr (p_val,w,1);
			IF p_s NOT IN ('0','1','2','3','4','5','6','7','8','9')
			THEN
				RETURN FALSE;
		  end IF;
	  END LOOP;

		RETURN TRUE;
	END;


  /*
	  Проверка email на наличие кириллицы, пробелов
		если да, то return false, иначе true
	*/
  FUNCTION f_check_spell_email (pi_val VARCHAR2) RETURN BOOLEAN IS
    p_val VARCHAR2(2000);
		p_s VARCHAR2(2);
	BEGIN

		IF pi_val IS NULL THEN
			RETURN FALSE;
		END IF;

		
		if instr(pi_val,'@') = 0
		then 
			return false;
		else
			if instr(pi_val,'.',instr(pi_val,'@')) = 0
			then 
				return false;
			end if;
		end if;


	  p_val := upper(TRIM(pi_val));
    
		
		FOR w IN 1..length (p_val)
		LOOP      

			p_s := substr (p_val,w,1);
			IF p_s IN ('А','Б','В','Г','Д',
			              'Е','Ё','Ж','З','И','Й',
										'К','Л','М','Н','О',
										'П','Р','С','Т','У',
										'Ф','Х','Ц','Ч','Ш',
										'Щ','Ъ','Ы','Ь','Э',
										'Ю','Я',' ')
			THEN
				RETURN FALSE;
		  end IF;
	  END LOOP;

		RETURN TRUE;

	END f_check_spell_email;
	
	FUNCTION F_MAKE_TRANSLIT(P_STR VARCHAR2) RETURN VARCHAR2
	IS
	l_tran_str VARCHAR2(4000) := P_STR;
	BEGIN
	  l_tran_str := translate(upper(l_tran_str),'АБВГДЕЗИЙКЛМНОПРСТУФЬЫЪЭ','ABVGDEZIJKLMNOPRSTUF''Y''E');
    l_tran_str:= replace(l_tran_str, 'Ж', 'ZH');
    l_tran_str:= replace(l_tran_str, 'Х', 'KH');
    l_tran_str:= replace(l_tran_str, 'Ц', 'TS');
    l_tran_str:= replace(l_tran_str, 'Ч', 'CH');
    l_tran_str:= replace(l_tran_str, 'Ш', 'SH');
    l_tran_str:= replace(l_tran_str, 'Щ', 'SH');
    l_tran_str:= replace(l_tran_str, 'Ю', 'YU');
    l_tran_str:= replace(l_tran_str, 'Я', 'YA');
		RETURN l_tran_str;
	END F_MAKE_TRANSLIT;
	
	FUNCTION F_GET_NAME_MONTHS (P_DATE DATE, P_CASE VARCHAR2 DEFAULT 'I') RETURN VARCHAR2
	IS 
		l_ret VARCHAR2(200);
	BEGIN 
	
	IF P_CASE = 'I' THEN
		IF EXTRACT(MONTH FROM P_DATE) = 1 THEN l_ret := 'ЯНВАРЬ';
		ELSIF EXTRACT(MONTH FROM P_DATE) = 2 THEN l_ret := 'ФЕВРАЛЬ';
		ELSIF EXTRACT(MONTH FROM P_DATE) = 3 THEN l_ret := 'МАРТ';
		ELSIF EXTRACT(MONTH FROM P_DATE) = 4 THEN l_ret := 'АПРЕЛЬ';
		ELSIF EXTRACT(MONTH FROM P_DATE) = 5 THEN l_ret := 'МАЙ';
		ELSIF EXTRACT(MONTH FROM P_DATE) = 6 THEN l_ret := 'ИЮНЬ';
		ELSIF EXTRACT(MONTH FROM P_DATE) = 7 THEN l_ret := 'ИЮЛЬ';
		ELSIF EXTRACT(MONTH FROM P_DATE) = 8 THEN l_ret := 'АВГУСТ';
		ELSIF EXTRACT(MONTH FROM P_DATE) = 9 THEN l_ret := 'СЕНТЯБРЬ';
		ELSIF EXTRACT(MONTH FROM P_DATE) = 10 THEN l_ret := 'ОКТЯБРЬ';
		ELSIF EXTRACT(MONTH FROM P_DATE) = 11 THEN l_ret := 'НОЯБРЬ';
		ELSIF EXTRACT(MONTH FROM P_DATE) = 12 THEN l_ret := 'ДЕКАБРЬ';
		ELSE NULL;
		END IF;
	ELSIF P_CASE = 'R' THEN 
		IF EXTRACT(MONTH FROM P_DATE) = 1 THEN l_ret := 'ЯНВАРЯ';
		ELSIF EXTRACT(MONTH FROM P_DATE) = 2 THEN l_ret := 'ФЕВРАЛЯ';
		ELSIF EXTRACT(MONTH FROM P_DATE) = 3 THEN l_ret := 'МАРТА';
		ELSIF EXTRACT(MONTH FROM P_DATE) = 4 THEN l_ret := 'АПРЕЛЯ';
		ELSIF EXTRACT(MONTH FROM P_DATE) = 5 THEN l_ret := 'МАЯ';
		ELSIF EXTRACT(MONTH FROM P_DATE) = 6 THEN l_ret := 'ИЮНЯ';
		ELSIF EXTRACT(MONTH FROM P_DATE) = 7 THEN l_ret := 'ИЮЛЯ';
		ELSIF EXTRACT(MONTH FROM P_DATE) = 8 THEN l_ret := 'АВГУСТА';
		ELSIF EXTRACT(MONTH FROM P_DATE) = 9 THEN l_ret := 'СЕНТЯБРЯ';
		ELSIF EXTRACT(MONTH FROM P_DATE) = 10 THEN l_ret := 'ОКТЯБРЯ';
		ELSIF EXTRACT(MONTH FROM P_DATE) = 11 THEN l_ret := 'НОЯБРЯ';
		ELSIF EXTRACT(MONTH FROM P_DATE) = 12 THEN l_ret := 'ДЕКАБРЯ';
		ELSE NULL;
		END IF;
	ELSIF P_CASE = 'P' THEN 
		IF EXTRACT(MONTH FROM P_DATE) = 1 THEN l_ret := 'ЯНВАРЕ';
		ELSIF EXTRACT(MONTH FROM P_DATE) = 2 THEN l_ret := 'ФЕВРАЛЕ';
		ELSIF EXTRACT(MONTH FROM P_DATE) = 3 THEN l_ret := 'МАРТЕ';
		ELSIF EXTRACT(MONTH FROM P_DATE) = 4 THEN l_ret := 'АПРЕЛЕ';
		ELSIF EXTRACT(MONTH FROM P_DATE) = 5 THEN l_ret := 'МАЕ';
		ELSIF EXTRACT(MONTH FROM P_DATE) = 6 THEN l_ret := 'ИЮНЕ';
		ELSIF EXTRACT(MONTH FROM P_DATE) = 7 THEN l_ret := 'ИЮЛЕ';
		ELSIF EXTRACT(MONTH FROM P_DATE) = 8 THEN l_ret := 'АВГУСТЕ';
		ELSIF EXTRACT(MONTH FROM P_DATE) = 9 THEN l_ret := 'СЕНТЯБРЕ';
		ELSIF EXTRACT(MONTH FROM P_DATE) = 10 THEN l_ret := 'ОКТЯБРЕ';
		ELSIF EXTRACT(MONTH FROM P_DATE) = 11 THEN l_ret := 'НОЯБРЕ';
		ELSIF EXTRACT(MONTH FROM P_DATE) = 12 THEN l_ret := 'ДЕКАБРЕ';
		ELSE NULL;
		END IF;	
	ELSE NULL;
	END if;
	
	RETURN l_ret;

	END; 
	
	FUNCTION F_DATE_WITH_NAME_MONTHS (P_DATE DATE, P_CASE VARCHAR2 DEFAULT 'I') RETURN VARCHAR2
	IS 
		l_ret VARCHAR2(200);
	BEGIN 
	l_ret:= TO_CHAR(P_DATE,'DD')||' '||LOWER(PKG_UTILS.F_GET_NAME_MONTHS(P_DATE=>P_DATE, P_CASE=>P_CASE))||' '||TO_CHAR(P_DATE,'YYYY');
	RETURN l_ret;
	END;
	
	FUNCTION F_GET_MONEY_TO_STR(P_SUM NUMBER, P_TRIM VARCHAR2 DEFAULT 'Y') RETURN VARCHAR2
	IS 
		l_str VARCHAR2(200);
	
	BEGIN
  IF P_SUM IS NULL THEN 
		RETURN '';     
	END IF;  
	
	  IF P_TRIM = 'Y' THEN
			 l_str := REPLACE(TO_CHAR(floor(P_SUM),'fm999G999G999G999G999G990','NLS_NUMERIC_CHARACTERS='', '''),'  ','')||' руб.';
			 IF MOD(P_SUM,1)!= 0. THEN                      
				 IF MOD(P_SUM,1)*100 < 10 THEN
					  l_str := l_str||' 0'||TO_CHAR(MOD(P_SUM,1)*100)||' коп.' ;
				 ELSE
  				  l_str := l_str||' '||TO_CHAR(MOD(P_SUM,1)*100)||' коп.' ;
				 END IF;				 
			 END IF;
			 
		ELSIF P_TRIM = 'N' THEN
			 l_str := REPLACE(TO_CHAR(floor(P_SUM),'fm999G999G999G999G999G990','NLS_NUMERIC_CHARACTERS='', '''),'  ','')||' рублей';
			 IF MOD(P_SUM,1)!= 0. THEN
				 l_str := l_str||' '||TO_CHAR(MOD(P_SUM,1)*100)||' копеек' ;
			 END IF;
		
		ELSE NULL;	 	 
		END IF;
		 				
	RETURN l_str;
	END;			
	
	FUNCTION F_CLEAR_PHONE(P_PHONE VARCHAR2) RETURN VARCHAR2
	IS
		l_ret VARCHAR2(200);
	BEGIN
		IF P_PHONE IS NULL THEN
		RETURN NULL;
		END IF;			 	
		l_ret := REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(P_PHONE,'(',''),')',''),' ',''),'-',''),'+','');
		RETURN l_ret;
		
	END;
	
	--оставляет в строке(содержащей номера телефонов) только числа и запятые
	FUNCTION F_SAVE_ONLY_DIGITS(P_PHONE VARCHAR2) RETURN VARCHAR2
	IS
		l_ret VARCHAR2(100);
	BEGIN
		IF P_PHONE IS NULL THEN
		RETURN NULL;
		END IF;			 	
		l_ret := REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(P_PHONE,'(',''),')',''),' ',''),'-',''),'+','');
		RETURN l_ret;
		
	END;    
	
	FUNCTION F_MAKE_PHONE(P_PHONE VARCHAR2) RETURN VARCHAR2
		 IS
		 l_ret VARCHAR2(30);
	BEGIN       
		 IF P_PHONE IS NULL THEN
			  RETURN NULL;
		 END IF;
		 l_ret := F_CLEAR_PHONE(P_PHONE);
		 l_ret :='+'||SUBSTR(l_ret,1,1)||'('||SUBSTR(l_ret,2,3)||')'||SUBSTR(l_ret,5,3)||'-'||SUBSTR(l_ret,8,2)||'-'||SUBSTR(l_ret,10);
		 RETURN l_ret;
	END;
	
	--функция проверяет, является ли строка числом
	FUNCTION F_IS_NUMBER(P_STR VARCHAR2) RETURN BOOLEAN
		 IS
	BEGIN       
		 IF translate(replace(replace(replace(P_STR,' ',''),',',''),'.',''),' 0123465789',' ') is null THEN
			RETURN TRUE;
		ELSE 
			RETURN FALSE;
		 END IF;            
	END;
	
	--функция возвращает требуемый элемент адреса по коду (INDEX, DISTRICT, STREET, BUILD, FLAT)
	FUNCTION F_GET_ADDRESS_ELEMENT(P_ADDRESS VARCHAR2,
		                             P_ELEMENT VARCHAR2) RETURN VARCHAR2
		 IS
		 l_element_for_returning VARCHAR2(1000):='';
		 l_addr_boofer VARCHAR2(4000);
     pos     NUMBER;
     l_word   varchar2(4000);
	BEGIN       
		 IF P_ELEMENT = 'INDEX' THEN
			FOR i IN 0..length(P_ADDRESS)-6 LOOP
				 IF F_IS_NUMBER( substr(P_ADDRESS, i, i+6) ) THEN
					 l_element_for_returning:= substr(P_ADDRESS, i, i+6); 
		       RETURN l_element_for_returning;				
				 END IF;
			END LOOP;	 
	   ELSIF P_ELEMENT = 'DISTRICT' THEN  
			l_addr_boofer := trim(REPLACE(replace(P_ADDRESS,',',''), ' '||' ', ' ')); 
			while  instr(l_addr_boofer, ' ')>0  
			LOOP  
				  pos := instr(l_addr_boofer, ' ');
          l_word := substr(l_addr_boofer, 1, pos-1);
						FOR i IN(SELECT d.DISTR_NAME
										 FROM t_district d)
						LOOP
							 IF Upper(l_word) LIKE '%'||i.DISTR_NAME||'%'  THEN
								 l_element_for_returning:= i.DISTR_NAME;
								 RETURN l_element_for_returning;
							 END IF;	  
						END LOOP;
				 l_addr_boofer := substr(l_addr_boofer, pos+1, LENGTH(l_addr_boofer));		 
			END LOOP;				 
		 ELSIF P_ELEMENT = 'CITY' THEN
			IF Upper(P_ADDRESS) LIKE '%'||'ВОЛГОГРАД'||'%'  THEN
							   l_element_for_returning:= 'ВОЛГОГРАД';
								 RETURN l_element_for_returning;
			END IF;	  			 
		 ELSIF P_ELEMENT = 'STREET' THEN
			 l_addr_boofer:=REPLACE(UPPER(P_ADDRESS), F_GET_ADDRESS_ELEMENT(P_ADDRESS, 'DISTRICT'),'');
			 l_addr_boofer:=REPLACE(UPPER(P_ADDRESS), F_GET_ADDRESS_ELEMENT(UPPER(P_ADDRESS), 'INDEX'),'');  	
			 l_addr_boofer:=REPLACE(l_addr_boofer,'ВОЛГОГРАД,','');

			 IF instr(l_addr_boofer, 'УЛ.')!=0 OR instr(l_addr_boofer, 'УЛ ')!=0 THEN
			 l_addr_boofer:= substr(l_addr_boofer, instr(l_addr_boofer, 'УЛ.'), instr(l_addr_boofer, 'Д.') - instr(l_addr_boofer, 'УЛ.')) ;
       ELSIF instr(l_addr_boofer, 'ПЕР.')!=0 OR instr(l_addr_boofer, 'ПЕР ')!=0  THEN
			 l_addr_boofer:= substr(l_addr_boofer, instr(l_addr_boofer, 'ПЕР.'), instr(l_addr_boofer, 'Д.') - instr(l_addr_boofer, 'УЛ.')) ;
       END IF;
			 l_element_for_returning:= REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(l_addr_boofer, ',', ''),  'Д.', ''),  '(', ''),  ')', ''),  '  ', '');			 
		  RETURN l_element_for_returning; 
		 ELSIF P_ELEMENT = 'BUILD' THEN
			 l_addr_boofer:=REPLACE(UPPER(P_ADDRESS), F_GET_ADDRESS_ELEMENT(P_ADDRESS, 'DISTRICT'),'');
			 l_addr_boofer:=REPLACE(UPPER(P_ADDRESS), F_GET_ADDRESS_ELEMENT(UPPER(P_ADDRESS), 'INDEX'),'');  	
			 l_addr_boofer:=REPLACE(l_addr_boofer,'ВОЛГОГРАД,','');
			 IF instr(l_addr_boofer, 'Д.')!=0 OR instr(l_addr_boofer, 'Д ')!=0 THEN
				 IF instr(l_addr_boofer, 'КВ')!=0  THEN
			      l_addr_boofer:= substr(l_addr_boofer, instr(l_addr_boofer, 'Д.'), instr(l_addr_boofer, 'КВ') - instr(l_addr_boofer, 'Д.')) ;
			   ELSE
					 l_addr_boofer:= substr(l_addr_boofer, instr(l_addr_boofer, 'Д.'), 10) ;
				 END IF; 
       ELSIF instr(l_addr_boofer, 'СТР.')!=0 OR instr(l_addr_boofer, 'СТР ')!=0  THEN
			 l_addr_boofer:= substr(l_addr_boofer, instr(l_addr_boofer, 'СТР.'), instr(l_addr_boofer, 'КВ') - instr(l_addr_boofer, 'СТР.')) ;
       END IF;
			 l_element_for_returning:= REPLACE(REPLACE(REPLACE(REPLACE(l_addr_boofer, ',', ''),  '(', ''),  ')', ''),  '  ', '');
		  RETURN l_element_for_returning;	 
		 ELSIF P_ELEMENT = 'FLAT' THEN
			 l_addr_boofer:=REPLACE(UPPER(P_ADDRESS), F_GET_ADDRESS_ELEMENT(P_ADDRESS, 'DISTRICT'),'');
			 l_addr_boofer:=REPLACE(UPPER(P_ADDRESS), F_GET_ADDRESS_ELEMENT(UPPER(P_ADDRESS), 'INDEX'),'');  	
			 l_addr_boofer:=REPLACE(l_addr_boofer,'ВОЛГОГРАД,','');
				 IF instr(l_addr_boofer, 'КВ.')!=0  THEN
			      l_addr_boofer:= substr(l_addr_boofer, instr(l_addr_boofer, 'КВ.'), 10) ;
			   ELSIF  instr(l_addr_boofer, 'КВ')!=0  THEN
					 l_addr_boofer:= substr(l_addr_boofer, instr(l_addr_boofer, 'КВ'), 10) ;
				 ELSE l_addr_boofer:=''; 
				 END IF;      
			 l_element_for_returning:= REPLACE(REPLACE(REPLACE(REPLACE(l_addr_boofer, ',', ''),  '(', ''),  ')', ''),  '  ', '');
		  RETURN l_element_for_returning;	 
		 END IF;
		RETURN l_element_for_returning;				 
	END;
		
	
	FUNCTION F_MAKE_MAIL_ID(P_IDX VARCHAR2) RETURN VARCHAR2
		 IS                         
		 L_EVEN NUMBER := 0;
		 L_ODD  NUMBER := 0;       
		 L_RET  NUMBER;
	BEGIN
		  FOR i IN 1..length(p_idx)
			LOOP  
				 --Складываются все числа в четной позиции
					  IF MOD(i,2) = 0 THEN
							 L_EVEN := L_EVEN + TO_NUMBER(SUBSTR(P_IDX,i,1));
				 --И все числа в нечетной
						ELSE
							 L_ODD := L_ODD + TO_NUMBER(SUBSTR(P_IDX,i,1));
						END IF;
		  END LOOP;                  
			-- Нечетные умножаются на 13                              
			L_ODD := L_ODD * 3;        
			-- Из 10 вычитается остаток от деления на 10 суммы четных и нечетных цифр
			L_RET := 10 - MOD((L_ODD + L_EVEN),10); 
			-- Если кратен 10
			IF MOD(L_RET,10) = 0 THEN
				 L_RET := 0;
			END IF;             
			RETURN P_IDX||L_RET;
	END; 

--Проверка корректности ИНН: 0 - ИНН неверный  1 - ИНН корректный NULL - Ошибка                                                         
FUNCTION F_INN_IS_VALID(p_inn VARCHAR2) RETURN NUMBER 
AS
  l_is_valid NUMBER:=0;
  l_length NUMBER:=0;
  l_num VARCHAR2(10):='0123456789';
  l_sum NUMBER:=0;
  l_next BOOLEAN:=TRUE;
  i NUMBER:=0;
  BEGIN
    BEGIN
      l_length:=LENGTH(p_inn);
      IF l_length=10 OR l_length=12 THEN
        
				WHILE (l_next AND i<=l_length) LOOP --Только цифры
          i:=i+1;
          IF INSTR(l_num,SUBSTR(p_inn,i,1),1)=0 THEN
            l_next:=FALSE;
          END IF;
        END LOOP;
				
        IF l_next THEN
          IF l_length=10 THEN
            l_sum:=MOD(
                        (
                          TO_NUMBER(SUBSTR(p_inn,1,1))*2+
                          TO_NUMBER(SUBSTR(p_inn,2,1))*4+
                          TO_NUMBER(SUBSTR(p_inn,3,1))*10+
                          TO_NUMBER(SUBSTR(p_inn,4,1))*3+
                          TO_NUMBER(SUBSTR(p_inn,5,1))*5+
                          TO_NUMBER(SUBSTR(p_inn,6,1))*9+ 
                          TO_NUMBER(SUBSTR(p_inn,7,1))*4+
                          TO_NUMBER(SUBSTR(p_inn,8,1))*6+
                          TO_NUMBER(SUBSTR(p_inn,9,1))*8
                        ),11
                      ); 
            IF l_sum=SUBSTR(p_inn,10,1) THEN --остаток от деления должен совпадать с последней цифрой
              l_is_valid:=1;
            END IF;
          ELSE
            l_sum:=MOD(
                        (
                          TO_NUMBER(SUBSTR(p_inn,1,1))*7+
                          TO_NUMBER(SUBSTR(p_inn,2,1))*2+
                          TO_NUMBER(SUBSTR(p_inn,3,1))*4+
                          TO_NUMBER(SUBSTR(p_inn,4,1))*10+
                          TO_NUMBER(SUBSTR(p_inn,5,1))*3+
                          TO_NUMBER(SUBSTR(p_inn,6,1))*5+
                          TO_NUMBER(SUBSTR(p_inn,7,1))*9+ 
                          TO_NUMBER(SUBSTR(p_inn,8,1))*4+
                          TO_NUMBER(SUBSTR(p_inn,9,1))*6+
                          TO_NUMBER(SUBSTR(p_inn,10,1))*8
                        ),11
                      );
            IF l_sum=SUBSTR(p_inn,11,1) THEN --остаток от деления должен совпадать с последней цифрой
              l_sum:=MOD(
                          (
                            TO_NUMBER(SUBSTR(p_inn,1,1))*3+
                            TO_NUMBER(SUBSTR(p_inn,2,1))*7+
                            TO_NUMBER(SUBSTR(p_inn,3,1))*2+
                            TO_NUMBER(SUBSTR(p_inn,4,1))*4+
                            TO_NUMBER(SUBSTR(p_inn,5,1))*10+
                            TO_NUMBER(SUBSTR(p_inn,6,1))*3+
                            TO_NUMBER(SUBSTR(p_inn,7,1))*5+
                            TO_NUMBER(SUBSTR(p_inn,8,1))*9+ 
                            TO_NUMBER(SUBSTR(p_inn,9,1))*4+
                            TO_NUMBER(SUBSTR(p_inn,10,1))*6+
                            TO_NUMBER(SUBSTR(p_inn,11,1))*8
                          ),11
                      );
              IF l_sum=SUBSTR(p_inn,12,1) THEN
                l_is_valid:=1;
              END IF;
            END IF;
          END IF;
        END IF;     
      END IF;
    EXCEPTION
      WHEN OTHERS THEN l_is_valid:=NULL;
    END;
    RETURN l_is_valid;
  END;
	
	-----------------Lob Compresion---------------------------
procedure clob_decompress(p_blob blob, p_clob clob)
as language java
name 'LobCompressor.decompress(oracle.sql.BLOB, oracle.sql.CLOB)';

procedure clob_compress(p_clob clob, p_blob blob)
as language java
name 'LobCompressor.compress(oracle.sql.CLOB, oracle.sql.BLOB)';

procedure blob_decompress(p_slob blob, p_blob blob)
as language java
name 'LobCompressor.decompress(oracle.sql.BLOB, oracle.sql.BLOB)';

procedure blob_compress(p_slob blob, p_blob blob)
as language java
name 'LobCompressor.compress(oracle.sql.BLOB, oracle.sql.BLOB)';

function F_CLOB_COMPRESS(p_clob clob) return blob is
l_blob blob;
begin
     if p_clob is null then
         return null;
     end if;
     dbms_lob.createtemporary(l_blob,true);
     clob_compress(p_clob,l_blob);
     return l_blob;
end;

function F_CLOB_DECOMPRESS(p_blob blob) return clob is
l_clob clob;
begin
     if p_blob is null then
         return null;
     end if;
     dbms_lob.createtemporary(l_clob,true);
     clob_decompress(p_blob,l_clob);
     return l_clob;
end;

function F_BLOB_COMPRESS(p_blob blob) return blob is
l_blob blob;
begin
     if p_blob is null then
         return null;
     end if;
     dbms_lob.createtemporary(l_blob,true);
     blob_compress(p_blob,l_blob);
     return l_blob;
end;

function F_BLOB_DECOMPRESS(p_blob blob) return blob is
l_blob blob;
begin
     if p_blob is null then
         return null;
     end if;
     dbms_lob.createtemporary(l_blob,true);
     blob_decompress(p_blob,l_blob);
     return l_blob;
end;
		

FUNCTION F_CHECK_PASSPORT_DATE(P_PASS_DATE DATE, P_BIRTH_DATE DATE) RETURN VARCHAR2
  IS
  L_AGE      NUMBER;
  L_PASS_AGE NUMBER;
BEGIN
  L_AGE := FLOOR(MONTHS_BETWEEN(SYSDATE,P_BIRTH_DATE));
  IF P_PASS_DATE > SYSDATE THEN
    RETURN 'Дата получения паспорта не может быть больше текущей';
  END IF;
  IF L_AGE BETWEEN 14 AND 19 THEN
    IF P_PASS_DATE < ADD_MONTHS(P_BIRTH_DATE,12*14) THEN
      RETURN 'Вы не могли получить паспорт раньше 14 лет';
    END IF;    
  ELSIF L_AGE BETWEEN 20 AND 44 THEN
    IF P_PASS_DATE < ADD_MONTHS(P_BIRTH_DATE,12*20) THEN
       RETURN 'Если вы не меняли паспорт после исполнения 20 лет - паспорт недействителен.';
    END IF;
  ELSIF L_AGE BETWEEN 45 AND 150 THEN
    IF P_PASS_DATE < ADD_MONTHS(P_BIRTH_DATE,12*20) THEN
       RETURN 'Если вы не меняли паспорт после исполнения 45 лет - паспорт недействителен.';
    END IF;  
  END IF;         
END;


procedure HtpPrn(p_clob in out nocopy clob) is
	 v_excel varchar2(32000);
	 v_clob clob := p_clob;
begin
		while length(v_clob) > 0 loop
			begin
				if length(v_clob) > 16000 then
					 v_excel:= substr(v_clob,1,16000);
					 htp.prn(v_excel);
					 v_clob:= substr(v_clob,length(v_excel)+1);
				else
					 v_excel := v_clob;
					 htp.prn(v_excel);
					 v_clob:='';
					 v_excel := '';
				end if;
			end;
		end loop;
	end;	

END;
/

