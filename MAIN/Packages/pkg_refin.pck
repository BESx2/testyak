i�?create or replace package lawmain.PKG_REFIN is

  -- Author  : EUGEN
  -- Created : 16.11.2016 14:29:06
  -- Purpose : пакет для работы с пени

	G$MAX_INT NUMBER := 2147483647; 
	
  TYPE R_CALC_PENY_PERIOD IS RECORD (DATE_START DATE, DATE_END DATE, PENY_RATE NUMBER, PENY_PRC NUMBER,SUM_PAY NUMBER, TYPE_CALC VARCHAR2(50));
  TYPE PERIOD_CALC IS TABLE OF R_CALC_PENY_PERIOD;
  TYPE R_CALC_PERIOD IS RECORD(DEBT_DATE DATE, SUM_DOC NUMBER, NUM_DOC VARCHAR2(100), ID_DOC NUMBER, CALC PERIOD_CALC);
	TYPE PENY_CALC IS TABLE OF R_CALC_PERIOD INDEX BY PLS_INTEGER;
	
	
	FUNCTION F_GET_DEBT_DATE(P_DATE DATE) RETURN DATE;	
	
	FUNCTION F_GET_PENY_FOR_DEBT(P_ID_CASE NUMBER
		                          ,P_GROUP VARCHAR2 DEFAULT NULL
															,P_DATE_TO DATE DEFAULT SYSDATE) RETURN NUMBER;
															
	--Процедура создаёт запись в справочнике рефинансирования
	PROCEDURE P_CREATE_REFIN(P_DATE_REF_BEG DATE,               --Дата начала действия ставки
		 											 P_DATE_REF_END DATE DEFAULT NULL,  --Дата конца действия
		 					             P_PRC_REF  NUMBER,                 --Процент рефин
													 P_REF_DOC  VARCHAR2 DEFAULT NULL); --Нормативный документ 
	
	--Процедура обновляет запись в справочнике рефинансирования
	PROCEDURE P_UPDATE_REF(P_ID_REF NUMBER,                     --ID ставки
												 P_DATE_REF_BEG DATE,                 --Дата начала действия ставки
												 P_DATE_REF_END DATE,                 --Дата конца действия
		 					           P_PRC_REF  NUMBER,                   --Процент рефин
												 P_REF_DOC  VARCHAR2); 								--Нормативный документ  
  
	--Процедура создаёт запись в таблице долей ставок рефинансирвания
	PROCEDURE P_CREATE_PENY_RATE(P_ID_REF NUMBER,               --ID ставки
															 P_DAYS_FROM NUMBER,            --Период дней с 
															 P_DAYS_TO   NUMBER,            --Период дней по
															 P_RATE      NUMBER,            --Доля
															 P_ABON_GROUP VARCHAR2);				--Группа абонентов							 

	--Процедура обновляет запись доли ставки рефинансирования															 
  PROCEDURE P_UPDATE_PENY_RATE(P_ID_RATE	 NUMBER,         		--ID доли
															 P_DAYS_FROM NUMBER,            --Период дней с 
															 P_DAYS_TO   NUMBER,            --Период дней по
															 P_RATE      NUMBER,            --Доля
															 P_ABON_GROUP VARCHAR2);			  --Группа абонентов												 

	--Функция возвращает сумму пени по делу на число
	FUNCTION F_GET_CLI_PENY(P_ID_CASE NUMBER,
													P_DATE DATE DEFAULT SYSDATE,
													P_GROUP   VARCHAR2 DEFAULT NULL) RETURN NUMBER;     
													
	--Процедура заполняет информацию о долях ставок в соответствии с предыдущей ставкой.
	PROCEDURE P_FILL_REFIN_ACCORDINGLY(P_ID_REFIN NUMBER);
														
	--Возвращает госпошлину по клиенту юр. лицу
	FUNCTION F_GET_CLI_GOV_TOLL(P_ID_CASE NUMBER
		                         ,P_TYPE    VARCHAR2) RETURN NUMBER;
		
	
	FUNCTION F_GET_CLI_GOV_FOR_SUM(P_SUM NUMBER) RETURN NUMBER;
	
													
end PKG_REFIN;
/

create or replace package body lawmain.PKG_REFIN is


	FUNCTION F_GET_DEBT_DATE(P_DATE DATE) RETURN DATE
    IS
  BEGIN
    IF PKG_CALENDAR.F_CHK_BUISNESS_DAY(P_DATE)  THEN
      RETURN P_DATE;
    ELSE            
      RETURN F_GET_DEBT_DATE(P_DATE+1);
		END IF;
	END;

  --Процедура создаёт запись в справочнике рефинансирования
	PROCEDURE P_CREATE_REFIN(P_DATE_REF_BEG DATE,
		 											 P_DATE_REF_END DATE DEFAULT NULL,
		 					             P_PRC_REF  NUMBER,     
													 P_REF_DOC  VARCHAR2 DEFAULT NULL)
	IS
	BEGIN
		INSERT INTO T_REFIN(ID_REFIN,DATE_REF_BEG,DATE_REF_END,PRC_REF,REF_DOC,DATE_CREATED,CREATED_BY)
		VALUES(seq_refin.nextval,P_DATE_REF_BEG,P_DATE_REF_END,P_PRC_REF,P_REF_DOC,SYSDATE,f$_usr_id);
	END;
  
	----------------------------------------------------------------------------------------
	
	--Процедура заполняет информацию о долях ставок в соответствии с предыдущей ставкой.
	PROCEDURE P_FILL_REFIN_ACCORDINGLY(P_ID_REFIN NUMBER)
		IS                                     
		l_last_ref NUMBER;
	BEGIN    
		 --ПОСЛЕДНЯЯ СТАВКА РЕФИНАНИРОВАНИЯ
		 SELECT MAX(r.id_refin) INTO l_last_ref FROM T_REFIN r WHERE r.id_refin != P_ID_REFIN; 
		 --УДАЛЯЕМ ВСЕ ДОЛИ ПО ТЕКУЩЕЙ СТАВКЕ
		 DELETE FROM t_peny_rate r WHERE r.id_refin = P_ID_REFIN;
		 --ВСТАВКА ДАННЫХ
		 INSERT INTO T_PENY_RATE(ID_RATE, DAYS_FROM, DAYS_TO, RATE, ABON_GROUP, ID_REFIN, CREATED_BY, DATE_CREATED)
		 SELECT SEQ_MAIN.NEXTVAL,R.DAYS_FROM,R.DAYS_TO,R.RATE,R.ABON_GROUP,P_ID_REFIN,F$_USR_ID,SYSDATE 
		 FROM T_PENY_RATE R WHERE R.ID_REFIN = L_LAST_REF; 
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			RAISE_APPLICATION_ERROR(-20200,'Не найдена информация о последней ставки рефинансирования');
	END;
	
	
  FUNCTION F_GET_PENY_FOR_DEBT(P_ID_CASE NUMBER
		                          ,P_GROUP VARCHAR2 DEFAULT NULL
															,P_DATE_TO DATE DEFAULT SYSDATE) RETURN NUMBER
	  IS
		L_RET     NUMBER := 0;		      
		L_PRC_REF NUMBER;
		L_RATE    NUMBER;       
		L_DEBT    NUMBER;                                 
		L_DATE_FROM DATE;           
		L_ID_REFIN NUMBER;    
		L_GROUP   VARCHAR2(50);
  BEGIN 
		
	  IF P_GROUP IS NULL THEN
			SELECT S.group_code INTO L_GROUP FROM V_CLI_CASES S WHERE S.ID_CASE = P_ID_CASE;
		ELSE
			L_GROUP := P_GROUP;
		END IF;
	
		SELECT L.LEVY_DEBT, L.LEVY_PENY_DATE 
		INTO L_DEBT, L_DATE_FROM 
		FROM T_CLI_LEVY L WHERE L.ID_CASE = P_ID_CASE;
	
		SELECT R.PRC_REF, R.ID_REFIN INTO L_PRC_REF, L_ID_REFIN FROM T_REFIN R 
		WHERE P_DATE_TO BETWEEN  R.DATE_REF_BEG AND NVL(R.DATE_REF_END,P_DATE_TO);
		
		SELECT PR.RATE INTO L_RATE FROM T_PENY_RATE PR 
		WHERE PR.ID_REFIN = L_ID_REFIN AND PR.ABON_GROUP = L_GROUP
		AND   PR.DAYS_TO IS NULL;
		
						
		FOR I IN (SELECT T.DATE_CHNG, T.PAYED, T.TYPE_CHNG, NVL(LEAD(T.DATE_CHNG) OVER(ORDER BY T.DATE_CHNG),P_DATE_TO) NEXT_DATE		               
			        FROM(SELECT L_DATE_FROM DATE_CHNG, 0 PAYED, 'START' TYPE_CHNG FROM DUAL
							      UNION ALL								
									 SELECT FDC.DATE_CON, SUM(-FDC.AMOUNT) PAYED, 'PAY' TYPE_CHNG			       
									 FROM T_CLI_DEBTS D,                                    
									      T_CLI_FINDOCS FD,
									 		  T_CLI_FINDOC_CONS FDC 
									 WHERE D.ID_WORK = P_ID_CASE
									 AND   FD.ID_DOC = D.ID_DOC
									 AND   (FDC.EXT_ID_TO = FD.EXT_ID OR FDC.EXT_ID_FROM = FD.EXT_ID)
									 AND    FDC.EXT_ID_FROM != FDC.EXT_ID_TO
									 AND    FDC.AMOUNT < 0                         
 									 AND    FDC.DOC_TYPE != 'Корректировка реализации' 
									 AND    FDC.DATE_CON BETWEEN L_DATE_FROM AND P_DATE_TO
									 GROUP BY FDC.DATE_CON) T        
							ORDER BY T.DATE_CHNG		 
							)
		LOOP			       			                                                                   
			L_DEBT := L_DEBT - I.PAYED;
			IF I.TYPE_CHNG != 'PAY' THEN				
			  L_RET := L_RET + L_DEBT/L_RATE * L_PRC_REF/100 * (I.NEXT_DATE - I.DATE_CHNG+1);    
			ELSE
				L_RET := L_RET + L_DEBT/L_RATE * L_PRC_REF/100 * (I.NEXT_DATE - I.DATE_CHNG);         
     			
			END IF;	 
     			
		END LOOP;																			
		RETURN L_RET;
	END;	
	
	--Процедура обновляет запись в справочнике рефинансирования
	PROCEDURE P_UPDATE_REF(P_ID_REF NUMBER,
												 P_DATE_REF_BEG DATE,
												 P_DATE_REF_END DATE,
		 					           P_PRC_REF  NUMBER,     
												 P_REF_DOC  VARCHAR2)  
	IS
	BEGIN
		UPDATE T_REFIN f SET f.date_ref_beg  =	P_DATE_REF_BEG,
												 f.date_ref_end  = P_DATE_REF_END,
												 f.prc_ref       = P_PRC_REF,
												 f.ref_doc       = P_REF_DOC,
												 f.modified_by   = f$_usr_id,
												 f.date_modified = SYSDATE
		WHERE f.id_refin = P_ID_REF;
	END;             
	
  --Процедура создаёт запись в таблице долей ставок рефинансирвания
	PROCEDURE P_CREATE_PENY_RATE(P_ID_REF NUMBER,
															 P_DAYS_FROM NUMBER,
															 P_DAYS_TO   NUMBER,
															 P_RATE      NUMBER,
															 P_ABON_GROUP VARCHAR2)
	IS
	BEGIN
		 INSERT INTO T_PENY_RATE(ID_RATE,DAYS_FROM,DAYS_TO,RATE,ABON_GROUP,ID_REFIN,CREATED_BY,DATE_CREATED)
		 VALUES (SEQ_MAIN.NEXTVAL,P_DAYS_FROM,P_DAYS_TO,P_RATE,P_ABON_GROUP,P_ID_REF,f$_usr_id,SYSDATE);	
	END P_CREATE_PENY_RATE;
	
	--Процедура обновляет запись доли ставки рефинансирования
	PROCEDURE P_UPDATE_PENY_RATE(P_ID_RATE	 NUMBER,
															 P_DAYS_FROM NUMBER,
															 P_DAYS_TO   NUMBER,
															 P_RATE      NUMBER,
															 P_ABON_GROUP VARCHAR2)
	IS
	BEGIN
 			UPDATE T_PENY_RATE r
			   SET r.days_from 		 = P_DAYS_FROM,
				     r.days_to   		 = P_DAYS_TO,
						 r.rate      		 = P_RATE,
						 r.abon_group	   = P_ABON_GROUP,
						 r.date_modified = SYSDATE,
						 r.modified_by 	 = f$_usr_id						 
			WHERE r.id_rate = P_ID_RATE;
	END P_UPDATE_PENY_RATE;    
	
	----------------------------------------------------------------------------------------------
	
	FUNCTION F_GET_PENY_FOREACH_DAY(P_DATE_ON DATE,
																	P_DATE_INV DATE,
																	P_ABON_GROUP VARCHAR2,
																	P_DEBT_SUM   NUMBER) RETURN NUMBER
		 IS
	l_prc_refin NUMBER;
	l_rate      NUMBER;
	l_sum       NUMBER := 0;
	l_debt_day  DATE;
	BEGIN
    --Если УК или ТСЖ, то долг отсчитывается от 16 числа месяца, следующего за расчетным		
	  IF P_ABON_GROUP IN ('UK','TSG') THEN
			 l_debt_day := TRUNC(LAST_DAY(P_DATE_INV)+16);	                                  
		-- Для остальных отсчитывается с 11 числа
		ELSE
 			 l_debt_day := TRUNC(LAST_DAY(P_DATE_INV)+11);
		END IF;
	 
	 --Если долга нет, то и считать нечего 
		IF P_DEBT_SUM = 0 THEN
			 RETURN 0;
		END IF;
		
		
		-- Считаем за каждый день просрочки
		FOR i IN (SELECT t.cal_date, ROWNUM rn  
							FROM
								 (SELECT cal_date
									FROM (Select TRUNC(P_DATE_ON+1-LEVEL) cal_date
												FROM dual
												CONNECT BY LEVEL <= TRUNC(P_DATE_ON) - l_debt_day+1)
									ORDER BY cal_date) t
						 )
		LOOP                                       
			 BEGIN
			 -- Процент рефинансирования и доля ставки на дату   
			 
 		   -- #873258 -- EBORODULIN -- 20.07.2017 
			 -- Смена логики расчета пени 
			 SELECT r.prc_ref, ra.rate      
			 INTO L_PRC_REFIN, L_RATE                
			 FROM T_REFIN r ,
						(SELECT rat.rate, rat.id_refin					 
						 FROM T_PENY_RATE rat
						 WHERE i.rn between rat.days_from AND NVL(rat.days_to,G$MAX_INT)
						 AND  rat.abon_group = P_ABON_GROUP) ra
			 WHERE P_DATE_ON BETWEEN r.date_ref_beg AND NVL(r.date_ref_end,P_DATE_ON)   ---WHERE i.cal_date
			 AND   r.id_refin = ra.id_refin;
			 
			 L_SUM := L_SUM + P_DEBT_SUM/L_RATE*(l_prc_refin/100);

			 -- Если на этот день в периоде пеня не считается, то пропускаем.
			 EXCEPTION 
					WHEN NO_DATA_FOUND THEN
						 CONTINUE;
			 END;			 
			 
		END LOOP;
		
	 -- Возвращаем сумму
			 RETURN l_sum;
	END; 
	
	-----Функция возвращает пени на деь P_DATE_ON для дел типа ПДК----
	FUNCTION F_GET_PENY_FOREACH_DAY_PDK(P_DATE_ON DATE,
																	    P_DATE_INV DATE,
																	    P_ABON_GROUP VARCHAR2,
																	    P_DEBT_SUM   NUMBER) RETURN NUMBER
		 IS
	l_prc_refin NUMBER;
	l_sum       NUMBER := 0;
	l_debt_day  DATE;
	BEGIN
   --Долг начинает отчитываться с первого дня просрочки
		l_debt_day :=P_DATE_INV;
	 
	 --Если долга нет, то и считать нечего 
		IF P_DEBT_SUM = 0 THEN
			 RETURN 0;
		END IF;
	
		-- Считаем за каждый день просрочки
		FOR i IN (SELECT t.cal_date, ROWNUM rn  
							FROM
								 (SELECT cal_date
									FROM (Select TRUNC(P_DATE_ON+1-LEVEL) cal_date
												FROM dual
												CONNECT BY LEVEL <= TRUNC(P_DATE_ON) - l_debt_day+1)
									ORDER BY cal_date) t
						 )
		LOOP                                       
			 BEGIN
			 -- Процент рефинансирования и доля ставки на дату 

		  -- #873258 -- EBORODULIN -- 20.07.2017 
			 -- Смена логики расчета пени 
			 SELECT r.prc_ref    
			 INTO L_PRC_REFIN               
			 FROM T_REFIN r 			
			 WHERE P_DATE_ON BETWEEN r.date_ref_beg AND NVL(r.date_ref_end,P_DATE_ON); --WHERE i.cal_date
			 			 		 
			 IF P_ABON_GROUP IN ('UK','TSG','OTHER') THEN
			 L_SUM := L_SUM + 2*P_DEBT_SUM*((l_prc_refin/100)/360);	                                  
		   -- для бюджета другой коэффициент
		   ELSIF P_ABON_GROUP = 'GOV' THEN
 			 L_SUM := L_SUM + (1/300)*P_DEBT_SUM*((l_prc_refin/100)/360);
		   END IF;
			 
			 -- Если на этот день в периоде пеня не считается, то пропускаем.
			 EXCEPTION 
					WHEN NO_DATA_FOUND THEN
						 CONTINUE;
			 END;			 
			 
		END LOOP;
		
	 -- Возвращаем сумму
			 RETURN l_sum;
	END;
	


  
	--Функция возвращает сумму пени по делу на текущее число
	FUNCTION F_GET_CLI_PENY(P_ID_CASE NUMBER,
													P_DATE    DATE DEFAULT SYSDATE,
													P_GROUP   VARCHAR2 DEFAULT NULL) RETURN NUMBER
		 IS    
		 l_group_code VARCHAR2(50);               
		 l_sum          NUMBER := 0; 
		 l_prc_refin    NUMBER; -- процент ставки рефинансирования ЦБ 
		 l_id_refin     NUMBER; -- ид ставки      
		 l_min_per      NUMBER; -- день с которого начинает начисляться пени, после образования задолженности.  
		 l_debt_date    DATE;   -- день, когда начисление становится долговым   
		 l_debt         NUMBER; -- Сумма, предъявленая к оплате  
		 l_peny_date    DATE;   -- Дата, с которой начала начисляться пени 
		 l_sum_pay      NUMBER; -- Сумма платежа      
		 l_prev_date    DATE;   -- Пердыдущая дата.   
		 l_rate         NUMBER; -- Доля ставки рефинансирования  
		 L_KOR          NUMBER;
		 
		 L_CNT NUMBER := 0;         
		 L_CALC PENY_CALC;   
		 L_CALC_IDX NUMBER := 0;
	BEGIN                                                
		
    IF P_GROUP IS NOT NULL THEN       
			L_GROUP_CODE := P_GROUP;
		ELSE
			--Достаём группу потребителей
			SELECT S.group_code
			INTO  l_group_code 	
			FROM  V_CLI_CASES S
			WHERE S.ID_CASE = P_ID_CASE;			
		END IF;
		            
		
		--ТЕКУЩАЯ СТАВКА РЕФЕНАНСИРОВАНИЯ НА ДЕНЬ РАСЧЕТА
		SELECT R.ID_REFIN,R.PRC_REF 
		INTO L_ID_REFIN,l_prc_refin
		FROM T_REFIN R 
		WHERE P_DATE BETWEEN  R.DATE_REF_BEG AND NVL(R.DATE_REF_END,P_DATE);

		--КОЛИЧЕСТВО ДНЕЙ С МОМЕНТА ВОЗНИКНОВЕНИЯ ЗАДОЛЖЕННСОТИ НАЧИНАЕТСЯ РАСЧЕТ ПЕНИ
		SELECT MIN(RA.DAYS_FROM) INTO l_min_per
		FROM T_PENY_RATE RA
		WHERE RA.ID_REFIN = l_id_refin
		AND   RA.ABON_GROUP = NVL(P_GROUP,L_GROUP_CODE);
   
		
			-- ПОДГОТАВЛИВАЕМ ДАННЫЕ
	FOR j IN (SELECT F_GET_DEBT_DATE(DECODE(L_GROUP_CODE
		                                     ,'UK',LAST_DAY(F.DOC_DATE)+15
		                                     ,'TSG',LAST_DAY(F.DOC_DATE)+15
																			   ,LAST_DAY(F.DOC_DATE)+10)
																  )+1 DEBT_START,
		                F.AMOUNT, F.ID_DOC, F.DOC_NUMBER, F.EXT_ID
						FROM T_CLI_DEBTS D,
								 T_CLI_FINDOCS F  
						WHERE F.ID_DOC = D.ID_DOC
						AND   D.ID_WORK = P_ID_CASE 
						ORDER BY  F.DOC_DATE)
	LOOP	               
/* 	  SELECT NVL(SUM(C.AMOUNT),0) 
		INTO   L_KOR
	  FROM   T_CLI_FINDOC_CONS C					        
		WHERE C.ID_DOC_TO IN (SELECT C.ID_DOC_TO 
		                      FROM T_CLI_FINDOC_CONS C 
													WHERE C.ID_DOC_FROM = J.ID_DOC)
		AND   C.ID_DOC_FROM != J.ID_DOC
		AND   C.DOC_TYPE = 'Корректировка реализации';	*/
	
    L_CNT := L_CNT + 1;
		L_CALC(L_CNT).DEBT_DATE := J.DEBT_START;
		L_CALC(L_CNT).NUM_DOC   := J.DOC_NUMBER;
		L_CALC(L_CNT).ID_DOC    := J.ID_DOC;
		L_CALC(L_CNT).SUM_DOC   := J.AMOUNT; --+ L_KOR;
		L_CALC(L_CNT).CALC := PERIOD_CALC();
		
		FOR Z IN (                              
		SELECT T.DATE_CHNG, NVL(LEAD(T.DATE_CHNG-1,1) OVER (ORDER BY T.DATE_CHNG), P_DATE) DATE_TO,
		       T.RATE, NULL PENY_PRC,T.PAY_SUM,T.TYPE_CALC		 
		FROM (SELECT TRUNC(C.DATE_CON,'DD') DATE_CHNG, 
		             SUM(CASE 
									 WHEN C.DOC_TYPE = 'Изменение состояния долга' THEN C.AMOUNT
									 ELSE  ABS(C.AMOUNT)
								  END)	  PAY_SUM, 0 RATE, 'PAYMENT' TYPE_CALC
					 FROM   T_CLI_FINDOC_CONS C					        
					 WHERE C.EXT_ID_TO != C.EXT_ID_FROM
					 AND   (C.EXT_ID_TO = J.EXT_ID OR C.EXT_ID_FROM = J.EXT_ID) 
					 --C.EXT_ID_TO IN (SELECT C.EXT_ID_TO FROM T_CLI_FINDOC_CONS C WHERE C.EXT_ID_FROM = J.EXT_ID)
--					 AND   C.EXT_ID_FROM != J.EXT_ID			
					 AND   TRUNC(C.DATE_CON,'DD') <= P_DATE                   
					 GROUP BY TRUNC(C.DATE_CON,'DD')  -- ВСЕ ПОСТУПЛЕНИЯ ДС ПО ЗАДОЛЖЕННОСТИ
					 UNION                              
					 SELECT J.DEBT_START + r.days_from - 1 DATE_CHNG, 0 PAY_SUM, r.rate , 'RATE' TYPE_CALC
					 FROM   T_PENY_RATE  r                        
					 WHERE  r.abon_group = NVL(P_GROUP,L_GROUP_CODE)
					 AND    r.id_refin = l_id_refin
					 AND    r.days_from + J.DEBT_START <= TRUNC(P_DATE,'DD')
					 )T
		ORDER BY T.DATE_CHNG ASC, T.TYPE_CALC ASC)
		LOOP                                                                                                      
				IF Z.TYPE_CALC = 'PAYMENT' THEN     
				L_CALC(L_CNT).CALC.EXTEND;
				L_CALC_IDX := L_CALC(L_CNT).CALC.COUNT;
				
				L_CALC(L_CNT).CALC(L_CALC_IDX).DATE_START := Z.DATE_CHNG;
				L_CALC(L_CNT).CALC(L_CALC_IDX).SUM_PAY    := Z.PAY_SUM;                                    
				L_CALC(L_CNT).CALC(L_CALC_IDX).TYPE_CALC  := Z.TYPE_CALC; 
				L_CALC(L_CNT).CALC(L_CALC_IDX).PENY_PRC  := l_prc_refin;
				
				IF L_CALC(L_CNT).CALC.EXISTS(L_CALC_IDX-1) AND L_CALC(L_CNT).CALC(L_CALC_IDX-1).TYPE_CALC != 'PAYMENT' THEN					
					L_CALC(L_CNT).CALC(L_CALC_IDX-1).DATE_END := L_CALC(L_CNT).CALC(L_CALC_IDX-1).DATE_END+1;
					L_CALC(L_CNT).CALC.EXTEND;
  				L_CALC_IDX := L_CALC(L_CNT).CALC.COUNT;				
					L_CALC(L_CNT).CALC(L_CALC_IDX).DATE_START  := Z.DATE_CHNG+1;
					L_CALC(L_CNT).CALC(L_CALC_IDX).DATE_END  := Z.DATE_TO;
					L_CALC(L_CNT).CALC(L_CALC_IDX).PENY_RATE := NVL(L_CALC(L_CNT).CALC(L_CALC_IDX-2).PENY_RATE,0);
					L_CALC(L_CNT).CALC(L_CALC_IDX).TYPE_CALC  := 'CALC';				                  
					L_CALC(L_CNT).CALC(L_CALC_IDX).PENY_PRC  := l_prc_refin;
				END IF;
				

			ELSE
				L_CALC(L_CNT).CALC.EXTEND;
				L_CALC_IDX := L_CALC(L_CNT).CALC.COUNT;				
				L_CALC(L_CNT).CALC(L_CALC_IDX).DATE_START := Z.DATE_CHNG;
        L_CALC(L_CNT).CALC(L_CALC_IDX).DATE_END   := Z.DATE_TO;
        L_CALC(L_CNT).CALC(L_CALC_IDX).TYPE_CALC  := Z.TYPE_CALC;				
        L_CALC(L_CNT).CALC(L_CALC_IDX).PENY_RATE  := Z.RATE;    
				L_CALC(L_CNT).CALC(L_CALC_IDX).PENY_PRC  := l_prc_refin;
			END IF;			
		END LOOP;																 
   END LOOP;	
		
	 FOR I IN 1..L_CALC.COUNT LOOP
		 L_DEBT := L_CALC(I).SUM_DOC;
		 L_SUM  := 0;
		 FOR J IN 1..L_CALC(I).CALC.COUNT LOOP
			  IF L_CALC(I).CALC(J).TYPE_CALC != 'PAYMENT' THEN    
					IF L_CALC(I).CALC(J).PENY_RATE != 0 THEN
					  L_SUM := L_SUM + L_DEBT/L_CALC(I).CALC(J).PENY_RATE*(L_CALC(I).CALC(J).PENY_PRC/100) * (L_CALC(I).CALC(J).DATE_END - L_CALC(I).CALC(J).DATE_START+1);						
					END IF;						
				ELSE
					L_DEBT := L_DEBT - L_CALC(I).CALC(J).SUM_PAY;
				END IF;
		 END LOOP;
		 UPDATE T_CLI_DEBTS D SET D.PENY = L_SUM, D.PENY_DATE = P_DATE WHERE D.ID_DOC = L_CALC(I).ID_DOC AND D.ID_WORK = P_ID_CASE; 	 		 
	 END LOOP;
	 
		
		 
		 SELECT SUM(D.PENY) INTO L_SUM FROM T_CLI_DEBTS D WHERE D.ID_WORK = P_ID_CASE;
		
		 RETURN L_SUM;
		 		 
	END F_GET_CLI_PENY;	
	
	
	
	--Функция расчитывает ГП на сумму.
	FUNCTION F_GET_CLI_GOV_FOR_SUM(P_SUM NUMBER) RETURN NUMBER
		IS  
	 L_RET NUMBER;
	BEGIN
		 		 --до 100 000 рублей - 4 процента цены иска, но не менее 2 000 рублей
		 IF P_SUM <= 100000 THEN
			  L_RET := P_SUM*0.04;
				IF L_RET < 2000 THEN
					 RETURN 2000;
				END IF; 
		 --от 100 001 рубля до 200 000 рублей - 4 000 рублей плюс 3 процента суммы, превышающей 100 000 рублей
		 ELSIF P_SUM BETWEEN 100001 AND 200000 THEN
		    L_RET := 4000 + (P_SUM-100000)*0.03;    
				
		 --от 200 001 рубля до 1 000 000 рублей - 7 000 рублей плюс 2 процента суммы, превышающей 200 000 рублей;
		 ELSIF P_SUM BETWEEN 200001 AND 1000000 THEN
		    L_RET := 7000 + (P_SUM-200000)*0.02;
		
	  --от 1 000 001 рубля до 2 000 000 рублей - 23 000 рублей плюс 1 процент суммы, превышающей 1 000 000 рублей;
		 ELSIF P_SUM BETWEEN 1000001 AND 2000000 THEN
		    L_ret := 23000 + (P_SUM-1000000)*0.01;                                                                  
		
		--свыше 2 000 000 рублей - 33 000 рублей плюс 0,5 процента суммы, превышающей 2 000 000 рублей, но не более 200 000 рублей;		
 		 ELSIF P_SUM >= 2000001 THEN
		    L_ret := 33000 + (P_SUM-2000000)*0.005;
				IF L_RET > 200000	THEN
					 RETURN 200000;
				END IF;
		 END IF;  
		 RETURN ceil(l_ret);
	END;
	
	--Возвращает госпошлину по клиенту юр. лицу
	FUNCTION F_GET_CLI_GOV_TOLL(P_ID_CASE NUMBER,
		                          P_TYPE    VARCHAR2) RETURN NUMBER
		 IS
		L_SUM  NUMBER;
		l_peny NUMBER;
		L_DEBT NUMBER;
		l_ret  NUMBER;
	BEGIN          
           
       SELECT S.peny, PKG_CLI_CASES.F_GET_DEBT_ON_DATE(S.ID_CASE,S.PENY_DATE)
       INTO   l_peny, l_debt
       FROM T_CLI_CASES S
			 WHERE s.id_case = P_ID_CASE;
	
	   --цена иска – основной долг+пеня
		 L_SUM := L_PENY+L_DEBT;
     -- ДЛЯ АРБИТРАЖА
		 IF P_TYPE IN ('ARBITR_LAWSUIT','ARBITR_ORDER') THEN
			 --до 100 000 рублей - 4 процента цены иска, но не менее 2 000 рублей
			 IF l_sum <= 100000 THEN
					L_RET := L_SUM*0.04;
					IF L_RET < 2000 THEN
						 L_RET := 2000;
					END IF; 
			 --от 100 001 рубля до 200 000 рублей - 4 000 рублей плюс 3 процента суммы, превышающей 100 000 рублей
			 ELSIF l_sum BETWEEN 100001 AND 200000 THEN
					L_RET := 4000 + (l_sum-100000)*0.03;    
					
			 --от 200 001 рубля до 1 000 000 рублей - 7 000 рублей плюс 2 процента суммы, превышающей 200 000 рублей;
			 ELSIF l_sum BETWEEN 200001 AND 1000000 THEN
					L_RET := 7000 + (l_sum-200000)*0.02;
			
			--от 1 000 001 рубля до 2 000 000 рублей - 23 000 рублей плюс 1 процент суммы, превышающей 1 000 000 рублей;
			 ELSIF l_sum BETWEEN 1000001 AND 2000000 THEN
					L_ret := 23000 + (l_sum-1000000)*0.01;                                                                  
			
			--свыше 2 000 000 рублей - 33 000 рублей плюс 0,5 процента суммы, превышающей 2 000 000 рублей, но не более 200 000 рублей;		
			 ELSIF l_sum >= 2000001 THEN
					L_ret := 33000 + (l_sum-2000000)*0.005;
					IF L_RET > 200000	THEN
						 L_RET := 200000;
					END IF;
			 END IF;                  
			 
			 IF P_TYPE = 'ARBITR_ORDER' THEN
				 L_RET := L_RET/2;
			 END IF;			 		 
 		 -- ДЛЯ ОБЩЕЙ ЮРИСДИКЦИИ 
		 ELSIF P_TYPE IN ('COMMON_LAWSUIT','COMMON_ORDER') THEN  
			--до 20 000 рублей - 4 процента цены иска, но не менее 400 рублей;
			 IF L_SUM <= 20000 THEN
				 L_RET := L_SUM * 0.04;
				 IF L_RET < 400 THEN
					  L_RET := 400;
				 END IF;
				 
			--от 20 001 рубля до 100 000 рублей - 800 рублей плюс 3 процента суммы, превышающей 20 000 рублей;	 
		   ELSIF L_SUM BETWEEN 20001 AND 100000 THEN
		     L_RET := 800 + (L_SUM-20000)*0.03;					                                                        
				 
			--от 100 001 рубля до 200 000 рублей - 3 200 рублей плюс 2 процента суммы, превышающей 100 000 рублей;	 
			 ELSIF L_SUM BETWEEN 100001 AND 200000 THEN
		     L_RET := 3200 + (L_SUM-100000)*0.02;	
				 
			--от 200 001 рубля до 1 000 000 рублей - 5 200 рублей плюс 1 процент суммы, превышающей 200 000 рублей;
			 ELSIF L_SUM BETWEEN 200001 AND 1000000 THEN
		     L_RET := 5200 + (L_SUM-200000)*0.01;
				 
		  --свыше 1 000 000 рублей - 13 200 рублей плюс 0,5 процента суммы, превышающей 1 000 000 рублей, но не более 60 000 рублей; 
		   ELSIF L_SUM BETWEEN 200001 AND 1000000 THEN
		     L_RET := 5200 + (L_SUM-200000)*0.01;	 	
				 	IF L_RET > 60000	THEN
						 L_RET := 60000;
					END IF;
			 END IF; 	 	
			 
			 IF P_TYPE = 'COMMON_ORDER' THEN
					L_RET := L_RET/2;
			 END IF;			 
		 END IF;
		 --Округляем до целых в большую сторону                                                                                                       
		 RETURN ROUND(l_ret,0);
	END;
	
		
end PKG_REFIN;
/

