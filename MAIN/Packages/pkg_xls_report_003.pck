i�?create or replace package lawmain.PKG_XLS_REPORT_003 is

  -- Author  : BES
  -- Created : 17.02.2017
  -- Purpose : Расчет суммы пени за просрочку платежей по договору для ВИВ
	-- Code:    SHUTDOWN_LIST 
	

  FUNCTION RUN_REPORT(P_PAR_01 VARCHAR2 DEFAULT NULL
											,P_PAR_02 VARCHAR2 DEFAULT NULL
											,P_PAR_03 VARCHAR2 DEFAULT NULL
											,P_PAR_04 VARCHAR2 DEFAULT NULL
											,P_PAR_05 VARCHAR2 DEFAULT NULL
											,P_PAR_06 VARCHAR2 DEFAULT NULL
											,P_PAR_07 VARCHAR2 DEFAULT NULL
											,P_PAR_08 VARCHAR2 DEFAULT NULL
											,P_PAR_09 VARCHAR2 DEFAULT NULL
											,P_PAR_10 VARCHAR2 DEFAULT NULL) RETURN LAWSUP.PKG_FILES.REC_DOC;	  

end PKG_XLS_REPORT_003;
/

create or replace package body lawmain.PKG_XLS_REPORT_003 is

	TYPE REC_DOC IS RECORD(	P_BLOB BLOB DEFAULT NULL );

	EXCEL_DOC REC_DOC := NULL;
------------------------------------------------------------------------

	PROCEDURE p$s( ps_value IN CLOB )  -- отправляет накопленное значение ps_value в буфер и записывает
	IS
		BUFFER    	RAW(32767);
    l_offset    NUMBER := 1;
    BUF_SIZE    NUMBER := 8000;
    BUF_VAR    VARCHAR2(32767);
  BEGIN
    LOOP
    EXIT WHEN L_OFFSET > DBMS_LOB.getlength(PS_VALUE);
    BUF_VAR := DBMS_LOB.substr(PS_VALUE,BUF_SIZE,L_OFFSET);
		BUFFER := UTL_RAW.cast_to_raw( CONVERT( BUF_VAR, 'UTF8'/*'CL8MSWIN1251'*/ ) );
		DBMS_LOB.WRITEAPPEND( EXCEL_DOC.P_BLOB, UTL_RAW.LENGTH( BUFFER ), BUFFER );
    L_OFFSET := L_OFFSET + BUF_SIZE;
    END LOOP;
  END;
------------------------------------------------------------------------
	PROCEDURE P_ADD_DATA(
	P_ID_FOLDER NUMBER
	) IS
	  L_XML CLOB;
		L_CNT NUMBER := 0;
	  l_group_code VARCHAR2(50);
		l_prc_refin NUMBER; --процент рефинансирования
    l_date_end DATE;
		L_ID_DOC       NUMBER;
		l_debt_date     DATE;  
    L_ID_REFIN NUMBER;
		L_FMT VARCHAR2(50) := '9999G999G999G999D99';
		L_NLS VARCHAR2(50) := 'NLS_NUMERIC_CHARACTERS='', ''';
		L_NEW_CTR BOOLEAN DEFAULT TRUE;
	BEGIN    
		SELECT MIN(S.SHUT_DATE) INTO L_DEBT_DATE FROM T_CLI_SHUTDOWN S WHERE S.ID_FOLDER = P_ID_FOLDER;
		L_XML :=
			'<?xml version="1.0"?>
<?mso-application progid="Excel.Sheet"?>
<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:o="urn:schemas-microsoft-com:office:office"
 xmlns:x="urn:schemas-microsoft-com:office:excel"
 xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:html="http://www.w3.org/TR/REC-html40">
 <DocumentProperties xmlns="urn:schemas-microsoft-com:office:office">
  <Author></Author>
  <LastAuthor></LastAuthor>
  <LastPrinted></LastPrinted>
  <Created></Created>
  <LastSaved></LastSaved>
  <Version>16.00</Version>
 </DocumentProperties>
 <OfficeDocumentSettings xmlns="urn:schemas-microsoft-com:office:office">
  <AllowPNG/>
 </OfficeDocumentSettings>
 <ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel">
  <WindowHeight>12300</WindowHeight>
  <WindowWidth>28800</WindowWidth>
  <WindowTopX>0</WindowTopX>
  <WindowTopY>0</WindowTopY>
  <RefModeR1C1/>
  <ProtectStructure>False</ProtectStructure>
  <ProtectWindows>False</ProtectWindows>
 </ExcelWorkbook>
 <Styles>
  <Style ss:ID="Default" ss:Name="Normal">
   <Alignment ss:Vertical="Bottom"/>
   <Borders/>
   <Font ss:FontName="Calibri" x:CharSet="204" x:Family="Swiss" ss:Size="11"
    ss:Color="#000000"/>
   <Interior/>
   <NumberFormat/>
   <Protection/>
  </Style>
  <Style ss:ID="s62" ss:Name="S0">
   <Alignment ss:Horizontal="Left" ss:Vertical="Top"/>
   <Borders/>
   <Font ss:FontName="Arial" x:CharSet="204" x:Family="Swiss" ss:Color="#000000"/>
   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>
   <NumberFormat/>
   <Protection/>
  </Style>
  <Style ss:ID="s63" ss:Name="S10">
   <Alignment ss:Horizontal="Center" ss:Vertical="Top"/>
   <Borders/>
   <Font ss:FontName="Arial" x:CharSet="204" x:Family="Swiss" ss:Color="#000000"/>
   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>
   <NumberFormat/>
   <Protection/>
  </Style>
  <Style ss:ID="s64" ss:Name="S11">
   <Alignment ss:Horizontal="Center" ss:Vertical="Top"/>
   <Borders/>
   <Font ss:FontName="Arial" x:CharSet="204" x:Family="Swiss" ss:Color="#000000"/>
   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>
   <NumberFormat/>
   <Protection/>
  </Style>
  <Style ss:ID="s65" ss:Name="S12">
   <Alignment ss:Horizontal="Center" ss:Vertical="Top"/>
   <Borders/>
   <Font ss:FontName="Arial" x:CharSet="204" x:Family="Swiss" ss:Color="#000000"/>
   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>
   <NumberFormat/>
   <Protection/>
  </Style>
  <Style ss:ID="s66" ss:Name="S14">
   <Alignment ss:Horizontal="Left" ss:Vertical="Top"/>
   <Borders/>
   <Font ss:FontName="Arial" x:CharSet="204" x:Family="Swiss" ss:Color="#000000"/>
   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>
   <NumberFormat/>
   <Protection/>
  </Style>
  <Style ss:ID="s67" ss:Name="S16">
   <Alignment ss:Horizontal="Center" ss:Vertical="Top"/>
   <Borders/>
   <Font ss:FontName="Arial" x:CharSet="204" x:Family="Swiss" ss:Color="#000000"/>
   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>
   <NumberFormat/>
   <Protection/>
  </Style>
  <Style ss:ID="s68" ss:Name="S17">
   <Alignment ss:Horizontal="Center" ss:Vertical="Top"/>
   <Borders/>
   <Font ss:FontName="Arial" x:CharSet="204" x:Family="Swiss" ss:Color="#000000"/>
   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>
   <NumberFormat/>
   <Protection/>
  </Style>
  <Style ss:ID="s69" ss:Name="S2">
   <Alignment ss:Horizontal="Center" ss:Vertical="Top"/>
   <Borders/>
   <Font ss:FontName="Arial" x:CharSet="204" x:Family="Swiss" ss:Color="#000000"
    ss:Bold="1"/>
   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>
   <NumberFormat/>
   <Protection/>
  </Style>
  <Style ss:ID="s71" ss:Name="S3">
   <Alignment ss:Horizontal="Left" ss:Vertical="Top"/>
   <Borders/>
   <Font ss:FontName="Arial" x:CharSet="204" x:Family="Swiss" ss:Color="#000000"/>
   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>
   <NumberFormat/>
   <Protection/>
  </Style>
  <Style ss:ID="s72" ss:Name="S4">
   <Alignment ss:Horizontal="Left" ss:Vertical="Top"/>
   <Borders/>
   <Font ss:FontName="Arial" x:CharSet="204" x:Family="Swiss" ss:Color="#000000"/>
   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>
   <NumberFormat/>
   <Protection/>
  </Style>
  <Style ss:ID="s73" ss:Name="S7">
   <Alignment ss:Horizontal="Center" ss:Vertical="Top"/>
   <Borders/>
   <Font ss:FontName="Arial" x:CharSet="204" x:Family="Swiss" ss:Size="8"
    ss:Color="#000000"/>
   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>
   <NumberFormat/>
   <Protection/>
  </Style>
  <Style ss:ID="s74" ss:Name="S8">
   <Alignment ss:Horizontal="Center" ss:Vertical="Top"/>
   <Borders/>
   <Font ss:FontName="Arial" x:CharSet="204" x:Family="Swiss" ss:Size="8"
    ss:Color="#000000"/>
   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>
   <NumberFormat/>
   <Protection/>
  </Style>
  <Style ss:ID="m476935672" ss:Parent="s68">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Arial" x:CharSet="204" x:Family="Swiss" ss:Size="24"/>
   <Interior/>
   <NumberFormat ss:Format="Standard"/>
  </Style>
  <Style ss:ID="m476935692" ss:Parent="s68">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Arial" x:CharSet="204" x:Family="Swiss" ss:Size="24"/>
   <Interior/>
   <NumberFormat ss:Format="Short Date"/>
  </Style>
  <Style ss:ID="m476906672">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Arial" x:CharSet="204" x:Family="Swiss" ss:Size="24"/>
   <Interior/>
  </Style>
  <Style ss:ID="m476906692" ss:Parent="s74">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Arial" x:CharSet="204" x:Family="Swiss" ss:Size="24"/>
   <Interior/>
   <NumberFormat ss:Format="0"/>
  </Style>
  <Style ss:ID="m476906712" ss:Parent="s66">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Arial" x:CharSet="204" x:Family="Swiss" ss:Size="24"/>
   <Interior/>
  </Style>
  <Style ss:ID="m476906732" ss:Parent="s66">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Arial" x:CharSet="204" x:Family="Swiss" ss:Size="24"/>
   <Interior/>
  </Style>
  <Style ss:ID="m476903452">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Arial" x:CharSet="204" x:Family="Swiss" ss:Size="24"/>
   <Interior/>
  </Style>
  <Style ss:ID="m476903472" ss:Parent="s65">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Arial" x:CharSet="204" x:Family="Swiss" ss:Size="24"/>
   <Interior/>
  </Style>
  <Style ss:ID="m476903492" ss:Parent="s65">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Arial" x:CharSet="204" x:Family="Swiss" ss:Size="24"/>
   <Interior/>
  </Style>
  <Style ss:ID="m476903512" ss:Parent="s65">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Arial" x:CharSet="204" x:Family="Swiss" ss:Size="24"/>
   <Interior/>
  </Style>
  <Style ss:ID="m476903532" ss:Parent="s65">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Arial" x:CharSet="204" x:Family="Swiss" ss:Size="24"/>
   <Interior/>
  </Style>
  <Style ss:ID="m476903552" ss:Parent="s65">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Arial" x:CharSet="204" x:Family="Swiss" ss:Size="24"/>
   <Interior/>
  </Style>
  <Style ss:ID="m476903572">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Arial" x:CharSet="204" x:Family="Swiss" ss:Size="24"/>
   <Interior/>
  </Style>
  <Style ss:ID="m476903592" ss:Parent="s65">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Arial" x:CharSet="204" x:Family="Swiss" ss:Size="24"/>
   <Interior/>
  </Style>
  <Style ss:ID="s94" ss:Parent="s71">
   <Alignment ss:Horizontal="Left" ss:Vertical="Top" ss:WrapText="1"/>
   <Font ss:FontName="Arial" x:CharSet="204" x:Family="Swiss" ss:Size="24"
    ss:Color="#000000"/>
   <Interior/>
  </Style>
  <Style ss:ID="s100" ss:Parent="s69">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders/>
   <Font ss:FontName="Arial" x:CharSet="204" x:Family="Swiss" ss:Size="24"
    ss:Color="#000000" ss:Bold="1"/>
   <Interior/>
  </Style>
  <Style ss:ID="s106">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Arial" x:CharSet="204" x:Family="Swiss" ss:Size="24"/>
   <Interior/>
  </Style>
  <Style ss:ID="s128" ss:Parent="s64">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Arial" x:CharSet="204" x:Family="Swiss" ss:Size="24"/>
   <Interior/>
  </Style>
  <Style ss:ID="s137" ss:Parent="s63">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Arial" x:CharSet="204" x:Family="Swiss" ss:Size="24"/>
   <Interior/>
  </Style>
  <Style ss:ID="s139" ss:Parent="s74">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Arial" x:CharSet="204" x:Family="Swiss" ss:Size="24"/>
   <Interior/>
   <NumberFormat ss:Format="0"/>
  </Style>
  <Style ss:ID="s141" ss:Parent="s66">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Arial" x:CharSet="204" x:Family="Swiss" ss:Size="24"/>
   <Interior/>
  </Style>
  <Style ss:ID="s143" ss:Parent="s68">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Arial" x:CharSet="204" x:Family="Swiss" ss:Size="24"/>
   <Interior/>
   <NumberFormat ss:Format="Standard"/>
  </Style>
  <Style ss:ID="s144" ss:Parent="s68">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Arial" x:CharSet="204" x:Family="Swiss" ss:Size="24"/>
   <Interior/>
   <NumberFormat ss:Format="Short Date"/>
  </Style>
  <Style ss:ID="s146" ss:Parent="s73">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Arial" x:CharSet="204" x:Family="Swiss" ss:Size="24"/>
   <Interior/>
   <NumberFormat ss:Format="Standard"/>
  </Style>
  <Style ss:ID="s147">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Arial" x:CharSet="204" x:Family="Swiss" ss:Size="24"/>
  </Style>
  <Style ss:ID="s149" ss:Parent="s67">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Arial" x:CharSet="204" x:Family="Swiss" ss:Size="24"/>
   <Interior/>
  </Style>
  <Style ss:ID="s150">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders/>
   <Font ss:FontName="Arial" x:CharSet="204" x:Family="Swiss" ss:Size="24"/>
   <Interior/>
  </Style>
  <Style ss:ID="s151" ss:Parent="s74">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders/>
   <Font ss:FontName="Arial" x:CharSet="204" x:Family="Swiss" ss:Size="24"/>
   <Interior/>
   <NumberFormat ss:Format="0"/>
  </Style>
  <Style ss:ID="s152" ss:Parent="s66">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders/>
   <Font ss:FontName="Arial" x:CharSet="204" x:Family="Swiss" ss:Size="24"/>
   <Interior/>
  </Style>
  <Style ss:ID="s153" ss:Parent="s68">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders/>
   <Font ss:FontName="Arial" x:CharSet="204" x:Family="Swiss" ss:Size="24"/>
   <Interior/>
   <NumberFormat ss:Format="Standard"/>
  </Style>
  <Style ss:ID="s154" ss:Parent="s74">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders/>
   <Font ss:FontName="Arial" x:CharSet="204" x:Family="Swiss" ss:Size="24"/>
   <Interior/>
   <NumberFormat ss:Format="Short Date"/>
  </Style>
  <Style ss:ID="s155" ss:Parent="s66">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders/>
   <Font ss:FontName="Arial" x:CharSet="204" x:Family="Swiss" ss:Size="24"/>
   <Interior/>
   <NumberFormat ss:Format="Short Date"/>
  </Style>
  <Style ss:ID="s156">
   <Alignment ss:Horizontal="Left" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders/>
   <Font ss:FontName="Arial" x:CharSet="204" x:Family="Swiss" ss:Size="24"
    ss:Bold="1"/>
   <Interior/>
  </Style>
  <Style ss:ID="s160" ss:Parent="s68">
   <Alignment ss:Horizontal="Left" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders/>
   <Font ss:FontName="Arial" x:CharSet="204" x:Family="Swiss" ss:Size="24"/>
   <Interior/>
   <NumberFormat ss:Format="Standard"/>
  </Style>
  <Style ss:ID="s161">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Font ss:FontName="Arial" x:CharSet="204" x:Family="Swiss" ss:Size="24"
    ss:Bold="1"/>
   <Interior/>
  </Style>
  <Style ss:ID="s163" ss:Parent="s62">
   <Alignment ss:Vertical="Top" ss:WrapText="1"/>
   <Font ss:FontName="Arial" x:CharSet="204" x:Family="Swiss" ss:Size="24"/>
   <Interior/>
  </Style>
  <Style ss:ID="s165">
   <Alignment ss:Horizontal="Right" ss:Vertical="Center" ss:WrapText="1"/>
   <Font ss:FontName="Arial" x:CharSet="204" x:Family="Swiss" ss:Size="24"/>
   <Interior/>
  </Style>
  <Style ss:ID="s166">
   <Alignment ss:Horizontal="Left" ss:Vertical="Center" ss:WrapText="1"/>
   <Font ss:FontName="Arial" x:CharSet="204" x:Family="Swiss" ss:Size="24"
    ss:Bold="1"/>
   <Interior/>
  </Style>
  <Style ss:ID="s261">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Font ss:FontName="Arial" x:CharSet="204" x:Family="Swiss" ss:Size="24"/>
   <Interior/>
  </Style>
  <Style ss:ID="s262" ss:Parent="s62">
   <Alignment ss:Horizontal="Left" ss:Vertical="Top" ss:WrapText="1"/>
   <Font ss:FontName="Arial" x:CharSet="204" x:Family="Swiss" ss:Size="24"
    ss:Color="#000000"/>
   <Interior/>
  </Style>
  <Style ss:ID="s264" ss:Parent="s62">
   <Alignment ss:Horizontal="Left" ss:Vertical="Center" ss:WrapText="1"/>
   <Font ss:FontName="Arial" x:CharSet="204" x:Family="Swiss" ss:Size="24"
    ss:Color="#000000"/>
   <Interior/>
  </Style>
  <Style ss:ID="s265" ss:Parent="s72">
   <Alignment ss:Horizontal="Left" ss:Vertical="Top" ss:WrapText="1"/>
   <Font ss:FontName="Arial" x:CharSet="204" x:Family="Swiss" ss:Size="24"
    ss:Color="#000000"/>
   <Interior/>
  </Style>
  <Style ss:ID="s266" ss:Parent="s72">
   <Alignment ss:Horizontal="Left" ss:Vertical="Top" ss:WrapText="1"/>
   <Font ss:FontName="Arial" x:CharSet="204" x:Family="Swiss" ss:Size="24"
    ss:Color="#000000"/>
   <Interior/>
  </Style>
  <Style ss:ID="s268" ss:Parent="s62">
   <Alignment ss:Horizontal="Left" ss:Vertical="Top" ss:WrapText="1"/>
   <Borders/>
   <Font ss:FontName="Arial" x:CharSet="204" x:Family="Swiss" ss:Size="24"
    ss:Color="#000000"/>
   <Interior/>
  </Style>
  <Style ss:ID="s269" ss:Parent="s62">
   <Alignment ss:Horizontal="Left" ss:Vertical="Top" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Arial" x:CharSet="204" x:Family="Swiss" ss:Size="24"
    ss:Color="#000000"/>
   <Interior/>
  </Style>
  <Style ss:ID="s270" ss:Parent="s69">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Arial" x:CharSet="204" x:Family="Swiss" ss:Size="24"
    ss:Color="#000000" ss:Bold="1"/>
   <Interior/>
  </Style>
  <Style ss:ID="s271" ss:Parent="s69">
   <Alignment ss:Horizontal="Center" ss:Vertical="Top" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Arial" x:CharSet="204" x:Family="Swiss" ss:Size="24"
    ss:Color="#000000" ss:Bold="1"/>
   <Interior/>
  </Style>
  <Style ss:ID="s272">
   <Alignment ss:Vertical="Bottom" ss:WrapText="1"/>
   <Font ss:FontName="Arial" x:CharSet="204" x:Family="Swiss" ss:Size="24"/>
   <Interior/>
  </Style>
  <Style ss:ID="s273">
   <Alignment ss:Vertical="Center" ss:WrapText="1"/>
   <Font ss:FontName="Arial" x:CharSet="204" x:Family="Swiss" ss:Size="24"
    ss:Color="#000000"/>
   <Interior/>
  </Style>
 </Styles>
 <Worksheet ss:Name="Лист1">
  <Table>
   <Column ss:StyleID="s261" ss:Width="39"/>
   <Column ss:StyleID="s272" ss:Width="81"/>
   <Column ss:StyleID="s272" ss:AutoFitWidth="0" ss:Width="189"/>
   <Column ss:StyleID="s272" ss:Width="462"/>
   <Column ss:StyleID="s272" ss:Width="327"/>
   <Column ss:StyleID="s273" ss:Width="168.75"/>
   <Column ss:StyleID="s272" ss:Width="228.75"/>
   <Column ss:StyleID="s272" ss:Width="555.75"/>
   <Column ss:StyleID="s272" ss:Width="549"/>
   <Row ss:AutoFitHeight="1">
    <Cell ss:Index="2" ss:StyleID="s262"><Data ss:Type="String" x:Ticked="1"></Data></Cell>
    <Cell ss:StyleID="s262"/>
    <Cell ss:StyleID="s262"/>
    <Cell ss:StyleID="s262"/>
    <Cell ss:StyleID="s264"/>
    <Cell ss:StyleID="s262"/>
    <Cell ss:StyleID="s262"/>
    <Cell ss:StyleID="s262"/>
   </Row>
   <Row ss:AutoFitHeight="1">
    <Cell ss:Index="2" ss:MergeAcross="5" ss:StyleID="s265"><Data ss:Type="String">Согласована техническая возможность введения временного ограничения/прекращения услуг ХВС/ВО на сетях АО &quot;НВ&quot;:</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="1">
    <Cell ss:Index="2" ss:MergeAcross="3" ss:StyleID="s94"><Data ss:Type="String">Директор по эксплуатации сетей и сооружений                                                                      </Data></Cell>
    <Cell ss:MergeAcross="1" ss:StyleID="s94"/>
   </Row>
   <Row ss:AutoFitHeight="1">
    <Cell ss:Index="2" ss:MergeAcross="3" ss:StyleID="s94"><Data ss:Type="String">_____________________________В.Е.Шацков</Data></Cell>
    <Cell ss:MergeAcross="1" ss:MergeDown="1" ss:StyleID="s94"/>
   </Row>
   <Row ss:AutoFitHeight="1">
    <Cell ss:Index="2" ss:MergeAcross="3" ss:StyleID="s262"/>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="9">
    <Cell ss:Index="2" ss:MergeAcross="3" ss:StyleID="s94"/>
    <Cell ss:MergeAcross="1" ss:StyleID="s262"/>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="75.75">
    <Cell ss:Index="2" ss:MergeAcross="7" ss:MergeDown="1" ss:StyleID="s100"><Data
      ss:Type="String">Список должников, в отношении которых вводится временное ограничение и/или прекращение услуги холодного водоснабжения и/или водоотведения c '||TO_CHAR(L_DEBT_DATE,'DD.MM.YYYY')||'г. согласно ст.21 Федерального закона РФ от 07.12.2011г. № 416-ФЗ &quot;О водоснабжении и водоотведении&quot;</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="75.75"/>
   <Row ss:AutoFitHeight="0">
    <Cell ss:Index="2" ss:StyleID="s268"/>
    <Cell ss:StyleID="s268"/>
    <Cell ss:StyleID="s268"/>
    <Cell ss:StyleID="s269"/>
    <Cell ss:StyleID="s270"/>
    <Cell ss:StyleID="s271"/>
    <Cell ss:StyleID="s271"/>
    <Cell ss:StyleID="s262"/>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="60">
    <Cell ss:MergeDown="1" ss:StyleID="m476903452"/>
    <Cell ss:MergeDown="1" ss:StyleID="m476903472"><Data ss:Type="String"
      x:Ticked="1">&#10;№&#10;дог.</Data></Cell>
    <Cell ss:MergeDown="1" ss:StyleID="m476903492"><Data ss:Type="String"
      x:Ticked="1">&#10;Район</Data></Cell>
    <Cell ss:MergeDown="1" ss:StyleID="m476903512"><Data ss:Type="String"
      x:Ticked="1">&#10;Наименование&#10;предприятия</Data></Cell>
    <Cell ss:StyleID="s128"><Data ss:Type="String" x:Ticked="1">Сумма долга, руб.</Data></Cell>
    <Cell ss:MergeDown="1" ss:StyleID="m476903532"><Data ss:Type="String"
      x:Ticked="1">Фактический &#10;срок&#10;ограничения/ отключения</Data></Cell>
    <Cell ss:MergeDown="1" ss:StyleID="m476903552"><Data ss:Type="String"
      x:Ticked="1">Ответственный&#10;исполнитель&#10;за отключение</Data></Cell>
    <Cell ss:MergeDown="1" ss:StyleID="m476903572"><Data ss:Type="String">Адрес объекта</Data></Cell>
    <Cell ss:MergeDown="1" ss:StyleID="m476903592"><Data ss:Type="String"
      x:Ticked="1">&#10;Примечание</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="60">
    <Cell ss:Index="5" ss:StyleID="s137"><Data ss:Type="String" x:Ticked="1">Период&#10;задолженности</Data></Cell>
   </Row>';
   p$s(L_XML);
   	 

   	FOR I IN (SELECT CT.CTR_NUMBER,        
										 COUNT (1) OVER(PARTITION BY CT.CTR_NUMBER) CTR_CNT,       
										 LEAD (CT.CTR_NUMBER)  OVER (ORDER BY S.SHUT_DATE, CT.CTR_NUMBER ASC, S.SUM_DEBT DESC) NEXT_CTR,
										 (SELECT D.DISTR_NAME FROM T_DISTRICT D WHERE D.ID_DISTRICT = CLI.ID_DISTRICT) DISTR,
										 CLI.CLI_NAME ,
										 S.SUM_DEBT,
										 (SELECT TO_CHAR(MIN(FD.DOC_DATE),'DD.MM.YYYY"г."')||' - '||TO_CHAR(MAX(FD.DOC_DATE),'DD.MM.YYYY"г."')
										  FROM  T_CLI_FINDOCS FD,
											      T_CLI_DEBTS D 
											WHERE FD.ID_DOC =  D.ID_DOC
											AND   FD.DEBT >= 0
											AND   D.ID_WORK = S.ID_WORK) PERIOD,
										  S.SHUT_DATE,
											O.ADDRESS_NAME OBJ_ADDRESS,
											O.OBJ_NAME, 
											O.ADDRESS_NAME,
											J.CON_TYPE                       
						   FROM T_CLI_SHUTDOWN S
              INNER JOIN  T_CONTRACTS CT ON (CT.ID_CONTRACT = S.ID_CONTRACT)
              INNER JOIN  T_CLIENTS CLI ON (CLI.ID_CLIENT = CT.ID_CLIENT)
              INNER JOIN  T_CONTRACT_OBJ O ON (CT.ID_CONTRACT = O.ID_CONTRACT)
              LEFT OUTER JOIN T_CTR_OBJ_DISABLE J ON (J.ID_OBJECT = O.ID_OBJECT ) 
              WHERE S.ID_FOLDER = P_ID_FOLDER
              AND   S.SHUT_DATE IS NOT NULL
              AND (J.IS_ABLE = 'Y' OR S.IS_MANUAL = 'Y')
              ORDER BY S.SHUT_DATE, CT.CTR_NUMBER ASC, S.SUM_DEBT DESC)
		LOOP
      IF L_NEW_CTR THEN
				L_CNT := L_CNT + 1;
			  L_XML := '<Row ss:AutoFitHeight="0" ss:Height="99.75">
									<Cell ss:MergeDown="'||TO_CHAR(I.CTR_CNT-1)||'" ss:StyleID="m476906672"><Data ss:Type="String">'||L_CNT||'</Data></Cell>
									<Cell ss:MergeDown="'||TO_CHAR(I.CTR_CNT-1)||'" ss:StyleID="m476906692"><Data ss:Type="String">'||I.CTR_NUMBER||'</Data></Cell>
									<Cell ss:MergeDown="'||TO_CHAR(I.CTR_CNT-1)||'" ss:StyleID="m476906712"><Data ss:Type="String">'||I.DISTR||'</Data></Cell>
									<Cell ss:MergeDown="'||TO_CHAR(I.CTR_CNT-1)||'" ss:StyleID="m476906732"><Data ss:Type="String">'||I.CLI_NAME||'</Data></Cell>
									<Cell ss:MergeDown="'||TO_CHAR(I.CTR_CNT-1)||'" ss:StyleID="m476935672"><Data ss:Type="String">'||TO_CHAR(I.SUM_DEBT,L_FMT,L_NLS)||'&#10;'||I.PERIOD||'</Data></Cell>
									<Cell ss:MergeDown="'||TO_CHAR(I.CTR_CNT-1)||'" ss:StyleID="m476935692"><Data ss:Type="String">'||TO_CHAR(I.SHUT_DATE,'DD.MM.YYYY')||'</Data></Cell>
									<Cell ss:StyleID="s146"><Data ss:Type="String">'||I.CON_TYPE||'</Data></Cell>
									<Cell ss:StyleID="s147"><Data ss:Type="String">'||I.OBJ_ADDRESS||'</Data></Cell>
									<Cell ss:StyleID="s149"><Data ss:Type="String">'||I.OBJ_NAME||'</Data></Cell>
								 </Row>';
				 p$s(L_XML);
				 L_NEW_CTR := FALSE;				 
			ELSE
			   L_XML := '<Row ss:AutoFitHeight="0" ss:Height="99.75">
											<Cell ss:Index="7" ss:StyleID="s146"><Data ss:Type="String">'||I.CON_TYPE||'</Data></Cell>
											<Cell ss:StyleID="s147"><Data ss:Type="String">'||I.OBJ_ADDRESS||'</Data></Cell>
											<Cell ss:StyleID="s149"><Data ss:Type="String">'||I.OBJ_NAME||'</Data></Cell>
									 </Row>';
				 p$s(L_XML);					 
			END IF;																																						 
			 
			IF I.NEXT_CTR !=  I.CTR_NUMBER THEN
				L_NEW_CTR := TRUE;
		  END IF;
		END LOOP; 		                 


		

	 L_XML := '<Row ss:AutoFitHeight="0" ss:Height="30">
    <Cell ss:StyleID="s150"/>
    <Cell ss:StyleID="s151"/>
    <Cell ss:StyleID="s150"/>
    <Cell ss:StyleID="s152"/>
    <Cell ss:StyleID="s153"/>
    <Cell ss:StyleID="s154"/>
    <Cell ss:StyleID="s155"/>
    <Cell ss:StyleID="s154"/>
    <Cell ss:StyleID="s150"/>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="69.75">
    <Cell ss:MergeAcross="8" ss:StyleID="s156"><Data ss:Type="String">Директор по работе с абонентами                                                                                                                                                                          А.В.Татаринов</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="30">
    <Cell ss:MergeAcross="2" ss:StyleID="s152"><Data ss:Type="String">Cогласовано:</Data></Cell>
    <Cell ss:StyleID="s160"><Data ss:Type="String">Н.М.Палинина</Data></Cell>
    <Cell ss:StyleID="s156"/>
    <Cell ss:StyleID="s156"/>
    <Cell ss:StyleID="s156"/>
    <Cell ss:StyleID="s156"/>
    <Cell ss:StyleID="s156"/>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="30">
    <Cell ss:MergeAcross="2" ss:StyleID="s152"><Data ss:Type="String">Cогласовано:</Data></Cell>
    <Cell ss:StyleID="s160"><Data ss:Type="String">Ю.Ю.Комельков</Data></Cell>
    <Cell ss:StyleID="s156"/>
    <Cell ss:StyleID="s156"/>
    <Cell ss:StyleID="s156"/>
    <Cell ss:StyleID="s156"/>
    <Cell ss:StyleID="s156"/>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="76.5">
    <Cell ss:StyleID="s161"/>
    <Cell ss:MergeAcross="2" ss:StyleID="s163"><Data ss:Type="String">Исп. Клопова&#10;тел.233-99-99 внутр.3-3032&#10;</Data></Cell>
    <Cell ss:MergeAcross="1" ss:StyleID="s165"/>
    <Cell ss:StyleID="s166"/>
    <Cell ss:StyleID="s166"/>
    <Cell ss:StyleID="s166"/>
   </Row>
  </Table>
  <WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">
   <PageSetup>
    <Header x:Margin="0.31496062992125984"/>
    <Footer x:Margin="0.31496062992125984"/>
    <PageMargins x:Bottom="0.74803149606299213" x:Left="0.70866141732283472"
     x:Right="0.70866141732283472" x:Top="0.74803149606299213"/>
   </PageSetup>
   <Unsynced/>
   <FitToPage/>
   <Print>
    <FitHeight>0</FitHeight>
    <ValidPrinterInfo/>
    <PaperSizeIndex>9</PaperSizeIndex>
    <Scale>17</Scale>
    <HorizontalResolution>600</HorizontalResolution>
    <VerticalResolution>600</VerticalResolution>
   </Print>
   <Zoom>40</Zoom>
   <Selected/>
   <Panes>
    <Pane>
     <Number>3</Number>
     <ActiveRow>19</ActiveRow>
     <ActiveCol>6</ActiveCol>
    </Pane>
   </Panes>
   <ProtectObjects>False</ProtectObjects>
   <ProtectScenarios>False</ProtectScenarios>
  </WorksheetOptions>
 </Worksheet>
</Workbook>';

		p$s( L_XML );
	
	
	END P_ADD_DATA;

---------------------------------------------------------------------------------------------------------------------------------

  FUNCTION RUN_REPORT(P_PAR_01 VARCHAR2 DEFAULT NULL
											,P_PAR_02 VARCHAR2 DEFAULT NULL
											,P_PAR_03 VARCHAR2 DEFAULT NULL
											,P_PAR_04 VARCHAR2 DEFAULT NULL
											,P_PAR_05 VARCHAR2 DEFAULT NULL
											,P_PAR_06 VARCHAR2 DEFAULT NULL
											,P_PAR_07 VARCHAR2 DEFAULT NULL
											,P_PAR_08 VARCHAR2 DEFAULT NULL
											,P_PAR_09 VARCHAR2 DEFAULT NULL
											,P_PAR_10 VARCHAR2 DEFAULT NULL) RETURN LAWSUP.PKG_FILES.REC_DOC
	IS
	l_BLOB LAWSUP.PKG_FILES.REC_DOC;
  BEGIN

	  EXCEL_DOC := NULL;
		DBMS_LOB.CREATETEMPORARY( EXCEL_DOC.P_BLOB, TRUE );
    DBMS_LOB.OPEN( EXCEL_DOC.P_BLOB, DBMS_LOB.LOB_READWRITE );

		P_ADD_DATA(TO_NUMBER(P_PAR_01));

		DBMS_LOB.CLOSE(EXCEL_DOC.P_BLOB);

		    l_BLOB.P_BLOB := EXCEL_DOC.P_BLOB;
	      l_BLOB.P_FILE_NAME := 'GRAFIC_OTKLUCHENIY_'||P_PAR_01;
				RETURN L_BLOB;

  END RUN_REPORT;

end PKG_XLS_REPORT_003;
/

