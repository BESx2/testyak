i�?CREATE OR REPLACE PACKAGE LAWMAIN.PKG_PEACE_AGREE IS

	-- Author  : EUGEN
	-- Created : 24.03.2019 11:23:29
	-- Purpose : Пакет для работы с рестуктуризацией  	
	
	TYPE R_CALC_RES IS RECORD (PAY_CWS NUMBER,
	                           PAY_SEW NUMBER,
														 PENY_CWS NUMBER,
														 PENY_SEW NUMBER,
	                           DATE_CALC DATE);
														 														      	
	TYPE DEBT_CALC IS TABLE OF R_CALC_RES INDEX BY PLS_INTEGER;													 
	
	TYPE R_DEBT_AGREE IS RECORD(ID_CASE_DEBT NUMBER,
	                            DOC_DATE      DATE,
														  DEBT          NUMBER);
															
	TYPE AGREE_DEBTS IS TABLE OF R_DEBT_AGREE;														
	
  PROCEDURE P_CALC_AGREE_PERIOD(P_DATE_START DATE
															 ,P_DATE_END   DATE
															 ,P_PAY_TO     NUMBER
															 ,P_ID_EXEC    NUMBER);
	
  PROCEDURE P_CALC_AGREE_AMOUNT(P_DATE_START DATE,
						 											 P_SUM_PAY NUMBER,
																	 P_PAY_TO NUMBER,
																	 P_ID_EXEC NUMBER) ;
																	   	
	PROCEDURE P_CREATE_AGREE(P_ID_EXEC    NUMBER		                      
													,P_START_DATE DATE  												
													,P_TYPE_CALC  VARCHAR2   													
													,P_END_DATE   DATE DEFAULT NULL
													,P_MONTH_PAY  NUMBER DEFAULT NULL
													,P_PAY_DAY    NUMBER DEFAULT NULL
													,P_INC_PENY   VARCHAR2);  
		
	
	PROCEDURE P_RECALC_PAYMENT(P_ID_PAYMENT NUMBER,
		                         P_SUM_PAY    NUMBER,
														 P_PAY_DATE   DATE);															 	
	
	--РАПСРЕДЕЛЕНИЕ ПЕНИ ПО ПЛАТЕЖАМ
	--PROCEDURE P_DISTRIBUTE_PENY(P_ID_EXEC NUMBER);
	
	--РАСЧЕТ ПЕНИ НА ДЕНЬ ФАКТИЧЕСКОЙ ОПЛАТЫ СОГЛАШЕНИЯ
	--PROCEDURE P_CALC_PENY(P_ID_EXEC NUMBER);												 											 										 
	
		
  PROCEDURE P_CHANGE_STATUS(P_ID_EXEC NUMBER,
		                        P_STATUS  VARCHAR2);
END PKG_PEACE_AGREE;
/

CREATE OR REPLACE PACKAGE BODY LAWMAIN.PKG_PEACE_AGREE IS

  /*PROCEDURE P_CREATE_PEACE_PAY_DET(P_ID_EXEC NUMBER)
    IS                                                                                                                
    L_BALANCE NUMBER;                                                                                                 
    L_AGREE_DEBTS AGREE_DEBTS := AGREE_DEBTS();
  BEGIN     
    SELECT CCD.ID_DEBT, FD.DOC_DATE, CCD.DEBT_CURRENT
    BULK COLLECT INTO L_AGREE_DEBTS
    FROM T_CLI_COURT_EXEC E, 
         T_CLI_DEBTS CCD, 
         T_CLI_FINDOCS FD
    WHERE E.ID = P_ID_EXEC 
    AND E.ID_CASE = CCD.ID_WORK 
    AND FD.ID_DOC = CCD.ID_DOC            
    AND CCD.DEBT_CURRENT > 0
    ORDER BY FD.DOC_DATE, CCD.ID_DEBT;
                   
    FOR I IN (SELECT P.ID,P.PAY_SUM, P.PENY_SUM FROM T_CLI_PEACE_AGREE_PAYMENTS P WHERE P.ID_EXEC = P_ID_EXEC ORDER BY p.pay_date)
    LOOP                
      L_BALANCE := I.PAY_SUM;
      FOR J IN 1..L_AGREE_DEBTS.COUNT 
      LOOP                            
        IF L_AGREE_DEBTS(J).DEBT <= 0 THEN 
          CONTINUE; 
        ELSE
           IF L_AGREE_DEBTS(J).DEBT >= L_BALANCE THEN
             INSERT INTO T_CLI_AGREE_PAY_DET(ID,ID_DEBT,ID_PAY,SUM_PAY)
             VALUES (SEQ_REST_PAY_DET.NEXTVAL,L_AGREE_DEBTS(J).ID_CASE_DEBT, I.ID,L_BALANCE);
             L_BALANCE := L_AGREE_DEBTS(J).DEBT - L_BALANCE;
             L_AGREE_DEBTS(J).DEBT := L_BALANCE;
             EXIT;
           ELSE                          
             INSERT INTO T_CLI_AGREE_PAY_DET(ID,ID_DEBT,ID_PAY,SUM_PAY)
             VALUES (SEQ_REST_PAY_DET.NEXTVAL,L_AGREE_DEBTS(J).ID_CASE_DEBT, I.ID,L_AGREE_DEBTS(J).DEBT);
						 L_BALANCE := L_BALANCE - L_AGREE_DEBTS(J).DEBT;                                                                 
             L_AGREE_DEBTS(J).DEBT := 0;
           END IF;
    		END IF; 
			END LOOP;
		END LOOP;
	END;*/           

------------------------------------------------------------------------------------------------

	PROCEDURE P_CALC_AGREE_PERIOD(P_DATE_START DATE
															 ,P_DATE_END    DATE
															 ,P_PAY_TO     NUMBER
															 ,P_ID_EXEC    NUMBER)
		IS      
		L_DEBT NUMBER;    
		L_CALC DEBT_CALC;
		L_DEBT_CWS NUMBER;
		L_DEBT_SEW NUMBER;
		L_PENY_CWS NUMBER;
		L_PENY_SEW NUMBER;		
	BEGIN        
    
	  DELETE FROM T_CLI_PEACE_AGREE_PAYMENTS P WHERE P.ID_EXEC = P_ID_EXEC;
    												
		UPDATE T_CLI_PEACE_AGREE R
		   SET R.DATE_FROM = P_DATE_START,
		  	   R.DATE_TO   = P_DATE_END,
		 			 R.PAY_DAY   = P_PAY_TO,
 					 R.TYPE_CALC  = 'PERIOD'
		 WHERE R.ID_EXEC = P_ID_EXEC
		 RETURNING R.DEBT_CWS,R.DEBT_SEW,R.PENY_CWS,R.PENY_SEW
		 INTO L_DEBT_CWS,L_DEBT_SEW,L_PENY_CWS,L_PENY_SEW;
		
		 SELECT 
		        FLOOR(L_DEBT_CWS / (COUNT(*) OVER()) * 100) / 100 + CASE
							 WHEN ROW_NUMBER() OVER(ORDER BY T.PERIOD) <=
										(L_DEBT_CWS - (FLOOR(L_DEBT_CWS / (COUNT(*) OVER()) * 100) / 100) *
										 (COUNT(*) OVER())) / 0.01 THEN		0.01
							 ELSE		0
						 END PAY_CWS   --РАСЧЕТ СУММЫ ПЛАТЕЖА, МАХИНАЦИИ С КОПЕЙКАМИ
						,FLOOR(L_DEBT_SEW / (COUNT(*) OVER()) * 100) / 100 + CASE
							 WHEN ROW_NUMBER() OVER(ORDER BY T.PERIOD) <=
										(L_DEBT_SEW - (FLOOR(L_DEBT_SEW / (COUNT(*) OVER()) * 100) / 100) *
										 (COUNT(*) OVER())) / 0.01 THEN		0.01
							 ELSE		0
						 END PAY_SEW
						,FLOOR(L_PENY_CWS / (COUNT(*) OVER()) * 100) / 100 + CASE
							 WHEN ROW_NUMBER() OVER(ORDER BY T.PERIOD) <=
										(L_PENY_CWS - (FLOOR(L_PENY_CWS / (COUNT(*) OVER()) * 100) / 100) *
										 (COUNT(*) OVER())) / 0.01 THEN		0.01
							 ELSE		0
						 END PENY_CWS   --РАСЧЕТ СУММЫ ПЛАТЕЖА, МАХИНАЦИИ С КОПЕЙКАМИ
						,FLOOR(L_PENY_SEW / (COUNT(*) OVER()) * 100) / 100 + CASE
							 WHEN ROW_NUMBER() OVER(ORDER BY T.PERIOD) <=
										(L_PENY_SEW - (FLOOR(L_PENY_SEW / (COUNT(*) OVER()) * 100) / 100) *
										 (COUNT(*) OVER())) / 0.01 THEN		0.01
							 ELSE		0
						 END PENY_SEW
						,T.PERIOD
		 BULK COLLECT INTO L_CALC
		 FROM 
					 ---СПИСОК ВСЕХ ДНЕЙ МЕЖДУ ДВУМЯ ДАТАМИ
					 (SELECT START_DATE + LEVEL - 1 PERIOD
						FROM (SELECT P_DATE_START START_DATE
												,P_DATE_END   END_DATE
									FROM DUAL)
						CONNECT BY LEVEL <= TRUNC(END_DATE) - TRUNC(START_DATE) + 1) T
						
		 WHERE EXTRACT(DAY FROM PERIOD) =    --ЕСЛИ ОПЛАТА ПО ИЗ ЧИСЛА ПОСЛЕДНИХ ДЕНЙ МЕСЯЦА
							CASE                                                                                            
								--ЕЛСИ ФЕВРАЛЬ И ОПЛАТА ПО > 29, ТОГДА БЕРЁМ ВСЕГДА ПОСЛЕДНИЙ ДЕНЬ МЕСЯЦА
								WHEN P_PAY_TO >= 29 AND EXTRACT(MONTH FROM PERIOD) = 2 THEN EXTRACT(DAY FROM LAST_DAY(PERIOD))
								--ЕСЛИ ОПЛАТИТЬ ПО 31, А ПОСЛЕДНИЙ ДЕНЬ МЕСЯЦА 30, ТО УСТАНАВЛИВАЕМ 30
								WHEN P_PAY_TO = 31 AND EXTRACT(DAY FROM LAST_DAY(PERIOD)) = 30 THEN 30
								--ИНАЧЕ ПРОСТО ОТБИРАЕМ ДАТУ В ЭТОМ МЕСЯЦЕ
								ELSE P_PAY_TO
							END    
			--ДОБАВЛЯЯ ПЕРИОД ПО, Т.К. ДАТА МОЖЕТ ОТЛИЧАТЬСЯ ОТ "ОПЛАТИТЬ ПО" 				
		 OR T.PERIOD = P_DATE_END; 
			
		FORALL I IN 1..L_CALC.COUNT
			INSERT INTO T_CLI_PEACE_AGREE_PAYMENTS(ID,ID_EXEC,PAY_DATE,SUM_CWS,PENY_CWS,SUM_SEW,PENY_SEW)
			VALUES(SEQ_REST_PAY.NEXTVAL,P_ID_EXEC,L_CALC(I).DATE_CALC,L_CALC(I).PAY_CWS,
			       L_CALC(I).PENY_CWS,L_CALC(I).PAY_SEW,L_CALC(I).PENY_SEW);
						
	END;            

----------------------------------------------------------------------------------
	
  PROCEDURE P_CALC_AGREE_AMOUNT(P_DATE_START DATE,
						 										P_SUM_PAY NUMBER,
																P_PAY_TO NUMBER,
																P_ID_EXEC NUMBER) 
	 IS
	 L_CNT NUMBER := 0;         
	 L_CALC DEBT_CALC;                                                    
	 L_INC_PENY VARCHAR2(1);
	 L_DEBT_CWS NUMBER;
	 L_DEBT_SEW NUMBER;
	 L_PENY_CWS NUMBER;
	 L_PENY_SEW NUMBER;		
	 L_DEBT NUMBER;
	BEGIN
    
		DELETE FROM T_CLI_PEACE_AGREE_PAYMENTS P WHERE P.ID_EXEC = P_ID_EXEC;
	  
   	UPDATE T_CLI_PEACE_AGREE R
		   SET R.DATE_FROM = P_DATE_START,
		 			 R.PAY_DAY    = P_PAY_TO,
					 R.MONTH_PAY  = P_SUM_PAY,
					 R.TYPE_CALC  = 'PAYMENT'
		WHERE R.ID_EXEC = P_ID_EXEC
		RETURNING R.DEBT_CWS,R.DEBT_SEW,R.PENY_CWS,R.PENY_SEW,R.INCLUDE_PENY
		INTO L_DEBT_CWS,L_DEBT_SEW,L_PENY_CWS,L_PENY_SEW,L_INC_PENY;
		
		L_DEBT := NVL(L_DEBT_CWS,0)+NVL(L_DEBT_SEW,0);
		--ЕСЛИ ВКЛЮЧЕНА ПЕНИ, ТО ПРИБАВЛЯЕМ ЕЁ К ОБЩЕЙ СУММЕ ДОЛГА		
		IF L_INC_PENY = 'Y' THEN
			 L_DEBT := L_DEBT + NVL(L_PENY_CWS,0)+NVL(L_PENY_SEW,0);
		END IF;
		
		--ПОДСЧЕТ КОЛИЧЕСТВА ПЛАТЕЖЕЙ ДЛЯ ВЫПЛАТЫ СУММЫ ДОЛГА
    L_CNT :=  CEIL(L_DEBT/P_SUM_PAY);
		SELECT CASE
						WHEN P_SUM_PAY * (L_DEBT_CWS/L_DEBT) > L_DEBT_CWS - P_SUM_PAY * (L_DEBT_CWS/L_DEBT) * (ROW_NUMBER() OVER(ORDER BY T.PERIOD)-1)  THEN  
							L_DEBT_CWS - P_SUM_PAY * (L_DEBT_CWS/L_DEBT) * (ROW_NUMBER() OVER(ORDER BY T.PERIOD)-1)
						ELSE P_SUM_PAY * (L_DEBT_CWS/L_DEBT)
					 END PAY_CWS,
					 CASE
						WHEN P_SUM_PAY * (L_DEBT_SEW/L_DEBT) > L_DEBT_SEW - P_SUM_PAY * (L_DEBT_SEW/L_DEBT) * (ROW_NUMBER() OVER(ORDER BY T.PERIOD)-1)  THEN  
							L_DEBT_SEW - P_SUM_PAY * (L_DEBT_SEW/L_DEBT) * (ROW_NUMBER() OVER(ORDER BY T.PERIOD)-1)
						ELSE P_SUM_PAY * (L_DEBT_SEW/L_DEBT)
					 END PAY_SEW,
					 CASE
						WHEN P_SUM_PAY * (L_PENY_CWS/L_DEBT) > L_PENY_CWS - P_SUM_PAY * (L_PENY_CWS/L_DEBT) * (ROW_NUMBER() OVER(ORDER BY T.PERIOD)-1)  THEN  
							L_PENY_CWS - P_SUM_PAY * (L_PENY_CWS/L_DEBT) * (ROW_NUMBER() OVER(ORDER BY T.PERIOD)-1)
						ELSE P_SUM_PAY * (L_PENY_CWS/L_DEBT)
					 END PENY_CWS,
					 CASE
						WHEN P_SUM_PAY * (L_PENY_SEW/L_DEBT) > L_PENY_SEW - P_SUM_PAY * (L_PENY_SEW/L_DEBT) * (ROW_NUMBER() OVER(ORDER BY T.PERIOD)-1)  THEN  
							L_PENY_SEW - P_SUM_PAY * (L_PENY_SEW/L_DEBT) * (ROW_NUMBER() OVER(ORDER BY T.PERIOD)-1)
						ELSE P_SUM_PAY * (L_PENY_SEW/L_DEBT)
					 END PENY_SEW,
					 T.PERIOD
		BULK COLLECT INTO L_CALC			 
		FROM   (SELECT START_DATE + LEVEL - 1 PERIOD
						FROM   (SELECT P_DATE_START START_DATE
													,ADD_MONTHS(P_DATE_START,L_CNT)   END_DATE
										FROM   DUAL)
						CONNECT BY LEVEL <=	 TRUNC(END_DATE) - TRUNC(START_DATE)) T
		WHERE  EXTRACT(DAY FROM PERIOD) = CASE
																			 WHEN P_PAY_TO >= 28 AND EXTRACT(MONTH FROM PERIOD) = 2 THEN EXTRACT(DAY FROM LAST_DAY(PERIOD))
																			 WHEN P_PAY_TO = 31 AND  EXTRACT(DAY FROM LAST_DAY(PERIOD)) = 30 THEN 30
																			 ELSE P_PAY_TO
																			END;
																			
		FORALL I IN 1..L_CALC.COUNT
			INSERT INTO T_CLI_PEACE_AGREE_PAYMENTS(ID,ID_EXEC,PAY_DATE,SUM_CWS,PENY_CWS,SUM_SEW,PENY_SEW)
			VALUES(SEQ_REST_PAY.NEXTVAL,P_ID_EXEC,L_CALC(I).DATE_CALC,L_CALC(I).PAY_CWS,
			       L_CALC(I).PENY_CWS,L_CALC(I).PAY_SEW,L_CALC(I).PENY_SEW);
		                                   
		UPDATE T_CLI_PEACE_AGREE R
		   SET R.DATE_TO = (SELECT MAX(P.PAY_DATE) FROM T_CLI_PEACE_AGREE_PAYMENTS P WHERE P.ID_EXEC = R.ID_EXEC)
		 WHERE R.ID_EXEC = P_ID_EXEC;	    
		 
  END;																										 

----------------------------------------------------------------------------------------------
	
	PROCEDURE P_CREATE_AGREE(P_ID_EXEC    NUMBER		                      
													,P_START_DATE DATE  												
													,P_TYPE_CALC  VARCHAR2   													
													,P_END_DATE   DATE DEFAULT NULL      
													,P_MONTH_PAY  NUMBER DEFAULT NULL
													,P_PAY_DAY    NUMBER DEFAULT NULL
													,P_INC_PENY   VARCHAR2)
	 IS                      
	 L_ID_CASE NUMBER;             
	 L_ID_CWS  NUMBER;
	 L_ID_SEW  NUMBER;   
	 L_PENY_DATE DATE;
	 L_DEBT_CWS NUMBER;
	 L_PENY_CWS NUMBER;
	 L_DEBT_SEW NUMBER;
	 L_PENY_SEW NUMBER;
	BEGIN             
		SELECT S.ID_CASE, S.ID_CWS_CONTRACT, S.ID_SEW_CONTRACT, S.PENY_DATE
		INTO   L_ID_CASE, L_ID_CWS, L_ID_SEW, L_PENY_DATE
		FROM T_CLI_COURT_EXEC EX, T_CLI_CASES S
		WHERE EX.ID = P_ID_EXEC AND S.ID_CASE = EX.ID_CASE;
		
		IF L_ID_CWS IS NOT NULL THEN
			SELECT SUM(D.DEBT_CURRENT), ROUND(SUM(D.PENY),2) INTO L_DEBT_CWS, L_PENY_CWS 
			FROM T_CLI_DEBTS D WHERE D.ID_WORK = L_ID_CASE AND D.ID_CONTRACT = L_ID_CWS;
	  END IF;
		
		IF L_ID_SEW IS NOT NULL THEN
			SELECT SUM(D.DEBT_CURRENT), ROUND(SUM(D.PENY),2) INTO L_DEBT_SEW, L_PENY_SEW
			FROM T_CLI_DEBTS D WHERE D.ID_WORK = L_ID_CASE AND D.ID_CONTRACT = L_ID_SEW;
	  END IF;
		  	
		INSERT INTO T_CLI_PEACE_AGREE(ID_EXEC,DATE_FROM,DATE_TO,MONTH_PAY,PAY_DAY,TYPE_CALC,PENY_DATE,CODE_STATUS
		                             ,INCLUDE_PENY,DEBT_CWS,DEBT_SEW,PENY_CWS,PENY_SEW)
    VALUES(P_ID_EXEC,P_START_DATE,P_END_DATE,P_MONTH_PAY,P_PAY_DAY,P_TYPE_CALC,L_PENY_DATE,'PROJECT'
		     ,P_INC_PENY,L_DEBT_CWS,L_DEBT_SEW,L_PENY_CWS,L_PENY_SEW);
	END;                                         
	
-------------------------------------------------------------------------------------------	
	
	PROCEDURE P_RECALC_PAYMENT(P_ID_PAYMENT NUMBER,
		                         P_SUM_PAY    NUMBER,
														 P_PAY_DATE   DATE)
	  IS                               
		L_ID_EXEC NUMBER;
		L_PAYMENT NUMBER;
	BEGIN              
		
	 /*
		UPDATE T_CLI_PEACE_AGREE_PAYMENTS S
		   SET S.PAY_SUM = NVL(P_SUM_PAY,S.PAY_SUM)
			    ,S.PAY_DATE = NVL(P_PAY_DATE,S.PAY_DATE)
					,S.IS_MANUAL = 'Y'
		 WHERE S.ID = P_ID_PAYMENT
		 RETURNING S.ID_EXEC INTO L_ID_EXEC;  			  				
		 
		SELECT R.SUM_DEBT 
		       - 
					 (SELECT SUM(P.PAY_SUM) FROM T_CLI_PEACE_AGREE_PAYMENTS P 
					  WHERE P.IS_MANUAL = 'Y' AND P.ID_EXEC = R.ID_EXEC)
	  INTO L_PAYMENT
	  FROM T_CLI_PEACE_AGREE R WHERE R.ID_EXEC = L_ID_EXEC;
		
		FOR I IN (SELECT  S.ID,
											FLOOR(L_PAYMENT / (COUNT(*) OVER()) * 100) / 100 + CASE
												 WHEN ROW_NUMBER() OVER(ORDER BY S.ID) <=
															(L_PAYMENT - (FLOOR(L_PAYMENT / (COUNT(*) OVER()) * 100) / 100) *
															 (COUNT(*) OVER())) / 0.01 THEN		0.01
												 ELSE		0
									     END PAYMENT   --РАСЧЕТ СУММЫ ПЛАТЕЖА, МАХИНАЦИИ С КОПЕЙКАМИ
						  FROM T_CLI_PEACE_AGREE_PAYMENTS  S 
						  WHERE S.ID_EXEC = L_ID_EXEC
						  AND S.IS_MANUAL = 'N')
    LOOP
      UPDATE T_CLI_PEACE_AGREE_PAYMENTS P 
			   SET P.PAY_SUM = I.PAYMENT
			 WHERE P.ID = I.ID;
    END LOOP;                         
		
		P_DISTRIBUTE_PENY(L_ID_EXEC);
		P_CREATE_PEACE_PAY_DET(L_ID_EXEC);		*/
		NULL;
	END;             

---------------------------------------------------------------------------------------------
	
 /*PROCEDURE P_DISTRIBUTE_PENY(P_ID_EXEC NUMBER)
		IS
	BEGIN                                          
		FOR I IN (SELECT FLOOR(A.PENY_SUM / (COUNT(*) OVER()) * 100) / 100 + CASE
										 WHEN ROW_NUMBER() OVER(ORDER BY P.ID) <=
													(A.PENY_SUM - (FLOOR(A.PENY_SUM / (COUNT(*) OVER()) * 100) / 100) *
													 (COUNT(*) OVER())) / 0.01 THEN		0.01
										 ELSE		0
									 END PAYMENT,   --РАСЧЕТ СУММЫ ПЛАТЕЖА, МАХИНАЦИИ С КОПЕЙКАМИ
									 P.ID
						 FROM T_CLI_PEACE_AGREE_PAYMENTS P,
									T_CLI_PEACE_AGREE A
						 WHERE A.ID_EXEC = P_ID_EXEC			 
						 AND   A.ID_EXEC = P.ID_EXEC)
		 LOOP
			 UPDATE T_CLI_PEACE_AGREE_PAYMENTS P SET P.PENY_SUM = I.PAYMENT WHERE P.ID = I.ID;
		 END LOOP; 		
	END;*/                                                                                       
	
---------------------------------------------------------------------------------------------	

	/*PROCEDURE P_CALC_PENY(P_ID_EXEC NUMBER)											               	 
		 IS    
		 l_group_code VARCHAR2(50);               		
		 l_date_end     DATE;
		 l_prc_refin    NUMBER; -- процент ставки рефинансирования ЦБ 
		 l_id_refin     NUMBER; -- ид ставки      																
		 L_PENY         NUMBER := 0;
	BEGIN                
		--Достаём группу потребителей
		SELECT S.GROUP_CODE, A.DATE_TO
		INTO  l_group_code,	l_date_end
		FROM  T_CLI_COURT_EXEC R, 
		      T_CLI_PEACE_AGREE  A,
		      V_CLI_CASES S
		WHERE R.ID = P_ID_EXEC
		AND   R.ID = A.ID_EXEC
		AND   S.ID_CASE = R.ID_CASE;			
				
		--ТЕКУЩАЯ СТАВКА РЕФЕНАНСИРОВАНИЯ НА ДЕНЬ РАСЧЕТА
		SELECT R.ID_REFIN, R.PRC_REF INTO L_ID_REFIN,l_prc_refin
		FROM T_REFIN R WHERE l_date_end BETWEEN  R.DATE_REF_BEG AND NVL(R.DATE_REF_END,l_date_end);

		
		--ДЛЯ КАЖДОГО НАЧИСЛЕНИЯ ПО ДЕЛУ

   	FOR I IN (SELECT DET.SUM_PAY, P.PAY_DATE,                         			
			               LAST_DAY(FD.DOC_DATE)+ 
										 CASE 
										    WHEN L_GROUP_CODE IN ('UK','TSG') THEN 16 
												ELSE 11 
										 END DEBT_DATE,
										 DET.ID
	  						FROM T_CLI_PEACE_AGREE_PAYMENTS P
										,T_CLI_AGREE_PAY_DET DET
										,T_CLI_DEBTS RD 
										,T_CLI_FINDOCS    FD                 
						  WHERE P.ID = DET.ID_PAY
						  AND   RD.ID_DEBT = DET.ID_DEBT
						  AND   RD.ID_DOC = FD.ID_DOC
							AND   P.ID_EXEC = P_ID_EXEC)
		LOOP					  
			L_PENY := 0;
			 FOR J IN (SELECT PR.DAYS_FROM, PR.DAYS_TO, PR.RATE
								 FROM T_REFIN R, T_PENY_RATE PR 
								 WHERE R.ID_REFIN = L_ID_REFIN
								 AND PR.ID_REFIN = R.ID_REFIN        
								 AND  PR.RATE != 0
								 AND  PR.ABON_GROUP = L_GROUP_CODE
								 AND  PR.DAYS_FROM <= ( I.PAY_DATE - I.DEBT_DATE   +1))
			 LOOP
			 	 L_PENY := L_PENY + I.SUM_PAY/J.RATE*(L_PRC_REFIN/100) * (NVL(J.DAYS_TO,I.PAY_DATE - I.DEBT_DATE+1)-J.DAYS_FROM + 1);						 
			 END LOOP; 		   
			UPDATE T_CLI_AGREE_PAY_DET DET SET DET.PENY_SUM = L_PENY WHERE DET.ID = I.ID;
		 END LOOP;			
		 
		 UPDATE T_CLI_PEACE_AGREE_PAYMENTS p 
		    SET p.peny_sum = (SELECT SUM(DET.PENY_SUM) FROM T_CLI_AGREE_PAY_DET DET WHERE DET.ID_PAY = P.ID)
		  WHERE P.ID_EXEC = P_ID_EXEC;
		 
		 UPDATE T_CLI_PEACE_AGREE R 
		    SET R.PENY_SUM = (SELECT SUM(d.peny_sum) FROM T_CLI_PEACE_AGREE_PAYMENTS d WHERE d.id_exec = R.ID_EXEC)
		  WHERE R.ID_EXEC = P_ID_EXEC;		 
	END P_CALC_PENY;	*/
	
---------------------------------------------------------------------------------------	
	
  PROCEDURE P_CHANGE_STATUS(P_ID_EXEC NUMBER,
		                        P_STATUS  VARCHAR2)
		IS
	BEGIN
		UPDATE T_CLI_PEACE_AGREE A SET A.CODE_STATUS = P_STATUS WHERE A.ID_EXEC = P_ID_EXEC;
	END;												
		
	
END PKG_PEACE_AGREE;
/

