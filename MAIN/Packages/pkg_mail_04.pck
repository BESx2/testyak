i�?CREATE OR REPLACE PACKAGE LAWMAIN.PKG_MAIL_04
IS
/* Автор: Бородулин Е.С.
   Дата:  21.12.2016
	 Назначение: Пакет отсылает email уведомление для судебного отдела
*/

   PROCEDURE p_send_mail(P_ID_CLI_CASE IN NUMBER, P_EMAIL VARCHAR2, P_ATTACH VARCHAR2 DEFAULT NULL);

END;
/

CREATE OR REPLACE PACKAGE BODY LAWMAIN.PKG_MAIL_04
IS

	G$HTML VARCHAR2(32000);

  PROCEDURE p_send_mail(P_ID_CLI_CASE IN NUMBER, P_EMAIL VARCHAR2, P_ATTACH VARCHAR2)
        IS
			L_RET    NUMBER;
			L_REP    LAWSUP.PKG_FILES.REC_DOC;
    BEGIN                                                        
			IF p_email IS NULL THEN 
				raise_application_error(-20200,'email не указан');
		  END IF;                
			
				G$HTML := PKG_PREF.F$HTML('COURT_EMAIL_NOTIF');
				FOR i IN (SELECT CASE 
					                 WHEN S.CWS_ID_CONTRACT IS NOT NULL AND S.SEW_ID_CONTRACT IS NOT NULL THEN
					                    S.CWS_CTR_NUM||' и '||S.SEW_CTR_NUM
													 WHEN S.CWS_ID_CONTRACT IS NOT NULL THEN
															S.CWS_CTR_NUM
													 WHEN S.SEW_CTR_NUM IS NOT NULL THEN
															S.SEW_CTR_NUM
													 END CTR_NUMBER,
					               (SELECT MAX(EV.DATE_EVENT) 
												  FROM V_CLI_EVENTS EV 
												  WHERE EV.ID_WORK = S.ID_CASE 
												  AND EV.code_event = 'IN_COURT') DATE_EVENT													
					        FROM V_CLI_CASES S
					        WHERE S.ID_CASE = P_ID_CLI_CASE)
            LOOP                      
							IF I.DATE_EVENT IS NULL THEN
								RAISE_APPLICATION_ERROR(-20200,'Нельзя отправить сообщению по делу, по которому не подавалось исковое заявление');
							END IF;
							G$HTML := REPLACE(G$HTML,'#CTRNUMBER#',I.Ctr_Number); 
							G$HTML := REPLACE(G$HTML,'#COURTDATE#',TO_CHAR(I.DATE_EVENT,'DD.MM.YYYY')); -- текущая дата.
            END LOOP;
           
            L_RET := PKG_MAIL.F_MAKE_EMAIL(P_HEADER => PKG_PREF.F$C1('COURT_EMAIL_NOTIF')
                                           ,P_FROM => PKG_PREF.F$C1('USER_MAIL') -- do not change !!!! if change => change password in sup.pkg_mail!!!
                                           ,P_BODY => G$HTML
                                          );
            PKG_MAIL.P_ATTACHE_EMAIL_ADDR(P_EMAIL      => P_EMAIL
                                         ,P_ID_MAIL    => L_RET
                                         ,P_EMAIL_TYPE => PKG_MAIL.LIST_EMAIL_TYPE.TO_);
																				 
				FOR I IN (SELECT COLUMN_VALUE FROM TABLE(APEX_STRING.split(P_ATTACH,':')))																 
				LOOP
						L_REP := PKG_CONTROLLER.P_RETURN_BLOB(P_REP_CODE => I.COLUMN_VALUE
						                                     ,P_PAR_01 => P_ID_CLI_CASE);    
																								 
						PKG_MAIL.P_ATTACHE_FILE(P_ID_MAIL => L_RET
						                       ,P_BLOB => L_REP.P_BLOB
																	 ,P_FILE_NAME => L_REP.P_FILE_NAME);																		 
				END LOOP;

        PKG_MAIL.P_SEND(P_ID => L_RET,P_SEND_TYPE => PKG_MAIL.LIST_TYPE_SEND.LTE);
				PKG_SEND_MAIL.P_INSERT_CLI_CASE_MAIL(P_ID_CLI_CASE,L_RET,'COURT_EMAIL_NOTIF' );
 				COMMIT;
    END;
    
		
END;
/

