i�?CREATE OR REPLACE PACKAGE LAWMAIN.PKG_MAIL_01
IS
  
  PROCEDURE p_send_password(P_ID_USER IN NUMBER);
END;
/

CREATE OR REPLACE PACKAGE BODY LAWMAIN.PKG_MAIL_01
IS

	G$HTML VARCHAR2(32000);

  PROCEDURE p_send_password(P_ID_USER IN NUMBER)
        IS
			L_RET                                     NUMBER;
			L_STR VARCHAR2(32767);
			L_HEADER     VARCHAR2(500);
			PRAGMA AUTONOMOUS_TRANSACTION;
			L_eMAIL VARCHAR2(4000);
    BEGIN
				G$HTML := PKG_PREF.F$HTML('PASS_MAIL_TEMP');
				FOR i IN (SELECT x.*
										 FROM t_USER_LIST  x WHERE id = P_ID_USER
										)
            LOOP
							G$HTML := REPLACE(G$HTML,'#COMPANY_LOGO#',PKG_PREF.F$C1('LOGO'));
							G$HTML := REPLACE(G$HTML,'#LK_URL#',PKG_PREF.F$C1('LK_URL'));
							G$HTML := REPLACE(G$HTML,'#EMAIL_SUPP#',PKG_PREF.F$C1('SUPPORT_EMAIL'));
							G$HTML := REPLACE(G$HTML,'#USER_LAST#',Initcap(i.first_name));
							G$HTML := REPLACE(G$HTML,'#USERNAME#',lower(i.username));
							G$HTML := REPLACE(G$HTML,'#PASS#',lower(i.password));
							G$HTML := REPLACE(G$HTML,'#EMAIL_ID#',round(dbms_random.value(1111111,9999999)));
							G$HTML := REPLACE(G$HTML,'#PHONE1#',PKG_PREF.F$C1('PHONE'));
							G$HTML := REPLACE(G$HTML,'#ENVIRONMENT#',PKG_PREF.F$C1('ENVIRONMENT'));
							G$HTML := REPLACE(G$HTML,'#APP_NAME#',PKG_PREF.F$C1('APP_NAME'));
							L_eMAIL := I.EMAIL;
            END LOOP;

            IF L_eMAIL IS NULL THEN
							dbms_output.put_line('sdsds:'||p_ID_user);
							RETURN;
            END IF;

            L_RET := PKG_MAIL.F_MAKE_EMAIL(P_HEADER => PKG_PREF.F$C1('PASS_MAIL_TEMP')
                                           ,P_FROM => PKG_PREF.F$C1('USER_MAIL') -- do not change !!!! if change => change password in sup.pkg_mail!!!
                                           ,P_BODY =>  pkg_pref.F$HTML_H('PASS_MAIL_TEMP')|| G$HTML||pkg_pref.F$HTML_F('PASS_MAIL_TEMP')
                                          );
            -- кому
            PKG_MAIL.P_ATTACHE_EMAIL_ADDR(    P_EMAIL         => L_EMAIL
                                             ,P_ID_MAIL      => L_RET
                                             ,P_EMAIL_TYPE   => PKG_MAIL.LIST_EMAIL_TYPE.TO_
                                        );

        PKG_MAIL.P_SEND(P_ID => L_RET,P_SEND_TYPE => PKG_MAIL.LIST_TYPE_SEND.LTE);
 				commit;
    END;

END;
/

