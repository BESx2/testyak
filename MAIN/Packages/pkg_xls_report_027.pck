i�?create or replace package lawmain.PKG_XLS_REPORT_027 is

  -- Author  : BES
  -- Created : 06.03.2020
  -- Purpose : Расчет задолженности
	-- Code:    RECON_ACT
	
  FUNCTION RUN_REPORT(P_PAR_01 VARCHAR2 DEFAULT NULL
											,P_PAR_02 VARCHAR2 DEFAULT NULL
											,P_PAR_03 VARCHAR2 DEFAULT NULL
											,P_PAR_04 VARCHAR2 DEFAULT NULL
											,P_PAR_05 VARCHAR2 DEFAULT NULL
											,P_PAR_06 VARCHAR2 DEFAULT NULL
											,P_PAR_07 VARCHAR2 DEFAULT NULL
											,P_PAR_08 VARCHAR2 DEFAULT NULL
											,P_PAR_09 VARCHAR2 DEFAULT NULL
											,P_PAR_10 VARCHAR2 DEFAULT NULL) RETURN LAWSUP.PKG_FILES.REC_DOC;	  

end PKG_XLS_REPORT_027;
/

create or replace package body lawmain.PKG_XLS_REPORT_027 is

	TYPE REC_DOC IS RECORD(	P_BLOB BLOB DEFAULT NULL );

	EXCEL_DOC REC_DOC := NULL;  
	
	TYPE R_D_T IS RECORD(CLI_ALT_NAME VARCHAR2(4000),
	                   SALDO        VARCHAR2(500),
										 CTR_TYPE     VARCHAR2(500),
										 CTR_NUMBER   VARCHAR2(500),
										 CUR_DATE     VARCHAR2(100),
										 CUR_USER     VARCHAR2(500),
										 DIRECTOR     VARCHAR2(500),
										 ORG_INN      VARCHAR2(100),
										 ORD_ADDRESS  VARCHAR2(4000),
										 ORG_BNK      VARCHAR2(4000),
										 ORG_KS       VARCHAR2(100),
										 ORG_RS       VARCHAR2(100),
										 ORG_BIK      VARCHAR2(100),
										 CLI_REP      VARCHAR2(500),
										 CLI_INN      VARCHAR2(100),  
										 CLI_ADDRESS  VARCHAR2(4000),
										 CLI_BANK     VARCHAR2(4000),
										 CLI_RS       VARCHAR2(100),
										 CLI_KS       VARCHAR2(100),
										 CLI_BIK      VARCHAR2(100));
   R_D R_D_T;										 
										 
------------------------------------------------------------------------

------------------------------------------------------------------------

	PROCEDURE p$s( ps_value IN CLOB )  -- отправляет накопленное значение ps_value в буфер и записывает
	IS
		BUFFER    	RAW(32767);
    l_offset    NUMBER := 1;
    BUF_SIZE    NUMBER := 8000;
    BUF_VAR    VARCHAR2(32767);
  BEGIN
    LOOP
    EXIT WHEN L_OFFSET > DBMS_LOB.getlength(PS_VALUE);
    BUF_VAR := DBMS_LOB.substr(PS_VALUE,BUF_SIZE,L_OFFSET);
		BUFFER := UTL_RAW.cast_to_raw( CONVERT( BUF_VAR, 'UTF8'/*'CL8MSWIN1251'*/ ) );
		DBMS_LOB.WRITEAPPEND( EXCEL_DOC.P_BLOB, UTL_RAW.LENGTH( BUFFER ), BUFFER );
    L_OFFSET := L_OFFSET + BUF_SIZE;
    END LOOP;
  END;
------------------------------------------------------------------------

  PROCEDURE P_GET_DATA(P_DATE_FROM DATE, P_ID_CONTRACT NUMBER)
		IS                                                		
	BEGIN          
		FOR I IN (SELECT  T.CLI_ALT_NAME, 
							TO_CHAR(T.SALDO,'FM999999999999990D00','NLS_NUMERIC_CHARACTERS=''. ''') SALDO, 
							DECODE(T.CTR_TYPE,'Основной в ХВС','Водоснабжение','Водоотведение') CTR_TYPE, 
							T.CTR_NUMBER,               
							TO_CHAR(SYSDATE,'DD.MM.YYYY HH24:MI:SS') CUR_DATE,
							INITCAP(PKG_USERS.F_GET_FIO_BY_ID(F$_USR_ID,'Y')) CUR_USER,
							PKG_PREF.F$C2('ORG_MAIN_FIO') DIR,              
							PKG_PREF.F$C1('ORG_LEG_ADDRESS') ADDRESS,  
							PKG_PREF.F$C1('ORG_INN') ORG_INN,                
							PKG_PREF.F$C1('ORG_BANK') ORG_BNK,                  
							PKG_PREF.F$C4('ORG_BANK') ORG_BIK,                                
							PKG_PREF.F$C3('ORG_BANK') ORG_KS,                                              
							PKG_PREF.F$C2('ORG_BANK') ORG_RS,  
							(SELECT INITCAP(R.FIO) FROM T_CLI_REPRES R WHERE R.ID_REPRES = T.ID_REP) REPRES,              
							PKG_CLIENTS.F_GET_ADDRESS(T.ID_CLIENT) CLI_ADDRESS,                                                             
							T.INN,              
							BNK.BANK_NAME,
							BNK.BIK,              
							BNK.COR_ACC,                                          
							BNK.ACC_NUMBER                                                           
			 FROM (SELECT (SELECT MAX(R.ID_REPRES) FROM T_CLI_REPRES R 
										 WHERE LOWER(R.ROLE) IN ('директор','руководитель','генеральный директор')
										 AND R.ID_CLIENT = CLI.ID_CLIENT) ID_REP
										,(SELECT MAX(A.ID_ACC) FROM T_SETL_ACCOUNTS A WHERE A.ID_CLIENT = CLI.ID_CLIENT) ID_ACC  
										,CLI.INN                                                
										,(SELECT SUM(DECODE(FD.OPER_TYPE,'Приход',FD.AMOUNT,-FD.AMOUNT)) 
											FROM T_CLI_FINDOC_CONS FD
											WHERE FD.ID_CONTRACT = CT.ID_CONTRACT
											AND   FD.DOC_DATE < P_DATE_FROM
											AND   FD.DEBT_TYPE IN ('Полезный отпуск', 'Пени')) SALDO
										,CLI.CLI_ALT_NAME                                                                
										,CLI.ID_CLIENT
										,CT.CTR_NUMBER
										,CT.CTR_TYPE
						 FROM  T_CONTRACTS CT,
									 T_CLIENTS CLI								 
						 WHERE CT.ID_CONTRACT = P_ID_CONTRACT
						 AND   CT.ID_CLIENT = CLI.ID_CLIENT) T
					 ,(SELECT A.ACC_NUMBER, B.COR_ACC, B.BANK_NAME, B.BIK, A.ID_ACC
						 FROM T_SETL_ACCOUNTS A, T_BANKS B 
						 WHERE A.ID_BANK = B.ID_BANK) BNK
			 WHERE T.ID_ACC = BNK.ID_ACC(+))
			 LOOP
				  R_D.CLI_ALT_NAME := I.CLI_ALT_NAME;
					R_D.SALDO        := I.SALDO;
					R_D.CTR_TYPE     := I.CTR_TYPE;
					R_D.CTR_NUMBER   := I.CTR_NUMBER;
					R_D.CUR_DATE     := I.CUR_DATE;
					R_D.CUR_USER     := I.CUR_USER;
					R_D.DIRECTOR     := I.DIR;
					R_D.ORG_INN      := I.ORG_INN;
					R_D.ORD_ADDRESS  := I.ADDRESS;
					R_D.ORG_BNK      := I.ORG_BNK;
					R_D.ORG_KS       := I.ORG_KS;
					R_D.ORG_RS       := I.ORG_KS;
					R_D.ORG_BIK      := I.ORG_BIK;
					R_D.CLI_REP      := I.REPRES;
					R_D.CLI_INN      := I.INN;
					R_D.CLI_BANK     := I.BANK_NAME;
					R_D.CLI_RS       := I.ACC_NUMBER;
					R_D.CLI_KS       := I.COR_ACC;
					R_D.CLI_BIK      := I.BIK;    
					R_D.CLI_ADDRESS  := I.CLI_ADDRESS;
			 END LOOP;
	END;


	PROCEDURE P_ADD_DATA(P_DATE_START DATE, P_DATE_END DATE, P_ID_CONTRACT NUMBER) IS
	  L_XML CLOB;   
		L_CNT NUMBER := 0;
	  L_FMT VARCHAR2(100) := 'FM999999999999999990D00';
		L_NLS VARCHAR2(100) := 'NLS_NUMERIC_CHARACTERS=''. ''';  
	BEGIN              

		L_XML :=
			'<?xml version="1.0"?>
<?mso-application progid="Excel.Sheet"?>
<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:o="urn:schemas-microsoft-com:office:office"
 xmlns:x="urn:schemas-microsoft-com:office:excel"
 xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:html="http://www.w3.org/TR/REC-html40">
 <DocumentProperties xmlns="urn:schemas-microsoft-com:office:office">
  <Author></Author>
  <LastAuthor></LastAuthor>
  <Created>2020-04-20T06:36:24Z</Created>
  <LastSaved>2020-04-20T08:53:34Z</LastSaved>
  <Version>16.00</Version>
 </DocumentProperties>
 <OfficeDocumentSettings xmlns="urn:schemas-microsoft-com:office:office">
  <AllowPNG/>
 </OfficeDocumentSettings>
 <ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel">
  <WindowHeight>5895</WindowHeight>
  <WindowWidth>11400</WindowWidth>
  <WindowTopX>0</WindowTopX>
  <WindowTopY>0</WindowTopY>
  <TabRatio>135</TabRatio>
  <RefModeR1C1/>
  <ProtectStructure>False</ProtectStructure>
  <ProtectWindows>False</ProtectWindows>
 </ExcelWorkbook>
 <Styles>
  <Style ss:ID="Default" ss:Name="Normal">
   <Alignment ss:Vertical="Bottom"/>
   <Borders/>
   <Font ss:FontName="Arial" ss:Size="8"/>
   <Interior/>
   <NumberFormat/>
   <Protection/>
  </Style>
  <Style ss:ID="m397032528">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss"/>
  </Style>
  <Style ss:ID="m397032628">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss"/>
  </Style>
  <Style ss:ID="m397039360">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss"/>
  </Style>
  <Style ss:ID="m397039460">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss"/>
  </Style>
  <Style ss:ID="m397034968">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss"/>
  </Style>
  <Style ss:ID="m397035068">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss"/>
  </Style>
  <Style ss:ID="m273290336">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss"/>
  </Style>
  <Style ss:ID="m273290436">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss"/>
  </Style>
  <Style ss:ID="m273288872">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss"/>
  </Style>
  <Style ss:ID="m273288972">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss"/>
  </Style>
  <Style ss:ID="m273286432">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss"/>
  </Style>
  <Style ss:ID="m273286532">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss"/>
  </Style>
  <Style ss:ID="m273283504">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss"/>
  </Style>
  <Style ss:ID="m273283604">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss"/>
  </Style>
  <Style ss:ID="m273287408">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss"/>
  </Style>
  <Style ss:ID="m273287508">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss"/>
  </Style>
  <Style ss:ID="m273290824">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss"/>
  </Style>
  <Style ss:ID="m273290924">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss"/>
  </Style>
  <Style ss:ID="m273285944">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss"/>
  </Style>
  <Style ss:ID="m273286044">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss"/>
  </Style>
  <Style ss:ID="m273285456">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss"/>
  </Style>';
	P$S(L_XML);
	
	
	L_XML := '
  <Style ss:ID="m273285556">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss"/>
  </Style>
  <Style ss:ID="m273289848">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss"/>
  </Style>
  <Style ss:ID="m273289948">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss"/>
  </Style>
  <Style ss:ID="m273286920">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss"/>
  </Style>
  <Style ss:ID="m273287020">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss"/>
  </Style>
  <Style ss:ID="m273284480">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss"/>
  </Style>
  <Style ss:ID="m273284580">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss"/>
  </Style>
  <Style ss:ID="m273288384">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss"/>
  </Style>
  <Style ss:ID="m273288484">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss"/>
  </Style>
  <Style ss:ID="m273287896">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss"/>
  </Style>
  <Style ss:ID="m273287996">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss"/>
  </Style>
  <Style ss:ID="m273289360">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss"/>
  </Style>
  <Style ss:ID="m273289460">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss"/>
  </Style>
  <Style ss:ID="m273283992">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss"/>
  </Style>
  <Style ss:ID="m273284092">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss"/>
  </Style>
  <Style ss:ID="m273284968">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss"/>
  </Style>
  <Style ss:ID="m273285068">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss"/>
  </Style>
  <Style ss:ID="m402590952">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss"/>
  </Style>
  <Style ss:ID="m402591052">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss"/>
  </Style>
  <Style ss:ID="m402590464">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss"/>
  </Style>
  <Style ss:ID="m402590564">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss"/>
  </Style>
  <Style ss:ID="m402592944">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss"/>
  </Style>
  <Style ss:ID="m402593044">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss"/>
  </Style>
  <Style ss:ID="m402593452">
   <Alignment ss:Horizontal="Center" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss"/>
  </Style>
  <Style ss:ID="m402593472">
   <Alignment ss:Horizontal="Right" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss" ss:Bold="1"/>
  </Style>
  <Style ss:ID="m402593632">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss"/>
  </Style>
  <Style ss:ID="m402593732">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss"/>
  </Style>
  <Style ss:ID="m402593832">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss"/>
  </Style>
  <Style ss:ID="m402592436">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss" ss:Bold="1"/>
  </Style>
  <Style ss:ID="m402592456">
   <Alignment ss:Horizontal="Right" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss" ss:Bold="1"/>
   <NumberFormat ss:Format="Standard"/>
  </Style>
  <Style ss:ID="m402592576">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss" ss:Bold="1"/>
  </Style>
  <Style ss:ID="m402592596">
   <Alignment ss:Horizontal="Right" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss" ss:Bold="1"/>
   <NumberFormat ss:Format="Standard"/>
  </Style>
  <Style ss:ID="m402592656">
   <Alignment ss:Horizontal="Right" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss" ss:Bold="1"/>
   <NumberFormat ss:Format="Standard"/>
  </Style>
  <Style ss:ID="m402592736">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss"/>
  </Style>
  <Style ss:ID="m402592836">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss"/>
  </Style>
  <Style ss:ID="m402596848">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss" ss:Bold="1"/>
  </Style>
  <Style ss:ID="m402596988">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss" ss:Bold="1"/>
  </Style>
  <Style ss:ID="s16">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
  </Style>
  <Style ss:ID="s17">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Font ss:FontName="Arial" x:Family="Swiss"/>
  </Style>
  <Style ss:ID="s18">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss"/>
  </Style>
  <Style ss:ID="s19">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss"/>
  </Style>
  <Style ss:ID="s21">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss"/>
  </Style>
  <Style ss:ID="s22">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss"/>
  </Style>
  <Style ss:ID="s23">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss"/>
  </Style>
  <Style ss:ID="s24">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss"/>
  </Style>
  <Style ss:ID="s26">
   <Alignment ss:Horizontal="Right" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss" ss:Bold="1"/>
  </Style>
  <Style ss:ID="s27">
   <Alignment ss:Horizontal="Right" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss" ss:Bold="1"/>
  </Style>';
	P$S(L_XML);
	L_XML := '
  <Style ss:ID="s28">
   <Alignment ss:Horizontal="Center" ss:Vertical="Bottom" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss" ss:Bold="1"/>
  </Style>
  <Style ss:ID="s29">
   <Alignment ss:Horizontal="Center" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss" ss:Bold="1"/>
  </Style>
  <Style ss:ID="s30">
   <Alignment ss:Horizontal="Right" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss"/>
  </Style>
  <Style ss:ID="s31">
   <Alignment ss:Horizontal="Right" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss"/>
  </Style>
  <Style ss:ID="s32">
   <Alignment ss:Horizontal="Right" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss" ss:Bold="1"/>
  </Style>
  <Style ss:ID="s33">
   <Alignment ss:Horizontal="Left" ss:Vertical="Top"/>
  </Style>
  <Style ss:ID="s34">
   <Alignment ss:Horizontal="Left" ss:Vertical="Top"/>
   <Borders>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#413003"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss" ss:Size="8" ss:Color="#413003"/>
   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s35">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Font ss:FontName="Arial" x:Family="Swiss" ss:Bold="1"/>
  </Style>
  <Style ss:ID="s37">
   <Alignment ss:Horizontal="Left" ss:Vertical="Top"/>
   <Borders/>
   <Font ss:FontName="Arial" x:Family="Swiss" ss:Size="8" ss:Color="#413003"/>
   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s38">
   <Alignment ss:Vertical="Top"/>
   <Borders>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss" ss:Size="8" ss:Color="#413003"/>
   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s39">
   <Alignment ss:Vertical="Top"/>
   <Borders/>
   <Font ss:FontName="Arial" x:Family="Swiss" ss:Size="8" ss:Color="#413003"/>
   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s76">
   <Alignment ss:Horizontal="Center" ss:Vertical="Bottom"/>
   <Font ss:FontName="Arial" x:Family="Swiss" ss:Size="12" ss:Bold="1"/>
  </Style>
  <Style ss:ID="s77">
   <Alignment ss:Horizontal="Center" ss:Vertical="Bottom"/>
   <Font ss:FontName="Arial" x:Family="Swiss"/>
  </Style>
  <Style ss:ID="s78">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom" ss:WrapText="1"/>
   <Font ss:FontName="Arial" x:Family="Swiss"/>
  </Style>
  <Style ss:ID="s79">
   <Alignment ss:Horizontal="Right" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss" ss:Bold="1"/>
  </Style>
  <Style ss:ID="s83">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss" ss:Bold="1"/>
   <NumberFormat ss:Format="Standard"/>
  </Style>
  <Style ss:ID="s84">
   <Alignment ss:Horizontal="Center" ss:Vertical="Bottom" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss" ss:Bold="1"/>
   <NumberFormat ss:Format="Standard"/>
  </Style>
  <Style ss:ID="s85">
   <Alignment ss:Horizontal="Center" ss:Vertical="Bottom" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss" ss:Bold="1"/>
   <NumberFormat ss:Format="Standard"/>
  </Style>
  <Style ss:ID="s86">
   <Alignment ss:Horizontal="Center" ss:Vertical="Bottom" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss" ss:Bold="1"/>
  </Style>
  <Style ss:ID="s87">
   <Alignment ss:Horizontal="Right" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss" ss:Bold="1"/>
   <NumberFormat ss:Format="Standard"/>
  </Style>
  <Style ss:ID="s89">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Font ss:FontName="Arial" x:CharSet="204" x:Family="Swiss"/>
  </Style>
  <Style ss:ID="s94">
   <Alignment ss:Horizontal="Right" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss"/>
   <NumberFormat ss:Format="Standard"/>
  </Style>
  <Style ss:ID="s95">
   <Alignment ss:Horizontal="Right" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss"/>
   <NumberFormat ss:Format="Standard"/>
  </Style>
  <Style ss:ID="s96">
   <Alignment ss:Horizontal="Right" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss"/>
  </Style>
 </Styles>
 <Worksheet ss:Name="TDSheet">
  <Table>
   <Column ss:StyleID="s16" ss:AutoFitWidth="0" ss:Width="10.5"/>
   <Column ss:StyleID="s16" ss:AutoFitWidth="0" ss:Width="0.75"/>
   <Column ss:StyleID="s16" ss:AutoFitWidth="0" ss:Width="50.25" ss:Span="1"/>
   <Column ss:Index="5" ss:StyleID="s16" ss:AutoFitWidth="0" ss:Width="66"/>
   <Column ss:StyleID="s16" ss:AutoFitWidth="0" ss:Width="27"/>
   <Column ss:StyleID="s16" ss:AutoFitWidth="0" ss:Width="39.75"/>
   <Column ss:StyleID="s16" ss:AutoFitWidth="0" ss:Width="50.25" ss:Span="1"/>
   <Column ss:Index="10" ss:StyleID="s16" ss:AutoFitWidth="0" ss:Width="39.75"/>
   <Column ss:StyleID="s16" ss:AutoFitWidth="0" ss:Width="63"/>
   <Column ss:StyleID="s16" ss:AutoFitWidth="0" ss:Width="25.5"/>
   <Column ss:StyleID="s16" ss:AutoFitWidth="0" ss:Width="96"/>
   <Column ss:StyleID="s16" ss:AutoFitWidth="0" ss:Width="0.75"/>
   <Row ss:AutoFitHeight="0"/>
   <Row ss:AutoFitHeight="0" ss:Height="15.9375">
    <Cell ss:Index="3" ss:MergeAcross="11" ss:StyleID="s76"><Data ss:Type="String">АКТ</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="12" ss:StyleID="s16">
    <Cell ss:Index="3" ss:MergeAcross="10" ss:StyleID="s77"><Data ss:Type="String">сверки задолженности</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="12.9375">
    <Cell ss:Index="3" ss:MergeAcross="11" ss:StyleID="s77"><Data ss:Type="String">за период с '||TO_CHAR(P_DATE_START,'DD.MM.YYYY')||
		' по '||TO_CHAR(P_DATE_END,'DD.MM.YYYY')||'</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="12.9375">
    <Cell ss:Index="3" ss:StyleID="s17"><Data ss:Type="String">Настоящим Актом сверки взаиморасчетов с потребителем</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="12.9375">
    <Cell ss:Index="3" ss:MergeAcross="10" ss:StyleID="s78"><Data ss:Type="String">'||R_D.CLI_ALT_NAME||'</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="12.9375">
    <Cell ss:Index="3" ss:MergeAcross="5" ss:StyleID="s17"><Data ss:Type="String">Тип договора: '||R_D.CTR_TYPE||'</Data></Cell>
    <Cell ss:MergeAcross="5" ss:StyleID="s17"><Data ss:Type="String">№ договора: '||R_D.CTR_NUMBER||'</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="12.9375" ss:Span="1"/>
   <Row ss:Index="10" ss:AutoFitHeight="0" ss:Height="12.9375">
    <Cell ss:Index="3" ss:MergeAcross="4" ss:StyleID="s17"><Data ss:Type="String">Момент печати: '||R_D.CUR_DATE||'</Data></Cell>
    <Cell ss:MergeAcross="6" ss:StyleID="s17"><Data ss:Type="String">Пользователь: '||R_D.CUR_USER||'</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="12.9375">
    <Cell ss:StyleID="s17"/>
    <Cell ss:StyleID="s17"/>
    <Cell ss:StyleID="s18"/>
    <Cell ss:StyleID="s19"/>
    <Cell ss:MergeAcross="5" ss:MergeDown="2" ss:StyleID="m402596988"><Data
      ss:Type="String">По данным АО &quot;Водоканал&quot;</Data></Cell>
    <Cell ss:MergeAcross="2" ss:MergeDown="2" ss:StyleID="m402596848"><Data
      ss:Type="String">По данным '||R_D.CLI_ALT_NAME||'</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="12.9375">
    <Cell ss:StyleID="s17"/>
    <Cell ss:StyleID="s17"/>
    <Cell ss:StyleID="s21"/>
    <Cell ss:StyleID="s22"/>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="12.9375">
    <Cell ss:StyleID="s17"/>
    <Cell ss:StyleID="s17"/>
    <Cell ss:StyleID="s23"/>
    <Cell ss:StyleID="s24"/>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="12.9375">
    <Cell ss:StyleID="s17"/>
    <Cell ss:StyleID="s17"/>
    <Cell ss:MergeAcross="3" ss:StyleID="m402592436"><Data ss:Type="String">Сальдо на начало периода</Data></Cell>
    <Cell ss:StyleID="s26"/>
    <Cell ss:StyleID="s26"/>
    <Cell ss:MergeAcross="1" ss:StyleID="m402592456"><Data ss:Type="Number">'||R_D.SALDO||'</Data></Cell>
    <Cell ss:StyleID="s26"/>
    <Cell ss:MergeAcross="1" ss:StyleID="s27"/>
    <Cell ss:StyleID="s27"/>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="26.0625">
    <Cell ss:StyleID="s17"/>
    <Cell ss:StyleID="s17"/>
    <Cell ss:MergeAcross="1" ss:StyleID="m402593452"/>
    <Cell ss:MergeAcross="1" ss:StyleID="s83"><Data ss:Type="String">Количество</Data></Cell>
    <Cell ss:MergeAcross="1" ss:StyleID="s84"><Data ss:Type="String">Подлежит оплате (руб.)</Data></Cell>
    <Cell ss:MergeAcross="1" ss:StyleID="s85"><Data ss:Type="String">Фактически оплачено (руб.)</Data></Cell>
    <Cell ss:MergeAcross="1" ss:StyleID="s86"><Data ss:Type="String">Подлежит оплате (руб.)</Data></Cell>
    <Cell ss:StyleID="s28"><Data ss:Type="String">Фактически оплачено (руб.)</Data></Cell>
    <Cell ss:StyleID="s29"/>
   </Row>';
   p$s(L_XML);
   
	 
 	 FOR I IN (SELECT T2.DOC_DATE, T1.VOLUME, T2.AMOUNT, T2.PAYED
						FROM   (SELECT TRUNC(FD.DOC_DATE, 'MM') DOC_DATE, SUM(D.VOLUME) VOLUME
										FROM   T_CLI_FINDOCS FD, T_CLI_FINDOCS_DETAILS D
										WHERE  FD.DOC_DATE BETWEEN P_DATE_START AND P_DATE_END
										AND    FD.EXT_ID = D.EXT_ID
										AND    FD.ID_CONTRACT = P_ID_CONTRACT
										AND    FD.OPER_TYPE = 'Приход'
										GROUP  BY TRUNC(FD.DOC_DATE, 'MM')) T1
									,(SELECT DOC_DATE, AMOUNT, PAYED
										FROM   (SELECT TRUNC(FD.DOC_DATE, 'MM') DOC_DATE
																	,FD.AMOUNT
																	,FD.OPER_TYPE
														FROM   (SELECT FDC.ID_CLIENT
																					,FDC.ID_CONTRACT
																					,FDC.EXT_ID_FROM
																					,FDC.DOC_DATE
																					,FDC.OPER_TYPE
																					,SUM(FDC.AMOUNT) AMOUNT
																		FROM   T_CLI_FINDOC_CONS FDC
																		WHERE  FDC.ID_CONTRACT = P_ID_CONTRACT
																		AND    FDC.DOC_DATE BETWEEN P_DATE_START AND P_DATE_END
																		AND    FDC.DEBT_TYPE IN ('Полезный отпуск', 'Пени')
																		GROUP  BY FDC.ID_CLIENT ,FDC.ID_CONTRACT,FDC.EXT_ID_FROM,FDC.DOC_DATE,FDC.OPER_TYPE) FD)
										PIVOT(SUM(AMOUNT)
										FOR    OPER_TYPE IN('Приход' AS "AMOUNT", 'Расход' AS "PAYED"))) T2
						WHERE  T2.DOC_DATE = T1.DOC_DATE(+)
						ORDER  BY T2.DOC_DATE)   
   LOOP    
		  L_CNT := L_CNT +1;
		  L_XML := '<Row ss:Height="12.75">'||CHR(13);
			L_XML := L_XML || '<Cell ss:StyleID="s17"/>'||CHR(13);
			L_XML := L_XML || '<Cell ss:StyleID="s17"/>'||CHR(13);
			L_XML := L_XML || '<Cell ss:MergeAcross="1" ss:StyleID="m402593632"><Data ss:Type="String">'||to_char(i.doc_date,'month YYYY')||'</Data></Cell>'||CHR(13);
			L_XML := L_XML || '<Cell ss:MergeAcross="1" ss:StyleID="s94"><Data ss:Type="Number">'||TO_CHAR(I.VOLUME,L_FMT,L_NLS)||'</Data></Cell>'||CHR(13);
			L_XML := L_XML || '<Cell ss:MergeAcross="1" ss:StyleID="s94"><Data ss:Type="Number">'||TO_CHAR(I.AMOUNT,L_FMT,L_NLS)||'</Data></Cell>'||CHR(13);
			L_XML := L_XML || '<Cell ss:MergeAcross="1" ss:StyleID="s95"><Data ss:Type="Number">'||TO_CHAR(I.PAYED,L_FMT,L_NLS)||'</Data></Cell>'||CHR(13);
			L_XML := L_XML || '<Cell ss:MergeAcross="1" ss:StyleID="s96"/>'||CHR(13);
			L_XML := L_XML || '<Cell ss:StyleID="s30"/>'||CHR(13);
			L_XML := L_XML || '<Cell ss:StyleID="s31"/>'||CHR(13);
			L_XML := L_XML || '</Row>'||CHR(13);
		  p$s(L_XML);
	 END LOOP;						
      
	 L_XML := '    
	  <Row ss:AutoFitHeight="0" ss:Height="12.9375">
	    <Cell ss:StyleID="s17"/>
    <Cell ss:StyleID="s17"/>
    <Cell ss:MergeAcross="1" ss:StyleID="m402593472"><Data ss:Type="String">ИТОГО</Data></Cell>
    <Cell ss:MergeAcross="1" ss:StyleID="s87" ss:Formula="=SUM(R[-'||L_CNT||']C:R[-1]C[1])"><Data
      ss:Type="Number">0</Data></Cell>
    <Cell ss:MergeAcross="1" ss:StyleID="s87" ss:Formula="=SUM(R[-'||L_CNT||']C:R[-1]C[1])"><Data
      ss:Type="Number">0</Data></Cell>
    <Cell ss:MergeAcross="1" ss:StyleID="m402592656"
     ss:Formula="=SUM(R[-'||L_CNT||']C:R[-1]C[1])"><Data ss:Type="Number">0</Data></Cell>
    <Cell ss:MergeAcross="1" ss:StyleID="s79"/>
    <Cell ss:StyleID="s32"/>
    <Cell ss:StyleID="s27"/>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="12.75">
    <Cell ss:StyleID="s17"/>
    <Cell ss:StyleID="s17"/>
    <Cell ss:MergeAcross="3" ss:StyleID="m402592576"><Data ss:Type="String">Сальдо на конец периода</Data></Cell>
    <Cell ss:StyleID="s26"/>
    <Cell ss:StyleID="s26"/>
    <Cell ss:MergeAcross="1" ss:StyleID="m402592596"
     ss:Formula="=R[-'||TO_CHAR(L_CNT+3)||']C+R[-1]C[-2]-R[-1]C"><Data ss:Type="Number">0</Data></Cell>
    <Cell ss:StyleID="s26"/>
    <Cell ss:StyleID="s26"/>
    <Cell ss:StyleID="s32"/>
    <Cell ss:StyleID="s27"/>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="11.0625" ss:StyleID="s33">
    <Cell ss:Index="2" ss:StyleID="s34"/>
    <Cell ss:StyleID="s38"/>
    <Cell ss:StyleID="s38"/>
    <Cell ss:StyleID="s38"/>
    <Cell ss:StyleID="s38"/>
    <Cell ss:StyleID="s38"/>
    <Cell ss:StyleID="s38"/>
    <Cell ss:StyleID="s38"/>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="11.0625" ss:StyleID="s33">
    <Cell ss:Index="2" ss:StyleID="s37"/>
    <Cell ss:StyleID="s39"/>
    <Cell ss:StyleID="s39"/>
    <Cell ss:StyleID="s39"/>
    <Cell ss:StyleID="s39"/>
    <Cell ss:StyleID="s39"/>
    <Cell ss:StyleID="s39"/>
    <Cell ss:StyleID="s39"/>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="12.9375">
    <Cell ss:Index="3" ss:StyleID="s35"><Data ss:Type="String">АО &quot;Водоканал&quot;</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="12.9375">
    <Cell ss:Index="3" ss:MergeAcross="11" ss:StyleID="s17"><Data ss:Type="String">Руководитель___________________('||R_D.DIRECTOR||')</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="12.9375"/>
   <Row ss:AutoFitHeight="0" ss:Height="12.9375">
    <Cell ss:Index="3" ss:MergeAcross="11" ss:StyleID="s17"><Data ss:Type="String">Гл. бухгалтер___________________ (Пинчук И.И.)</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="12.9375"/>
   <Row ss:AutoFitHeight="0" ss:Height="12.9375">
    <Cell ss:Index="3" ss:StyleID="s17"><Data ss:Type="String">Бухгалтер</Data></Cell>
    <Cell ss:StyleID="s17"><Data ss:Type="String">_____________________</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="12.9375"/>
   <Row ss:AutoFitHeight="0" ss:Height="12.9375">
    <Cell ss:Index="3" ss:MergeAcross="11" ss:StyleID="s35"><Data ss:Type="String">АО &quot;Водоканал&quot;</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="12.9375">
    <Cell ss:Index="3" ss:MergeAcross="8" ss:StyleID="s17"><Data ss:Type="String">Адрес: '||R_D.ORD_ADDRESS||'</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="12.9375">
    <Cell ss:Index="3" ss:MergeAcross="11" ss:StyleID="s17"><Data ss:Type="String">ИНН: '||R_D.ORG_INN||'</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="12.9375">
    <Cell ss:Index="3" ss:StyleID="s17"><Data ss:Type="String">Банковские реквизиты:</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="12.9375">
    <Cell ss:Index="3" ss:MergeAcross="11" ss:StyleID="s17"><Data ss:Type="String">'||R_D.ORG_BNK||'</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="12.9375">
    <Cell ss:Index="3" ss:MergeAcross="11" ss:StyleID="s17"><Data ss:Type="String">БИК: '||R_D.ORG_BIK||'</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="12.9375">
    <Cell ss:Index="3" ss:MergeAcross="11" ss:StyleID="s17"><Data ss:Type="String">Корр. счет: '||R_D.ORG_KS||'</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="12.9375">
    <Cell ss:Index="3" ss:MergeAcross="11" ss:StyleID="s17"><Data ss:Type="String">Расч. счет: '||R_D.ORG_RS||'</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="12.9375"/>
   <Row ss:AutoFitHeight="0" ss:Height="12.9375">
    <Cell ss:Index="3" ss:StyleID="s35"><Data ss:Type="String">Абонент</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="12.9375"/>
   <Row ss:AutoFitHeight="0" ss:Height="12.9375">
    <Cell ss:Index="3" ss:MergeAcross="11" ss:StyleID="s17"><Data ss:Type="String">Руководитель___________________('||R_D.CLI_REP||')</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="12.9375"/>
   <Row ss:AutoFitHeight="0" ss:Height="12.9375">
    <Cell ss:Index="3" ss:MergeAcross="11" ss:StyleID="s17"><Data ss:Type="String">Гл. бухгалтер___________________</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="12.9375"/>
   <Row ss:AutoFitHeight="0" ss:Height="12.9375">
    <Cell ss:Index="3" ss:MergeAcross="11" ss:StyleID="s35"><Data ss:Type="String">'||R_D.CLI_ALT_NAME||'</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="12.9375">
    <Cell ss:Index="3" ss:MergeAcross="11" ss:StyleID="s17"><Data ss:Type="String">Адрес: '||R_D.CLI_ADDRESS||'</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="12.9375">
    <Cell ss:Index="3" ss:MergeAcross="11" ss:StyleID="s17"><Data ss:Type="String">ИНН: '||R_D.CLI_INN||'</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="12.9375">
    <Cell ss:Index="3" ss:StyleID="s17"><Data ss:Type="String">Банковские реквизиты:</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="12.9375">
    <Cell ss:Index="3" ss:MergeAcross="10" ss:StyleID="s89"><Data ss:Type="String">'||R_D.CLI_BANK||'</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="12.9375">
    <Cell ss:Index="3" ss:MergeAcross="11" ss:StyleID="s17"><Data ss:Type="String">БИК: '||R_D.CLI_BIK||'</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="12.9375">
    <Cell ss:Index="3" ss:MergeAcross="11" ss:StyleID="s17"><Data ss:Type="String">Корр. счет: '||R_D.CLI_KS||'</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="12.9375">
    <Cell ss:Index="3" ss:MergeAcross="11" ss:StyleID="s17"><Data ss:Type="String">Расч. счет: '||R_D.CLI_RS||'</Data></Cell>
   </Row>
  </Table>
 </Worksheet>
</Workbook>';
  p$s( L_XML );
		
	
	
	END P_ADD_DATA;

---------------------------------------------------------------------------------------------------------------------------------

  FUNCTION RUN_REPORT(P_PAR_01 VARCHAR2 DEFAULT NULL
											,P_PAR_02 VARCHAR2 DEFAULT NULL
											,P_PAR_03 VARCHAR2 DEFAULT NULL
											,P_PAR_04 VARCHAR2 DEFAULT NULL
											,P_PAR_05 VARCHAR2 DEFAULT NULL
											,P_PAR_06 VARCHAR2 DEFAULT NULL
											,P_PAR_07 VARCHAR2 DEFAULT NULL
											,P_PAR_08 VARCHAR2 DEFAULT NULL
											,P_PAR_09 VARCHAR2 DEFAULT NULL
											,P_PAR_10 VARCHAR2 DEFAULT NULL) RETURN LAWSUP.PKG_FILES.REC_DOC
	  IS
	  l_BLOB LAWSUP.PKG_FILES.REC_DOC;
		L_DATE_BEG DATE;
		L_DATE_END DATE;
  BEGIN

	  EXCEL_DOC := NULL;
		DBMS_LOB.CREATETEMPORARY( EXCEL_DOC.P_BLOB, TRUE );
    DBMS_LOB.OPEN( EXCEL_DOC.P_BLOB, DBMS_LOB.LOB_READWRITE );
    
		L_DATE_BEG := NVL(TO_DATE(P_PAR_02, 'DD.MM.YYYY'), DATE '2000-01-01');
		L_DATE_END := NVL(TO_DATE(P_PAR_03, 'DD.MM.YYYY'), TRUNC(SYSDATE,'DD')) + 1 -
									(INTERVAL '1' SECOND);
	
		P_GET_DATA(L_DATE_BEG,TO_NUMBER(P_PAR_04));
		P_ADD_DATA(L_DATE_BEG,L_DATE_END,TO_NUMBER(P_PAR_04));

		DBMS_LOB.CLOSE(EXCEL_DOC.P_BLOB);

		l_BLOB.P_BLOB := EXCEL_DOC.P_BLOB;
		l_BLOB.P_FILE_NAME := 'Акт сверки';
    RETURN L_BLOB;

  END RUN_REPORT;

end PKG_XLS_REPORT_027;
/

