i�?create or replace package lawmain.PKG_XLS_REPORT_018 is

  -- Author  : BES
  -- Created : 17.02.2017
  -- Purpose : Отчет о произведенной работе
	-- Code:    REG_CRED_CLAIMS
	
  
  FUNCTION RUN_REPORT(P_PAR_01 VARCHAR2 DEFAULT NULL
											,P_PAR_02 VARCHAR2 DEFAULT NULL
											,P_PAR_03 VARCHAR2 DEFAULT NULL
											,P_PAR_04 VARCHAR2 DEFAULT NULL
											,P_PAR_05 VARCHAR2 DEFAULT NULL
											,P_PAR_06 VARCHAR2 DEFAULT NULL
											,P_PAR_07 VARCHAR2 DEFAULT NULL
											,P_PAR_08 VARCHAR2 DEFAULT NULL
											,P_PAR_09 VARCHAR2 DEFAULT NULL
											,P_PAR_10 VARCHAR2 DEFAULT NULL) RETURN LAWSUP.PKG_FILES.REC_DOC;	  

end PKG_XLS_REPORT_018;
/

create or replace package body lawmain.PKG_XLS_REPORT_018 is

	TYPE REC_DOC IS RECORD(	P_BLOB BLOB DEFAULT NULL );

	EXCEL_DOC REC_DOC := NULL;
------------------------------------------------------------------------

	PROCEDURE p$s( ps_value IN CLOB )  -- отправляет накопленное значение ps_value в буфер и записывает
	IS
		BUFFER    	RAW(32767);
    l_offset    NUMBER := 1;
    BUF_SIZE    NUMBER := 8000;
    BUF_VAR    VARCHAR2(32767);
  BEGIN
    LOOP
    EXIT WHEN L_OFFSET > DBMS_LOB.getlength(PS_VALUE);
    BUF_VAR := DBMS_LOB.substr(PS_VALUE,BUF_SIZE,L_OFFSET);
		BUFFER := UTL_RAW.cast_to_raw( CONVERT( BUF_VAR, 'UTF8'/*'CL8MSWIN1251'*/ ) );
		DBMS_LOB.WRITEAPPEND( EXCEL_DOC.P_BLOB, UTL_RAW.LENGTH( BUFFER ), BUFFER );
    L_OFFSET := L_OFFSET + BUF_SIZE;
    END LOOP;
  END;
------------------------------------------------------------------------
	PROCEDURE P_ADD_DATA(P_DATE_FROM DATE,P_DATE_TO DATE) IS 
	  L_XML CLOB;
		L_CNT NUMBER := 0;
		L_FMT VARCHAR2(50) := '9999999999999D00';
		L_NLS VARCHAR2(50) := 'NLS_NUMERIC_CHARACTERS=''. ''';		
		L_DATE_START DATE;
    L_DATE_END   DATE;     
		L_TYPE_WORK VARCHAR2(100);
		L_ID_CLI    NUMBER;        
		
		--ИТОГОВЫЕ СУММЫ В РАЗРЕЗЕ РАБОТЫ
		L_TOT_WORK_CNT NUMBER := 0;
		L_TOT_WORK_SUM NUMBER := 0;
		L_TOT_DOC_DEBT NUMBER := 0;
	  L_TOT_DOC_PAY  NUMBER := 0;
		L_TOT_DOC_KOR  NUMBER := 0;   
		L_TOT_DOC_KOR_DEBT NUMBER := 0;
		L_TOT_DEBT     NUMBER := 0;
		

		L_NEW_CLI  BOOLEAN DEFAULT TRUE;
		L_NEW_TYPE BOOLEAN DEFAULT TRUE;
		
		L_CNT_WORK    NUMBER;
		
	BEGIN       
		L_DATE_START := NVL(P_DATE_FROM,DATE '2000-01-01');
		L_DATE_END   := NVL(P_DATE_TO,SYSDATE);  
		
		L_XML := '<?xml version="1.0"?>
<?mso-application progid="Excel.Sheet"?>
<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:o="urn:schemas-microsoft-com:office:office"
 xmlns:x="urn:schemas-microsoft-com:office:excel"
 xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:html="http://www.w3.org/TR/REC-html40">
 <DocumentProperties xmlns="urn:schemas-microsoft-com:office:office">
  <Author>debit6</Author>
  <LastAuthor>Evgeniy Borodulin</LastAuthor>
  <LastPrinted>2018-01-10T12:44:17Z</LastPrinted>
  <Created>2013-04-04T05:56:29Z</Created>
  <LastSaved>2019-06-05T20:43:31Z</LastSaved>
  <Company>Reanimator Extreme Edition</Company>
  <Version>16.00</Version>
 </DocumentProperties>
 <OfficeDocumentSettings xmlns="urn:schemas-microsoft-com:office:office">
  <AllowPNG/>
 </OfficeDocumentSettings>
 <ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel">
  <WindowHeight>9480</WindowHeight>
  <WindowWidth>15180</WindowWidth>
  <WindowTopX>120</WindowTopX>
  <WindowTopY>1200</WindowTopY>
  <RefModeR1C1/>
  <ProtectStructure>False</ProtectStructure>
  <ProtectWindows>False</ProtectWindows>
 </ExcelWorkbook>
 <Styles>
  <Style ss:ID="Default" ss:Name="Normal">
   <Alignment ss:Vertical="Bottom"/>
   <Borders/>
   <Font ss:FontName="Calibri" x:CharSet="204" x:Family="Swiss" ss:Size="11"
    ss:Color="#000000"/>
   <Interior/>
   <NumberFormat/>
   <Protection/>
  </Style>
  <Style ss:ID="s16">
   <Alignment ss:Vertical="Bottom" ss:WrapText="1"/>
   <Font ss:FontName="Calibri" x:CharSet="204" x:Family="Swiss" ss:Size="14"
    ss:Color="#000000"/>
  </Style>
  <Style ss:ID="s17">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="14" ss:Color="#000000"/>
   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>
   <NumberFormat ss:Format="@"/>
  </Style>
  <Style ss:ID="s18">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="14" ss:Color="#000000"/>
   <NumberFormat ss:Format="#,##0.00;[Red]#,##0.00"/>
  </Style>
  <Style ss:ID="s19">
   <Alignment ss:Vertical="Bottom" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="14" ss:Color="#000000"/>
  </Style>
  <Style ss:ID="s20">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="14" ss:Color="#000000"/>
   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>
   <NumberFormat ss:Format="#,##0.00;[Red]#,##0.00"/>
  </Style>
  <Style ss:ID="s21">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="14" ss:Color="#000000"/>
  </Style>
  <Style ss:ID="s22">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="14" ss:Color="#000000"/>
   <NumberFormat ss:Format="Short Date"/>
  </Style>
  <Style ss:ID="s23">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="14" ss:Color="#000000"/>
   <NumberFormat ss:Format="Short Date"/>
  </Style>
  <Style ss:ID="s24">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="14" ss:Color="#000000"/>
   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s25">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="14" ss:Color="#000000"/>
   <NumberFormat ss:Format="#,##0.00;[Red]#,##0.00"/>
  </Style>
  <Style ss:ID="s26">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="12" ss:Color="#000000"/>
   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>
   <NumberFormat ss:Format="@"/>
  </Style>
  <Style ss:ID="s27">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="12" ss:Color="#000000"/>
  </Style>
  <Style ss:ID="s28">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="12" ss:Color="#000000"/>
   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>
   <NumberFormat ss:Format="#,##0.00;[Red]#,##0.00"/>
  </Style>
  <Style ss:ID="s29">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="12" ss:Color="#000000"/>
   <NumberFormat ss:Format="Short Date"/>
  </Style>
  <Style ss:ID="s30">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="12" ss:Color="#000000"/>
   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>
   <NumberFormat ss:Format="Short Date"/>
  </Style>
  <Style ss:ID="s35">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="14" ss:Color="#000000" ss:Bold="1"/>
   <Interior ss:Color="#FDE9D9" ss:Pattern="Solid"/>
   <NumberFormat ss:Format="@"/>
  </Style>
  <Style ss:ID="s36">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="14" ss:Color="#000000" ss:Bold="1"/>
   <Interior ss:Color="#FDE9D9" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s37">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="14" ss:Color="#000000" ss:Bold="1"/>
   <Interior ss:Color="#FDE9D9" ss:Pattern="Solid"/>
   <NumberFormat ss:Format="#,##0.00;[Red]#,##0.00"/>
  </Style>
  <Style ss:ID="s38">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="14" ss:Color="#000000" ss:Bold="1"/>
   <Interior ss:Color="#FDE9D9" ss:Pattern="Solid"/>
   <NumberFormat ss:Format="#,##0.00;[Red]#,##0.00"/>
  </Style>
  <Style ss:ID="s39">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="14" ss:Color="#000000" ss:Bold="1"/>
   <Interior ss:Color="#FDE9D9" ss:Pattern="Solid"/>
   <NumberFormat ss:Format="Short Date"/>
  </Style>
  <Style ss:ID="s40">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="12"/>
  </Style>
  <Style ss:ID="s41">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="12"/>
   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s42">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="12" ss:Color="#000000"/>
   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s43">
   <Alignment ss:Vertical="Bottom" ss:WrapText="1"/>
   <Font ss:FontName="Calibri" x:CharSet="204" x:Family="Swiss" ss:Size="14"
    ss:Color="#000000"/>
   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s44">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="12"/>
   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>
   <NumberFormat ss:Format="Short Date"/>
  </Style>
  <Style ss:ID="s45">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="12"/>
   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>
   <NumberFormat ss:Format="#,##0.00;[Red]#,##0.00"/>
  </Style>
  <Style ss:ID="s46">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="12" ss:Color="#000000"/>
   <NumberFormat ss:Format="#,##0.00;[Red]#,##0.00"/>
  </Style>
  <Style ss:ID="s47">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="14" ss:Color="#000000"/>
  </Style>
  <Style ss:ID="s48">
   <Alignment ss:Vertical="Bottom" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Calibri" x:CharSet="204" x:Family="Swiss" ss:Size="14"
    ss:Color="#000000"/>
  </Style>
  <Style ss:ID="s49">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="11" ss:Color="#000000"/>
   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>
   <NumberFormat ss:Format="Short Date"/>
  </Style>
  <Style ss:ID="s50">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="11"/>
   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>
   <NumberFormat ss:Format="#,##0.00;[Red]#,##0.00"/>
  </Style>
  <Style ss:ID="s51">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="11" ss:Color="#000000"/>
   <NumberFormat ss:Format="#,##0.00;[Red]#,##0.00"/>
  </Style>
  <Style ss:ID="s52">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Color="#000000"/>
   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>
   <NumberFormat ss:Format="#,##0.00;[Red]#,##0.00"/>
  </Style>
  <Style ss:ID="s53">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="12"/>
   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s54">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="12" ss:Color="#000000"/>
   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>
   <NumberFormat ss:Format="@"/>
  </Style>
  <Style ss:ID="s55">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="12" ss:Color="#000000"/>
   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>
   <NumberFormat ss:Format="#,##0.00;[Red]#,##0.00"/>
  </Style>
  <Style ss:ID="s56">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="14"/>
   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>
   <NumberFormat ss:Format="#,##0.00;[Red]#,##0.00"/>
  </Style>
  <Style ss:ID="s57">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="11" ss:Color="#000000"/>
   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s58">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="11" ss:Color="#000000"/>
   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>
   <NumberFormat ss:Format="#,##0.00;[Red]#,##0.00"/>
  </Style>
  <Style ss:ID="s59">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="11" ss:Color="#000000"/>
   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>
   <NumberFormat ss:Format="Standard"/>
  </Style>
  <Style ss:ID="s64">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="12" ss:Color="#000000"/>
   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>
   <NumberFormat ss:Format="@"/>
  </Style>
  <Style ss:ID="s65">
   <Alignment ss:Vertical="Top" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="16" ss:Color="#000000" ss:Bold="1" ss:Italic="1"/>
  </Style>
  <Style ss:ID="s68">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="16" ss:Color="#000000" ss:Bold="1" ss:Italic="1" ss:Underline="Single"/>
  </Style>
  <Style ss:ID="s115">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:CharSet="204" x:Family="Roman"
    ss:Size="12"/>
   <NumberFormat ss:Format="Short Date"/>
  </Style>
 </Styles>
 <Worksheet ss:Name="План ">
  <Table>
   <Column ss:StyleID="s16" ss:AutoFitWidth="0" ss:Width="65.25" ss:Span="1"/>
   <Column ss:Index="3" ss:StyleID="s16" ss:AutoFitWidth="0" ss:Width="101.25"/>
   <Column ss:StyleID="s21" ss:AutoFitWidth="0" ss:Width="141"/>
   <Column ss:StyleID="s25" ss:AutoFitWidth="0" ss:Width="138.75"/>
   <Column ss:StyleID="s25" ss:AutoFitWidth="0" ss:Width="147.75"/>
   <Column ss:StyleID="s25" ss:AutoFitWidth="0" ss:Width="139.5"/>
   <Column ss:StyleID="s23" ss:AutoFitWidth="0" ss:Width="126"/>
   <Column ss:StyleID="s16" ss:AutoFitWidth="0" ss:Width="159"/>
   <Row ss:AutoFitHeight="0" ss:Height="73.5">
    <Cell ss:MergeAcross="8" ss:StyleID="s65"><ss:Data ss:Type="String"
      xmlns="http://www.w3.org/TR/REC-html40"><B><I><Font html:Size="18"
         html:Color="#000000">                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  </Font><Font
         html:Color="#000000">               </Font></I></B></ss:Data><NamedCell
      ss:Name="Print_Area"/></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="33">
    <Cell ss:MergeAcross="8" ss:StyleID="s68"><Data ss:Type="String">Отчет по поданным заявлениям о включении в реестр кредиторов</Data><NamedCell
      ss:Name="Print_Area"/></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="93.75">
    <Cell ss:StyleID="s35"><Data ss:Type="String">№ п/п</Data><NamedCell
      ss:Name="Print_Area"/></Cell>
    <Cell ss:StyleID="s35"><Data ss:Type="String">№ дела</Data><NamedCell
      ss:Name="Print_Area"/></Cell>
    <Cell ss:StyleID="s35"><Data ss:Type="String">Дата признания должника банкротом</Data><NamedCell
      ss:Name="Print_Area"/></Cell>
    <Cell ss:StyleID="s36"><Data ss:Type="String">Ответчик/должник</Data><NamedCell
      ss:Name="_FilterDatabase"/><NamedCell ss:Name="Print_Area"/></Cell>
    <Cell ss:StyleID="s37"><Data ss:Type="String">Сумма задолженности в руб. на день подачи заявления</Data><NamedCell
      ss:Name="Print_Area"/></Cell>
    <Cell ss:StyleID="s38"><Data ss:Type="String">Сумма пени в руб. на день подачи заявления </Data><NamedCell
      ss:Name="Print_Area"/></Cell>
    <Cell ss:StyleID="s38"><Data ss:Type="String">Сумма госпошлины в руб., включаемая в реестр</Data><NamedCell
      ss:Name="Print_Area"/></Cell>
    <Cell ss:StyleID="s39"><Data ss:Type="String">Дата подачи заявления о включении в реестр кредиторов</Data><NamedCell
      ss:Name="Print_Area"/></Cell>
    <Cell ss:StyleID="s39"><Data ss:Type="String">Примечание</Data><NamedCell
      ss:Name="Print_Area"/></Cell>	
   </Row>';
   p$s(L_XML);		                                                                  
		    
		 FOR I IN (SELECT EXC.NUM_EXEC,
											 BC.DATE_BNK, 
											 CLI.CLI_ALT_NAME, 
											 BC.LAWSUIT_SUM, 
											 BC.LAWSUIT_PENY, 
											 BC.LAWSUIT_TOLL,
											 EV.EV_DATE,
											 BC.COMMENTARY
								FROM T_CLI_BANKRUPT BC,
										 T_CLIENTS CLI,
										 (SELECT EX1.NUM_EXEC,EX1.ID_BANKRUPT
											FROM T_CLI_BNK_COURT_EXEC EX1 
											LEFT OUTER JOIN T_CLI_BNK_COURT_EXEC EX2
											ON (EX1.ID_BANKRUPT = EX2.ID_BANKRUPT AND EX1.ID < EX2.ID)
											WHERE EX1.DATE_EXEC BETWEEN L_DATE_START AND L_DATE_END) EXC,
											(SELECT EV.ID_BANKRUPT, MAX(EV.DATE_EVENT) EV_DATE
											 FROM T_BANKRUPT_EVENTS EV, T_EVENT_TYPES ET
											 WHERE EV.ID_EVENT_TYPE = ET.ID_EVENT_TYPE
											 AND ET.CODE_EVENT = 'IN_COURT'
											 AND ET.DATE_CREATED BETWEEN L_DATE_START AND L_DATE_END
											 GROUP BY EV.ID_BANKRUPT) EV
								WHERE BC.ID_CLIENT = CLI.ID_CLIENT
								AND   BC.ID = EXC.ID_BANKRUPT(+)
								AND   EV.ID_BANKRUPT = BC.ID)
		 LOOP      
			  L_CNT := L_CNT + 1; 										 
				L_XML := '<Row ss:AutoFitHeight="0" ss:Height="40.5">'||CHR(13);
				L_XML := L_XML||'  <Cell ss:StyleID="s26"><Data ss:Type="String">'||TO_CHAR(L_CNT)||'</Data><NamedCell ss:Name="Print_Area"/></Cell>'||CHR(13);
				L_XML := L_XML||'  <Cell ss:StyleID="s26"><Data ss:Type="String">'||I.NUM_EXEC||'</Data><NamedCell ss:Name="Print_Area"/></Cell>'||CHR(13);
				IF I.DATE_BNK IS NOT NULL THEN
				  L_XML := L_XML||'<Cell ss:StyleID="s115"><Data ss:Type="DateTime">'||TO_CHAR(I.DATE_BNK,'YYYY-MM-DD')||'T00:00:00.000</Data><NamedCell ss:Name="Print_Area"/></Cell>'||CHR(13);
				ELSE
					L_XML := L_XML||'<Cell ss:StyleID="s115"><NamedCell ss:Name="Print_Area"/></Cell>'||CHR(13);
				END IF;	 
				L_XML := L_XML||'<Cell ss:StyleID="s27"><Data ss:Type="String">'||I.CLI_ALT_NAME||'</Data><NamedCell ss:Name="_FilterDatabase"/><NamedCell ss:Name="Print_Area"/></Cell>'||CHR(13);
				L_XML := L_XML||'<Cell ss:StyleID="s46"><Data ss:Type="Number">'||TO_CHAR(I.LAWSUIT_SUM,L_FMT,L_NLS)||'</Data><NamedCell ss:Name="Print_Area"/></Cell>'||CHR(13);
				L_XML := L_XML||'<Cell ss:StyleID="s46"><Data ss:Type="Number">'||TO_CHAR(I.LAWSUIT_PENY,L_FMT,L_NLS)||'</Data><NamedCell ss:Name="Print_Area"/></Cell>'||CHR(13);
				L_XML := L_XML||'<Cell ss:StyleID="s46"><Data ss:Type="Number">'||TO_CHAR(I.LAWSUIT_TOLL,L_FMT,L_NLS)||'</Data><NamedCell ss:Name="Print_Area"/></Cell>'||CHR(13);
				IF I.EV_DATE IS NOT NULL THEN
           L_XML := L_XML||'<Cell ss:StyleID="s115"><Data ss:Type="DateTime">'||TO_CHAR(I.EV_DATE,'YYYY-MM-DD')||'T00:00:00.000</Data><NamedCell ss:Name="Print_Area"/></Cell>'||CHR(13);
				ELSE
					 L_XML := L_XML||'<Cell ss:StyleID="s115"><NamedCell ss:Name="Print_Area"/></Cell>'||CHR(13);
				END IF;	 
				L_XML := L_XML||'<Cell ss:StyleID="s27"><Data ss:Type="String">'||I.COMMENTARY||'</Data><NamedCell ss:Name="_FilterDatabase"/><NamedCell ss:Name="Print_Area"/></Cell>'||CHR(13);
				L_XML := L_XML||'  </Row>';
			  P$S(L_XML);
			
		 END LOOP;                                            	 
		
   

	 L_XML := '  <Row ss:AutoFitHeight="0" ss:Height="40.5">
    <Cell ss:StyleID="s26"><Data ss:Type="String">ИТОГО:</Data><NamedCell ss:Name="Print_Area"/></Cell>
    <Cell ss:StyleID="s26"><NamedCell ss:Name="Print_Area"/></Cell>
    <Cell ss:StyleID="s48"><NamedCell ss:Name="Print_Area"/></Cell>
    <Cell ss:StyleID="s47"><NamedCell ss:Name="_FilterDatabase"/><NamedCell ss:Name="Print_Area"/></Cell>
    <Cell ss:StyleID="s18" ss:Formula="=SUM(R[-'||L_CNT||']C:R[-1]C)"><Data ss:Type="Number">0</Data><NamedCell ss:Name="Print_Area"/></Cell>
    <Cell ss:StyleID="s18" ss:Formula="=SUM(R[-'||L_CNT||']C:R[-1]C)"><Data ss:Type="Number">0</Data><NamedCell ss:Name="Print_Area"/></Cell>
    <Cell ss:StyleID="s18" ss:Formula="=SUM(R[-'||L_CNT||']C:R[-1]C)"><Data ss:Type="Number">0</Data><NamedCell ss:Name="Print_Area"/></Cell>
    <Cell ss:StyleID="s22"><NamedCell ss:Name="Print_Area"/></Cell>
    <Cell ss:StyleID="s22"><NamedCell ss:Name="Print_Area"/></Cell>
   </Row>
  </Table>
  <WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">
   <PageSetup>
    <Layout x:Orientation="Landscape"/>
    <Header x:Margin="0.3"/>
    <Footer x:Margin="0.3"/>
    <PageMargins x:Bottom="0.75" x:Left="0.7" x:Right="0.7" x:Top="0.75"/>
   </PageSetup>
   <Unsynced/>
   <FitToPage/>
   <Print>
    <FitHeight>0</FitHeight>
    <ValidPrinterInfo/>
    <PaperSizeIndex>9</PaperSizeIndex>
    <Scale>74</Scale>
    <HorizontalResolution>600</HorizontalResolution>
    <VerticalResolution>600</VerticalResolution>
   </Print>
   <Zoom>70</Zoom>
   <PageBreakZoom>80</PageBreakZoom>
   <Selected/>
   <FreezePanes/>
   <FrozenNoSplit/>
   <SplitHorizontal>3</SplitHorizontal>
   <TopRowBottomPane>3</TopRowBottomPane>
   <ActivePane>2</ActivePane>
   <Panes>
    <Pane>
     <Number>3</Number>
    </Pane>
    <Pane>
     <Number>2</Number>
    </Pane>
   </Panes>
   <ProtectObjects>False</ProtectObjects>
   <ProtectScenarios>False</ProtectScenarios>
  </WorksheetOptions>
 </Worksheet>
</Workbook>';

		p$s( L_XML );
	
	
	END P_ADD_DATA;

---------------------------------------------------------------------------------------------------------------------------------

  FUNCTION RUN_REPORT(P_PAR_01 VARCHAR2 DEFAULT NULL
											,P_PAR_02 VARCHAR2 DEFAULT NULL
											,P_PAR_03 VARCHAR2 DEFAULT NULL
											,P_PAR_04 VARCHAR2 DEFAULT NULL
											,P_PAR_05 VARCHAR2 DEFAULT NULL
											,P_PAR_06 VARCHAR2 DEFAULT NULL
											,P_PAR_07 VARCHAR2 DEFAULT NULL
											,P_PAR_08 VARCHAR2 DEFAULT NULL
											,P_PAR_09 VARCHAR2 DEFAULT NULL
											,P_PAR_10 VARCHAR2 DEFAULT NULL) RETURN LAWSUP.PKG_FILES.REC_DOC
	IS
	l_BLOB LAWSUP.PKG_FILES.REC_DOC;
  BEGIN
	  EXCEL_DOC := NULL;
		DBMS_LOB.CREATETEMPORARY( EXCEL_DOC.P_BLOB, TRUE );
    DBMS_LOB.OPEN( EXCEL_DOC.P_BLOB, DBMS_LOB.LOB_READWRITE );

		P_ADD_DATA(TO_DATE(P_PAR_02,'DD.MM.YYYY'),TO_DATE(P_PAR_03,'DD.MM.YYYY'));

		DBMS_LOB.CLOSE(EXCEL_DOC.P_BLOB);

		    l_BLOB.P_BLOB := EXCEL_DOC.P_BLOB;
	      l_BLOB.P_FILE_NAME := 'Отчет по заявлениям реестр кредиторов';
				RETURN L_BLOB;

  END RUN_REPORT;

end PKG_XLS_REPORT_018;
/

