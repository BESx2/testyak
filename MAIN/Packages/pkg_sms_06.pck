i�?create or replace package lawmain.PKG_SMS_06 is

  -- Author  : EUGEN
  -- Created : 12.01.2017 10:44:26
  -- Purpose : СМС о задолженности от досудебного по договору
  
  	PROCEDURE P_SEND_SMS(P_ID_CASE  NUMBER,P_PHONE   VARCHAR2);
		
		PROCEDURE P_SEND_MASS(P_ID_FOLDER  NUMBER);

end PKG_SMS_06;
/

create or replace package body lawmain.PKG_SMS_06 is

	PROCEDURE P_SEND_SMS(P_ID_CASE    NUMBER,
											 P_PHONE      VARCHAR2)
		 IS
		G$SMS_TEXT VARCHAR2(2000);
		L_PHONE    VARCHAR2(100);                                     
		L_SMS_ID   NUMBER;
	BEGIN                       
		G$SMS_TEXT := PKG_PREF.F$DESC1('PRET_SMS_DEBT');  
	  
	  FOR I IN (SELECT W.CURRENT_DEBT SUM_DEBT
			        FROM T_CLI_DEBT_WORK W WHERE W.ID_WORK = P_ID_CASE)
		LOOP                                                  			
			G$SMS_TEXT := REPLACE(G$SMS_TEXT,'#DEBT#',TO_CHAR(I.SUM_DEBT,'FM999999999999999990D00'));			
		END LOOP;						 
		
		IF P_PHONE IS NULL THEN
			 RAISE NO_DATA_FOUND;
		END IF;
		--Отсылка СМС
		L_SMS_ID := LAWSUP.PKG_SMS.F_PUT_MESSAGE(P_PHONE => P_PHONE,P_MESSAGE => G$SMS_TEXT);	
		--Событие и привязка к делу
		--PKG_EVENTS.P_CREATE_CLI_EVENT(P_CASE_ID => P_ID_CASE,P_CODE => 'INFORM_SMS_LEG');
		
		PKG_SEND_SMS.P_INSERT_CLI_CASE_SMS(P_ID_CASE,L_SMS_ID,'PRET_SMS_DEBT');
	
	EXCEPTION 
		 WHEN NO_DATA_FOUND THEN
			  PKG_LOG.P$LOG(p_PROG_UNIT => 'PKG_SMS_06',p_MESS => 'Не найден телефон для отправки'
										 ,P_C1 => 'P_ID_CASE: '||P_ID_CASE,P_d1 => SYSDATE);   
				 RAISE_APPLICATION_ERROR(-20200,'Не найден телефон для отправки'); 
	END;    

----------------------------------------------------------------------------------------
	
	PROCEDURE P_SEND_MASS(P_ID_FOLDER NUMBER)
		 IS
		G$SMS_TEXT VARCHAR2(2000);
		L_TEXT     VARCHAR2(2000);
		L_PHONE    VARCHAR2(100);                                     
		L_SMS_ID   NUMBER;
	BEGIN                       
		G$SMS_TEXT := PKG_PREF.F$DESC1('PRET_SMS_DEBT');  
	  
	  FOR I IN (SELECT S.SUM_DEBT, S.ID_CLIENT,S.ID_WORK FROM V_CLI_PRETENSION S, T_FOLDER_CLI_CASES FC 
			        WHERE FC.ID_FOLDER = P_ID_FOLDER AND S.ID_WORK = FC.ID_CASE
							AND   S.SUM_DEBT > 0)
		LOOP
      L_PHONE := '';
																								
      FOR J IN (SELECT REGEXP_SUBSTR(D.DET_VAL,'(7|8)9\d{9}') PHONE INTO L_PHONE FROM T_CLI_CONTACT_DETAILS D 
				        WHERE D.DET_TYPE = 'Телефон' AND D.DET_DESC = 'Телефон для рассылок' AND D.ID_CLIENT = I.ID_CLIENT)
			LOOP
				 L_PHONE := J.PHONE;
			END LOOP;             
			
			IF L_PHONE IS NULL THEN                                                         
				FOR J IN (SELECT REGEXP_SUBSTR(D.DET_VAL,'(7|8)9\d{9}') PHONE INTO L_PHONE FROM T_CLI_CONTACT_DETAILS D 
					        WHERE D.ID_CLIENT = I.ID_CLIENT AND D.DET_TYPE = 'Телефон' AND ROWNUM = 1)
				LOOP
					L_PHONE := J.PHONE;
				END LOOP;
			END IF;                
			
			IF L_PHONE IS NULL THEN
				RETURN;
			END IF;
																			
			L_TEXT := REPLACE(G$SMS_TEXT,'#DEBT#',TO_CHAR(I.SUM_DEBT,'FM999999999999999990D00'));			
								 
		
			--Отсылка СМС
			L_SMS_ID := LAWSUP.PKG_SMS.F_PUT_MESSAGE(P_PHONE => L_PHONE,P_MESSAGE => G$SMS_TEXT);	
			--Событие и привязка к делу
			--PKG_EVENTS.P_CREATE_CLI_EVENT(P_CASE_ID => P_ID_CASE,P_CODE => 'INFORM_SMS_LEG');
			
			PKG_SEND_SMS.P_INSERT_CLI_CASE_SMS(I.ID_WORK,L_SMS_ID,'PRET_SMS_DEBT');
	   END LOOP;
	END;
  
end PKG_SMS_06;
/

