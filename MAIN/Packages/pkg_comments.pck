i�?create or replace package lawmain.PKG_COMMENTS is

  -- Author  : BES
  -- Purpose : Пакет для работы с комментариями
  
	PROCEDURE P_CREATE_CLI_COMMENT(P_ID_CLIENT VARCHAR2,
																 P_TEXT      VARCHAR2,
																 P_ID_CASE   NUMBER DEFAULT NULL);
																 
  PROCEDURE P_UPDATE_CLI_COMMENT(P_ID_COMMENT NUMBER, P_TEXT VARCHAR2);
																					 
end PKG_COMMENTS;
/

create or replace package body lawmain.PKG_COMMENTS is

	-----------------------------------------------------------------------------	
	PROCEDURE P_CREATE_CLI_COMMENT(P_ID_CLIENT VARCHAR2,
																 P_TEXT      VARCHAR2,
																 P_ID_CASE   NUMBER DEFAULT NULL)
	IS                                       
	BEGIN
  	 INSERT INTO T_CLI_COMMENT(ID_COMMENT,ID_CLIENT,TEXT,CREATED_BY,DATE_CREATED,ID_CASE)
		 VALUES(SEQ_COMMENT.nextval,P_ID_CLIENT,P_TEXT,F$_USR_ID,SYSDATE,P_ID_CASE);
	END;	                                                                  
	
	-----------------------------------------------------------------------------------
	
	PROCEDURE P_UPDATE_CLI_COMMENT(P_ID_COMMENT NUMBER, P_TEXT VARCHAR2) 
		IS
	BEGIN
		UPDATE T_CLI_COMMENT C
		SET    C.TEXT          = P_TEXT,
					 C.MODIFIED_BY   = F$_USR_ID,
					 C.DATE_MODIFIED = SYSDATE
		WHERE  C.ID_COMMENT    = P_ID_COMMENT;
	END;												
END PKG_COMMENTS;
/

