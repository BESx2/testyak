i�?CREATE OR REPLACE PACKAGE LAWMAIN.PKG_XLSX_002 IS

	-- Author  : EUGEN
	-- Created : 21.12.2019 13:12:05
	-- Purpose : Акт сверки

	FUNCTION RUN_REPORT(P_PAR_01 IN VARCHAR2 DEFAULT NULL
										 ,P_PAR_02 IN VARCHAR2 DEFAULT NULL
										 ,P_PAR_03 IN VARCHAR2 DEFAULT NULL
										 ,P_PAR_04 IN VARCHAR2 DEFAULT NULL
										 ,P_PAR_05 IN VARCHAR2 DEFAULT NULL
										 ,P_PAR_06 IN VARCHAR2 DEFAULT NULL
										 ,P_PAR_07 IN VARCHAR2 DEFAULT NULL
										 ,P_PAR_08 IN VARCHAR2 DEFAULT NULL
										 ,P_PAR_09 IN VARCHAR2 DEFAULT NULL
										 ,P_PAR_10 IN VARCHAR2 DEFAULT NULL)
		RETURN LAWSUP.PKG_FILES.REC_DOC;

END PKG_XLSX_002;
/

CREATE OR REPLACE PACKAGE BODY LAWMAIN.PKG_XLSX_002 IS

	FUNCTION RUN_REPORT(P_PAR_01 IN VARCHAR2 DEFAULT NULL
										 ,P_PAR_02 IN VARCHAR2 DEFAULT NULL
										 ,P_PAR_03 IN VARCHAR2 DEFAULT NULL
										 ,P_PAR_04 IN VARCHAR2 DEFAULT NULL
										 ,P_PAR_05 IN VARCHAR2 DEFAULT NULL
										 ,P_PAR_06 IN VARCHAR2 DEFAULT NULL
										 ,P_PAR_07 IN VARCHAR2 DEFAULT NULL
										 ,P_PAR_08 IN VARCHAR2 DEFAULT NULL
										 ,P_PAR_09 IN VARCHAR2 DEFAULT NULL
										 ,P_PAR_10 IN VARCHAR2 DEFAULT NULL)
		RETURN LAWSUP.PKG_FILES.REC_DOC IS
		L_NAME     VARCHAR2(100) := 'Акт сверки';
		L_XML      XMLTYPE;         
		L_TAB      XMLTYPE;
		L_SQL      SYS_REFCURSOR;
		L_REP      LAWSUP.PKG_FILES.REC_DOC; 
		L_FMT      VARCHAR2(100) := 'FM999G999G99G999G990D00';
		L_NLS      VARCHAR2(100) := 'NLS_NUMERIC_CHARACTERS='', ''';
		L_DATE_BEG DATE;
		L_DATE_END DATE;
		L_CELL     PKG_CONTROLLER.CELL_TAGS;
	BEGIN
	
		L_DATE_BEG := NVL(TO_DATE(P_PAR_02, 'DD.MM.YYYY'), DATE '2000-01-01');
		L_DATE_END := NVL(TO_DATE(P_PAR_03, 'DD.MM.YYYY'), TRUNC(SYSDATE,'DD')) + 1 -
									(INTERVAL '1' SECOND);
	
		L_CELL :=  PKG_CONTROLLER.CELL_TAGS('период', 'объем', 'начисленно', 'оплаченно');
	
		OPEN L_SQL FOR 
		  SELECT T2.DOC_DATE, T1.VOLUME, T2.AMOUNT, T2.PAYED
			FROM   (SELECT TRUNC(FD.DOC_DATE, 'MM') DOC_DATE, SUM(D.VOLUME) VOLUME
							FROM   T_CLI_FINDOCS FD, T_CLI_FINDOCS_DETAILS D
							WHERE  FD.DOC_DATE BETWEEN L_DATE_BEG AND L_DATE_END
							AND    FD.EXT_ID = D.EXT_ID
							AND    FD.ID_CONTRACT = TO_NUMBER(P_PAR_04)
							AND    FD.OPER_TYPE = 'Приход'
							GROUP  BY TRUNC(FD.DOC_DATE, 'MM')) T1
						,(SELECT DOC_DATE, AMOUNT, PAYED
							FROM   (SELECT TRUNC(FD.DOC_DATE, 'MM') DOC_DATE
														,FD.AMOUNT
														,FD.OPER_TYPE
											FROM   (SELECT FDC.ID_CLIENT
																		,FDC.ID_CONTRACT
																		,FDC.EXT_ID_FROM
																		,FDC.DOC_DATE
																		,FDC.OPER_TYPE
																		,SUM(FDC.AMOUNT) AMOUNT
															FROM   T_CLI_FINDOC_CONS FDC
															WHERE  FDC.ID_CONTRACT = TO_NUMBER(P_PAR_04)
															AND    FDC.DOC_DATE BETWEEN L_DATE_BEG AND L_DATE_END
															AND    FDC.DEBT_TYPE IN ('Полезный отпуск', 'Пени')
															GROUP  BY FDC.ID_CLIENT ,FDC.ID_CONTRACT,FDC.EXT_ID_FROM,FDC.DOC_DATE,FDC.OPER_TYPE) FD)
							PIVOT(SUM(AMOUNT)
							FOR    OPER_TYPE IN('Приход' AS "AMOUNT", 'Расход' AS "PAYED"))) T2
			WHERE  T2.DOC_DATE = T1.DOC_DATE(+)
			ORDER  BY T2.DOC_DATE;
					 	
		L_XML :=  PKG_CONTROLLER.F_XLSX_XML(P_SQL => L_SQL, P_REP_CODE => 'RECON_ACT', P_TABLE_NAME => 'таблица', P_TAGS => L_CELL);
	  
		SELECT XMLAGG(XMLELEMENT("tags", 
							XMLELEMENT("tag", XMLATTRIBUTES('дата_начала' AS "name"), to_char(l_date_beg,'dd.mm.yyyy')), 
							XMLELEMENT("tag", XMLATTRIBUTES('дата_окончания' AS "name"), to_char(l_date_end,'dd.mm.yyyy')), 
							XMLELEMENT("tag", XMLATTRIBUTES('абонент' AS "name"), T.CLI_ALT_NAME), 
							XMLELEMENT("tag", XMLATTRIBUTES('сальдо_начало' AS "name"), TO_CHAR(T.SALDO,'FM999G999G999G999G990D00','NLS_NUMERIC_CHARACTERS='', ''')),	
              XMLELEMENT("tag", XMLATTRIBUTES('тип_договора' AS "name"), DECODE(T.CTR_TYPE,'Основной в ХВС','Водоснабжение','Водоотведение')), 
              XMLELEMENT("tag", XMLATTRIBUTES('номер_договора' AS "name"), T.CTR_NUMBER), 							
              XMLELEMENT("tag", XMLATTRIBUTES('дата_печати' AS "name"), TO_CHAR(SYSDATE,'DD.MM.YYYY HH24:MI:SS')),
              XMLELEMENT("tag", XMLATTRIBUTES('пользователь' AS "name"), INITCAP(PKG_USERS.F_GET_FIO_BY_ID(F$_USR_ID,'Y'))),
              XMLELEMENT("tag", XMLATTRIBUTES('директор' AS "name"), PKG_PREF.F$C2('ORG_MAIN_FIO')),							
              XMLELEMENT("tag", XMLATTRIBUTES('адрес_орг' AS "name"), PKG_PREF.F$C1('ORG_LEG_ADDRESS')),	
              XMLELEMENT("tag", XMLATTRIBUTES('инн_орг' AS "name"), PKG_PREF.F$C1('ORG_INN')),								
              XMLELEMENT("tag", XMLATTRIBUTES('банк_орг' AS "name"), PKG_PREF.F$C1('ORG_BANK')),									
              XMLELEMENT("tag", XMLATTRIBUTES('бик_орг' AS "name"), PKG_PREF.F$C4('ORG_BANK')),																
              XMLELEMENT("tag", XMLATTRIBUTES('кс_орг' AS "name"), PKG_PREF.F$C3('ORG_BANK')),																							
              XMLELEMENT("tag", XMLATTRIBUTES('рс_орг' AS "name"), PKG_PREF.F$C2('ORG_BANK')),	
              XMLELEMENT("tag", XMLATTRIBUTES('предст_абон' AS "name"), (SELECT INITCAP(R.FIO) FROM T_CLI_REPRES R WHERE R.ID_REPRES = T.ID_REP)),							
							XMLELEMENT("tag", XMLATTRIBUTES('адрес_абон' AS "name"), PKG_CLIENTS.F_GET_ADDRESS(T.ID_CLIENT)),	 																													
							XMLELEMENT("tag", XMLATTRIBUTES('инн_абон' AS "name"), T.INN),							
							XMLELEMENT("tag", XMLATTRIBUTES('банк_абон' AS "name"), BNK.BANK_NAME),
							XMLELEMENT("tag", XMLATTRIBUTES('бик_абон' AS "name"), BNK.BIK),							
							XMLELEMENT("tag", XMLATTRIBUTES('кс_абон' AS "name"), BNK.COR_ACC),							              							
							XMLELEMENT("tag", XMLATTRIBUTES('рс_абон' AS "name"), BNK.ACC_NUMBER),
							XMLELEMENT("tag", XMLATTRIBUTES('итого_кол' AS "name"), TO_CHAR(FF.VOLUME,L_FMT,L_NLS)),
							XMLELEMENT("tag", XMLATTRIBUTES('итого_нач' AS "name"), TO_CHAR(FF.AMOUNT,L_FMT,L_NLS)),			
							XMLELEMENT("tag", XMLATTRIBUTES('итого_опл' AS "name"), TO_CHAR(FF.PAYED,L_FMT,L_NLS)),			
							XMLELEMENT("tag", XMLATTRIBUTES('сальдо_конец' AS "name"), TO_CHAR(T.SALDO+(FF.AMOUNT-FF.PAYED),L_FMT,L_NLS))																								
							))
		 INTO L_TAB							
		 FROM (SELECT (SELECT MAX(R.ID_REPRES) FROM T_CLI_REPRES R 
									 WHERE LOWER(R.ROLE) IN ('директор','руководитель','генеральный директор')
									 AND R.ID_CLIENT = CLI.ID_CLIENT) ID_REP
									,(SELECT MAX(A.ID_ACC) FROM T_SETL_ACCOUNTS A WHERE A.ID_CLIENT = CLI.ID_CLIENT) ID_ACC	
									,CLI.INN                                                
									,(SELECT SUM(DECODE(FD.OPER_TYPE,'Приход',FD.AMOUNT,-FD.AMOUNT)) 
									  FROM T_CLI_FINDOC_CONS FD
										WHERE FD.ID_CONTRACT = CT.ID_CONTRACT
										AND   FD.DOC_DATE < L_DATE_BEG
										AND   FD.DEBT_TYPE IN ('Полезный отпуск', 'Пени')) SALDO
									,CLI.CLI_ALT_NAME                                                                
									,CLI.ID_CLIENT
									,CT.CTR_NUMBER
									,CT.CTR_TYPE
					 FROM  T_CONTRACTS CT,
								 T_CLIENTS CLI								 
					 WHERE CT.ID_CONTRACT = TO_NUMBER(P_PAR_04)
					 AND   CT.ID_CLIENT = CLI.ID_CLIENT) T
				 ,(SELECT A.ACC_NUMBER, B.COR_ACC, B.BANK_NAME, B.BIK, A.ID_ACC
					 FROM T_SETL_ACCOUNTS A, T_BANKS B 
					 WHERE A.ID_BANK = B.ID_BANK) BNK
				 , (SELECT  SUM(T.VOLUME) VOLUME,SUM(T.AMOUNT) AMOUNT,SUM(T.PAYED) PAYED
						FROM (SELECT TRUNC(FD.DOC_DATE,'MM') DOC_DATE,
												 (SELECT SUM(D.VOLUME)
													FROM T_CLI_FINDOCS_DETAILS D
													WHERE D.EXT_ID = FD.EXT_ID) VOLUME,
													FD.AMOUNT,
													FD.AMOUNT-FD.DEBT PAYED
									FROM T_CLI_FINDOCS FD                     			
									WHERE FD.ID_CONTRACT = TO_NUMBER(P_PAR_04)
									AND   FD.DOC_DATE BETWEEN L_DATE_BEG AND L_DATE_END
									AND   FD.OPER_TYPE = 'Приход')T) FF
		 WHERE T.ID_ACC = BNK.ID_ACC(+);
		
		L_XML := L_XML.appendChildXML('/request/input-data',L_TAB,NULL);			 
		
		L_REP.P_BLOB :=  PKG_CONTROLLER.F_GET_REPORT_XML(L_XML.GETCLOBVAL(), 'excel_report_xml');
	
		L_REP.P_FILE_NAME := PKG_UTILS.F_MAKE_TRANSLIT(L_NAME);
	  
		RETURN L_REP;
	END;

END PKG_XLSX_002;
/

