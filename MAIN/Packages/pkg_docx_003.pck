i�?create or replace package lawmain.PKG_DOCX_003 is

  -- Author  : EUGEN
  -- Created : 03.03.2020 15:02:04
  -- Purpose : Соглашение на мировое 	                                
	-- CODE    : PEACE_AGREE
  
  FUNCTION RUN_REPORT(P_PAR_01  IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_02 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_03 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_04 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_05 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_06 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_07 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_08 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_09 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_10 IN VARCHAR2 DEFAULT NULL) RETURN LAWSUP.PKG_FILES.REC_DOC;		                           

end PKG_DOCX_003;
/

create or replace package body lawmain.PKG_DOCX_003 is

  FUNCTION RUN_REPORT(P_PAR_01  IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_02 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_03 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_04 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_05 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_06 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_07 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_08 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_09 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_10 IN VARCHAR2 DEFAULT NULL) RETURN LAWSUP.PKG_FILES.REC_DOC
   IS  
   L_XML XMLTYPE;          
	 L_TAB XMLTYPE;
   L_SQL SYS_REFCURSOR;     
   L_REP LAWSUP.PKG_FILES.REC_DOC;    
	 L_PENY  VARCHAR2(1); 
	 L_ID_CWS NUMBER;
	 L_ID_SEW NUMBER;
  BEGIN         
    OPEN L_SQL FOR 
		SELECT  T.NUM_EXEC  "номер_дела",
            TO_CHAR(SYSDATE,'DD.MM.YYYY') "тек_дата",
            PKG_PREF.F$C1('ORG_INN') "инн_орг",                              
            PKG_PREF.F$C1('ORG_OGRN') "огрн_орг",
            PKG_PREF.F$C2('COURT_MAIN') "руководитель_рп", 
            PKG_PREF.F$C4('COURT_MAIN') "доверенность", 
            PKG_PREF.F$D5('COURT_MAIN') "дата_доверенности",
            T.CLI_NAME "абонент",
            T.OGRN "огрн_абонента",
            (SELECT R.POSITION FROM T_CLI_REPRES R WHERE R.ID_REPRES = T.ID_REP) "долж_абон",
            (SELECT LAWSUP.PKG_MORFER.F_GET_DECLENSION(INITCAP(R.FIO),'Gen','S') FROM T_CLI_REPRES R WHERE R.ID_REPRES = T.ID_REP) "предст_абон",
            INITCAP(PKG_CLIENTS_UTL.F_GET_REPRES_FIO_REVERSE(T.ID_REP)) "предст_абон_кр",  
						--ХВС-----------------
						NVL2(T.CWS_ID_CONTRACT,'true','false') "если_хвс",
						NVL(T.DEBT_CWS,0) + NVL(T.PENY_CWS,0) "общий_долг_хвс",
						T.CWS_CTR_NUM "договор_хвс",  
						T.CWS_CTR_DATE "дата_хвс",                            
						T.DEBT_CWS "сумма_долга_хвс",
						NVL(T.PENY_CWS,0) "пени_хвс",
						T.PER_CWS "периоды_хвс",
						--ВО-----------------
						NVL2(T.SEW_ID_CONTRACT,'true','false') "если_во",
						NVL(T.DEBT_SEW,0) + NVL(T.PENY_SEW,0) "общий_долг_во",
						T.SEW_CTR_NUM "договор_во",  
						T.SEW_CTR_DATE "дата_во",    
						T.DEBT_SEW "сумма_долга_во",
						NVL(T.PENY_SEW,0) "пени_во",
						T.PER_SEW "периоды_во",             
						---------------------
						T.PENY_DATE "дата_неустойки",						
            NVL(T.DEBT_CWS,0)+NVL(T.DEBT_SEW,0) + DECODE(T.INCLUDE_PENY,'Y',NVL(T.PENY_CWS,0)+NVL(T.PENY_SEW,0),0) "общая_сумма_долга",  
						NVL(T.PENY_CWS,0) + NVL(T.PENY_SEW,0) "общая_сумма_пени",
						--таблицы-----------
						CASE WHEN T.CWS_ID_CONTRACT IS NOT NULL AND T.SEW_ID_CONTRACT IS NOT NULL THEN 'true' ELSE 'false' END "таблица_хвс_во",
						CASE WHEN T.CWS_ID_CONTRACT IS NOT NULL AND T.SEW_ID_CONTRACT IS NULL THEN 'true' ELSE 'false' END "таблица_хвс",	      
						CASE WHEN T.CWS_ID_CONTRACT IS NULL AND T.SEW_ID_CONTRACT IS NOT NULL THEN 'true' ELSE 'false' END "таблица_во",							
						--итоги таблиц------
						NVL(T.DEBT_CWS,0) "итого_хвс",
						NVL(T.DEBT_SEW,0) "итого_во",
						NVL(T.PENY_CWS,0) "итого_пени_хвс",
						NVL(T.PENY_SEW,0) "итого_пени_во",
						NVL(T.DEBT_CWS,0)+NVL(T.DEBT_SEW,0) + DECODE(T.INCLUDE_PENY,'Y',NVL(T.PENY_CWS,0)+NVL(T.PENY_SEW,0),0)  "итого_оплата",
						--------------------
						PKG_PREF.F$C1('ORG_LEG_ADDRESS') "адрес_орг",
						PKG_PREF.F$C1('ORG_KPP') "кпп_орг",
						PKG_PREF.F$C1('ORG_BANK') "банк_орг",
						PKG_PREF.F$C2('ORG_BANK') "рс_орг",
						PKG_PREF.F$C3('ORG_BANK') "кс_орг",
            PKG_PREF.F$C4('ORG_BANK') "бик_орг",
            PKG_PREF.F$C3('COURT_MAIN') "руководитель_кр",
						PKG_CLIENTS.F_GET_ADDRESS(T.ID_CLIENT) "адрес_абон",
				    T.KPP  "кпп_абон",            
						T.INN "инн_абон",
            BNK.ACC_NUMBER "рс_абон",
						BNK.COR_ACC  "кс_абон",
						BNK.BANK_NAME "банк_абон",
					  BNK.BIK "бик_абон",
						CASE 
							WHEN T.TYPE_EXEC = 'FIRST' THEN '70% ('||TO_CHAR(T.GOV_TOLL*70/100,'FM999G999G999G9999G9990D00','NLS_NUMERIC_CHARACTERS='', ''')||' руб.)'
						  WHEN T.TYPE_EXEC = 'APPEAL' THEN '50% ('||TO_CHAR(T.GOV_TOLL*50/100,'FM999G999G999G9999G9990D00','NLS_NUMERIC_CHARACTERS='', ''')||' руб.)'
						  ELSE '30% ('||TO_CHAR(T.GOV_TOLL*30/100,'FM999G999G999G9999G9990D00','NLS_NUMERIC_CHARACTERS='', ''')||' руб.)'
						END "процент1",		
						CASE 
							WHEN T.TYPE_EXEC = 'FIRST' THEN '30% ('||TO_CHAR(T.GOV_TOLL*30/100,'FM999G999G999G9999G9990D00','NLS_NUMERIC_CHARACTERS='', ''')||' руб.)'
						  WHEN T.TYPE_EXEC = 'APPEAL' THEN '50% ('||TO_CHAR(T.GOV_TOLL*50/100,'FM999G999G999G9999G9990D00','NLS_NUMERIC_CHARACTERS='', ''')||' руб.)'
						  ELSE '70% ('||TO_CHAR(T.GOV_TOLL*70/100,'FM999G999G999G9999G9990D00','NLS_NUMERIC_CHARACTERS='', ''')||' руб.)'
						END "процент2"
		 FROM (SELECT EX.NUM_EXEC  
									,CLI.CLI_NAME 
									,CLI.OGRN 
									,(SELECT MAX(R.ID_REPRES) FROM T_CLI_REPRES R 
									 WHERE LOWER(R.ROLE) IN ('директор','руководитель','генеральный директор')
									 AND R.ID_CLIENT = CLI.ID_CLIENT) ID_REP
									,A.DEBT_CWS
									,A.PENY_CWS
									,A.DEBT_SEW
									,A.PENY_SEW  
									,A.INCLUDE_PENY
									,CC.SEW_ID_CONTRACT  
									,CC.SEW_CTR_NUM 
									,CC.SEW_CTR_DATE
									,CC.CWS_ID_CONTRACT 
									,CC.CWS_CTR_NUM    
									,CC.CWS_CTR_DATE
									,CC.ID_CASE              
									,CC.GOV_TOLL
									,cli.ID_CLIENT
									,A.PENY_DATE   
									,PKG_CLI_CASES_UTL.F_GET_DEBT_PERIOD(CC.ID_CASE,CC.CWS_ID_CONTRACT) PER_CWS
									,PKG_CLI_CASES_UTL.F_GET_DEBT_PERIOD(CC.ID_CASE,CC.SEW_ID_CONTRACT) PER_SEW
									,CLI.KPP   
									,cli.INN       
									,(SELECT EX.TYPE_EXEC FROM T_CLI_COURT_EXEC EX WHERE EX.ID = A.ID_EXEC) TYPE_EXEC           
									,(SELECT MAX(A.ID_ACC) FROM T_SETL_ACCOUNTS A WHERE A.ID_CLIENT = CLI.ID_CLIENT) ID_ACC						
					 FROM T_CLI_PEACE_AGREE A,
								T_CLI_COURT_EXEC EX,
								V_CLI_CASES CC,
								T_CLIENTS CLI					 
					 WHERE A.ID_EXEC = P_PAR_01
					 AND   A.ID_EXEC = EX.ID 		 
					 AND   EX.ID_CASE = CC.ID_CASE
					 AND   CC.ID_CLIENT = CLI.ID_CLIENT) T,
          (SELECT A.ACC_NUMBER, B.COR_ACC, B.BANK_NAME, B.BIK, A.ID_ACC
				   FROM T_SETL_ACCOUNTS A, T_BANKS B 
					 WHERE A.ID_BANK = B.ID_BANK) BNK
		WHERE T.ID_ACC = BNK.ID_ACC(+);
					
		SELECT A.INCLUDE_PENY, S.ID_CWS_CONTRACT, S.ID_SEW_CONTRACT 
		INTO L_PENY,L_ID_CWS,L_ID_SEW 
		FROM T_CLI_PEACE_AGREE A, T_CLI_COURT_EXEC EX, T_CLI_CASES S
		WHERE A.ID_EXEC = TO_NUMBER(P_PAR_01)
		AND   EX.ID= A.ID_EXEC AND S.ID_CASE = EX.ID_CASE;
		                                                                                                                                               
		--ТАБЛИЧНАЯ ЧАСТЬ ОТЧЕТА
		SELECT XMLELEMENT("rows", 
						XMLAGG(XMLELEMENT("row", 
							XMLELEMENT("cell", XMLATTRIBUTES('пункт' AS "tag"), ROWNUM), 
							XMLELEMENT("cell", XMLATTRIBUTES('дата_платежа' AS "tag"), 'до '||TO_CHAR(T.PAY_DATE, 'dd.mm.yyyy') || 'г.'),
							XMLELEMENT("cell", XMLATTRIBUTES('долг_таб_хвс' AS "tag"), TO_CHAR(T.SUM_CWS, 'FM999G999G999G999G999G990D00')), 
							XMLELEMENT("cell", XMLATTRIBUTES('пени_таб_хвс' AS "tag"), TO_CHAR(T.PENY_CWS, 'FM999G999G999G999G999G990D00')),
							XMLELEMENT("cell", XMLATTRIBUTES('долг_таб_во' AS "tag"), TO_CHAR(T.SUM_SEW, 'FM999G999G999G999G999G990D00')), 
							XMLELEMENT("cell", XMLATTRIBUTES('пени_таб_во' AS "tag"), TO_CHAR(T.PENY_SEW, 'FM999G999G999G999G999G990D00')), 
							CASE WHEN L_PENY = 'Y' THEN                           
								XMLELEMENT("cell", XMLATTRIBUTES('итого_таб' AS "tag"), 
												 TO_CHAR(NVL(T.SUM_CWS, 0) + NVL(T.SUM_SEW, 0) + NVL(T.PENY_CWS, 0) +NVL(T.PENY_SEW, 0), 'FM999G999G999G999G999G990D00'))
							ELSE
								XMLELEMENT("cell", XMLATTRIBUTES('итого_таб' AS "tag"), 
												 TO_CHAR(NVL(T.SUM_CWS, 0) + NVL(T.SUM_SEW, 0), 'FM999G999G999G999G999G990D00'))
							END					 						                                                                  
							)))
		INTO   L_TAB
		FROM   (SELECT P.PAY_DATE, P.SUM_CWS, P.PENY_CWS, P.SUM_SEW, P.PENY_SEW
						FROM   T_CLI_PEACE_AGREE_PAYMENTS P
						WHERE  P.ID_EXEC = TO_NUMBER(P_PAR_01)) T
		ORDER  BY T.PAY_DATE ASC;
		
		-- В зависимости от пени формируем тот или иной шаблон
		-- Удаляя из обще таблицы ненужные ноды
		IF L_PENY = 'Y' THEN 
			IF L_ID_CWS IS NOT NULL AND L_ID_SEW IS NOT NULL THEN       
				 L_TAB := XMLTYPE('<tables><table name="таблица1"></table></tables>').appendChildXML('/tables/table',l_tab,NULL);
			ELSIF L_ID_CWS IS NOT NULL AND L_ID_SEW IS NULL THEN    																																	 
				 L_TAB := L_TAB.deleteXML('rows/row/cell[@tag="долг_таб_во" or @tag="пени_таб_во"]',NULL);	
 				 L_TAB := XMLTYPE('<tables><table name="таблица2"></table></tables>').appendChildXML('/tables/table',l_tab,NULL);
			ELSIF L_ID_CWS IS NULL AND L_ID_SEW IS NOT NULL THEN	                                                             
				 L_TAB := L_TAB.deleteXML('rows/row/cell[@tag="долг_таб_хвс" or @tag="пени_таб_хвс"]',NULL);	
 				 L_TAB := XMLTYPE('<tables><table name="таблица3"></table></tables>').appendChildXML('/tables/table',l_tab,NULL);
			END IF;
			L_XML := PKG_CONTROLLER.F_DOCX_XML(P_SQL => L_SQL,P_REP_CODE => 'PEACE_AGREE_PENY');   
		ELSE
			L_TAB := L_TAB.deleteXML('rows/row/cell[@tag="пени_таб_во" or @tag="пени_таб_хвс"]',NULL);
			IF L_ID_CWS IS NOT NULL AND L_ID_SEW IS NOT NULL THEN       
				 L_TAB := XMLTYPE('<tables><table name="таблица1"></table></tables>').appendChildXML('/tables/table',l_tab,NULL);
			ELSIF L_ID_CWS IS NOT NULL AND L_ID_SEW IS NULL THEN    																																	 
				 L_TAB := L_TAB.deleteXML('rows/row/cell[@tag="долг_таб_во"]',NULL);	
 				 L_TAB := XMLTYPE('<tables><table name="таблица2"></table></tables>').appendChildXML('/tables/table',l_tab,NULL);
			ELSIF L_ID_CWS IS NULL AND L_ID_SEW IS NOT NULL THEN	                                                             
				 L_TAB := L_TAB.deleteXML('rows/row/cell[@tag="долг_таб_хвс"]',NULL);	
 				 L_TAB := XMLTYPE('<tables><table name="таблица3"></table></tables>').appendChildXML('/tables/table',l_tab,NULL);
			END IF;
			L_XML := PKG_CONTROLLER.F_DOCX_XML(P_SQL => L_SQL,P_REP_CODE => 'PEACE_AGREE');   	
		END IF;		 
      
		L_XML := L_XML.APPENDCHILDXML('/request/input-data',l_tab,NULL);
				  										  						
    L_REP.P_BLOB := PKG_CONTROLLER.F_GET_REPORT_XML(L_XML.getClobVal());
    L_REP.P_FILE_NAME := 'Мировое соглашение';
		
		
		
		RETURN l_rep;
	END;										
	
end PKG_DOCX_003;
/

