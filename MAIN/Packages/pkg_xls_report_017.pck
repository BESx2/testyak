i�?create or replace package lawmain.PKG_XLS_REPORT_017 is

  -- Author  : BES
  -- Created : 17.02.2017
  -- Purpose : Отчет о произведенной работе
	-- Code:    WORK_DONE_PRETRIAL
	
  
  FUNCTION RUN_REPORT(P_PAR_01 VARCHAR2 DEFAULT NULL
											,P_PAR_02 VARCHAR2 DEFAULT NULL
											,P_PAR_03 VARCHAR2 DEFAULT NULL
											,P_PAR_04 VARCHAR2 DEFAULT NULL
											,P_PAR_05 VARCHAR2 DEFAULT NULL
											,P_PAR_06 VARCHAR2 DEFAULT NULL
											,P_PAR_07 VARCHAR2 DEFAULT NULL
											,P_PAR_08 VARCHAR2 DEFAULT NULL
											,P_PAR_09 VARCHAR2 DEFAULT NULL
											,P_PAR_10 VARCHAR2 DEFAULT NULL) RETURN LAWSUP.PKG_FILES.REC_DOC;	  

end PKG_XLS_REPORT_017;
/

create or replace package body lawmain.PKG_XLS_REPORT_017 is

	TYPE REC_DOC IS RECORD(	P_BLOB BLOB DEFAULT NULL );

	EXCEL_DOC REC_DOC := NULL;
------------------------------------------------------------------------

	PROCEDURE p$s( ps_value IN CLOB )  -- отправляет накопленное значение ps_value в буфер и записывает
	IS
		BUFFER    	RAW(32767);
    l_offset    NUMBER := 1;
    BUF_SIZE    NUMBER := 8000;
    BUF_VAR    VARCHAR2(32767);
  BEGIN
    LOOP
    EXIT WHEN L_OFFSET > DBMS_LOB.getlength(PS_VALUE);
    BUF_VAR := DBMS_LOB.substr(PS_VALUE,BUF_SIZE,L_OFFSET);
		BUFFER := UTL_RAW.cast_to_raw( CONVERT( BUF_VAR, 'UTF8'/*'CL8MSWIN1251'*/ ) );
		DBMS_LOB.WRITEAPPEND( EXCEL_DOC.P_BLOB, UTL_RAW.LENGTH( BUFFER ), BUFFER );
    L_OFFSET := L_OFFSET + BUF_SIZE;
    END LOOP;
  END;
------------------------------------------------------------------------
	PROCEDURE P_ADD_DATA(P_DATE_FROM DATE,P_DATE_TO DATE,P_GROUP VARCHAR2) IS 
	  L_XML CLOB;
		L_CNT NUMBER := 0;
		L_FMT VARCHAR2(50) := '9999999999999D00';
		L_NLS VARCHAR2(50) := 'NLS_NUMERIC_CHARACTERS=''. ''';		
		L_DATE_START DATE;
    L_DATE_END   DATE;     
		L_TYPE_WORK VARCHAR2(100);
		L_ID_CLI    NUMBER;        
		
		--ИТОГОВЫЕ СУММЫ В РАЗРЕЗЕ РАБОТЫ
		L_TOT_WORK_CNT NUMBER := 0;
		L_TOT_WORK_SUM NUMBER := 0;
		L_TOT_DOC_DEBT NUMBER := 0;
	  L_TOT_DOC_PAY  NUMBER := 0;
		L_TOT_DOC_KOR  NUMBER := 0;   
		L_TOT_DOC_KOR_DEBT NUMBER := 0;
		L_TOT_DEBT     NUMBER := 0;
		

		L_NEW_CLI  BOOLEAN DEFAULT TRUE;
		L_NEW_TYPE BOOLEAN DEFAULT TRUE;
		
		L_CNT_WORK    NUMBER;
		
	BEGIN       
		L_DATE_START := NVL(P_DATE_FROM,DATE '2000-01-01');
		L_DATE_END   := NVL(P_DATE_TO,SYSDATE);  
		
		L_XML :=
			'<?xml version="1.0"?>
<?mso-application progid="Excel.Sheet"?>
<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:o="urn:schemas-microsoft-com:office:office"
 xmlns:x="urn:schemas-microsoft-com:office:excel"
 xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:html="http://www.w3.org/TR/REC-html40">
 <DocumentProperties xmlns="urn:schemas-microsoft-com:office:office">
  <Author></Author>
  <LastAuthor></LastAuthor>
  <LastPrinted></LastPrinted>
  <Created></Created>
  <Company></Company>
  <Version>16.00</Version>
 </DocumentProperties>
 <OfficeDocumentSettings xmlns="urn:schemas-microsoft-com:office:office">
  <AllowPNG/>
 </OfficeDocumentSettings>
 <ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel">
  <WindowHeight>10305</WindowHeight>
  <WindowWidth>23790</WindowWidth>
  <WindowTopX>0</WindowTopX>
  <WindowTopY>0</WindowTopY>
  <RefModeR1C1/>
  <ProtectStructure>False</ProtectStructure>
  <ProtectWindows>False</ProtectWindows>
 </ExcelWorkbook>
 <Styles>
  <Style ss:ID="Default" ss:Name="Normal">
   <Alignment ss:Vertical="Bottom"/>
   <Borders/>
   <Font ss:FontName="Calibri" x:CharSet="204" x:Family="Swiss" ss:Size="11"
    ss:Color="#000000"/>
   <Interior/>
   <NumberFormat/>
   <Protection/>
  </Style>
  <Style ss:ID="s69">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Interior ss:Color="#C6E0B4" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s80">
   <Alignment ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
  </Style>
  <Style ss:ID="s81">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
  </Style>
  <Style ss:ID="s92">
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Calibri" x:CharSet="204" x:Family="Swiss" ss:Size="11"
    ss:Color="#000000" ss:Bold="1"/>
   <Interior ss:Color="#C6E0B4" ss:Pattern="Solid"/>
   <NumberFormat ss:Format="Standard"/>
  </Style>
  <Style ss:ID="s93">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Calibri" x:CharSet="204" x:Family="Swiss" ss:Size="11"
    ss:Color="#000000" ss:Bold="1"/>
   <Interior ss:Color="#C6E0B4" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s96">
   <Alignment ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Interior/>
   <NumberFormat ss:Format="Standard"/>
  </Style>
  <Style ss:ID="s97">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Calibri" x:CharSet="204" x:Family="Swiss" ss:Size="11"
    ss:Color="#000000" ss:Bold="1"/>
   <Interior ss:Color="#C6E0B4" ss:Pattern="Solid"/>
  </Style>
 </Styles>
 <Worksheet ss:Name="Лист1">
 
  <Table>
   <Column ss:AutoFitWidth="0" ss:Width="160"/>
   <Column ss:AutoFitWidth="0" ss:Width="100"/>
   <Column ss:AutoFitWidth="0" ss:Width="250"/>
   <Column ss:AutoFitWidth="0" ss:Width="100"/>
   <Column ss:AutoFitWidth="0" ss:Width="70"/>
   <Column ss:AutoFitWidth="0" ss:Width="100"/>
   <Row>
    <Cell ss:StyleID="s69"><Data ss:Type="String">Стадия</Data><NamedCell
      ss:Name="Print_Area"/></Cell>                                       
    <Cell ss:StyleID="s69"><Data ss:Type="String">Группа</Data><NamedCell
      ss:Name="Print_Area"/></Cell>	
    <Cell ss:StyleID="s69"><Data ss:Type="String">Контрагент</Data><NamedCell
      ss:Name="Print_Area"/></Cell>
    <Cell ss:StyleID="s69"><Data ss:Type="String">Договор</Data><NamedCell
      ss:Name="Print_Area"/></Cell>
    <Cell ss:StyleID="s69"><Data ss:Type="String">Количество</Data><NamedCell
      ss:Name="Print_Area"/></Cell>
    <Cell ss:StyleID="s69"><Data ss:Type="String">Сумма</Data><NamedCell
      ss:Name="Print_Area"/></Cell>		
   </Row>';
   p$s(L_XML);		                                                                  
		    
		 FOR I IN (SELECT WT.NAME_TYPE NAME_TYPE, 
                     CG.GROUP_NAME,
                     CLI.CLI_ALT_NAME,
                     CT.CTR_NUMBER,   
                     T.CNT,
										 T.SUM_DEBT
              FROM (SELECT D.ID_CONTRACT, DW.TYPE_WORK, COUNT(DISTINCT D.ID_WORK) CNT, SUM(D.DEBT_CREATED) SUM_DEBT
                    FROM T_CLI_DEBTS D,
										     T_CLI_DEBT_WORK DW                                      
                    WHERE D.DATE_CREATED BETWEEN L_DATE_START AND L_DATE_END   
										AND DW.TYPE_WORK IN ('CASE','SHUTDOWN','RESTRUCT')
										AND   D.ID_WORK = DW.Id_Work
										GROUP BY D.ID_CONTRACT, DW.TYPE_WORK) T,             
                   T_CLI_DEBT_WORK_TYPE WT,
                   T_CLIENTS CLI,  
                   T_CUSTOMER_GROUP CG,
                   T_CONTRACTS CT     
              WHERE T.ID_CONTRACT = CT.ID_CONTRACT
							AND   T.TYPE_WORK = WT.CODE_TYPE							
              AND CLI.ID_CLIENT = CT.ID_CLIENT
              AND CG.GROUP_NAME = CT.CUST_GROUP
              AND (CG.GROUP_NAME IN (SELECT COLUMN_VALUE FROM TABLE(APEX_STRING.split(P_GROUP,':'))) OR P_GROUP IS NULL))
		 LOOP      
			  										 
				L_XML := '<Row>'||CHR(13);
				L_XML := L_XML || '<Cell ss:StyleID="s80"><Data ss:Type="String">'||I.NAME_TYPE||'</Data><NamedCell ss:Name="Print_Area"/></Cell>'||CHR(13);
				L_XML := L_XML || '<Cell ss:StyleID="s81"><Data ss:Type="String">'||I.GROUP_NAME||'</Data><NamedCell ss:Name="Print_Area"/></Cell>'||CHR(13);
				L_XML := L_XML || '<Cell ss:StyleID="s81"><Data ss:Type="String">'||I.CLI_ALT_NAME||'</Data><NamedCell ss:Name="Print_Area"/></Cell>'||CHR(13);
				L_XML := L_XML || '<Cell ss:StyleID="s80"><Data ss:Type="String">'||i.Ctr_Number||'</Data><NamedCell ss:Name="Print_Area"/></Cell>'||CHR(13);
				L_XML := L_XML || '<Cell ss:StyleID="s96"><Data ss:Type="Number">'||TO_CHAR(i.cnt)||'</Data><NamedCell ss:Name="Print_Area"/></Cell>'||CHR(13);
				L_XML := L_XML || '<Cell ss:StyleID="s96"><Data ss:Type="Number">'||TO_CHAR(i.Sum_Debt,L_FMT,L_NLS)||'</Data><NamedCell ss:Name="Print_Area"/></Cell>'||CHR(13);				
				L_XML := L_XML || '</Row>'||CHR(13);
			  P$S(L_XML);
			
		 END LOOP;                                            	 
		
   

	 L_XML := '  </Table>
  <WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">
   <PageSetup>
    <Header x:Margin="0.31496062992125984"/>
    <Footer x:Margin="0.31496062992125984"/>
    <PageMargins x:Bottom="0.74803149606299213" x:Left="0.70866141732283472"
     x:Right="0.70866141732283472" x:Top="0.74803149606299213"/>
   </PageSetup>
   <FitToPage/>
   <Print>
    <FitHeight>0</FitHeight>
    <ValidPrinterInfo/>
    <PaperSizeIndex>9</PaperSizeIndex>
    <Scale>38</Scale>
    <HorizontalResolution>-3</HorizontalResolution>
    <VerticalResolution>0</VerticalResolution>
   </Print>
   <PageBreakZoom>60</PageBreakZoom>
   <Selected/>
   <Panes>
    <Pane>
     <Number>3</Number>
     <ActiveRow>10</ActiveRow>
     <ActiveCol>3</ActiveCol>
    </Pane>
   </Panes>
   <ProtectObjects>False</ProtectObjects>
   <ProtectScenarios>False</ProtectScenarios>
  </WorksheetOptions>
 </Worksheet>
</Workbook>';

		p$s( L_XML );
	
	
	END P_ADD_DATA;

---------------------------------------------------------------------------------------------------------------------------------

  FUNCTION RUN_REPORT(P_PAR_01 VARCHAR2 DEFAULT NULL
											,P_PAR_02 VARCHAR2 DEFAULT NULL
											,P_PAR_03 VARCHAR2 DEFAULT NULL
											,P_PAR_04 VARCHAR2 DEFAULT NULL
											,P_PAR_05 VARCHAR2 DEFAULT NULL
											,P_PAR_06 VARCHAR2 DEFAULT NULL
											,P_PAR_07 VARCHAR2 DEFAULT NULL
											,P_PAR_08 VARCHAR2 DEFAULT NULL
											,P_PAR_09 VARCHAR2 DEFAULT NULL
											,P_PAR_10 VARCHAR2 DEFAULT NULL) RETURN LAWSUP.PKG_FILES.REC_DOC
	IS
	l_BLOB LAWSUP.PKG_FILES.REC_DOC;
  BEGIN

	  EXCEL_DOC := NULL;
		DBMS_LOB.CREATETEMPORARY( EXCEL_DOC.P_BLOB, TRUE );
    DBMS_LOB.OPEN( EXCEL_DOC.P_BLOB, DBMS_LOB.LOB_READWRITE );

		P_ADD_DATA(TO_DATE(P_PAR_02,'DD.MM.YYYY'),
							 TO_DATE(P_PAR_03,'DD.MM.YYYY'),
		           P_PAR_07);

		DBMS_LOB.CLOSE(EXCEL_DOC.P_BLOB);

		    l_BLOB.P_BLOB := EXCEL_DOC.P_BLOB;
	      l_BLOB.P_FILE_NAME := 'Произведенная работа';
				RETURN L_BLOB;

  END RUN_REPORT;

end PKG_XLS_REPORT_017;
/

