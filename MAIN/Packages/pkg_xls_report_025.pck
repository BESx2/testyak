i�?create or replace package lawmain.PKG_XLS_REPORT_025 is

  -- Author  : BES
  -- Created : 06.08.2019
  -- Purpose : Расчет суммы пени для исп. произво.
	-- Code:    CALL_RESULTS
	

	
	
  
  FUNCTION RUN_REPORT(P_PAR_01 VARCHAR2 DEFAULT NULL
											,P_PAR_02 VARCHAR2 DEFAULT NULL
											,P_PAR_03 VARCHAR2 DEFAULT NULL
											,P_PAR_04 VARCHAR2 DEFAULT NULL
											,P_PAR_05 VARCHAR2 DEFAULT NULL
											,P_PAR_06 VARCHAR2 DEFAULT NULL
											,P_PAR_07 VARCHAR2 DEFAULT NULL
											,P_PAR_08 VARCHAR2 DEFAULT NULL
											,P_PAR_09 VARCHAR2 DEFAULT NULL
											,P_PAR_10 VARCHAR2 DEFAULT NULL) RETURN LAWSUP.PKG_FILES.REC_DOC;	  

end PKG_XLS_REPORT_025;
/

create or replace package body lawmain.PKG_XLS_REPORT_025 is

	TYPE REC_DOC IS RECORD(	P_BLOB BLOB DEFAULT NULL );

	EXCEL_DOC REC_DOC := NULL;
------------------------------------------------------------------------

------------------------------------------------------------------------

	PROCEDURE p$s( ps_value IN CLOB )  -- отправляет накопленное значение ps_value в буфер и записывает
	IS
		BUFFER    	RAW(32767);
    l_offset    NUMBER := 1;
    BUF_SIZE    NUMBER := 8000;
    BUF_VAR    VARCHAR2(32767);
  BEGIN
    LOOP
    EXIT WHEN L_OFFSET > DBMS_LOB.getlength(PS_VALUE);
    BUF_VAR := DBMS_LOB.substr(PS_VALUE,BUF_SIZE,L_OFFSET);
		BUFFER := UTL_RAW.cast_to_raw( CONVERT( BUF_VAR, 'UTF8'/*'CL8MSWIN1251'*/ ) );
		DBMS_LOB.WRITEAPPEND( EXCEL_DOC.P_BLOB, UTL_RAW.LENGTH( BUFFER ), BUFFER );
    L_OFFSET := L_OFFSET + BUF_SIZE;
    END LOOP;
  END;
------------------------------------------------------------------------
	PROCEDURE P_ADD_DATA(P_ID_FOLDER NUMBER,P_ID_TASK VARCHAR2) IS
	  L_XML CLOB;
	
	BEGIN
		L_XML :=
			'<?xml version="1.0"?>
<?mso-application progid="Excel.Sheet"?>
<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:o="urn:schemas-microsoft-com:office:office"
 xmlns:x="urn:schemas-microsoft-com:office:excel"
 xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:html="http://www.w3.org/TR/REC-html40">
 <DocumentProperties xmlns="urn:schemas-microsoft-com:office:office">
  <Author></Author>
  <LastAuthor></LastAuthor>
  <Created></Created>
  <Version>16.00</Version>
 </DocumentProperties>
 <OfficeDocumentSettings xmlns="urn:schemas-microsoft-com:office:office">
  <AllowPNG/>
 </OfficeDocumentSettings>
 <ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel">
  <WindowHeight>12300</WindowHeight>
  <WindowWidth>28800</WindowWidth>
  <WindowTopX>0</WindowTopX>
  <WindowTopY>0</WindowTopY>
  <RefModeR1C1/>
  <ProtectStructure>False</ProtectStructure>
  <ProtectWindows>False</ProtectWindows>
 </ExcelWorkbook>
 <Styles>
  <Style ss:ID="Default" ss:Name="Normal">
   <Alignment ss:Vertical="Bottom"/>
   <Borders/>
   <Font ss:FontName="Calibri" x:CharSet="204" x:Family="Swiss" ss:Size="11"
    ss:Color="#000000"/>
   <Interior/>
   <NumberFormat/>
   <Protection/>
  </Style>
  <Style ss:ID="s78">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Interior ss:Color="#E2EFDA" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s79">
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
  </Style>
  <Style ss:ID="s80">
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <NumberFormat ss:Format="Standard"/>
  </Style>
  <Style ss:ID="s82">
   <Alignment ss:Horizontal="Center" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <NumberFormat ss:Format="@"/>
  </Style>
  <Style ss:ID="s85">
   <Alignment ss:Horizontal="Center" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
  </Style>
 </Styles>
 <Worksheet ss:Name="обзвон">
  <Table>
   <Column ss:Width="217.5"/>
   <Column ss:AutoFitWidth="0" ss:Width="80"/>
   <Column ss:AutoFitWidth="0" ss:Width="80"/>
   <Column ss:AutoFitWidth="0" ss:Width="132"/>
   <Column ss:AutoFitWidth="0" ss:Width="114.75"/>
   <Column ss:Width="120"/>
   <Column ss:Width="121.5"/>
   <Row ss:AutoFitHeight="0" ss:Height="35.25">
    <Cell ss:StyleID="s78"><Data ss:Type="String">Абонент</Data></Cell>
    <Cell ss:StyleID="s78"><Data ss:Type="String">Договор ХВС</Data></Cell>
    <Cell ss:StyleID="s78"><Data ss:Type="String">Договор ВО</Data></Cell>
    <Cell ss:StyleID="s78"><Data ss:Type="String">Группа </Data></Cell>
    <Cell ss:StyleID="s78"><Data ss:Type="String">Долг</Data></Cell>
    <Cell ss:StyleID="s78"><Data ss:Type="String">Телефон</Data></Cell>
    <Cell ss:StyleID="s78"><Data ss:Type="String">Статус звонка</Data></Cell>
   </Row>';
   p$s(L_XML);
   
	 
 	 FOR I IN (SELECT CLI.CLI_ALT_NAME,
									 CWS.CTR_CODE CWS_CTR_CODE,
									 SEW.CTR_CODE SEW_CTR_CODE,
									 COALESCE(CWS.CUST_GROUP,SEW.CUST_GROUP) GROUP_NAME,
									 AC.DEBT_CREATED,      
									 AC.PHONE,
									 (SELECT S.CODE_DESC FROM LAWSUP.T_CALL_STATUSES S WHERE S.CODE_STATUS = AC.CALL_STATUS) CALL_STATUS
						FROM T_CLIENTS CLI,
								 T_CONTRACTS CWS,
								 T_CONTRACTS SEW,
								 T_CLI_DEBT_WORK W,
								 T_CLI_AUTO_CALL AC								 
						WHERE AC.ID_FOLDER = P_ID_FOLDER
							AND AC.ID_WORK = W.ID_WORK
							AND W.ID_CLIENT = CLI.ID_CLIENT
							AND AC.ID_CWS_CONTRACT = CWS.ID_CONTRACT(+)
							AND AC.ID_SEW_CONTRACT = SEW.ID_CONTRACT(+))   
   LOOP
			L_XML := '<Row>'||CHR(13);
			L_XML := L_XML ||'   <Cell ss:StyleID="s79"><Data ss:Type="String">'||I.CLI_ALT_NAME||'</Data></Cell>'||CHR(13);
			L_XML := L_XML ||'   <Cell ss:StyleID="s82"><Data ss:Type="String">'||I.CWS_CTR_CODE||'</Data></Cell>'||CHR(13);
			L_XML := L_XML ||'   <Cell ss:StyleID="s82"><Data ss:Type="String">'||I.SEW_CTR_CODE||'</Data></Cell>'||CHR(13);
			L_XML := L_XML ||'   <Cell ss:StyleID="s79"><Data ss:Type="String">'||I.GROUP_NAME||'</Data></Cell>'||CHR(13);
			L_XML := L_XML ||'   <Cell ss:StyleID="s80"><Data ss:Type="Number">'||TO_CHAR(I.DEBT_CREATED,'FM99999999999999999999D99','NLS_NUMERIC_CHARACTERS=''. ''')||'</Data></Cell>'||CHR(13);
			L_XML := L_XML ||'   <Cell ss:StyleID="s82"><Data ss:Type="String">'||I.PHONE||'</Data></Cell>'||CHR(13);
			L_XML := L_XML ||'   <Cell ss:StyleID="s79"><Data ss:Type="String">'||I.CALL_STATUS||'</Data></Cell>'||CHR(13);
			L_XML := L_XML ||'  </Row>';
			 p$s(L_XML);
	 END LOOP;						
      


	 L_XML := '  </Table>
  <WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">
   <PageSetup>
    <Header x:Margin="0.3"/>
    <Footer x:Margin="0.3"/>
    <PageMargins x:Bottom="0.75" x:Left="0.7" x:Right="0.7" x:Top="0.75"/>
   </PageSetup>
   <Print>
    <ValidPrinterInfo/>
    <HorizontalResolution>600</HorizontalResolution>
    <VerticalResolution>600</VerticalResolution>
   </Print>
   <Selected/>
   <Panes>
    <Pane>
     <Number>3</Number>
     <ActiveRow>1</ActiveRow>
     <ActiveCol>1</ActiveCol>
    </Pane>
   </Panes>
   <ProtectObjects>False</ProtectObjects>
   <ProtectScenarios>False</ProtectScenarios>
  </WorksheetOptions>
 </Worksheet>
</Workbook>';

		p$s( L_XML );
	
	
	END P_ADD_DATA;

---------------------------------------------------------------------------------------------------------------------------------

  FUNCTION RUN_REPORT(P_PAR_01 VARCHAR2 DEFAULT NULL
											,P_PAR_02 VARCHAR2 DEFAULT NULL
											,P_PAR_03 VARCHAR2 DEFAULT NULL
											,P_PAR_04 VARCHAR2 DEFAULT NULL
											,P_PAR_05 VARCHAR2 DEFAULT NULL
											,P_PAR_06 VARCHAR2 DEFAULT NULL
											,P_PAR_07 VARCHAR2 DEFAULT NULL
											,P_PAR_08 VARCHAR2 DEFAULT NULL
											,P_PAR_09 VARCHAR2 DEFAULT NULL
											,P_PAR_10 VARCHAR2 DEFAULT NULL) RETURN LAWSUP.PKG_FILES.REC_DOC
	IS
	l_BLOB LAWSUP.PKG_FILES.REC_DOC;
  BEGIN

	  EXCEL_DOC := NULL;
		DBMS_LOB.CREATETEMPORARY( EXCEL_DOC.P_BLOB, TRUE );
    DBMS_LOB.OPEN( EXCEL_DOC.P_BLOB, DBMS_LOB.LOB_READWRITE );

		P_ADD_DATA(TO_NUMBER(P_PAR_01),P_PAR_02);

		DBMS_LOB.CLOSE(EXCEL_DOC.P_BLOB);

		    l_BLOB.P_BLOB := EXCEL_DOC.P_BLOB;
	      l_BLOB.P_FILE_NAME := 'REZULTATI_OBZVONA_'||P_PAR_01;
				RETURN L_BLOB;

  END RUN_REPORT;

end PKG_XLS_REPORT_025;
/

