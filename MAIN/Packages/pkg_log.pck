i�?CREATE OR REPLACE PACKAGE LAWMAIN."PKG_LOG"
	IS

	PROCEDURE P$LOG(	p_PROG_UNIT 		VARCHAR2 DEFAULT NULL
											,p_MESS				VARCHAR2	DEFAULT NULL
											,P_C1 in varchar2 default null
											,P_C2 in varchar2 default null
											,P_C3 in varchar2 default null
											,P_C4 in varchar2 default null
											,P_C5 in varchar2 default null
											,P_C6 in varchar2 default null
											,P_C7 in varchar2 default null
											,P_C8 in varchar2 default null
											,P_C9 in varchar2 default null
											,P_C10 in varchar2 default null
											,P_n1 in varchar2 default null
											,P_n2 in varchar2 default null
											,P_n3 in varchar2 default null
											,P_n4 in varchar2 default null
											,P_n5 in varchar2 default null
											,P_n6 in varchar2 default null
											,P_n7 in varchar2 default null
											,P_n8 in varchar2 default null
											,P_n9 in varchar2 default null
											,P_n10 in varchar2 default null
											,P_d1 in varchar2 default null
											,P_d2 in varchar2 default null
											,P_d3 in varchar2 default null
											,P_d4 in varchar2 default null
											,P_d5 in varchar2 default null
											,P_d6 in varchar2 default null
											,P_d7 in varchar2 default null
											,P_d8 in varchar2 default null
											,P_d9 in varchar2 default null
											,P_d10 in varchar2 default null

                        );

	FUNCTION f$LOG(
										p_PROG_UNIT 		VARCHAR2 DEFAULT NULL
										,p_MESS				VARCHAR2	DEFAULT NULL
										,P_C1 in varchar2 default null
										,P_C2 in varchar2 default null
										,P_C3 in varchar2 default null
										,P_C4 in varchar2 default null
										,P_C5 in varchar2 default null
										,P_C6 in varchar2 default null
										,P_C7 in varchar2 default null
										,P_C8 in varchar2 default null
										,P_C9 in varchar2 default null
										,P_C10 in varchar2 default null
										,P_n1 in varchar2 default null
										,P_n2 in varchar2 default null
										,P_n3 in varchar2 default null
										,P_n4 in varchar2 default null
										,P_n5 in varchar2 default null
										,P_n6 in varchar2 default null
										,P_n7 in varchar2 default null
										,P_n8 in varchar2 default null
										,P_n9 in varchar2 default null
										,P_n10 in varchar2 default null
										,P_d1 in varchar2 default null
										,P_d2 in varchar2 default null
										,P_d3 in varchar2 default null
										,P_d4 in varchar2 default null
										,P_d5 in varchar2 default null
										,P_d6 in varchar2 default null
										,P_d7 in varchar2 default null
										,P_d8 in varchar2 default null
										,P_d9 in varchar2 default null
										,P_d10 in varchar2 default NULL
										) RETURN NUMBER;

	/*
	  Возвращает текст лога по ид.
	*/
  FUNCTION f_log_text (pi_id NUMBER) RETURN VARCHAR2;


END PKG_LOG;
/

CREATE OR REPLACE PACKAGE BODY LAWMAIN."PKG_LOG"
	IS
	PROCEDURE P$LOG(	p_PROG_UNIT 		VARCHAR2 DEFAULT NULL
											,p_MESS				VARCHAR2	DEFAULT NULL
											,P_C1 in varchar2 default null
											,P_C2 in varchar2 default null
											,P_C3 in varchar2 default null
											,P_C4 in varchar2 default null
											,P_C5 in varchar2 default null
											,P_C6 in varchar2 default null
											,P_C7 in varchar2 default null
											,P_C8 in varchar2 default null
											,P_C9 in varchar2 default null
											,P_C10 in varchar2 default null
											,P_n1 in varchar2 default null
											,P_n2 in varchar2 default null
											,P_n3 in varchar2 default null
											,P_n4 in varchar2 default null
											,P_n5 in varchar2 default null
											,P_n6 in varchar2 default null
											,P_n7 in varchar2 default null
											,P_n8 in varchar2 default null
											,P_n9 in varchar2 default null
											,P_n10 in varchar2 default null
											,P_d1 in varchar2 default null
											,P_d2 in varchar2 default null
											,P_d3 in varchar2 default null
											,P_d4 in varchar2 default null
											,P_d5 in varchar2 default null
											,P_d6 in varchar2 default null
											,P_d7 in varchar2 default null
											,P_d8 in varchar2 default null
											,P_d9 in varchar2 default null
											,P_d10 in varchar2 default NULL
									)
	IS
		PRAGMA AUTONOMOUS_TRANSACTION;
	BEGIN
		INSERT	INTO T_USER_LOG(	ID,
														PROGRAMM_UNIT,
														MESSAGE,
														PUT_TIME
														,C1
														,C2
														,C3
														,C4
														,C5
														,C6
														,C7
														,C8
														,C9
														,c10
														,n1
														,n2
														,n3
														,n4
														,n5
														,n6
														,n7
														,n8
														,n9
														,n10
														,d1
														,d2
														,d3
														,d4
														,d5
														,d6
														,d7
														,d8
														,d9
														,d10
														)
							VALUES(	SEQ_MAIN.Nextval,
											P_PROG_UNIT,
											P_MESS,
											SYSDATE
											,p_C1
											,p_C2
											,p_C3
											,p_C4
											,p_C5
											,p_C6
											,p_C7
											,p_C8
											,p_C9
											,p_C10
											,p_n1
											,p_n2
											,p_n3
											,p_n4
											,p_n5
											,p_n6
											,p_n7
											,p_n8
											,p_n9
											,p_n10
											,p_d1
											,p_d2
											,p_d3
											,p_d4
											,p_d5
											,p_d6
											,p_d7
											,p_d8
											,p_d9
											,p_d10
											);
		COMMIT;
	END;

	FUNCTION f$LOG(
										p_PROG_UNIT 		VARCHAR2 DEFAULT NULL
										,p_MESS				VARCHAR2	DEFAULT NULL
										,P_C1 in varchar2 default null
										,P_C2 in varchar2 default null
										,P_C3 in varchar2 default null
										,P_C4 in varchar2 default null
										,P_C5 in varchar2 default null
										,P_C6 in varchar2 default null
										,P_C7 in varchar2 default null
										,P_C8 in varchar2 default null
										,P_C9 in varchar2 default null
										,P_C10 in varchar2 default null
										,P_n1 in varchar2 default null
										,P_n2 in varchar2 default null
										,P_n3 in varchar2 default null
										,P_n4 in varchar2 default null
										,P_n5 in varchar2 default null
										,P_n6 in varchar2 default null
										,P_n7 in varchar2 default null
										,P_n8 in varchar2 default null
										,P_n9 in varchar2 default null
										,P_n10 in varchar2 default null
										,P_d1 in varchar2 default null
										,P_d2 in varchar2 default null
										,P_d3 in varchar2 default null
										,P_d4 in varchar2 default null
										,P_d5 in varchar2 default null
										,P_d6 in varchar2 default null
										,P_d7 in varchar2 default null
										,P_d8 in varchar2 default null
										,P_d9 in varchar2 default null
										,P_d10 in varchar2 default NULL

										) RETURN NUMBER
	IS
		PRAGMA AUTONOMOUS_TRANSACTION;
		ret_val NUMBER;
	BEGIN
		INSERT	INTO T_USER_LOG(	ID,
														PROGRAMM_UNIT,
														MESSAGE,
														PUT_TIME
														,C1
														,C2
														,C3
														,C4
														,C5
														,C6
														,C7
														,C8
														,C9
														,C10
														,n1
														,n2
														,n3
														,n4
														,n5
														,n6
														,n7
														,n8
														,n9
														,n10
														,d1
														,d2
														,d3
														,d4
														,d5
														,d6
														,d7
														,d8
														,d9
														,d10
														)
							VALUES(	SEQ_MAIN.Nextval,
											p_PROG_UNIT,
											p_MESS,
											SYSDATE
											,p_C1
											,p_C2
											,p_C3
											,p_C4
											,p_C5
											,p_C6
											,p_C7
											,p_C8
											,p_C9
											,p_C10
											,p_n1
											,p_n2
											,p_n3
											,p_n4
											,p_n5
											,p_n6
											,p_n7
											,p_n8
											,p_n9
											,p_n10
											,p_d1
											,p_d2
											,p_d3
											,p_d4
											,p_d5
											,p_d6
											,p_d7
											,p_d8
											,p_d9
											,p_d10
										) RETURNING id INTO ret_val;
		COMMIT;
		RETURN ret_val;
	END;


	/*
	  Возвращает текVARCHAR2 DEFAULT NULLт лога по ид.
	*/
  FUNCTION f_log_text (pi_id NUMBER) RETURN VARCHAR2
    IS
  BEGIN
    for w in (SELECT t.message
                FROM T_USER_LOG t
               WHERE t.id = pi_id
             )
    loop
      RETURN w.message;
    end loop;
    RETURN null;
  END;


END PKG_LOG;
/

