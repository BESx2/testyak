i�?create or replace package lawmain.PKG_XLS_REPORT_021 is

  -- Author  : BES
  -- Created : 17.02.2017
  -- Purpose : Сводный отчет по исполнительному производству
	-- Code:    FSSP_SVOD 
	
	

  FUNCTION RUN_REPORT(P_PAR_01 VARCHAR2 DEFAULT NULL
											,P_PAR_02 VARCHAR2 DEFAULT NULL
											,P_PAR_03 VARCHAR2 DEFAULT NULL
											,P_PAR_04 VARCHAR2 DEFAULT NULL
											,P_PAR_05 VARCHAR2 DEFAULT NULL
											,P_PAR_06 VARCHAR2 DEFAULT NULL
											,P_PAR_07 VARCHAR2 DEFAULT NULL
											,P_PAR_08 VARCHAR2 DEFAULT NULL
											,P_PAR_09 VARCHAR2 DEFAULT NULL
											,P_PAR_10 VARCHAR2 DEFAULT NULL) RETURN LAWSUP.PKG_FILES.REC_DOC;	  

end PKG_XLS_REPORT_021;
/

create or replace package body lawmain.PKG_XLS_REPORT_021 is

	TYPE REC_DOC IS RECORD(	P_BLOB BLOB DEFAULT NULL );

	EXCEL_DOC REC_DOC := NULL;
------------------------------------------------------------------------

	PROCEDURE p$s( ps_value IN CLOB )  -- отправляет накопленное значение ps_value в буфер и записывает
	IS
		BUFFER    	RAW(32767);
    l_offset    NUMBER := 1;
    BUF_SIZE    NUMBER := 8000;
    BUF_VAR    VARCHAR2(32767);
  BEGIN
    LOOP
    EXIT WHEN L_OFFSET > DBMS_LOB.getlength(PS_VALUE);
    BUF_VAR := DBMS_LOB.substr(PS_VALUE,BUF_SIZE,L_OFFSET);
		BUFFER := UTL_RAW.cast_to_raw( CONVERT( BUF_VAR, 'UTF8'/*'CL8MSWIN1251'*/ ) );
		DBMS_LOB.WRITEAPPEND( EXCEL_DOC.P_BLOB, UTL_RAW.LENGTH( BUFFER ), BUFFER );
    L_OFFSET := L_OFFSET + BUF_SIZE;
    END LOOP;
  END;
------------------------------------------------------------------------
	PROCEDURE P_ADD_DATA(P_DATE_FROM DATE, P_DATE_TO DATE) IS
	  L_XML CLOB;
		L_CNT NUMBER := 0;
		L_FMT VARCHAR2(50) := '9999999999999D00';
		L_NLS VARCHAR2(50) := 'NLS_NUMERIC_CHARACTERS=''. ''';         
		L_TOT_CNT1 NUMBER := 0; 
		L_TOT_SUM1 NUMBER := 0;
		L_TOT_CNT2 NUMBER := 0; 
		L_TOT_SUM2 NUMBER := 0;  
		
		L_DATE_START DATE := NVL(P_DATE_FROM,DATE '2000-01-01');
		L_DATE_END   DATE := NVL(P_DATE_TO +1-(INTERVAL '1' SECOND),DATE'3000-01-01');
		
		L_INWORK_CNT NUMBER := 0;
		L_INWORK_SUM NUMBER := 0;
		
		L_PAYED_BEFORE NUMBER;
		L_PAYED_AFTER NUMBER;
		L_PENY_BEFORE NUMBER;
		L_PENY_AFTER NUMBER;
	BEGIN    
	
		L_XML :=
			'<?xml version="1.0"?>
<?mso-application progid="Excel.Sheet"?>
<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:o="urn:schemas-microsoft-com:office:office"
 xmlns:x="urn:schemas-microsoft-com:office:excel"
 xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:html="http://www.w3.org/TR/REC-html40">
 <DocumentProperties xmlns="urn:schemas-microsoft-com:office:office">
  <Author>Кривошеина Татьяна Владимировна</Author>
  <LastAuthor>Evgeniy Borodulin</LastAuthor>
  <LastPrinted>2019-04-25T12:26:55Z</LastPrinted>
  <Created>2019-04-15T10:54:25Z</Created>
  <LastSaved>2019-06-21T10:39:41Z</LastSaved>
  <Company>Нижегородский водоканал, ОАО</Company>
  <Version>16.00</Version>
 </DocumentProperties>
 <OfficeDocumentSettings xmlns="urn:schemas-microsoft-com:office:office">
  <AllowPNG/>
 </OfficeDocumentSettings>
 <ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel">
  <WindowHeight>6975</WindowHeight>
  <WindowWidth>9555</WindowWidth>
  <WindowTopX>360</WindowTopX>
  <WindowTopY>45</WindowTopY>
  <RefModeR1C1/>
  <ProtectStructure>False</ProtectStructure>
  <ProtectWindows>False</ProtectWindows>
 </ExcelWorkbook>
 <Styles>
  <Style ss:ID="Default" ss:Name="Normal">
   <Alignment ss:Vertical="Bottom"/>
   <Borders/>
   <Font ss:FontName="Calibri" x:CharSet="204" x:Family="Swiss" ss:Size="11"
    ss:Color="#000000"/>
   <Interior/>
   <NumberFormat/>
   <Protection/>
  </Style>
  <Style ss:ID="s16">
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
  </Style>
  <Style ss:ID="s17">
   <Alignment ss:Vertical="Bottom" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
  </Style>
  <Style ss:ID="s18">
   <Alignment ss:Vertical="Bottom" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Calibri" x:CharSet="204" x:Family="Swiss" ss:Size="11"
    ss:Color="#000000" ss:Bold="1"/>
  </Style>
  <Style ss:ID="s19">
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Calibri" x:CharSet="204" x:Family="Swiss" ss:Size="11"
    ss:Color="#000000" ss:Bold="1"/>
  </Style>
  <Style ss:ID="s20">
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
  </Style>
  <Style ss:ID="s21">
   <Alignment ss:Vertical="Bottom" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
  </Style>
  <Style ss:ID="s22">
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
  </Style>
  <Style ss:ID="s25">
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
  </Style>
  <Style ss:ID="s26">
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
  </Style>
  <Style ss:ID="s27">
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
  </Style>
  <Style ss:ID="s28">
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
  </Style>
  <Style ss:ID="s29">
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Calibri" x:CharSet="204" x:Family="Swiss" ss:Size="11"
    ss:Color="#000000"/>
  </Style>
  <Style ss:ID="s31">
   <Alignment ss:Horizontal="Right" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Calibri" x:CharSet="204" x:Family="Swiss" ss:Size="11"
    ss:Color="#FF0000"/>
  </Style>
  <Style ss:ID="s33">
   <Alignment ss:Horizontal="Right" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Calibri" x:CharSet="204" x:Family="Swiss" ss:Size="11"
    ss:Color="#0070C0" ss:Bold="1"/>
  </Style>
  <Style ss:ID="s34">
   <Alignment ss:Horizontal="Center" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Calibri" x:CharSet="204" x:Family="Swiss" ss:Size="11"
    ss:Color="#0070C0" ss:Bold="1"/>
  </Style>
  <Style ss:ID="s35">
   <Alignment ss:Horizontal="Center" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Calibri" x:CharSet="204" x:Family="Swiss" ss:Size="11"
    ss:Color="#009900" ss:Bold="1"/>
  </Style>
  <Style ss:ID="s36">
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Calibri" x:CharSet="204" x:Family="Swiss" ss:Size="11"
    ss:Color="#009900" ss:Bold="1"/>
  </Style>
  <Style ss:ID="s86">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
  </Style>
  <Style ss:ID="s89">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Calibri" x:CharSet="204" x:Family="Swiss" ss:Size="11"
    ss:Color="#FF0000" ss:Bold="1"/>
  </Style>
  <Style ss:ID="s90">
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
  </Style>
  <Style ss:ID="s91">
   <Alignment ss:Horizontal="Right" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="2"/>
		<Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <NumberFormat ss:Format="Standard"/>
  </Style>
  <Style ss:ID="s95">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Calibri" x:CharSet="204" x:Family="Swiss" ss:Size="11"
    ss:Color="#FF0000" ss:Bold="1"/>
  </Style>
  <Style ss:ID="s99">
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Calibri" x:CharSet="204" x:Family="Swiss" ss:Size="11"
    ss:Color="#000000" ss:Bold="1"/>
  </Style>
  <Style ss:ID="s100">
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Calibri" x:CharSet="204" x:Family="Swiss" ss:Size="11"
    ss:Color="#000000" ss:Bold="1"/>
  </Style>
  <Style ss:ID="s101">
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
  </Style>
  <Style ss:ID="s102">
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
  </Style>
  <Style ss:ID="s103">
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
  </Style>
  <Style ss:ID="s146">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font ss:FontName="Microsoft Sans Serif" x:CharSet="204" x:Family="Swiss"
    ss:Size="9" ss:Bold="1"/>
   <Interior ss:Color="#D8E4BC" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s147">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font ss:FontName="Microsoft Sans Serif" x:CharSet="204" x:Family="Swiss"
    ss:Size="9" ss:Bold="1"/>
   <Interior ss:Color="#D8E4BC" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s148">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font ss:FontName="Microsoft Sans Serif" x:CharSet="204" x:Family="Swiss"
    ss:Size="9" ss:Bold="1"/>
   <Interior ss:Color="#D8E4BC" ss:Pattern="Solid"/>
   <NumberFormat ss:Format="Standard"/>
  </Style>
  <Style ss:ID="s149">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font ss:FontName="Microsoft Sans Serif" x:CharSet="204" x:Family="Swiss"
    ss:Size="9" ss:Bold="1"/>
   <Interior ss:Color="#D8E4BC" ss:Pattern="Solid"/>
   <NumberFormat ss:Format="Standard"/>
  </Style>
  <Style ss:ID="s150">
   <Alignment ss:Vertical="Bottom" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <NumberFormat ss:Format="dd\.mm\.yyyy\ hh:mm:ss"/>
  </Style>
  <Style ss:ID="s151">
   <Alignment ss:Vertical="Bottom" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <NumberFormat ss:Format="dd\.mm\.yyyy"/>
  </Style>
  <Style ss:ID="s152">
   <Alignment ss:Vertical="Bottom" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <NumberFormat ss:Format="Standard"/>
  </Style>
 </Styles>
 <Worksheet ss:Name="Свод">
  <Table >
   <Column ss:AutoFitWidth="0" ss:Width="29.25"/>
   <Column ss:AutoFitWidth="0" ss:Width="425.25"/>
   <Column ss:AutoFitWidth="0" ss:Width="66"/>
   <Column ss:AutoFitWidth="0" ss:Width="75"/>
   <Row ss:Height="15.75"/>
   <Row>
    <Cell ss:StyleID="s20"/>
    <Cell ss:StyleID="s21"/>
    <Cell ss:StyleID="s22"><Data ss:Type="String">кол-во </Data></Cell>
    <Cell ss:StyleID="s90"><Data ss:Type="String">сумма, руб.</Data></Cell>
   </Row>';
	 p$s(l_xml);
	 L_XML := ''; 
	 FOR I IN (SELECT DECODE(ST.CODE_STATUS,'WORK_FSSP','Находящиеся в ЦРА',ST.NAME_STATUS) NAME_STATUS ,
										 COUNT(S.ID_CASE) CNT,
										 SUM(NVL(L.LEVY_DEBT,0) + NVL(L.LEVY_PENY,0) + NVL(L.LEVY_GOV_TOLL,0)) DEBT																							 
							FROM T_CLI_CASE_STATUS_LOG SL,
									 T_CLI_CASES S, 
									 T_CLI_LEVY L,
									 T_CASE_STATUS ST
							WHERE SL.ID_LOG IN (SELECT MAX(L.ID_LOG) FROM T_CLI_CASE_STATUS_LOG L
							                    WHERE L.DATE_CHANGED <= L_DATE_END
																	AND   L.CODE_STATUS NOT IN ('LEVY_WAIT','CLOSE','COURT_EXEC_DONE')																						
																	GROUP BY L.ID_CASE) 										
							AND S.ID_CASE = SL.ID_CASE
							AND ST.CODE_STATUS = SL.CODE_STATUS  
							AND  L.ID_CASE = S.ID_CASE
							AND S.DEP IN ('FSSP','BANKRUPT')
							GROUP BY ST.CODE_STATUS, ST.NAME_STATUS)
	 LOOP     
		 L_TOT_CNT1 := L_TOT_CNT1 + I.CNT;
		 L_TOT_SUM1 := L_TOT_SUM1 + I.DEBT;
		 L_XML := L_XML||'<Row>
								<Cell ss:StyleID="s31"><Data ss:Type="String"></Data></Cell>
								<Cell ss:StyleID="s16"><Data ss:Type="String">'||I.NAME_STATUS||'</Data></Cell>
								<Cell ss:StyleID="s86"><Data ss:Type="Number">'||TO_CHAR(I.CNT)||'</Data></Cell>
								<Cell ss:StyleID="s91"><Data ss:Type="Number">'||TO_CHAR(I.DEBT,L_FMT,L_NLS)||'</Data></Cell>
							 </Row>';
	 END LOOP;					                   
	 L_XML :=  '<Row ss:AutoFitHeight="0" ss:Height="34.5">
    <Cell ss:StyleID="s99"><Data ss:Type="Number">1</Data></Cell>
    <Cell ss:StyleID="s18"><Data ss:Type="String">Общее количество исполнительных документов, находящихся на исполнении в отчетном периоде</Data></Cell>
    <Cell ss:StyleID="s86"><Data ss:Type="Number">'||TO_CHAR(L_TOT_CNT1)||'</Data></Cell>
    <Cell ss:StyleID="s91"><Data ss:Type="Number">'||TO_CHAR(L_TOT_SUM1,L_FMT,L_NLS)||'</Data></Cell>
   </Row>
	 <Row ss:AutoFitHeight="0" ss:Height="34.5">
    <Cell ss:StyleID="s36"></Cell>
    <Cell ss:StyleID="s18"><Data ss:Type="String">из них: количество вновь поступивших исполнительных документов (за заданный период)</Data></Cell>
    <Cell ss:StyleID="s86"><Data ss:Type="Number">#INWORK_CNT#</Data></Cell>
    <Cell ss:StyleID="s91"><Data ss:Type="Number">#INWORK_SUM#</Data></Cell>
   </Row>'||L_XML;
	 
   SELECT COUNT(S.ID_CASE),
          SUM(NVL(L.LEVY_DEBT,0) + NVL(L.LEVY_PENY,0) + NVL(L.LEVY_GOV_TOLL,0)) DEBT  		
	 INTO L_INWORK_CNT, L_INWORK_SUM						 				                                              
   FROM T_CLI_CASES S,
	      T_CLI_LEVY L
   WHERE EXISTS (SELECT 1
								 FROM V_CLI_EVENTS EV         
								 WHERE EV.code_event IN ('LEVY_GET')
								 AND   EV.id_cli_case = S.ID_CASE
								 AND   EV.DATE_EVENT BETWEEN L_DATE_START AND L_DATE_END)
   AND S.DEP IN ('FSSP','BANKRUPT')
	 AND L.ID_CASE = S.ID_CASE;                                                              
								 
   L_XML := REPLACE(L_XML,'#INWORK_CNT#',L_INWORK_CNT);
 	 L_XML := REPLACE(L_XML,'#INWORK_SUM#',TO_CHAR(L_INWORK_SUM,L_FMT,L_NLS));		
	 
	 P$S(L_XML);
	 
	 L_XML := '';
	 
	 FOR I IN (SELECT  CRT.CLOSE_REASON,
										 COUNT(S.ID_CASE) CNT,
										 SUM(NVL(L.LEVY_DEBT,0) + NVL(L.LEVY_PENY,0) + NVL(L.LEVY_GOV_TOLL,0)) DEBT																							 
							FROM T_CLI_CASES S,
									 V_CLI_EVENTS EVT,
									 (SELECT E.id_cli_case, MAX(E.id_event) ID_EVENT 
									  FROM T_CLI_EVENTS E
										GROUP BY E.ID_CLI_CASE) EV, 									  									          
									 T_CLI_LEVY L,
									 (SELECT CR.ID_REASON, CASE 
											 WHEN CR.CODE_REASON IN ('PAYED','ACT_IMPOSSIBLE','LIQUIDATION','SPISANO') THEN CR.REASON
											 ELSE 'По иным обстоятельствам'
										 END close_reason
										 FROM T_CASE_CLOSE_REASON CR) CRT									 
							WHERE 1=1                                                     
							AND S.ID_CASE = EV.ID_CLI_CASE
							AND EVT.ID_EVENT = EV.ID_EVENT
							AND EVT.DATE_EVENT BETWEEN L_DATE_START AND L_DATE_END
							AND EVT.code_event = 'CLOSE'
							AND CRT.ID_REASON = S.ID_REASON_CLOSE
							AND L.ID_CASE = S.ID_CASE
							AND S.DEP IN ('FSSP','BANKRUPT')
							GROUP BY CRT.CLOSE_REASON)
	 LOOP  
		 L_TOT_CNT2 := L_TOT_CNT2 + I.CNT;
		 L_TOT_SUM2 := L_TOT_SUM2 + I.DEBT;
		 L_XML := L_XML ||'<Row>
							<Cell ss:StyleID="s33"><Data ss:Type="String"></Data></Cell>
							<Cell ss:StyleID="s29"><Data ss:Type="String">'||I.CLOSE_REASON||'</Data></Cell>
							<Cell ss:StyleID="s86"><Data ss:Type="Number">'||TO_CHAR(I.CNT)||'</Data></Cell>
							<Cell ss:StyleID="s91"><Data ss:Type="Number">'||TO_CHAR(I.DEBT,L_FMT,L_NLS)||'</Data></Cell>
						 </Row>';
	 END LOOP;					
	 L_XML := '<Row>
							<Cell ss:StyleID="s99"><Data ss:Type="Number">2</Data></Cell>
							<Cell ss:StyleID="s19"><Data ss:Type="String">Количество оконченных исполнительных документов</Data></Cell>
							<Cell ss:StyleID="s86"><Data ss:Type="Number">'||L_TOT_CNT2||'</Data></Cell>
							<Cell ss:StyleID="s91"><Data ss:Type="Number">'||TO_CHAR(L_TOT_SUM2,L_FMT,L_NLS)||'</Data></Cell>
						 </Row>'||L_XML;
	 P$S(L_XML);				 
   
	 L_XML := '';
	 L_TOT_CNT1 := 0;
	 L_TOT_SUM1 := 0;
	 FOR I IN (SELECT DECODE(ST.CODE_STATUS,'WORK_FSSP','Находящиеся в ЦРА',ST.NAME_STATUS) NAME_STATUS ,
										 COUNT(S.ID_CASE) CNT,
										 SUM((SELECT NVL(SUM(FD.AMOUNT),0)
										  FROM T_CLI_FINDOC_CONS FD, 
											     T_CLI_DEBTS D 
											WHERE D.ID_WORK = S.ID_CASE
											AND   D.ID_DOC = FD.ID_DOC_TO
											AND   FD.DATE_CON <= L_DATE_END)
											+NVL(S.GOV_TOLL,0) + NVL(S.Peny_Court,0)  + NVL(S.Peny_Fact,0)
											-(SELECT NVL(SUM(P.AMOUNT),0) FROM T_CLI_PENALTY_PAYMENTS P 
											  WHERE P.DATE_PAY <= L_DATE_END
												AND P.ID_CASE = S.ID_CASE)) DEBT																					 
							FROM T_CLI_CASE_STATUS_LOG SL,
									 T_CLI_CASES S, 
									 T_CLI_LEVY L,
									 T_CASE_STATUS ST
							WHERE SL.ID_LOG IN (SELECT MAX(L.ID_LOG) FROM T_CLI_CASE_STATUS_LOG L
							                    WHERE L.DATE_CHANGED <= L_DATE_END																																					
																	GROUP BY L.ID_CASE)                                                     
							AND S.ID_CASE = SL.ID_CASE        
							AND SL.CODE_STATUS IN ('WORK_FSSP','LEVY_CLAIM_BANK','LEVY_CLAIM_DEPFIN',
																		'LEVY_CLAIM_ROSP','LEVY_CLAIM_UFK','REG_CRED')		
							AND ST.CODE_STATUS = SL.CODE_STATUS
							AND  L.ID_CASE = S.ID_CASE
							AND S.DEP IN ('FSSP','BANKRUPT')
							GROUP BY ST.CODE_STATUS, ST.NAME_STATUS)
	 LOOP     
		 L_TOT_CNT1 := L_TOT_CNT1 + I.CNT;
		 L_TOT_SUM1 := L_TOT_SUM1 + I.DEBT;
		 L_XML := L_XML||'<Row>
								<Cell ss:StyleID="s31"><Data ss:Type="String"></Data></Cell>
								<Cell ss:StyleID="s16"><Data ss:Type="String">'||I.NAME_STATUS||'</Data></Cell>
								<Cell ss:StyleID="s86"><Data ss:Type="Number">'||TO_CHAR(I.CNT)||'</Data></Cell>
								<Cell ss:StyleID="s91"><Data ss:Type="Number">'||TO_CHAR(I.DEBT,L_FMT,L_NLS)||'</Data></Cell>
							 </Row>';
	 END LOOP;					                   
	 L_XML :=  ' <Row ss:AutoFitHeight="0">
    <Cell ss:StyleID="s99"><Data ss:Type="Number">3</Data></Cell>
    <Cell ss:StyleID="s18"><Data ss:Type="String">Остаток исполнительных  документов на конец отчетного периода</Data></Cell>
    <Cell ss:StyleID="s86"><Data ss:Type="String">'||TO_CHAR(L_TOT_CNT1)||'</Data></Cell>
		<Cell ss:StyleID="s91"><Data ss:Type="Number">'||TO_CHAR(L_TOT_SUM1,L_FMT,L_NLS)||'</Data></Cell>
   </Row>'||L_XML;	 
	  P$S(L_XML);		
		
	BEGIN    
	 SELECT SUM((SELECT SUM(-FD.AMOUNT)
			  FROM T_CLI_DEBTS CD, T_CLI_FINDOC_CONS FD			 
				WHERE  CD.ID_WORK = CC.ID_CASE
				AND   (FD.ID_DOC_TO = CD.ID_DOC OR FD.ID_DOC_FROM = CD.ID_DOC)
				AND   FD.ID_DOC_FROM != FD.ID_DOC_TO
				AND   FD.AMOUNT < 0
				AND   FD.DATE_CON <= NVL(LM.MOV_DATE,SYSDATE))) PAYED_BEFORE,
				SUM((SELECT SUM(PP.AMOUNT)
				 FROM T_CLI_PENALTY_PAYMENTS PP
				 WHERE PP.ID_CASE = CC.ID_CASE 
				 AND   PP.DATE_PAY <= LM.MOV_DATE)) PENALTY_BEFORE ,
			 SUM((SELECT SUM(-FD.AMOUNT)
			  FROM T_CLI_DEBTS CD, T_CLI_FINDOC_CONS FD			 
				WHERE CD.ID_WORK = CC.ID_CASE
				AND   (FD.ID_DOC_TO = CD.ID_DOC OR FD.ID_DOC_FROM = CD.ID_DOC)
				AND   FD.ID_DOC_FROM != FD.ID_DOC_TO
				AND   FD.AMOUNT < 0
				AND   FD.DATE_CON >= LM.MOV_DATE)) PAYED_AFTER,     
				SUM((SELECT SUM(PP.AMOUNT)
				 FROM T_CLI_PENALTY_PAYMENTS PP
				 WHERE PP.ID_CASE = CC.ID_CASE 
				 AND   PP.DATE_PAY > LM.MOV_DATE)) PENALTY_AFTER
		INTO L_PAYED_BEFORE, L_PENY_BEFORE, L_PAYED_AFTER,L_PENY_AFTER 
				FROM V_CLI_CASES CC,
						 T_CLI_LEVY CL,          
						 T_CASE_STATUS ST,
						 T_CLI_COURT_EXEC E,		 
						 (SELECT LM1.ID_LEVY, MAX(LM1.MOV_SEND_DATE) MOV_DATE
							FROM T_CLI_LEVY_MOVING LM1   
							WHERE LM1.MOV_SEND_DATE BETWEEN L_DATE_START AND L_DATE_END
							GROUP BY LM1.ID_LEVY) LM,					 
						 (SELECT L1.ID_CASE, L1.CODE_STATUS, L1.DATE_CHANGED
							FROM T_CLI_CASE_STATUS_LOG L1
							LEFT OUTER JOIN T_CLI_CASE_STATUS_LOG L2
							ON (L1.ID_CASE = L2.ID_CASE AND L1.ID_LOG < L2.ID_LOG)
							WHERE L1.DATE_CHANGED  BETWEEN L_DATE_START AND L_DATE_END
							AND L2.ID_LOG IS NULL)	L			
				WHERE CC.ID_CASE = L.ID_CASE
				AND   CC.DEP IN ('FSSP','BANKRUPT')
				AND   CL.ID_CASE = CC.ID_CASE
				AND   E.ID = CC.LAST_EXEC
				AND   ST.CODE_STATUS = L.CODE_STATUS
				AND   CL.ID_LEVY = LM.ID_LEVY(+);
	 EXCEPTION 
			WHEN NO_DATA_FOUND THEN				
			L_PAYED_BEFORE := 0;
			L_PAYED_AFTER  := 0;
			L_PENY_BEFORE  := 0;
			L_PENY_AFTER   := 0;	
	 END;			
   
	 L_XML := '
   <Row>
    <Cell ss:StyleID="s99"><Data ss:Type="Number">4</Data></Cell>
    <Cell ss:StyleID="s19"><Data ss:Type="String">Оплата долга  до предъявления исполнит.документа</Data></Cell>
    <Cell ss:StyleID="s19"/>
    <Cell ss:StyleID="s91"><Data ss:Type="Number">'||TO_CHAR(NVL(L_PAYED_BEFORE,0)+NVL(L_PENY_BEFORE,0),L_FMT,L_NLS)||'</Data></Cell>
   </Row>
   <Row>
    <Cell ss:StyleID="s101"/>
    <Cell ss:StyleID="s102"><Data ss:Type="String">в том числе неустойка</Data></Cell>
    <Cell ss:StyleID="s102"></Cell>
    <Cell ss:StyleID="s91"><Data ss:Type="Number">'||TO_CHAR(NVL(L_PENY_BEFORE,0),L_FMT,L_NLS)||'</Data></Cell>
   </Row>
   <Row>
    <Cell ss:StyleID="s99"><Data ss:Type="Number">5</Data></Cell>
    <Cell ss:StyleID="s18"><Data ss:Type="String">Оплата после предъявления исполнительного документа </Data></Cell>
    <Cell ss:StyleID="s19"/>
    <Cell ss:StyleID="s91"><Data ss:Type="Number">'||TO_CHAR(NVL(L_PAYED_AFTER,0)+NVL(L_PENY_AFTER,0),L_FMT,L_NLS)||'</Data></Cell>
   </Row>
   <Row ss:Height="15.75">
    <Cell ss:StyleID="s26"/>
    <Cell ss:StyleID="s27"><Data ss:Type="String">в том числе неустойка</Data></Cell>
    <Cell ss:StyleID="s27"></Cell>
    <Cell ss:StyleID="s91"><Data ss:Type="Number">'||TO_CHAR(NVL(L_PENY_AFTER,0),L_FMT,L_NLS)||'</Data></Cell>
   </Row>
  </Table>
  <WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">
   <PageSetup>
    <Layout x:Orientation="Landscape"/>
    <Header x:Margin="0.3"/>
    <Footer x:Margin="0.3"/>
    <PageMargins x:Bottom="0.75" x:Left="0.25" x:Right="0.25" x:Top="0.75"/>
   </PageSetup>
   <Print>
    <ValidPrinterInfo/>
    <PaperSizeIndex>9</PaperSizeIndex>
    <HorizontalResolution>600</HorizontalResolution>
    <VerticalResolution>600</VerticalResolution>
   </Print>
   <Selected/>
   <TopRowVisible>3</TopRowVisible>
   <ProtectObjects>False</ProtectObjects>
   <ProtectScenarios>False</ProtectScenarios>
  </WorksheetOptions>
 </Worksheet>
</Workbook>
';

		p$s( L_XML );
	
	
	END P_ADD_DATA;

---------------------------------------------------------------------------------------------------------------------------------

  FUNCTION RUN_REPORT(P_PAR_01 VARCHAR2 DEFAULT NULL
											,P_PAR_02 VARCHAR2 DEFAULT NULL
											,P_PAR_03 VARCHAR2 DEFAULT NULL
											,P_PAR_04 VARCHAR2 DEFAULT NULL
											,P_PAR_05 VARCHAR2 DEFAULT NULL
											,P_PAR_06 VARCHAR2 DEFAULT NULL
											,P_PAR_07 VARCHAR2 DEFAULT NULL
											,P_PAR_08 VARCHAR2 DEFAULT NULL
											,P_PAR_09 VARCHAR2 DEFAULT NULL
											,P_PAR_10 VARCHAR2 DEFAULT NULL) RETURN LAWSUP.PKG_FILES.REC_DOC
	IS
	l_BLOB LAWSUP.PKG_FILES.REC_DOC;
  BEGIN

	  EXCEL_DOC := NULL;
		DBMS_LOB.CREATETEMPORARY( EXCEL_DOC.P_BLOB, TRUE );
    DBMS_LOB.OPEN( EXCEL_DOC.P_BLOB, DBMS_LOB.LOB_READWRITE );

		P_ADD_DATA(TO_DATE(P_PAR_02,'DD.MM.YYYY'),
		           TO_DATE(P_PAR_03,'DD.MM.YYYY'));

		DBMS_LOB.CLOSE(EXCEL_DOC.P_BLOB);

		    l_BLOB.P_BLOB := EXCEL_DOC.P_BLOB;
	      l_BLOB.P_FILE_NAME := 'Сводный отчет испол производства'||P_PAR_01;
				RETURN L_BLOB;

  END RUN_REPORT;

end PKG_XLS_REPORT_021;
/

