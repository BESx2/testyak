i�?CREATE OR REPLACE PACKAGE LAWMAIN.PKG_DOC_010 IS

	-- Author  : BES
	-- Created : 14.02.2019
	-- Purpose : Исковое заявление 
	-- CODE    : PEACE_AGREE

	FUNCTION RUN_REPORT(P_PAR_01 VARCHAR2 DEFAULT NULL
										 ,P_PAR_02 VARCHAR2 DEFAULT NULL
										 ,P_PAR_03 VARCHAR2 DEFAULT NULL
										 ,P_PAR_04 VARCHAR2 DEFAULT NULL
										 ,P_PAR_05 VARCHAR2 DEFAULT NULL
										 ,P_PAR_06 VARCHAR2 DEFAULT NULL
										 ,P_PAR_07 VARCHAR2 DEFAULT NULL
										 ,P_PAR_08 VARCHAR2 DEFAULT NULL
										 ,P_PAR_09 VARCHAR2 DEFAULT NULL
										 ,P_PAR_10 VARCHAR2 DEFAULT NULL)
		RETURN LAWSUP.PKG_FILES.REC_DOC;

END PKG_DOC_010;
/

CREATE OR REPLACE PACKAGE BODY LAWMAIN.PKG_DOC_010 IS

	TYPE REC_DOC IS RECORD(
		 P_BLOB BLOB DEFAULT NULL);

	RTF_DOC REC_DOC := NULL;

	------------------------------------------------------------------------       
	FUNCTION F_GET_TABLE(P_ID_EXEC NUMBER) RETURN CLOB
		IS
		L_RET CLOB;   
		L_HEADER CLOB;
		L_ROW_TMP CLOB; 
		L_FMT  VARCHAR2(50) := 'FM999G999G999G999G990D00';
		L_NLS  VARCHAR2(50) := 'NLS_NUMERIC_CHARACTERS='', ''';
	BEGIN                                                                  
		--Хедер таблицы
		L_HEADER := '\pard \ltrpar\s16\qj \li0\ri0\widctlpar\wrapdefault\faauto\rin0\lin0\itap0\pararsid8135856 {\rtlch\fcs1 \af1\afs24 \ltrch\fcs0 \fs24\insrsid10646706\charrsid8135856 
\par \ltrrow}\trowd \irow0\irowband0\ltrrow\ts18\trgaph108\trleft567\trbrdrt\brdrs\brdrw10 \trbrdrl\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 \trbrdrr\brdrs\brdrw10 \trbrdrh\brdrs\brdrw10 \trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth9639\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddft3\trpaddfb3\trpaddfr3\tblrsid8135856\tbllkhdrrows\tbllkhdrcols\tbllknocolband\tblind675\tblindtype3 \clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl
\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth3511\clshdrawnil \cellx4078\clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3152\clshdrawnil \cellx7230\clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth2976\clshdrawnil \cellx10206\pard\plain \ltrpar
\s16\qj \fi567\li0\ri0\widctlpar\intbl\wrapdefault\faauto\rin0\lin0\pararsid8135856\yts18 \rtlch\fcs1 \af1\afs20\alang1025 \ltrch\fcs0 \f1\fs20\lang1049\langfe1033\cgrid\langnp1049\langfenp1033 {\rtlch\fcs1 \af232\afs24 \ltrch\fcs0 
\b\f232\fs24\insrsid868083\charrsid8135856 \''ce\''f1\''ed\''ee\''e2\''ed\''ee\''e9 \''e4\''ee\''eb\''e3 \cell \''cd\''e5\''f3\''f1\''f2\''ee\''e9\''ea\''e0 (\''ef\''e5\''ed\''e8)\cell \''d1\''f0\''ee\''ea \''ee\''ef\''eb\''e0\''f2\''fb\cell }\pard\plain \ltrpar
\ql \li0\ri0\widctlpar\intbl\wrapdefault\aspalpha\aspnum\faauto\adjustright\rin0\lin0 \rtlch\fcs1 \af39\afs22\alang1025 \ltrch\fcs0 \f39\fs22\lang1049\langfe1033\cgrid\langnp1049\langfenp1033 {\rtlch\fcs1 \af1\afs24 \ltrch\fcs0 
\f1\fs24\insrsid868083\charrsid8135856 \trowd \irow0\irowband0\ltrrow\ts18\trgaph108\trleft567\trbrdrt\brdrs\brdrw10 \trbrdrl\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 \trbrdrr\brdrs\brdrw10 \trbrdrh\brdrs\brdrw10 \trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth9639\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddft3\trpaddfb3\trpaddfr3\tblrsid8135856\tbllkhdrrows\tbllkhdrcols\tbllknocolband\tblind675\tblindtype3 \clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl
\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth3511\clshdrawnil \cellx4078\clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3152\clshdrawnil \cellx7230\clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth2976\clshdrawnil \cellx10206\row \ltrrow}';

   --шаблон строки
    L_ROW_TMP := '\pard\plain \ltrpar
\s16\qj \fi567\li0\ri0\widctlpar\intbl\wrapdefault\faauto\rin0\lin0\pararsid8135856\yts18 \rtlch\fcs1 \af1\afs20\alang1025 \ltrch\fcs0 \f1\fs20\lang1049\langfe1033\cgrid\langnp1049\langfenp1033 {\rtlch\fcs1 \af1\afs24 \ltrch\fcs0 
\fs24\insrsid868083\charrsid8135856 PAYMENT \cell PENY \cell PAYDAY \cell }\pard\plain \ltrpar\ql \li0\ri0\widctlpar\intbl\wrapdefault\aspalpha\aspnum\faauto\adjustright\rin0\lin0 \rtlch\fcs1 \af39\afs22\alang1025 \ltrch\fcs0 
\f39\fs22\lang1049\langfe1033\cgrid\langnp1049\langfenp1033 {\rtlch\fcs1 \af1\afs24 \ltrch\fcs0 \f1\fs24\insrsid868083\charrsid8135856 \trowd \irow2\irowband2\lastrow \ltrrow\ts18\trgaph108\trleft567\trbrdrt\brdrs\brdrw10 \trbrdrl\brdrs\brdrw10 \trbrdrb
\brdrs\brdrw10 \trbrdrr\brdrs\brdrw10 \trbrdrh\brdrs\brdrw10 \trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth9639\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddft3\trpaddfb3\trpaddfr3\tblrsid8135856\tbllkhdrrows\tbllkhdrcols\tbllknocolband\tblind675\tblindtype3 \clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl
\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth3511\clshdrawnil \cellx4078\clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3152\clshdrawnil \cellx7230\clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth2976\clshdrawnil \cellx10206\row }';


    L_RET := L_HEADER;

    FOR I IN (SELECT P.PAY_DATE,
			               TO_CHAR(SUM(DET.SUM_PAY),L_FMT,L_NLS) PAYMENT, 
										 TO_CHAR(SUM(DET.PENY_SUM),L_FMT,L_NLS)  PENY
			        FROM T_CLI_PEACE_AGREE_PAYMENTS P,
							     T_CLI_AGREE_PAY_DET DET
							WHERE P.ID_EXEC = P_ID_EXEC
							AND   DET.ID_PAY = P.ID
							GROUP BY P.PAY_DATE
							ORDER BY P.PAY_DATE)
		LOOP
			L_RET := L_RET||L_ROW_TMP;
			L_RET := REPLACE(L_RET,'PAYDAY',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(TO_CHAR(I.PAY_DATE,'DD.MM.YYYY')));
			L_RET := REPLACE(L_RET,'PAYMENT',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(I.PAYMENT));			
			L_RET := REPLACE(L_RET,'PENY',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(I.PENY));
		END LOOP;					                                                     
		
		   
		RETURN L_RET;
	END;
	
	
	------------------------------------------------------------------------
	FUNCTION REPLACE_CLOB(IN_SOURCE  IN CLOB
											 ,IN_SEARCH  IN VARCHAR2
											 ,IN_REPLACE IN CLOB) RETURN CLOB IS
		L_POS PLS_INTEGER;
	BEGIN
		L_POS := INSTR(IN_SOURCE, IN_SEARCH);

		IF L_POS > 0 THEN
			RETURN SUBSTR(IN_SOURCE, 1, L_POS - 1) || IN_REPLACE || SUBSTR(IN_SOURCE, L_POS +
																																			LENGTH(IN_SEARCH));
		END IF;

		RETURN IN_SOURCE;
	END REPLACE_CLOB;

	PROCEDURE P_ADD_DATA(P_ID_EXEC NUMBER) IS
		L_BLOB BLOB;
		L_RTF  CLOB;  
		L_BODY CLOB;
		L_TEMP CLOB;
		L_VAL  VARCHAR2(10000);
		L_FMT  VARCHAR2(50) := 'FM999G999G999G999G990D00';
		L_NLS  VARCHAR2(50) := 'NLS_NUMERIC_CHARACTERS='', ''';
		L_CNT  NUMBER;     
		L_SUM  NUMBER;
	BEGIN
	
		SELECT T.FILE_CONTENT INTO L_RTF FROM T_RTF_TEMPLATES T WHERE  T.ID = 20;
	
		FOR I IN (SELECT A.DATE_FROM,
			               A.DATE_TO,
										 CC.NUM_EXEC,         
										 S.LAWSUIT_TOLL,
										 S.CLI_NAME,
										 S.CTR_NUMBER,
										 S.CTR_DATE,
										 PKG_CLIENTS.F_GET_ADDRESS_COURT(S.ID_CLIENT) ADDRESS,
										 A.SUM_DEBT,            
										 C.COURT_NAME,
										 C.ADDRESS COURT_ADDRESS,
 										 (SELECT LOWER(PKG_UTILS.F_GET_NAME_MONTHS(MIN(FD.DOC_DATE))||' '||TO_CHAR(MIN(FD.DOC_DATE),'YYYY')||' - '||
										        PKG_UTILS.F_GET_NAME_MONTHS(MAX(FD.DOC_DATE))||' '||TO_CHAR(MAX(FD.DOC_DATE),'YYYY'))
										  FROM T_CLI_DEBTS D, T_CLI_FINDOCS FD										 										
										  WHERE D.ID_WORK = S.ID_CASE
											AND   D.ID_DOC = FD.ID_DOC) PERIOD											
			        FROM T_CLI_PEACE_AGREE A,
							     T_CLI_COURT_EXEC CC,   
									 T_COURTS C,
									 V_CLI_CASES S
							WHERE A.ID_EXEC = P_ID_EXEC
							AND   CC.ID = A.ID_EXEC
							AND   C.ID_COURT = CC.ID_COURT
							AND   S.ID_CASE = CC.ID_CASE)
		LOOP
			L_RTF := REPLACE(L_RTF,'CLINAME',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(I.CLI_NAME));
			L_RTF := REPLACE(L_RTF,'CLIADDRESS',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(I.ADDRESS));
			L_RTF := REPLACE(L_RTF,'NUMCOURTCASE',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(I.NUM_EXEC));
			L_RTF := REPLACE(L_RTF,'AGREEDATEFROM',PKG_DOC_REPORTS.F_CONVERT_TO_RTF('«'||TO_CHAR(I.DATE_FROM,'DD')||'»'||' '||
																						 LOWER(PKG_UTILS.F_GET_NAME_MONTHS(I.DATE_FROM))||' '||TO_CHAR(I.DATE_FROM,'YYYY')));
																						 
			L_RTF := REPLACE(L_RTF,'CTRNUMBER',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(I.CTR_NUMBER));
			L_RTF := REPLACE(L_RTF,'CTRDATE',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(I.CTR_DATE));
			L_RTF := REPLACE(L_RTF,'PERIOD',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(I.PERIOD));
      L_RTF := REPLACE(L_RTF,'AGREEDATENUM',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(TO_CHAR(I.DATE_FROM,'DD.MM.YYYY')));
		  L_RTF := REPLACE(L_RTF,'COURTNAME',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(I.COURT_NAME));			
      L_RTF := REPLACE(L_RTF,'COURTADDRESS',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(I.COURT_ADDRESS));			
			L_RTF := REPLACE(L_RTF,'ORGANIZATION',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(PKG_PREF.F$C2('REQUISITES')));			
			L_RTF := REPLACE(L_RTF,'SUMDEBT',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(TO_CHAR(I.SUM_DEBT,L_FMT,L_NLS)));               
			
			L_RTF := REPLACE(L_RTF,'TEXTDEBT',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(LAWSUP.PKG_FMT_RU.number_in_words(TRUNC(I.SUM_DEBT))||
			                                                                   ' рублей'||' '||TO_CHAR(MOD(I.SUM_DEBT,1)*100)||' копеек'));
																																				 
			L_RTF := REPLACE(L_RTF,'AGREEDATEEND',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(TO_CHAR(I.DATE_TO,'DD.MM.YYYY')));																							
			L_RTF := REPLACE(L_RTF,'GOVTOLL',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(TO_CHAR(I.LAWSUIT_TOLL,L_FMT,L_NLS)));																							
		END LOOP;                                                                            
		  
	  L_RTF := REPLACE_CLOB(L_RTF,'TABLE',F_GET_TABLE(P_ID_EXEC));
				
		L_BLOB         := PKG_DOC_REPORTS.CLOB2BLOB(PAR_CLOB => L_RTF);
		RTF_DOC.P_BLOB := L_BLOB;
	
	END P_ADD_DATA;

	---------------------------------------------------------------------------------------------------------------------------------

	FUNCTION RUN_REPORT(P_PAR_01 VARCHAR2 DEFAULT NULL
										 ,P_PAR_02 VARCHAR2 DEFAULT NULL
										 ,P_PAR_03 VARCHAR2 DEFAULT NULL
										 ,P_PAR_04 VARCHAR2 DEFAULT NULL
										 ,P_PAR_05 VARCHAR2 DEFAULT NULL
										 ,P_PAR_06 VARCHAR2 DEFAULT NULL
										 ,P_PAR_07 VARCHAR2 DEFAULT NULL
										 ,P_PAR_08 VARCHAR2 DEFAULT NULL
										 ,P_PAR_09 VARCHAR2 DEFAULT NULL
										 ,P_PAR_10 VARCHAR2 DEFAULT NULL)
		RETURN LAWSUP.PKG_FILES.REC_DOC IS
		REC_DOC LAWSUP.PKG_FILES.REC_DOC;
	BEGIN
	
		RTF_DOC := NULL;
		P_ADD_DATA(P_PAR_01);
		REC_DOC.P_BLOB      := RTF_DOC.P_BLOB;
		REC_DOC.P_FILE_NAME := 'MIROVOE_' || P_PAR_01;
	
		RETURN REC_DOC;
	
	END RUN_REPORT;

END PKG_DOC_010;
/

