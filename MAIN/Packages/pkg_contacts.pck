i�?create or replace package lawmain.PKG_CONTACTS is

  -- Author  : EUGEN
  -- Created : 21.11.2016 12:17:24
  -- Purpose : пакет для работы с контактами юр. лиц 
  
  PROCEDURE P_CREATE_REPRES(P_ID_CLIENT NUMBER,
                            P_FIO       VARCHAR2,                  																																																																											
														P_NOTES     VARCHAR2 DEFAULT NULL,
														P_POSITION  VARCHAR2);     
	
	PROCEDURE P_UPDATE_REPRES(P_ID_CONTACT NUMBER,
														P_FIO       VARCHAR2,														 														 														 														 														 														 
														P_NOTES     VARCHAR2 DEFAULT NULL,
														P_POSITION  VARCHAR2);
														 
 	PROCEDURE P_UPDATE_CONTACT(P_ID_CONTACT NUMBER,
														 P_DET_TYPE   VARCHAR2,														 														 														 														 														 														 
														 P_DET_VAL    VARCHAR2,
														 P_DET_DESC   VARCHAR2);
	
	PROCEDURE P_CREATE_CONTACT(P_ID_CLIENT  NUMBER,
														 P_DET_TYPE   VARCHAR2,																																																																																				
														 P_DET_VAL    VARCHAR2,
														 P_DET_DESC   VARCHAR2);		
														 
  PROCEDURE P_CREATE_REP_CONTACT(P_ID_REPRES  NUMBER,
														 P_DET_TYPE   VARCHAR2,																																																																																				
														 P_DET_VAL    VARCHAR2,
														 P_DET_DESC   VARCHAR2);		

	PROCEDURE P_UPDATE_REP_CONTACT(P_ID_REP_CONT NUMBER,
																 P_DET_TYPE   VARCHAR2,														 														 														 														 														 														 
																 P_DET_VAL    VARCHAR2,
																 P_DET_DESC   VARCHAR2);														 											 											 									 

end PKG_CONTACTS;
/

create or replace package body lawmain.PKG_CONTACTS is
	
	PROCEDURE P_CREATE_REPRES(P_ID_CLIENT NUMBER,
														P_FIO       VARCHAR2,																																																																																				
														P_NOTES     VARCHAR2 DEFAULT NULL,
														P_POSITION  VARCHAR2)
	IS
	BEGIN
			INSERT INTO T_CLI_REPRES(ID_REPRES,FIO,POSITION,ID_CLIENT,CONT_SOURCE,CREATED_BY,Date_Created,NOTES)
			VALUES(SEQ_REPRES.nextval,upper(P_FIO),P_POSITION,P_ID_CLIENT,'L',f$_usr_id,SYSDATE,P_NOTES);
	END; 
	
	PROCEDURE P_UPDATE_REPRES(P_ID_CONTACT NUMBER,
														P_FIO       VARCHAR2,														 														 														 														 														 														 
														P_NOTES     VARCHAR2 DEFAULT NULL,
														P_POSITION  VARCHAR2)
	IS
	BEGIN
			UPDATE T_CLI_REPRES c 
			SET c.fio   = P_FIO,			    
					C.POSITION = P_POSITION,
					C.MODIFIED_BY = f$_usr_id,
					C.DATE_MODIFIED = SYSDATE,
					NOTES = P_NOTES
			WHERE c.id_REPRES = P_ID_CONTACT;
	END;     
	
	PROCEDURE P_CREATE_CONTACT(P_ID_CLIENT  NUMBER,
														 P_DET_TYPE   VARCHAR2,																																																																																				
														 P_DET_VAL    VARCHAR2,
														 P_DET_DESC   VARCHAR2)
	IS
	BEGIN
			INSERT INTO T_CLI_CONTACT_DETAILS(ID,ID_CLIENT,DET_TYPE,DET_VAL,DET_DESC,CONT_SOURCE,CREATED_BY,DATE_CREATED)
			VALUES(SEQ_CLI_CONTACT.NEXTVAL,P_ID_CLIENT,P_DET_TYPE,P_DET_VAL,P_DET_DESC,'L',f$_usr_id,SYSDATE);
	END; 
	
	PROCEDURE P_UPDATE_CONTACT(P_ID_CONTACT NUMBER,
														 P_DET_TYPE   VARCHAR2,														 														 														 														 														 														 
														 P_DET_VAL    VARCHAR2,
														 P_DET_DESC   VARCHAR2)
	IS
	BEGIN
			UPDATE T_CLI_CONTACT_DETAILS c 
			SET c.det_type = P_DET_TYPE
			   ,C.DET_VAL  = P_DET_VAL
				 ,C.DET_DESC = P_DET_DESC
				 ,C.MODIFIED_BY = f$_usr_id
				 ,C.DATE_MODIFIED = SYSDATE
			WHERE c.id = P_ID_CONTACT;
	END;                       
	 
  	
	PROCEDURE P_CREATE_REP_CONTACT(P_ID_REPRES  NUMBER,
														 P_DET_TYPE   VARCHAR2,																																																																																				
														 P_DET_VAL    VARCHAR2,
														 P_DET_DESC   VARCHAR2)
	IS
	BEGIN
			INSERT INTO T_CLI_REP_CONTACTS(ID,ID_REPRES,DET_TYPE,DET_VAL,DET_DESC,CONT_SOURCE,CREATED_BY,DATE_CREATED)
			VALUES(SEQ_CLI_CONTACT.NEXTVAL,P_ID_REPRES,P_DET_TYPE,P_DET_VAL,P_DET_DESC,'L',f$_usr_id,SYSDATE);
	END; 
	
	PROCEDURE P_UPDATE_REP_CONTACT(P_ID_REP_CONT NUMBER,
														 P_DET_TYPE   VARCHAR2,														 														 														 														 														 														 
														 P_DET_VAL    VARCHAR2,
														 P_DET_DESC   VARCHAR2)
	IS
	BEGIN
			UPDATE T_CLI_CONTACT_DETAILS c 
			SET c.det_type = P_DET_TYPE
			   ,C.DET_VAL  = P_DET_VAL
				 ,C.DET_DESC = P_DET_DESC
				 ,C.MODIFIED_BY = f$_usr_id
				 ,C.DATE_MODIFIED = SYSDATE
			WHERE c.id = P_ID_REP_CONT;
	END;      

end PKG_CONTACTS;
/

