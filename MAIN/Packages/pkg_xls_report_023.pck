i�?create or replace package lawmain.PKG_XLS_REPORT_023 is

  -- Author  : BES
  -- Created : 03.07.2019
  -- Purpose : ПОСТУПЛЕНИЕ ДС ПО ИСПОЛНИТЕЛЬНОМУ ПРОИЗВОДСТВУ
	-- Code:    FSSP_PAYEMENTS
		

  FUNCTION RUN_REPORT(P_PAR_01 VARCHAR2 DEFAULT NULL
											,P_PAR_02 VARCHAR2 DEFAULT NULL
											,P_PAR_03 VARCHAR2 DEFAULT NULL
											,P_PAR_04 VARCHAR2 DEFAULT NULL
											,P_PAR_05 VARCHAR2 DEFAULT NULL
											,P_PAR_06 VARCHAR2 DEFAULT NULL
											,P_PAR_07 VARCHAR2 DEFAULT NULL
											,P_PAR_08 VARCHAR2 DEFAULT NULL
											,P_PAR_09 VARCHAR2 DEFAULT NULL
											,P_PAR_10 VARCHAR2 DEFAULT NULL) RETURN LAWSUP.PKG_FILES.REC_DOC;	  

end PKG_XLS_REPORT_023;
/

create or replace package body lawmain.PKG_XLS_REPORT_023 is

	TYPE REC_DOC IS RECORD(	P_BLOB BLOB DEFAULT NULL );

	EXCEL_DOC REC_DOC := NULL;
------------------------------------------------------------------------

	PROCEDURE p$s( ps_value IN CLOB )  -- отправляет накопленное значение ps_value в буфер и записывает
	IS
		BUFFER    	RAW(32767);
    l_offset    NUMBER := 1;
    BUF_SIZE    NUMBER := 8000;
    BUF_VAR    VARCHAR2(32767);
  BEGIN
    LOOP
    EXIT WHEN L_OFFSET > DBMS_LOB.getlength(PS_VALUE);
    BUF_VAR := DBMS_LOB.substr(PS_VALUE,BUF_SIZE,L_OFFSET);
		BUFFER := UTL_RAW.cast_to_raw( CONVERT( BUF_VAR, 'UTF8'/*'CL8MSWIN1251'*/ ) );
		DBMS_LOB.WRITEAPPEND( EXCEL_DOC.P_BLOB, UTL_RAW.LENGTH( BUFFER ), BUFFER );
    L_OFFSET := L_OFFSET + BUF_SIZE;
    END LOOP;
  END;

----------------------------------------------------------------------

------------------------------------------------------------------------
	PROCEDURE P_ADD_DATA(P_DATE_FROM DATE,P_DATE_TO DATE)
		 IS
	  L_XML CLOB;
		L_CNT NUMBER := 0;
		L_FMT VARCHAR2(50) := '9999999999999D00';
		L_NLS VARCHAR2(50) := 'NLS_NUMERIC_CHARACTERS=''. ''';         
    
		L_DATE_START DATE := NVL(P_DATE_FROM,DATE '2000-01-01');
		L_DATE_END   DATE := NVL(P_DATE_TO,SYSDATE) + 1 - (INTERVAL '1' SECOND);

	BEGIN    
	
		L_XML :=
			'<?xml version="1.0"?>
<?mso-application progid="Excel.Sheet"?>
<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:o="urn:schemas-microsoft-com:office:office"
 xmlns:x="urn:schemas-microsoft-com:office:excel"
 xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:html="http://www.w3.org/TR/REC-html40">
 <DocumentProperties xmlns="urn:schemas-microsoft-com:office:office">
  <Author></Author>
  <LastAuthor></LastAuthor>
  <Created>2019-07-03T18:05:16Z</Created>
  <LastSaved>2019-07-03T18:05:16Z</LastSaved>
  <Version>16.00</Version>
 </DocumentProperties>
 <OfficeDocumentSettings xmlns="urn:schemas-microsoft-com:office:office">
  <AllowPNG/>
 </OfficeDocumentSettings>
 <ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel">
  <WindowHeight>12660</WindowHeight>
  <WindowWidth>19980</WindowWidth>
  <WindowTopX>480</WindowTopX>
  <WindowTopY>135</WindowTopY>
  <ProtectStructure>False</ProtectStructure>
  <ProtectWindows>False</ProtectWindows>
 </ExcelWorkbook>
 <Styles>
  <Style ss:ID="Default" ss:Name="Normal">
   <Alignment ss:Vertical="Bottom"/>
   <Borders/>
   <Font ss:FontName="Microsoft Sans Serif" x:CharSet="204" ss:Size="9"/>
   <Interior/>
   <NumberFormat/>
   <Protection/>
  </Style>
  <Style ss:ID="m228034616">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Microsoft Sans Serif" x:CharSet="204" x:Family="Swiss"
    ss:Size="9" ss:Bold="1"/>
  </Style>
  <Style ss:ID="s22">
   <Font ss:FontName="Microsoft Sans Serif" x:CharSet="204" ss:Size="9" ss:Bold="1"/>
  </Style>
  <Style ss:ID="s67">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Font ss:FontName="Microsoft Sans Serif" x:CharSet="204" ss:Size="9" ss:Bold="1"/>
  </Style>
  <Style ss:ID="s90">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font ss:FontName="Microsoft Sans Serif" x:CharSet="204" ss:Size="9" ss:Bold="1"/>
   <Interior ss:Color="#C6E0B4" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s91">
   <Alignment ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
  </Style>
  <Style ss:ID="s92">
   <Alignment ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
  </Style>
  <Style ss:ID="s97">
   <Alignment ss:Horizontal="Right" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <NumberFormat ss:Format="Standard"/>
  </Style>
  <Style ss:ID="s98">
   <Alignment ss:Horizontal="Right" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <NumberFormat ss:Format="Standard"/>
  </Style>
  <Style ss:ID="s99">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <NumberFormat ss:Format="@"/>
  </Style>
  <Style ss:ID="s100">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <NumberFormat ss:Format="@"/>
  </Style>
  <Style ss:ID="s113">
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <NumberFormat ss:Format="Standard"/>
  </Style>
  <Style ss:ID="s121">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Font ss:FontName="Microsoft Sans Serif" x:CharSet="204" x:Family="Swiss"
    ss:Size="11" ss:Bold="1"/>
  </Style>
 </Styles>
 <Worksheet ss:Name="Лист 1">
  <Table>
   <Column ss:AutoFitWidth="0" ss:Width="72.75"/>
   <Column ss:AutoFitWidth="0" ss:Width="96.75"/>
   <Column ss:AutoFitWidth="0" ss:Width="226.5"/>
   <Column ss:AutoFitWidth="0" ss:Width="73.5"/>
   <Column ss:AutoFitWidth="0" ss:Width="73.5"/>
   <Column ss:AutoFitWidth="0" ss:Width="87.75"/>
   <Column ss:AutoFitWidth="0" ss:Width="72"/>
   <Column ss:AutoFitWidth="0" ss:Width="76.5"/>
   <Column ss:AutoFitWidth="0" ss:Width="89.25"/>      
   <Column ss:AutoFitWidth="0" ss:Width="60" ss:Span="247"/>
   <Row ss:AutoFitHeight="0" ss:Height="33">
    <Cell ss:MergeAcross="8" ss:StyleID="s121"><Data ss:Type="String">Поступление денежных средство по исполнительному производству за период с '
		      ||TO_CHAR(L_DATE_START,'DD.MM.YYYY')||' по '||TO_CHAR(L_DATE_END,'DD.MM.YYYY')||'.</Data></Cell>
   </Row>
   <Row ss:Height="13.5"/>
   <Row ss:Height="26.25" ss:StyleID="s22">
    <Cell ss:StyleID="s90"><Data ss:Type="String">№ дела</Data></Cell>
    <Cell ss:StyleID="s90"><Data ss:Type="String">№ дела в суде</Data></Cell>
    <Cell ss:StyleID="s90"><Data ss:Type="String">Абонент</Data></Cell>
    <Cell ss:StyleID="s90"><Data ss:Type="String">Договор ХВС</Data></Cell>
    <Cell ss:StyleID="s90"><Data ss:Type="String">Договор ВО</Data></Cell>
    <Cell ss:StyleID="s90"><Data ss:Type="String">Оплата ОД</Data></Cell>
    <Cell ss:StyleID="s90"><Data ss:Type="String">Оплата пени</Data></Cell>
    <Cell ss:StyleID="s90"><Data ss:Type="String">Оплата ГП</Data></Cell>
    <Cell ss:StyleID="s90"><Data ss:Type="String">Всего оплачено</Data></Cell>
    <Cell ss:StyleID="s67"/>
   </Row>'; 
   p$s(L_XML);
   	 

   	FOR I IN (SELECT CC.CASE_NAME
										,(SELECT EX.NUM_EXEC
											FROM T_CLI_COURT_EXEC EX
											WHERE EX.ID = CC.LAST_EXEC) NUM_EXEC
										,CC.CLI_ALT_NAME
										,CC.CWS_CTR_CODE
										,CC.SEW_CTR_CODE
										,T.AMOUNT
										,T.PENY
										,T.TOLL
							FROM (SELECT NVL(PP.ID_CASE,OD.ID_WORK) ID_CASE
													,OD.AMOUNT
													,PP.PENY
													,PP.TOLL
										FROM (SELECT CD.ID_WORK
																,SUM(CASE 
																	 WHEN C.DOC_TYPE = 'Изменение состояния долга' THEN C.AMOUNT
																	 ELSE  ABS(C.AMOUNT)
																	END) AMOUNT
													FROM T_CLI_FINDOC_CONS C
															,T_CLI_FINDOCS     FD
															,T_CLI_DEBTS       CD
													WHERE (C.EXT_ID_FROM = FD.EXT_ID OR C.EXT_ID_TO = FD.EXT_ID)
													AND FD.ID_DOC = CD.ID_DOC
													AND C.EXT_ID_FROM != C.EXT_ID_TO
													AND C.DOC_DATE BETWEEN GREATEST(NVL(CD.DATE_FSSP,L_DATE_START),L_DATE_START) AND L_DATE_END
													AND CD.DEPT IN ('FSSP','BANKRUPT')
													GROUP BY CD.ID_WORK) OD
										FULL OUTER JOIN (SELECT *
																		FROM (SELECT P.ID_CASE
																								,P.AMOUNT
																								,P.PURPOSE
																					FROM T_CLI_PENALTY_PAYMENTS P
																					WHERE P.DATE_PAY BETWEEN L_DATE_START AND L_DATE_END)
																--					AND   P.DATE_PAY <= NVL(F_GET_CLOSE_DATE(P.ID_CASE,'SPISANO'),L_DATE_END))
																		PIVOT(SUM(AMOUNT)
																		FOR PURPOSE IN('PENY' AS PENY,
																									 'TOLL' AS TOLL))) PP
										ON (PP.ID_CASE = OD.ID_WORK)) T
									,V_CLI_CASES CC
							WHERE CC.ID_CASE = T.ID_CASE)							
		LOOP					    
        L_CNT := L_CNT +1;																															 
			  L_XML := '<Row>'||CHR(13);
				L_XML := L_XML ||'    <Cell ss:StyleID="s91"><Data ss:Type="String">'||I.CASE_NAME||'</Data></Cell>'||CHR(13);
				L_XML := L_XML ||'    <Cell ss:StyleID="s91"><Data ss:Type="String">'||I.NUM_EXEC||'</Data></Cell>'||CHR(13);
				L_XML := L_XML ||'    <Cell ss:StyleID="s91"><Data ss:Type="String">'||I.CLI_ALT_NAME||'</Data></Cell>'||CHR(13);
				L_XML := L_XML ||'    <Cell ss:StyleID="s99"><Data ss:Type="String">'||I.CWS_CTR_CODE||'</Data></Cell>'||CHR(13);
        L_XML := L_XML ||'    <Cell ss:StyleID="s99"><Data ss:Type="String">'||I.SEW_CTR_CODE||'</Data></Cell>'||CHR(13);
				L_XML := L_XML ||'    <Cell ss:StyleID="s97"><Data ss:Type="Number">'||TO_CHAR(I.AMOUNT,L_FMT,L_NLS)||'</Data></Cell>'||CHR(13);
				L_XML := L_XML ||'    <Cell ss:StyleID="s97"><Data ss:Type="Number">'||TO_CHAR(I.PENY,L_FMT,L_NLS)||'</Data></Cell>'||CHR(13);
				L_XML := L_XML ||'    <Cell ss:StyleID="s97"><Data ss:Type="Number">'||TO_CHAR(I.TOLL,L_FMT,L_NLS)||'</Data></Cell>'||CHR(13);
  			L_XML := L_XML ||'    <Cell ss:StyleID="s97"><Data ss:Type="Number">'||TO_CHAR(NVL(I.Toll,0)+ NVL(I.AMOUNT,0) + NVL(I.PENY,0) ,L_FMT,L_NLS)||'</Data></Cell>'||CHR(13);
				L_XML := L_XML ||'</Row>'||CHR(13);
				P$S(L_XML);
		END LOOP;		
		
	 L_XML := ' <Row>
    <Cell ss:MergeAcross="4" ss:StyleID="m228034616"><Data ss:Type="String">Итого</Data></Cell>
    <Cell ss:StyleID="s113" ss:Formula="=SUM(R[-'||L_CNT||']C:R[-1]C)"><Data ss:Type="Number">0</Data></Cell>
    <Cell ss:StyleID="s113" ss:Formula="=SUM(R[-'||L_CNT||']C:R[-1]C)"><Data ss:Type="Number">0</Data></Cell>
    <Cell ss:StyleID="s113" ss:Formula="=SUM(R[-'||L_CNT||']C:R[-1]C)"><Data ss:Type="Number">0</Data></Cell>
		<Cell ss:StyleID="s113" ss:Formula="=SUM(R[-'||L_CNT||']C:R[-1]C)"><Data ss:Type="Number">0</Data></Cell>
   </Row>
  </Table>
  <WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">
   <PageSetup>
    <Header x:Margin="0"/>
    <Footer x:Margin="0"/>
   </PageSetup>
   <Print>
    <ValidPrinterInfo/>
    <HorizontalResolution>300</HorizontalResolution>
    <VerticalResolution>300</VerticalResolution>
   </Print>
   <Zoom>130</Zoom>
   <Selected/>
   <FreezePanes/>
   <FrozenNoSplit/>
   <SplitHorizontal>3</SplitHorizontal>
   <TopRowBottomPane>3</TopRowBottomPane>
   <ActivePane>2</ActivePane>
   <Panes>
    <Pane>
     <Number>3</Number>
    </Pane>
    <Pane>
     <Number>2</Number>
     <ActiveRow>15</ActiveRow>
     <ActiveCol>2</ActiveCol>
    </Pane>
   </Panes>
   <ProtectObjects>False</ProtectObjects>
   <ProtectScenarios>False</ProtectScenarios>
  </WorksheetOptions>
 </Worksheet>
</Workbook>';

		p$s( L_XML );
	
	
	END P_ADD_DATA;

---------------------------------------------------------------------------------------------------------------------------------

  FUNCTION RUN_REPORT(P_PAR_01 VARCHAR2 DEFAULT NULL
											,P_PAR_02 VARCHAR2 DEFAULT NULL
											,P_PAR_03 VARCHAR2 DEFAULT NULL
											,P_PAR_04 VARCHAR2 DEFAULT NULL
											,P_PAR_05 VARCHAR2 DEFAULT NULL
											,P_PAR_06 VARCHAR2 DEFAULT NULL
											,P_PAR_07 VARCHAR2 DEFAULT NULL
											,P_PAR_08 VARCHAR2 DEFAULT NULL
											,P_PAR_09 VARCHAR2 DEFAULT NULL
											,P_PAR_10 VARCHAR2 DEFAULT NULL) RETURN LAWSUP.PKG_FILES.REC_DOC
	IS
	l_BLOB LAWSUP.PKG_FILES.REC_DOC;
  BEGIN

	  EXCEL_DOC := NULL;
		DBMS_LOB.CREATETEMPORARY( EXCEL_DOC.P_BLOB, TRUE );
    DBMS_LOB.OPEN( EXCEL_DOC.P_BLOB, DBMS_LOB.LOB_READWRITE );

		P_ADD_DATA(TO_DATE(P_PAR_02,'DD.MM.YYYY'),
		           TO_DATE(P_PAR_03,'DD.MM.YYYY'));

		DBMS_LOB.CLOSE(EXCEL_DOC.P_BLOB);

		    l_BLOB.P_BLOB := EXCEL_DOC.P_BLOB;
	      l_BLOB.P_FILE_NAME := 'Поступление ДС по ИП';
				RETURN L_BLOB;

  END RUN_REPORT;

end PKG_XLS_REPORT_023;
/

