i�?create or replace package lawmain.PKG_SMS_03 is

  -- Author  : EUGEN
  -- Created : 12.01.2017 10:44:26
  -- Purpose : СМС о задолженности от досудебного по договору
  
  	PROCEDURE P_SEND_SMS(P_ID_CLIENT   NUMBER,
		                   P_ID_CONTRACT NUMBER,
											 P_PHONE       VARCHAR2);

end PKG_SMS_03;
/

create or replace package body lawmain.PKG_SMS_03 is

	PROCEDURE P_SEND_SMS(P_ID_CLIENT   NUMBER,
		                   P_ID_CONTRACT NUMBER,
											 P_PHONE       VARCHAR2)
		 IS
		G$SMS_TEXT VARCHAR2(2000);
		L_PHONE    VARCHAR2(12);                                     
		L_SMS_ID   NUMBER;
	BEGIN                       
		 G$SMS_TEXT := PKG_PREF.F$DESC1('PRE_TRIAL_DEBT_SMS');  
		
	  IF P_ID_CONTRACT IS NULL THEN
				RAISE_APPLICATION_ERROR(-20200,'Укажите договор');				 
		END IF;	
	 
		FOR i IN (SELECT CT.CTR_NUMBER,
			               L.PHONE,
										 cli.curator_pre
			        FROM T_CONTRACTS CT,
							     T_CLIENTS CLI,
									 T_USER_LIST L
			        WHERE CT.ID_CONTRACT = P_ID_CONTRACT
							AND   CT.ID_CLIENT = CLI.ID_CLIENT
							AND   CLI.CURATOR_PRE = L.ID(+))
    LOOP                 
	    IF I.curator_pre IS NULL THEN
				RAISE_APPLICATION_ERROR(-20200,'Необходимо назначить исполнителя перед отправкой СМС');				 
			END IF;	
      G$SMS_TEXT := REPLACE(G$SMS_TEXT,'#CTRNUMBER#',I.Ctr_Number); 
			G$SMS_TEXT := REPLACE(G$SMS_TEXT,'#PHONE#',I.PHONE); -- текущая дата.
    END LOOP;            
		
		IF P_PHONE IS NULL THEN
			 RAISE NO_DATA_FOUND;
		END IF;
		--Отсылка СМС
		L_SMS_ID := LAWSUP.PKG_SMS.F_PUT_MESSAGE(P_PHONE => P_PHONE,P_MESSAGE => G$SMS_TEXT);	
		--Событие и привязка к делу
		--PKG_EVENTS.P_CREATE_CLI_EVENT(P_CASE_ID => P_ID_CASE,P_CODE => 'INFORM_SMS_LEG');
		
		PKG_SEND_SMS.P_INSERT_CLI_SMS(P_ID_CLIENT,L_SMS_ID,'PRE_TRIAL_DEBT_SMS');
	
	EXCEPTION 
		 WHEN NO_DATA_FOUND THEN
			  PKG_LOG.P$LOG(p_PROG_UNIT => 'PKG_SMS_03',p_MESS => 'Не найден телефон для отправки'
										 ,P_C1 => 'P_ID_CLIENT: '||P_ID_CLIENT,P_d1 => SYSDATE);   
				 RAISE_APPLICATION_ERROR(-20200,'Не найден телефон для отправки'); 
	END;
  
end PKG_SMS_03;
/

