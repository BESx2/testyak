i�?create or replace package lawmain.PKG_CLI_SHUTDOWN is

  -- Author  : EUGEN
  -- Created : 01.04.2019 11:15:40
  -- Purpose : Пакет для работы с графиком отключений
  
  -- Public type declarations
  PROCEDURE P_CREATE_CLI_SHUTDOWN(P_CONTRACTS   wwv_flow_t_varchar2,
		                              P_DATE_FROM   DATE DEFAULT NULL,
														      P_DATE_TO     DATE DEFAULT NULL,	             
																  P_DATE_DEBT   DATE,
																  P_ID_FOLDER   NUMBER);
																	
  PROCEDURE P_DELETE_SHUTDOWN(P_ID_SHUT NUMBER);			
	
  PROCEDURE P_SET_CURATOR(P_ID_SHUT NUMBER, P_CURATOR NUMBER);		
	
	
  PROCEDURE P_SET_OBJECTS(P_ID_WORK NUMBER,
		                      P_OBJECTS  wwv_flow_global.vc_arr2);		 
													
	FUNCTION F_GET_OBJECT_LIST(P_ID_WORK NUMBER
		                        ,P_CNT NUMBER DEFAULT NULL
														,P_SHORT VARCHAR2 DEFAULT 'N') RETURN CLOB;                       
	
  PROCEDURE P_SET_EVENT(P_ID_WORK NUMBER, P_DATE DATE, P_EVENT VARCHAR2);
		
	/*FUNCTION F_CREATE_CLI_SHUT_MANUAL(P_ID_CONTRACT NUMBER, 
		                                P_ID_FOLDER   NUMBER,
		                                P_DATE_DEBT DATE) RETURN NUMBER	 ;*/									
end PKG_CLI_SHUTDOWN;
/

create or replace package body lawmain.PKG_CLI_SHUTDOWN is

  --ПРОЦЕДУРА СОЗДАЁТ ДЕЛА НА ОТКЛЮЧЕНИЕ/ОГРАНИЧЕНИЕ ИЗ ФОРМЫ "РЕЕСТРЫ УВЕДОМЛЕНИЙ"
  PROCEDURE P_CREATE_CLI_SHUTDOWN(P_CONTRACTS   wwv_flow_t_varchar2,              
                               		P_DATE_FROM   DATE DEFAULT NULL,
														      P_DATE_TO     DATE DEFAULT NULL,				
		                              P_DATE_DEBT   DATE,
																	P_ID_FOLDER   NUMBER)
		IS                                         
		L_DEBT_CREATED NUMBER := 0; 
		L_DEBT_CURRENT NUMBER := 0; 
		L_ID_CTR_CWS   NUMBER;
		L_ID_CTR_SEW   NUMBER;   
		L_TYPE         VARCHAR2(100);
		L_ID_SHUT NUMBER;
		L_ID_CLIENT NUMBER;
	BEGIN                                                            
    --Определяем договор
  	FOR I IN 1..P_CONTRACTS.COUNT LOOP
			SELECT CT.CTR_TYPE, CT.ID_CLIENT INTO L_TYPE, L_ID_CLIENT
			FROM T_CONTRACTS CT WHERE CT.ID_CONTRACT = P_CONTRACTS(I);
			
			IF L_TYPE = 'Основной в ХВС' THEN
				L_ID_CTR_CWS := TO_NUMBER(P_CONTRACTS(I));
			ELSE
				L_ID_CTR_SEW := TO_NUMBER(P_CONTRACTS(I));
			END IF;                          				
		END LOOP;
			  
	  INSERT INTO T_CLI_DEBT_WORK(ID_WORK,ID_CLIENT,TYPE_WORK,DEPT,DATE_CREATED,CREATED_BY)
		VALUES(SEQ_CLI_DEBT_WORK.NEXTVAL,L_ID_CLIENT,'SHUTDOWN','PRE_TRIAL',SYSDATE,F$_USR_ID)
		RETURNING ID_WORK INTO L_ID_SHUT;
	
	  FOR i IN (SELECT *
							FROM   (SELECT FD.ID_DOC,FD.ID_CONTRACT,FD.ID_CLIENT
														,(SELECT NVL(SUM(DECODE(C.OPER_TYPE, 'Приход', C.AMOUNT,-C.AMOUNT)), 0)
															FROM   T_CLI_FINDOC_CONS C WHERE  C.EXT_ID_TO = FD.EXT_ID
															AND    C.DATE_CON <= P_DATE_DEBT) DEBT_ON_DATE
														,FD.DEBT
											FROM   T_CLI_FINDOCS FD 
											WHERE FD.OPER_TYPE = 'Приход'        
											AND FD.DOC_DATE BETWEEN NVL(P_DATE_FROM,DATE '2000-01-01') AND NVL(P_DATE_TO,DATE '3000-01-01')
											AND    NOT EXISTS  (SELECT 1
																					FROM   T_CLI_DEBTS CCD
																					LEFT OUTER JOIN T_CLI_DEBT_DOCS CC ON (CCD.ID_WORK = CC.ID AND CC.IS_ACTIVE = 'Y')
																					LEFT OUTER JOIN T_CLI_REST R ON (CCD.ID_WORK = R.ID_REST AND R.CODE_STATUS = 'VALID') 
																					LEFT OUTER JOIN T_CLI_CASES S ON (S.ID_CASE = CCD.ID_WORK)
																					WHERE CCD.ID_DOC = FD.ID_DOC
																					AND  (CC.ID IS NOT NULL OR R.ID_REST IS NOT NULL OR S.ID_CASE IS NOT NULL)) 
																					 --КОТОРЫХ НЕТ В ДАННОЙ ПАПКЕ
											AND NOT EXISTS (SELECT 1
																			FROM   T_CLI_DEBTS D, T_CLI_SHUTDOWN S
																			WHERE  D.ID_DOC = FD.ID_DOC AND S.ID_WORK = D.ID_WORK
																			AND    S.ID_FOLDER = P_ID_FOLDER) 
											AND FD.ID_CONTRACT IN (SELECT TO_NUMBER(COLUMN_VALUE) FROM TABLE(P_CONTRACTS)))
							WHERE  DEBT_ON_DATE > 0)
		LOOP            
			L_DEBT_CREATED := I.DEBT_ON_DATE + L_DEBT_CREATED;
			L_DEBT_CURRENT := I.DEBT + L_DEBT_CURRENT;
			
			INSERT INTO T_CLI_DEBTS(ID_DEBT,ID_WORK,ID_DOC,DATE_CREATED,CREATED_BY,ID_CONTRACT,ID_CLIENT,DEBT_CURRENT,DEPT,DEBT_CREATED)
			VALUES(SEQ_CLI_CASE_DEBTS.NEXTVAL,L_ID_SHUT,I.ID_DOC,SYSDATE,F$_USR_ID,I.ID_CONTRACT,I.ID_CLIENT,I.DEBT,'PRE_TRIAL',I.DEBT_ON_DATE);
		END LOOP;					
	
					
		INSERT INTO T_CLI_SHUTDOWN(ID_WORK,DEBT_DATE,SUM_DEBT,DATE_CREATED,CREATED_BY,ID_FOLDER,DEBT_CREATED,ID_CWS_CONTRACT,ID_SEW_CONTRACT,ID_CLIENT)
		VALUES(L_ID_SHUT,P_DATE_DEBT,L_DEBT_CURRENT, SYSDATE,F$_USR_ID,P_ID_FOLDER,L_DEBT_CREATED,L_ID_CTR_CWS,L_ID_CTR_SEW,L_ID_CLIENT);          
		
		UPDATE T_CLI_DEBT_WORK W 
		SET W.DEBT_CREATED = L_DEBT_CREATED, 
		    W.CURRENT_DEBT = L_DEBT_CURRENT 
		WHERE W.ID_WORK = L_ID_SHUT;
		
		PKG_EVENTS.P_CREATE_CLI_EVENT(P_ID_WORK => L_ID_SHUT,P_CODE => 'SHUTDOWN_NOTIF');
		
	END;                       
	
/*	FUNCTION F_CREATE_CLI_SHUT_MANUAL(P_ID_CONTRACT NUMBER, 
		                                P_ID_FOLDER   NUMBER,
		                                P_DATE_DEBT DATE) RETURN NUMBER	                              
	  IS	
    L_ID_WORK   NUMBER;
    L_SUM       NUMBER := 0;
    L_ID_CLIENT NUMBER;                
  BEGIN
    
	  SELECT CT.ID_CLIENT INTO L_ID_CLIENT FROM T_CONTRACTS CT WHERE CT.ID_CONTRACT = P_ID_CONTRACT;
	
		INSERT INTO T_CLI_DEBT_WORK(ID_WORK,ID_CLIENT,TYPE_WORK,DEPT,DATE_CREATED,CREATED_BY)
		VALUES(SEQ_CLI_DEBT_WORK.NEXTVAL,L_ID_CLIENT,'SHUTDOWN','PRE_TRIAL',SYSDATE,F$_USR_ID)      
		RETURNING ID_WORK INTO L_ID_WORK;
		
  	INSERT INTO T_CLI_SHUTDOWN(ID_WORK,ID_CONTRACT,DEBT_DATE,DATE_CREATED,CREATED_BY,ID_FOLDER,IS_MANUAL)
		VALUES(L_ID_WORK,P_ID_CONTRACT,P_DATE_DEBT, SYSDATE,F$_USR_ID,P_ID_FOLDER,'Y');
  
    FOR I IN (SELECT S.n001, D.debt FROM APEX_COLLECTIONS S, V_CLI_DEBTS D
			        WHERE S.collection_name = 'DEBTS_CLI_CASES' 
							AND   D.id_doc = S.n001)
		LOOP
    	INSERT INTO T_CLI_DEBTS(ID_DEBT,ID_WORK,ID_DOC,DATE_CREATED,CREATED_BY,ID_CONTRACT,ID_CLIENT,DEBT_CURRENT,DEPT,DEBT_CREATED)
      VALUES(SEQ_CLI_CASE_DEBTS.NEXTVAL,L_ID_WORK,I.N001,SYSDATE,F$_USR_ID,P_ID_CONTRACT
            ,L_ID_CLIENT,I.DEBT,'PRE_TRIAL',I.DEBT);
						          
      L_SUM   := L_SUM + I.DEBT;
    END LOOP;
    
		UPDATE T_CLI_DEBT_WORK W
		   SET W.DEBT_CREATED = L_SUM
		WHERE W.ID_WORK = L_ID_WORK;               
		
		UPDATE T_CLI_SHUTDOWN S SET S.SUM_DEBT = L_SUM, S.DEBT_CREATED = L_SUM
		WHERE S.ID_WORK = L_ID_WORK;
		
  
    PKG_CLIENTS_UTL.P_CHANGE_CLI_FLG(P_ID_CLIENT => L_ID_CLIENT
                                    ,P_FLG_NAME => 'FLG_DEBTOR'
                                    ,P_FLG_VAL => 'Y');
    
    RETURN L_ID_WORK;
  EXCEPTION
    WHEN OTHERS THEN
    	RAISE_APPLICATION_ERROR(-20200,SQLERRM);
	END;		*/ 										                                 
	
	PROCEDURE P_DELETE_SHUTDOWN(P_ID_SHUT NUMBER)
		IS
	BEGIN    
		 DELETE FROM T_CLI_EVENTS EV WHERE EV.ID_WORK = P_ID_SHUT;
		 DELETE FROM T_CLI_DEBTS D WHERE D.ID_WORK = P_ID_SHUT;                       
		 DELETE FROM T_CLI_SHUTDOWN_OBJ O WHERE O.ID_WORK = P_ID_SHUT;
		 DELETE FROM T_CLI_SHUTDOWN D WHERE D.ID_WORK = P_ID_SHUT;
		 DELETE FROM T_CLI_DEBT_WORK W WHERE W.ID_WORK = P_ID_SHUT;
	END;

----------------------------------------------------------------------------------
	
	PROCEDURE P_SET_CURATOR(P_ID_SHUT NUMBER, P_CURATOR NUMBER)
		IS
	BEGIN
		UPDATE T_CLI_SHUTDOWN S SET S.CURATOR = P_CURATOR WHERE S.ID_WORK = P_ID_SHUT;
	END;                                                            
	
-----------------------------------------------------------------------------------

  PROCEDURE P_SET_OBJECTS(P_ID_WORK NUMBER,
		                      P_OBJECTS  wwv_flow_global.vc_arr2)
		IS
	BEGIN
		 DELETE FROM T_CLI_SHUTDOWN_OBJ O WHERE O.ID_WORK = P_ID_WORK;
		 FORALL I IN P_OBJECTS.FIRST..P_OBJECTS.LAST 
		   INSERT INTO  T_CLI_SHUTDOWN_OBJ(ID,ID_WORK,ID_OBJECT)    
			 VALUES(SEQ_SHUT_OBJ.NEXTVAL,P_ID_WORK,TO_NUMBER(P_OBJECTS(I)));
	END;                     

------------------------------------------------------------------------------------	    
	
	FUNCTION F_GET_OBJECT_LIST(P_ID_WORK NUMBER, 
		                         P_CNT NUMBER DEFAULT NULL,
														 P_SHORT VARCHAR2 DEFAULT 'N') RETURN CLOB
		IS
		L_RET CLOB;                    
		L_CNT NUMBER;
	BEGIN
		SELECT COUNT(1) INTO L_CNT FROM T_CLI_SHUTDOWN_OBJ O WHERE O.ID_WORK = P_ID_WORK;
		IF L_CNT = 0 THEN
			SELECT REPLACE(
			          RTRIM(
								  XMLAGG(
									  XMLELEMENT(E,DECODE(P_SHORT,'Y',SUBSTR(O.ADDRESS_NAME,INSTR(O.ADDRESS_NAME,'г Якутск')),O.ADDRESS_NAME),', ').EXTRACT('//text()') 
										ORDER BY O.ADDRESS_NAME
										).GetClobVal(),', '
										),'&quot;','"'
										)
			INTO L_RET			
			FROM T_CONTRACT_OBJ O,
					 T_CLI_SHUTDOWN S,
					 V_FOLDERS F
			WHERE S.ID_WORK = P_ID_WORK
			AND   F.ID_FOLDER = S.ID_FOLDER
			AND  (O.ID_CONTRACT = S.ID_CWS_CONTRACT AND F.TYPE_CODE = 'PT_SHUTDOWN_ALL'
					 OR O.ID_CONTRACT = S.ID_SEW_CONTRACT AND F.TYPE_CODE = 'PT_SHUTDOWN_SEW')
			AND (ROWNUM <= P_CNT OR P_CNT IS NULL);
		ELSE
      SELECT REPLACE(RTRIM(XMLAGG(XMLELEMENT(E,DECODE(P_SHORT,'Y',SUBSTR(O.ADDRESS_NAME,INSTR(O.ADDRESS_NAME,'г Якутск')),O.ADDRESS_NAME),', ').EXTRACT('//text()') ORDER BY O.ADDRESS_NAME).GetClobVal(),', '),'&quot;','"')
			INTO L_RET			
			FROM T_CONTRACT_OBJ O,
					 T_CLI_SHUTDOWN S,
					 V_FOLDERS F,
					 T_CLI_SHUTDOWN_OBJ SO
			WHERE S.ID_WORK = P_ID_WORK
			AND   F.ID_FOLDER = S.ID_FOLDER
			AND   SO.ID_WORK = S.ID_WORK
			AND   SO.ID_OBJECT = O.ID_OBJECT
			AND  (O.ID_CONTRACT = S.ID_CWS_CONTRACT AND F.TYPE_CODE = 'PT_SHUTDOWN_ALL'
					 OR O.ID_CONTRACT = S.ID_SEW_CONTRACT AND F.TYPE_CODE = 'PT_SHUTDOWN_SEW')
			AND (ROWNUM <= P_CNT OR P_CNT IS NULL);			
		END IF;    
		RETURN l_ret;
	END; 
	
--------------------------------------------------------------------------------------

  PROCEDURE P_SET_EVENT(P_ID_WORK NUMBER, P_DATE DATE, P_EVENT VARCHAR2)
		IS               
		L_EVENT NUMBER;
	BEGIN                   
    SELECT S.id_event INTO L_EVENT FROM V_CLI_EVENTS S WHERE S.code_event = P_EVENT AND S.ID_WORK = P_ID_WORK;                                    								
		UPDATE T_CLI_EVENTS E SET E.DATE_EVENT = P_DATE WHERE E.ID_EVENT = L_EVENT;
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			PKG_EVENTS.P_CREATE_CLI_EVENT(P_ID_WORK,P_EVENT,P_DATE);	
	END;
		
  
end PKG_CLI_SHUTDOWN;
/

