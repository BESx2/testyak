i�?create or replace package lawmain.PKG_SMS_01 is

  -- Author  : EUGEN
  -- Created : 12.01.2017 10:44:26
  -- Purpose : пакет для отправки СМС юридическому лицу сотрудником УРВиВ
  
  PROCEDURE P_SEND_SMS(P_ID_CASE NUMBER,
		                   P_PHONE VARCHAR2 DEFAULT NULL);

end PKG_SMS_01;
/

create or replace package body lawmain.PKG_SMS_01 is

	PROCEDURE P_SEND_SMS(P_ID_CASE NUMBER,
		                   P_PHONE VARCHAR2 DEFAULT NULL)
		 IS
		G$SMS_TEXT VARCHAR2(2000);
		L_PHONE    VARCHAR2(12);                                     
		L_SMS_ID   NUMBER;
	BEGIN                       
		 G$SMS_TEXT := PKG_PREF.F$DESC1('FSSP_SMS_EXEC_START');  
		 
		FOR i IN (SELECT '№ '||L.LEVY_SERIES||' '||L.LEVY_NUM LEVY_NUM,
				                 S.CLI_ALT_NAME, 
												 (SELECT C.DATE_END FROM T_CLI_COURT_EXEC C WHERE C.ID = S.LAST_EXEC) DATE_EXEC,
												 S.CODE_STATUS												 
					        FROM V_CLI_CASES S,
									     T_CLI_LEVY L
									WHERE S.ID_CASE = P_ID_CASE
									AND   L.ID_CASE = S.ID_CASE)
            LOOP                      
							IF I.CODE_STATUS NOT IN ('LEVY_CLAIM_DEPFIN','LEVY_CLAIM_UFK','LEVY_CLAIM_BANK','LEVY_CLAIM_ROSP') THEN
								RAISE_APPLICATION_ERROR(-20200,'Нельзя отправить сообщению по делу, по которому не предъявлен исполнительный документ');
							END IF;
							IF I.CODE_STATUS = 'LEVY_CLAIM_DEPFIN' THEN
								G$SMS_TEXT := REPLACE(G$SMS_TEXT,'#PLACE#','Департамент финансов');
              ELSIF i.code_status = 'LEVY_CLAIM_UFK' THEN
                G$SMS_TEXT := REPLACE(G$SMS_TEXT,'#PLACE#','Управление федерального казначейства');
              ELSIF i.code_status = 'LEVY_CLAIM_BANK' THEN
                G$SMS_TEXT := REPLACE(G$SMS_TEXT,'#PLACE#','банк');
              ELSIF i.code_status IN ('LEVY_CLAIM_ROSP','EXEC_GOING') THEN                  
                G$SMS_TEXT := REPLACE(G$SMS_TEXT,'#PLACE#','отдел судебных приставов');
							END IF;                                                
							
							G$SMS_TEXT := REPLACE(G$SMS_TEXT,'#LEVY#',I.LEVY_NUM); -- договор.
							G$SMS_TEXT := REPLACE(G$SMS_TEXT,'#CLINAME#',I.CLI_ALT_NAME); -- Абонент
							G$SMS_TEXT := REPLACE(G$SMS_TEXT,'#COURTDATE#',TO_CHAR(I.DATE_EXEC,'DD.MM.YYYY')); -- текущая дата.
            END LOOP;            
		
		IF P_PHONE IS NULL THEN
			 RAISE NO_DATA_FOUND;
		END IF;
		--Отсылка СМС
		L_SMS_ID := LAWSUP.PKG_SMS.F_PUT_MESSAGE(P_PHONE => P_PHONE,P_MESSAGE => G$SMS_TEXT);	
		--Событие и привязка к делу
		--PKG_EVENTS.P_CREATE_CLI_EVENT(P_CASE_ID => P_ID_CASE,P_CODE => 'INFORM_SMS_LEG');
		
		PKG_SEND_SMS.P_INSERT_CLI_CASE_SMS(P_ID_CASE,L_SMS_ID,'FSSP_SMS_EXEC_START');
	
	EXCEPTION 
		 WHEN NO_DATA_FOUND THEN
			  PKG_LOG.P$LOG(p_PROG_UNIT => 'PKG_SMS_01',p_MESS => 'Не найден телефон для отправки'
										 ,P_C1 => 'ID_CASE: '||P_ID_CASE,P_d1 => SYSDATE);   
				 RAISE_APPLICATION_ERROR(-20200,'Не найден телефон для отправки'); 
	END;
  
end PKG_SMS_01;
/

