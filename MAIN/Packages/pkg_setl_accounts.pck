i�?create or replace package lawmain.PKG_SETL_ACCOUNTS is

  -- Author  : EUGEN
  -- Created : 24.11.2016 11:58:44
  -- Purpose : пакет для работы с расчетными счетами
  
	PROCEDURE P_CREATE_ACC(P_ID_BANK VARCHAR2,
												 P_ACC_NUMBER VARCHAR2,
												 P_ID_CLIENT  VARCHAR2);
												 
	  PROCEDURE P_UPDATE_ACC(P_ID_ACC VARCHAR2,
												 P_ACC_NUMBER VARCHAR2,
												 P_ID_BANK  VARCHAR2);

end PKG_SETL_ACCOUNTS;
/

create or replace package body lawmain.PKG_SETL_ACCOUNTS is
  
	PROCEDURE P_CREATE_ACC(P_ID_BANK VARCHAR2,
												 P_ACC_NUMBER VARCHAR2,
												 P_ID_CLIENT  VARCHAR2)
	IS
	BEGIN
		  INSERT INTO T_SETL_ACCOUNTS(ID_ACC,ID_BANK,ACC_NUMBER,ID_CLIENT,ACC_SOURCE,CREATED_BY,DATE_CREATED)
			VALUES (SEQ_SETL_ACC.NEXTVAL,P_ID_BANK,P_ACC_NUMBER,P_ID_CLIENT,'L',f$_usr_id,SYSDATE);
	END;   
	
  PROCEDURE P_UPDATE_ACC(P_ID_ACC VARCHAR2,
												 P_ACC_NUMBER VARCHAR2,
												 P_ID_BANK  VARCHAR2)
	IS
	BEGIN
		 UPDATE T_SETL_ACCOUNTS a SET a.id_bank = P_ID_BANK,
		                              a.acc_number = P_ACC_NUMBER,
											            a.modified_by = f$_usr_id,
																	a.date_modified = SYSDATE
     WHERE a.id_acc = P_ID_ACC;																	
	END;

end PKG_SETL_ACCOUNTS;
/

