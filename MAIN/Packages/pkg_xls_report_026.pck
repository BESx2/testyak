i�?create or replace package lawmain.PKG_XLS_REPORT_026 is

  -- Author  : BES
  -- Created : 06.03.2020
  -- Purpose : Расчет задолженности
	-- Code:    DEBT_CALC
	

	
	
  
  FUNCTION RUN_REPORT(P_PAR_01 VARCHAR2 DEFAULT NULL
											,P_PAR_02 VARCHAR2 DEFAULT NULL
											,P_PAR_03 VARCHAR2 DEFAULT NULL
											,P_PAR_04 VARCHAR2 DEFAULT NULL
											,P_PAR_05 VARCHAR2 DEFAULT NULL
											,P_PAR_06 VARCHAR2 DEFAULT NULL
											,P_PAR_07 VARCHAR2 DEFAULT NULL
											,P_PAR_08 VARCHAR2 DEFAULT NULL
											,P_PAR_09 VARCHAR2 DEFAULT NULL
											,P_PAR_10 VARCHAR2 DEFAULT NULL) RETURN LAWSUP.PKG_FILES.REC_DOC;	  

end PKG_XLS_REPORT_026;
/

create or replace package body lawmain.PKG_XLS_REPORT_026 is

	TYPE REC_DOC IS RECORD(	P_BLOB BLOB DEFAULT NULL );

	EXCEL_DOC REC_DOC := NULL;
------------------------------------------------------------------------

------------------------------------------------------------------------

	PROCEDURE p$s( ps_value IN CLOB )  -- отправляет накопленное значение ps_value в буфер и записывает
	IS
		BUFFER    	RAW(32767);
    l_offset    NUMBER := 1;
    BUF_SIZE    NUMBER := 8000;
    BUF_VAR    VARCHAR2(32767);
  BEGIN
    LOOP
    EXIT WHEN L_OFFSET > DBMS_LOB.getlength(PS_VALUE);
    BUF_VAR := DBMS_LOB.substr(PS_VALUE,BUF_SIZE,L_OFFSET);
		BUFFER := UTL_RAW.cast_to_raw( CONVERT( BUF_VAR, 'UTF8'/*'CL8MSWIN1251'*/ ) );
		DBMS_LOB.WRITEAPPEND( EXCEL_DOC.P_BLOB, UTL_RAW.LENGTH( BUFFER ), BUFFER );
    L_OFFSET := L_OFFSET + BUF_SIZE;
    END LOOP;
  END;
------------------------------------------------------------------------
	PROCEDURE P_ADD_DATA(P_ID_CASE NUMBER) IS
	  L_XML CLOB;
	  L_FMT VARCHAR2(100) := 'FM999999999999999990D00';
		L_NLS VARCHAR2(100) := 'NLS_NUMERIC_CHARACTERS=''. ''';  
		L_FORMULA VARCHAR2(100);
		L_CLI VARCHAR2(500);
		L_INN VARCHAR2(100);
		L_CUR VARCHAR2(500);
		L_CNT NUMBER := 0;              
		L_ID_CWS NUMBER;
		L_ID_SEW NUMBER;
	BEGIN              
		FOR I IN (SELECT S.CLI_ALT_NAME,S.INN, 
										 INITCAP(PKG_USERS.F_GET_FIO_BY_ID(S.CURATOR_COURT,'Y')) CUR,
										 S.CWS_ID_CONTRACT, S.SEW_ID_CONTRACT
			        FROM V_CLI_CASES S WHERE S.ID_CASE = P_ID_CASE)
		LOOP
			L_CLI := I.CLI_ALT_NAME;
			L_INN := I.INN;
			L_CUR := I.CUR;                 
			L_ID_SEW := I.SEW_ID_CONTRACT;
			L_ID_CWS := I.CWS_ID_CONTRACT;
		END LOOP;
		
		L_XML :=
			'<?xml version="1.0"?>
<?mso-application progid="Excel.Sheet"?>
<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:o="urn:schemas-microsoft-com:office:office"
 xmlns:x="urn:schemas-microsoft-com:office:excel"
 xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:html="http://www.w3.org/TR/REC-html40">
 <DocumentProperties xmlns="urn:schemas-microsoft-com:office:office">
  <Author>user0608</Author>
  <LastAuthor>Evgeniy Borodulin</LastAuthor>
  <Created>2020-03-06T07:29:15Z</Created>
  <LastSaved>2020-03-06T08:06:47Z</LastSaved>
  <Version>16.00</Version>
 </DocumentProperties>
 <OfficeDocumentSettings xmlns="urn:schemas-microsoft-com:office:office">
  <AllowPNG/>
 </OfficeDocumentSettings>
 <ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel">
  <WindowHeight>12600</WindowHeight>
  <WindowWidth>27795</WindowWidth>
  <WindowTopX>480</WindowTopX>
  <WindowTopY>105</WindowTopY>
  <ProtectStructure>False</ProtectStructure>
  <ProtectWindows>False</ProtectWindows>
 </ExcelWorkbook>
 <Styles>
  <Style ss:ID="Default" ss:Name="Normal">
   <Alignment ss:Vertical="Bottom"/>
   <Borders/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"/>
   <Interior/>
   <NumberFormat/>
   <Protection/>
  </Style>
  <Style ss:ID="s16" ss:Name="Обычный 2">
   <Alignment ss:Vertical="Bottom"/>
   <Borders/>
   <Font ss:FontName="Calibri" x:CharSet="204" x:Family="Swiss" ss:Size="11"
    ss:Color="#000000"/>
   <Interior/>
   <NumberFormat/>
   <Protection/>
  </Style>
  <Style ss:ID="m300339364" ss:Parent="s16">
   <Alignment ss:Horizontal="Right" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Calibri" x:CharSet="204" x:Family="Swiss" ss:Size="11"
    ss:Color="#000000"/>
   <Interior/>
   <NumberFormat ss:Format="Standard"/>
  </Style>
  <Style ss:ID="m300339384" ss:Parent="s16">
   <Alignment ss:Horizontal="Right" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Calibri" x:CharSet="204" x:Family="Swiss" ss:Size="11"
    ss:Color="#000000"/>
   <Interior/>
   <NumberFormat ss:Format="Standard"/>
  </Style>
  <Style ss:ID="s18">
   <Alignment ss:Vertical="Center" ss:WrapText="1"/>
   <Font ss:FontName="Calibri" x:CharSet="204" x:Family="Swiss" ss:Size="11"
    ss:Color="#000000"/>
  </Style>
  <Style ss:ID="s19" ss:Parent="s16">
   <Font ss:FontName="Calibri" x:CharSet="204" x:Family="Swiss" ss:Size="11"
    ss:Color="#000000"/>
  </Style>
  <Style ss:ID="s23" ss:Parent="s16">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Calibri" x:CharSet="204" x:Family="Swiss" ss:Size="11"
    ss:Color="#000000"/>
  </Style>
  <Style ss:ID="s35" ss:Parent="s16">
   <Font ss:FontName="Calibri" x:CharSet="204" x:Family="Swiss" ss:Size="11"
    ss:Color="#FF0000"/>
   <Interior/>
  </Style>
  <Style ss:ID="s42" ss:Parent="s16">
   <Alignment ss:Horizontal="Center" ss:Vertical="Bottom"/>
   <Borders/>
   <Font ss:FontName="Calibri" x:CharSet="204" x:Family="Swiss" ss:Size="11"
    ss:Color="#000000"/>
  </Style>
  <Style ss:ID="s50" ss:Parent="s16">
   <Alignment ss:Horizontal="Center" ss:Vertical="Bottom"/>
   <Font ss:FontName="Calibri" x:CharSet="204" x:Family="Swiss" ss:Size="11"
    ss:Color="#000000"/>
  </Style>
  <Style ss:ID="s168" ss:Parent="s16">   
	 <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Calibri" x:CharSet="204" x:Family="Swiss" ss:Size="11"
    ss:Color="#000000"/>
   <Interior/>
   <NumberFormat ss:Format="@"/>
  </Style>
  <Style ss:ID="s169" ss:Parent="s16">
   <Alignment ss:Horizontal="Center" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Calibri" x:CharSet="204" x:Family="Swiss" ss:Size="11"
    ss:Color="#000000"/>
   <Interior/>
   <NumberFormat ss:Format="#,##0"/>
  </Style>
  <Style ss:ID="s170" ss:Parent="s16">
   <Alignment ss:Horizontal="Center" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Calibri" x:CharSet="204" x:Family="Swiss" ss:Size="11"
    ss:Color="#000000"/>
   <Interior/>
   <NumberFormat ss:Format="Standard"/>
  </Style>
  <Style ss:ID="s171" ss:Parent="s16"> 
	<Alignment ss:Horizontal="Right" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Calibri" x:CharSet="204" x:Family="Swiss" ss:Size="11"
    ss:Color="#000000"/>
   <Interior/>
   <NumberFormat ss:Format="Standard"/>
  </Style>
  <Style ss:ID="s180" ss:Parent="s16">
   <Font ss:FontName="Calibri" x:CharSet="204" x:Family="Swiss" ss:Size="11"
    ss:Color="#000000"/>
   <Interior/>
  </Style>
  <Style ss:ID="s181">
   <Alignment ss:Horizontal="Left" ss:Vertical="Center" ss:WrapText="1"/>
   <Font ss:FontName="Calibri" x:CharSet="204" x:Family="Swiss" ss:Size="11"
    ss:Color="#000000"/>
   <Interior/>
  </Style>
  <Style ss:ID="s182">
   <Alignment ss:Vertical="Center" ss:WrapText="1"/>
   <Font ss:FontName="Calibri" x:CharSet="204" x:Family="Swiss" ss:Size="11"
    ss:Color="#000000"/>
   <Interior/>
  </Style>
  <Style ss:ID="s183">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Calibri" x:CharSet="204" x:Family="Swiss" ss:Size="11"
    ss:Color="#000000"/>
   <Interior/>
  </Style>
  <Style ss:ID="s184">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Font ss:FontName="Calibri" x:CharSet="204" x:Family="Swiss" ss:Size="11"
    ss:Color="#000000"/>
   <Interior/>
  </Style>
 </Styles>';
 P$S(L_XML);
 
 IF L_ID_CWS IS NOT NULL THEN 
	 L_XML := '
 <Worksheet ss:Name="Холодная вода (расчет долга) ">
  <Table>
   <Column ss:StyleID="s19" ss:AutoFitWidth="0" ss:Width="135"/>
   <Column ss:StyleID="s19" ss:AutoFitWidth="0" ss:Width="72"/>
   <Column ss:StyleID="s19" ss:AutoFitWidth="0" ss:Width="61.5"/>
   <Column ss:StyleID="s19" ss:AutoFitWidth="0" ss:Width="87.75"/>
   <Column ss:StyleID="s19" ss:AutoFitWidth="0" ss:Width="66.75"/>
   <Column ss:StyleID="s19" ss:AutoFitWidth="0" ss:Width="87.75"/>
   <Row ss:AutoFitHeight="0">
    <Cell ss:MergeAcross="5" ss:StyleID="s181"><Data ss:Type="String">Абонент: '||L_CLI||', ИНН: '||L_INN||'</Data></Cell>
    <Cell ss:StyleID="s18"/>
   </Row>
   <Row>
    <Cell ss:MergeAcross="5" ss:StyleID="s50"/>
   </Row>
   <Row>
    <Cell ss:MergeAcross="5" ss:StyleID="s50"/>
   </Row>
   <Row>
    <Cell ss:MergeAcross="5" ss:StyleID="s50"><Data ss:Type="String">РАСЧЕТ</Data></Cell>
   </Row>
   <Row>
    <Cell ss:MergeAcross="5" ss:StyleID="s50"><Data ss:Type="String">взыскиваемой денежной суммы</Data></Cell>
   </Row>
   <Row>
    <Cell ss:MergeAcross="5" ss:StyleID="s50"><Data ss:Type="String">холодное водоснабжение (основной долг)</Data></Cell>
   </Row>
   <Row>
    <Cell ss:MergeAcross="5" ss:StyleID="s42"/>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="60">
    <Cell ss:MergeDown="1" ss:StyleID="s23"><Data ss:Type="String">период (месяц, когда выписан акт, счет, счет-фактура)</Data></Cell>
    <Cell ss:StyleID="s23"><Data ss:Type="String">объем</Data></Cell>
    <Cell ss:StyleID="s23"><Data ss:Type="String">тариф </Data></Cell>
    <Cell ss:StyleID="s23"><Data ss:Type="String">сумма к оплате по счету</Data></Cell>
    <Cell ss:StyleID="s23"><Data ss:Type="String">внесенные платежи</Data><Comment
      ss:Author="Автор"><ss:Data xmlns="http://www.w3.org/TR/REC-html40"><B><Font
         html:Face="Tahoma" x:CharSet="204" x:Family="Swiss" html:Size="9"
         html:Color="#000000">Автор:</Font></B><Font html:Face="Tahoma"
        x:CharSet="1" html:Size="9" html:Color="#000000">&#10;т.к. платежи не делятся по тарифам указываем общую сумму&#10;оплаты</Font></ss:Data></Comment></Cell>
    <Cell ss:StyleID="s23"><Data ss:Type="String">задолженность </Data><Comment
      ss:Author="Автор"><ss:Data xmlns="http://www.w3.org/TR/REC-html40"><B><Font
         html:Face="Tahoma" x:CharSet="204" x:Family="Swiss" html:Size="9"
         html:Color="#000000">Автор:</Font></B><Font html:Face="Tahoma"
        x:CharSet="204" x:Family="Swiss" html:Size="9" html:Color="#000000">&#10;т.к. платежи не делятся по тарифам указываем общую сумму&#10;задолженности</Font></ss:Data></Comment></Cell>
   </Row>
   <Row ss:Height="17.25">
    <Cell ss:Index="2" ss:StyleID="s23"><ss:Data ss:Type="String"
      xmlns="http://www.w3.org/TR/REC-html40"><Font html:Color="#000000">м</Font><Sup><Font
        html:Color="#000000">3</Font></Sup></ss:Data></Cell>
    <Cell ss:StyleID="s23"><ss:Data ss:Type="String"
      xmlns="http://www.w3.org/TR/REC-html40"><Font html:Color="#000000">руб./м</Font><Sup><Font
        html:Color="#000000">3</Font></Sup></ss:Data></Cell>
    <Cell ss:StyleID="s23"><Data ss:Type="String">руб. (в т.ч. НДС)</Data></Cell>
    <Cell ss:StyleID="s23"><Data ss:Type="String">руб.</Data></Cell>
    <Cell ss:StyleID="s23"><Data ss:Type="String">руб. (в т.ч. НДС)</Data></Cell>
   </Row>
   <Row>
    <Cell ss:StyleID="s23"><Data ss:Type="Number">1</Data></Cell>
    <Cell ss:StyleID="s23"><Data ss:Type="Number">2</Data></Cell>
    <Cell ss:StyleID="s23"><Data ss:Type="Number">3</Data></Cell>
    <Cell ss:StyleID="s23"><Data ss:Type="String">4 (2 х 3)</Data></Cell>
    <Cell ss:StyleID="s23"><Data ss:Type="Number">5</Data></Cell>
    <Cell ss:StyleID="s23"><Data ss:Type="String">6 (4-5)</Data></Cell>
   </Row>';
   p$s(L_XML);
   
	 
 	 FOR I IN (SELECT TO_CHAR(FD.DOC_DATE,'fmmonth YYYY') doc_date,
                    dd.volume, dd.tarif, fd.debt, DD.NDS, fd.amount,
                    fd.amount - fd.debt payed,
										DD.EXT_ID,        
                    COUNT(DD.EXT_ID) OVER (PARTITION BY FD.EXT_ID ORDER BY FD.DOC_DATE) CNT
										,LAG(DD.EXT_ID) OVER (ORDER BY FD.DOC_DATE) PREV
             FROM T_CLI_DEBTS D
                 ,T_CLI_FINDOCS FD
                 ,T_CLI_FINDOCS_DETAILS DD
             WHERE D.ID_WORK = P_ID_CASE
             AND   D.ID_DOC = FD.ID_DOC   
						 AND   D.ID_CONTRACT = L_ID_CWS
             AND   DD.EXT_ID = FD.EXT_ID
             ORDER BY FD.DOC_DATE)   
   LOOP    
		 L_CNT := L_CNT +1;
		 IF I.PREV IS NULL OR I.PREV != I.EXT_ID THEN     
			  L_FORMULA := 'RC[-2]-RC[-1]';				
				FOR J IN 2..I.CNT LOOP
					L_FORMULA := L_FORMULA || '+R['||TO_NUMBER(J-1)||']C[-2]';
				END LOOP;
				
				L_XML := '<Row>'||CHR(13);
				L_XML := L_XML ||'   <Cell ss:MergeDown="'||TO_CHAR(I.CNT-1)||'" ss:StyleID="s168"><Data ss:Type="String">'||I.DOC_DATE||'</Data></Cell>'||CHR(13);
				L_XML := L_XML ||'   <Cell ss:StyleID="s169"><Data ss:Type="Number">'||TO_CHAR(I.VOLUME,L_FMT,L_NLS)||'</Data></Cell>'||CHR(13);
				L_XML := L_XML ||'   <Cell ss:StyleID="s170"><Data ss:Type="Number">'||TO_CHAR(I.TARIF,L_FMT,L_NLS)||'</Data></Cell>'||CHR(13);
				L_XML := L_XML ||'   <Cell ss:MergeDown="'||TO_CHAR(I.CNT-1)||'" ss:StyleID="s171"><Data ss:Type="Number">'||TO_CHAR(I.amount,L_FMT,L_NLS)||'</Data></Cell>'||CHR(13);
				L_XML := L_XML ||'   <Cell ss:MergeDown="'||TO_CHAR(I.CNT-1)||'" ss:StyleID="m300339364"><Data ss:Type="Number">'||TO_CHAR(I.PAYED,L_FMT,L_NLS)||'</Data></Cell>'||CHR(13);
				L_XML := L_XML ||'   <Cell ss:MergeDown="'||TO_CHAR(I.CNT-1)||'" ss:StyleID="m300339384" ss:Formula="='||L_FORMULA||'"><Data ss:Type="Number">0</Data></Cell>'||CHR(13);
				L_XML := L_XML ||'  </Row>'||CHR(13);
			ELSE
				L_XML := '  <Row>'||CHR(13);
				L_XML := L_XML ||'   <Cell ss:Index="2" ss:StyleID="s169"><Data ss:Type="Number">'||TO_CHAR(I.VOLUME,L_FMT,L_NLS)||'</Data></Cell>'||CHR(13);
				L_XML := L_XML ||'   <Cell ss:StyleID="s170"><Data ss:Type="Number">'||TO_CHAR(I.TARIF,L_FMT,L_NLS)||'</Data></Cell>'||CHR(13);
				L_XML := L_XML ||'  </Row>'||CHR(13);
			END IF;			
		 p$s(L_XML);
	 END LOOP;						
      
	 L_XML := '    
	  <Row>
    <Cell ss:StyleID="s168"><Data ss:Type="String">итого</Data></Cell>
    <Cell ss:StyleID="s169" ss:Formula="=SUM(R[-'||TO_CHAR(L_CNT)||']C:R[-1]C)"><Data
      ss:Type="Number">0</Data></Cell>
    <Cell ss:StyleID="s170"><Data ss:Type="String">-</Data></Cell>
    <Cell ss:StyleID="s171" ss:Formula="=SUM(R[-'||TO_CHAR(L_CNT)||']C:R[-1]C)"><Data
      ss:Type="Number">0</Data></Cell>
    <Cell ss:StyleID="s171" ss:Formula="=SUM(R[-'||TO_CHAR(L_CNT)||']C:R[-1]C)"><Data
      ss:Type="Number">0</Data></Cell>
    <Cell ss:StyleID="s171" ss:Formula="=SUM(R[-'||TO_CHAR(L_CNT)||']C:R[-1]C)"><Data
      ss:Type="Number">0</Data></Cell>
   </Row>
	 <Row>
    <Cell ss:StyleID="s180"/>
    <Cell ss:StyleID="s180"/>
    <Cell ss:StyleID="s180"/>
    <Cell ss:StyleID="s180"/>
    <Cell ss:StyleID="s180"/>
    <Cell ss:StyleID="s180"/>
   </Row>
   <Row>
    <Cell ss:StyleID="s180"/>
    <Cell ss:StyleID="s180"/>
    <Cell ss:StyleID="s180"/>
    <Cell ss:StyleID="s180"/>
    <Cell ss:StyleID="s180"/>
    <Cell ss:StyleID="s180"/>
   </Row>
   <Row>
    <Cell ss:MergeAcross="2" ss:StyleID="s181"><Data ss:Type="String">Представитель АО &quot;Водоканал&quot;</Data></Cell>
    <Cell ss:StyleID="s182"/>
    <Cell ss:MergeAcross="1" ss:StyleID="s183"><Data ss:Type="String">'||L_CUR||'</Data></Cell>
   </Row>
   <Row>
    <Cell ss:StyleID="s180"/>
    <Cell ss:StyleID="s180"/>
    <Cell ss:StyleID="s180"/>
    <Cell ss:StyleID="s180"/>
    <Cell ss:StyleID="s180"/>
    <Cell ss:StyleID="s180"/>
   </Row>
   <Row>
    <Cell ss:StyleID="s180"/>
    <Cell ss:StyleID="s180"/>
    <Cell ss:StyleID="s180"/>
    <Cell ss:StyleID="s180"/>
    <Cell ss:StyleID="s180"/>
    <Cell ss:StyleID="s180"/>
   </Row>
   <Row>
    <Cell ss:StyleID="s180"/>
    <Cell ss:StyleID="s180"/>
    <Cell ss:StyleID="s180"/>
    <Cell ss:MergeAcross="1" ss:StyleID="s184"><Data ss:Type="String">&quot;___&quot; __________ 20__ г.</Data></Cell>
    <Cell ss:StyleID="s180"/>
   </Row>
   <Row>
    <Cell ss:StyleID="s180"/>
    <Cell ss:StyleID="s180"/>
    <Cell ss:StyleID="s180"/>
    <Cell ss:StyleID="s180"/>
    <Cell ss:StyleID="s180"/>
    <Cell ss:StyleID="s180"/>
   </Row>
  </Table> 
 </Worksheet>';
 P$S(L_XML);
 END IF;
 
 IF L_ID_SEW IS NOT NULL THEN
	 L_XML := '
 <Worksheet ss:Name="Водоотведение (расчет долга) ">
  <Table>
   <Column ss:StyleID="s19" ss:AutoFitWidth="0" ss:Width="135"/>
   <Column ss:StyleID="s19" ss:AutoFitWidth="0" ss:Width="72"/>
   <Column ss:StyleID="s19" ss:AutoFitWidth="0" ss:Width="61.5"/>
   <Column ss:StyleID="s19" ss:AutoFitWidth="0" ss:Width="87.75"/>
   <Column ss:StyleID="s19" ss:AutoFitWidth="0" ss:Width="66.75"/>
   <Column ss:StyleID="s19" ss:AutoFitWidth="0" ss:Width="87.75"/>
   <Row ss:AutoFitHeight="0">
    <Cell ss:MergeAcross="5" ss:StyleID="s181"><Data ss:Type="String">Абонент: '||L_CLI||', ИНН: '||L_INN||'</Data></Cell>
    <Cell ss:StyleID="s18"/>
   </Row>
   <Row>
    <Cell ss:MergeAcross="5" ss:StyleID="s50"/>
   </Row>
   <Row>
    <Cell ss:MergeAcross="5" ss:StyleID="s50"/>
   </Row>
   <Row>
    <Cell ss:MergeAcross="5" ss:StyleID="s50"><Data ss:Type="String">РАСЧЕТ</Data></Cell>
   </Row>
   <Row>
    <Cell ss:MergeAcross="5" ss:StyleID="s50"><Data ss:Type="String">взыскиваемой денежной суммы</Data></Cell>
   </Row>
   <Row>
    <Cell ss:MergeAcross="5" ss:StyleID="s50"><Data ss:Type="String">водоотведение (основной долг)</Data></Cell>
   </Row>
   <Row>
    <Cell ss:MergeAcross="5" ss:StyleID="s42"/>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="60">
    <Cell ss:MergeDown="1" ss:StyleID="s23"><Data ss:Type="String">период (месяц, когда выписан акт, счет, счет-фактура)</Data></Cell>
    <Cell ss:StyleID="s23"><Data ss:Type="String">объем</Data></Cell>
    <Cell ss:StyleID="s23"><Data ss:Type="String">тариф </Data></Cell>
    <Cell ss:StyleID="s23"><Data ss:Type="String">сумма к оплате по счету</Data></Cell>
    <Cell ss:StyleID="s23"><Data ss:Type="String">внесенные платежи</Data><Comment
      ss:Author="Автор"><ss:Data xmlns="http://www.w3.org/TR/REC-html40"><B><Font
         html:Face="Tahoma" x:CharSet="204" x:Family="Swiss" html:Size="9"
         html:Color="#000000">Автор:</Font></B><Font html:Face="Tahoma"
        x:CharSet="1" html:Size="9" html:Color="#000000">&#10;т.к. платежи не делятся по тарифам указываем общую сумму&#10;оплаты</Font></ss:Data></Comment></Cell>
    <Cell ss:StyleID="s23"><Data ss:Type="String">задолженность </Data><Comment
      ss:Author="Автор"><ss:Data xmlns="http://www.w3.org/TR/REC-html40"><B><Font
         html:Face="Tahoma" x:CharSet="204" x:Family="Swiss" html:Size="9"
         html:Color="#000000">Автор:</Font></B><Font html:Face="Tahoma"
        x:CharSet="204" x:Family="Swiss" html:Size="9" html:Color="#000000">&#10;т.к. платежи не делятся по тарифам указываем общую сумму&#10;задолженности</Font></ss:Data></Comment></Cell>
   </Row>
   <Row ss:Height="17.25">
    <Cell ss:Index="2" ss:StyleID="s23"><ss:Data ss:Type="String"
      xmlns="http://www.w3.org/TR/REC-html40"><Font html:Color="#000000">м</Font><Sup><Font
        html:Color="#000000">3</Font></Sup></ss:Data></Cell>
    <Cell ss:StyleID="s23"><ss:Data ss:Type="String"
      xmlns="http://www.w3.org/TR/REC-html40"><Font html:Color="#000000">руб./м</Font><Sup><Font
        html:Color="#000000">3</Font></Sup></ss:Data></Cell>
    <Cell ss:StyleID="s23"><Data ss:Type="String">руб. (в т.ч. НДС)</Data></Cell>
    <Cell ss:StyleID="s23"><Data ss:Type="String">руб.</Data></Cell>
    <Cell ss:StyleID="s23"><Data ss:Type="String">руб. (в т.ч. НДС)</Data></Cell>
   </Row>
   <Row>
    <Cell ss:StyleID="s23"><Data ss:Type="Number">1</Data></Cell>
    <Cell ss:StyleID="s23"><Data ss:Type="Number">2</Data></Cell>
    <Cell ss:StyleID="s23"><Data ss:Type="Number">3</Data></Cell>
    <Cell ss:StyleID="s23"><Data ss:Type="String">4 (2 х 3)</Data></Cell>
    <Cell ss:StyleID="s23"><Data ss:Type="Number">5</Data></Cell>
    <Cell ss:StyleID="s23"><Data ss:Type="String">6 (4-5)</Data></Cell>
   </Row>';
   p$s(L_XML);
   
	 L_CNT := 0;
 	 FOR I IN (SELECT TO_CHAR(FD.DOC_DATE,'fmmonth YYYY') doc_date,
                    dd.volume, dd.tarif, fd.debt, fd.amount,
                    fd.amount - fd.debt payed,
										DD.EXT_ID, DD.NDS,
                    COUNT(DD.EXT_ID) OVER (PARTITION BY FD.EXT_ID ORDER BY FD.DOC_DATE) CNT
										,LAG(DD.EXT_ID) OVER (ORDER BY FD.DOC_DATE) PREV
             FROM T_CLI_DEBTS D
                 ,T_CLI_FINDOCS FD
                 ,T_CLI_FINDOCS_DETAILS DD
             WHERE D.ID_WORK = P_ID_CASE
             AND   D.ID_DOC = FD.ID_DOC  
						 AND   D.ID_CONTRACT = L_ID_SEW
             AND   DD.EXT_ID = FD.EXT_ID
             ORDER BY FD.DOC_DATE)   
   LOOP    
		 L_CNT := L_CNT +1;
		 IF I.PREV IS NULL OR I.PREV != I.EXT_ID THEN     
			  L_FORMULA := 'RC[-2]-RC[-1]';				
				FOR J IN 2..I.CNT LOOP
					L_FORMULA := L_FORMULA || '+R['||TO_NUMBER(J-1)||']C[-2]';
				END LOOP;
				
				L_XML := '<Row>'||CHR(13);
				L_XML := L_XML ||'   <Cell ss:MergeDown="'||TO_CHAR(I.CNT-1)||'" ss:StyleID="s168"><Data ss:Type="String">'||I.DOC_DATE||'</Data></Cell>'||CHR(13);
				L_XML := L_XML ||'   <Cell ss:StyleID="s169"><Data ss:Type="Number">'||TO_CHAR(I.VOLUME,L_FMT,L_NLS)||'</Data></Cell>'||CHR(13);
				L_XML := L_XML ||'   <Cell ss:StyleID="s170"><Data ss:Type="Number">'||TO_CHAR(I.TARIF,L_FMT,L_NLS)||'</Data></Cell>'||CHR(13);
				L_XML := L_XML ||'   <Cell ss:MergeDown="'||TO_CHAR(I.CNT-1)||'" ss:StyleID="s171"><Data ss:Type="Number">'||TO_CHAR(I.amount,L_FMT,L_NLS)||'</Data></Cell>'||CHR(13);
				L_XML := L_XML ||'   <Cell ss:MergeDown="'||TO_CHAR(I.CNT-1)||'" ss:StyleID="m300339364"><Data ss:Type="Number">'||TO_CHAR(I.PAYED,L_FMT,L_NLS)||'</Data></Cell>'||CHR(13);
				L_XML := L_XML ||'   <Cell ss:MergeDown="'||TO_CHAR(I.CNT-1)||'" ss:StyleID="m300339384" ss:Formula="='||L_FORMULA||'"><Data ss:Type="Number">0</Data></Cell>'||CHR(13);
				L_XML := L_XML ||'  </Row>'||CHR(13);
			ELSE
				L_XML := '  <Row>'||CHR(13);
				L_XML := L_XML ||'   <Cell ss:Index="2" ss:StyleID="s169"><Data ss:Type="Number">'||TO_CHAR(I.VOLUME,L_FMT,L_NLS)||'</Data></Cell>'||CHR(13);
				L_XML := L_XML ||'   <Cell ss:StyleID="s170"><Data ss:Type="Number">'||TO_CHAR(I.TARIF,L_FMT,L_NLS)||'</Data></Cell>'||CHR(13);
				L_XML := L_XML ||'  </Row>'||CHR(13);
			END IF;			
		 p$s(L_XML);
	 END LOOP;						
      
	 L_XML := '    
	  <Row>
    <Cell ss:StyleID="s168"><Data ss:Type="String">итого</Data></Cell>
    <Cell ss:StyleID="s169" ss:Formula="=SUM(R[-'||TO_CHAR(L_CNT)||']C:R[-1]C)"><Data
      ss:Type="Number">0</Data></Cell>
    <Cell ss:StyleID="s170"><Data ss:Type="String">-</Data></Cell>
    <Cell ss:StyleID="s171" ss:Formula="=SUM(R[-'||TO_CHAR(L_CNT)||']C:R[-1]C)"><Data
      ss:Type="Number">0</Data></Cell>
    <Cell ss:StyleID="s171" ss:Formula="=SUM(R[-'||TO_CHAR(L_CNT)||']C:R[-1]C)"><Data
      ss:Type="Number">0</Data></Cell>
    <Cell ss:StyleID="s171" ss:Formula="=SUM(R[-'||TO_CHAR(L_CNT)||']C:R[-1]C)"><Data
      ss:Type="Number">0</Data></Cell>
   </Row>
	 <Row>
    <Cell ss:StyleID="s180"/>
    <Cell ss:StyleID="s180"/>
    <Cell ss:StyleID="s180"/>
    <Cell ss:StyleID="s180"/>
    <Cell ss:StyleID="s180"/>
    <Cell ss:StyleID="s180"/>
   </Row>
   <Row>
    <Cell ss:StyleID="s180"/>
    <Cell ss:StyleID="s180"/>
    <Cell ss:StyleID="s180"/>
    <Cell ss:StyleID="s180"/>
    <Cell ss:StyleID="s180"/>
    <Cell ss:StyleID="s180"/>
   </Row>
   <Row>
    <Cell ss:MergeAcross="2" ss:StyleID="s181"><Data ss:Type="String">Представитель АО &quot;Водоканал&quot;</Data></Cell>
    <Cell ss:StyleID="s182"/>
    <Cell ss:MergeAcross="1" ss:StyleID="s183"><Data ss:Type="String">'||L_CUR||'</Data></Cell>
   </Row>
   <Row>
    <Cell ss:StyleID="s180"/>
    <Cell ss:StyleID="s180"/>
    <Cell ss:StyleID="s180"/>
    <Cell ss:StyleID="s180"/>
    <Cell ss:StyleID="s180"/>
    <Cell ss:StyleID="s180"/>
   </Row>
   <Row>
    <Cell ss:StyleID="s180"/>
    <Cell ss:StyleID="s180"/>
    <Cell ss:StyleID="s180"/>
    <Cell ss:StyleID="s180"/>
    <Cell ss:StyleID="s180"/>
    <Cell ss:StyleID="s180"/>
   </Row>
   <Row>
    <Cell ss:StyleID="s180"/>
    <Cell ss:StyleID="s180"/>
    <Cell ss:StyleID="s180"/>
    <Cell ss:MergeAcross="1" ss:StyleID="s184"><Data ss:Type="String">&quot;___&quot; __________ 20__ г.</Data></Cell>
    <Cell ss:StyleID="s180"/>
   </Row>
   <Row>
    <Cell ss:StyleID="s180"/>
    <Cell ss:StyleID="s180"/>
    <Cell ss:StyleID="s180"/>
    <Cell ss:StyleID="s180"/>
    <Cell ss:StyleID="s180"/>
    <Cell ss:StyleID="s180"/>
   </Row>
  </Table> 
 </Worksheet>';
 p$s( L_XML );
 END IF;       
 L_XML := '</Workbook>';
  p$s( L_XML );
		
	
	
	END P_ADD_DATA;

---------------------------------------------------------------------------------------------------------------------------------

  FUNCTION RUN_REPORT(P_PAR_01 VARCHAR2 DEFAULT NULL
											,P_PAR_02 VARCHAR2 DEFAULT NULL
											,P_PAR_03 VARCHAR2 DEFAULT NULL
											,P_PAR_04 VARCHAR2 DEFAULT NULL
											,P_PAR_05 VARCHAR2 DEFAULT NULL
											,P_PAR_06 VARCHAR2 DEFAULT NULL
											,P_PAR_07 VARCHAR2 DEFAULT NULL
											,P_PAR_08 VARCHAR2 DEFAULT NULL
											,P_PAR_09 VARCHAR2 DEFAULT NULL
											,P_PAR_10 VARCHAR2 DEFAULT NULL) RETURN LAWSUP.PKG_FILES.REC_DOC
	IS
	l_BLOB LAWSUP.PKG_FILES.REC_DOC;
  BEGIN

	  EXCEL_DOC := NULL;
		DBMS_LOB.CREATETEMPORARY( EXCEL_DOC.P_BLOB, TRUE );
    DBMS_LOB.OPEN( EXCEL_DOC.P_BLOB, DBMS_LOB.LOB_READWRITE );

		P_ADD_DATA(TO_NUMBER(P_PAR_01));

		DBMS_LOB.CLOSE(EXCEL_DOC.P_BLOB);

		    l_BLOB.P_BLOB := EXCEL_DOC.P_BLOB;
	      l_BLOB.P_FILE_NAME := 'Raschet_zadolgennosti';
				RETURN L_BLOB;

  END RUN_REPORT;

end PKG_XLS_REPORT_026;
/

