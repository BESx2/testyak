i�?create or replace package lawmain.PKG_XLS_REPORT_008 is

  -- Author  : BES
  -- Created : 17.02.2017
  -- Purpose : Расчет задолженности по абоненту
	-- Code:     DEBT_CALC_DET
	
  
  FUNCTION RUN_REPORT(P_PAR_01 VARCHAR2 DEFAULT NULL
											,P_PAR_02 VARCHAR2 DEFAULT NULL
											,P_PAR_03 VARCHAR2 DEFAULT NULL
											,P_PAR_04 VARCHAR2 DEFAULT NULL
											,P_PAR_05 VARCHAR2 DEFAULT NULL
											,P_PAR_06 VARCHAR2 DEFAULT NULL
											,P_PAR_07 VARCHAR2 DEFAULT NULL
											,P_PAR_08 VARCHAR2 DEFAULT NULL
											,P_PAR_09 VARCHAR2 DEFAULT NULL
											,P_PAR_10 VARCHAR2 DEFAULT NULL) RETURN LAWSUP.PKG_FILES.REC_DOC;	  

end PKG_XLS_REPORT_008;
/

create or replace package body lawmain.PKG_XLS_REPORT_008 is

	TYPE REC_DOC IS RECORD(	P_BLOB BLOB DEFAULT NULL );

	EXCEL_DOC REC_DOC := NULL;
------------------------------------------------------------------------

	PROCEDURE p$s( ps_value IN CLOB )  -- отправляет накопленное значение ps_value в буфер и записывает
	IS
		BUFFER    	RAW(32767);
    l_offset    NUMBER := 1;
    BUF_SIZE    NUMBER := 8000;
    BUF_VAR    VARCHAR2(32767);
  BEGIN
    LOOP
    EXIT WHEN L_OFFSET > DBMS_LOB.getlength(PS_VALUE);
    BUF_VAR := DBMS_LOB.substr(PS_VALUE,BUF_SIZE,L_OFFSET);
		BUFFER := UTL_RAW.cast_to_raw( CONVERT( BUF_VAR, 'UTF8'/*'CL8MSWIN1251'*/ ) );
		DBMS_LOB.WRITEAPPEND( EXCEL_DOC.P_BLOB, UTL_RAW.LENGTH( BUFFER ), BUFFER );
    L_OFFSET := L_OFFSET + BUF_SIZE;
    END LOOP;
  END;
------------------------------------------------------------------------
	PROCEDURE P_ADD_DATA(P_DATE_ON   DATE, 
											 P_DATE_FROM DATE,
											 P_DATE_TO   DATE,
											 P_ID_CONTRACT NUMBER,
											 P_CIPHERS   VARCHAR2) IS
	  L_XML CLOB;
		L_CNT NUMBER := 0;
		L_FMT VARCHAR2(50) := '9999999999999D00';
		L_NLS VARCHAR2(50) := 'NLS_NUMERIC_CHARACTERS=''. ''';		
		L_DATE_START DATE;
    L_DATE_END   DATE;                
		L_CLIENT     VARCHAR2(500);  
		L_CTR_NUMBER VARCHAR2(100);
		L_FIO        VARCHAR2(500);
		L_PHONE      VARCHAR2(100);
		L_EMAIL      VARCHAR2(100);
		L_SUM        NUMBER := 0; 
		L_NEW        BOOLEAN DEFAULT TRUE;
	BEGIN    
	  L_DATE_START := NVL(P_DATE_FROM,DATE '2000-01-01');
		L_DATE_END   := NVL(P_DATE_TO,SYSDATE);   
		
		SELECT INITCAP(L.LAST_NAME||' '||L.FIRST_NAME||' '||L.SECOND_NAME) FIO,
		       L.PHONE, L.EMAIL
		INTO   L_FIO, L_PHONE, L_EMAIL			 
		FROM T_USER_LIST L WHERE L.ID = F$_USR_ID;
		
		SELECT CLI.CLI_NAME, CT.CTR_NUMBER
		INTO   L_CLIENT, L_CTR_NUMBER
		FROM T_CONTRACTS CT,
		     T_CLIENTS CLI
		WHERE CT.ID_CONTRACT = P_ID_CONTRACT
		AND   CLI.ID_CLIENT = CT.ID_CLIENT;
		L_XML :=
			'<?xml version="1.0"?>
<?mso-application progid="Excel.Sheet"?>
<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:o="urn:schemas-microsoft-com:office:office"
 xmlns:x="urn:schemas-microsoft-com:office:excel"
 xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:html="http://www.w3.org/TR/REC-html40">
 <DocumentProperties xmlns="urn:schemas-microsoft-com:office:office">
  <Author></Author>
  <LastAuthor></LastAuthor>
  <Created>2019-05-06T09:52:17Z</Created>
  <Company></Company>
  <Version>16.00</Version>
 </DocumentProperties>
 <OfficeDocumentSettings xmlns="urn:schemas-microsoft-com:office:office">
  <AllowPNG/>
 </OfficeDocumentSettings>
 <ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel">
  <WindowHeight>12300</WindowHeight>
  <WindowWidth>28800</WindowWidth>
  <WindowTopX>0</WindowTopX>
  <WindowTopY>0</WindowTopY>
  <RefModeR1C1/>
  <ProtectStructure>False</ProtectStructure>
  <ProtectWindows>False</ProtectWindows>
 </ExcelWorkbook>
 <Styles>
  <Style ss:ID="Default" ss:Name="Normal">
   <Alignment ss:Vertical="Bottom"/>
   <Borders/>
   <Font ss:FontName="Calibri" x:CharSet="204" x:Family="Swiss" ss:Size="11"
    ss:Color="#000000"/>
   <Interior/>
   <NumberFormat/>
   <Protection/>
  </Style>
  <Style ss:ID="s74">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
  </Style>
  <Style ss:ID="s76">
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
  </Style>
  <Style ss:ID="s79">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
  </Style>
  <Style ss:ID="s85">
   <Alignment ss:Horizontal="Center" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
  </Style>
  <Style ss:ID="s86">
   <Alignment ss:Horizontal="Center" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <NumberFormat ss:Format="Short Date"/>
  </Style>
  <Style ss:ID="s89">
   <Alignment ss:Horizontal="Right" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <NumberFormat ss:Format="Standard"/>
  </Style>
  <Style ss:ID="s91">
   <Alignment ss:Horizontal="Center" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <NumberFormat ss:Format="@"/>
  </Style>  
	 <Style ss:ID="s101">
   <Alignment ss:Horizontal="Center" ss:Vertical="Bottom"/>
  </Style>
  <Style ss:ID="s104">
   <Font ss:FontName="Calibri" x:CharSet="204" x:Family="Swiss" ss:Size="11"
    ss:Color="#000000" ss:Underline="Single"/>
   <NumberFormat ss:Format="Standard"/>
  </Style>
  <Style ss:ID="s105">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
  </Style>
  <Style ss:ID="s106">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Font ss:FontName="Calibri" x:CharSet="204" x:Family="Swiss" ss:Size="11"
    ss:Color="#000000" ss:Underline="Single"/>
  </Style>
  <Style ss:ID="s107">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <NumberFormat ss:Format="Short Date"/>
  </Style>
  <Style ss:ID="s108">
   <Alignment ss:Horizontal="Right" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <NumberFormat ss:Format="Standard"/>
  </Style>
  <Style ss:ID="s109">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
  </Style>  
  <Style ss:ID="s110">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Font ss:FontName="Calibri" x:CharSet="204" x:Family="Swiss" ss:Size="11"
    ss:Color="#000000" ss:Underline="Single" ss:Bold="1"/>
	 <NumberFormat ss:Format="Standard"/>
  </Style>             
	<Style ss:ID="s111">
   <Font ss:FontName="Calibri" x:CharSet="204" x:Family="Swiss" ss:Size="11"
    ss:Color="#000000" ss:Underline="Single" ss:Bold="1"/>
   <NumberFormat ss:Format="Standard"/>
  </Style>
  <Style ss:ID="s112">
   <Alignment ss:Horizontal="Center" ss:Vertical="Bottom"/>
   <Font ss:FontName="Calibri" x:CharSet="204" x:Family="Swiss" ss:Size="11"
    ss:Color="#000000" ss:Bold="1"/>
  </Style>
  <Style ss:ID="s113">
   <Alignment ss:Horizontal="Center" ss:Vertical="Bottom"/>
   <Font ss:FontName="Calibri" x:CharSet="204" x:Family="Swiss" ss:Size="11"
    ss:Color="#000000" ss:Bold="1"/>
  </Style>
  <Style ss:ID="s114">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders/>
  </Style>
  <Style ss:ID="s115">
   <Alignment ss:Horizontal="Right" ss:Vertical="Bottom"/>
   <Borders/>
   <NumberFormat ss:Format="Standard"/>
  </Style>
  <Style ss:ID="s116">
   <Borders/>
  </Style>
 </Styles>
 <Worksheet ss:Name="Лист1">
  <Table>
   <Column ss:AutoFitWidth="0" ss:Width="15"/>
   <Column ss:AutoFitWidth="0" ss:Width="12"/>
   <Column ss:AutoFitWidth="0" ss:Width="22.5"/>
   <Column ss:AutoFitWidth="0" ss:Width="81.75"/>
   <Column ss:Width="82.5"/>
   <Column ss:AutoFitWidth="0" ss:Width="80.25"/>
   <Column ss:AutoFitWidth="0" ss:Width="128.25"/>
   <Column ss:AutoFitWidth="0" ss:Width="79.5"/>
   <Column ss:AutoFitWidth="0" ss:Width="102"/>
   <Column ss:AutoFitWidth="0" ss:Width="125.25"/>
   <Column ss:AutoFitWidth="0" ss:Width="108.75"/>
   <Row>
    <Cell ss:Index="3" ss:MergeAcross="8" ss:StyleID="s112"><Data ss:Type="String">Расчет задолженности на '||TO_CHAR(NVL(P_DATE_ON,SYSDATE),'DD.MM.YYYY')
		 ||' за период с '||TO_CHAR(L_DATE_START,'DD.MM.YYYY')||' по '||TO_CHAR(L_DATE_END,'DD.MM.YYYY')||'</Data></Cell>
    <Cell ss:StyleID="s113"/>
   </Row>
   <Row>
    <Cell ss:Index="3" ss:MergeAcross="8" ss:StyleID="s112"><Data ss:Type="String">между '||L_CLIENT||' и '||PKG_PREF.F$C2('REQUISITES')||' по договору № '||L_CTR_NUMBER||'</Data></Cell>
    <Cell ss:StyleID="s113"/>
   </Row>
   <Row ss:Index="4">
    <Cell ss:Index="3" ss:MergeAcross="1" ss:MergeDown="1" ss:StyleID="s74"><Data
      ss:Type="String">№ доккумента</Data></Cell>
    <Cell ss:MergeDown="1" ss:StyleID="s74"><Data ss:Type="String">Дата документа</Data></Cell>
    <Cell ss:MergeDown="1" ss:StyleID="s74"><Data ss:Type="String">Шифр</Data></Cell>
    <Cell ss:MergeDown="1" ss:StyleID="s74"><Data ss:Type="String">Сумма выставленного</Data></Cell>
    <Cell ss:MergeAcross="2" ss:StyleID="s79"><Data ss:Type="String">Оплата</Data></Cell>
    <Cell ss:MergeDown="1" ss:StyleID="s79"><Data ss:Type="String">Сумма недоплаты</Data></Cell>
    <Cell ss:StyleID="s114"><Data ss:Type="String"> </Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="39.75">
    <Cell ss:Index="8" ss:StyleID="s109"><Data ss:Type="String">Дата оплаты</Data></Cell>
    <Cell ss:StyleID="s109"><Data ss:Type="String">Сумма оплаты</Data></Cell>
    <Cell ss:StyleID="s109"><Data ss:Type="String">Общая сумма оплаты</Data></Cell>
    <Cell ss:Index="12" ss:StyleID="s114"/>
   </Row>';
   p$s(L_XML);
   	 

   	FOR I IN (SELECT * FROM (
			        SELECT FD.DOC_NUMBER, FD.DOC_DATE, FD.SERV_TYPE, FD.AMOUNT, FD.ID_DOC, FD.DOC_PERIOD,
										 (SELECT SUM(FDC.AMOUNT)
										 FROM  T_CLI_FINDOC_CONS FDC
										 WHERE FDC.ID_DOC_TO = FD.ID_DOC 
										 AND   FDC.SERV_TYPE = FD.SERV_TYPE
										 AND   FDC.DATE_CON <= NVL(P_DATE_ON,SYSDATE)) DEBT
							FROM T_CLI_FINDOCS FD
							WHERE FD.ID_CONTRACT = P_ID_CONTRACT          
							AND   (FD.AMOUNT > 0 OR FD.DOC_TYPE = 'Документ расчетов с контрагентом (ручной учет)')
 		 			 	  AND   NVL(INSTR(P_CIPHERS,FD.SERV_TYPE),1) != 0 
							AND   FD.DOC_DATE BETWEEN L_DATE_START AND L_DATE_END
							AND   FD.DOC_TYPE != 'Ввод начальных остатков по взаиморасчетам')T							
							ORDER BY  T.DOC_DATE)						
		LOOP					         		                                                 
			 L_XML := '<Row>'||CHR(13);
			 L_XML := L_XML||'<Cell ss:Index="3" ss:MergeAcross="1" ss:StyleID="s91"><Data ss:Type="String">'||I.DOC_NUMBER||'</Data></Cell>'||CHR(13);
			 L_XML := L_XML||'   <Cell ss:StyleID="s86"><Data ss:Type="DateTime">'||TO_CHAR(I.DOC_DATE,'YYYY-MM-DD')||'T00:00:00.000</Data></Cell>'||CHR(13);
			 L_XML := L_XML||'   <Cell ss:StyleID="s91"><Data ss:Type="String">'||i.serv_type||'</Data></Cell>'||CHR(13);
			 L_XML := L_XML||'   <Cell ss:StyleID="s89"><Data ss:Type="Number">'||TO_CHAR(I.AMOUNT,L_FMT,L_NLS)||'</Data></Cell>'||CHR(13);
			 L_XML := L_XML||'   <Cell ss:StyleID="s85"/>'||CHR(13);
			 L_XML := L_XML||'   <Cell ss:StyleID="s85"/>'||CHR(13);
			 L_XML := L_XML||'   <Cell ss:StyleID="s85"/>'||CHR(13);
			 L_XML := L_XML||'   <Cell ss:StyleID="s89"><Data ss:Type="Number">'||TO_CHAR(I.DEBT,L_FMT,L_NLS)||'</Data></Cell>'||CHR(13);
			 L_XML := L_XML||'   <Cell ss:StyleID="s115"/>'||CHR(13);
			 L_XML := L_XML||' </Row>'||CHR(13);
			 P$S(L_XML);
			 
			 FOR J IN (SELECT * FROM (
				        SELECT C.DOC_COM , C.DATE_CON,          
								   CASE                              
									 WHEN I.AMOUNT = 0 THEN -C.AMOUNT
				          WHEN -C.AMOUNT >  (SELECT S.AMOUNT FROM T_CLI_FINDOC_CONS S WHERE S.ID_DOC_FROM = I.ID_DOC AND S.ID_DOC_TO = C.ID_DOC_FROM) THEN  (SELECT S.AMOUNT FROM T_CLI_FINDOC_CONS S WHERE S.ID_DOC_FROM = I.ID_DOC AND S.ID_DOC_TO = C.ID_DOC_FROM)
									 ELSE -C.AMOUNT
								 END AMOUNT, 
                 -(SELECT SUM(F.AMOUNT) FROM T_CLI_FINDOCS F WHERE F.EXT_ID = C.EXT_ID_FROM) TOT_AM,
				          (SELECT F.Pay_Purpose FROM T_CLI_FINDOCS F WHERE F.ID_DOC = C.ID_DOC_FROM) purpose
								 FROM (SELECT c1.doc_com, c1.Date_Con,
								              CASE 
																WHEN C1.DOC_TYPE = 'Корректировка долга' AND C1.AMOUNT > 0 THEN -C1.AMOUNT
															  ELSE C1.AMOUNT
															END AMOUNT
															,C1.EXT_ID_FROM,C1.DOC_TYPE, C1.ID_DOC_FROM
								       FROM T_CLI_FINDOC_CONS C1 
								       WHERE C1.ID_DOC_TO = I.ID_DOC    											  
											 AND   C1.DOC_CALC_TYPE = 'Документ расчетов с контрагентом (ручной учет)'
											 UNION ALL
								       SELECT c2.doc_com, c2.Date_Con,
								              CASE 
																WHEN C2.DOC_TYPE = 'Корректировка долга' AND C2.AMOUNT > 0 THEN -C2.AMOUNT
															  ELSE C2.AMOUNT
															END AMOUNT
														,C2.EXT_ID_FROM 
														,C2.DOC_TYPE, C2.ID_DOC_FROM
                       FROM T_CLI_FINDOC_CONS C2
                       WHERE C2.ID_DOC_TO IN (SELECT C.ID_DOC_TO
											                        FROM T_CLI_FINDOC_CONS C 
											                        WHERE C.ID_DOC_FROM = I.ID_DOC)								 
                        AND C2.AMOUNT <0 ) C
								 WHERE C.DATE_CON <= NVL(P_DATE_ON,SYSDATE)								 
								 AND   C.DOC_TYPE NOT IN ('Ввод начальных остатков по взаиморасчетам','Корректировка записей регистров'))t
								 WHERE t.tot_am <> 0
								 ORDER BY DATE_CON)
								 
			 LOOP
					L_XML := '<Row  ss:AutoFitHeight="1">'||CHR(13);
					L_XML := L_XML ||'    <Cell ss:Index="4" ss:MergeAcross="2" ss:StyleID="s79"><Data ss:Type="String">'||J.DOC_COM||'</Data></Cell>    '||CHR(13);
   				L_XML := L_XML ||'    <Cell ss:StyleID="s107"><Data ss:Type="String">'||j.purpose||'</Data></Cell>'||CHR(13);
					L_XML := L_XML ||'    <Cell ss:StyleID="s107"><Data ss:Type="DateTime">'||TO_CHAR(J.DATE_CON,'YYYY-MM-DD')||'T00:00:00.000</Data></Cell>'||CHR(13);
					L_XML := L_XML ||'    <Cell ss:StyleID="s108"><Data ss:Type="Number">'||TO_CHAR(J.AMOUNT,L_FMT,L_NLS)||'</Data></Cell>'||CHR(13);
					L_XML := L_XML ||'    <Cell ss:StyleID="s108"><Data ss:Type="Number">'||TO_CHAR(J.TOT_AM,L_FMT,L_NLS)||'</Data></Cell>'||CHR(13);
					L_XML := L_XML ||'    <Cell ss:StyleID="s76"/>'||CHR(13);
					L_XML := L_XML ||'    <Cell ss:StyleID="s116"/>'||CHR(13);
					L_XML := L_XML ||' </Row>'||CHR(13);    
					P$S(L_XML);
			 END LOOP;				 
     END LOOP;          
		 
		FOR Z IN (SELECT FD.COMMENTARY, FD.DOC_DATE, FD.AMOUNT, FD.DEBT,
			              FD.PAY_PURPOSE purpose,
               	   -(SELECT SUM(F.AMOUNT) 
								     FROM T_CLI_FINDOCS F 
									   WHERE F.EXT_ID = FD.EXT_ID) TOT_AM
			        FROM T_CLI_FINDOCS FD 
							WHERE FD.ID_CONTRACT = P_ID_CONTRACT  
 		 			 	  AND   NVL(INSTR(P_CIPHERS,FD.SERV_TYPE),1) != 0 
							AND FD.DOC_DATE BETWEEN L_DATE_START AND L_DATE_END
							AND FD.DEBT < 0
							ORDER BY FD.DOC_DATE)
	  LOOP
			IF L_NEW THEN 
				L_XML := '<Row>'||CHR(13);
				L_XML := L_XML ||'     <Cell ss:Index="4" ss:MergeAcross="2"><Data ss:Type="String">Данные по авансам</Data></Cell>    '||CHR(13);
				L_XML := L_XML ||' </Row>'||CHR(13); 
			  P$S(L_XML);   
				L_NEW := FALSE;
			END IF;		
			
				L_XML := '<Row>'||CHR(13);
				L_XML := L_XML ||'    <Cell ss:Index="4" ss:MergeAcross="2" ss:StyleID="s79"><Data ss:Type="String">'||Z.COMMENTARY||'</Data></Cell>    '||CHR(13);
 				L_XML := L_XML ||'    <Cell ss:StyleID="s107"><Data ss:Type="String">'||Z.purpose||'</Data></Cell>'||CHR(13);
				L_XML := L_XML ||'    <Cell ss:StyleID="s107"><Data ss:Type="DateTime">'||TO_CHAR(Z.DOC_DATE,'YYYY-MM-DD')||'T00:00:00.000</Data></Cell>'||CHR(13);
				L_XML := L_XML ||'    <Cell ss:StyleID="s108"><Data ss:Type="Number">'||TO_CHAR(-Z.DEBT,L_FMT,L_NLS)||'</Data></Cell>'||CHR(13);
				L_XML := L_XML ||'    <Cell ss:StyleID="s108"><Data ss:Type="Number">'||TO_CHAR(Z.TOT_AM,L_FMT,L_NLS)||'</Data></Cell>'||CHR(13);
				L_XML := L_XML ||'    <Cell ss:StyleID="s89"><Data ss:Type="Number">'||TO_CHAR(Z.DEBT,L_FMT,L_NLS)||'</Data></Cell>'||CHR(13);
				L_XML := L_XML ||'    <Cell ss:StyleID="s116"/>'||CHR(13);
				L_XML := L_XML ||' </Row>'||CHR(13);    
				P$S(L_XML);
		END LOOP;						
		 
		  		                 
		FOR Z IN (SELECT * FROM 
			       (SELECT C.CIPHER_GROUP, SUM(FDC.AMOUNT) DEBT
						 FROM  T_CLI_FINDOC_CONS FDC, 
									 T_CLI_FINDOCS FD,
									 T_CIPHERS C
						 WHERE FDC.ID_DOC_TO = FD.ID_DOC  
						 AND   FD.SERV_TYPE = FDC.SERV_TYPE
						 AND   FDC.DATE_CON <= NVL(P_DATE_ON,SYSDATE)
						 AND   FD.ID_CONTRACT = P_ID_CONTRACT   
 		 			 	 AND   NVL(INSTR(P_CIPHERS,FD.SERV_TYPE),1) != 0 
						 --AND   FD.AMOUNT > 0
						 AND   FD.SERV_TYPE = C.CIPHER
						 AND   FD.DOC_DATE BETWEEN L_DATE_START AND L_DATE_END 
						 GROUP BY C.CIPHER_GROUP) T
						 WHERE T.DEBT <> 0)
		LOOP                     
			 L_SUM := Z.DEBT + L_SUM;
			 L_XML := '<Row ss:AutoFitHeight="0">
									<Cell ss:Index="4" ss:MergeAcross="3" ss:StyleID="s106"><Data ss:Type="String">'||Z.CIPHER_GROUP||'</Data></Cell>
									<Cell ss:Index="9" ss:StyleID="s104"><Data ss:Type="Number">'||TO_CHAR(Z.DEBT,L_FMT,L_NLS)||'</Data></Cell>
								 </Row><Row/>';                                    
			 P$S(L_XML);					 
		END LOOP;				 
    L_XML := '      
		<Row ss:AutoFitHeight="0">
			<Cell ss:Index="4" ss:MergeAcross="3" ss:StyleID="s110"><Data ss:Type="String">Сальдо на '||TO_CHAR(NVL(P_DATE_ON,SYSDATE),'DD.MM.YYYY')||'</Data></Cell>
			<Cell ss:Index="9" ss:StyleID="s111"><Data ss:Type="Number">'||TO_CHAR(L_SUM,L_FMT,L_NLS)||'</Data></Cell>
		</Row>
		<Row/>
   <Row ss:AutoFitHeight="0">
    <Cell ss:Index="3" ss:MergeAcross="3" ss:StyleID="s106"><Data ss:Type="String">Генеральный директор</Data></Cell>
    <Cell ss:Index="8" ss:MergeAcross="1" ss:StyleID="s101"><Data ss:Type="String">'||PKG_PREF.F$C2('GENDIR')||'</Data></Cell>
   </Row>
	 <Row/>
   <Row ss:AutoFitHeight="0">
    <Cell ss:Index="3" ss:MergeAcross="3" ss:StyleID="s106"><Data ss:Type="String">Главный бухгалтер</Data></Cell>
    <Cell ss:Index="8" ss:MergeAcross="1" ss:StyleID="s101"/>
   </Row>
	 <Row/>
   <Row ss:AutoFitHeight="0">
    <Cell ss:Index="3" ss:MergeAcross="1" ss:StyleID="s105"><Data ss:Type="String">М.П</Data></Cell>
   </Row>
	 <Row/>
   <Row ss:AutoFitHeight="0">
    <Cell ss:Index="3" ss:MergeAcross="3" ss:StyleID="s105"><Data ss:Type="String">Исп. '||INITCAP(PKG_USERS.F_GET_FIO_BY_ID(F$_USR_ID))||'</Data></Cell>
    <Cell ss:MergeAcross="2" ss:StyleID="s101"><Data ss:Type="String">'||L_PHONE||' '||L_EMAIL||'</Data></Cell>
		<Cell ss:StyleID="s101"><Data ss:Type="String">'||TO_CHAR(SYSDATE,'DD.MM.YYYY')||'</Data></Cell>
   </Row>
	 </Table>
  <WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">
   <PageSetup>
    <Header x:Margin="0.31496062992125984"/>
    <Footer x:Margin="0.31496062992125984"/>
    <PageMargins x:Bottom="0.74803149606299213" x:Left="0.70866141732283472"
     x:Right="0.70866141732283472" x:Top="0.74803149606299213"/>
   </PageSetup>   
   <Unsynced/>
   <FitToPage/>
   <Print>    
	 	<FitHeight>0</FitHeight>
    <ValidPrinterInfo/>
    <PaperSizeIndex>9</PaperSizeIndex>
    <Scale>48</Scale>
    <HorizontalResolution>600</HorizontalResolution>
    <VerticalResolution>600</VerticalResolution>
   </Print>
   <PageBreakZoom>130</PageBreakZoom>
   <Selected/>
   <Panes>
    <Pane>
     <Number>3</Number>
     <ActiveRow>13</ActiveRow>
     <ActiveCol>8</ActiveCol>
    </Pane>
   </Panes>
   <ProtectObjects>False</ProtectObjects>
   <ProtectScenarios>False</ProtectScenarios>
  </WorksheetOptions>
 </Worksheet>
</Workbook>';

		p$s( L_XML );
	
	
	END P_ADD_DATA;

---------------------------------------------------------------------------------------------------------------------------------

  FUNCTION RUN_REPORT(P_PAR_01 VARCHAR2 DEFAULT NULL
											,P_PAR_02 VARCHAR2 DEFAULT NULL
											,P_PAR_03 VARCHAR2 DEFAULT NULL
											,P_PAR_04 VARCHAR2 DEFAULT NULL
											,P_PAR_05 VARCHAR2 DEFAULT NULL
											,P_PAR_06 VARCHAR2 DEFAULT NULL
											,P_PAR_07 VARCHAR2 DEFAULT NULL
											,P_PAR_08 VARCHAR2 DEFAULT NULL
											,P_PAR_09 VARCHAR2 DEFAULT NULL
											,P_PAR_10 VARCHAR2 DEFAULT NULL) RETURN LAWSUP.PKG_FILES.REC_DOC
	IS
	l_BLOB LAWSUP.PKG_FILES.REC_DOC;
  BEGIN

	  EXCEL_DOC := NULL;
		DBMS_LOB.CREATETEMPORARY( EXCEL_DOC.P_BLOB, TRUE );
    DBMS_LOB.OPEN( EXCEL_DOC.P_BLOB, DBMS_LOB.LOB_READWRITE );

		P_ADD_DATA(TO_DATE(P_PAR_01,'DD.MM.YYYY'),TO_DATE(P_PAR_02,'DD.MM.YYYY'),TO_DATE(P_PAR_03,'DD.MM.YYYY'),P_PAR_04,P_PAR_06);

		DBMS_LOB.CLOSE(EXCEL_DOC.P_BLOB);

		    l_BLOB.P_BLOB := EXCEL_DOC.P_BLOB;
	      l_BLOB.P_FILE_NAME := 'Расчет задолженности(дет.)';
				RETURN L_BLOB;

  END RUN_REPORT;

end PKG_XLS_REPORT_008;
/

