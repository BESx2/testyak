i�?CREATE OR REPLACE PACKAGE LAWMAIN."PKG_DOC_REPORTS"
	IS


	G_P_N VARCHAR2(200) := 'PKG_DOC_REPORTS.';

	FUNCTION CLOB2BLOB(PAR_CLOB IN CLOB,P_CHARACTER_SET IN VARCHAR2 DEFAULT NULL ) RETURN BLOB;	
  
	--Функция преобразовывает слова под нужный формат RTF
	FUNCTION F_CONVERT_TO_RTF(P_WORD CLOB,
														P_ENCODING VARCHAR2 DEFAULT 'CL8MSWIN1251') RETURN CLOB;      
	
	--Функция преобразовывает слова под нужный формат RTF
	FUNCTION F_CONVERT_TO_RTF(P_WORD VARCHAR2,
														P_ENCODING VARCHAR2 DEFAULT 'CL8MSWIN1251') RETURN VARCHAR2;													
	
	-- Процедура заменяет строки подстановки в документе, используя селект													
	PROCEDURE P_REPLACE_SUBSTITUTION(P_SQL 				   VARCHAR2,                              -- исходный селект
															 		 L_CLOB          IN OUT NOCOPY CLOB,                    -- rtd
		                               P_DATE_FORMAT   VARCHAR2 DEFAULT 'DD.MM.YYYY',         -- формат даты для замены
																	 P_NUMBER_FORMAT VARCHAR2 DEFAULT '999G999G999G990D99', -- формат числа
																	 P_SUBS_START    VARCHAR2 DEFAULT NULL,                  -- символ начала строки замены
																	 P_SUBS_END      VARCHAR2 DEFAULT NULL);                 -- символ, завершающий строку замены
	
  -- Замена клоб 
 	FUNCTION REPLACE_CLOB(IN_SOURCE  IN CLOB
											 ,IN_SEARCH  IN VARCHAR2
											 ,IN_REPLACE IN CLOB) RETURN CLOB;    
											 
	PROCEDURE P_INIT_MERGE(P_TEMPLATE CLOB);	
	
	PROCEDURE P_MERGE_RTF (P_RTF CLOB);
	
	 FUNCTION F_FINISH_MERGE RETURN CLOB;									 
											 
END PKG_DOC_REPORTS;
/

CREATE OR REPLACE PACKAGE BODY LAWMAIN."PKG_DOC_REPORTS"
	IS       
		
	  G_RTF_HEADER CLOB;
		G_RTF_FOOTER CLOB;  
		G_RTF_BODY   CLOB;
	
	PROCEDURE P_DOWNLOAD_FILE (P_FILE IN LAWSUP.PKG_FILES.REC_DOC)
	AS
		-- V_MIME        VARCHAR2 (48);
		 V_LENGTH      NUMBER;
		 LOB_LOC       BLOB;
	BEGIN    
     LOB_LOC  := P_FILE.P_BLOB;     
		 V_LENGTH := DBMS_LOB.getlength(P_FILE.P_BLOB);
	
		 OWA_UTIL.MIME_HEADER (NVL ('RRRR', 'application/word'), FALSE);
		 OWA_UTIL.MIME_HEADER (NVL ('RRRR', 'charset=Windows-1251'), FALSE);

		 HTP.P ('CONTENT-LENGTH: ' || V_LENGTH);
		 HTP.P ('CONTENT-DISPOSITION:  ATTACHMENT; FILENAME="'
						|| P_FILE.P_FILE_NAME||'.DOC'
						|| '"'
					 );
		 OWA_UTIL.HTTP_HEADER_CLOSE;
		 WPG_DOCLOAD.DOWNLOAD_FILE (LOB_LOC);
		 
	END P_DOWNLOAD_FILE;
----------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------------
	
	--Функция преобразовывает слова под нужный формат RTF
	FUNCTION F_CONVERT_TO_RTF(P_WORD CLOB,
														P_ENCODING VARCHAR2 DEFAULT 'CL8MSWIN1251') RETURN CLOB
	IS
  	l_ret CLOB;
	BEGIN
		IF P_WORD IS NULL THEN
			RETURN NULL;
		END IF;    
		DBMS_LOB.createtemporary(L_RET,TRUE);                                        
	--Каждый символ в слове преобразуется в кодировку cp1251
	--Потом он преобразуется в 16-чное число и далее добавляются управляющие символы формата rtf
	--Грубо говоря это "\'" + 16-значение таблицы ASCII для cp1251 
  FOR i IN 1..LENGTH(P_WORD) LOOP
			 DBMS_LOB.append(L_RET,'\'''||UTL_RAW.cast_to_raw(CONVERT(DBMS_LOB.substr(P_WORD,1,I),P_ENCODING)));
		END LOOP;                                                                                
		RETURN l_ret;
	END;		        
	          
	
	--Функция преобразовывает слова под нужный формат RTF
	FUNCTION F_CONVERT_TO_RTF(P_WORD VARCHAR2,
														P_ENCODING VARCHAR2 DEFAULT 'CL8MSWIN1251') RETURN VARCHAR2
	IS
  	l_ret VARCHAR2(32767);
	BEGIN
		IF P_WORD IS NULL THEN
			RETURN NULL;
		END IF;                                            
	--Каждый символ в слове преобразуется в кодировку cp1251
	--Потом он преобразуется в 16-чное число и далее добавляются управляющие символы формата rtf
	--Грубо говоря это "\'" + 16-значение таблицы ASCII для cp1251 
  FOR i IN 1..LENGTH(P_WORD) LOOP
			 l_ret:= l_ret||'\'''||UTL_RAW.cast_to_raw(CONVERT(SUBSTR(P_WORD,i,1),P_ENCODING));
		END LOOP;                                                                                
		RETURN l_ret;
	END;		  


	FUNCTION CLOB2BLOB(PAR_CLOB IN CLOB,P_CHARACTER_SET IN VARCHAR2 DEFAULT NULL ) RETURN BLOB
 			IS
    POS       	PLS_INTEGER  := 1;
    BUFFER    	RAW(32767);
    RES       	BLOB;
    LOB_LEN   	PLS_INTEGER  := DBMS_LOB.GETLENGTH(PAR_CLOB);
		CHUNK_SIZE	PLS_INTEGER := 8000;
	BEGIN
    DBMS_LOB.CREATETEMPORARY( RES, TRUE );
    DBMS_LOB.OPEN( RES, DBMS_LOB.LOB_READWRITE );
		
    LOOP
			IF P_CHARACTER_SET IS NOT NULL  THEN
				BUFFER := UTL_RAW.CAST_TO_RAW(	
																				convert(
																								DBMS_LOB.SUBSTR(PAR_CLOB,CHUNK_SIZE,POS)
																								,P_CHARACTER_SET
																							 )
																			);
			ELSE
				BUFFER := UTL_RAW.CAST_TO_RAW(DBMS_LOB.SUBSTR(PAR_CLOB,CHUNK_SIZE,POS));
			END IF;

      IF UTL_RAW.LENGTH( BUFFER ) > 0 THEN
        DBMS_LOB.WRITEAPPEND( RES, UTL_RAW.LENGTH( BUFFER ), BUFFER );
      END IF;

      POS := POS + CHUNK_SIZE;

      EXIT WHEN pos > lob_len;
    END LOOP;

  	RETURN RES; -- RES IS OPEN HERE
	END CLOB2BLOB;  
	
	
	PROCEDURE P_REPLACE_SUBSTITUTION(P_SQL 				   VARCHAR2,                            
															 		 L_CLOB          IN OUT NOCOPY CLOB,
		                               P_DATE_FORMAT   VARCHAR2 DEFAULT 'DD.MM.YYYY',
																	 P_NUMBER_FORMAT VARCHAR2 DEFAULT '999G999G999G990D99',
																	 P_SUBS_START    VARCHAR2 DEFAULT NULL,
																	 P_SUBS_END      VARCHAR2 DEFAULT NULL)
		IS  
		TYPE ASSOC_ARRAY IS TABLE OF VARCHAR2(32767) INDEX BY VARCHAR2(128);  
		L_ASSOC     		ASSOC_ARRAY;
		L_CURSOR_ID 		INTEGER;
		L_COL_COUNT 		INTEGER;
		L_COLUMNS   		DBMS_SQL.desc_tab;
		L_VARCHAR2  		VARCHAR2(32767) := ' ';
		L_DATE      	 	DATE := SYSDATE; 
		L_NUMBER    	 	NUMBER := '0';
		L_DUMMY     	 	NUMBER;      
		L_SUBS       	  VARCHAR2(128);
	BEGIN                                                   
		-- Открываем курсор
		L_CURSOR_ID := DBMS_SQL.open_cursor;
		DBMS_SQL.parse(L_CURSOR_ID,P_SQL,DBMS_SQL.native);
		--Получаем описание колонок
		DBMS_SQL.describe_columns(L_CURSOR_ID,L_COL_COUNT,L_COLUMNS); 
		--Определяем колонки
		FOR I IN 1..L_COL_COUNT LOOP   
				IF L_COLUMNS(I).COL_TYPE = DBMS_TYPES.TYPECODE_DATE THEN
					DBMS_SQL.define_column(L_CURSOR_ID,I,L_DATE);
				ELSIF L_COLUMNS(I).COL_TYPE = DBMS_TYPES.TYPECODE_NUMBER THEN
					DBMS_SQL.define_column(L_CURSOR_ID,I,L_NUMBER);
				ELSIF L_COLUMNS(I).COL_TYPE IN (DBMS_TYPES.TYPECODE_VARCHAR2,DBMS_TYPES.TYPECODE_VARCHAR) THEN
					DBMS_SQL.define_column(L_CURSOR_ID,I,L_VARCHAR2,4000);
				END IF;
		END LOOP;			                                                                                    
		--Выполнили курсор
	  L_DUMMY := DBMS_SQL.execute(L_CURSOR_ID);
		-- Цикл по курсору
		LOOP
    	IF DBMS_SQL.fetch_rows(L_CURSOR_ID) = 0 THEN
				EXIT;
			END IF;         
			--Для каждой колонки
			FOR I IN 1..L_COL_COUNT LOOP   
				--Определяем тип и записываем в массив
				--Ключ - наименование колонки
				--Значение - значение поля 
				IF L_COLUMNS(I).COL_TYPE = DBMS_TYPES.TYPECODE_DATE THEN
				  DBMS_SQL.column_value(L_CURSOR_ID,I,L_DATE);
				  L_ASSOC(L_COLUMNS(I).COL_NAME):=TO_CHAR(L_DATE,P_DATE_FORMAT); 
				ELSIF L_COLUMNS(I).COL_TYPE = DBMS_TYPES.TYPECODE_NUMBER THEN
				  DBMS_SQL.column_value(L_CURSOR_ID,I,L_NUMBER);
				  L_ASSOC(L_COLUMNS(I).COL_NAME):=TO_CHAR(L_NUMBER,P_NUMBER_FORMAT,'NLS_NUMERIC_CHARACTERS = '', '''); 
				ELSIF L_COLUMNS(I).COL_TYPE IN (DBMS_TYPES.TYPECODE_VARCHAR2,DBMS_TYPES.TYPECODE_VARCHAR) THEN
				  DBMS_SQL.column_value(L_CURSOR_ID,I,L_VARCHAR2);
				  L_ASSOC(L_COLUMNS(I).COL_NAME):= F_CONVERT_TO_RTF(L_VARCHAR2); 				 
				END IF;
			END LOOP;			
		END LOOP;
		--Закрываем курсор
		DBMS_SQL.close_cursor(L_CURSOR_ID); 
		
		--Цикл по массиву
		L_SUBS := L_ASSOC.first;
		WHILE L_SUBS IS NOT NULL LOOP
			--Заменяем в документе необходимые поля
    	 L_CLOB := REPLACE(L_CLOB,P_SUBS_START||UPPER(L_SUBS)||P_SUBS_END,L_ASSOC(L_SUBS));
			 L_SUBS := L_ASSOC.NEXT(L_SUBS);
  	END LOOP;
		
	END;	
	
----------------------------------------------------------------------------------------------	
	                        
 	FUNCTION REPLACE_CLOB(IN_SOURCE  IN CLOB
											 ,IN_SEARCH  IN VARCHAR2
											 ,IN_REPLACE IN CLOB) RETURN CLOB
	 IS
		L_POS PLS_INTEGER;
	BEGIN
		L_POS := INSTR(IN_SOURCE, IN_SEARCH);

		IF L_POS > 0 THEN
			RETURN SUBSTR(IN_SOURCE, 1, L_POS - 1) || IN_REPLACE || SUBSTR(IN_SOURCE, L_POS +
																																			LENGTH(IN_SEARCH));
		END IF;

		RETURN IN_SOURCE;
	END REPLACE_CLOB;

-------------------------------------------------------------------------------------------------
  
  PROCEDURE P_INIT_MERGE(P_TEMPLATE CLOB)
    IS
  BEGIN
     G_RTF_HEADER := SUBSTR(P_TEMPLATE,1,INSTR(P_TEMPLATE,'\pard')-1);
     G_RTF_FOOTER := SUBSTR(P_TEMPLATE,INSTR(P_TEMPLATE,'\par',-1)+6);
     G_RTF_BODY   := null;
  END;
                 
  PROCEDURE P_MERGE_RTF (P_RTF CLOB)
    IS
  BEGIN    
    IF G_RTF_BODY IS NULL THEN
      G_RTF_BODY := G_RTF_BODY||SUBSTR(P_RTF,INSTR(P_RTF,'\pard'),INSTR(P_RTF,'\par',-1)-INSTR(P_RTF,'\pard')+6);
    ELSE      
      G_RTF_BODY := G_RTF_BODY||'{\rtlch\fcs1 \ltrch\fcs0 \pagebb \par}'||SUBSTR(P_RTF,INSTR(P_RTF,'\pard'),INSTR(P_RTF,'\par',-1)-INSTR(P_RTF,'\pard')+6);
    END IF;
  END;                                                                                                                  
  
  FUNCTION F_FINISH_MERGE RETURN CLOB
    IS
  BEGIN
     RETURN G_RTF_HEADER||G_RTF_BODY||G_RTF_FOOTER;
  END;

END PKG_DOC_REPORTS;
/

