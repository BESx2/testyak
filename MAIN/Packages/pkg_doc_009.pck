i�?CREATE OR REPLACE PACKAGE LAWMAIN.PKG_DOC_009 IS

	-- Author  : BES
	-- Created : 14.02.2019
	-- Purpose : Исковое заявление 
	-- CODE    : LAWSUIT

	FUNCTION RUN_REPORT(P_PAR_01 VARCHAR2 DEFAULT NULL
										 ,P_PAR_02 VARCHAR2 DEFAULT NULL
										 ,P_PAR_03 VARCHAR2 DEFAULT NULL
										 ,P_PAR_04 VARCHAR2 DEFAULT NULL
										 ,P_PAR_05 VARCHAR2 DEFAULT NULL
										 ,P_PAR_06 VARCHAR2 DEFAULT NULL
										 ,P_PAR_07 VARCHAR2 DEFAULT NULL
										 ,P_PAR_08 VARCHAR2 DEFAULT NULL
										 ,P_PAR_09 VARCHAR2 DEFAULT NULL
										 ,P_PAR_10 VARCHAR2 DEFAULT NULL)
		RETURN LAWSUP.PKG_FILES.REC_DOC;

END PKG_DOC_009;
/

CREATE OR REPLACE PACKAGE BODY LAWMAIN.PKG_DOC_009 IS

	TYPE REC_DOC IS RECORD(
		 P_BLOB BLOB DEFAULT NULL);

	RTF_DOC REC_DOC := NULL;

	------------------------------------------------------------------------       
	FUNCTION F_GET_TABLE(P_ID_CASE NUMBER, P_CIPHER VARCHAR2) RETURN CLOB
		IS
		L_RET CLOB;   
		L_HEADER CLOB;
		L_ROW_TMP CLOB; 
		L_FMT  VARCHAR2(50) := 'FM999G999G999G999G990D00';
		L_NLS  VARCHAR2(50) := 'NLS_NUMERIC_CHARACTERS='', ''';
	BEGIN                                                                  
		--Хедер таблицы
		L_HEADER := q'|\trowd \irow0\irowband0\ltrrow\ts16\trgaph108\trleft0\trbrdrt\brdrs\brdrw10 \trbrdrl\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 \trbrdrr\brdrs\brdrw10 \trbrdrh\brdrs\brdrw10 \trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddft3\trpaddfb3\trpaddfr3\tblrsid12588567\tbllkhdrrows\tbllkhdrcols\tbllknocolband\tblind108\tblindtype3 \clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb
\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth2355\clshdrawnil \cellx2355\clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth2464\clshdrawnil \cellx4819
\clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth2464\clshdrawnil \cellx7283\clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr
\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth2640\clshdrawnil \cellx9923\pard\plain \ltrpar\qc \li0\ri0\sl276\slmult1\widctlpar\intbl\wrapdefault\aspalpha\aspnum\faauto\adjustright\rin0\lin0\pararsid5406410\yts16 \rtlch\fcs1 \af31507\afs22\alang1025 
\ltrch\fcs0 \f31506\fs22\lang1049\langfe1033\cgrid\langnp1049\langfenp1033 {\rtlch\fcs1 \af1\afs24 \ltrch\fcs0 \f1\fs24\insrsid11823939\charrsid15031512 \'cd\'ee\'ec\'e5\'f0 \'f1\'f7\'e5\'f2\'e0-\'f4\'e0\'ea\'f2\'f3\'f0\'fb / \'e4\'e0\'f2\'e0\cell \'d1
\'f3\'ec\'ec\'e0 \'e2\'fb\'f1\'f2\'e0\'e2\'eb\'e5\'ed\'ed\'ee\'e3\'ee \'f1\'f7\'e5\'f2\'e0, \'f0\'f3\'e1.\cell \'ce\'ef\'eb\'e0\'f7\'e5\'ed\'ed\'e0\'ff \'f1\'f3\'ec\'ec\'e0, \'f0\'f3\'e1.\cell \'d1\'f3\'ec\'ec\'e0 \'e7\'e0\'e4\'ee\'eb\'e6\'e5\'ed\'ed\'ee
\'f1\'f2\'e8, \'f0\'f3\'e1.\cell }\pard\plain \ltrpar\ql \li0\ri0\sa200\sl276\slmult1\widctlpar\intbl\wrapdefault\aspalpha\aspnum\faauto\adjustright\rin0\lin0 \rtlch\fcs1 \af31507\afs22\alang1025 \ltrch\fcs0 
\f31506\fs22\lang1049\langfe1033\cgrid\langnp1049\langfenp1033 {\rtlch\fcs1 \af31507 \ltrch\fcs0 \insrsid11823939\charrsid15031512 \trowd \irow0\irowband0\ltrrow\ts16\trgaph108\trleft0\trbrdrt\brdrs\brdrw10 \trbrdrl\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 \trbrdrh\brdrs\brdrw10 \trbrdrv\brdrs\brdrw10 \trftsWidth1\trftsWidthB3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddft3\trpaddfb3\trpaddfr3\tblrsid12588567\tbllkhdrrows\tbllkhdrcols\tbllknocolband\tblind108\tblindtype3 
\clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth2355\clshdrawnil \cellx2355\clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr
\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth2464\clshdrawnil \cellx4819\clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth2464\clshdrawnil \cellx7283\clvertalt\clbrdrt
\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth2640\clshdrawnil \cellx9923\row \ltrrow}|';

   --шаблон строки
    L_ROW_TMP := q'|\pard\plain \ltrpar\qj \li0\ri0\sl276\slmult1
\widctlpar\intbl\wrapdefault\aspalpha\aspnum\faauto\adjustright\rin0\lin0\pararsid5406410\yts16 \rtlch\fcs1 \af31507\afs22\alang1025 \ltrch\fcs0 \f31506\fs22\lang1049\langfe1033\cgrid\langnp1049\langfenp1033 {\rtlch\fcs1 \af1\afs24 \ltrch\fcs0 
\f1\fs24\insrsid11823939\charrsid15031512 INVOICE\cell AMOUNT\cell PAYED\cell DEBT\cell }\pard\plain \ltrpar\ql \li0\ri0\sa200\sl276\slmult1\widctlpar\intbl\wrapdefault\aspalpha\aspnum\faauto\adjustright\rin0\lin0 \rtlch\fcs1 \af31507\afs22\alang1025 \ltrch\fcs0 
\f31506\fs22\lang1049\langfe1033\cgrid\langnp1049\langfenp1033 {\rtlch\fcs1 \af31507 \ltrch\fcs0 \insrsid11823939\charrsid15031512 \trowd \irow1\irowband1\lastrow \ltrrow\ts16\trgaph108\trleft0\trbrdrt\brdrs\brdrw10 \trbrdrl\brdrs\brdrw10 \trbrdrb
\brdrs\brdrw10 \trbrdrr\brdrs\brdrw10 \trbrdrh\brdrs\brdrw10 \trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddft3\trpaddfb3\trpaddfr3\tblrsid12588567\tbllkhdrrows\tbllkhdrcols\tbllknocolband\tblind108\tblindtype3 \clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb
\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth2355\clshdrawnil \cellx2355\clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth2464\clshdrawnil \cellx4819
\clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth2464\clshdrawnil \cellx7283\clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr
\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth2640\clshdrawnil \cellx9923\row }|';


    L_RET := L_HEADER;

    FOR I IN (SELECT FD.DOC_NUMBER, FD.DOC_DATE
			               ,TO_CHAR(FD.AMOUNT,L_FMT,L_NLS) AMOUNT 
										 ,TO_CHAR(FD.DEBT,L_FMT,L_NLS) DEBT
										 ,TO_CHAR(FD.AMOUNT - FD.DEBT,L_FMT,L_NLS) PAYED
										 ,FD.DEBT DEBT_NUM
			        FROM T_CLI_DEBTS D, T_CLI_FINDOCS FD
			        WHERE FD.ID_DOC = D.ID_DOC --AND FD.DEBT > 0
							AND   FD.SERV_TYPE IN (SELECT COLUMN_VALUE FROM TABLE(APEX_STRING.split(P_CIPHER,',')))
							AND   D.ID_WORK = P_ID_CASE
							ORDER BY FD.DOC_DATE)
		LOOP
			L_RET := L_RET||L_ROW_TMP;
			L_RET := REPLACE(L_RET,'INVOICE',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(I.DOC_NUMBER||' от '||TO_CHAR(I.DOC_DATE,'DD.MM.YYYY')));
			L_RET := REPLACE(L_RET,'AMOUNT',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(I.AMOUNT));			
			L_RET := REPLACE(L_RET,'PAYED',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(I.PAYED));
			L_RET := REPLACE(L_RET,'DEBT',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(I.DEBT));
		END LOOP;					                                                     
		
		   
		RETURN L_RET;
	END;
	
	
	------------------------------------------------------------------------
	FUNCTION REPLACE_CLOB(IN_SOURCE  IN CLOB
											 ,IN_SEARCH  IN VARCHAR2
											 ,IN_REPLACE IN CLOB) RETURN CLOB IS
		L_POS PLS_INTEGER;
	BEGIN
		L_POS := INSTR(IN_SOURCE, IN_SEARCH);

		IF L_POS > 0 THEN
			RETURN SUBSTR(IN_SOURCE, 1, L_POS - 1) || IN_REPLACE || SUBSTR(IN_SOURCE, L_POS +
																																			LENGTH(IN_SEARCH));
		END IF;

		RETURN IN_SOURCE;
	END REPLACE_CLOB;

	PROCEDURE P_ADD_DATA(P_ID_CASE NUMBER, P_ID_COURT NUMBER) IS
		L_BLOB BLOB;
		L_RTF  CLOB;  
		L_BODY CLOB;
		L_TEMP CLOB;
		L_VAL  VARCHAR2(10000);
		L_FMT  VARCHAR2(50) := 'FM999G999G999G999G990D00';
		L_NLS  VARCHAR2(50) := 'NLS_NUMERIC_CHARACTERS='', ''';
		L_CNT  NUMBER;     
		L_SUM  NUMBER;
		L_ABON_GROUP VARCHAR2(50);
		L_PENY_GROUP VARCHAR2(50);                    
		L_ID_CLIENT  NUMBER;
	BEGIN
    SELECT S.ABON_GROUP, S.PENY_GROUP_CALC, S.ID_CLIENT 
		INTO   L_ABON_GROUP, L_PENY_GROUP, L_ID_CLIENT		
		FROM V_CLI_CASES S WHERE S.ID_CASE = P_ID_CASE;                
		
		IF L_ABON_GROUP IN ('UK','TSG') THEN
  		SELECT T.FILE_CONTENT INTO L_RTF FROM T_RTF_TEMPLATES T WHERE  T.ID = 12;
			IF L_PENY_GROUP = 'UK' THEN
     	  SELECT T.FILE_CONTENT INTO L_TEMP FROM T_RTF_TEMPLATES T WHERE  T.ID = 26;
			ELSE
	   		SELECT T.FILE_CONTENT INTO L_TEMP FROM T_RTF_TEMPLATES T WHERE  T.ID = 27;
			END IF;	                                                                    
			L_RTF := REPLACE_CLOB(L_RTF,'PENYBODY',L_TEMP);
		ELSE
			SELECT T.FILE_CONTENT INTO L_RTF FROM T_RTF_TEMPLATES T WHERE  T.ID = 28;      
			IF L_PENY_GROUP = 'TSG' THEN
     	  SELECT T.FILE_CONTENT INTO L_TEMP FROM T_RTF_TEMPLATES T WHERE  T.ID = 36;
			ELSE
	   		SELECT T.FILE_CONTENT INTO L_TEMP FROM T_RTF_TEMPLATES T WHERE  T.ID = 35;
			END IF;	                                                                    
			L_RTF := REPLACE_CLOB(L_RTF,'PENYBODY',L_TEMP);

		END IF;	
	  	
				
		FOR I IN (SELECT TO_CHAR(S.LAWSUIT_SUM,L_FMT,L_NLS) LAWSUIT_SUM
										,TO_CHAR(S.LAWSUIT_PENY,L_FMT,L_NLS) LAWSUIT_PENY
										,TO_CHAR(S.LAWSUIT_TOLL,L_FMT,L_NLS) LAWSUIT_TOLL
										,TO_CHAR(S.SUM_DEBT,L_FMT,L_NLS) SUM_DEBT
										,TO_CHAR(S.PENY,L_FMT,L_NLS) PENY
										,TO_CHAR(S.GOV_TOLL,L_FMT,L_NLS) GOV_TOLL     
										,TO_CHAR(S.SUM_DEBT + S.PENY,L_FMT,L_NLS) CASE_ISK_SUM
										,TO_CHAR(S.LAWSUIT_SUM + S.LAWSUIT_PENY,L_FMT,L_NLS) ISK_SUM
										,S.CLI_NAME
										,C.COURT_NAME
										,C.ADDRESS
										,S.CTR_NUMBER
										,S.CTR_DATE    
										,S.LAWSUIT_PENY_DATE
										,S.PENY_DATE
										,LOWER(PKG_UTILS.F_GET_NAME_MONTHS(CD.MIN_DATE, 'R')) || ' ' || TO_CHAR(CD.MIN_DATE, 'YYYY') || ' года' MIN_PER
										,LOWER(PKG_UTILS.F_GET_NAME_MONTHS(CD.MAX_DATE, 'I')) || ' ' ||TO_CHAR(CD.MAX_DATE, 'YYYY') || ' год' MAX_PER
										,INITCAP(SUBSTR(UL.FIRST_NAME, 1, 1) || '. ' ||SUBSTR(UL.SECOND_NAME, 1, 1) || '. ' ||UL.LAST_NAME) CURATOR
										,UL.PHONE
										,UL.PHONE_PRIV 
										,(SELECT T.DET_VAL
											FROM (SELECT CD.DET_VAL
																	,CD.ID_CLIENT
																	,ROW_NUMBER() OVER(PARTITION BY CD.ID_CLIENT ORDER BY T.ORDER_NUM DESC) AS RN
														FROM   T_CLI_CONTACT_DETAILS CD, T_CLI_CONTACT_TYPES T
														WHERE  T.DET_TYPE = 'Адрес'     
														AND    CD.ID_CLIENT = L_ID_CLIENT
														AND    T.DET_DESC IN ('Юридический адрес контрагента','Юридический адрес контрагента ЕГРЮЛ')
														AND    CD.DET_DESC = T.DET_DESC) T
											WHERE T.RN = 1) CLIADDRESS
										,(SELECT MAX(EV.DATE_EVENT) FROM V_CLI_EVENTS EV WHERE EV.id_cli_case = S.ID_CASE AND EV.code_event = 'PRETENSION_SEND') PRETDATE
										,S.CODE_STATUS
										,(SELECT MAX(J.DISTR_DATE)
									    FROM T_CTR_JOURNAL J 
											WHERE J.JRNL_DATE = (SELECT MAX(J2.JRNL_DATE) FROM T_CTR_JOURNAL J2 
                      									   WHERE J2.ID_CONTRACT = S.ID_CONTRACT)) CTR_END_DATE
							FROM   V_CLI_CASES S
										,T_COURTS C    										
										,(SELECT MIN(F.DOC_DATE) MIN_DATE
														,MAX(F.DOC_DATE) MAX_DATE
											FROM   T_CLI_DEBTS CD, T_CLI_FINDOCS F
											WHERE  CD.ID_WORK = P_ID_CASE
											AND    F.ID_DOC = CD.ID_DOC) CD
										,T_USER_LIST UL
							WHERE  S.ID_CASE = P_ID_CASE
							AND    C.ID_COURT = P_ID_COURT
							AND    UL.ID(+) = S.CURATOR_COURT)
		LOOP
			L_RTF := REPLACE(L_RTF,'COURTNAME',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(I.COURT_NAME));
			L_RTF := REPLACE(L_RTF,'COURTADDRESS',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(I.ADDRESS));
			L_RTF := REPLACE(L_RTF,'CLINAME',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(I.CLI_NAME));
			L_RTF := REPLACE(L_RTF,'CLIADDRESS',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(I.CLIADDRESS));
			L_RTF := REPLACE(L_RTF,'CTRNUMBER',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(I.CTR_NUMBER));
			L_RTF := REPLACE(L_RTF,'CTRDATE',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(I.CTR_DATE));
			L_RTF := REPLACE(L_RTF,'PERIODS',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(I.MIN_PER||' по '||I.MAX_PER));
			L_RTF := REPLACE(L_RTF,'CTRENDDATE','________');
			L_RTF := REPLACE(L_RTF,'PRETDATE',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(TO_CHAR(I.PRETDATE,'DD.MM.YYYY')));
			IF I.CODE_STATUS IN ('WORK','ASSIGN') OR I.LAWSUIT_SUM IS NULL OR I.LAWSUIT_PENY IS NULL OR I.LAWSUIT_TOLL IS NULL THEN
				 L_RTF := REPLACE(L_RTF,'ISKSUM',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(I.CASE_ISK_SUM));
				 L_RTF := REPLACE(L_RTF,'ISKDEBT',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(I.SUM_DEBT));
 				 L_RTF := REPLACE(L_RTF,'ISKPENY',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(I.PENY));
 				 L_RTF := REPLACE(L_RTF,'ISKTOLL',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(I.GOV_TOLL));
				 L_RTF := REPLACE(L_RTF,'ISKDATEPENY',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(TO_CHAR(I.PENY_DATE+1,'DD.MM.YYYY')));
			ELSE
				 L_RTF := REPLACE(L_RTF,'ISKSUM',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(I.ISK_SUM));
				 L_RTF := REPLACE(L_RTF,'ISKDEBT',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(I.LAWSUIT_SUM));
 				 L_RTF := REPLACE(L_RTF,'ISKPENY',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(I.LAWSUIT_PENY));
 				 L_RTF := REPLACE(L_RTF,'ISKTOLL',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(I.LAWSUIT_TOLL));					 
 				 L_RTF := REPLACE(L_RTF,'ISKDATEPENY',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(TO_CHAR(I.LAWSUIT_PENY_DATE+1,'DD.MM.YYYY')));
			END IF;               
			
			L_RTF := REPLACE(L_RTF,'CURATOR',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(I.CURATOR));
			L_RTF := REPLACE(L_RTF,'PHONEWORK',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(I.PHONE));
			L_RTF := REPLACE(L_RTF,'PHONEPRIV',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(PKG_UTILS.F_MAKE_PHONE(I.PHONE_PRIV)));			
			L_RTF := REPLACE(L_RTF,'ORGANIZATION',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(PKG_PREF.F$C2('REQUISITES')));																				
		END LOOP;                                                                            
		
		--ПРОВЕРЯЕМ НА 120 ШИФР
		SELECT COUNT(1),SUM(DEBT) INTO L_CNT,L_SUM	FROM T_CLI_DEBTS CD, T_CLI_FINDOCS FD    
		WHERE CD.ID_DOC = FD.ID_DOC	AND   FD.SERV_TYPE = '120'
		AND   FD.DEBT > 0 AND   CD.ID_WORK = P_ID_CASE;
	  
		IF L_CNT > 0 THEN 
			IF L_ABON_GROUP IN ('UK','TSG') THEN
 			  SELECT T.FILE_CONTENT INTO L_TEMP FROM T_RTF_TEMPLATES T WHERE T.ID = 13;  
			ELSE
				SELECT T.FILE_CONTENT INTO L_TEMP FROM T_RTF_TEMPLATES T WHERE T.ID = 29;  
			END IF;	 
			L_TEMP := REPLACE_CLOB(L_TEMP,'TABLE',F_GET_TABLE(P_ID_CASE,'120'));
			L_TEMP := REPLACE(L_TEMP,'TOTALDEBT',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(TO_CHAR(L_SUM,L_FMT,L_NLS)));			    
			L_BODY := L_BODY||L_TEMP;
		END IF;
		
		--ПРОВЕРЯЕМ НА 142 ШИФР
		SELECT COUNT(1),SUM(DEBT) INTO L_CNT,L_SUM	FROM T_CLI_DEBTS CD, T_CLI_FINDOCS FD    
		WHERE CD.ID_DOC = FD.ID_DOC	AND   FD.SERV_TYPE = '142'
		AND   FD.DEBT > 0 AND   CD.ID_WORK = P_ID_CASE;
	  
		IF L_CNT > 0 THEN
			SELECT T.FILE_CONTENT INTO L_TEMP FROM T_RTF_TEMPLATES T WHERE T.ID = 14;  
			L_TEMP := REPLACE_CLOB(L_TEMP,'TABLE',F_GET_TABLE(P_ID_CASE,'142'));
			L_TEMP := REPLACE(L_TEMP,'TOTALDEBT',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(TO_CHAR(L_SUM,L_FMT,L_NLS)));			    
			L_BODY := L_BODY||L_TEMP;
		END IF;
		
		--ПРОВЕРЯЕМ НА 140 ШИФР
		SELECT COUNT(1),SUM(DEBT) INTO L_CNT,L_SUM	FROM T_CLI_DEBTS CD, T_CLI_FINDOCS FD    
		WHERE CD.ID_DOC = FD.ID_DOC	AND   FD.SERV_TYPE = '140'
		AND   FD.DEBT > 0 AND   CD.ID_WORK = P_ID_CASE;
	  
		IF L_CNT > 0 THEN
			SELECT T.FILE_CONTENT INTO L_TEMP FROM T_RTF_TEMPLATES T WHERE T.ID = 15;  
			L_TEMP := REPLACE_CLOB(L_TEMP,'TABLE',F_GET_TABLE(P_ID_CASE,'140'));
			L_TEMP := REPLACE(L_TEMP,'TOTALDEBT',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(TO_CHAR(L_SUM,L_FMT,L_NLS)));			    
			L_BODY := L_BODY||L_TEMP;
		END IF;   
		
		--ПРОВЕРЯЕМ НА 126 ШИФР
		SELECT COUNT(1),SUM(DEBT) INTO L_CNT,L_SUM	FROM T_CLI_DEBTS CD, T_CLI_FINDOCS FD    
		WHERE CD.ID_DOC = FD.ID_DOC	AND   FD.SERV_TYPE = '126'
		AND   FD.DEBT > 0 AND   CD.ID_WORK = P_ID_CASE;
	  
		IF L_CNT > 0 THEN
			SELECT T.FILE_CONTENT INTO L_TEMP FROM T_RTF_TEMPLATES T WHERE T.ID = 16;  
			L_TEMP := REPLACE_CLOB(L_TEMP,'TABLE',F_GET_TABLE(P_ID_CASE,'126'));
			L_TEMP := REPLACE(L_TEMP,'TOTALDEBT',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(TO_CHAR(L_SUM,L_FMT,L_NLS)));			    
			L_BODY := L_BODY||L_TEMP;
		END IF;		

		SELECT COUNT(1),SUM(DEBT) INTO L_CNT,L_SUM	FROM T_CLI_DEBTS CD, T_CLI_FINDOCS FD    
		WHERE CD.ID_DOC = FD.ID_DOC	AND   FD.SERV_TYPE IN ('210','220','310')
		AND   FD.DEBT > 0 AND   CD.ID_WORK = P_ID_CASE;
	  
		IF L_CNT > 0 THEN
			SELECT T.FILE_CONTENT INTO L_TEMP FROM T_RTF_TEMPLATES T WHERE T.ID = 30;  
			L_TEMP := REPLACE_CLOB(L_TEMP,'TABLE',F_GET_TABLE(P_ID_CASE,'210,220,310'));
			L_TEMP := REPLACE(L_TEMP,'TOTALDEBT',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(TO_CHAR(L_SUM,L_FMT,L_NLS)));			    
			L_BODY := L_BODY||L_TEMP;
		END IF;	
		
		SELECT COUNT(1),SUM(DEBT) INTO L_CNT,L_SUM	FROM T_CLI_DEBTS CD, T_CLI_FINDOCS FD    
		WHERE CD.ID_DOC = FD.ID_DOC	AND   FD.SERV_TYPE = '320'
		AND   FD.DEBT > 0 AND   CD.ID_WORK = P_ID_CASE;
	  
		IF L_CNT > 0 THEN
			SELECT T.FILE_CONTENT INTO L_TEMP FROM T_RTF_TEMPLATES T WHERE T.ID = 31;  
			L_TEMP := REPLACE_CLOB(L_TEMP,'TABLE',F_GET_TABLE(P_ID_CASE,'320'));
			L_TEMP := REPLACE(L_TEMP,'TOTALDEBT',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(TO_CHAR(L_SUM,L_FMT,L_NLS)));			    
			L_BODY := L_BODY||L_TEMP;
		END IF;			 
		
		SELECT COUNT(1),SUM(DEBT) INTO L_CNT,L_SUM	FROM T_CLI_DEBTS CD, T_CLI_FINDOCS FD    
		WHERE CD.ID_DOC = FD.ID_DOC	AND   FD.SERV_TYPE = '300'
		AND   FD.DEBT > 0 AND   CD.ID_WORK = P_ID_CASE;
	  
		IF L_CNT > 0 THEN
			SELECT T.FILE_CONTENT INTO L_TEMP FROM T_RTF_TEMPLATES T WHERE T.ID = 32;  
			L_TEMP := REPLACE_CLOB(L_TEMP,'TABLE',F_GET_TABLE(P_ID_CASE,'300'));
			L_TEMP := REPLACE(L_TEMP,'TOTALDEBT',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(TO_CHAR(L_SUM,L_FMT,L_NLS)));			    
			L_BODY := L_BODY||L_TEMP;
		END IF;			  
		
		
	  SELECT COUNT(1),SUM(DEBT) INTO L_CNT,L_SUM	FROM T_CLI_DEBTS CD, T_CLI_FINDOCS FD    
		WHERE CD.ID_DOC = FD.ID_DOC	AND   FD.SERV_TYPE = '520'
		AND   FD.DEBT > 0 AND   CD.ID_WORK = P_ID_CASE;
	  
		IF L_CNT > 0 THEN
			SELECT T.FILE_CONTENT INTO L_TEMP FROM T_RTF_TEMPLATES T WHERE T.ID = 33;  
			L_TEMP := REPLACE_CLOB(L_TEMP,'TABLE',F_GET_TABLE(P_ID_CASE,'520'));
			L_TEMP := REPLACE(L_TEMP,'TOTALDEBT',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(TO_CHAR(L_SUM,L_FMT,L_NLS)));			    
			L_BODY := L_BODY||L_TEMP;
		END IF;		
		
	  SELECT COUNT(1),SUM(DEBT) INTO L_CNT,L_SUM	FROM T_CLI_DEBTS CD, T_CLI_FINDOCS FD    
		WHERE CD.ID_DOC = FD.ID_DOC	AND   FD.SERV_TYPE = '531'
		AND   FD.DEBT > 0 AND   CD.ID_WORK = P_ID_CASE;
	  
		IF L_CNT > 0 THEN
			SELECT T.FILE_CONTENT INTO L_TEMP FROM T_RTF_TEMPLATES T WHERE T.ID = 34;  
			L_TEMP := REPLACE_CLOB(L_TEMP,'TABLE',F_GET_TABLE(P_ID_CASE,'531'));
			L_TEMP := REPLACE(L_TEMP,'TOTALDEBT',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(TO_CHAR(L_SUM,L_FMT,L_NLS)));			    
			L_BODY := L_BODY||L_TEMP;
		END IF;			
		
		
		L_RTF := REPLACE_CLOB(L_RTF,'ISKBODY',L_BODY);
		
		
		L_BLOB         := PKG_DOC_REPORTS.CLOB2BLOB(PAR_CLOB => L_RTF);
		RTF_DOC.P_BLOB := L_BLOB;
	
	END P_ADD_DATA;

	---------------------------------------------------------------------------------------------------------------------------------

	FUNCTION RUN_REPORT(P_PAR_01 VARCHAR2 DEFAULT NULL
										 ,P_PAR_02 VARCHAR2 DEFAULT NULL
										 ,P_PAR_03 VARCHAR2 DEFAULT NULL
										 ,P_PAR_04 VARCHAR2 DEFAULT NULL
										 ,P_PAR_05 VARCHAR2 DEFAULT NULL
										 ,P_PAR_06 VARCHAR2 DEFAULT NULL
										 ,P_PAR_07 VARCHAR2 DEFAULT NULL
										 ,P_PAR_08 VARCHAR2 DEFAULT NULL
										 ,P_PAR_09 VARCHAR2 DEFAULT NULL
										 ,P_PAR_10 VARCHAR2 DEFAULT NULL)
		RETURN LAWSUP.PKG_FILES.REC_DOC IS
		REC_DOC LAWSUP.PKG_FILES.REC_DOC;
	BEGIN
	
		RTF_DOC := NULL;
		P_ADD_DATA(TO_NUMBER(P_PAR_01),TO_NUMBER(P_PAR_02));
		REC_DOC.P_BLOB      := RTF_DOC.P_BLOB;
		REC_DOC.P_FILE_NAME := 'ISKOVOE_' || P_PAR_01;
	
		RETURN REC_DOC;
	
	END RUN_REPORT;

END PKG_DOC_009;
/

