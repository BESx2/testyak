i�?create or replace package lawmain.PKG_CLIENTS_UTL is

  -- Author  : EUGEN
  -- Created : 26.12.2017 18:11:09
  -- Purpose :  ПАКЕТ С РАЗДИЧНЫМИ УТИЛИТАМИ ПО ЮРИКАМ
  
	
	
	PROCEDURE P_CHANGE_CLI_FLG(P_ID_CLIENT NUMBER
		                        ,P_FLG_NAME  VARCHAR2
														,P_FLG_VAL   VARCHAR2);
	

	PROCEDURE P_ASSIGN_CURATOR_PRE(P_ID_CLIENT NUMBER
																,P_CUR_PRE   NUMBER);
																
  FUNCTION F_GET_REPRES_FIO_REVERSE(P_ID_REP NUMBER)  RETURN VARCHAR2;																
end PKG_CLIENTS_UTL;
/

create or replace package body lawmain.PKG_CLIENTS_UTL is

	PROCEDURE P_CHANGE_CLI_FLG(P_ID_CLIENT NUMBER
		                        ,P_FLG_NAME  VARCHAR2
														,P_FLG_VAL   VARCHAR2)
	  IS
		L_CNT NUMBER;  
		L_CUR_FLG VARCHAR2(10);
		TYPE FLG_CUR IS REF CURSOR;
		L_CUR FLG_CUR;
	BEGIN
		SELECT COUNT(1) INTO L_CNT FROM USER_TAB_COLS C WHERE C.TABLE_NAME = 'T_CLIENTS' AND C.COLUMN_NAME = P_FLG_NAME;
		IF L_CNT = 0 THEN
			 RAISE_APPLICATION_ERROR(-20200,'Неверно указан флаг');			 
		END IF;                                               
		
		OPEN L_CUR FOR 'SELECT '||P_FLG_NAME||' FROM T_CLIENTS S WHERE S.ID_CLIENT = :P1' USING  P_ID_CLIENT;
		FETCH L_CUR INTO L_CUR_FLG;
		CLOSE L_CUR;
    
		IF L_CUR_FLG != P_FLG_VAL THEN                                                    
			FOR I IN (SELECT MAX(ST.ID) ID FROM T_CLI_FLAG_STATUS ST WHERE ST.FLG_NAME = P_FLG_NAME	AND   ST.ID_CLIENT = P_ID_CLIENT)
			LOOP
				 UPDATE T_CLI_FLAG_STATUS EV SET EV.DATE_TO = SYSDATE	 WHERE EV.ID = I.ID;
			END LOOP;                                                                  
			
		  INSERT INTO T_CLI_FLAG_STATUS(ID,ID_CLIENT,FLG_NAME,FLG_STATUS,DATE_CHANGE,CREATED_BY)
		  VALUES(SEQ_CLI_FLG.NEXTVAL,P_ID_CLIENT,P_FLG_NAME,P_FLG_VAL,SYSDATE,F$_USR_ID);
			
			EXECUTE IMMEDIATE 'UPDATE T_CLIENTS s SET s.'||P_FLG_NAME||' = :1  WHERE s.id_client = :2' USING P_FLG_VAL, P_ID_CLIENT;                 
		END IF;		
	END;        
	
----------------------------------------------------------------------------------------------------------------

	PROCEDURE P_ASSIGN_CURATOR_PRE(P_ID_CLIENT NUMBER
																,P_CUR_PRE   NUMBER) 
    IS
	BEGIN
		UPDATE T_CLIENTS S SET S.CURATOR_PRE = P_CUR_PRE WHERE S.ID_CLIENT = P_ID_CLIENT;
  END;       

-------------------------------------------------------------------------------------------------------------

 FUNCTION F_GET_REPRES_FIO_REVERSE(P_ID_REP NUMBER)  RETURN VARCHAR2
	 IS
	 L_FIO VARCHAR2(500);
	 L_COL  wwv_flow_t_varchar2;
 BEGIN
	 SELECT R.FIO INTO L_FIO FROM T_CLI_REPRES R WHERE R.ID_REPRES = P_ID_REP;
	 L_COL := APEX_STRING.SPLIT(L_FIO,' ');
	 IF L_COL.COUNT < 3 THEN 
		 RETURN L_FIO;
	 END IF;
	 L_FIO := SUBSTR(L_COL(2),1,1)||'. '||SUBSTR(L_COL(3),1,1)||'. '||L_COL(1);
   
	 RETURN L_FIO;
 END;
			 
end PKG_CLIENTS_UTL;
/

