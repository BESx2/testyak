i�?create or replace package lawmain.PKG_DOCX_017 is

  -- Author  : EUGEN
  -- Created : 16.03.2020 15:02:04
  -- Purpose : Ходатайство
	-- CODE    : RESTRICTION
  
  FUNCTION RUN_REPORT(P_PAR_01  IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_02 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_03 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_04 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_05 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_06 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_07 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_08 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_09 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_10 IN VARCHAR2 DEFAULT NULL) RETURN LAWSUP.PKG_FILES.REC_DOC;		                           

end PKG_DOCX_017;
/

create or replace package body lawmain.PKG_DOCX_017 is

  FUNCTION RUN_REPORT(P_PAR_01  IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_02 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_03 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_04 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_05 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_06 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_07 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_08 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_09 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_10 IN VARCHAR2 DEFAULT NULL) RETURN LAWSUP.PKG_FILES.REC_DOC
   IS  
   L_XML XMLTYPE;
   L_SQL SYS_REFCURSOR;     
	 L_GROUP VARCHAR2(50);
   L_REP LAWSUP.PKG_FILES.REC_DOC;    
  BEGIN         
    OPEN L_SQL FOR 
      SELECT S.CLI_ALT_NAME "абонент",
			       PKG_CLIENTS.F_GET_ADDRESS(S.ID_CLIENT) "адрес_абонента",
						 CASE 
							 WHEN S.ID_CWS_CONTRACT IS NOT NULL AND S.ID_SEW_CONTRACT IS NOT NULL THEN
								 'холодного водоснабжения № '||s.CWS_CTR_NUMBER||' от '||TO_CHAR(s.CWS_CTR_DATE,'dd.mm.yyyy"г."')||
								 ' и водоотведения № '||s.SEW_CTR_NUMBER||' от '||TO_CHAR(S.SEW_CTR_DATE,'DD.MM.YYYY"г."')
							 WHEN S.ID_CWS_CONTRACT IS NOT NULL THEN                                                            
 								  'холодного водоснабжения № '||s.CWS_CTR_NUMBER||' от '||TO_CHAR(s.CWS_CTR_DATE,'dd.mm.yyyy"г."')
							 WHEN S.ID_SEW_CONTRACT IS NOT NULL THEN                                                                
										'водоотведения № ' || S.SEW_CTR_NUMBER || ' от ' ||TO_CHAR(S.SEW_CTR_DATE, 'dd.mm.yyyy"г."')		
						 END "договоры",
						 CASE 
							 WHEN S.ID_CWS_CONTRACT IS NOT NULL AND S.ID_SEW_CONTRACT IS NOT NULL THEN
								 'холодного водоснабжения и водоотведения'
							 WHEN S.ID_CWS_CONTRACT IS NOT NULL THEN                                                            
 								  'холодного водоснабжения'
							 ELSE 'водоотведения'
						 END "услуга",         
 						 DECODE(F.TYPE_CODE,'PT_SHUTDOWN_ALL','true','false') "если_хвс",
 						 DECODE(F.TYPE_CODE,'PT_SHUTDOWN_ALL','false','true') "если_во",
						 DECODE(F.TYPE_CODE,'PT_SHUTDOWN_ALL','об ограничении (прекращении) холодного водоснабжения','о прекращении водоотведения') "тип_уведомления",
 						 DECODE(F.TYPE_CODE,'PT_SHUTDOWN_ALL','ограничении','прекращении') "мера",
						 TO_CHAR(s.DEBT_DATE,'DD.MM.YYYY"г."') "дата_долга",
						 to_char(TRUNC(S.DEBT_CREATED),'FM999G999G999G999G') "долг",
						 LAWSUP.PKG_FMT_RU.NUMBER_IN_WORDS(TRUNC(S.DEBT_CREATED)) "долг_прописью",
						 TO_CHAR(MOD(s.DEBT_CREATED,1)*100) "копейки",      
						 PKG_PREF.F$C2('ORG_MAIN_FIO') "директор",
						 (SELECT INITCAP(L.LAST_NAME||' '||L.FIRST_NAME||' '||L.SECOND_NAME)  
						  FROM T_USER_LIST L WHERE L.ID = F$_USR_ID) "исполнитель",
						 (SELECT l.PHONE FROM T_USER_LIST L WHERE L.ID = F$_USR_ID) "телефон",
						 PKG_CLI_SHUTDOWN.F_GET_OBJECT_LIST(s.ID_WORK) "адрес_объектов",
						 TO_CHAR(SYSDATE,'"«"DD"»"')||' '||LOWER(PKG_UTILS.F_GET_NAME_MONTHS(SYSDATE,'R'))||' '||TO_CHAR(SYSDATE,'YYYY" г."') "текущая_дата"
			FROM V_CLI_SHUTDOWN S,
			     V_FOLDERS F
			WHERE S.ID_WORK = P_PAR_01
			AND   F.ID_FOLDER = S.ID_FOLDER;
			
    L_XML := PKG_CONTROLLER.F_DOCX_XML(P_SQL => L_SQL,P_REP_CODE => 'RESTRICTION');    		
    L_REP.P_BLOB := PKG_CONTROLLER.F_GET_REPORT_XML(L_XML.getClobVal());
    L_REP.P_FILE_NAME := 'Уведомление на ограничение';
		
		RETURN l_rep;
	END;										
	
end PKG_DOCX_017;
/

