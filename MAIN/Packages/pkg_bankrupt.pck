i�?create or replace package lawmain.PKG_BANKRUPT is

  -- Author  : EUGEN
  -- Created : 03.05.2019 10:51:07
  -- Purpose : Пакет для работы с банкротством
  
	PROCEDURE P_CREATE_BANKRUPT(P_CASES WWV_FLOW_GLOBAL.vc_arr2
		                         ,P_ID_CLIENT NUMBER
														 ,P_TYPE      VARCHAR2);
	
	PROCEDURE P_CHANGE_STATUS(P_ID_BANKRUPT NUMBER,
		                        P_CODE_STATUS VARCHAR2);  
														
	PROCEDURE P_UPDATE_BANKRUPT(P_ID_BANKRUPT NUMBER,
		                          P_CASES WWV_FLOW_GLOBAL.vc_arr2);													
  
	
	PROCEDURE P_CREATE_COURT_EXEC(P_ID_BANKRUPT NUMBER,
		                            P_DATE_EXEC DATE,
																P_TYPE_EXEC VARCHAR2,
																P_NUM_EXEC  VARCHAR2,
																P_ID_JUDGE  NUMBER DEFAULT NULL,
																P_ID_COURT  NUMBER);
	
		
  PROCEDURE P_UPDATE_COURT_EXEC(P_ID_EXEC NUMBER,
		                            P_DATE_EXEC DATE,
															  P_TYPE_EXEC VARCHAR2,
                                P_NUM_EXEC  VARCHAR2,
																P_ID_JUDGE  NUMBER,
																P_ID_COURT  NUMBER);
  
	PROCEDURE P_CREATE_CLI_EXEC_STAGE(P_ID_EXEC NUMBER,
                                    P_CODE    VARCHAR2,
																		P_COMMENT VARCHAR2,
																		P_DATE    DATE,
																		P_CALEND  VARCHAR2);
	
	PROCEDURE P_UPDATE_CLI_EXEC_STAGE(P_ID_EXEC_STAGE NUMBER,
		                                P_CODE          VARCHAR2,
																		P_COMMENT       VARCHAR2,
																		P_DATE          DATE);
	
	PROCEDURE P_FINISH_COURT_EXEC(P_ID_EXEC   NUMBER,
		                            P_DATE_END  DATE,
																P_END_CODE  VARCHAR2,
																P_COMMENT   VARCHAR2 DEFAULT NULL,
																P_DEBT      NUMBER DEFAULT NULL,
																P_GOV_TOLL  NUMBER DEFAULT NULL,
																P_PENY      NUMBER DEFAULT NULL,
																P_PENY_DATE DATE DEFAULT NULL);
	
	PROCEDURE P_SEND_TO_REGS(P_ID_BANKRUPT NUMBER);											
  																																																				
																
end PKG_BANKRUPT;
/

create or replace package body lawmain.PKG_BANKRUPT is

  PROCEDURE P_CREATE_BANKRUPT(P_CASES WWV_FLOW_GLOBAL.vc_arr2
		                         ,P_ID_CLIENT NUMBER
														 ,P_TYPE      VARCHAR2)
		IS 
		L_RET NUMBER;	
	BEGIN          
		IF P_CASES.COUNT = 0 THEN
			RAISE_APPLICATION_ERROR(-20200,'Не указаны дела');
		END IF;
	
    INSERT INTO T_CLI_BANKRUPT(ID,DATE_CREATED,CREATED_BY,ID_CLIENT,CLAIM_TYPE)
		VALUES(SEQ_BANKRUPT.NEXTVAL,SYSDATE,f$_usr_id,P_ID_CLIENT,P_TYPE)
		RETURNING id INTO L_RET;
		 																												 
		FORALL I IN 1..P_CASES.COUNT 
		 INSERT INTO T_CLI_BANKRUPT_CASES(ID,ID_CASE,ID_BANKRUPT)
		 VALUES(SEQ_BANKRUPT.NEXTVAL,P_CASES(I),L_RET);													
		
		FOR I IN (SELECT BC.ID_CASE FROM T_CLI_BANKRUPT_CASES BC WHERE BC.ID_BANKRUPT = L_RET)
		LOOP		
      UPDATE T_CLI_CASES S SET S.FLG_BANKRUPT = 'Y' WHERE S.ID_CASE = I.ID_CASE;			
		END LOOP;
		 
		P_CHANGE_STATUS(L_RET,'WORK');
		PKG_EVENTS.P_CREATE_BNK_EVENT(L_RET,'CREATE'); 
	END;    
	
	PROCEDURE P_CHANGE_STATUS(P_ID_BANKRUPT NUMBER,
		                        P_CODE_STATUS VARCHAR2)
	  IS 
		L_ID NUMBER;
	BEGIN
		SELECT ST.ID_STATUS INTO L_ID FROM T_CASE_STATUS ST WHERE ST.CODE_STATUS = P_CODE_STATUS;
		
		UPDATE T_CLI_BANKRUPT C 
		   SET C.ID_STATUS = L_ID,
			     C.DATE_MODIFIED = SYSDATE,
					 C.MODIFIED_BY = F$_USR_ID
		WHERE C.ID = P_ID_BANKRUPT;
	
	EXCEPTION	
		WHEN NO_DATA_FOUND THEN
			RAISE_APPLICATION_ERROR(-20200,'Неправильно указан код статуса');
	END;
	
	PROCEDURE P_UPDATE_BANKRUPT(P_ID_BANKRUPT NUMBER,
		                          P_CASES WWV_FLOW_GLOBAL.vc_arr2)
		IS
	BEGIN
		IF P_CASES.COUNT = 0 THEN
			RAISE_APPLICATION_ERROR(-20200,'Не указаны дела');
		END IF;
		
    DELETE FROM T_CLI_BANKRUPT_CASES S WHERE S.ID_BANKRUPT = P_ID_BANKRUPT;
		FORALL I IN 1..P_CASES.COUNT 
		 INSERT INTO T_CLI_BANKRUPT_CASES(ID,ID_CASE,ID_BANKRUPT)
		 VALUES(SEQ_BANKRUPT.NEXTVAL,P_CASES(I),P_ID_BANKRUPT);												
  END;			
	  
  PROCEDURE P_CREATE_COURT_EXEC(P_ID_BANKRUPT NUMBER,
		                            P_DATE_EXEC DATE,
																P_TYPE_EXEC VARCHAR2,
																P_NUM_EXEC  VARCHAR2,
																P_ID_JUDGE  NUMBER DEFAULT NULL,
																P_ID_COURT  NUMBER)
  	IS
  BEGIN
		INSERT INTO T_CLI_BNK_COURT_EXEC(ID,ID_BANKRUPT,NUM_EXEC,TYPE_EXEC,DATE_EXEC,CREATED_BY,DATE_CREATED,ID_JUDGE,ID_COURT)
		VALUES (SEQ_COURT_EXEC.NEXTVAL,P_ID_BANKRUPT,P_NUM_EXEC,P_TYPE_EXEC,P_DATE_EXEC,F$_USR_ID,SYSDATE,P_ID_JUDGE,P_ID_COURT);
	END;	
	
		
  PROCEDURE P_UPDATE_COURT_EXEC(P_ID_EXEC NUMBER,
		                            P_DATE_EXEC DATE,
															  P_TYPE_EXEC VARCHAR2,
                                P_NUM_EXEC  VARCHAR2,
																P_ID_JUDGE  NUMBER,
																P_ID_COURT  NUMBER)
  	IS
	BEGIN
	  UPDATE T_CLI_BNK_COURT_EXEC C 
		SET C.NUM_EXEC = P_NUM_EXEC,
		    C.DATE_EXEC = P_DATE_EXEC,
				C.TYPE_EXEC = P_TYPE_EXEC,
				C.ID_JUDGE  = P_ID_JUDGE,
				C.MODIFIED_BY = F$_USR_ID,
				C.DATE_MODIFIED = SYSDATE,
				C.ID_COURT = P_ID_COURT
		WHERE C.ID = P_ID_EXEC;
	END;		
	
----------------------------------------------------------------------------------	
	
	PROCEDURE P_CREATE_CLI_EXEC_STAGE(P_ID_EXEC NUMBER,
                                    P_CODE    VARCHAR2,
																		P_COMMENT VARCHAR2,
																		P_DATE    DATE,
																		P_CALEND  VARCHAR2)
		IS                            
		L_ID_STAGE   NUMBER;     
		L_ID         NUMBER;   
		L_ID_CAL     NUMBER;    
		L_STAGE_NAME VARCHAR2(200); 
		L_NUM_EXEC   VARCHAR2(100);
	BEGIN               
	   SELECT ST.ID_STAGE, ST.NAME_STAGE INTO L_ID_STAGE, L_STAGE_NAME
		 FROM T_COURT_STAGES ST WHERE ST.CODE_STAGE = P_CODE;	
	
		 INSERT INTO T_BNK_COURT_EXEC_STAGE(ID_EXEC_STAGE,ID_STAGE,ID_EXEC,COMMENTARY
		                                   ,CREATED_BY,DATE_CREATED,DATE_STAGE)
		 VALUES(SEQ_CLI_STAGE.NEXTVAL, L_ID_STAGE,P_ID_EXEC,P_COMMENT,F$_USR_ID,SYSDATE, P_DATE)
		 RETURNING ID_EXEC_STAGE INTO L_ID;                   
		 
	 	 IF P_CALEND = 'Y' THEN						      
				SELECT E.NUM_EXEC 
				INTO   L_NUM_EXEC
				FROM T_CLI_BNK_COURT_EXEC E
				WHERE E.ID = P_ID_EXEC ;
		      
				L_ID_CAL := PKG_CALENDAR.F_CREATE_EVENT(P_EVENT_NAME => L_STAGE_NAME
																							 ,P_START_DATE => TO_DATE(P_DATE,'DD.MM.YYYY HH24:MI:SS')
																							 ,P_END_DATE   => TO_DATE(P_DATE||' 23:59:59','DD.MM.YYYY HH24:MI:SS')
																							 ,P_COMMENTARY => 'По делу '||L_NUM_EXEC||' (банкротство)'
																							 ,P_NOTIF      => 'Y');       
																							                          
				UPDATE T_BNK_COURT_EXEC_STAGE B SET B.ID_CALEND = L_ID_CAL WHERE B.ID_EXEC_STAGE = L_ID;
		 END IF;
		 
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			RAISE_APPLICATION_ERROR(-20200,'Не правильно указан код события. Обратитесь к администратору системы');			 
	END;								

----------------------------------------------------------------------------------------------------------------
	
	PROCEDURE P_UPDATE_CLI_EXEC_STAGE(P_ID_EXEC_STAGE NUMBER,
		                                P_CODE          VARCHAR2,
																		P_COMMENT       VARCHAR2,
																		P_DATE          DATE)
		IS
		L_ID_STAGE NUMBER;
	BEGIN                                                                                       
	   SELECT ST.ID_STAGE INTO L_ID_STAGE FROM T_COURT_STAGES ST WHERE ST.CODE_STAGE = P_CODE;	
		 
		 UPDATE T_BNK_COURT_EXEC_STAGE ST
		    SET ST.COMMENTARY = P_COMMENT,
				    ST.ID_STAGE   = L_ID_STAGE,
						ST.DATE_STAGE = P_DATE
		  WHERE ST.ID_EXEC_STAGE = P_ID_EXEC_STAGE;
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			RAISE_APPLICATION_ERROR(-20200,'Не правильно указан код события. Обратитесь к администратору системы');		
	END;  											

---------------------------------------------------------------------------------------------------------------

  PROCEDURE P_FINISH_COURT_EXEC(P_ID_EXEC   NUMBER,
		                            P_DATE_END  DATE,
																P_END_CODE  VARCHAR2,
																P_COMMENT   VARCHAR2 DEFAULT NULL,
																P_DEBT      NUMBER DEFAULT NULL,
																P_GOV_TOLL  NUMBER DEFAULT NULL,
																P_PENY      NUMBER DEFAULT NULL,
																P_PENY_DATE DATE DEFAULT NULL)    
  	IS                       
	BEGIN
		UPDATE T_CLI_BNK_COURT_EXEC E
		   SET E.DATE_END  = P_DATE_END,
			     E.END_CODE  = P_END_CODE,
					 E.EXEC_COMM = P_COMMENT,
					 E.COURT_DEBT= P_DEBT,
					 E.COURT_PENY_DATE = P_PENY_DATE,
					 E.COURT_PENY = P_PENY,
					 E.COURT_TOLL = P_GOV_TOLL,
					 E.FLG_ACTIVE = 'N'
	 	 WHERE E.ID = P_ID_EXEC;		 		 
		 
	END;	
	
----------------------------------------------------------------------------------------------------------------
  
  PROCEDURE P_SEND_TO_REGS(P_ID_BANKRUPT NUMBER)
		IS       
		L_CASES VARCHAR2(4000);
	BEGIN                
		FOR J IN (SELECT S.CASE_NAME FROM T_CLI_BANKRUPT_CASES BC, V_CLI_CASES S 
		          WHERE BC.ID_BANKRUPT = P_ID_BANKRUPT AND BC.ID_CASE =  S.ID_CASE
		          AND   S.CODE_STATUS IN ('LEVY_CLAIM_DEPFIN','LEVY_CLAIM_UFK','LEVY_CLAIM_BANK','LEVY_CLAIM_ROSP'))
		LOOP
			L_CASES := L_CASES||j.case_name||',';
		END LOOP;                            
		
		IF L_CASES IS NOT NULL THEN
			RAISE_APPLICATION_ERROR(-20200,'По делам '||rtrim(l_cases,',')||' идет исполнительное производство.'||
			                       ' Завершите производство перед передачей дел в реестр кредиторов');
		END IF;

		 FOR I IN (SELECT S.ID_CASE 
			         FROM T_CLI_BANKRUPT_CASES S,
							      T_CLI_CASES CC,
										T_CASE_STATUS ST 
							 WHERE S.ID_BANKRUPT = P_ID_BANKRUPT
							 AND   S.ID_CASE = CC.ID_CASE
							 AND   CC.ID_STATUS = ST.ID_STATUS
							 AND   ST.CODE_STATUS != 'CLOSE')
			LOOP
				UPDATE T_CLI_CASES S SET S.DEP = 'BANKRUPT' WHERE S.ID_CASE = I.ID_CASE;
				UPDATE T_CLI_DEBTS D SET D.DEPT = 'BANKRUPT' WHERE D.ID_WORK = I.ID_CASE;
				PKG_EVENTS.P_CREATE_CLI_EVENT(I.ID_CASE,'REG_CRED');
				PKG_CLI_CASES.P_CHANGE_STATUS(I.ID_CASE,'REG_CRED');
			END LOOP;		
			
			PKG_BANKRUPT.P_CHANGE_STATUS(P_ID_BANKRUPT,'REG_CRED');
			PKG_EVENTS.P_CREATE_BNK_EVENT(P_ID_BANKRUPT,'REG_CRED');		 
	END;		                 
																
end PKG_BANKRUPT;
/

