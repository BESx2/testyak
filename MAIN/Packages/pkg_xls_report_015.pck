i�?create or replace package lawmain.PKG_XLS_REPORT_015 is

  -- Author  : BES
  -- Created : 17.02.2017
  -- Purpose : Оплата отработанной дебиторки
	-- Code:     DEBT_STRUCTURE
	
  
  FUNCTION RUN_REPORT(P_PAR_01 VARCHAR2 DEFAULT NULL
											,P_PAR_02 VARCHAR2 DEFAULT NULL
											,P_PAR_03 VARCHAR2 DEFAULT NULL
											,P_PAR_04 VARCHAR2 DEFAULT NULL
											,P_PAR_05 VARCHAR2 DEFAULT NULL
											,P_PAR_06 VARCHAR2 DEFAULT NULL
											,P_PAR_07 VARCHAR2 DEFAULT NULL
											,P_PAR_08 VARCHAR2 DEFAULT NULL
											,P_PAR_09 VARCHAR2 DEFAULT NULL
											,P_PAR_10 VARCHAR2 DEFAULT NULL) RETURN LAWSUP.PKG_FILES.REC_DOC;	  

end PKG_XLS_REPORT_015;
/

create or replace package body lawmain.PKG_XLS_REPORT_015 is

	TYPE REC_DOC IS RECORD(	P_BLOB BLOB DEFAULT NULL );

	EXCEL_DOC REC_DOC := NULL;
------------------------------------------------------------------------

	PROCEDURE p$s( ps_value IN CLOB )  -- отправляет накопленное значение ps_value в буфер и записывает
	IS
		BUFFER    	RAW(32767);
    l_offset    NUMBER := 1;
    BUF_SIZE    NUMBER := 8000;
    BUF_VAR    VARCHAR2(32767);
  BEGIN
    LOOP
    EXIT WHEN L_OFFSET > DBMS_LOB.getlength(PS_VALUE);
    BUF_VAR := DBMS_LOB.substr(PS_VALUE,BUF_SIZE,L_OFFSET);
		BUFFER := UTL_RAW.cast_to_raw( CONVERT( BUF_VAR, 'UTF8'/*'CL8MSWIN1251'*/ ) );
		DBMS_LOB.WRITEAPPEND( EXCEL_DOC.P_BLOB, UTL_RAW.LENGTH( BUFFER ), BUFFER );
    L_OFFSET := L_OFFSET + BUF_SIZE;
    END LOOP;
  END;
------------------------------------------------------------------------
	PROCEDURE P_ADD_DATA(P_DATE_ON DATE
		                  ,P_DATE_START DATE
											,P_DATE_END DATE										
											,P_GROUP VARCHAR2) IS 
	  L_XML CLOB;
		L_CNT NUMBER := 0;
		L_FMT VARCHAR2(50) := '9999999999999D00';
		L_NLS VARCHAR2(50) := 'NLS_NUMERIC_CHARACTERS=''. ''';		
		L_DATE_START DATE;
    L_DATE_END   DATE;     		
		--ИТОГОВЫЕ СУММЫ В РАЗРЕЗЕ РАБОТЫ	
		L_TOT_DEBT     NUMBER := 0;		
		L_NEW_CTR  BOOLEAN DEFAULT TRUE;
		L_NEW_GROUP BOOLEAN DEFAULT TRUE;
		
	BEGIN       
    L_DATE_START := NVL(P_DATE_START,DATE '2000-01-01');
		L_DATE_END   := NVL(P_DATE_END,SYSDATE)+1-(INTERVAL '1' SECOND);  
		
		L_XML :=
			'<?xml version="1.0"?>
<?mso-application progid="Excel.Sheet"?>
<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:o="urn:schemas-microsoft-com:office:office"
 xmlns:x="urn:schemas-microsoft-com:office:excel"
 xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:html="http://www.w3.org/TR/REC-html40">
 <DocumentProperties xmlns="urn:schemas-microsoft-com:office:office">
  <Author></Author>
  <LastAuthor></LastAuthor>
  <LastPrinted></LastPrinted>
  <Created></Created>
  <Company></Company>
  <Version>16.00</Version>
 </DocumentProperties>
 <OfficeDocumentSettings xmlns="urn:schemas-microsoft-com:office:office">
  <AllowPNG/>
 </OfficeDocumentSettings>
 <ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel">
  <WindowHeight>10305</WindowHeight>
  <WindowWidth>23790</WindowWidth>
  <WindowTopX>0</WindowTopX>
  <WindowTopY>0</WindowTopY>
  <RefModeR1C1/>
  <ProtectStructure>False</ProtectStructure>
  <ProtectWindows>False</ProtectWindows>
 </ExcelWorkbook>
 <Styles>
  <Style ss:ID="Default" ss:Name="Normal">
   <Alignment ss:Vertical="Bottom"/>
   <Borders/>
   <Font ss:FontName="Calibri" x:CharSet="204" x:Family="Swiss" ss:Size="11"
    ss:Color="#000000"/>
   <Interior/>
   <NumberFormat/>
   <Protection/>
  </Style>
  <Style ss:ID="s69">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Interior ss:Color="#C6E0B4" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s80">
   <Alignment ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>     
	  <NumberFormat ss:Format="Short Date"/>
  </Style>
  <Style ss:ID="s81">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
  </Style>
  <Style ss:ID="s92">
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Calibri" x:CharSet="204" x:Family="Swiss" ss:Size="11"
    ss:Color="#000000" ss:Bold="1"/>
   <Interior ss:Color="#C6E0B4" ss:Pattern="Solid"/>
   <NumberFormat ss:Format="Standard"/>
  </Style>
  <Style ss:ID="s93">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Calibri" x:CharSet="204" x:Family="Swiss" ss:Size="11"
    ss:Color="#000000" ss:Bold="1"/>
   <Interior ss:Color="#C6E0B4" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s96">
   <Alignment ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Interior/>
   <NumberFormat ss:Format="Standard"/>
  </Style>
  <Style ss:ID="s97">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Calibri" x:CharSet="204" x:Family="Swiss" ss:Size="11"
    ss:Color="#000000" ss:Bold="1"/>
   <Interior ss:Color="#C6E0B4" ss:Pattern="Solid"/>
  </Style>
 </Styles>
 <Worksheet ss:Name="Лист1"> 
  <Table>
   <Column ss:AutoFitWidth="0" ss:Width="160"/>    
   <Column ss:AutoFitWidth="0" ss:Width="358.5"/>
   <Column ss:AutoFitWidth="0" ss:Width="100"/>
   <Column ss:AutoFitWidth="0" ss:Width="250"/>
   <Column ss:AutoFitWidth="0" ss:Width="100"/>
   <Column ss:AutoFitWidth="0" ss:Width="140"/>
   <Column ss:AutoFitWidth="0" ss:Width="100"/>
   <Row>
    <Cell ss:StyleID="s69"><Data ss:Type="String">Группа</Data><NamedCell
      ss:Name="Print_Area"/></Cell>                                       
    <Cell ss:StyleID="s69"><Data ss:Type="String">Контрагент</Data><NamedCell
      ss:Name="Print_Area"/></Cell>	
    <Cell ss:StyleID="s69"><Data ss:Type="String">Договор</Data><NamedCell
      ss:Name="Print_Area"/></Cell>
    <Cell ss:StyleID="s69"><Data ss:Type="String">Стадия</Data><NamedCell
      ss:Name="Print_Area"/></Cell>
		<Cell ss:StyleID="s69"><Data ss:Type="String">Дата установки статуса</Data><NamedCell
      ss:Name="Print_Area"/></Cell>	
    <Cell ss:StyleID="s69"><Data ss:Type="String">Период</Data><NamedCell
      ss:Name="Print_Area"/></Cell>
    <Cell ss:StyleID="s69"><Data ss:Type="String">Задолженность</Data><NamedCell
      ss:Name="Print_Area"/></Cell>
   </Row>';
   p$s(L_XML);		                                                                  
		    
		 FOR I IN (SELECT CG.GROUP_NAME
			               ,CLI.CLI_NAME
										 ,CT.CTR_NUMBER
										 ,T2.WORK_NAME                                        
										 ,TO_CHAR(T2.WORK_DATE,'YYYY-MM-DD"T00:00:00.000"') WORK_DATE
										 ,T2.PERIOD
										 ,T2.DEBT 
								FROM (SELECT  T.ID_CONTRACT, 
															T.WORK_NAME,    
															T.WORK_DATE,
															TO_CHAR(TRUNC(MIN(T.DOC_DATE)),'DD.MM.YYYY')||' - '
															||TO_CHAR(LAST_DAY(MAX(T.DOC_DATE)),'DD.MM.YYYY') PERIOD,
															SUM(T.DEBT) DEBT
											FROM    (SELECT FD.ID_CONTRACT                 
																,NVL((SELECT EV.Event_Desc FROM V_CLI_EVENTS EV WHERE EV.id_event = EVT.ID_EVENT),'Без статуса') WORK_NAME
																,(SELECT EV.DATE_EVENT FROM V_CLI_EVENTS EV WHERE EV.id_event = EVT.ID_EVENT) WORK_DATE  
																 ,FD.DOC_DATE
																,(SELECT SUM(DECODE(FDC.OPER_TYPE,'Приход',FDC.AMOUNT,'Расход',-FDC.AMOUNT))
																 FROM T_CLI_FINDOC_CONS FDC
																 WHERE FDC.EXT_ID_TO = FD.EXT_ID
																 AND   FDC.DATE_CON <= P_DATE_ON)  DEBT 																
													FROM T_CLI_FINDOCS FD                      													
													LEFT OUTER JOIN (SELECT D.ID_DOC, MAX(EV.id_event) ID_EVENT FROM  T_CLI_DEBTS D, V_CLI_EVENTS EV
																					 WHERE  EV.ID_WORK = D.ID_WORK AND EV.TYPE_EVENT = 'SYSTEM' AND EV.DATE_EVENT <= P_DATE_ON
																					 GROUP BY D.ID_DOC) EVT
														    ON (FD.ID_DOC = EVT.ID_DOC)																			
												  WHERE FD.DOC_PERIOD  BETWEEN L_DATE_START AND L_DATE_END )T  
									    WHERE   T.DEBT > 0		
											GROUP BY T.ID_CONTRACT, T.WORK_NAME, T.WORK_DATE) T2                          
								INNER JOIN T_CONTRACTS CT ON CT.ID_CONTRACT = T2.ID_CONTRACT
								INNER JOIN T_CLIENTS CLI  ON CT.ID_CLIENT = CLI.ID_CLIENT
								INNER JOIN T_CUSTOMER_GROUP CG ON CG.GROUP_NAME = CT.CUST_GROUP
								AND (CG.GROUP_NAME IN (SELECT COLUMN_VALUE FROM TABLE(APEX_STRING.split(P_GROUP,':'))) OR P_GROUP IS NULL)
								AND   T2.DEBT > 0
								ORDER BY CG.GROUP_NAME, T2.WORK_NAME, T2.DEBT DESC)
		 LOOP      				  
				L_XML := '<Row>'||CHR(13);
				L_XML := L_XML || '<Cell ss:StyleID="s80"><Data ss:Type="String">'||I.GROUP_NAME||'</Data><NamedCell ss:Name="Print_Area"/></Cell>'||CHR(13);
				L_XML := L_XML || '<Cell ss:StyleID="s81"><Data ss:Type="String">'||I.CLI_NAME||'</Data><NamedCell ss:Name="Print_Area"/></Cell>'||CHR(13);
				L_XML := L_XML || '<Cell ss:StyleID="s81"><Data ss:Type="String">'||i.Ctr_Number||'</Data><NamedCell ss:Name="Print_Area"/></Cell>'||CHR(13);
				L_XML := L_XML || '<Cell ss:StyleID="s80"><Data ss:Type="String">'||I.WORK_NAME||'</Data><NamedCell ss:Name="Print_Area"/></Cell>'||CHR(13);
				IF I.WORK_DATE IS NULL THEN
				   L_XML := L_XML || '<Cell ss:StyleID="s80"><NamedCell ss:Name="Print_Area"/></Cell>'||CHR(13);
				ELSE
				   L_XML := L_XML || '<Cell ss:StyleID="s80"><Data ss:Type="DateTime">'||I.WORK_DATE||'</Data><NamedCell ss:Name="Print_Area"/></Cell>'||CHR(13);
			  END IF;
				L_XML := L_XML || '<Cell ss:StyleID="s80"><Data ss:Type="String">'||I.PERIOD||'</Data><NamedCell ss:Name="Print_Area"/></Cell>'||CHR(13);
				L_XML := L_XML || '<Cell ss:StyleID="s96"><Data ss:Type="Number">'||TO_CHAR(i.Debt,L_FMT,L_NLS)||'</Data><NamedCell ss:Name="Print_Area"/></Cell>'||CHR(13);
				L_XML := L_XML || '</Row>'||CHR(13);
				P$S(L_XML);                                                                  
	 END LOOP;					
	 
	 
	 L_XML := '</Table>
  <WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">
   <PageSetup>
    <Header x:Margin="0.31496062992125984"/>
    <Footer x:Margin="0.31496062992125984"/>
    <PageMargins x:Bottom="0.74803149606299213" x:Left="0.70866141732283472"
     x:Right="0.70866141732283472" x:Top="0.74803149606299213"/>
   </PageSetup>
   <FitToPage/>
   <Print>
    <FitHeight>0</FitHeight>
    <ValidPrinterInfo/>
    <PaperSizeIndex>9</PaperSizeIndex>
    <Scale>38</Scale>
    <HorizontalResolution>-3</HorizontalResolution>
    <VerticalResolution>0</VerticalResolution>
   </Print>
   <PageBreakZoom>60</PageBreakZoom>
   <Selected/>
   <Panes>
    <Pane>
     <Number>3</Number>
     <ActiveRow>10</ActiveRow>
     <ActiveCol>3</ActiveCol>
    </Pane>
   </Panes>
   <ProtectObjects>False</ProtectObjects>
   <ProtectScenarios>False</ProtectScenarios>
  </WorksheetOptions>
 </Worksheet>
</Workbook>';

		p$s( L_XML );
	
	
	END P_ADD_DATA;

---------------------------------------------------------------------------------------------------------------------------------

  FUNCTION RUN_REPORT(P_PAR_01 VARCHAR2 DEFAULT NULL
											,P_PAR_02 VARCHAR2 DEFAULT NULL
											,P_PAR_03 VARCHAR2 DEFAULT NULL
											,P_PAR_04 VARCHAR2 DEFAULT NULL
											,P_PAR_05 VARCHAR2 DEFAULT NULL
											,P_PAR_06 VARCHAR2 DEFAULT NULL
											,P_PAR_07 VARCHAR2 DEFAULT NULL
											,P_PAR_08 VARCHAR2 DEFAULT NULL
											,P_PAR_09 VARCHAR2 DEFAULT NULL
											,P_PAR_10 VARCHAR2 DEFAULT NULL) RETURN LAWSUP.PKG_FILES.REC_DOC
	IS
	l_BLOB LAWSUP.PKG_FILES.REC_DOC;
  BEGIN

	  EXCEL_DOC := NULL;
		DBMS_LOB.CREATETEMPORARY( EXCEL_DOC.P_BLOB, TRUE );
    DBMS_LOB.OPEN( EXCEL_DOC.P_BLOB, DBMS_LOB.LOB_READWRITE );

		P_ADD_DATA(P_DATE_ON =>NVL(TO_DATE(P_PAR_01,'DD.MM.YYYY'),SYSDATE)
		          ,P_DATE_START => TO_DATE(P_PAR_02,'DD.MM.YYYY')
							,P_DATE_END => TO_DATE(P_PAR_03,'DD.MM.YYYY')					
							,P_GROUP => P_PAR_07);


		DBMS_LOB.CLOSE(EXCEL_DOC.P_BLOB);

		    l_BLOB.P_BLOB := EXCEL_DOC.P_BLOB;
	      l_BLOB.P_FILE_NAME := 'Структуру дебиторки';
				RETURN L_BLOB;

  END RUN_REPORT;

end PKG_XLS_REPORT_015;
/

