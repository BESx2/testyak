i�?create or replace package lawmain.PKG_CLIENTS is

  -- Author  : EUGEN
  -- Created : 14.11.2016 12:28:56
  -- Purpose : Пакет для работы с юр. лицами
  
  FUNCTION F_GET_DEBT(P_ID_CLIENT VARCHAR2) RETURN NUMBER;
	
	
	
	PROCEDURE P_GET_FULL_CLI_INFO(P_ID_CLIENT VARCHAR2);      
		

	PROCEDURE P_UPDATE_SUM_DEBT (P_ID_CLIENT VARCHAR2);   
	
		--Процедура создаёт выездное мероприятие
	PROCEDURE P_CREATE_OFFSITE_CLI_EVENT(P_ID_CLIENT 	VARCHAR2,
													 						 P_EVENT      VARCHAR2,
																			 P_COMMENTARY VARCHAR2 DEFAULT NULL);
																			 
  FUNCTION F_GET_ADDRESS(P_ID_CLIENT NUMBER) RETURN VARCHAR2;       
	
	FUNCTION F_GET_EMAIL(P_ID_CLIENT NUMBER) RETURN VARCHAR2;
	
	FUNCTION F_GET_ADDRESS_COURT(P_ID_CLIENT NUMBER) RETURN VARCHAR2;
	
		FUNCTION F_GET_PHONE(P_ID_CLIENT NUMBER) RETURN VARCHAR2;
end PKG_CLIENTS;
/

create or replace package body lawmain.PKG_CLIENTS is

  FUNCTION F_GET_DEBT(P_ID_CLIENT VARCHAR2) RETURN NUMBER
		IS
		L_RET NUMBER;
	BEGIN
			 SELECT SUM(PKG_CONTRACTS.F_GET_DEBT(P_ID_CONTRACT => ct.id_contract))
			 INTO L_RET
			 FROM T_CONTRACTS ct
			 WHERE ct.id_client = P_ID_CLIENT;
	 IF L_RET IS NULL THEN
		 RETURN 0;
	 END IF;	 
   RETURN L_RET;                      			
	END;  



	
	PROCEDURE P_UPDATE_SUM_DEBT(P_ID_CLIENT VARCHAR2) IS
		L_DEBT NUMBER;    
		L_CURATOR_PRE NUMBER;    
		L_USER        NUMBER;  
		L_DEBT_PRE    NUMBER;    
		L_NOTIF       NUMBER;
	BEGIN

	  UPDATE T_CONTRACTS CT 
		   SET CT.SUM_DEBT = (SELECT NVL(SUM(D.DEBT),0)
              						FROM   T_CLI_FINDOCS D
						              WHERE  D.ID_CONTRACT = CT.ID_CONTRACT
													AND    D.OPER_TYPE = 'Приход')	       
		    ,CT.SUM_CREDIT = (SELECT NVL(SUM(D.DEBT),0)
              						FROM   T_CLI_FINDOCS D
						              WHERE  D.ID_CONTRACT = CT.ID_CONTRACT
													AND    D.OPER_TYPE = 'Расход')
				,CT.DEBT_PRETRIAL = (SELECT SUM(d.debt) 
				                     FROM T_CLI_FINDOCS D
														 WHERE EXISTS (SELECT DISTINCT CCD.id_doc      
																					 FROM T_CLI_DEBTS CCD														      
																					 WHERE CCD.DEPT = 'PRE_TRIAL'       																					 
																					 AND   D.id_doc = CCD.ID_DOC)
														 AND D.ID_CONTRACT = CT.ID_CONTRACT
														 AND D.debt > 0)
		WHERE CT.ID_CLIENT = P_ID_CLIENT;
		
 FOR I IN (SELECT CLI.ID_CLIENT, CLI.VIP, cli.curator_pre, CLI.DEBT_PRETRIAL FROM T_CLIENTS CLI WHERE CLI.ID_CLIENT = P_ID_CLIENT)
	 LOOP
			UPDATE T_CLIENTS S
			SET    S.SUM_DEBT = (SELECT NVL(SUM(CT.SUM_DEBT),0)
													 FROM T_CONTRACTS CT
													 WHERE CT.ID_CLIENT = S.ID_CLIENT)
						,S.SUM_CREDIT = (SELECT NVL(SUM(CT.SUM_CREDIT),0)
														 FROM T_CONTRACTS CT
														 WHERE CT.ID_CLIENT = S.ID_CLIENT)
						,S.DEBT_PRETRIAL = (SELECT SUM(CT.DEBT_PRETRIAL)
																FROM T_CONTRACTS CT
																WHERE CT.ID_CLIENT = S.ID_CLIENT)
			WHERE S.ID_CLIENT = I.ID_CLIENT
			RETURN S.DEBT_PRETRIAL INTO L_DEBT;
			
			IF L_DEBT != I.DEBT_PRETRIAL AND I.VIP = 'Y'  THEN    
				L_NOTIF := PKG_NOTIF.F_CREATE_NOTIFICATION(P_HEADING => 'VIP - изменение долга'
				                               ,P_TEXT => 'По VIP абоненту изменилась сумма задолженности'
																			 ,P_USER => 19512
																			 ,P_TYPE => 'PRE_TRIAL_CLI'
																			 ,P_PARAMS => I.Id_Client);		
				
				L_NOTIF := PKG_NOTIF.F_CREATE_NOTIFICATION(P_HEADING => 'VIP - изменение долга'
				                               ,P_TEXT => 'По VIP абоненту изменилась сумма задолженности'
																			 ,P_USER => 19508
																			 ,P_TYPE => 'PRE_TRIAL_CLI'
																			 ,P_PARAMS => I.Id_Client);		
																			 																			 
																			 		
				IF I.CURATOR_PRE IS NOT NULL THEN															 
				 L_NOTIF := PKG_NOTIF.F_CREATE_NOTIFICATION(P_HEADING => 'VIP - изменение долга'
				                               ,P_TEXT => 'По VIP абоненту изменилась сумма задолженности'
																			 ,P_USER => I.CURATOR_PRE
																			 ,P_TYPE => 'PRE_TRIAL_CLI'
																			 ,P_PARAMS => I.Id_Client);				
				 
				END IF; 											 
		  END IF;
	 END LOOP;														


	 UPDATE T_CLI_DEBTS D SET D.DEBT_CURRENT = (SELECT FD.DEBT FROM T_CLI_FINDOCS FD WHERE FD.ID_DOC = D.ID_DOC)
	 WHERE D.ID_CLIENT = P_ID_CLIENT;
   
	 FOR I IN (SELECT W.ID_WORK, W.TYPE_WORK, W.ID_CLIENT, W.CURRENT_DEBT, W.CREATED_BY FROM T_CLI_DEBT_WORK W
		         WHERE W.ID_CLIENT = P_ID_CLIENT) 
	 LOOP
		 UPDATE T_CLI_DEBT_WORK W 
			  SET W.CURRENT_DEBT = (SELECT SUM(D.DEBT_CURRENT) FROM T_CLI_DEBTS D WHERE D.ID_WORK = W.ID_WORK)
			WHERE W.ID_WORK = I.ID_WORK
			RETURNING W.CURRENT_DEBT INTO L_DEBT;
			 																
		 IF I.TYPE_WORK IN ('GARANT') AND L_DEBT != I.CURRENT_DEBT THEN 
			  
		   SELECT CLI.CURATOR_PRE INTO L_USER FROM T_CLIENTS CLI WHERE CLI.ID_CLIENT = I.ID_CLIENT;
				
			 IF L_USER IS NOT NULL THEN
			  L_NOTIF := PKG_NOTIF.F_CREATE_NOTIFICATION(P_HEADING => 'Оплата по гарантийному письму'
				                               ,P_TEXT => 'Изменилась сумма долга по гарантийному письму'
																			 ,P_USER => L_USER
																			 ,P_TYPE => 'PRE_TRIAL_CLI'
																			 ,P_PARAMS => I.Id_Client);
			 END IF;	
			  L_NOTIF :=  PKG_NOTIF.F_CREATE_NOTIFICATION(P_HEADING => 'Оплата по соглашению'
				                               ,P_TEXT => 'Изменилась сумма долга по гарантийному письму'
																			 ,P_USER => 19512
																			 ,P_TYPE => 'PRE_TRIAL_CLI'
																			 ,P_PARAMS => I.Id_Client);		
																			 
			  L_NOTIF :=  PKG_NOTIF.F_CREATE_NOTIFICATION(P_HEADING => 'Оплата по соглашению'
				                               ,P_TEXT => 'Изменилась сумма долга по гарантийному письму'
																			 ,P_USER => 19508
																			 ,P_TYPE => 'PRE_TRIAL_CLI'
																			 ,P_PARAMS => I.Id_Client);																	 				 		 
		 END IF;
		
	 	 IF I.TYPE_WORK IN ('RESTRUCT') AND L_DEBT != I.CURRENT_DEBT THEN        			 
			 IF I.CREATED_BY IS NOT NULL THEN
			  L_NOTIF := PKG_NOTIF.F_CREATE_NOTIFICATION(P_HEADING => 'Оплата по соглашению'
				                               ,P_TEXT => 'Изменилась сумма долга по соглашению о реструктуризации'
																			 ,P_USER => I.CREATED_BY
																			 ,P_TYPE => 'RESTRUCT'
																			 ,P_PARAMS => I.ID_WORK);
			 END IF;				 
		 END IF;
		 
	 END LOOP;

	 
	 UPDATE T_CLI_DEBT_WORK W SET W.CURRENT_DEBT = (SELECT SUM(D.DEBT_CURRENT) FROM T_CLI_DEBTS D WHERE D.ID_WORK = W.ID_WORK)
	 WHERE W.ID_CLIENT = P_ID_CLIENT;
   
	 FOR I IN (SELECT S.ID_CASE, S.CURATOR_COURT, S.SUM_DEBT, S.CASE_NAME, S.DEP
		         FROM T_CLI_CASES S 
						 WHERE  s.id_client = P_ID_CLIENT)
		 LOOP
	     UPDATE T_CLI_CASES S SET S.SUM_DEBT = (SELECT W.CURRENT_DEBT FROM T_CLI_DEBT_WORK W WHERE W.ID_WORK = S.ID_CASE)
			 WHERE S.ID_CASE = I.ID_CASE RETURNING S.SUM_DEBT INTO L_DEBT;
			 
			 IF I.DEP = 'COURT' AND L_DEBT != I.SUM_DEBT THEN   
				IF I.CURATOR_COURT IS NOT NULL THEN															 
				 L_NOTIF := PKG_NOTIF.F_CREATE_NOTIFICATION(P_HEADING => 'Оплата по делу'
				                               ,P_TEXT => 'По делу '||i.case_name||' изменилась сумма долга'
																			 ,P_USER => I.CURATOR_COURT
																			 ,P_TYPE => 'COURT_CASE'
																			 ,P_PARAMS => I.ID_CASE);
				END IF;															 

																			 															 
			 ELSIF I.DEP = 'FSSP' AND L_DEBT != I.SUM_DEBT THEN
				 
						 L_NOTIF := PKG_NOTIF.F_CREATE_NOTIFICATION(P_HEADING => 'Оплата по делу'
																				 ,P_TEXT => 'По делу '||i.case_name||' изменилась сумма долга'
																				 ,P_USER => 310121
																				 ,P_TYPE => 'FSSP_CASE'
																				 ,P_PARAMS => I.ID_CASE);			
			 END IF;
     END LOOP;  
		 
	 UPDATE T_CLI_PRETENSION P SET P.SUM_DEBT =	(SELECT W.CURRENT_DEBT FROM T_CLI_DEBT_WORK W WHERE W.ID_WORK = P.ID_WORK)
	 WHERE P.ID_WORK IN (SELECT W.ID_WORK FROM T_CLI_DEBT_WORK W WHERE W.ID_CLIENT = P_ID_CLIENT);

	 UPDATE T_CLI_SHUTDOWN S SET S.SUM_DEBT = (SELECT W.CURRENT_DEBT FROM T_CLI_DEBT_WORK W WHERE W.ID_WORK = S.ID_WORK)
 	 WHERE S.ID_WORK IN (SELECT W.ID_WORK FROM T_CLI_DEBT_WORK W WHERE W.ID_CLIENT = P_ID_CLIENT);

	 
	 
	END;	  
	
	
	PROCEDURE P_GET_FULL_CLI_INFO(P_ID_CLIENT VARCHAR2)
		 IS      	
	BEGIN   																								
		 --Обновляем информациб о клиенте.
		 PKG_BILLING.P_GET_CLIENTS(P_CLIENT_ID => P_ID_CLIENT);
     PKG_BILLING.P_GET_CONTRACTS(P_CLIENT_ID => P_ID_CLIENT);                                                                             		 
		 FOR I IN (SELECT CT.ID_CONTRACT FROM T_CONTRACTS CT WHERE CT.ID_CLIENT = P_ID_CLIENT)
  	 LOOP                              			 
       PKG_BILLING.P_GET_FINDOCS_CONS(P_ID_CONTRACT => I.ID_CONTRACT);
			 PKG_BILLING.P_CREATE_FINDOCS(P_ID_CONTRACT => I.ID_CONTRACT);
	   END LOOP;
     PKG_BILLING.P_GET_FINDOC_DETAILS(P_ID_CLIENT => P_ID_CLIENT);
		 P_UPDATE_SUM_DEBT(P_ID_CLIENT);			  
	END;                                   
	
	
	--Процедура создаёт выездное мероприятие
	PROCEDURE P_CREATE_OFFSITE_CLI_EVENT(P_ID_CLIENT 	VARCHAR2,
													 						 P_EVENT      VARCHAR2,
																			 P_COMMENTARY VARCHAR2 DEFAULT NULL)
	IS                             
	  L_ID NUMBER;
	BEGIN
		 --Достаём ИД мероприятия
		 SELECT ev.id_event INTO L_ID FROM T_OFF_SITE_EVENTS ev WHERE ev.code_event = P_EVENT; 
		 
		 INSERT INTO T_CLI_OFF_SITE(ID_CLI_EVENT,ID_CLIENT,COMMENTARY,CREATED_BY,DATE_CREATED,ID_EVENT)
		 VALUES(SEQ_CLI_OFFSITE.NEXTVAL,P_ID_CLIENT,P_COMMENTARY,f$_usr_id,SYSDATE,L_ID);                
		 
  EXCEPTION
			WHEN NO_DATA_FOUND THEN
					RAISE_APPLICATION_ERROR(-20200,'Неверно указан код мероприятия'); 
	END;	
	

	---------------------------------------------------------------------
	
	FUNCTION F_GET_ADDRESS(P_ID_CLIENT NUMBER) RETURN VARCHAR2
		 IS
		L_RET VARCHAR2(1000);
	BEGIN                                                       
		SELECT  CD.DET_VAL
		INTO L_RET
		FROM T_CLI_CONTACT_DETAILS CD, T_CLI_CONTACT_TYPES T
		WHERE  T.DET_TYPE = 'Адрес'     
    AND    CD.ID_CLIENT = P_ID_CLIENT
		AND    CD.DET_DESC = T.DET_DESC
		ORDER BY T.ORDER_NUM FETCH FIRST 1 ROW ONLY;                        
		
	  RETURN L_RET;  
	 
	EXCEPTION
		 WHEN NO_DATA_FOUND THEN
			 RETURN '-';
	END;   
	
	FUNCTION F_GET_ADDRESS_COURT(P_ID_CLIENT NUMBER) RETURN VARCHAR2
		 IS
		L_RET VARCHAR2(1000);
	BEGIN
	  SELECT T.DET_VAL                        
		INTO L_RET
		FROM (SELECT CD.DET_VAL
					      ,CD.ID_CLIENT
					      ,ROW_NUMBER() OVER(PARTITION BY CD.ID_CLIENT ORDER BY T.ORDER_NUM DESC) AS RN
					FROM   T_CLI_CONTACT_DETAILS CD, T_CLI_CONTACT_TYPES T
					WHERE  T.DET_TYPE = 'Адрес'     
					AND    T.DET_DESC IN ('Юридический адрес контрагента','Юридический адрес контрагента ЕГРЮЛ')
					AND    CD.ID_CLIENT = P_ID_CLIENT
					AND    CD.DET_DESC = T.DET_DESC) T
		WHERE T.RN = 1;  
		
	  RETURN L_RET;  
	 
	EXCEPTION
		 WHEN NO_DATA_FOUND THEN
			 RETURN '-';
	END;   
	
		
	FUNCTION F_GET_EMAIL(P_ID_CLIENT NUMBER) RETURN VARCHAR2
		 IS
		L_RET VARCHAR2(1000);
	BEGIN
	  SELECT T.DET_VAL                        
		INTO L_RET
		FROM (SELECT CD.DET_VAL
					      ,CD.ID_CLIENT
					      ,ROW_NUMBER() OVER(PARTITION BY CD.ID_CLIENT ORDER BY T.ORDER_NUM) AS RN
					FROM   T_CLI_CONTACT_DETAILS CD, T_CLI_CONTACT_TYPES T
					WHERE  T.DET_TYPE = 'E-Mail'     
					AND    CD.ID_CLIENT = P_ID_CLIENT
					AND    CD.DET_DESC = T.DET_DESC) T
		WHERE T.RN = 1;  
		
	  RETURN L_RET;  
	 
	EXCEPTION
		 WHEN NO_DATA_FOUND THEN
			 RETURN '-';
	END;        
	
	FUNCTION F_GET_PHONE(P_ID_CLIENT NUMBER) RETURN VARCHAR2
		IS   
		L_RET VARCHAR2(500);
	BEGIN
	  SELECT T.DET_VAL                        
		INTO L_RET
		FROM (SELECT CD.DET_VAL
					      ,CD.ID_CLIENT
					      ,ROW_NUMBER() OVER(PARTITION BY CD.ID_CLIENT ORDER BY T.ORDER_NUM) AS RN
					FROM   T_CLI_CONTACT_DETAILS CD, T_CLI_CONTACT_TYPES T
					WHERE  T.DET_TYPE = 'Телефон'     
					AND    CD.ID_CLIENT = P_ID_CLIENT
					AND    CD.DET_DESC = T.DET_DESC) T
		WHERE T.RN = 1;  
		
	  RETURN L_RET;  
	 
	EXCEPTION
		 WHEN NO_DATA_FOUND THEN
			 RETURN '-';
	END;
			
end PKG_CLIENTS;
/

