i�?create or replace package lawmain.PKG_DOC_002 is

  -- Author  : BES
  -- Created : 14.02.2019
  -- Purpose : Претензии юридическим лицам
	-- CODE    : PRETENSION_DOC  
	

	  
  FUNCTION RUN_REPORT(P_PAR_01 VARCHAR2 DEFAULT NULL
											,P_PAR_02 VARCHAR2 DEFAULT NULL
											,P_PAR_03 VARCHAR2 DEFAULT NULL
											,P_PAR_04 VARCHAR2 DEFAULT NULL
											,P_PAR_05 VARCHAR2 DEFAULT NULL
											,P_PAR_06 VARCHAR2 DEFAULT NULL
											,P_PAR_07 VARCHAR2 DEFAULT NULL
											,P_PAR_08 VARCHAR2 DEFAULT NULL
											,P_PAR_09 VARCHAR2 DEFAULT NULL
											,P_PAR_10 VARCHAR2 DEFAULT NULL) RETURN LAWSUP.PKG_FILES.REC_DOC;	  

end PKG_DOC_002;
/

create or replace package body lawmain.PKG_DOC_002 is

	TYPE REC_DOC IS RECORD(	P_BLOB BLOB DEFAULT NULL );

	RTF_DOC REC_DOC := NULL;      
	
	
	
------------------------------------------------------------------------
		
	PROCEDURE P_ADD_DATA 
		IS   
	  l_blob BLOB;		
    l_rtf    clob; 
		L_GROUP  VARCHAR2(50); 
		L_NEW_ROW CLOB;  
		L_ROW    CLOB;          
		L_TABLE  CLOB;
		L_TYPE   VARCHAR2(50);
	BEGIN	             
		L_TABLE := Q'|\trowd \irow0\irowband0\ltrrow\ts25\trqc\trgaph108\trrh438\trleft-108\trbrdrt\brdrs\brdrw10 \trbrdrl\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 \trbrdrr\brdrs\brdrw10 \trbrdrh\brdrs\brdrw10 \trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth10485\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddft3\trpaddfb3\trpaddfr3\tblrsid6370757\tbllkhdrrows\tbllkhdrcols\tbllknocolband\tblind0\tblindtype3 \clvertalc\clbrdrt\brdrs\brdrw10 \clbrdrl
\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth539\clshdrawnil \cellx436\clvertalc\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1840\clshdrawnil \cellx2274\clvertalc\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth2591\clshdrawnil \cellx4864\clvertalc\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth1909\clshdrawnil \cellx6773\clvertalc\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1936\clshdrawnil \cellx8709\clvertalc\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth1670\clshdrawnil \cellx10377\pard\plain \ltrpar
\s21\qc \li0\ri0\widctlpar\intbl\wrapdefault\aspalpha\aspnum\faauto\adjustright\rin0\lin0\pararsid6370757\yts25 \rtlch\fcs1 \af0\afs24\alang1025 \ltrch\fcs0 \fs24\lang1049\langfe1049\cgrid\langnp1049\langfenp1049 {\rtlch\fcs1 \af1 \ltrch\fcs0 
\f1\insrsid9075312\charrsid11740259 \'b9 \'ef/\'ef}{\rtlch\fcs1 \ab\af1 \ltrch\fcs0 \f1\insrsid9075312\charrsid11740259 \cell }{\rtlch\fcs1 \af1 \ltrch\fcs0 \f1\insrsid9075312\charrsid11740259 \'b9
\par \'e4\'ee\'e3\'ee\'e2\'ee\'f0\'e0}{\rtlch\fcs1 \ab\af1 \ltrch\fcs0 \b\f1\insrsid9075312\charrsid11740259 \cell }{\rtlch\fcs1 \af1 \ltrch\fcs0 \f1\insrsid9075312\charrsid11740259 \'c0\'e1\'ee\'ed\'e5\'ed\'f2}{\rtlch\fcs1 \ab\af1 \ltrch\fcs0 
\b\f1\insrsid9075312\charrsid11740259 \cell }{\rtlch\fcs1 \af1 \ltrch\fcs0 \f1\insrsid9075312\charrsid11740259 \'cf\'e5\'f0\'e5\'e4\'e0\'e2\'e0\'e5\'ec\'e0\'ff \'ed\'e0 \'e2\'e7\'fb\'f1\'ea\'e0\'ed\'e8\'e5 \'f1\'f3\'ec\'ec\'e0, \'f0\'f3\'e1.}{\rtlch\fcs1 
\ab\af1 \ltrch\fcs0 \b\f1\insrsid9075312\charrsid11740259 \cell }{\rtlch\fcs1 \af1 \ltrch\fcs0 \f1\insrsid9075312\charrsid11740259 \'cf\'e5\'f0\'e8\'ee\'e4
\par \'e7\'e0\'e4\'ee\'eb\'e6\'e5\'ed\'ed\'ee\'f1\'f2\'e8}{\rtlch\fcs1 \ab\af1 \ltrch\fcs0 \b\f1\insrsid9075312\charrsid11740259 \cell }{\rtlch\fcs1 \af1 \ltrch\fcs0 \f1\insrsid9075312\charrsid11740259 \'d8\'e8\'f4\'f0\'fb}{\rtlch\fcs1 \ab\af1 \ltrch\fcs0 
\b\f1\insrsid9075312\charrsid11740259 \cell }\pard\plain \ltrpar\ql \li0\ri0\widctlpar\intbl\wrapdefault\aspalpha\aspnum\faauto\adjustright\rin0\lin0 \rtlch\fcs1 \af0\afs22\alang1025 \ltrch\fcs0 \f39\fs22\lang1049\langfe1049\cgrid\langnp1049\langfenp1049 
{\rtlch\fcs1 \af0 \ltrch\fcs0 \insrsid9075312  \trowd \irow0\irowband0\ltrrow\ts25\trqc\trgaph108\trrh438\trleft-108\trbrdrt\brdrs\brdrw10 \trbrdrl\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 \trbrdrr\brdrs\brdrw10 \trbrdrh\brdrs\brdrw10 \trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth10485\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddft3\trpaddfb3\trpaddfr3\tblrsid6370757\tbllkhdrrows\tbllkhdrcols\tbllknocolband\tblind0\tblindtype3 \clvertalc\clbrdrt\brdrs\brdrw10 \clbrdrl
\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth539\clshdrawnil \cellx436\clvertalc\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1840\clshdrawnil \cellx2274\clvertalc\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth2591\clshdrawnil \cellx4864\clvertalc\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth1909\clshdrawnil \cellx6773\clvertalc\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1936\clshdrawnil \cellx8709\clvertalc\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth1670\clshdrawnil \cellx10377\row \ltrrow}|';
		L_ROW := '\pard\plain \ltrpar
\s21\qc \li0\ri0\widctlpar\intbl\wrapdefault\aspalpha\aspnum\faauto\adjustright\rin0\lin0\pararsid6370757\yts25 \rtlch\fcs1 \af0\afs24\alang1025 \ltrch\fcs0 \fs24\lang1049\langfe1049\cgrid\langnp1049\langfenp1049 {\rtlch\fcs1 \af0 \ltrch\fcs0 
\f1\lang1033\langfe1049\langnp1033\insrsid9075312 RNNUM}{\rtlch\fcs1 \af0 \ltrch\fcs0 \f1\lang1033\langfe1049\langnp1033\insrsid9075312\charrsid8914673 \cell }{\rtlch\fcs1 \af0 \ltrch\fcs0 \f1\lang1033\langfe1049\langnp1033\insrsid9075312 CTRNUM}{\rtlch\fcs1 
\af0 \ltrch\fcs0 \f1\lang1033\langfe1049\langnp1033\insrsid9075312\charrsid8914673 \cell }{\rtlch\fcs1 \af1 \ltrch\fcs0 \f1\lang1033\langfe1049\langnp1033\insrsid9075312 CLINAME}{\rtlch\fcs1 \af0 \ltrch\fcs0 
\f1\lang1033\langfe1049\langnp1033\insrsid9075312\charrsid8914673 \cell }{\rtlch\fcs1 \af0 \ltrch\fcs0 \f1\lang1033\langfe1049\langnp1033\insrsid9075312 DEBTSUM}{\rtlch\fcs1 \af0 \ltrch\fcs0 
\f1\lang1033\langfe1049\langnp1033\insrsid9075312\charrsid8914673 \cell }{\rtlch\fcs1 \af0 \ltrch\fcs0 \f1\lang1033\langfe1049\langnp1033\insrsid9075312 PERIOD}{\rtlch\fcs1 \af0 \ltrch\fcs0 
\f1\lang1033\langfe1049\langnp1033\insrsid9075312\charrsid8914673 \cell }{\rtlch\fcs1 \af0 \ltrch\fcs0 \f1\lang1033\langfe1049\langnp1033\insrsid9075312 CIPHER}{\rtlch\fcs1 \af0 \ltrch\fcs0 \f1\lang1033\langfe1049\langnp1033\insrsid9075312\charrsid8914673 
\cell }\pard\plain \ltrpar\ql \li0\ri0\widctlpar\intbl\wrapdefault\aspalpha\aspnum\faauto\adjustright\rin0\lin0 \rtlch\fcs1 \af0\afs22\alang1025 \ltrch\fcs0 \f39\fs22\lang1049\langfe1049\cgrid\langnp1049\langfenp1049 {\rtlch\fcs1 \af1 \ltrch\fcs0 
\f1\insrsid9075312\charrsid11740259 \trowd \irow2\irowband2\lastrow \ltrrow\ts25\trqc\trgaph108\trrh438\trleft-108\trbrdrt\brdrs\brdrw10 \trbrdrl\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 \trbrdrr\brdrs\brdrw10 \trbrdrh\brdrs\brdrw10 \trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth10485\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddft3\trpaddfb3\trpaddfr3\tblrsid6370757\tbllkhdrrows\tbllkhdrcols\tbllknocolband\tblind0\tblindtype3 \clvertalc\clbrdrt\brdrs\brdrw10 \clbrdrl
\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth539\clshdrawnil \cellx436\clvertalc\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1840\clshdrawnil \cellx2274\clvertalc\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth2591\clshdrawnil \cellx4864\clvertalc\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth1909\clshdrawnil \cellx6773\clvertalc\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1936\clshdrawnil \cellx8709\clvertalc\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth1670\clshdrawnil \cellx10377\row }';
			
     SELECT T.FILE_CONTENT INTO L_RTF FROM T_RTF_TEMPLATES T WHERE T.ID = 5;

	   FOR I IN (SELECT ROWNUM RN, T2.* 
			        FROM (SELECT S.CTR_NUMBER, S.CLI_ALT_NAME, S.SUM_DEBT, T1.CIPHERS, 
                     (SELECT TO_CHAR(TRUNC(MIN(FD.DOC_DATE),'MM'),'DD.MM.YYYY') || ' - ' ||
                             TO_CHAR(MAX(FD.DOC_DATE),'DD.MM.YYYY')
                       FROM T_CLI_DEBTS D
                           ,T_CLI_FINDOCS FD
                       WHERE D.ID_WORK = S.ID_CASE
                       AND FD.ID_DOC = D.ID_DOC)  PERIOD
               FROM V_CLI_CASES S,    
							      APEX_COLLECTIONS COL,                                     
                   (SELECT T.ID_WORK, LISTAGG (T.SERV_TYPE,', ') WITHIN GROUP (ORDER BY T.SERV_TYPE) CIPHERS
									 FROM
                    (SELECT DISTINCT FD.SERV_TYPE, D.ID_WORK
                     FROM T_CLI_DEBTS D,
                          T_CLI_FINDOCS FD
                     WHERE FD.ID_DOC = D.ID_DOC) T                            
                    GROUP BY T.ID_WORK) T1 
               WHERE S.ID_CASE = T1.ID_WORK
							 AND   S.ID_CASE = COL.c001
							 AND   COL.collection_name = 'PRE_TRIAL_COURT_DATA'
							 ORDER BY S.ID_CASE) T2)                
		 LOOP     
			L_NEW_ROW := L_ROW;
			L_NEW_ROW := REPLACE(L_NEW_ROW,'RNNUM',I.RN);
			L_NEW_ROW := REPLACE(L_NEW_ROW,'CTRNUM',I.CTR_NUMBER);
			L_NEW_ROW := REPLACE(L_NEW_ROW,'CLINAME',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(I.CLI_ALT_NAME));
  		L_NEW_ROW := REPLACE(L_NEW_ROW,'DEBTSUM',TO_CHAR(I.SUM_DEBT,'999G999G999G999G990D99','NLS_NUMERIC_CHARACTERS='', '''));
  		L_NEW_ROW := REPLACE(L_NEW_ROW,'PERIOD',I.PERIOD);
  		L_NEW_ROW := REPLACE(L_NEW_ROW,'CIPHER',I.CIPHERS);			
			L_TABLE := L_TABLE || L_NEW_ROW;
		 END LOOP;					
    
		 L_RTF := PKG_DOC_REPORTS.REPLACE_CLOB(L_RTF,'TABLE',L_TABLE);
		 
		 L_RTF := REPLACE(L_RTF,'DATE',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(TO_CHAR(SYSDATE,'DD.MM.YYYY')||'г'));
		 L_RTF := REPLACE(L_RTF,'PREMAIN',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(PKG_PREF.F$C1('PRE_TRIAL_MAIN')));
		 
  	 l_blob := PKG_DOC_REPORTS.CLOB2BLOB(PAR_CLOB =>l_rtf);		
		 RTF_DOC.P_BLOB :=  l_blob;
	
	END P_ADD_DATA;
	
---------------------------------------------------------------------------------------------------------------------------------

  FUNCTION RUN_REPORT(P_PAR_01 VARCHAR2 DEFAULT NULL
											,P_PAR_02 VARCHAR2 DEFAULT NULL
											,P_PAR_03 VARCHAR2 DEFAULT NULL
											,P_PAR_04 VARCHAR2 DEFAULT NULL
											,P_PAR_05 VARCHAR2 DEFAULT NULL
											,P_PAR_06 VARCHAR2 DEFAULT NULL
											,P_PAR_07 VARCHAR2 DEFAULT NULL
											,P_PAR_08 VARCHAR2 DEFAULT NULL
											,P_PAR_09 VARCHAR2 DEFAULT NULL
											,P_PAR_10 VARCHAR2 DEFAULT NULL) RETURN LAWSUP.PKG_FILES.REC_DOC 
	IS
		REC_DOC LAWSUP.PKG_FILES.REC_DOC;
  BEGIN
 	
	  RTF_DOC := NULL;
		P_ADD_DATA;    		
		REC_DOC.P_BLOB := RTF_DOC.P_BLOB;
		REC_DOC.P_FILE_NAME := 'SLUZHEBNAYA_ZAPISKA';

		RETURN REC_DOC;

  END RUN_REPORT;		

end PKG_DOC_002;
/

