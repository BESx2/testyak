i�?create or replace package lawmain.PKG_DOCX_001 is

  -- Author  : EUGEN
  -- Created : 04.03.2020 15:02:04
  -- Purpose : Претензия
	-- CODE    : PRETENSION
  
  FUNCTION RUN_REPORT(P_PAR_01  IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_02 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_03 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_04 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_05 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_06 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_07 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_08 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_09 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_10 IN VARCHAR2 DEFAULT NULL) RETURN LAWSUP.PKG_FILES.REC_DOC;		                           

end PKG_DOCX_001;
/

create or replace package body lawmain.PKG_DOCX_001 is

  FUNCTION RUN_REPORT(P_PAR_01  IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_02 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_03 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_04 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_05 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_06 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_07 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_08 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_09 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_10 IN VARCHAR2 DEFAULT NULL) RETURN LAWSUP.PKG_FILES.REC_DOC
   IS  
   L_XML XMLTYPE;
   L_SQL SYS_REFCURSOR;     
	 L_GROUP VARCHAR2(50);            
	 L_CTR_NUM VARCHAR2(100);
   L_REP LAWSUP.PKG_FILES.REC_DOC;    
	 L_TYPE VARCHAR2(50);
  BEGIN         
		SELECT W.TYPE_WORK INTO L_TYPE FROM T_CLI_DEBT_WORK W WHERE W.ID_WORK = TO_NUMBER(P_PAR_01);
		IF L_TYPE = 'PRETEN' THEN 
			OPEN L_SQL FOR 
				SELECT CLI.CLI_ALT_NAME "абонент",       
							 s.date_pretension "дата_претензии",
							 PKG_CLIENTS.F_GET_ADDRESS(S.ID_CLIENT) "адрес_абонента",
							 (SELECT TO_CHAR(TRUNC(MIN(FD.DOC_DATE),'MM'),'DD.MM.YYYY')||
											 ' года по '||
											 TO_CHAR(LAST_DAY(MAX(FD.DOC_DATE)),'DD.MM.YYYY')||' года'
								FROM T_CLI_DEBTS D, 
										 T_CLI_FINDOCS FD
								WHERE D.ID_WORK = S.ID_WORK
								AND   FD.ID_DOC = D.ID_DOC) "период",
								PKG_CLI_CASES_UTL.F_GET_CONTRACTS(S.ID_WORK,'и ') "договоры",
								s.SUM_DEBT "общая_сумма_долга",      
								'«'||TO_CHAR(S.PENY_DATE,'DD')||'» '||LOWER(PKG_UTILS.F_GET_NAME_MONTHS(S.PENY_DATE,'R'))||' '||TO_CHAR(S.PENY_DATE,'YYYY"г."') "дата_пени",
								(SELECT SUM(D.DEBT_CURRENT) FROM T_CLI_DEBTS D WHERE D.ID_WORK = S.ID_WORK
								 AND D.ID_CONTRACT = S.CWS_ID_CONTRACT)	"сумма_задолженности1",				
								(SELECT SUM(D.DEBT_CURRENT) FROM T_CLI_DEBTS D WHERE D.ID_WORK = S.ID_WORK
								 AND D.ID_CONTRACT = S.SEW_ID_CONTRACT)	"сумма_задолженности2",  
								s.peny "общая_сумма_пеней",
								(SELECT SUM(D.PENY) FROM T_CLI_DEBTS D WHERE D.ID_WORK = S.ID_WORK
								 AND D.ID_CONTRACT = S.CWS_ID_CONTRACT)	"сумма_пеней1",				
								(SELECT SUM(D.PENY) FROM T_CLI_DEBTS D WHERE D.ID_WORK = S.ID_WORK
								 AND D.ID_CONTRACT = S.SEW_ID_CONTRACT)	"сумма_пеней2",
								 nvl(s.SUM_DEBT,0) + nvl(s.PENY,0) "весь_долг",
								 INITCAP(L.LAST_NAME||' '||L.FIRST_NAME||' '||L.SECOND_NAME) "исполнитель",
								 NVL2(L.PHONE,' ,'||l.phone,NULL) "телефон_исп",
								 CASE
										WHEN S.CWS_ID_CONTRACT IS NOT NULL THEN 'true'
										ELSE 'false'
								 END 	"если_водоснабжение",
								 CASE
										WHEN S.SEW_ID_CONTRACT IS NOT NULL THEN 'true'
										ELSE 'false'
								 END 	"если_водоотведение",
								 CASE
										WHEN S.CWS_ID_CONTRACT IS NOT NULL AND S.SEW_ID_CONTRACT IS NOT NULL THEN 'подачу холодной воды и прием сточных вод'
										WHEN S.CWS_ID_CONTRACT IS NOT NULL AND S.CWS_CTR_NUM LIKE '%СОИ%' THEN 'подачу холодной воды на содержание общего имущества' 
										WHEN S.CWS_ID_CONTRACT IS NOT NULL THEN 'подачу холодной воды' 
										WHEN S.SEW_ID_CONTRACT IS NOT NULL THEN 'прием сточных вод' 
								 END  "текст1",
								 CASE
										WHEN S.CWS_ID_CONTRACT IS NOT NULL AND S.SEW_ID_CONTRACT IS NOT NULL THEN 'поданной за указанный период холодной воды и принятых сточных вод'
										WHEN S.CWS_ID_CONTRACT IS NOT NULL AND S.CWS_CTR_NUM LIKE '%СОИ%' THEN 'поданной за указанный период холодной воды на содержание общего имущества' 
										WHEN S.CWS_ID_CONTRACT IS NOT NULL THEN 'поданной за указанный период холодной воды' 
										WHEN S.SEW_ID_CONTRACT IS NOT NULL THEN 'принятых за указанный период сточных вод' 
								 END  "текст2",                                                                        
								 CASE
										WHEN S.CWS_ID_CONTRACT IS NOT NULL AND S.SEW_ID_CONTRACT IS NOT NULL THEN 'поданной холодной воды и принятых сточных вод'
										WHEN S.CWS_ID_CONTRACT IS NOT NULL AND S.CWS_CTR_NUM LIKE '%СОИ%' THEN 'поданной холодной воды на содержание общего имущества' 
										WHEN S.CWS_ID_CONTRACT IS NOT NULL THEN 'поданной холодной воды' 
										WHEN S.SEW_ID_CONTRACT IS NOT NULL THEN 'принятых сточных вод' 
								 END  "текст3",
								 PKG_PREF.F$C2('SBIT_MAIN') "ФИО"															 
				FROM V_CLI_PRETENSION S,
						 T_CLIENTS CLI,
						 T_USER_LIST L
				WHERE CLI.ID_CLIENT = S.ID_CLIENT
				AND   S.CURATOR = L.ID(+)
				AND   S.ID_WORK = TO_NUMBER(P_PAR_01);                                                                             
				
				SELECT S.ABON_GROUP,S.CWS_CTR_NUM INTO L_GROUP, L_CTR_NUM FROM V_CLI_PRETENSION S WHERE S.ID_WORK = TO_NUMBER(P_PAR_01);
		ELSE
			OPEN L_SQL FOR 
				SELECT CLI.CLI_ALT_NAME "абонент",       
							 s.date_pretension "дата_претензии",
							 PKG_CLIENTS.F_GET_ADDRESS(S.ID_CLIENT) "адрес_абонента",
							 (SELECT TO_CHAR(TRUNC(MIN(FD.DOC_DATE),'MM'),'DD.MM.YYYY')||
											 ' года по '||
											 TO_CHAR(LAST_DAY(MAX(FD.DOC_DATE)),'DD.MM.YYYY')||' года'
								FROM T_CLI_DEBTS D, 
										 T_CLI_FINDOCS FD
								WHERE D.ID_WORK = S.ID_CASE
								AND   FD.ID_DOC = D.ID_DOC) "период",
								PKG_CLI_CASES_UTL.F_GET_CONTRACTS(S.ID_CASE,'и ') "договоры",
								s.SUM_DEBT "общая_сумма_долга",      
								'«'||TO_CHAR(S.PENY_DATE,'DD')||'» '||LOWER(PKG_UTILS.F_GET_NAME_MONTHS(S.PENY_DATE,'R'))||' '||TO_CHAR(S.PENY_DATE,'YYYY"г."') "дата_пени",
								(SELECT SUM(D.DEBT_CURRENT) FROM T_CLI_DEBTS D WHERE D.ID_WORK = S.ID_CASE
								 AND D.ID_CONTRACT = S.CWS_ID_CONTRACT)	"сумма_задолженности1",				
								(SELECT SUM(D.DEBT_CURRENT) FROM T_CLI_DEBTS D WHERE D.ID_WORK = S.ID_CASE
								 AND D.ID_CONTRACT = S.SEW_ID_CONTRACT)	"сумма_задолженности2",  
								s.peny "общая_сумма_пеней",
								(SELECT SUM(D.PENY) FROM T_CLI_DEBTS D WHERE D.ID_WORK = S.ID_CASE
								 AND D.ID_CONTRACT = S.CWS_ID_CONTRACT)	"сумма_пеней1",				
								(SELECT SUM(D.PENY) FROM T_CLI_DEBTS D WHERE D.ID_WORK = S.ID_CASE
								 AND D.ID_CONTRACT = S.SEW_ID_CONTRACT)	"сумма_пеней2",
								 nvl(s.SUM_DEBT,0) + nvl(s.PENY,0) "весь_долг",
								 INITCAP(L.LAST_NAME||' '||L.FIRST_NAME||' '||L.SECOND_NAME) "исполнитель",
								 NVL2(L.PHONE,' ,'||l.phone,NULL) "телефон_исп",
								 CASE
										WHEN S.CWS_ID_CONTRACT IS NOT NULL THEN 'true'
										ELSE 'false'
								 END 	"если_водоснабжение",
								 CASE
										WHEN S.SEW_ID_CONTRACT IS NOT NULL THEN 'true'
										ELSE 'false'
								 END 	"если_водоотведение",
								 CASE
										WHEN S.CWS_ID_CONTRACT IS NOT NULL AND S.SEW_ID_CONTRACT IS NOT NULL THEN 'подачу холодной воды и прием сточных вод'
										WHEN S.CWS_ID_CONTRACT IS NOT NULL AND S.CWS_CTR_NUM LIKE '%СОИ%' THEN 'подачу холодной воды на содержание общего имущества' 
										WHEN S.CWS_ID_CONTRACT IS NOT NULL THEN 'подачу холодной воды' 
										WHEN S.SEW_ID_CONTRACT IS NOT NULL THEN 'прием сточных вод' 
								 END  "текст1",
								 CASE
										WHEN S.CWS_ID_CONTRACT IS NOT NULL AND S.SEW_ID_CONTRACT IS NOT NULL THEN 'поданной за указанный период холодной воды и принятых сточных вод'
										WHEN S.CWS_ID_CONTRACT IS NOT NULL AND S.CWS_CTR_NUM LIKE '%СОИ%' THEN 'поданной за указанный период холодной воды на содержание общего имущества' 
										WHEN S.CWS_ID_CONTRACT IS NOT NULL THEN 'поданной за указанный период холодной воды' 
										WHEN S.SEW_ID_CONTRACT IS NOT NULL THEN 'принятых за указанный период сточных вод' 
								 END  "текст2",                                                                        
								 CASE
										WHEN S.CWS_ID_CONTRACT IS NOT NULL AND S.SEW_ID_CONTRACT IS NOT NULL THEN 'поданной холодной воды и принятых сточных вод'
										WHEN S.CWS_ID_CONTRACT IS NOT NULL AND S.CWS_CTR_NUM LIKE '%СОИ%' THEN 'поданной холодной воды на содержание общего имущества' 
										WHEN S.CWS_ID_CONTRACT IS NOT NULL THEN 'поданной холодной воды' 
										WHEN S.SEW_ID_CONTRACT IS NOT NULL THEN 'принятых сточных вод' 
								 END  "текст3",
								 PKG_PREF.F$C2('SBIT_MAIN') "ФИО"															
				FROM V_CLI_CASES S,
						 T_CLIENTS CLI,
						 T_USER_LIST L
				WHERE CLI.ID_CLIENT = S.ID_CLIENT
				AND   S.CURATOR_PRE = L.ID(+)
				AND   S.ID_CASE = TO_NUMBER(P_PAR_01);
				
				SELECT S.GROUP_CODE,S.CWS_CTR_NUM INTO L_GROUP, L_CTR_NUM FROM V_CLI_CASES S WHERE S.ID_CASE = TO_NUMBER(P_PAR_01);
		END IF;		
			
		
		IF L_GROUP IN ('UK','TSG') OR L_CTR_NUM LIKE '%СОИ%' THEN	
      L_XML := PKG_CONTROLLER.F_DOCX_XML(P_SQL => L_SQL,P_REP_CODE => 'UK_PRETEN');    
		ELSE
			L_XML := PKG_CONTROLLER.F_DOCX_XML(P_SQL => L_SQL,P_REP_CODE => 'OTHER_PRETEN');    
		END IF;

    L_REP.P_BLOB := PKG_CONTROLLER.F_GET_REPORT_XML(L_XML.getClobVal());
    L_REP.P_FILE_NAME := 'Перетензия';
		
		RETURN l_rep;
	END;										
	
end PKG_DOCX_001;
/

