i�?CREATE OR REPLACE PACKAGE LAWMAIN."PKG_ROLES"
	IS
	/*
		Package for manipulation klients in application
		@author vaschenkov
		@since 1.11.2010

	*/

	PROCEDURE P_ADD_ROLE(	P_USER_ID IN NUMBER
												,P_ROLE_ID  IN NUMBER
										 );

	PROCEDURE P_REVOKE_USER_ROLE(P_ID_PRIVS IN NUMBER);

	FUNCTION F_GET_ROLE_NAME(P_ROLE_ID IN NUMBER) RETURN VARCHAR2;

	/*
		For current user define 'Is user have this role?'
		If Yes - return  TRUE
		If NO  - return  FALSE
	*/
/*	FUNCTION F$(P_ROLE_NAME IN VARCHAR2) RETURN BOOLEAN;*/

	/*
		For current user define 'Is user have privilege to object?'
		If Yes - return  TRUE
		If NO  - return  FALSE
	*/
	FUNCTION F$OBJ(P_OBJ IN VARCHAR2) RETURN BOOLEAN;

/*	FUNCTION F$OBJ_USR_YN(P_OBJ IN VARCHAR2,P_USR_ID IN VARCHAR2) RETURN VARCHAR2;*/


	/*
		Revoke object from role
	*/
	PROCEDURE P_REVOKE_OBJ_PRV(P_ID IN NUMBER);

	FUNCTION F_CREATE_ROLE(	P_SHORT_NAME IN VARCHAR2
									,P_NAME IN VARCHAR2
									,P_DESCRIPTION IN VARCHAR2
									,P_ID_FK IN NUMBER
							 ) RETURN NUMBER;

	G_P_N VARCHAR2(200) := 'PKG_ROLES.';

	PROCEDURE P_UPDATE_ROLE(	P_ID IN NUMBER
														,P_SHORT_NAME IN VARCHAR2
														,P_NAME IN VARCHAR2
														,P_DESCRIPTION IN VARCHAR2
														,P_ID_ROLE_FK IN NUMBER
													);

	PROCEDURE P_DELETE_ROLE(P_ID IN NUMBER);

	FUNCTION F_GET_ACCESS_TO_PAGE RETURN BOOLEAN;

	PROCEDURE P_ADD_ACCESS(P_ID_PAGE NUMBER,P_ID_OBJ NUMBER);

	PROCEDURE P_REMOVE_ACCESS(P_ID_PAGE IN NUMBER);

	/*FUNCTION F_GET_OBJ_ID(P_OBJ_NAME IN VARCHAR2) RETURN NUMBER;

	FUNCTION F$_R_U(P_ROLE_NAME IN VARCHAR2,P_USER_ID NUMBER) RETURN BOOLEAN;
	FUNCTION F$_R_U_Y_N(P_ROLE_NAME IN VARCHAR2,P_USER_ID NUMBER) RETURN VARCHAR2;*/

	PROCEDURE P_PRINT_TREE;

END;
/

CREATE OR REPLACE PACKAGE BODY LAWMAIN."PKG_ROLES"
	IS

	FUNCTION F_GET_ROLE_NAME(P_ROLE_ID IN NUMBER) RETURN VARCHAR2
		IS
		L_NAME  VARCHAR2(200);
	BEGIN
		SELECT TRIM(UPPER(RLS.SHORT_NAME)) INTO L_NAME
				FROM T_USER_ROLES RLS
			WHERE RLS.ID = P_ROLE_ID;
		RETURN L_NAME;
	END;

	PROCEDURE P_ADD_ROLE(	P_USER_ID IN NUMBER
												,P_ROLE_ID  IN NUMBER
										 )
		IS
		G_F_N VARCHAR2(300) := G_P_N ||'P_ADD_ROLE';
		L_USER_NAME VARCHAR2(2000);
		L_ROLE_NAME VARCHAR2(200);
	BEGIN
		IF P_USER_ID IS NULL THEN
			raise_application_error(-20200,'PK is nULL');
		END IF;

		INSERT INTO T_USER_ROLE_PRIVS(ID,USER_ID,ROLE_ID,DATE_CREATED,CREATED_BY_ID)
		VALUES(	SEQ_MAIN.Nextval
						,P_USER_ID
						,P_ROLE_ID
						,SYSDATE
						,f$_usr_id);

		--P_SET_ADMIN_PRIVS(P_USER_ID);
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE = -20200 THEN
				RAISE;
			ELSIF SQLCODE = -1 THEN
			 --ERM_PKG.raise_Error('ROLE_EXIST',G_F_N);
			 raise_application_error(-20000,'Roles already exists');
			ELSIF SQLCODE = -54 THEN
				raise_application_error(-20000,'Record is locked');
			ELSE
				raise_application_error(-20000,SQLERRM);
			END IF;
	END;

 
	PROCEDURE P_REVOKE_USER_ROLE(P_ID_PRIVS IN NUMBER)
		IS
		G_F_N VARCHAR2(300) := G_P_N ||'P_REVOKE_USER_ROLE';
		L_ID NUMBER;
		L_ROLE_ID NUMBER;
		L_USER_ID NUMBER;
		L_USER_NAME VARCHAR2(2000);
	BEGIN
		IF P_ID_PRIVS IS NULL THEN
			raise_application_error(-20000,'ID_PRIVS is null');
		END IF;

		DELETE FROM T_USER_ROLE_PRIVS
			WHERE id = P_ID_PRIVS;

	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE = -20200 THEN
				RAISE;
			END IF;
	END;
   /*
	FUNCTION F$(P_ROLE_NAME IN VARCHAR2) RETURN BOOLEAN
		IS
	BEGIN
		RETURN F$_R_U(P_ROLE_NAME,f$_usr_id);
	END;

	FUNCTION F$_R_U_Y_N(P_ROLE_NAME IN VARCHAR2,P_USER_ID NUMBER) RETURN VARCHAR2
		IS
	BEGIN
		IF F$_R_U(P_ROLE_NAME,P_USER_ID) = TRUE THEN
		 RETURN 'Y';
		ELSE
			RETURN 'N';
		END IF;
		RETURN 'N';
	END;


	FUNCTION F$_R_U(P_ROLE_NAME IN VARCHAR2,P_USER_ID NUMBER) RETURN BOOLEAN
		IS
		L_CNT NUMBER;
		L_RET BOOLEAN := TRUE;
	BEGIN
		SELECT COUNT(1) INTO L_CNT
				FROM T_USER_ROLES R
			WHERE 1=1
				AND R.SHORT_NAME = UPPER(TRIM(P_ROLE_NAME))
				START WITH R.ID IN	(
															SELECT PR.ROLE_ID
																	FROM T_USER_ROLE_PRIVS PR
																WHERE 	1=1
																		AND PR.USER_ID = P_USER_ID
														)
				CONNECT BY PRIOR r.id = r.id_fk;
		IF L_CNT = 0 THEN
			L_RET := FALSE;
		END IF;
		RETURN L_RET;
	END;


	FUNCTION F_GET_OBJ_ID(P_OBJ_NAME IN VARCHAR2) RETURN NUMBER
		IS
		L_ID NUMBER;
	BEGIN
		SELECT o.id INTO L_ID
			FROM T_USER_OBJECTS O
		WHERE O.SHORT_NAME = UPPER(TRIM(P_OBJ_NAME));
		RETURN L_ID;
	END; */


	FUNCTION F$OBJ(P_OBJ IN VARCHAR2) RETURN BOOLEAN
		IS
		L_CNT NUMBER;
		L$user_id  NUMBER :=  f$_usr_id;
	BEGIN

		SELECT COUNT(1) INTO L_CNT
		FROM  -- список ролей по объекту
					(
						SELECT PRV.ID_ROLE
						FROM 	T_USER_OBJECT_ROLE_PRIVS PRV
									,T_USER_OBJECTS OBJ
						WHERE PRV.ID_OBJECT = OBJ.ID
							AND OBJ.SHORT_NAME = TRIM(UPPER(P_OBJ)) --'OBJ_001'
					) OBJ_
					,
					-- список ролей по пользователю
					(
					SELECT R.ID ID_ROLE,R.Name
						FROM T_USER_ROLES R
					WHERE 1=1
						START WITH R.ID IN	(
																	SELECT PR.ROLE_ID
																			FROM T_USER_ROLE_PRIVS PR
																		WHERE 	1=1
																				AND PR.USER_ID = L$user_id
																)
						CONNECT BY PRIOR r.id = r.id_fk
				)	RLS_
		WHERE OBJ_.ID_ROLE = RLS_.ID_ROLE ;

		IF L_CNT > 0 THEN
		 RETURN TRUE;
		ELSE
			RETURN FALSE;
		END IF;
	END;
  /*
	FUNCTION F$OBJ_USR_YN(P_OBJ IN VARCHAR2,P_USR_ID IN VARCHAR2) RETURN VARCHAR2
		IS
		L_CNT NUMBER;
	BEGIN

		SELECT COUNT(1) INTO L_CNT
		FROM  -- список ролей по объекту
					(
						SELECT PRV.ID_ROLE
						FROM 	T_USER_OBJECT_ROLE_PRIVS PRV
									,T_USER_OBJECTS OBJ
						WHERE PRV.ID_OBJECT = OBJ.ID
							AND OBJ.SHORT_NAME = TRIM(UPPER(P_OBJ)) --'OBJ_001'
					) OBJ_
					,
					-- список ролей по пользователю
					(
					SELECT R.ID ID_ROLE,R.Name
						FROM T_USER_ROLES R
					WHERE 1=1
						START WITH R.ID IN	(
																	SELECT PR.ROLE_ID
																			FROM T_USER_ROLE_PRIVS PR
																		WHERE 	1=1
																				AND PR.USER_ID = P_USR_ID
																)
						CONNECT BY PRIOR r.id = r.id_fk
				)	RLS_
		WHERE OBJ_.ID_ROLE = RLS_.ID_ROLE ;

		IF L_CNT > 0 THEN
		 RETURN 'Y';
		ELSE
			RETURN 'N';
		END IF;
	END;*/


	PROCEDURE P_REVOKE_OBJ_PRV(P_ID IN NUMBER)
		IS
	BEGIN
		DELETE FROM T_USER_OBJECT_ROLE_PRIVS S
			WHERE S.ID = P_ID;
	END;

	FUNCTION F_CREATE_ROLE(	P_SHORT_NAME IN VARCHAR2
									,P_NAME IN VARCHAR2
									,P_DESCRIPTION IN VARCHAR2
									,P_ID_FK IN NUMBER
							 ) RETURN NUMBER

		IS
		L_ID NUMBER;
	BEGIN 
		INSERT INTO T_USER_ROLES(ID,SHORT_NAME,NAME,DESCRIPTION,ID_FK,CREATED_BY_ID,DATE_CREATED)
				VALUES(SEQ_main.NEXTVAL,TRIM(UPPER(P_SHORT_NAME)),TRIM(P_NAME),TRIM(P_DESCRIPTION),P_ID_FK,f$_usr_id,SYSDATE)
		RETURNING ID INTO L_ID;
		RETURN L_ID;
	END;

	PROCEDURE P_UPDATE_ROLE(	P_ID IN NUMBER
														,P_SHORT_NAME IN VARCHAR2
														,P_NAME IN VARCHAR2
														,P_DESCRIPTION IN VARCHAR2
														,P_ID_ROLE_FK IN NUMBER
													)
		IS
		L_cnt NUMBER;
		G_F_N VARCHAR2(300) := G_P_N ||'P_UPDATE_ROLE';
		L_EXP EXCEPTION;
	BEGIN


		SELECT COUNT(1) INTO L_cnt
		FROM T_USER_ROLES R
		WHERE R.ID =  P_ID_ROLE_FK
		START WITH R.ID = P_ID
		CONNECT BY PRIOR R.ID =  R.ID_FK;

		IF L_CNT > 0 THEN
			RAISE L_EXP;
		END IF;

		UPDATE T_USER_ROLES R
			SET R.SHORT_NAME = TRIM(UPPER(P_SHORT_NAME))
					,R.NAME = TRIM(P_NAME)
					,R.DESCRIPTION = TRIM(P_DESCRIPTION)
					,R.ID_FK = P_ID_ROLE_FK
					,R.MODIFIED_BY_ID = f$_usr_id
					,R.Date_Modified = SYSDATE
		WHERE R.ID = P_ID;

	EXCEPTION
		WHEN L_EXP THEN
			RAISE_APPLICATION_ERROR(-20000,'Inpossible to set child role as parents role');
			--ERM_PKG.raise_Error('ROLE_CHL',G_F_N);
		WHEN OTHERS THEN
			IF SQLCODE = -20200 THEN
				RAISE;
			END IF;
	END;

	PROCEDURE P_DELETE_ROLE(P_ID IN NUMBER)
		IS
		G_F_N VARCHAR2(300) := G_P_N ||'P_DELETE_ROLE';
	BEGIN
		DELETE FROM T_USER_OBJECT_ROLE_PRIVS D WHERE d.id_role = P_ID;
		DELETE FROM T_USER_ROLES D WHERE d.id = P_ID;
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE = -20200 THEN
				RAISE;
			END IF;
	END;

	FUNCTION F_GET_ACCESS_TO_PAGE RETURN BOOLEAN
		IS
		L_OBJ VARCHAR2(300);
		L_RET BOOLEAN := FALSE;
	BEGIN
		BEGIN
			SELECT O.SHORT_NAME INTO L_OBJ
				FROM 	T_USER_PAGE_ACCESS P
							,T_USER_OBJECTS O
				WHERE 1=1
					AND O.Id = p.id_object
					AND P.ID_PAGE = TO_NUMBER(V('APP_PAGE_ID'));

			L_RET := f$obj(L_OBJ);
			IF L_RET = FALSE THEN
					pkg_log.P$LOG(	P_PROG_UNIT => 'ACCESS_DENIDED'
														,P_MESS => V('APP_PAGE_ID')||':'||V('APP_USER')
														);
			END IF;
		EXCEPTION
			WHEN NO_DATA_FOUND THEN
				L_RET := FALSE;
				pkg_log.P$LOG(	P_PROG_UNIT => 'ACCESS_DENIDED'
													,P_MESS => V('APP_PAGE_ID')||':'||V('APP_USER')||':ДОСТУП НЕ ИНИЦИАЛИЗИРОВАН'
													);
			WHEN OTHERS THEN
				pkg_log.P$LOG(	P_PROG_UNIT => 'ACCESS_DENIDED'
													,P_MESS => V('APP_PAGE_ID')||':'||V('APP_USER')||':ДОСТУП НЕ ИНИЦИАЛИЗИРОВАН'
													);
				RAISE;
    END;

		RETURN L_RET;

	END;

 	PROCEDURE P_ADD_ACCESS(P_ID_PAGE NUMBER,P_ID_OBJ NUMBER)
		IS
	BEGIN
		INSERT INTO T_USER_PAGE_ACCESS(ID,ID_PAGE,ID_OBJECT,DESCRIPTION)
		VALUES(SEQ_MAIN.Nextval,P_ID_PAGE,P_ID_OBJ,NULL);
	END;

	PROCEDURE P_REMOVE_ACCESS(P_ID_PAGE IN NUMBER)
		IS
	BEGIN
		DELETE FROM T_USER_PAGE_ACCESS S WHERE s.id_page = P_ID_PAGE;
	END;


	PROCEDURE P_PRINT_TREE
		IS
		t varchar2(32000);
		cnt NUMBER := 0;
	BEGIN
		IF SUBSTR(APEX_APPLICATION.G_x01,1,2) = '-1' THEN
			t :=   '[';
				FOR I IN
				(
						SELECT T.ID,
									 APEX_JAVASCRIPT.escape(
									 														substr(t.name,1,30)||decode(sign(length(t.name)-30),1,' ...','')
									 												) short_name
							FROM T_USER_ROLES T
						 WHERE t.id IN (Select DECODE(f$obj_yn('ADMIN'),'Y',rr.id_fk,rr.id)
						 									FROM t_user_roles rr
														WHERE rr.id_fk IN (Select r.id FROM t_user_roles r
																								WHERE r.short_name = 'ADMIN'))
						ORDER BY t.name
				)
				LOOP
					IF cnt = 1 THEN
						t := t || ',';
					END IF;
					t := t||'{"attr":{"id":"'||i.ID||'","timestemp":"'||to_char(SYSDATE,'mm:ss')||'"}
									,"data":{"title" : "'||i.short_name||'","timestemp":"'||to_char(SYSDATE,'mm:ss')||'"}
									,"state":"closed"
									}';
						cnt := 1;
					END LOOP;
				t := t || ']';
		ELSE
			t :=   '[';
			FOR i IN (
						SELECT T.ID,
									 APEX_JAVASCRIPT.escape(
									 														substr(t.name,1,30)||decode(sign(length(t.name)-30),1,' ...','')
									 												) short_name
									,(SELECT COUNT(1) FROM t_user_roles ff WHERE ff.id_fk = t.id AND rownum < 2 ) cnt
							FROM T_USER_ROLES T
						 WHERE t.id_fk = TO_NUMBER(APEX_APPLICATION.G_x01)
						 --AND F$OBJ_ID_YN(t.ID_OBJECT) = 'Y'
						 ORDER BY t.name
			)
			LOOP
				IF cnt = 1 THEN
					t := t || ',';
				END IF;
					t := t ||
								'{"attr":{"id":"'||i.ID||'","timestemp":"'||to_char(SYSDATE,'mm:ss')||'"}
									,"data":{"title" : "'||i.short_name||'","timestemp":"'||to_char(SYSDATE,'mm:ss')||'"}';
					IF i.cnt > 0 THEN
						t := t || '	,"state":"closed"
											';
					END IF;
					t := t ||'}';
				cnt := 1;
			END LOOP;
			t := t || ']';
		END IF;
		htp.prn(t);
	END;




END;
/

