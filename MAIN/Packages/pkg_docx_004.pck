i�?create or replace package lawmain.PKG_DOCX_004 is

  -- Author  : EUGEN
  -- Created : 04.03.2020 15:02:04
  -- Purpose : Исковое заявление 	                                
	-- CODE    : LAWSUIT
  
  FUNCTION RUN_REPORT(P_PAR_01  IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_02 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_03 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_04 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_05 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_06 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_07 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_08 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_09 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_10 IN VARCHAR2 DEFAULT NULL) RETURN LAWSUP.PKG_FILES.REC_DOC;		                           

end PKG_DOCX_004;
/

create or replace package body lawmain.PKG_DOCX_004 is

  FUNCTION RUN_REPORT(P_PAR_01  IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_02 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_03 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_04 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_05 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_06 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_07 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_08 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_09 IN VARCHAR2 DEFAULT NULL
                      ,P_PAR_10 IN VARCHAR2 DEFAULT NULL) RETURN LAWSUP.PKG_FILES.REC_DOC
   IS  
   L_XML XMLTYPE;
   L_SQL SYS_REFCURSOR;     
	 L_GROUP VARCHAR2(50);
   L_REP LAWSUP.PKG_FILES.REC_DOC;    
  BEGIN         
    OPEN L_SQL FOR 
      SELECT INITCAP(L.LAST_NAME||' '||L.FIRST_NAME||' '||L.SECOND_NAME) "ФИО"
			      ,L.PHONE "телефон"
						,S.CLI_ALT_NAME "абонент"
						,S.OGRN  "огрн_абон"                 
						,S.INN   "инн_абон"
						,PKG_CLIENTS.F_GET_ADDRESS(S.ID_CLIENT)	"адрес_абон"
						,nvl(s.SUM_DEBT,0) + nvl(s.PENY,0) "цена_иска"
						,nvl(s.GOV_TOLL,0) "сумма_госпошлины"
						,(SELECT TO_CHAR(TRUNC(MIN(FD.DOC_DATE),'MM'),'DD.MM.YYYY')||
						         ' - '||TO_CHAR(LAST_DAY(MAX(FD.DOC_DATE)),'DD.MM.YYYY')
						  FROM T_CLI_FINDOCS FD,
							     T_CLI_DEBTS D
							WHERE D.ID_WORK = S.ID_CASE
							AND   FD.ID_DOC = D.ID_DOC)	 "период"
						,CASE    
						   WHEN S.CWS_CTR_NUM LIKE '%СОИ%' THEN 'подачу холодной воды на содержание общего имущества в многоквартирном доме'
						   WHEN S.CWS_ID_CONTRACT IS NOT NULL AND S.SEW_ID_CONTRACT IS NOT NULL THEN 'подачу холодной воды и прием сточных вод' 
						   WHEN S.CWS_ID_CONTRACT IS NOT NULL THEN 'подачу холодной воды'  				 
							 WHEN S.SEW_ID_CONTRACT IS NOT NULL THEN 'прием сточных вод'
						 END "услуги2"   
						,CASE 
 						   WHEN S.CWS_CTR_NUM LIKE '%СОИ%' THEN 'за потребленную холодную воду на содержание общего имущества в многоквартирном доме'
						   WHEN S.CWS_ID_CONTRACT IS NOT NULL AND S.SEW_ID_CONTRACT IS NOT NULL THEN 'холодное водоснабжение и водоотведение' 
						   WHEN S.CWS_ID_CONTRACT IS NOT NULL THEN 'холодное водоснабжение'  				 
							 WHEN S.SEW_ID_CONTRACT IS NOT NULL THEN 'водоотведение'
						 END "услуги1"  
					 ,CASE                                                                                                                         
						   WHEN S.CWS_ID_CONTRACT IS NOT NULL AND S.SEW_ID_CONTRACT IS NOT NULL THEN 'подано холодной воды и принято сточных вод' 
						   WHEN S.CWS_ID_CONTRACT IS NOT NULL THEN 'подано холодной воды'  				 
							 WHEN S.SEW_ID_CONTRACT IS NOT NULL THEN 'принято сточных вод'
						 END "услуги3"             
						,CASE 
						   WHEN S.CWS_ID_CONTRACT IS NOT NULL AND S.SEW_ID_CONTRACT IS NOT NULL THEN 'Подача холодной воды и прием сточных вод осуществлялись' 
						   WHEN S.CWS_ID_CONTRACT IS NOT NULL THEN 'Подача холодной воды осуществлялась'  				 
							 WHEN S.SEW_ID_CONTRACT IS NOT NULL THEN 'Прием сточных вод осуществлялся'
						 END "услуги4"   
						,'Договор холодного водоснабжения № '||s.CWS_CTR_NUM||' от '||TO_CHAR(S.CWS_CTR_DATE,'DD.MM.YYYY') "договор_хвс"
            ,'Договор водоотведения № '||s.SEW_CTR_NUM||' от '||TO_CHAR(S.SEW_CTR_DATE,'DD.MM.YYYY') "договор_во",
						 (SELECT SUM(FD.AMOUNT) FROM T_CLI_FINDOCS FD, T_CLI_DEBTS D 
						  WHERE FD.ID_DOC = D.ID_DOC AND D.ID_WORK = S.ID_CASE) "общая_сумма_к_оплате",
						 (SELECT SUM(FD.AMOUNT) FROM T_CLI_FINDOCS FD, T_CLI_DEBTS D 
						  WHERE FD.ID_DOC = D.ID_DOC AND D.ID_WORK = S.ID_CASE
							AND   FD.ID_CONTRACT = S.CWS_ID_CONTRACT) "сумма_к_оплате1",             
						 (SELECT SUM(FD.AMOUNT) FROM T_CLI_FINDOCS FD, T_CLI_DEBTS D 
						  WHERE FD.ID_DOC = D.ID_DOC AND D.ID_WORK = S.ID_CASE
							AND   FD.ID_CONTRACT = S.SEW_ID_CONTRACT) "сумма_к_оплате2",
						 (SELECT SUM(FD.AMOUNT) - NVL(SUM(FD.DEBT),0) FROM T_CLI_FINDOCS FD, T_CLI_DEBTS D 
						  WHERE FD.ID_DOC = D.ID_DOC AND D.ID_WORK = S.ID_CASE) "общая_сумма_оплаты",
						 (SELECT SUM(FD.AMOUNT) - NVL(SUM(FD.DEBT),0) FROM T_CLI_FINDOCS FD, T_CLI_DEBTS D 
						  WHERE FD.ID_DOC = D.ID_DOC AND D.ID_WORK = S.ID_CASE
							AND   FD.ID_CONTRACT = S.CWS_ID_CONTRACT) "сумма_оплаты1",             
						 (SELECT SUM(FD.AMOUNT) - NVL(SUM(FD.DEBT),0) FROM T_CLI_FINDOCS FD, T_CLI_DEBTS D 
						  WHERE FD.ID_DOC = D.ID_DOC AND D.ID_WORK = S.ID_CASE
							AND   FD.ID_CONTRACT = S.SEW_ID_CONTRACT) "сумма_оплаты2",              
							s.SUM_DEBT "общая_сумма_долга",
							(SELECT SUM(FD.DEBT) FROM T_CLI_FINDOCS FD, T_CLI_DEBTS D 
						  WHERE FD.ID_DOC = D.ID_DOC AND D.ID_WORK = S.ID_CASE
							AND   FD.ID_CONTRACT = S.CWS_ID_CONTRACT) "сумма_задолженности1",             
						 (SELECT SUM(FD.DEBT) FROM T_CLI_FINDOCS FD, T_CLI_DEBTS D 
						  WHERE FD.ID_DOC = D.ID_DOC AND D.ID_WORK = S.ID_CASE
							AND   FD.ID_CONTRACT = S.SEW_ID_CONTRACT) "сумма_задолженности2",  
							S.PENY "общая_сумма_пеней",
						  s.PENY_DATE "дата_пени",
						 (SELECT ROUND(SUM(D.PENY),2) FROM T_CLI_DEBTS D WHERE D.ID_WORK = S.ID_CASE AND D.ID_CONTRACT = S.CWS_ID_CONTRACT) "сумма_пеней1",  
             (SELECT ROUND(SUM(D.PENY),2) FROM T_CLI_DEBTS D WHERE D.ID_WORK = S.ID_CASE AND D.ID_CONTRACT = S.SEW_ID_CONTRACT) "сумма_пеней2",
						 DECODE(S.CWS_ID_CONTRACT,NULL,'false','true') "если_водоснабжение",
 						 DECODE(S.SEW_ID_CONTRACT,NULL,'false','true') "если_водоотведение",
						 '«'||TO_CHAR(SYSDATE,'dd')||'» '||LOWER(PKG_UTILS.F_GET_NAME_MONTHS(SYSDATE,'R'))||' '||TO_CHAR(SYSDATE,'YYYY')||'г.' "дата_иска",
						 (SELECT S.COURT_NAME FROM T_COURTS S WHERE S.ID_COURT = TO_NUMBER(P_PAR_02)) "суд"						 
			FROM V_CLI_CASES S,			    
			     T_USER_LIST L
			WHERE S.ID_CASE = TO_NUMBER(P_PAR_01)
			AND   S.CURATOR_COURT = L.ID(+);
			
    L_XML := PKG_CONTROLLER.F_DOCX_XML(P_SQL => L_SQL,P_REP_CODE => 'LAWSUIT');    		
    L_REP.P_BLOB := PKG_CONTROLLER.F_GET_REPORT_XML(L_XML.getClobVal());
    L_REP.P_FILE_NAME := 'Исковое';
		
		RETURN l_rep;
	END;										
	
end PKG_DOCX_004;
/

