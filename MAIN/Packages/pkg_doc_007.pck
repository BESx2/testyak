i�?CREATE OR REPLACE PACKAGE LAWMAIN.PKG_DOC_007 IS

	-- Author  : BES
	-- Created : 14.02.2019
	-- Purpose : Возврат исполнительного листа
	-- CODE    : LEVY_RETURN



	FUNCTION RUN_REPORT(P_PAR_01 VARCHAR2 DEFAULT NULL
										 ,P_PAR_02 VARCHAR2 DEFAULT NULL
										 ,P_PAR_03 VARCHAR2 DEFAULT NULL
										 ,P_PAR_04 VARCHAR2 DEFAULT NULL
										 ,P_PAR_05 VARCHAR2 DEFAULT NULL
										 ,P_PAR_06 VARCHAR2 DEFAULT NULL
										 ,P_PAR_07 VARCHAR2 DEFAULT NULL
										 ,P_PAR_08 VARCHAR2 DEFAULT NULL
										 ,P_PAR_09 VARCHAR2 DEFAULT NULL
										 ,P_PAR_10 VARCHAR2 DEFAULT NULL)
		RETURN LAWSUP.PKG_FILES.REC_DOC;

END PKG_DOC_007;
/

CREATE OR REPLACE PACKAGE BODY LAWMAIN.PKG_DOC_007 IS

  TYPE REC_DOC IS RECORD(
     P_BLOB BLOB DEFAULT NULL);

  RTF_DOC REC_DOC := NULL;

  ------------------------------------------------------------------------

  PROCEDURE P_ADD_DATA(P_ID_CASE NUMBER) IS
    L_BLOB  BLOB;
    L_RTF   CLOB;
    L_VAL   VARCHAR2(10000);
    L_FMT   VARCHAR2(50) := '999G999G999G999G990D99';
    L_NLS   VARCHAR2(50) := 'NLS_NUMERIC_CHARACTERS='', ''';
  BEGIN
  
    SELECT T.FILE_CONTENT INTO L_RTF FROM T_RTF_TEMPLATES T WHERE T.ID = 10;
  
    FOR I IN (SELECT R.ROSP_NAME
			              ,R.ADDRESS
										,INITCAP(PKG_COURTS.F_GET_FIO_BAILIFF(ex.ID_BAILIFF,'Y')) BAILIFF									
                    ,T.CLI_NAME
                    ,T.LEVY_SERIES
                    ,T.LEVY_NUM
                    ,T.CURATOR             
                    ,T.PHONE_PRIV
										,EX.EXEC_NUM
										,EX.EXEC_START_DATE                                 
                    ,(SELECT S.COURT_NAME FROM T_COURTS S WHERE S.ID_COURT = E.ID_COURT) COURT_NAME
										,T.DOVER_INFO
              FROM (SELECT S.CLI_NAME
                          ,(SELECT MAX(E.ID_EXEC) FROM T_CLI_LEVY_EXECS E
													  WHERE E.ID_LEVY = L.ID_LEVY) ID_LEVY_EXEC
                          ,(SELECT MAX(E.ID)
                            FROM T_CLI_COURT_EXEC E 
                            WHERE E.ID_CASE = S.ID_CASE) ID_EXEC
													,(SELECT MAX(LM.ID_MOV)
													  FROM T_CLI_LEVY_MOVING LM
														WHERE LM.ID_LEVY = L.ID_LEVY
														AND   LM.MOV_TYPE = 'ROSP') ID_MOV
                          ,L.LEVY_SERIES
                          ,L.LEVY_NUM                                  
                          ,INITCAP(SUBSTR(UL.FIRST_NAME,1,1) || '. ' ||
                                   SUBSTR(UL.SECOND_NAME,1,1) || '. ' || 
                                   UL.LAST_NAME) CURATOR
                          ,UL.PHONE_PRIV              
													,UL.DOVER_INFO
                    FROM V_CLI_CASES S
                    LEFT OUTER JOIN T_USER_LIST UL
                    ON S.CURATOR_FSSP = UL.ID                
                    INNER JOIN T_CLI_LEVY L
                    ON L.ID_CASE = S.ID_CASE
                    WHERE S.ID_CASE = P_ID_CASE) T
                  ,T_CLI_LEVY_EXECS EX                     
                  ,T_CLI_COURT_EXEC E                         
									,T_CLI_LEVY_MOVING M 
									,T_ROSP R
              WHERE EX.ID_EXEC = T.ID_LEVY_EXEC
							AND   M.ID_MOV = T.ID_MOV
							AND   R.ID_ROSP = M.ID_PLACE              
              AND   E.ID = T.ID_EXEC)
    LOOP
      L_RTF := REPLACE(L_RTF,'ROSPNAME',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(I.ROSP_NAME));
      L_RTF := REPLACE(L_RTF,'ROSPADDRESS',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(I.ADDRESS));
      L_RTF := REPLACE(L_RTF,'LEVYNUMBER',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(I.LEVY_SERIES||' № '||I.LEVY_NUM));      
      L_RTF := REPLACE(L_RTF,'COURTNAME',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(I.COURT_NAME));                        
      L_RTF := REPLACE(L_RTF,'CLINAME',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(I.CLI_NAME));                                             
      L_RTF := REPLACE(L_RTF,'BAILLIFNAME',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(PKG_MORFER.F_GET_DECLENSION(nvl(I.BAILIFF,'-'),'Dat','S')));
      L_RTF := REPLACE(L_RTF,'CURATOR',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(I.CURATOR));                                
			L_RTF := REPLACE(L_RTF,'EXECNUM',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(I.Exec_Num));                                             
			L_RTF := REPLACE(L_RTF,'EXECDATE',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(to_char(I.Exec_Start_Date,'dd.mm.yyyy')));			
      L_RTF := REPLACE(L_RTF,'PHONE',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(I.PHONE_PRIV));     
			L_RTF := REPLACE(L_RTF,'ORGANIZATION',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(PKG_PREF.F$C2('REQUISITES')));      
			L_RTF := REPLACE(L_RTF,'DOVERINFO',PKG_DOC_REPORTS.F_CONVERT_TO_RTF(I.DOVER_INFO)); 						                                                            
    END LOOP;
  
  
    L_BLOB         := PKG_DOC_REPORTS.CLOB2BLOB(PAR_CLOB => L_RTF);
    RTF_DOC.P_BLOB := L_BLOB;
                                   
  END P_ADD_DATA;

  ---------------------------------------------------------------------------------------------------------------------------------

  FUNCTION RUN_REPORT(P_PAR_01 VARCHAR2 DEFAULT NULL
                     ,P_PAR_02 VARCHAR2 DEFAULT NULL
                     ,P_PAR_03 VARCHAR2 DEFAULT NULL
                     ,P_PAR_04 VARCHAR2 DEFAULT NULL
                     ,P_PAR_05 VARCHAR2 DEFAULT NULL
                     ,P_PAR_06 VARCHAR2 DEFAULT NULL
                     ,P_PAR_07 VARCHAR2 DEFAULT NULL
                     ,P_PAR_08 VARCHAR2 DEFAULT NULL
                     ,P_PAR_09 VARCHAR2 DEFAULT NULL
                     ,P_PAR_10 VARCHAR2 DEFAULT NULL)
    RETURN LAWSUP.PKG_FILES.REC_DOC IS
    REC_DOC LAWSUP.PKG_FILES.REC_DOC;
  BEGIN
  
    RTF_DOC := NULL;
    P_ADD_DATA(to_number(P_PAR_01));
    REC_DOC.P_BLOB      := RTF_DOC.P_BLOB;
    REC_DOC.P_FILE_NAME := 'Otziv_ID_' || P_PAR_01;
  
    RETURN REC_DOC;
  
  END RUN_REPORT;

END PKG_DOC_007;
/

