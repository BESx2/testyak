i�?create or replace package lawmain.PKG_CLOSE is

  -- Author  : EUGEN
  -- Created : 15.03.2019 16:26:03
  -- Purpose : Пакет для работы со статусами закрытия дел
  
   PROCEDURE P_UPDATE_CLOSE_REASON(P_ID_REASON NUMBER,
		                              P_REASON    VARCHAR2,
																	P_IS_ACTIVE VARCHAR2,
                                  P_DESCR     VARCHAR2 DEFAULT NULL); 
   
	 
  PROCEDURE P_CREATE_CLOSE_REASON(P_REASON VARCHAR2,
		                              P_DESCR  VARCHAR2 DEFAULT NULL,
																	P_DEP    VARCHAR2);
end PKG_CLOSE;
/

create or replace package body lawmain.PKG_CLOSE is

  PROCEDURE P_CREATE_CLOSE_REASON(P_REASON VARCHAR2,
		                              P_DESCR  VARCHAR2 DEFAULT NULL,
																	P_DEP    VARCHAR2)
		IS
    L_CODE VARCHAR2(50);
	BEGIN
		L_CODE := REPLACE(SUBSTR(PKG_UTILS.F_MAKE_TRANSLIT(P_REASON),1,50),' ','_');
		INSERT INTO T_CASE_CLOSE_REASON(ID_REASON,REASON,DESCRIPTION,
                                    CODE_REASON,DEP,CREATED_BY,DATE_CREATED)
		VALUES(SEQ_MAIN.NEXTVAL,P_REASON,P_DESCR,L_CODE,P_DEP,f$_usr_id,SYSDATE);																            
	END;
  
	PROCEDURE P_UPDATE_CLOSE_REASON(P_ID_REASON NUMBER,
		                              P_REASON    VARCHAR2,
																	P_IS_ACTIVE VARCHAR2,
                                  P_DESCR     VARCHAR2 DEFAULT NULL)
	  IS
	BEGIN																
		UPDATE T_CASE_CLOSE_REASON R
		   SET R.REASON = P_REASON,
			     R.DESCRIPTION = P_DESCR,
					 R.IS_ACTIVE = P_IS_ACTIVE
		 WHERE R.ID_REASON = P_ID_REASON;		 			 
	END;								

end PKG_CLOSE;
/

