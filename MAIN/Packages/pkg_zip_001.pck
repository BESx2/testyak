i�?create or replace package lawmain.PKG_ZIP_001 is

  -- Author  : MM
  -- Created : 06.03.2017 12:32:49
  -- Purpose : Печать комплекта документов: исковое заявление, расчет пени, акт сверки
  

  FUNCTION RUN_REPORT(P_PAR_01 VARCHAR2 DEFAULT NULL
											,P_PAR_02 VARCHAR2 DEFAULT NULL
											,P_PAR_03 VARCHAR2 DEFAULT NULL
											,P_PAR_04 VARCHAR2 DEFAULT NULL
											,P_PAR_05 VARCHAR2 DEFAULT NULL
											,P_PAR_06 VARCHAR2 DEFAULT NULL
											,P_PAR_07 VARCHAR2 DEFAULT NULL
											,P_PAR_08 VARCHAR2 DEFAULT NULL
											,P_PAR_09 VARCHAR2 DEFAULT NULL
											,P_PAR_10 VARCHAR2 DEFAULT NULL) RETURN LAWSUP.PKG_FILES.REC_DOC;	

end PKG_ZIP_001;
/

create or replace package body lawmain.PKG_ZIP_001 is
  	

---------------------------------------------------------------------------------------------------------------------------------

  FUNCTION RUN_REPORT(P_PAR_01 VARCHAR2 DEFAULT NULL
											,P_PAR_02 VARCHAR2 DEFAULT NULL
											,P_PAR_03 VARCHAR2 DEFAULT NULL
											,P_PAR_04 VARCHAR2 DEFAULT NULL
											,P_PAR_05 VARCHAR2 DEFAULT NULL
											,P_PAR_06 VARCHAR2 DEFAULT NULL
											,P_PAR_07 VARCHAR2 DEFAULT NULL
											,P_PAR_08 VARCHAR2 DEFAULT NULL
											,P_PAR_09 VARCHAR2 DEFAULT NULL
											,P_PAR_10 VARCHAR2 DEFAULT NULL) RETURN LAWSUP.PKG_FILES.REC_DOC 
	IS
	l_ret LAWSUP.PKG_FILES.REC_DOC;
	l_zip BLOB;
	L_REP LAWSUP.PKG_FILES.REC_DOC;
	L_BLOB LAWSUP.PKG_FILES.REC_DOC;      
	l_type VARCHAR2(10);
BEGIN
	DBMS_LOB.createtemporary(l_zip,FALSE);
  FOR I IN (SELECT F.ID_FOLDER, f.order_num FROM T_FOLDERS F WHERE F.ID_PARENT = P_PAR_01)  
		LOOP
			L_REP := PKG_PDF_002.RUN_REPORT(I.ID_FOLDER);
		  PKG_ZIP.add1file(p_zipped_blob => l_zip,p_name => 'Список пртензий '||i.order_num||'.pdf',p_content => l_rep.p_blob);
		END LOOP;
                                          
	PKG_ZIP.finish_zip(p_zipped_blob => l_zip); 
	
	l_BLOB.P_BLOB := l_zip;
	l_BLOB.P_FILE_NAME := 'Комплект претензий';
	RETURN L_BLOB;
	

  END RUN_REPORT;		



end PKG_ZIP_001;
/

