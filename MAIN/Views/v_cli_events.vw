i�?CREATE OR REPLACE FORCE VIEW LAWMAIN.V_CLI_EVENTS AS
SELECT ev.id_event,
			 ev.ID_WORK,
			 ev.date_created,
			 ev.created_by,
			 t.code_event,
			 t.short_name,
			 t.Event_Desc,
			 EV.DATE_EVENT,
			 T.TYPE_EVENT
FROM T_CLI_EVENTS ev,
		 T_EVENT_TYPES t
WHERE ev.id_event_type = t.id_event_type;

