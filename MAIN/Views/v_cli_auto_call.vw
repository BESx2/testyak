i�?CREATE OR REPLACE FORCE VIEW LAWMAIN.V_CLI_AUTO_CALL AS
SELECT AC.ID_CALL
			,AC.CREATED_BY
			,AC.DATE_CREATED
			,AC.DEBT_CREATED
			,AC.DEBT_ON_DATE
			,AC.CURRENT_DEBT
			,AC.ID_FOLDER
			,AC.PHONE
			,AC.IS_VALID
			,AC.ID_WORK
			,AC.ID_CWS_CONTRACT
			,AC.ID_SEW_CONTRACT
			,AC.MESSAGE
			,AC.CALL_STATUS
			,AC.DATE_CALL
			,AC.CALL_CNT
			,AC.UTC
			,CWS.CTR_NUMBER CWS_CTR_NUMBER
			,SEW.CTR_NUMBER SEW_CTR_NUMBER
FROM   T_CLI_AUTO_CALL AC, T_CONTRACTS CWS, T_CONTRACTS SEW
WHERE  AC.ID_CWS_CONTRACT = CWS.ID_CONTRACT(+)
AND    AC.ID_SEW_CONTRACT = SEW.ID_CONTRACT(+);

